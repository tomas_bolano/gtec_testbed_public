%% Inicializacións básicas

clear variables;
close all;
format long g
restoredefaultpath;
clc;

%% Definición de variables básicas dependentes do caso de estudio:

addpath('/home/tecrail/gtisrc/LTEFDD_PHY/')
TECRAIL_lte_RX_Metro_RepresentaPDPs_Parametros_ot_ch0_mode00;

RutaActual = [fileparts(fileparts(mfilename('fullpath'))) filesep];
addpath([RutaActual 'LTEFDD_PHY/'])
addpath([RutaActual 'LTEFDD_PHY' filesep 'GPS_Processing'])

%% Inicializacións avanzadas

% >> Constantes

fs=15.36e6;
    
% >> Engado partes necesarias para o salvagardado e lectura de datos:

RutaResultados='/home/tecrail/gtisrc/5G/common/ArtigoCapacidadeFerroviario_AWPL2016/MetroResults/';
addpath(RutaResultados);
load(ResultsFilename);
load([ResultsFilename(1:(end-4)) '_' IdentificadorFicheirosProcesados ResultsFilename((end-3):end)])

%% Representación de exemplo


% Calcular pdp
%MostrasPDP = size(estChannels,1);
%MostrasPDP = 1024;
PDPs_2 = zeros(MostrasPDP, max(EixoIndicesValidos));

for ii = 1:max(EixoIndicesValidos);
    Indice = EixoIndicesValidos(ii);
    
    estChannelPDP = estChannels(:,:,Indice);
    estChannelPDP = bsxfun(@times, estChannelPDP, hanning(size(estChannels,1)));
    timeResponses = sqrt(size(estChannels,1))*ifft(estChannelPDP,MostrasPDP);
    timeResponses = timeResponses(1:MostrasPDP,:);
    pdp = diag(abs(timeResponses*(timeResponses')))/140;
    PDPs_2(:,Indice) = pdp;
end

% for kk=1:size(PDPs,2)
%     PDPs(:,kk)=PDPs(:,kk)/max(PDPs(:,kk));
% end

eixoy = ((0:(MostrasPDP-1))*(1/15.36e6))*1e6;
eixox = EixoIndicesValidos;

% Referencia de eixo x temporal (xa non teño información do GPS)

% obtencion puntos kilometricos equivalentes para instantes temporais
% FactorDesalinhamento = 0;
% RutaGPSLog = [MeasurementsFolder samplesFilename '.usrpnmea'];
% GPSData = getGPSData(RutaGPSLog);
% DesprazamentoFinalPorIndicesNonValidos = EixoIndicesValidos(end)-length(IndicesCela);
Eixot = (1/fs)*TramasSeparacion*140*(1024+(6*72+80)/7)*(EixoIndicesValidos-1);
% puntosKmtsD = getGPSKP(GPSData, 0, Eixot,getEndFileOffsetGPSKP(GPSData, tfgps+DesprazamentoFinalPorIndicesNonValidos), FactorDesalinhamento);
% eixox=puntosKmtsD;
eixox = Eixot;

% PDPsNormalizados = PDPs(:,EixoIndicesValidos)/max(max(PDPs(:,EixoIndicesValidos)));
% PDPsRecortados = PDPsNormalizados;
% PDPsRecortados(PDPsRecortados<(10^(-MaximoRangoDBsRepresentacion/10)))=10^(-MaximoRangoDBsRepresentacion/10);
%PDPsRecortados = PDPs(:,EixoIndicesValidos);
PDPsRecortados = PDPs_2(:,EixoIndicesValidos);
eixoy=eixoy-MostrasRetardoAnticausal*(1/15.36e6)*1e6;
figure,surf(eixox,eixoy,10*log10(circshift(PDPsRecortados,MostrasRetardoAnticausal)))
shading interp
%shading flat
view([0 90])
colorbar
title('normalized power delay profile [dB]'),xlabel('time [s]'),ylabel('relative delay [\mus]')
%xlim([1 length(EixoIndicesValidos)]);
xlim([eixox(1) eixox(end)]);
ylim([eixoy(1) eixoy(end)])
ylim([eixoy(1) LimiteMaximoRepresentacionPDPsUs])
caxis(10*log10(max(max(abs(PDPsRecortados))))+[-MaximoRangoDBsRepresentacion 0]);
c=caxis;

%% Cálculo do RMS Doppler Spread

PDPsRecortadosC = circshift(PDPsRecortados,round(MostrasPDP/2));

DopplerSpread=zeros(1,length(EixoIndicesValidos));
for ii=1:length(EixoIndicesValidos)
    DopplerSpread(ii)=sqrt(sum(PDPsRecortadosC(:,ii).*(((eixoy*1e-6).^2).'))...
        /sum(PDPsRecortadosC(:,ii))-(sum(PDPsRecortadosC(:,ii).*((eixoy*1e-6).'))...
        /sum(PDPsRecortadosC(:,ii))).^2);
end

figure()
plot(eixox,DopplerSpread*1e6);
% xlim([1 length(EixoIndicesValidos)]);
xlim([eixox(1) eixox(end)]);
grid on
xlabel('time [s]')
ylabel('RMS delay spread [\mu{}s]')
title('delay spread')

% eixoy = ((0:71)*(1/15.36e6))*1e6;
% eixox = EixoIndicesValidos;
% PDPsNormalizados = PDPs(:,EixoIndicesValidos)/max(max(PDPs(:,EixoIndicesValidos)));
% PDPsRecortados = PDPsNormalizados;
% PDPsRecortados(PDPsRecortados<(10^(-MaximoRangoDBsRepresentacion/10)))=10^(-MaximoRangoDBsRepresentacion/10);
% figure,surf(eixox,eixoy,10*log10(PDPsRecortados))
% shading interp
% view([0 90])
% colorbar
% title('normalized power delay profile [dB]'),xlabel('kilometric point [km]'),ylabel('relative delay [\mus]')
% xlim([1 length(EixoIndicesValidos)]);
% ylim([eixoy(1) eixoy(end)])

%% Cálculo da SNR ao longo da traxectoria

%
% Coa seguinte instrucción haberase de estimar un valor fixo do parámetro
% ValorRuidoEstimado, que ha de quedar reflexado no arquivo de parámetros.
%
% figure,plot(10*log10(potenciaDatos(:)))
%

SNR=10*log10(mean((potenciaDatos-ValorRuidoEstimado)./ValorRuidoEstimado));

figure()
plot(eixox,SNR);
xlim([eixox(1) eixox(end)]);
grid on
xlabel('time [s]')
ylabel('SNR [dB]')
title('signal-to-noise ratio')

%% Miro de facer unha gráfica tendo en conta o retardo de propagación

return; % Polo momento non podo calcula-lo retardo de propagación

MostrasRetardo = PuntosSincronizacionValidos-min(PuntosSincronizacionValidos);
%eixoy = ((0:((MostrasPDP-1)+max(MostrasRetardo)))*(1/15.36e6))*1e6;
eixoy = ((0:((MostrasPDP-1)))*(1/15.36e6))*1e6;

% PDPsRetardados = (10^(-MaximoRangoDBsRepresentacion/10))*ones(length(eixoy),size(PDPsRecortados,2));

%PDPsRetardados = c(1)*ones(length(eixoy),size(PDPsRecortados,2));
%PDPsRetardados(1:size(PDPsRecortados,1),1:size(PDPsRecortados,2))=PDPsRecortados;
PDPsRetardados = PDPsRecortados;
for kk=1:length(EixoIndicesValidos)
    if(MostrasRetardo(kk)>0)
        PDPsRetardados(:,kk)=circshift(PDPsRetardados(:,kk),MostrasRetardo(kk)+0);
        %PDPsRetardados(1:MostrasRetardo(kk),kk)=(10^(-MaximoRangoDBsRepresentacion/10));
    end
end
figure,surf(eixox,eixoy,10*log10(PDPsRetardados))
shading interp
view([0 90])
colorbar
caxis(c);
title('normalized power delay profile [dB]'),xlabel('kilometric point [km]'),ylabel('relative delay [\mus]')
% xlim([1 length(EixoIndicesValidos)]);
xlim([eixox(1) eixox(end)]);
ylim([eixoy(1) eixoy(MostrasPdpRepresentacion)])