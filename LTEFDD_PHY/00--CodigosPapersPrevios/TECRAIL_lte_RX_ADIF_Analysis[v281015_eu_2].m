% TODO: calcular tamaño de trama lte para saber canto capturar


clear variables;
close all;
format long g
clc;

fprintf('\t ==================================================\n');
fprintf('\t\t TECRAIL USRP B210 LTE 10 MHz MIMO sniffer\n');
fprintf('\t\t       ADIF Measurements 9-10-dec-2014     \n');
fprintf('\t ==================================================\n');

addpath('~/src/testbed/src/pnet', '-end');
addpath('~/src/testbed/src/matlab_utils','-end');
addpath('~/src/testbed/src/pers/net_commands', '-end');
addpath('~/Desktop','-end');
addpath('~/src/testbed/src/LTE_noGUI','-end');

% % % % %************************* RECEPTION MODE *********************************
% % % % %available options:
% % % % % 0 = standard-compliant LTE with OUTDOOR ANTENNAS
% % % % %
% % % % % 1 = standard-compliant LTE with INDOOR ANTENNAS
% % % % %
% % % % RECEPTION_MODE = 1;
% % % % 
% % % % %available options:
% % % % % 0: 'MeasureOnly' = Only acquisition on receiver.
% % % % % 1: 'MeasureAndProcess' = Acquisition and processing data on receiver.
% % % % EXECUTION_MODE = 0;
% % % % 
% % % % %**************************************************************************
% % % % 
% % % % 
% % % % 
% % % % %******************** PARAMETER INITIALIZATION ****************************
% % % % % Hardware parameters
% % % % rfCarrier = 2.63e9;
signalSamplingRate = 15.36e6;
% % % % % rxGain = 40;
% % % % % masterClockRate = 30.72e6;
% % % % % rfBandwidth = 52e6;
% % % % % doGenerateNMEALog = 1;
% % % % % secondsBetweenNMEAMeasurements = 2;
% % % % % numberOfTrialsGPSLocked = 10;
% % % % % numberOfTrialsREFLocked = 10;
% % % % % 
% % % % % % Common parameters
% % % % % rxUSRPSerialNumber = ''; % If empty, then the first one is used 
% % % % % usrpReferenceSource = 'internal';
% % % % % rxIP = '127.0.0.1';
% % % % % rxPort = 5002;
% % % % % rxLocalPort= 5001;
% % % % % 
% % % % % % Transmission-mode-dependent parameters
% % % % % if (RECEPTION_MODE == 0)
% % % % %     fprintf('\n RECEPTION MODE: standard-compliant 2x2 MIMO 10 MHz LTE from OUTDOOR ANTENNAS\n');
% % % % %     Ntx = 2;
% % % % %     Nrx = 2;
% % % % %     doFrequencyShift = 0;
% % % % %     interpolationFactors = 1;
% % % % %     decimationFactorForProcessing = interpolationFactors(1);
% % % % %     nZeroSamples = 0;
% % % % %     measurementDirectorySuffix = '--LTE_ADIF_BW-10MHz_Fc-2630MHz_singleLayer';
% % % % %     measurementsDirectoryDesc = 'outdoor-antennas';
% % % % %     measurementFilenamePrefix = '_outdoor_etcs_voip_max_n_to_s';
% % % % % elseif (RECEPTION_MODE == 1)
% % % % %     fprintf('\n RECEPTION MODE: standard-compliant 2x2 MIMO 10 MHz LTE from INDOOR ANTENNAS\n');
% % % % %     Ntx = 2;
% % % % %     Nrx = 2;
% % % % %     doFrequencyShift = 0;
% % % % %     interpolationFactors = 1;
% % % % %     decimationFactorForProcessing = interpolationFactors(1);
% % % % %     nZeroSamples = 0;
% % % % %     measurementDirectorySuffix = '--LTE_ADIF_BW-10MHz_Fc-2630MHz_singleLayer';
% % % % %     measurementsDirectoryDesc = 'indoor-antennas';
% % % % %     measurementFilenamePrefix = '_ruido';
% % % % % else
% % % % %     error('Wrong ''RECEPTION_MODE'' value (%d)', RECEPTION_MODE);
% % % % % end
% % % % % 
% % % % % if (EXECUTION_MODE == 0)
% % % % %     usrpWaitForKeyboard = true; % true => requires key pressed before acquisition starts
% % % % %     doAcquireWithBothAntennas = 1;
% % % % % elseif (EXECUTION_MODE == 1)
% % % % %     usrpWaitForKeyboard = false;
% % % % %     doAcquireWithBothAntennas = 1;
% % % % % else
% % % % %     error('Wrong ''EXECUTION_MODE'' value (%d)', EXECUTION_MODE);
% % % % % end
% % % % % 
% % % % % % % % % Necessary to avoid interference
% % % % % % % % if (doFrequencyShift)
% % % % % % % %     rfCarrier = rfCarrier - 2*fc;
% % % % % % % % end
% % % % % 
% % % % % basePathForDataFiles = fullfile(filesep, 'home', 'tecrail', 'RXsignals');
% % % % % if (isempty(dir(basePathForDataFiles)))
% % % % %     mkdir(basePathForDataFiles);
% % % % % end
% % % % % 
% % % % % rxSignalsInUSRPFormatFilenames = {...
% % % % %     fullfile(basePathForDataFiles, 'LTE_RX_FramesCh0.usrp'), ...
% % % % %     fullfile(basePathForDataFiles, 'LTE_RX_FramesCh1.usrp')};
% % % % % 
% % % % % outputRxFile = fullfile(basePathForDataFiles, 'ReceivedFrames.nosplit.usrp');
% % % % % 
% % % % % if (doAcquireWithBothAntennas)
% % % % %     splitRxFiles = [rxSignalsInUSRPFormatFilenames{1}, ', ', rxSignalsInUSRPFormatFilenames{2}];
% % % % % else
% % % % %     splitRxFiles = rxSignalsInUSRPFormatFilenames{1};
% % % % % end
% % % % % 
% % % % % measurementTimestamp = now;
% % % % % measurementsDirectoryPath = fullfile(filesep, ...
% % % % %     'media', 'tecrail', 'measData', 'measurements', ...
% % % % %     [datestr(measurementTimestamp,'yyyy-mm-dd'), measurementDirectorySuffix]);
% % % % % 
% % % % % %**************************************************************************
% % % % % 
% % % % % 
% % % % % 
% % % % % %************************* USRP INITIALIZATION ****************************
% % % % % % Save library paths
% % % % % MatlabPath = getenv('LD_LIBRARY_PATH');
% % % % % 
% % % % % % Make Matlab use system libraries
% % % % % setenv('LD_LIBRARY_PATH',getenv('PATH'));
% % % % % 
% % % % % %initializate the transmitter
% % % % % cstr = 'gnome-terminal -t PERS_RX --profile hold_on_exit '; 
% % % % % cstr = [cstr, '-e "/home/tecrail/src/testbed/src/pers/build/pers_rx '];
% % % % % cstr = [cstr, '--args='''];
% % % % % if (~isempty(rxUSRPSerialNumber))
% % % % %     cstr = [cstr, 'serial=', rxUSRPSerialNumber, ', '];
% % % % % end
% % % % % cstr = [cstr, 'master_clock_rate=', num2str(masterClockRate), ', '];
% % % % % cstr = [cstr, 'recv_frame_size=16384, num_recv_frames=200'' '];
% % % % % cstr = [cstr, '--rx_rf_frequency ', num2str(rfCarrier), ' '];
% % % % % cstr = [cstr, '--sampling_rate ', num2str(signalSamplingRate), ' '];
% % % % % % cstr = [cstr, '--reference_clock_source ', usrpReferenceSource, ' '];
% % % % % cstr = [cstr, '--bandwidth ', num2str(rfBandwidth), ' '];
% % % % % cstr = [cstr, '--rx_gain ', num2str(rxGain), ' '];
% % % % % cstr = [cstr, '--samples_per_antenna_buffer=16384 --number_of_antenna_buffers 20000 '];
% % % % % cstr = [cstr, '--wait_for_key_pressed '];
% % % % % if (usrpWaitForKeyboard)
% % % % %     cstr = [cstr, 'true', ' '];    
% % % % % else
% % % % %     cstr = [cstr, 'false', ' '];
% % % % % end
% % % % % 
% % % % % if (doAcquireWithBothAntennas)
% % % % %     cstr = [cstr, '--channels_mapping "0,1" '];
% % % % % else
% % % % %     cstr = [cstr, '--channels_mapping "0" '];
% % % % % end
% % % % % 
% % % % % if (doGenerateNMEALog)
% % % % %     cstr = [cstr, '--generate_nmea_log '];
% % % % % end
% % % % % cstr = [cstr, '--seconds_between_nmea_measurements ', ...
% % % % %     num2str(secondsBetweenNMEAMeasurements), ' '];
% % % % % cstr = [cstr, '--number_of_trials_gps_locked ', ...
% % % % %     num2str(numberOfTrialsGPSLocked), ' '];
% % % % % cstr = [cstr, '--number_of_trials_ref_locked ', ...
% % % % %     num2str(numberOfTrialsREFLocked), ' '];
% % % % % 
% % % % % % Two measurement modes defined by the EXECUTION_MODE variable
% % % % % % 0 = 'MeasureOnly': signals are acquired continuously and stored in the SSD as they
% % % % % % come out of the PERS_RX, but no signal processing tasks are carried
% % % % % % out.
% % % % % %
% % % % % % 1 = 'MeasureAndProcess': a specific amount of samples (twice the length
% % % % % % of the TX signals) are acquired and processed fully using the WiMAX
% % % % % % SDR implementation.
% % % % % 
% % % % % if (EXECUTION_MODE == 0)
% % % % %     %create folder depending on configuration parameters
% % % % %     measurementDirectoryName = [...
% % % % %         num2str(Ntx), 'x', num2str(Nrx), ...
% % % % %         measurementsDirectoryDesc];
% % % % %     if (isempty(dir(fullfile(measurementsDirectoryPath, measurementDirectoryName))))
% % % % %         mkdir(measurementsDirectoryPath, measurementDirectoryName);
% % % % %     end
% % % % %     commandTimestampStr = datestr(now,'yyyy-mm-dd--HH-MM');
% % % % %     measurementFilenameWithPath = fullfile(...
% % % % %         measurementsDirectoryPath, measurementDirectoryName, ...
% % % % %         [ commandTimestampStr, ...
% % % % %           measurementFilenamePrefix, '.nosplit.usrp']);
% % % % % 
% % % % %     cstr = [cstr, '--output_file_name "', measurementFilenameWithPath, '" '];
% % % % %     cstr = [cstr, '--net_commands false '];
% % % % %     cstr = [cstr, '" & '];
% % % % %     % Saves the command file as a documentation
% % % % %     measurementPersCommandFilenameWithPath = fullfile(...
% % % % %         measurementsDirectoryPath, measurementDirectoryName, ...
% % % % %         [ commandTimestampStr, '_pers_rx_command.txt']);
% % % % %     fid = fopen(measurementPersCommandFilenameWithPath, 'w+');
% % % % %     if (fid ~= (-1))
% % % % %         fprintf(fid, ' # Command creation timestamp: %s\n # Command: \n %s\n', ...
% % % % %             commandTimestampStr, cstr);
% % % % %         fclose(fid);
% % % % %     else
% % % % %         error(' Cannot create filename: %s', ...
% % % % %             measurementPersCommandFilenameWithPath);
% % % % %     end
% % % % %     [status, cmdout] = unix(cstr);
% % % % %     
% % % % % elseif (EXECUTION_MODE == 1)
% % % % % 
% % % % %     nTotalTXSamples = sum((153600 + nZeroSamples) .* interpolationFactors);
% % % % %     cstr = [cstr, '--output_file_name "', outputRxFile, '" '];
% % % % %     cstr = [cstr, '--net_commands true '];
% % % % %     cstr = [cstr, ' --socket_port_number ', num2str(rxPort), ' '];
% % % % %     nTotalSamplesToAcquire = 2 * nTotalTXSamples;
% % % % %     cstr = [cstr, '--total_number_samples_to_receive ', ...
% % % % %         num2str(nTotalSamplesToAcquire), ' '];
% % % % %     cstr = [cstr, '" & '];
% % % % %     [status, cmdout] = unix(cstr);
% % % % %     
% % % % %     fprintf(' Waiting for USRP to be ready . . . ')
% % % % %     while (PERS_CommandReady(rxIP, rxPort, rxLocalPort) ~= 1)
% % % % %     end
% % % % %     fprintf('OK\n');
% % % % % else
% % % % %     error(' Not supported mode: %s\n', EXECUTION_MODE);
% % % % % end
% % % % % 
% % % % % % Reassign old library paths
% % % % % setenv('LD_LIBRARY_PATH',MatlabPath)
% % % % % %**************************************************************************
% % % % % 



%% *************************** DATA PROCESSING ****************************
% % if (EXECUTION_MODE == 1)
    
% % %     responseSetRxFile = PERS_CommandSetRxFile(rxIP, rxPort, rxLocalPort, outputRxFile)
% % % 
% % %     responseSetSplitFiles = PERS_CommandSetSplitRxFiles(rxIP, rxPort, rxLocalPort, splitRxFiles)
    
% % % % %     responseRxFrequency = ...
% % % % %         PERS_CommandRxFrequency(rxIP, rxPort, rxLocalPort, rfCarrier)
% % % % % 
% % % % %     responseRxGain = ...
% % % % %         PERS_CommandRxGain(rxIP, rxPort, rxLocalPort, rxGain)
% % % % % 
% % % % %     % Acquire
% % % % %     PERS_CommandStartRX(rxIP, rxPort, rxLocalPort)
% % % % % 
% % % % % % %     filenameCh1 = fullfile(filesep, 'home','tecrail','RXsignals','LteReceivedFramesSplit0.usrp');
% % % % % % %     filenameCh2 = fullfile(filesep, 'home','tecrail','RXsignals','LteReceivedFramesSplit1.usrp');
% % % % % % %     if (Nrx == 2)
% % % % % % %         LteAcquiredFileName = [filenameCh1, ',', filenameCh2];
% % % % % % %     else
% % % % % % %         LteAcquiredFileName = filenameCh1;
% % % % % % %     end
% % % % %     
% % % % %     responseSplitRxFiles = ...
% % % % %         PERS_CommandSetSplitRxFiles(rxIP, rxPort, rxLocalPort, splitRxFiles)
% % % % %     responseSplitRxData = ...
% % % % %         PERS_CommandSplitRxData(rxIP, rxPort, rxLocalPort)



basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.ch1.usrp')};


% step = 1024*10;
%     figure;
% success=1;
% iter=0;
% while success>0
%     % After USRP acqquisition
%     [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
%         rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
%     if (success == (-1))
%         fprintf('FAIL\n');
%         error('Cannot read the number of requested samples ');
%     elseif (success == 0)
%         fprintf('FAIL\n');
%         error(' Cannot read file acquired by the USRP ');
%     else
% %         fprintf('OK\n');
%     end
%     
%     rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
%     
% 
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
%     pause(.005)
%     iter=iter+1;
% end



% return

tic
step = 1024;
success=1;
TamanhoFicheiroBytes=38448726016;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.o.ch0.vpd.mat'),'valoresPotenciaDatos')

%% Representación gráfica

eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(valoresPotenciaDatos)/((15.36e6)/1024));
figure,plot(eixoTemporal,10+log10(valoresPotenciaDatos))

lonxitudeFiestra = 10000;
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;
Df=conv(valoresPotenciaDatos,F);
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
Df=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(Df)/((15.36e6)/1024));
figure,plot(eixoTemporal,10*log10(Df))

return

    %% Segundo ficheiro
    
    
    basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--00-06_max_throughput_50kmh_n_to_s.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--00-06_max_throughput_50kmh_n_to_s.ch1.usrp')};

    
    tic
step = 1024;
success=1;
TamanhoFicheiroBytes=36918657024;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--00-06_max_throughput_50kmh_n_to_s.o.ch0.vpd.mat'),'valoresPotenciaDatos')

%% Representación gráfica

eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(valoresPotenciaDatos)/((15.36e6)/1024));
figure,plot(eixoTemporal,10+log10(valoresPotenciaDatos))

lonxitudeFiestra = 10000;
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;
Df=conv(valoresPotenciaDatos,F);
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
Df=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(Df)/((15.36e6)/1024));
figure,plot(eixoTemporal,10*log10(Df))

return

%% Terceiro ficheiro
    
    
    basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.ch1.usrp')};

    
    tic
step = 1024;
success=1;
TamanhoFicheiroBytes=36748722176;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.o.ch0.vpd.mat'),'valoresPotenciaDatos')

%% Representación gráfica

eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(valoresPotenciaDatos)/((15.36e6)/1024));
figure,plot(eixoTemporal,10+log10(valoresPotenciaDatos))

lonxitudeFiestra = 10000;
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;
Df=conv(valoresPotenciaDatos,F);
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
Df=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(Df)/((15.36e6)/1024));
figure,plot(eixoTemporal,10*log10(Df))

return

    %% Cuarto ficheiro
    
    
    basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--00-07_max_throughput_50kmh_n_to_s.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--00-07_max_throughput_50kmh_n_to_s.ch0.usrp')};

    
    tic
step = 1024;
success=1;
TamanhoFicheiroBytes=36919902208;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--00-07_max_throughput_50kmh_n_to_s.i.ch0.vpd.mat'),'valoresPotenciaDatos')

%% Representación gráfica

eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(valoresPotenciaDatos)/((15.36e6)/1024));
figure,plot(eixoTemporal,10+log10(valoresPotenciaDatos))

lonxitudeFiestra = 10000;
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;
Df=conv(valoresPotenciaDatos,F);
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
Df=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(Df)/((15.36e6)/1024));
figure,plot(eixoTemporal,10*log10(Df))
    
    return
    
%% Quinto ficheiro
    
    
    basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.ch1.usrp')};

    
    tic
step = 1024;
success=1;
TamanhoFicheiroBytes=38453575680;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.i.ch0.vpd.mat'),'valoresPotenciaDatos')

%% Representación gráfica

eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(valoresPotenciaDatos)/((15.36e6)/1024));
figure,plot(eixoTemporal,10+log10(valoresPotenciaDatos))

lonxitudeFiestra = 10000;
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;
Df=conv(valoresPotenciaDatos,F);
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
Df=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(Df)/((15.36e6)/1024));
figure,plot(eixoTemporal,10*log10(Df))
    
    return
    
    
%% Sexto ficheiro
    
    
    basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.ch1.usrp')};

    
    tic
step = 1024;
success=1;
TamanhoFicheiroBytes=36752326656;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.i.ch0.vpd.mat'),'valoresPotenciaDatos')

%% Representación gráfica

eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(valoresPotenciaDatos)/((15.36e6)/1024));
figure,plot(eixoTemporal,10+log10(valoresPotenciaDatos))

lonxitudeFiestra = 10000;
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;
Df=conv(valoresPotenciaDatos,F);
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
Df=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(Df)/((15.36e6)/1024));
figure,plot(eixoTemporal,10*log10(Df))
    
    return
    
     %% Séptimo ficheiro
    
    
    basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--00-06_max_throughput_50kmh_n_to_s.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--00-06_max_throughput_50kmh_n_to_s.ch1.usrp')};

    
    tic
step = 1024;
success=1;
TamanhoFicheiroBytes=36918657024;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--00-06_max_throughput_50kmh_n_to_s.o.ch1.vpd.mat'),'valoresPotenciaDatos')

%% Representación gráfica

eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(valoresPotenciaDatos)/((15.36e6)/1024));
figure,plot(eixoTemporal,10+log10(valoresPotenciaDatos))

lonxitudeFiestra = 10000;
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;
Df=conv(valoresPotenciaDatos,F);
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
Df=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(Df)/((15.36e6)/1024));
figure,plot(eixoTemporal,10*log10(Df))
    
    return
    
%% Oitavo Ficheiro

basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.ch1.usrp')};
    

tic
step = 1024;
success=1;
TamanhoFicheiroBytes=38448726016;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.o.ch1.vpd.mat'),'valoresPotenciaDatos')

%% Representación gráfica

eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(valoresPotenciaDatos)/((15.36e6)/1024));
figure,plot(eixoTemporal,10+log10(valoresPotenciaDatos))

lonxitudeFiestra = 10000;
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;
Df=conv(valoresPotenciaDatos,F);
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
Df=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(Df)/((15.36e6)/1024));
figure,plot(eixoTemporal,10*log10(Df))
    
    return
    
%% Noveno ficheiro

 basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.ch1.usrp')};

    
    tic
step = 1024;
success=1;
TamanhoFicheiroBytes=36752326656;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.i.ch1.vpd.mat'),'valoresPotenciaDatos')

%% Décimo ficheiro
    
    
    basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--00-07_max_throughput_50kmh_n_to_s.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--00-07_max_throughput_50kmh_n_to_s.ch1.usrp')};

    
    tic
step = 1024;
success=1;
TamanhoFicheiroBytes=36919902208;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--00-07_max_throughput_50kmh_n_to_s.i.ch1.vpd.mat'),'valoresPotenciaDatos')


%% Representación gráfica

eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(valoresPotenciaDatos)/((15.36e6)/1024));
figure,plot(eixoTemporal,10+log10(valoresPotenciaDatos))

lonxitudeFiestra = 10000;
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;
Df=conv(valoresPotenciaDatos,F);
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
Df=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(Df)/((15.36e6)/1024));
figure,plot(eixoTemporal,10*log10(Df))

return

%% Décimo primeiro ficheiro
    
    
    basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.ch1.usrp')};

    
    tic
step = 1024;
success=1;
TamanhoFicheiroBytes=38453575680;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.i.ch1.vpd.mat'),'valoresPotenciaDatos')

%% Representación gráfica

eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(valoresPotenciaDatos)/((15.36e6)/1024));
figure,plot(eixoTemporal,10+log10(valoresPotenciaDatos))

lonxitudeFiestra = 10000;
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;
Df=conv(valoresPotenciaDatos,F);
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
Df=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(Df)/((15.36e6)/1024));
figure,plot(eixoTemporal,10*log10(Df))
    
    return
    
    
%% Décimo segundo ficheiro
    
    
    basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.ch1.usrp')};

    
    tic
step = 1024;
success=1;
TamanhoFicheiroBytes=36752326656;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.i.ch1.vpd.mat'),'valoresPotenciaDatos')

%% Representación gráfica

eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(valoresPotenciaDatos)/((15.36e6)/1024));
figure,plot(eixoTemporal,10+log10(valoresPotenciaDatos))

lonxitudeFiestra = 10000;
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;
Df=conv(valoresPotenciaDatos,F);
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
Df=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(Df)/((15.36e6)/1024));
figure,plot(eixoTemporal,10*log10(Df))
    
    
    return
    
%% Décimo terceiro ficheiro
    
    
    basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.ch1.usrp')};

    
    tic
step = 1024;
success=1;
TamanhoFicheiroBytes=36748722176;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.o.ch1.vpd.mat'),'valoresPotenciaDatos')

%% Representación gráfica

eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(valoresPotenciaDatos)/((15.36e6)/1024));
figure,plot(eixoTemporal,10+log10(valoresPotenciaDatos))

lonxitudeFiestra = 10000;
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;
Df=conv(valoresPotenciaDatos,F);
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
Df=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(Df)/((15.36e6)/1024));
figure,plot(eixoTemporal,10*log10(Df))
    
    
    return
    
%% Ficheiros a 100 e 200 km/h

% Décimo cuarto ficheiro
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.o.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.o.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=20910899200;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.o.ch0.vpd.mat'),'valoresPotenciaDatos')
    
% Décimo quinto ficheiro
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.o.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.o.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=20910899200;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.o.ch1.vpd.mat'),'valoresPotenciaDatos')

% Décimo sexto ficheiro
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-10--03-55_indoor_etcs_voip_100kmh_n_to_s.i.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-10--03-55_indoor_etcs_voip_100kmh_n_to_s.i.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=20929576960;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-10--03-55_indoor_etcs_voip_100kmh_n_to_s.i.ch0.vpd.mat'),'valoresPotenciaDatos')
    
% Décimo séptimo ficheiro
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-10--03-55_indoor_etcs_voip_100kmh_n_to_s.i.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-10--03-55_indoor_etcs_voip_100kmh_n_to_s.i.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=20929576960;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-10--03-55_indoor_etcs_voip_100kmh_n_to_s.i.ch1.vpd.mat'),'valoresPotenciaDatos')

% Décimo oitavo ficheiro
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-10--04-24_indoor_etcs_voip_200kmh_n_to_s.i.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-10--04-24_indoor_etcs_voip_200kmh_n_to_s.i.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=12775784448;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-10--04-24_indoor_etcs_voip_200kmh_n_to_s.i.ch0.vpd.mat'),'valoresPotenciaDatos')

% Décimo noveno ficheiro
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-10--04-24_indoor_etcs_voip_200kmh_n_to_s.i.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-10--04-24_indoor_etcs_voip_200kmh_n_to_s.i.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=12775784448;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-10--04-24_indoor_etcs_voip_200kmh_n_to_s.i.ch1.vpd.mat'),'valoresPotenciaDatos')
 

% Vixésimo ficheiro
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.o.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.o.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=12776505344;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.o.ch0.vpd.mat'),'valoresPotenciaDatos')

% Vixésimo primeiro ficheiro
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.o.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.o.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=12776505344;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=MostrasComplexasFicheiro/1024;
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.o.ch1.vpd.mat'),'valoresPotenciaDatos')
 
%% Análise de tódolos datos (tres pasadas)

clear
cd('/media/tecrail/measData/r/')

% for ki=1:3
%     for kj=1:2
%         for kk=1:2
%             valoresPotenciaDatos3Pasadas.Pasada(ki).Canle(kj).Localizacion(kk)=0;
%         end
%     end
% end

load('2014-12-11--00-06_max_throughput_50kmh_n_to_s.o.ch0.vpd.mat')
valoresPotenciaDatos3Pasadas.Pasada(1).Canle(1).Localizacion(2).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--00-06_max_throughput_50kmh_n_to_s.o.ch1.vpd.mat')
valoresPotenciaDatos3Pasadas.Pasada(1).Canle(2).Localizacion(2).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--00-07_max_throughput_50kmh_n_to_s.i.ch0.vpd.mat')
valoresPotenciaDatos3Pasadas.Pasada(1).Canle(1).Localizacion(1).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--00-07_max_throughput_50kmh_n_to_s.i.ch1.vpd.mat')
valoresPotenciaDatos3Pasadas.Pasada(1).Canle(2).Localizacion(1).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.i.ch0.vpd.mat')
valoresPotenciaDatos3Pasadas.Pasada(2).Canle(1).Localizacion(1).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.i.ch1.vpd.mat')
valoresPotenciaDatos3Pasadas.Pasada(2).Canle(2).Localizacion(1).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.o.ch0.vpd.mat')
valoresPotenciaDatos3Pasadas.Pasada(2).Canle(1).Localizacion(2).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.o.ch1.vpd.mat')
valoresPotenciaDatos3Pasadas.Pasada(2).Canle(2).Localizacion(2).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.i.ch0.vpd.mat')
valoresPotenciaDatos3Pasadas.Pasada(3).Canle(1).Localizacion(1).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.i.ch1.vpd.mat')
valoresPotenciaDatos3Pasadas.Pasada(3).Canle(2).Localizacion(1).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.o.ch0.vpd.mat')
valoresPotenciaDatos3Pasadas.Pasada(3).Canle(1).Localizacion(2).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.o.ch1.vpd.mat')
valoresPotenciaDatos3Pasadas.Pasada(3).Canle(2).Localizacion(2).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
save valoresPotenciaDatos3Pasadas valoresPotenciaDatos3Pasadas

return

%% Análise de tódolos datos (pasadas de alta velocidade)

clear
cd('/media/tecrail/measData/r/')

% for ki=1:3
%     for kj=1:2
%         for kk=1:2
%             valoresPotenciaDatos3Pasadas.Pasada(ki).Canle(kj).Localizacion(kk)=0;
%         end
%     end
% end

load('2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.o.ch0.vpd.mat')
valoresPotenciaDatosAltaVelocidade.Velocidade(1).Canle(1).Localizacion(1).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.o.ch1.vpd.mat')
valoresPotenciaDatosAltaVelocidade.Velocidade(1).Canle(2).Localizacion(1).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-10--03-55_indoor_etcs_voip_100kmh_n_to_s.i.ch0.vpd.mat')
valoresPotenciaDatosAltaVelocidade.Velocidade(1).Canle(1).Localizacion(2).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-10--03-55_indoor_etcs_voip_100kmh_n_to_s.i.ch1.vpd.mat')
valoresPotenciaDatosAltaVelocidade.Velocidade(1).Canle(2).Localizacion(2).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.o.ch0.vpd.mat')
valoresPotenciaDatosAltaVelocidade.Velocidade(2).Canle(1).Localizacion(1).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.o.ch1.vpd.mat')
valoresPotenciaDatosAltaVelocidade.Velocidade(2).Canle(2).Localizacion(1).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-10--04-24_indoor_etcs_voip_200kmh_n_to_s.i.ch0.vpd.mat')
valoresPotenciaDatosAltaVelocidade.Velocidade(2).Canle(1).Localizacion(2).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-10--04-24_indoor_etcs_voip_200kmh_n_to_s.i.ch1.vpd.mat')
valoresPotenciaDatosAltaVelocidade.Velocidade(2).Canle(2).Localizacion(2).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
save valoresPotenciaDatosAltaVelocidade valoresPotenciaDatosAltaVelocidade

return

%% Representación xeral (tres pasadas)

clear
cd('/media/tecrail/measData/r/')
load('valoresPotenciaDatos3Pasadas.mat')

lonxitudeFiestra = 10000;
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;

% eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(valoresPotenciaDatos)/((15.36e6)/1024));
% figure,plot(eixoTemporal,10+log10(valoresPotenciaDatos))

valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(1).Canle(1).Localizacion(1).valores;
Df=conv(valoresPotenciaDatos,F);
valoresPotenciaDatos3Pasadas.Pasada(1).Canle(1).Localizacion(1).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);

valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(1).Canle(2).Localizacion(1).valores;
Df=conv(valoresPotenciaDatos,F);
valoresPotenciaDatos3Pasadas.Pasada(1).Canle(2).Localizacion(1).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);

valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(1).Canle(1).Localizacion(2).valores;
Df=conv(valoresPotenciaDatos,F);
valoresPotenciaDatos3Pasadas.Pasada(1).Canle(1).Localizacion(2).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);

valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(1).Canle(2).Localizacion(2).valores;
Df=conv(valoresPotenciaDatos,F);
valoresPotenciaDatos3Pasadas.Pasada(1).Canle(2).Localizacion(2).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);

valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(2).Canle(1).Localizacion(1).valores;
valoresPotenciaDatos=valoresPotenciaDatos(end:(-1):1);
Df=conv(valoresPotenciaDatos,F);
valoresPotenciaDatos3Pasadas.Pasada(2).Canle(1).Localizacion(1).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);

valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(2).Canle(2).Localizacion(1).valores;
valoresPotenciaDatos=valoresPotenciaDatos(end:(-1):1);
Df=conv(valoresPotenciaDatos,F);
valoresPotenciaDatos3Pasadas.Pasada(2).Canle(2).Localizacion(1).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);

valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(2).Canle(1).Localizacion(2).valores;
valoresPotenciaDatos=valoresPotenciaDatos(end:(-1):1);
Df=conv(valoresPotenciaDatos,F);
valoresPotenciaDatos3Pasadas.Pasada(2).Canle(1).Localizacion(2).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);

valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(2).Canle(2).Localizacion(2).valores;
valoresPotenciaDatos=valoresPotenciaDatos(end:(-1):1);
Df=conv(valoresPotenciaDatos,F);
valoresPotenciaDatos3Pasadas.Pasada(2).Canle(2).Localizacion(2).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);

valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(3).Canle(1).Localizacion(1).valores;
Df=conv(valoresPotenciaDatos,F);
valoresPotenciaDatos3Pasadas.Pasada(3).Canle(1).Localizacion(1).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);

valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(3).Canle(2).Localizacion(1).valores;
Df=conv(valoresPotenciaDatos,F);
valoresPotenciaDatos3Pasadas.Pasada(3).Canle(2).Localizacion(1).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);

valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(3).Canle(1).Localizacion(2).valores;
Df=conv(valoresPotenciaDatos,F);
valoresPotenciaDatos3Pasadas.Pasada(3).Canle(1).Localizacion(2).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);

valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(3).Canle(2).Localizacion(2).valores;
Df=conv(valoresPotenciaDatos,F);
valoresPotenciaDatos3Pasadas.Pasada(3).Canle(2).Localizacion(2).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);

save valoresPotenciaDatos3Pasadas valoresPotenciaDatos3Pasadas

clear
cd('/media/tecrail/measData/r/')
load('valoresPotenciaDatos3Pasadas.mat')
load('valoresPotenciaRuido.mat')

estilos.canle(1).localizacion(2).estilo='r';
estilos.canle(2).localizacion(2).estilo='--';
estilos.canle(1).localizacion(1).estilo='g';
estilos.canle(2).localizacion(1).estilo='k--';

h1=figure;
for kp=1:3
    for kc=1:2
        for kl=1:2
            eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+...
                length(valoresPotenciaDatos3Pasadas.Pasada(kp).Canle(kc).Localizacion(kl).valoresMediaMobil)/((15.36e6)/1024));
            subplot(3,1,kp)
            kl2=2;
            if(kl==2)
                kl2=1;
            end
            valores=10*log10(valoresPotenciaDatos3Pasadas.Pasada(kp).Canle(kc).Localizacion(kl).valoresMediaMobil)-10*log10(valoresPotenciaRuido.ValorMedioRuidoPorLocalizacion(kl2));
            hold on
            plot(eixoTemporal,valores,estilos.canle(kc).localizacion(kl).estilo,'linewidth',1)
            hold off
            xlabel('time [s]'),ylabel('SNR [dB]')
        end
    end
end
subplot(3,1,1)
legend('ant. #1 (o)','ant. #2 (o)','ant. #1 (i)','ant. #2 (i)')
EIXO=axis+[0 100 0 0];
EIXO(4)=65;
EIXO(3)=-1;
title('both sectors active')
axis(EIXO);
grid on
subplot(3,1,2)
title('first sector disabled and second sector active')
axis(EIXO);
grid on
subplot(3,1,3)
title('first sector active and second sector disabled')
axis(EIXO);
grid on
set(findall(h1,'-property','FontSize'),'FontSize',12);%set(findall(h1,'-property','FontName'),'FontName','Times New Roman')
%set(h1,'PaperPositionMode','auto')
print(h1,'-dpdf','-cmyk','SNR3Pasadas.pdf')
return

%% Representación xeral (tres pasadas, VERSI�N PAPER RADIOENGINEERING)

clear
cd('Y:\measurements\2014-12-10_11--AdifAntequera\Entregable-E5-3--ProcesamentoMedidas\r')
% load('valoresPotenciaDatos3Pasadas.mat')
% 
% lonxitudeFiestra = 10000;
% mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
% F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;
% 
% % eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+length(valoresPotenciaDatos)/((15.36e6)/1024));
% % figure,plot(eixoTemporal,10+log10(valoresPotenciaDatos))
% 
% valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(1).Canle(1).Localizacion(1).valores;
% Df=conv(valoresPotenciaDatos,F);
% valoresPotenciaDatos3Pasadas.Pasada(1).Canle(1).Localizacion(1).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
% 
% valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(1).Canle(2).Localizacion(1).valores;
% Df=conv(valoresPotenciaDatos,F);
% valoresPotenciaDatos3Pasadas.Pasada(1).Canle(2).Localizacion(1).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
% 
% valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(1).Canle(1).Localizacion(2).valores;
% Df=conv(valoresPotenciaDatos,F);
% valoresPotenciaDatos3Pasadas.Pasada(1).Canle(1).Localizacion(2).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
% 
% valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(1).Canle(2).Localizacion(2).valores;
% Df=conv(valoresPotenciaDatos,F);
% valoresPotenciaDatos3Pasadas.Pasada(1).Canle(2).Localizacion(2).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
% 
% valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(2).Canle(1).Localizacion(1).valores;
% valoresPotenciaDatos=valoresPotenciaDatos(end:(-1):1);
% Df=conv(valoresPotenciaDatos,F);
% valoresPotenciaDatos3Pasadas.Pasada(2).Canle(1).Localizacion(1).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
% 
% valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(2).Canle(2).Localizacion(1).valores;
% valoresPotenciaDatos=valoresPotenciaDatos(end:(-1):1);
% Df=conv(valoresPotenciaDatos,F);
% valoresPotenciaDatos3Pasadas.Pasada(2).Canle(2).Localizacion(1).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
% 
% valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(2).Canle(1).Localizacion(2).valores;
% valoresPotenciaDatos=valoresPotenciaDatos(end:(-1):1);
% Df=conv(valoresPotenciaDatos,F);
% valoresPotenciaDatos3Pasadas.Pasada(2).Canle(1).Localizacion(2).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
% 
% valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(2).Canle(2).Localizacion(2).valores;
% valoresPotenciaDatos=valoresPotenciaDatos(end:(-1):1);
% Df=conv(valoresPotenciaDatos,F);
% valoresPotenciaDatos3Pasadas.Pasada(2).Canle(2).Localizacion(2).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
% 
% valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(3).Canle(1).Localizacion(1).valores;
% Df=conv(valoresPotenciaDatos,F);
% valoresPotenciaDatos3Pasadas.Pasada(3).Canle(1).Localizacion(1).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
% 
% valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(3).Canle(2).Localizacion(1).valores;
% Df=conv(valoresPotenciaDatos,F);
% valoresPotenciaDatos3Pasadas.Pasada(3).Canle(2).Localizacion(1).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
% 
% valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(3).Canle(1).Localizacion(2).valores;
% Df=conv(valoresPotenciaDatos,F);
% valoresPotenciaDatos3Pasadas.Pasada(3).Canle(1).Localizacion(2).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
% 
% valoresPotenciaDatos=valoresPotenciaDatos3Pasadas.Pasada(3).Canle(2).Localizacion(2).valores;
% Df=conv(valoresPotenciaDatos,F);
% valoresPotenciaDatos3Pasadas.Pasada(3).Canle(2).Localizacion(2).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
% 
% save valoresPotenciaDatos3Pasadas valoresPotenciaDatos3Pasadas

clear
close all
format long g
clc
% cd('/media/tecrail/measData/r/')
load('valoresPotenciaDatos3Pasadas.mat')
load('valoresPotenciaRuido.mat')

estilos.canle(1).localizacion(2).estilo='r';
estilos.canle(2).localizacion(2).estilo='--';
estilos.canle(1).localizacion(1).estilo='g';
estilos.canle(2).localizacion(1).estilo='k--';

h1=figure;
for kp=1:3
    for kl=2:-1:1
        for kc=1:2
            eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+...
                length(valoresPotenciaDatos3Pasadas.Pasada(kp).Canle(kc).Localizacion(kl).valoresMediaMobil)/((15.36e6)/1024));
            subplot(3,1,kp)
            kl2=2;
            if(kl==2)
                kl2=1;
            end
            valores=10*log10(valoresPotenciaDatos3Pasadas.Pasada(kp).Canle(kc).Localizacion(kl).valoresMediaMobil)-10*log10(valoresPotenciaRuido.ValorMedioRuidoPorLocalizacion(kl2));
            hold on
            plot(eixoTemporal,valores,estilos.canle(kc).localizacion(kl).estilo,'linewidth',1)
            hold off
            xlabel('time [s]'),ylabel('SNR [dB]')
        end
    end
end
subplot(3,1,1)
legend('ant. #1 (outd.)','ant. #2 (outd.)','ant. #1 (ind.)','ant. #2 (ind.)')
EIXO=axis+[0 140 0 0];
EIXO(4)=65;
EIXO(3)=-1;
title('both sectors on')
axis(EIXO);
grid on
subplot(3,1,2)
title('first sector off and second sector on')
axis(EIXO);
grid on
subplot(3,1,3)
title('first sector on and second sector off')
axis(EIXO);
grid on
set(findall(h1,'-property','FontSize'),'FontSize',12);%set(findall(h1,'-property','FontName'),'FontName','Times New Roman')
%set(h1,'PaperPositionMode','auto')
print(h1,'-dpdf','-cmyk','SNR3Pasadas_Radioengineering2015.pdf')


h1=figure;
kp=1;
kc=1;
kl=2;
eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+...
    length(valoresPotenciaDatos3Pasadas.Pasada(kp).Canle(kc).Localizacion(kl).valoresMediaMobil)/((15.36e6)/1024));
% subplot(3,1,kp)
kl=1;
kl2=2;
if(kl==2)
    kl2=1;
end
valores1=10*log10(valoresPotenciaDatos3Pasadas.Pasada(kp).Canle(kc).Localizacion(kl).valoresMediaMobil)-10*log10(valoresPotenciaRuido.ValorMedioRuidoPorLocalizacion(kl2));
kl=2;
kl2=2;
if(kl==2)
    kl2=1;
end
valores2=10*log10(valoresPotenciaDatos3Pasadas.Pasada(kp).Canle(kc).Localizacion(kl).valoresMediaMobil)-10*log10(valoresPotenciaRuido.ValorMedioRuidoPorLocalizacion(kl2));
valores1((length(valores2)+1):end)=[];
diferencia = valores2-valores1;


diferencia = (10.^(valores2/10))./(10.^(valores1/10));




lonxitudeFiestra = 100000;
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;
Df=conv(diferencia,F);
diferenciaMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);







hold on
plot(eixoTemporal,10*log10(diferencia),estilos.canle(kc).localizacion(kl).estilo,'linewidth',1)
plot(eixoTemporal(1:length(diferenciaMediaMobil)),10*log10(diferenciaMediaMobil),estilos.canle(kc).localizacion(kl-1).estilo,'linewidth',3)
hold off
title('both sectors on')
grid on
legend('instantaneus gain','averaged gain')
xlabel('time [s]'),ylabel('SNR gain (SNR outd. / SNR ind.) [dB]')
xlim([eixoTemporal(1) eixoTemporal(end)])
ylim([5 45])
set(findall(h1,'-property','FontSize'),'FontSize',12);%set(findall(h1,'-property','FontName'),'FontName','Times New Roman')
print(h1,'-dpdf','-cmyk','DiffSNR3Pasadas_Radioengineering2015.pdf')


return

%% Representación xeral (pasadas alta velocidade)

clear
cd('/media/tecrail/measData/r/')
load('valoresPotenciaDatosAltaVelocidade.mat')

lonxitudeFiestra = 10000;
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;

for kv=1:2
    for kc=1:2
        for kl=1:2
            valoresPotenciaDatos=valoresPotenciaDatosAltaVelocidade.Velocidade(kv).Canle(kc).Localizacion(kl).valores;
            Df=conv(valoresPotenciaDatos,F);
            valoresPotenciaDatosAltaVelocidade.Velocidade(kv).Canle(kc).Localizacion(kl).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
        end
    end
end

save valoresPotenciaDatosAltaVelocidade valoresPotenciaDatosAltaVelocidade

clear
cd('/media/tecrail/measData/r/')
load('valoresPotenciaDatosAltaVelocidade.mat')
load('valoresPotenciaRuido.mat')

estilos.canle(1).localizacion(1).estilo='r';
estilos.canle(2).localizacion(1).estilo='--';
estilos.canle(1).localizacion(2).estilo='g';
estilos.canle(2).localizacion(2).estilo='k--';

h2=figure;
for kv=1:2
    for kc=1:2
        for kl=1:2
            eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+...
                length(valoresPotenciaDatosAltaVelocidade.Velocidade(kv).Canle(kc).Localizacion(kl).valoresMediaMobil)/((15.36e6)/1024));
            subplot(2,1,kv)
            valores=10*log10(valoresPotenciaDatosAltaVelocidade.Velocidade(kv).Canle(kc).Localizacion(kl).valoresMediaMobil)-10*log10(valoresPotenciaRuido.ValorMedioRuidoPorLocalizacion(kl));
            hold on
            plot(eixoTemporal,valores,estilos.canle(kc).localizacion(kl).estilo,'linewidth',1)
            hold off
            xlabel('time [s]'),ylabel('SNR [dB]')
        end
    end
end
subplot(2,1,1)
EIXO=axis;
EIXO(3)=-1;
EIXO(4)=60;
title('speed equal to 100 km/h')
axis(EIXO);
grid on
subplot(2,1,2)
title('speed equal to 200 km/h')
legend('ant. #1 (o)','ant. #2 (o)','ant. #1 (i)','ant. #2 (i)')
axis(EIXO);
grid on
set(findall(h2,'-property','FontSize'),'FontSize',12);%set(findall(h1,'-property','FontName'),'FontName','Times New Roman')
%set(h1,'PaperPositionMode','auto')
print(h2,'-dpdf','-cmyk','PasadasAltaVelocidade.pdf')

return

%% Pinto unha realización da canle

% CASO 1: Primeira pasada, sobre t=150s, canle 1, fóra (cellid=3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=150;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-11--00-06_max_throughput_50kmh_n_to_s.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end


% CASO 2: Primeira pasada, sobre t=200s, canle 1, fóra (cellid=3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=200;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-11--00-06_max_throughput_50kmh_n_to_s.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end


% CASO 3: Primeira pasada, sobre t=150s, canle 1, dentro (idcell=3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=150;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-11--00-07_max_throughput_50kmh_n_to_s.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 4: Primeira pasada, sobre t=230s, canle 1, dentro (cellid=3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=230;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-11--00-07_max_throughput_50kmh_n_to_s.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 5: Primeira pasada, sobre t=350s, canle 1, dentro (cellid=4)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=350;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-11--00-07_max_throughput_50kmh_n_to_s.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 6: Primeira pasada, sobre t=400s, canle 1, dentro (idcell=4)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=400;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-11--00-07_max_throughput_50kmh_n_to_s.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 7: Segunda pasada, sobre t=300s, canle 1, fóra (cellid=4)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=300;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 8: Segunda pasada, sobre t=410s, canle 1, fóra (cellid=4)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=410;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 9: Segunda pasada, sobre t=300s, canle 1, dentro (cellid=4)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=300;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 10: Segunda pasada, sobre t=370s, canle 1, dentro (cellid = 4)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=370;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 11: Terceira pasada, sobre t=230s, canle 1, fóra (cellid = 3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=230;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 12: Terceira pasada, sobre t=330s, canle 1, fóra (cellid = 3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=330;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 13: Terceira pasada, sobre t=170s, canle 1, dentro (DESCARTADO)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=250;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.ch1.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 14: Pasada a 100 km/h, sobre t=100s, canle 1, fóra (cellid = 3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=100;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.o.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 15: Pasada a 100 km/h, sobre t=155s, canle 1, fóra (cellid = 4)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=155;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.o.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 16: Pasada a 100 km/h, sobre t=115s, canle 1, dentro (cellid = 3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=115;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-10--03-55_indoor_etcs_voip_100kmh_n_to_s.i.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 17: Pasada a 100 km/h, sobre t=160s, canle 1, dentro (cellid = 4)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=160;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.o.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 18: Pasada a 200 km/h, sobre t=85s, canle 1, fóra (cellid = 3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=85;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.o.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 19: Pasada a 200 km/h, sobre t=112.5s, canle 1, fóra (cellid = 4)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=112.5;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.o.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 20: Pasada a 200 km/h, sobre t=84s, canle 1, dentro (cellid = 3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=84;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-10--04-24_indoor_etcs_voip_200kmh_n_to_s.i.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% CASO 21: Pasada a 200 km/h, sobre t=112.5s, canle 1, dentro (cellid = 4)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=112.5;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-10--04-24_indoor_etcs_voip_200kmh_n_to_s.i.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

%% Análise de medidas estáticas

% Caso 1, Canle 0
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--03-19_static_antequera_.o.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--03-19_static_antequera_.o.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=857800704;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=floor(MostrasComplexasFicheiro/1024);
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--03-19_static_antequera_.o.ch0.vpd.mat'),'valoresPotenciaDatos')

% Caso 1, Canle 1
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--03-19_static_antequera_.o.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--03-19_static_antequera_.o.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=857800704;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=floor(MostrasComplexasFicheiro/1024);
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--03-19_static_antequera_.o.ch1.vpd.mat'),'valoresPotenciaDatos')

% Caso 2, Canle 0
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--03-21_static_antequera_2_.o.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--03-21_static_antequera_2_.o.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=2129854464;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=floor(MostrasComplexasFicheiro/1024);
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--03-21_static_antequera_2_.o.ch0.vpd.mat'),'valoresPotenciaDatos')

% Caso 2, Canle 1
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--03-21_static_antequera_2_.o.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--03-21_static_antequera_2_.o.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=2129854464;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=floor(MostrasComplexasFicheiro/1024);
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--03-21_static_antequera_2_.o.ch1.vpd.mat'),'valoresPotenciaDatos')

% Estima canle caso estático (Caso 1, Canle 0) [cellid = 3]

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=2;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-11--03-19_static_antequera_.o.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

%% Análise de tódolos datos (medidas en estático)

clear
cd('/media/tecrail/measData/r/')

% for ki=1:3
%     for kj=1:2
%         for kk=1:2
%             valoresPotenciaDatos3Pasadas.Pasada(ki).Canle(kj).Localizacion(kk)=0;
%         end
%     end
% end

load('2014-12-11--03-19_static_antequera_.o.ch0.vpd.mat')
valoresPotenciaDatosEnEstatico.Canle(1).Localizacion(1).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--03-19_static_antequera_.o.ch1.vpd.mat')
valoresPotenciaDatosEnEstatico.Canle(2).Localizacion(1).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--03-21_static_antequera_2_.o.ch0.vpd.mat')
valoresPotenciaDatosEnEstatico.Canle(1).Localizacion(2).valores=valoresPotenciaDatos;
load('2014-12-11--03-21_static_antequera_2_.o.ch1.vpd.mat')
valoresPotenciaDatosEnEstatico.Canle(2).Localizacion(2).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
save valoresPotenciaDatosEnEstatico valoresPotenciaDatosEnEstatico

return

%% Representación xeral (medidas en estático)

clear
cd('/media/tecrail/measData/r/')
load('valoresPotenciaDatosEnEstatico.mat')

lonxitudeFiestra = 10000;
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;

for kc=1:2
    for kl=1:2
        valoresPotenciaDatos=valoresPotenciaDatosEnEstatico.Canle(kc).Localizacion(kl).valores;
        Df=conv(valoresPotenciaDatos,F);
        valoresPotenciaDatosEnEstatico.Canle(kc).Localizacion(kl).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
    end
end

save valoresPotenciaDatosEnEstatico valoresPotenciaDatosEnEstatico

clear
cd('/media/tecrail/measData/r/')
load('valoresPotenciaDatosEnEstatico.mat')
load('valoresPotenciaRuido.mat')

estilos.canle(1).localizacion(1).estilo='r';
estilos.canle(2).localizacion(1).estilo='--';
estilos.canle(1).localizacion(2).estilo='g';
estilos.canle(2).localizacion(2).estilo='k--';

h3=figure;
kl=1;
for kc=1:2
    decimaPorcionValores=round(length(valoresPotenciaDatosEnEstatico.Canle(kc).Localizacion(kl).valoresMediaMobil)/10);
    valores=valoresPotenciaDatosEnEstatico.Canle(kc).Localizacion(kl).valoresMediaMobil(decimaPorcionValores:(end-decimaPorcionValores));
    eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+...
        length(valores)/((15.36e6)/1024));
    valores=10*log10(valores)-10*log10(valoresPotenciaRuido.ValorMedioRuidoPorLocalizacion(kl));
    hold on
    plot(eixoTemporal,valores,estilos.canle(kc).localizacion(kl).estilo,'linewidth',1)
    hold off
    xlabel('time [s]'),ylabel('SNR [dB]')
end
EIXO=axis;
axis(EIXO);
legend('ant. #1 (o)','ant. #2 (o)','location','southeast')
grid on
set(findall(h3,'-property','FontSize'),'FontSize',12);%set(findall(h1,'-property','FontName'),'FontName','Times New Roman')
%set(h1,'PaperPositionMode','auto')
print(h3,'-dpdf','-cmyk','ResultadosEnEstatico.pdf')

return
%% Análise do ruido

% Ruido Externo, Canle 0
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--03-23_noise_antequera_.o.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--03-23_noise_antequera_.o.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=6812565156;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=floor(MostrasComplexasFicheiro/1024);
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--03-23_noise_antequera_.o.ch0.vpd.mat'),'valoresPotenciaDatos')

% Ruido Externo, Canle 1
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--03-23_noise_antequera_.o.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--03-23_noise_antequera_.o.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=6812565156;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=floor(MostrasComplexasFicheiro/1024);
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--03-23_noise_antequera_.o.ch1.vpd.mat'),'valoresPotenciaDatos')

% Ruido interno, Canle 0
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--03-28_ruido.i.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--03-28_ruido.i.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=1763966976;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=floor(MostrasComplexasFicheiro/1024);
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{1}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--03-28_ruido.i.ch0.vpd.mat'),'valoresPotenciaDatos')

% Ruido interno, Canle 1
    
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData1');
rxSignalsInUSRPFormatFilenames = {...
    fullfile(basePathForDataFiles, '2014-12-11--03-28_ruido.i.ch0.usrp'), ...
    fullfile(basePathForDataFiles, '2014-12-11--03-28_ruido.i.ch1.usrp')};
tic
step = 1024;
success=1;
TamanhoFicheiroBytes=1763966976;
MostrasComplexasFicheiro=TamanhoFicheiroBytes/4;
BloquesFicheiro=floor(MostrasComplexasFicheiro/1024);
iter=0;
MaximoIter=BloquesFicheiro-1;
valoresPotenciaDatos=zeros(1,MaximoIter+1);
iterc=iter+1;
while iter<(MaximoIter+1)%success>0
    % After USRP acqquisition
    [rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilenames{2}, step, iter*step);
    if (success == (-1))
        fprintf('FAIL\n');
        error('Cannot read the number of requested samples ');
    elseif (success == 0)
        fprintf('FAIL\n');
        error(' Cannot read file acquired by the USRP ');
    else
%         fprintf('OK\n');
    end
    
    rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);
%     rxSamplesOrig = rxSamples;
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
%     TECRAIL_spectrum(rxSamples, signalSamplingRate);
    iter=iter+1;
    valoresPotenciaDatos(iter)=potenciaDatos;
    if(iter==iterc)
        disp(['[' sprintf('%3.5f',100*iter/MaximoIter) '%]Procesando medidas...']);
        toc
        iterc=iterc+10000;
    end
end

save(fullfile(basePathForDataFiles, '2014-12-11--03-28_ruido.i.ch1.vpd.mat'),'valoresPotenciaDatos')

%% Análise de tódalas mostras de ruido

clear
cd('/media/tecrail/measData/r/')

% for ki=1:3
%     for kj=1:2
%         for kk=1:2
%             valoresPotenciaDatos3Pasadas.Pasada(ki).Canle(kj).Localizacion(kk)=0;
%         end
%     end
% end



load('2014-12-11--03-23_noise_antequera_.o.ch0.vpd.mat')
valoresPotenciaRuido.Canle(1).Localizacion(1).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--03-23_noise_antequera_.o.ch1.vpd.mat')
valoresPotenciaRuido.Canle(2).Localizacion(1).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--03-28_ruido.i.ch0.vpd.mat')
valoresPotenciaRuido.Canle(1).Localizacion(2).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
load('2014-12-11--03-28_ruido.i.ch1.vpd.mat')
valoresPotenciaRuido.Canle(2).Localizacion(2).valores=valoresPotenciaDatos;
clear valoresPotenciaDatos
save valoresPotenciaRuido valoresPotenciaRuido

return

%% Representación xeral (ruido)

clear
cd('/media/tecrail/measData/r/')
load('valoresPotenciaRuido.mat')

lonxitudeFiestra = 10000;
mediaLonxitudeFiestra = floor(lonxitudeFiestra/2);
F = ones(1,lonxitudeFiestra)/lonxitudeFiestra;

for kc=1:2
    for kl=1:2
        valoresPotenciaDatos=valoresPotenciaRuido.Canle(kc).Localizacion(kl).valores;
        Df=conv(valoresPotenciaDatos,F);
        valoresPotenciaRuido.Canle(kc).Localizacion(kl).valoresMediaMobil=Df(mediaLonxitudeFiestra+1:end-mediaLonxitudeFiestra);
    end
end

save valoresPotenciaRuido valoresPotenciaRuido

clear
cd('/media/tecrail/measData/r/')
load('valoresPotenciaRuido.mat')

estilos.canle(1).localizacion(1).estilo='r';
estilos.canle(2).localizacion(1).estilo=':';
estilos.canle(1).localizacion(2).estilo='g';
estilos.canle(2).localizacion(2).estilo='k:';

figure
for kc=1:2
    for kl=1:2
        eixoTemporal = 0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+...
            length(valoresPotenciaRuido.Canle(kc).Localizacion(kl).valoresMediaMobil)/((15.36e6)/1024));
        hold on
        plot(eixoTemporal,10*log10(valoresPotenciaRuido.Canle(kc).Localizacion(kl).valoresMediaMobil),estilos.canle(kc).localizacion(kl).estilo,'linewidth',2)
        hold off
    end
end

return

%% Obtención dos valores medios da potencia de ruido

% Caso de antenas externas

clear
cd('/media/tecrail/measData/r/')
load('valoresPotenciaRuido.mat')

kl=1;
kc=1;

LonxitudeTotal=length(valoresPotenciaRuido.Canle(kc).Localizacion(kl).valoresMediaMobil);
DecimaParte=round(LonxitudeTotal/10);
MostrasRuidoConsideradas = valoresPotenciaRuido.Canle(kc).Localizacion(kl).valoresMediaMobil(DecimaParte:(end-DecimaParte));
ValorMedio=mean(MostrasRuidoConsideradas);
valoresPotenciaRuido.Canle(kc).Localizacion(kl).valorMedio=ValorMedio;

kl=1;
kc=2;

LonxitudeTotal=length(valoresPotenciaRuido.Canle(kc).Localizacion(kl).valoresMediaMobil);
DecimaParte=round(LonxitudeTotal/10);
MostrasRuidoConsideradas = valoresPotenciaRuido.Canle(kc).Localizacion(kl).valoresMediaMobil(DecimaParte:(end-DecimaParte));
ValorMedio=mean(MostrasRuidoConsideradas);
valoresPotenciaRuido.Canle(kc).Localizacion(kl).valorMedio=ValorMedio;

kl=2;
kc=1;

LonxitudeTotal=length(valoresPotenciaRuido.Canle(kc).Localizacion(kl).valoresMediaMobil);
DecimaParte=round(LonxitudeTotal/10);
MostrasRuidoConsideradas = valoresPotenciaRuido.Canle(kc).Localizacion(kl).valoresMediaMobil(DecimaParte:(end-DecimaParte));
ValorMedio=mean(MostrasRuidoConsideradas);
valoresPotenciaRuido.Canle(kc).Localizacion(kl).valorMedio=ValorMedio;
valoresPotenciaRuido.ValorMedioRuidoPorLocalizaciovaloresPotenciaRuido.ValorMedioRuidoPorLocalizacionn
kl=2;
kc=2;

LonxitudeTotal=length(valoresPotenciaRuido.CanvaloresPotenciaRuido.ValorMedioRuidoPorLocalizacionle(kc).Localizacion(kl).valoresMediaMobil);
DecimaParte=round(LonxitudeTotal/10);
MostrasRuidoConsideradas = valoresPotenciaRuido.Canle(kc).Localizacion(kl).valoresMediaMobil(DecimaParte:(end-DecimaParte));
ValorMedio=mean(MostrasRuidoConsideradas);
valoresPotenciaRuido.Canle(kc).Localizacion(kl).valorMedio=ValorMedio;

valoresPotenciaRuido.ValorMedioRuidoPorLocalizacion(1)=mean([valoresPotenciaRuido.Canle(1).Localizacion(1).valorMedio valoresPotenciaRuido.Canle(2).Localizacion(1).valorMedio])
valoresPotenciaRuido.ValorMedioRuidoPorLocalizacion(2)=mean([valoresPotenciaRuido.Canle(1).Localizacion(2).valorMedio valoresPotenciaRuido.Canle(2).Localizacion(2).valorMedio])

save valoresPotenciaRuido valoresPotenciaRuido