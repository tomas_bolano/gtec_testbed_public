clear
close all

fprintf('\t ==================================================\n');
fprintf('\t\t       Metro Measurements 18-sept-2014     \n');
fprintf('\t ==================================================\n');

% addpath('Z:\localhome\src\testbed\src\matlab_utils');
addpath('/home/tecrail/src/testbed/src/matlab_utils/');

%% LTE Downlink Channel Estimation and Equalization
% This example shows how to use the LTE System Toolbox(TM) to create a
% frame worth of data, pass it through a fading channel and perform channel
% estimation and equalization. Two figures are created illustrating the
% received and equalized frame.
    
% Copyright 2009-2013 The MathWorks, Inc.

%% Introduction
% This example shows how a simple transmitter-channel-receiver simulation
% may be created using functions from the LTE System Toolbox. The example
% generates a frame worth of data on one antenna port. As no transport
% channel is created in this example the data is random bits, QPSK
% modulated and mapped to every symbol in a subframe. A cell specific
% reference signal and primary and secondary synchronization signals are
% created and mapped to the subframe. 10 subframes are individually
% generated to create a frame. The frame is OFDM modulated, passed through
% an Extended Vehicular A Model (EVA5) fading channel, additive white
% Gaussian noise added and demodulated. MMSE equalization using channel and
% noise estimation is applied and finally the received and equalized
% resource grids are plotted.

%% Cell-Wide Settings
% The cell-wide settings are specified in a structure |enb|. A number of
% the functions used in this example require a subset of the settings
% specified below. In this example only one transmit antenna is used.

enb.NDLRB = 50;                 % Number of resource blocks
enb.CellRefP = 1;               % One transmit antenna port
enb.NCellID = 3;               % Cell ID
enb.CyclicPrefix = 'Normal';    % Normal cyclic prefix
enb.DuplexMode = 'FDD';         % FDD
enb.LookForAllNCellIDs = false;

%% Channel Estimator Configuration

cec.FreqWindow = 3;               % Frequency averaging window in 
                                  % Resource Elements (REs)
cec.TimeWindow = 3;               % Time averaging window in REs
cec.InterpType = 'linear';         % Cubic interpolation
cec.PilotAverage = 'UserDefined'; % Pilot averaging method
cec.InterpWinSize = 3;            % Interpolate up to 3 subframes 
                                  % simultaneously
cec.InterpWindow = 'Causal';     % Interpolation windowing method

%% Synchronization
% The offset caused by the channel in the received time domain signal is
% obtained using <matlab:doc('lteDLFrameOffset') lteDLFrameOffset>. This
% function returns a value |offset| which indicates how many samples the
% waveform has been delayed. The offset is considered identical for
% waveforms received on all antennas. The received time domain waveform can
% then be manipulated to remove the delay using |offset|.
clear rxWaveform;
clear rxWaveformSync;
clear rxGrid;
clear estChannel;
xvFileName='/media/tecrail/measBackup/medidasProcesadas--E5-3/measEUCAP/LteReceivedFrameSync.mat';
load(xvFileName,'xvRXSamplest2', 'measurementTimeStamp');
fprintf(' Measurement data timestamp\n\t ''%s''\n\n', measurementTimeStamp);
rxWaveform = xvRXSamplest2.';
corrcfg.PSS = 'On';
corrcfg.SSS = 'On';
corrcfg.CellRS = 'On';
offset = 0;


cfgdl = struct;
cfgdl.NDLRB = enb.NDLRB;
cfgdl.CyclicPrefix = 'Normal';
cfgdl.DuplexMode = 'FDD';
foffset = lteFrequencyOffset(cfgdl, rxWaveform)
rxWaveform = lteFrequencyCorrect(cfgdl,rxWaveform,foffset);

if (enb.NDLRB == 100)
    samplingRate = 30.72e6;
elseif (enb.NDLRB == 50)
    samplingRate = 15.36e6;
end
enb.CellRefP = 1; 
if (enb.LookForAllNCellIDs)
    fprintf(' Looking for all possible Physical Cell IDs . . . ');
    [cellid, offset] = lteCellSearch(enb,rxWaveform)
else
    fprintf(' Looking for Physical Cell ID = %d . . . ', enb.NCellID);
    [cellid, offset] = lteCellSearch(enb,rxWaveform, enb.NCellID)
end
fprintf(' OK\n');
% % % 
% % % enb.NSubframe = 0; %% !!!
% % % [offset, corr] = lteDLFrameOffset(enb,rxWaveform);

% % chs = lteTestModel('1.1', '10MHz', 10);
% % [offset] = lteDLFrameOffset(chs, rxWaveform);
startIndex = 1 + offset;% - 3*15360;
endIndex = startIndex - 1 + samplingRate*10e-3;
rxWaveformSync = rxWaveform(startIndex:endIndex, :);
% rxWaveformSync = rxWaveformSync ./ sqrt(mean(abs(rxWaveformSync)));
xvRXSamplest2 = rxWaveformSync.';
save('/media/tecrail/measBackup/medidasProcesadas--E5-3/measEUCAP/LteReceivedFrameSync.mat', ...
    'xvRXSamplest2', 'measurementTimeStamp')
clear xvRXSamplest2

%% A partires de aqu� estudamos a canle

%%% OFDM Demodulation
% The time domain waveform undergoes OFDM demodulation to transform it to
% the frequency domain and recreate a resource grid. This is accomplished
% using <matlab:doc('lteOFDMDemodulate') lteOFDMDemodulate>. The resulting
% grid is a 3-dimensional matrix. The number of rows represents the number
% of subcarriers. The number of columns equals the number of OFDM symbols
% in a subframe. The number of subcarriers and symbols is the same for the
% returned grid from OFDM demodulation as the grid passed into
% <matlab:doc('lteOFDMModulate') lteOFDMModulate>. The number of planes
% (3rd dimension) in the grid corresponds to the number of receive
% antennas.

enb.Ng = 'One';
enb.CFI = 2;
enb.PHICHDuration = 'Normal';

rxGrid = lteOFDMDemodulate(enb, rxWaveformSync);
figure('Color','w');
helperPlotReceiveResourceGrid(enb,rxGrid);

%%% Channel Estimation
% To create an estimation of the channel over the duration of the
% transmitted resource grid <matlab:doc('lteDLChannelEstimate')
% lteDLChannelEstimate> is used. The channel estimation function is
% configured by the structure |cec|. <matlab:doc('lteDLChannelEstimate')
% lteDLChannelEstimate> assumes the first subframe within the resource grid
% is subframe number |enb.NSubframe| and therefore the subframe number must
% be set prior to calling the function. In this example the whole received
% frame will be estimated in one call and the first subframe within the
% frame is subframe number 0. The function returns a 4-D array of complex
% weights which the channel applies to each resource element in the
% transmitted grid for each possible transmit and receive antenna
% combination. The possible combinations are based upon the eNodeB
% configuration |enb| and the number of receive antennas (determined by the
% size of the received resource grid). The 1st dimension is the subcarrier,
% the 2nd dimension is the OFDM symbol, the 3rd dimension is the receive
% antenna and the 4th dimension is the transmit antenna. In this example
% one transmit and one receive antenna is used therefore the size of
% |estChannel| is 180-by-140-by-1-by-1.

enb.NSubframe = 0;

cec.PilotAverage = 'UserDefined';
cec.FreqWindow = 1;
cec.TimeWindow = 1;
cec.InterpType = 'v4';
cec.InterpWindow = 'Centered';
cec.InterpWinSize = 1;


[estChannel, noiseEst] = lteDLChannelEstimate(enb, cec, rxGrid);
figure('Color','w');
helperPlotChannelEstimate(estChannel);

return
% % %   
% % % figure;
% % % hold off;
% % % for ii=1:140,
% % %     plot(abs(estChannel(:,ii)))
% % %     pause(0.3);
% % % end
% MMSE Equalization
% The effects of the channel on the received resource grid are equalized
% using <matlab:doc('lteEqualizeMMSE') lteEqualizeMMSE>. This function uses
% the estimate of the channel |estChannel| and noise |noiseEst| to equalize
% the received resource grid |rxGrid|. The function returns |eqGrid| which
% is the equalized grid. The dimensions of the equalized grid are the same
% as the original transmitted grid (|txGrid|) before OFDM modulation.

eqGrid = lteEqualizeMMSE(rxGrid, estChannel, noiseEst);
figure('Color','w');
helperPlotEqualizedResourceGrid(enb,eqGrid);
% symbols = eqGrid(:,(1:5)+7*10); 
symbols = eqGrid(:,1:5); 
scatterplot(symbols(:));


%% Analysis
% The received resource grid is compared with the equalized resource grid.
% The error between the transmitted and equalized grid and transmitted and
% received grids are calculated. This creates two matrices (the same size
% as the resource arrays) which contain the error for each symbol. To allow
% easy inspection the received and equalized grids are plotted on a
% logarithmic scale using <matlab:doc('surf') surf> within
% <matlab:edit('hDownlinkEstimationEqualizationResults.m')
% hDownlinkEstimationEqualizationResults.m>. These diagrams show that
% performing channel equalization drastically reduces the error in the
% received resource grid.

% Calculate error between transmitted and equalized grid
eqError = txGrid - eqGrid;
rxError = txGrid - rxGrid;

% Compute EVM across all input values
% EVM of pre-equalized receive signal
preEqualisedEVM = lteEVM(txGrid,rxGrid);
fprintf('Percentage RMS EVM of Pre-Equalized signal: %0.3f%%\n', ...
        preEqualisedEVM.RMS*100); 
% EVM of post-equalized receive signal
postEqualisedEVM = lteEVM(txGrid,eqGrid);
fprintf('Percentage RMS EVM of Post-Equalized signal: %0.3f%%\n', ...
        postEqualisedEVM.RMS*100); 

% Plot the received and equalized resource grids 
hDownlinkEstimationEqualizationResults(rxGrid, eqGrid);

%% Appendix
% This example uses the helper function:
%
% * <matlab:edit('hDownlinkEstimationEqualizationResults.m')
% hDownlinkEstimationEqualizationResults.m>

displayEndOfDemoMessage(mfilename)