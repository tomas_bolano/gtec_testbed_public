clear
close all
clc
format long g

L = 20;
load('/home/tecrail/EUCAP/r/pdp.mat') 
pdp = pdp(1:L);
load('/home/tecrail/EUCAP/r/estChannel.mat') 
%%
Nsubcarriers = 1024;
enb.CyclicPrefix = 'Normal';
enb.NDLRB        = 50;
enb.CellRefP     = 1;
enb.NCellID      = 3;
enb.NSubframe    = 0;
antPort          = 0;
enb.DuplexMode = 'FDD';         % FDD

resourceGrid = lteDLResourceGrid(enb);
ind = lteCellRSIndices(enb,antPort);
rs = lteCellRS(enb,antPort);
resourceGrid (ind) = 1;
Npilots = length(ind(:));
%% Covarianza frecuencia
[kk, ll] = meshgrid(0:Nsubcarriers-1, 0:Nsubcarriers-1);
F = 1/sqrt(Nsubcarriers)*fftshift(exp(-1j*2*pi*kk.*ll/Nsubcarriers), 1);
Fl = F(213:end-212, 1:L);

varianceChannel = var(estChannel(:));
Cf = Fl*diag(pdp)*Fl';
Cf = varianceChannel*Cf/Cf(1,1);

%% Covarianza tiempo

vN=[50 100 200];

SNRefectiva = 0:5:60;

capacity = zeros(length(vN),length(SNRefectiva));
capacityUpper = zeros(length(vN),length(SNRefectiva));

for jj=1:length(vN)

    v = vN(jj);
    fc = 2.4e9;
    vSI = v*1000/3600;
    fd = vSI*fc/3e8;
    timeInstants = [0;1024+80;1024+80+(1024+72)*1;1024+80+(1024+72)*2;...
        1024+80+(1024+72)*3;1024+80+(1024+72)*4;1024+80+(1024+72)*5;...
        1024+80+(1024+72)*6;1024+80+(1024+72)*6 + 1024+80;...
        1024+80+(1024+72)*7 + 1024+80;1024+80+(1024+72)*8 + 1024+80;...
        1024+80+(1024+72)*9 + 1024+80;1024+80+(1024+72)*10 + 1024+80;...
        1024+80+(1024+72)*11 + 1024+80]/15.36e6;
    timeInstants = timeInstants(1:12);
    covarianceTime = besselj(0, 2*pi*fd*timeInstants);
    Ct = toeplitz(covarianceTime);

    %% Covarianza total
    Cfull = kron(Ct, Cf);

    Cfpfp = Cfull(ind, ind);
    Cffp = Cfull(:, ind);

    

    sigmaICI = Cf(1,1)*power_ici(fd, 1024, 15.36e6, 'jakes');
    sigmaN = Cf(1,1)*10.^(-SNRefectiva/10);



    xTest = 1/sqrt(2)*(randn(1, 10000)+1j*randn(1, 10000));
    for ii = 1:length(sigmaN)
        sigmaNiter = sigmaN(ii);
        Ce = Cfull - Cffp/(Cfpfp + (sigmaNiter + sigmaICI)*eye(Npilots))*Cffp';

        % P = sparse(double(ind), (1:length(ind)).', 1, 8400, 400);
        % Ce2 = inv(inv(Cfull) + 1/sigmaN*(P*P'));
        % Ce2 = (eye(length(Cfull)) + 1/sigmaN*Cfull*(P*P'))\Cfull;

        sigmaE = real(1/(600*12)*trace(Ce));
        % figure,helperPlotReceiveResourceGrid(enb,resourceGrid);
        SNR= 10*log10(real(trace(Cfull)/(8400*(sigmaE+sigmaNiter + sigmaICI))));

        fprintf('SNR efectiva: %f, sigmaE: %f\n', SNR, sigmaE);

        %% Capacidad
        capacity(jj,ii) = real(mean(log2(1 + 1/(sigmaE+sigmaNiter + sigmaICI)*abs(estChannel(:)).^2)));
        capacityUpper(jj,ii) = capacity(jj,ii) + ...
            real(mean(log2( (sigmaNiter+sigmaICI+sigmaE) ./ (sigmaNiter+(sigmaICI+sigmaE)*abs(xTest).^2) )));
    end

end

PintaIntervalosConfianza = @(x,lower,upper,color) set(fill([x,x(end:-1:1)],[upper,lower(end:-1:1)],color),'EdgeColor',color,'FaceAlpha',.15,'EdgeAlpha',.4);


h1=figure;
hold on

styles={'r','m','b'};

for jj=1:length(vN)
    PintaIntervalosConfianza(SNRefectiva,8*capacity(jj,:),8*capacityUpper(jj,:),styles{jj});
%     plot(SNRefectiva,8*capacity(jj,:),styles{jj},'linewidth',1.5);
end
hold off

grid on
% title('capacity bounds')
xlabel('SNR [dB]')
ylabel('capacity [Mbit/s]')
legend('v = 50 km/h','v = 100 km/h','v = 200 km/h','location','southeast')
set(findall(h1,'-property','FontSize'),'FontSize',12);
% set(findall(h1,'-property','FontName'),'FontName','Nimbus Roman No9 L')
print(h1,'-dpdf','-cmyk','/home/tecrail/EUCAP/r/CotasCapacidade_EUCAP2016.pdf')

return

%% Cálculo eficiencia espectral

clear
close all
clc
format long g

L = 20;
load('/home/tecrail/EUCAP/r/pdp.mat') 
pdp = pdp(1:L);
load('/home/tecrail/EUCAP/r/estChannel.mat') 


Npilots =0;

for ii=0:9
%%
    Nsubcarriers = 1024;
    enb.CyclicPrefix = 'Normal';
    enb.NDLRB        = 50;
    enb.CellRefP     = 1;
    enb.NCellID      = 3;
    enb.NSubframe    = ii;
    antPort          = 0;
    enb.DuplexMode = 'FDD';         % FDD

    resourceGrid = lteDLResourceGrid(enb);
    ind = lteCellRSIndices(enb,antPort);
    rs = lteCellRS(enb,antPort);
    resourceGrid (ind) = 1;
    Npilots = Npilots+length(ind(:));

end

UsefulBandwith=15.36*((600*140-(Npilots))/(1024*140+(2*(80+72*6))*10))