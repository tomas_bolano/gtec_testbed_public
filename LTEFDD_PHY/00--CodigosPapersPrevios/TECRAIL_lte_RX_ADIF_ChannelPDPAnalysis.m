clear variables;
close all;
format long g
clc;

fprintf('\t ==================================================\n');
fprintf('\t\t TECRAIL USRP B210 LTE 10 MHz MIMO sniffer\n');
fprintf('\t\t       ADIF Measurements 9-10-dec-2014     \n');
fprintf('\t ==================================================\n');

addpath('~/src/testbed/src/pnet', '-end');
addpath('~/src/testbed/src/matlab_utils','-end');
addpath('~/src/testbed/src/pers/net_commands', '-end');
addpath('~/Desktop','-end');
addpath('~/src/testbed/src/LTE_noGUI','-end');
addpath('~/src/testbed/src/matlab_utils/');


%% CASO 1: Primeira pasada, sobre t=150s, canle 1, fóra (cellid=3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=150;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measBackup','medidasProcesadas--E5-3','measEUCAP');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
'2014-12-11--00-06_max_throughput_50kmh_n_to_s.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

for iter=1:140
    rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSamples(1:1024+(iter-1)*1024)),[0,300]);
    rxSubcarriers = rxSubcarriersPadded([1:300,302:601]);
    potenciaDatos=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
    SNRinstantaneaT(iter)=(potenciaDatos-0.25)/0.25;
end

SNRinstantaneaTp=10*log10(mean(SNRinstantaneaT));

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    %TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    %TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    clear('N_id_1','N_id_2','N_id_cell','Nrx','NsymbSlot','Ntx','basePathForDataFiles','decimationFactorForProcessing','doFrequencyShift','f_start_idx','filenameForMatlabLTESystemToolbox','freq_offset','fs','interpolationFactors','longCPlength','longOFDMsymbolLength','m0','measurementTimestamp','mostras','nFFT','nHalfSamplesSlot','nSamplesFrame','nSamplesSlot','nSamplesSubframe','nZeroSamples','pss_symb','pss_thresh','rxSamples','rxSamplesUSRP','rxSignalsDecimated','rxSignalsInUSRPFormatFilename','rxSignalsShifted','shortCPlength','shortOFDMsymbolLength','signalSamplingRate','start_loc_vec','success','t0','windowLength')
%     save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% Cell-Wide Settings

% clear
% close all

enb.NDLRB = 50;                 % Number of resource blocks
enb.CellRefP = 1;               % One transmit antenna port
enb.NCellID = 3;               % Cell ID
enb.CyclicPrefix = 'Normal';    % Normal cyclic prefix
enb.DuplexMode = 'FDD';         % FDD
enb.LookForAllNCellIDs = false;

% Channel Estimator Configuration

cec.FreqWindow = 3;               % Frequency averaging window in 
                                  % Resource Elements (REs)
cec.TimeWindow = 3;               % Time averaging window in REs
cec.InterpType = 'linear';         % Cubic interpolation
cec.PilotAverage = 'UserDefined'; % Pilot averaging method
cec.InterpWinSize = 3;            % Interpolate up to 3 subframes 
                                  % simultaneously
cec.InterpWindow = 'Causal';     % Interpolation windowing method

% Synchronization

clear rxWaveform;
clear rxWaveformSync;
clear rxGrid;
clear estChannel;
% xvFileName='/media/tecrail/measBackup/medidasProcesadas--E5-3/measEUCAP/LteReceivedFrameSync.mat';
% load(xvFileName,'xvRXSamplest2', 'measurementTimeStamp');
fprintf(' Measurement data timestamp\n\t ''%s''\n\n', measurementTimeStamp);
rxWaveform = xvRXSamplest2.';
corrcfg.PSS = 'On';
corrcfg.SSS = 'On';
corrcfg.CellRS = 'On';
offset = 0;


cfgdl = struct;
cfgdl.NDLRB = enb.NDLRB;
cfgdl.CyclicPrefix = 'Normal';
cfgdl.DuplexMode = 'FDD';
foffset = lteFrequencyOffset(cfgdl, rxWaveform)
rxWaveform = lteFrequencyCorrect(cfgdl,rxWaveform,foffset);

if (enb.NDLRB == 100)
    samplingRate = 30.72e6;
elseif (enb.NDLRB == 50)
    samplingRate = 15.36e6;
end
enb.CellRefP = 1; 
if (enb.LookForAllNCellIDs)
    fprintf(' Looking for all possible Physical Cell IDs . . . ');
    [cellid, offset] = lteCellSearch(enb,rxWaveform)
else
    fprintf(' Looking for Physical Cell ID = %d . . . ', enb.NCellID);
    [cellid, offset] = lteCellSearch(enb,rxWaveform, enb.NCellID)
end
fprintf(' OK\n');
startIndex = 1 + offset;
endIndex = startIndex - 1 + samplingRate*10e-3;
rxWaveformSync = rxWaveform(startIndex:endIndex, :);
% xvRXSamplest2 = rxWaveformSync.';
% save('/media/tecrail/measBackup/medidasProcesadas--E5-3/measEUCAP/LteReceivedFrameSync.mat', ...
%     'xvRXSamplest2', 'measurementTimeStamp')
% clear xvRXSamplest2

% OFDM Demodulation

enb.Ng = 'One';
enb.CFI = 2;
enb.PHICHDuration = 'Normal';

rxGrid = sqrt(600)/1024*lteOFDMDemodulate(enb, rxWaveformSync);
%figure('Color','w');
%helperPlotReceiveResourceGrid(enb,rxGrid);

% Channel Estimation

enb.NSubframe = 0;

cec.PilotAverage = 'UserDefined';
cec.FreqWindow = 1;
cec.TimeWindow = 1;
cec.InterpType = 'v4';
cec.InterpWindow = 'Centered';
cec.InterpWinSize = 1;


[estChannel, noiseEst] = lteDLChannelEstimate(enb, cec, rxGrid);
figure('Color','w');
helperPlotChannelEstimate(estChannel);

instantaneousSNR=10*log10((((rxGrid(:)')*rxGrid(:)/length(rxGrid(:)))-0.25)/(0.25));

% Estimación do PDP

extendedEstChannel = [zeros(212,140);estChannel;zeros(212,140)];
extendedEstChannel = ifftshift(extendedEstChannel,1);
timeResponses = sqrt(1024)*ifft(extendedEstChannel);
timeResponses = timeResponses(1:72,:);
% timeResponses = timeResponses-mean(timeResponses,2)*ones(1,140); !!
pdp=diag(abs(timeResponses*(timeResponses')))/140;
pdp = pdp/max(pdp);
figure,stem(1e6*(0:19)/(15.36e6),pdp(1:20))
title('normalized power delay profile')
xlabel('delay [\mus]'),ylabel('normalized magnitude')
grid on
hold on
plot(1e6*(0:19)/(15.36e6),pdp(1:20),'r:')
timeResponses1=timeResponses;
pdp1=pdp;

save('/home/tecrail/EUCAP/r/pdp1.mat','pdp1') 

%% CASO 2: Primeira pasada, sobre t=200s, canle 1, fóra (cellid=3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=200;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measBackup','medidasProcesadas--E5-3','measEUCAP');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
'2014-12-11--00-06_max_throughput_50kmh_n_to_s.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    %TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    %TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    clear('N_id_1','N_id_2','N_id_cell','Nrx','NsymbSlot','Ntx','basePathForDataFiles','decimationFactorForProcessing','doFrequencyShift','f_start_idx','filenameForMatlabLTESystemToolbox','freq_offset','fs','interpolationFactors','longCPlength','longOFDMsymbolLength','m0','measurementTimestamp','mostras','nFFT','nHalfSamplesSlot','nSamplesFrame','nSamplesSlot','nSamplesSubframe','nZeroSamples','pss_symb','pss_thresh','rxSamples','rxSamplesUSRP','rxSignalsDecimated','rxSignalsInUSRPFormatFilename','rxSignalsShifted','shortCPlength','shortOFDMsymbolLength','signalSamplingRate','start_loc_vec','success','t0','windowLength')
%     save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% Cell-Wide Settings

% clear
% close all

enb.NDLRB = 50;                 % Number of resource blocks
enb.CellRefP = 1;               % One transmit antenna port
enb.NCellID = 3;               % Cell ID
enb.CyclicPrefix = 'Normal';    % Normal cyclic prefix
enb.DuplexMode = 'FDD';         % FDD
enb.LookForAllNCellIDs = false;

% Channel Estimator Configuration

cec.FreqWindow = 3;               % Frequency averaging window in 
                                  % Resource Elements (REs)
cec.TimeWindow = 3;               % Time averaging window in REs
cec.InterpType = 'linear';         % Cubic interpolation
cec.PilotAverage = 'UserDefined'; % Pilot averaging method
cec.InterpWinSize = 3;            % Interpolate up to 3 subframes 
                                  % simultaneously
cec.InterpWindow = 'Causal';     % Interpolation windowing method

% Synchronization

clear rxWaveform;
clear rxWaveformSync;
clear rxGrid;
clear estChannel;
% xvFileName='/media/tecrail/measBackup/medidasProcesadas--E5-3/measEUCAP/LteReceivedFrameSync.mat';
% load(xvFileName,'xvRXSamplest2', 'measurementTimeStamp');
fprintf(' Measurement data timestamp\n\t ''%s''\n\n', measurementTimeStamp);
rxWaveform = xvRXSamplest2.';
corrcfg.PSS = 'On';
corrcfg.SSS = 'On';
corrcfg.CellRS = 'On';
offset = 0;


cfgdl = struct;
cfgdl.NDLRB = enb.NDLRB;
cfgdl.CyclicPrefix = 'Normal';
cfgdl.DuplexMode = 'FDD';
foffset = lteFrequencyOffset(cfgdl, rxWaveform)
rxWaveform = lteFrequencyCorrect(cfgdl,rxWaveform,foffset);

if (enb.NDLRB == 100)
    samplingRate = 30.72e6;
elseif (enb.NDLRB == 50)
    samplingRate = 15.36e6;
end
enb.CellRefP = 1; 
if (enb.LookForAllNCellIDs)
    fprintf(' Looking for all possible Physical Cell IDs . . . ');
    [cellid, offset] = lteCellSearch(enb,rxWaveform)
else
    fprintf(' Looking for Physical Cell ID = %d . . . ', enb.NCellID);
    [cellid, offset] = lteCellSearch(enb,rxWaveform, enb.NCellID)
end
fprintf(' OK\n');
startIndex = 1 + offset;
endIndex = startIndex - 1 + samplingRate*10e-3;
rxWaveformSync = rxWaveform(startIndex:endIndex, :);
% xvRXSamplest2 = rxWaveformSync.';
% save('/media/tecrail/measBackup/medidasProcesadas--E5-3/measEUCAP/LteReceivedFrameSync.mat', ...
%     'xvRXSamplest2', 'measurementTimeStamp')
% clear xvRXSamplest2

% OFDM Demodulation

enb.Ng = 'One';
enb.CFI = 2;
enb.PHICHDuration = 'Normal';

rxGrid = lteOFDMDemodulate(enb, rxWaveformSync);
%figure('Color','w');
%helperPlotReceiveResourceGrid(enb,rxGrid);

% Channel Estimation

enb.NSubframe = 0;

cec.PilotAverage = 'UserDefined';
cec.FreqWindow = 1;
cec.TimeWindow = 1;
cec.InterpType = 'v4';
cec.InterpWindow = 'Centered';
cec.InterpWinSize = 1;


[estChannel, noiseEst] = lteDLChannelEstimate(enb, cec, rxGrid);
figure('Color','w');
helperPlotChannelEstimate(estChannel);

% Estimación do PDP

extendedEstChannel = [zeros(212,140);estChannel;zeros(212,140)];
extendedEstChannel = ifftshift(extendedEstChannel,1);
timeResponses = sqrt(1024)*ifft(extendedEstChannel);
timeResponses = timeResponses(1:72,:);
pdp=diag(abs(timeResponses*(timeResponses')))/140;
pdp = pdp/max(pdp);
figure,stem(1e6*(0:19)/(15.36e6),pdp(1:20))
title('normalized power delay profile')
xlabel('delay [\mus]'),ylabel('normalized magnitude')
grid on
hold on
plot(1e6*(0:19)/(15.36e6),pdp(1:20),'r:')
timeResponses2=timeResponses;
pdp2=pdp;

save('/home/tecrail/EUCAP/r/pdp2.mat','pdp2') 

% figure(30)
% hold on
% plot(abs(timeResponses(1,:))/mean(abs(timeResponses(1,:))))
% title('first tap evolution')
% xlabel('time [ms]'),ylabel('normalized magnitude')
% grid on

% figure(31)
% hold on
% plot(abs(timeResponses(2,:))/mean(abs(timeResponses(2,:))))
% title('second tap evolution')
% xlabel('time [ms]'),ylabel('normalized magnitude')
% grid on
% 
% figure(32)
% hold on
% plot(abs(timeResponses(3,:))/mean(abs(timeResponses(3,:))))
% title('third tap evolution')
% xlabel('time [ms]'),ylabel('normalized magnitude')
% grid on

%% CASO 4: Primeira pasada, sobre t=230s, canle 1, dentro (cellid=3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=230;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measBackup','medidasProcesadas--E5-3','measEUCAP');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
'2014-12-11--00-07_max_throughput_50kmh_n_to_s.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    %TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    %TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    clear('N_id_1','N_id_2','N_id_cell','Nrx','NsymbSlot','Ntx','basePathForDataFiles','decimationFactorForProcessing','doFrequencyShift','f_start_idx','filenameForMatlabLTESystemToolbox','freq_offset','fs','interpolationFactors','longCPlength','longOFDMsymbolLength','m0','measurementTimestamp','mostras','nFFT','nHalfSamplesSlot','nSamplesFrame','nSamplesSlot','nSamplesSubframe','nZeroSamples','pss_symb','pss_thresh','rxSamples','rxSamplesUSRP','rxSignalsDecimated','rxSignalsInUSRPFormatFilename','rxSignalsShifted','shortCPlength','shortOFDMsymbolLength','signalSamplingRate','start_loc_vec','success','t0','windowLength')
%     save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% Cell-Wide Settings

% clear
% close all

enb.NDLRB = 50;                 % Number of resource blocks
enb.CellRefP = 1;               % One transmit antenna port
enb.NCellID = 3;               % Cell ID
enb.CyclicPrefix = 'Normal';    % Normal cyclic prefix
enb.DuplexMode = 'FDD';         % FDD
enb.LookForAllNCellIDs = false;

% Channel Estimator Configuration

cec.FreqWindow = 3;               % Frequency averaging window in 
                                  % Resource Elements (REs)
cec.TimeWindow = 3;               % Time averaging window in REs
cec.InterpType = 'linear';         % Cubic interpolation
cec.PilotAverage = 'UserDefined'; % Pilot averaging method
cec.InterpWinSize = 3;            % Interpolate up to 3 subframes 
                                  % simultaneously
cec.InterpWindow = 'Causal';     % Interpolation windowing method

% Synchronization

clear rxWaveform;
clear rxWaveformSync;
clear rxGrid;
clear estChannel;
% xvFileName='/media/tecrail/measBackup/medidasProcesadas--E5-3/measEUCAP/LteReceivedFrameSync.mat';
% load(xvFileName,'xvRXSamplest2', 'measurementTimeStamp');
fprintf(' Measurement data timestamp\n\t ''%s''\n\n', measurementTimeStamp);
rxWaveform = xvRXSamplest2.';
corrcfg.PSS = 'On';
corrcfg.SSS = 'On';
corrcfg.CellRS = 'On';
offset = 0;


cfgdl = struct;
cfgdl.NDLRB = enb.NDLRB;
cfgdl.CyclicPrefix = 'Normal';
cfgdl.DuplexMode = 'FDD';
foffset = lteFrequencyOffset(cfgdl, rxWaveform)
rxWaveform = lteFrequencyCorrect(cfgdl,rxWaveform,foffset);

if (enb.NDLRB == 100)
    samplingRate = 30.72e6;
elseif (enb.NDLRB == 50)
    samplingRate = 15.36e6;
end
enb.CellRefP = 1; 
if (enb.LookForAllNCellIDs)
    fprintf(' Looking for all possible Physical Cell IDs . . . ');
    [cellid, offset] = lteCellSearch(enb,rxWaveform)
else
    fprintf(' Looking for Physical Cell ID = %d . . . ', enb.NCellID);
    [cellid, offset] = lteCellSearch(enb,rxWaveform, enb.NCellID)
end
fprintf(' OK\n');
startIndex = 1 + offset;
endIndex = startIndex - 1 + samplingRate*10e-3;
rxWaveformSync = rxWaveform(startIndex:endIndex, :);
% xvRXSamplest2 = rxWaveformSync.';
% save('/media/tecrail/measBackup/medidasProcesadas--E5-3/measEUCAP/LteReceivedFrameSync.mat', ...
%     'xvRXSamplest2', 'measurementTimeStamp')
% clear xvRXSamplest2

% OFDM Demodulation

enb.Ng = 'One';
enb.CFI = 2;
enb.PHICHDuration = 'Normal';

rxGrid = lteOFDMDemodulate(enb, rxWaveformSync);
%figure('Color','w');
%helperPlotReceiveResourceGrid(enb,rxGrid);

% Channel Estimation

enb.NSubframe = 0;

cec.PilotAverage = 'UserDefined';
cec.FreqWindow = 1;
cec.TimeWindow = 1;
cec.InterpType = 'v4';
cec.InterpWindow = 'Centered';
cec.InterpWinSize = 1;


[estChannel, noiseEst] = lteDLChannelEstimate(enb, cec, rxGrid);
figure('Color','w');
helperPlotChannelEstimate(estChannel);

% Estimación do PDP

extendedEstChannel = [zeros(212,140);estChannel;zeros(212,140)];
extendedEstChannel = ifftshift(extendedEstChannel,1);
timeResponses = sqrt(1024)*ifft(extendedEstChannel);
timeResponses = timeResponses(1:72,:);
pdp=diag(abs(timeResponses*(timeResponses')))/140;
pdp = pdp/max(pdp);
figure,stem(1e6*(0:19)/(15.36e6),pdp(1:20))
title('normalized power delay profile')
xlabel('delay [\mus]'),ylabel('normalized magnitude')
grid on
hold on
plot(1e6*(0:19)/(15.36e6),pdp(1:20),'r:')
timeResponses4=timeResponses;
pdp4=pdp;

save('/home/tecrail/EUCAP/r/pdp4.mat','pdp4') 

figure(30)
hold on
plot(((1:140)-1)/14,abs(timeResponses(1,:))/mean(abs(timeResponses(1,:))),'r','linewidth',1.5)
%title('first tap evolution')
xlabel('time [ms]'),ylabel('normalized magnitude')
grid on

figure(31)
hold on
plot(abs(timeResponses(2,:))/mean(abs(timeResponses(2,:))),'r')
title('second tap evolution')
xlabel('time [ms]'),ylabel('normalized magnitude')
grid on

figure(32)
hold on
plot(abs(timeResponses(3,:))/mean(abs(timeResponses(3,:))),'r')
title('third tap evolution')
xlabel('time [ms]'),ylabel('normalized magnitude')
grid on

%% CASO 14: Pasada a 100 km/h, sobre t=100s, canle 1, fóra (cellid = 3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=100;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measBackup','medidasProcesadas--E5-3','measEUCAP');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
'2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.o.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    %TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    %TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    clear('N_id_1','N_id_2','N_id_cell','Nrx','NsymbSlot','Ntx','basePathForDataFiles','decimationFactorForProcessing','doFrequencyShift','f_start_idx','filenameForMatlabLTESystemToolbox','freq_offset','fs','interpolationFactors','longCPlength','longOFDMsymbolLength','m0','measurementTimestamp','mostras','nFFT','nHalfSamplesSlot','nSamplesFrame','nSamplesSlot','nSamplesSubframe','nZeroSamples','pss_symb','pss_thresh','rxSamples','rxSamplesUSRP','rxSignalsDecimated','rxSignalsInUSRPFormatFilename','rxSignalsShifted','shortCPlength','shortOFDMsymbolLength','signalSamplingRate','start_loc_vec','success','t0','windowLength')
%     save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% Cell-Wide Settings

% clear
% close all

enb.NDLRB = 50;                 % Number of resource blocks
enb.CellRefP = 1;               % One transmit antenna port
enb.NCellID = 3;               % Cell ID
enb.CyclicPrefix = 'Normal';    % Normal cyclic prefix
enb.DuplexMode = 'FDD';         % FDD
enb.LookForAllNCellIDs = false;

% Channel Estimator Configuration

cec.FreqWindow = 3;               % Frequency averaging window in 
                                  % Resource Elements (REs)
cec.TimeWindow = 3;               % Time averaging window in REs
cec.InterpType = 'linear';         % Cubic interpolation
cec.PilotAverage = 'UserDefined'; % Pilot averaging method
cec.InterpWinSize = 3;            % Interpolate up to 3 subframes 
                                  % simultaneously
cec.InterpWindow = 'Causal';     % Interpolation windowing method

% Synchronization

clear rxWaveform;
clear rxWaveformSync;
clear rxGrid;
clear estChannel;
% xvFileName='/media/tecrail/measBackup/medidasProcesadas--E5-3/measEUCAP/LteReceivedFrameSync.mat';
% load(xvFileName,'xvRXSamplest2', 'measurementTimeStamp');
fprintf(' Measurement data timestamp\n\t ''%s''\n\n', measurementTimeStamp);
rxWaveform = xvRXSamplest2.';
corrcfg.PSS = 'On';
corrcfg.SSS = 'On';
corrcfg.CellRS = 'On';
offset = 0;


cfgdl = struct;
cfgdl.NDLRB = enb.NDLRB;
cfgdl.CyclicPrefix = 'Normal';
cfgdl.DuplexMode = 'FDD';
foffset = lteFrequencyOffset(cfgdl, rxWaveform)
rxWaveform = lteFrequencyCorrect(cfgdl,rxWaveform,foffset);

if (enb.NDLRB == 100)
    samplingRate = 30.72e6;
elseif (enb.NDLRB == 50)
    samplingRate = 15.36e6;
end
enb.CellRefP = 1; 
if (enb.LookForAllNCellIDs)
    fprintf(' Looking for all possible Physical Cell IDs . . . ');
    [cellid, offset] = lteCellSearch(enb,rxWaveform)
else
    fprintf(' Looking for Physical Cell ID = %d . . . ', enb.NCellID);
    [cellid, offset] = lteCellSearch(enb,rxWaveform, enb.NCellID)
end
fprintf(' OK\n');
startIndex = 1 + offset;
endIndex = startIndex - 1 + samplingRate*10e-3;
rxWaveformSync = rxWaveform(startIndex:endIndex, :);
% xvRXSamplest2 = rxWaveformSync.';
% save('/media/tecrail/measBackup/medidasProcesadas--E5-3/measEUCAP/LteReceivedFrameSync.mat', ...
%     'xvRXSamplest2', 'measurementTimeStamp')
% clear xvRXSamplest2

% OFDM Demodulation

enb.Ng = 'One';
enb.CFI = 2;
enb.PHICHDuration = 'Normal';

rxGrid = lteOFDMDemodulate(enb, rxWaveformSync);
%figure('Color','w');
%helperPlotReceiveResourceGrid(enb,rxGrid);

% Channel Estimation

enb.NSubframe = 0;

cec.PilotAverage = 'UserDefined';
cec.FreqWindow = 1;
cec.TimeWindow = 1;
cec.InterpType = 'v4';
cec.InterpWindow = 'Centered';
cec.InterpWinSize = 1;


[estChannel, noiseEst] = lteDLChannelEstimate(enb, cec, rxGrid);
figure('Color','w');
helperPlotChannelEstimate(estChannel);

% Estimación do PDP

extendedEstChannel = [zeros(212,140);estChannel;zeros(212,140)];
extendedEstChannel = ifftshift(extendedEstChannel,1);
timeResponses = sqrt(1024)*ifft(extendedEstChannel);
timeResponses = timeResponses(1:72,:);
pdp=diag(abs(timeResponses*(timeResponses')))/140;
pdp = pdp/max(pdp);
figure,stem(1e6*(0:19)/(15.36e6),pdp(1:20))
title('normalized power delay profile')
xlabel('delay [\mus]'),ylabel('normalized magnitude')
grid on
hold on
plot(1e6*(0:19)/(15.36e6),pdp(1:20),'r:')
timeResponses14=timeResponses;
pdp14=pdp;

save('/home/tecrail/EUCAP/r/pdp14.mat','pdp14') 

figure(30)
hold on
plot(((1:140)-1)/14,abs(timeResponses(1,:))/mean(abs(timeResponses(1,:))),'m','linewidth',1.5)
%title('first tap evolution')
xlabel('time [ms]'),ylabel('normalized magnitude')
grid on

figure(31)
hold on
plot(abs(timeResponses(2,:))/mean(abs(timeResponses(2,:))),'g')
title('second tap evolution')
xlabel('time [ms]'),ylabel('normalized magnitude')
grid on

figure(32)
hold on
plot(abs(timeResponses(3,:))/mean(abs(timeResponses(3,:))),'g')
title('third tap evolution')
xlabel('time [ms]'),ylabel('normalized magnitude')
grid on

%% CASO 18: Pasada a 200 km/h, sobre t=85s, canle 1, fóra (cellid = 3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=85;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measBackup','medidasProcesadas--E5-3','measEUCAP');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
'2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.o.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    %TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    %TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    clear('N_id_1','N_id_2','N_id_cell','Nrx','NsymbSlot','Ntx','basePathForDataFiles','decimationFactorForProcessing','doFrequencyShift','f_start_idx','filenameForMatlabLTESystemToolbox','freq_offset','fs','interpolationFactors','longCPlength','longOFDMsymbolLength','m0','measurementTimestamp','mostras','nFFT','nHalfSamplesSlot','nSamplesFrame','nSamplesSlot','nSamplesSubframe','nZeroSamples','pss_symb','pss_thresh','rxSamples','rxSamplesUSRP','rxSignalsDecimated','rxSignalsInUSRPFormatFilename','rxSignalsShifted','shortCPlength','shortOFDMsymbolLength','signalSamplingRate','start_loc_vec','success','t0','windowLength')
%     save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% Cell-Wide Settings

% clear
% close all

enb.NDLRB = 50;                 % Number of resource blocks
enb.CellRefP = 1;               % One transmit antenna port
enb.NCellID = 3;               % Cell ID
enb.CyclicPrefix = 'Normal';    % Normal cyclic prefix
enb.DuplexMode = 'FDD';         % FDD
enb.LookForAllNCellIDs = false;

% Channel Estimator Configuration

cec.FreqWindow = 3;               % Frequency averaging window in 
                                  % Resource Elements (REs)
cec.TimeWindow = 3;               % Time averaging window in REs
cec.InterpType = 'linear';         % Cubic interpolation
cec.PilotAverage = 'UserDefined'; % Pilot averaging method
cec.InterpWinSize = 3;            % Interpolate up to 3 subframes 
                                  % simultaneously
cec.InterpWindow = 'Causal';     % Interpolation windowing method

% Synchronization

clear rxWaveform;
clear rxWaveformSync;
clear rxGrid;
clear estChannel;
% xvFileName='/media/tecrail/measBackup/medidasProcesadas--E5-3/measEUCAP/LteReceivedFrameSync.mat';
% load(xvFileName,'xvRXSamplest2', 'measurementTimeStamp');
fprintf(' Measurement data timestamp\n\t ''%s''\n\n', measurementTimeStamp);
rxWaveform = xvRXSamplest2.';
corrcfg.PSS = 'On';
corrcfg.SSS = 'On';
corrcfg.CellRS = 'On';
offset = 0;


cfgdl = struct;
cfgdl.NDLRB = enb.NDLRB;
cfgdl.CyclicPrefix = 'Normal';
cfgdl.DuplexMode = 'FDD';
foffset = lteFrequencyOffset(cfgdl, rxWaveform)
rxWaveform = lteFrequencyCorrect(cfgdl,rxWaveform,foffset);

if (enb.NDLRB == 100)
    samplingRate = 30.72e6;
elseif (enb.NDLRB == 50)
    samplingRate = 15.36e6;
end
enb.CellRefP = 1; 
if (enb.LookForAllNCellIDs)
    fprintf(' Looking for all possible Physical Cell IDs . . . ');
    [cellid, offset] = lteCellSearch(enb,rxWaveform)
else
    fprintf(' Looking for Physical Cell ID = %d . . . ', enb.NCellID);
    [cellid, offset] = lteCellSearch(enb,rxWaveform, enb.NCellID)
end
fprintf(' OK\n');
startIndex = 1 + offset;
endIndex = startIndex - 1 + samplingRate*10e-3;
rxWaveformSync = rxWaveform(startIndex:endIndex, :);
% xvRXSamplest2 = rxWaveformSync.';
% save('/media/tecrail/measBackup/medidasProcesadas--E5-3/measEUCAP/LteReceivedFrameSync.mat', ...
%     'xvRXSamplest2', 'measurementTimeStamp')
% clear xvRXSamplest2

% OFDM Demodulation

enb.Ng = 'One';
enb.CFI = 2;
enb.PHICHDuration = 'Normal';

rxGrid = lteOFDMDemodulate(enb, rxWaveformSync);
%figure('Color','w');
%helperPlotReceiveResourceGrid(enb,rxGrid);

% Channel Estimation

enb.NSubframe = 0;

cec.PilotAverage = 'UserDefined';
cec.FreqWindow = 1;
cec.TimeWindow = 1;
cec.InterpType = 'v4';
cec.InterpWindow = 'Centered';
cec.InterpWinSize = 1;


[estChannel, noiseEst] = lteDLChannelEstimate(enb, cec, rxGrid);
figure('Color','w');
helperPlotChannelEstimate(estChannel);

% Estimación do PDP

extendedEstChannel = [zeros(212,140);estChannel;zeros(212,140)];
extendedEstChannel = ifftshift(extendedEstChannel,1);
timeResponses = sqrt(1024)*ifft(extendedEstChannel);
timeResponses = timeResponses(1:72,:);
pdp=diag(abs(timeResponses*(timeResponses')))/140;
pdp = pdp/max(pdp);
figure,stem(1e6*(0:19)/(15.36e6),pdp(1:20))
title('normalized power delay profile')
xlabel('delay [\mus]'),ylabel('normalized magnitude')
grid on
hold on
plot(1e6*(0:19)/(15.36e6),pdp(1:20),'r:')
timeResponses18=timeResponses;
pdp18=pdp;

save('/home/tecrail/EUCAP/r/pdp18.mat','pdp18') 

h1=figure(30)
hold on
plot(((1:140)-1)/14,abs(timeResponses(1,:))/mean(abs(timeResponses(1,:))),'k','linewidth',1.5)
%title('first tap evolution')
xlabel('time [ms]'),ylabel('normalized magnitude')
grid on
legend('v = 50 km/h (outd.)','v = 100 km/h (outd.)','v = 200 km/h (outd.)')
hold off
set(findall(h1,'-property','FontSize'),'FontSize',12);%set(findall(h1,'-property','FontName'),'FontName','Times New Roman')
print(h1,'-dpdf','-cmyk','/home/tecrail/EUCAP/r/RepresentacionEvolucionPDPs_EUCAP2016.pdf')

figure(31)
hold on
plot(abs(timeResponses(2,:))/mean(abs(timeResponses(2,:))),'k')
title('second tap evolution')
xlabel('time [ms]'),ylabel('normalized magnitude')
grid on
legend('50 km/h (outd.)','100 km/h (outd.)','200 km/h (outd.)')
hold off

figure(32)
hold on
plot(abs(timeResponses(3,:))/mean(abs(timeResponses(3,:))),'k')
title('third tap evolution')
xlabel('time [ms]'),ylabel('normalized magnitude')
grid on
legend('50 km/h (outd.)','100 km/h (outd.)','200 km/h (outd.)')
hold off


%% CASO 20: Pasada a 200 km/h, sobre t=84s, canle 1, dentro (cellid = 3)

clear

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;



t0=84;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs;
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measBackup','medidasProcesadas--E5-3','measEUCAP');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
'2014-12-10--04-24_indoor_etcs_voip_200kmh_n_to_s.i.ch0.usrp');
[rxSamplesUSRP, success] = TECRAIL_readUSRPSignalFromFile(...
        rxSignalsInUSRPFormatFilename, mostras, m0);
rxSamples = TECRAIL_convertFromUSRPFormat(rxSamplesUSRP);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end
    
    %TECRAIL_spectrum(rxSignalsShifted(1,:), signalSamplingRate);
    
    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
    %TECRAIL_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);
    
    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
    xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);

    % Save for Matlab LTE System Toolbox
    filenameForMatlabLTESystemToolbox = ...
        fullfile(filesep, basePathForDataFiles, 'LteReceivedFrameSync.mat');
    if (~ isempty(dir(filenameForMatlabLTESystemToolbox)))
        delete(filenameForMatlabLTESystemToolbox);
    end
    measurementTimestamp = now;
    measurementTimeStamp = datestr(measurementTimestamp,'yyyy mmm dd HH:MM');
    clear('N_id_1','N_id_2','N_id_cell','Nrx','NsymbSlot','Ntx','basePathForDataFiles','decimationFactorForProcessing','doFrequencyShift','f_start_idx','filenameForMatlabLTESystemToolbox','freq_offset','fs','interpolationFactors','longCPlength','longOFDMsymbolLength','m0','measurementTimestamp','mostras','nFFT','nHalfSamplesSlot','nSamplesFrame','nSamplesSlot','nSamplesSubframe','nZeroSamples','pss_symb','pss_thresh','rxSamples','rxSamplesUSRP','rxSignalsDecimated','rxSignalsInUSRPFormatFilename','rxSignalsShifted','shortCPlength','shortOFDMsymbolLength','signalSamplingRate','start_loc_vec','success','t0','windowLength')
%     save(filenameForMatlabLTESystemToolbox, 'xvRXSamplest2', 'measurementTimeStamp');

    % PERS_CommandFinish(rxIP, rxPort, rxLocalPort)
% % % end

% Cell-Wide Settings

% clear
% close all

enb.NDLRB = 50;                 % Number of resource blocks
enb.CellRefP = 1;               % One transmit antenna port
enb.NCellID = 3;               % Cell ID
enb.CyclicPrefix = 'Normal';    % Normal cyclic prefix
enb.DuplexMode = 'FDD';         % FDD
enb.LookForAllNCellIDs = false;

% Channel Estimator Configuration

cec.FreqWindow = 3;               % Frequency averaging window in 
                                  % Resource Elements (REs)
cec.TimeWindow = 3;               % Time averaging window in REs
cec.InterpType = 'linear';         % Cubic interpolation
cec.PilotAverage = 'UserDefined'; % Pilot averaging method
cec.InterpWinSize = 3;            % Interpolate up to 3 subframes 
                                  % simultaneously
cec.InterpWindow = 'Causal';     % Interpolation windowing method

% Synchronization

clear rxWaveform;
clear rxWaveformSync;
clear rxGrid;
clear estChannel;
% xvFileName='/media/tecrail/measBackup/medidasProcesadas--E5-3/measEUCAP/LteReceivedFrameSync.mat';
% load(xvFileName,'xvRXSamplest2', 'measurementTimeStamp');
fprintf(' Measurement data timestamp\n\t ''%s''\n\n', measurementTimeStamp);
rxWaveform = xvRXSamplest2.';
corrcfg.PSS = 'On';
corrcfg.SSS = 'On';
corrcfg.CellRS = 'On';
offset = 0;


cfgdl = struct;
cfgdl.NDLRB = enb.NDLRB;
cfgdl.CyclicPrefix = 'Normal';
cfgdl.DuplexMode = 'FDD';
foffset = lteFrequencyOffset(cfgdl, rxWaveform)
rxWaveform = lteFrequencyCorrect(cfgdl,rxWaveform,foffset);

if (enb.NDLRB == 100)
    samplingRate = 30.72e6;
elseif (enb.NDLRB == 50)
    samplingRate = 15.36e6;
end
enb.CellRefP = 1; 
if (enb.LookForAllNCellIDs)
    fprintf(' Looking for all possible Physical Cell IDs . . . ');
    [cellid, offset] = lteCellSearch(enb,rxWaveform)
else
    fprintf(' Looking for Physical Cell ID = %d . . . ', enb.NCellID);
    [cellid, offset] = lteCellSearch(enb,rxWaveform, enb.NCellID)
end
fprintf(' OK\n');
startIndex = 1 + offset;
endIndex = startIndex - 1 + samplingRate*10e-3;
rxWaveformSync = rxWaveform(startIndex:endIndex, :);
% xvRXSamplest2 = rxWaveformSync.';
% save('/media/tecrail/measBackup/medidasProcesadas--E5-3/measEUCAP/LteReceivedFrameSync.mat', ...
%     'xvRXSamplest2', 'measurementTimeStamp')
% clear xvRXSamplest2

% OFDM Demodulation

enb.Ng = 'One';
enb.CFI = 2;
enb.PHICHDuration = 'Normal';

rxGrid = lteOFDMDemodulate(enb, rxWaveformSync);
% figure('Color','w');
%helperPlotReceiveResourceGrid(enb,rxGrid);

% Channel Estimation

enb.NSubframe = 0;

cec.PilotAverage = 'UserDefined';
cec.FreqWindow = 1;
cec.TimeWindow = 1;
cec.InterpType = 'v4';
cec.InterpWindow = 'Centered';
cec.InterpWinSize = 1;


[estChannel, noiseEst] = lteDLChannelEstimate(enb, cec, rxGrid);
figure('Color','w');
helperPlotChannelEstimate(estChannel);

% Estimación do PDP

extendedEstChannel = [zeros(212,140);estChannel;zeros(212,140)];
extendedEstChannel = ifftshift(extendedEstChannel,1);
timeResponses = sqrt(1024)*ifft(extendedEstChannel);
timeResponses = timeResponses(1:72,:);
pdp=diag(abs(timeResponses*(timeResponses')))/140;
pdp = pdp/max(pdp);
figure,stem(1e6*(0:19)/(15.36e6),pdp(1:20))
title('normalized power delay profile')
xlabel('delay [\mus]'),ylabel('normalized magnitude')
grid on
hold on
plot(1e6*(0:19)/(15.36e6),pdp(1:20),'r:')
timeResponses20=timeResponses;
pdp20=pdp;

save('/home/tecrail/EUCAP/r/pdp20.mat','pdp20') 

%% Representación do pdp (EUCAP 2016)

load('/home/tecrail/EUCAP/r/pdp1.mat')
load('/home/tecrail/EUCAP/r/pdp4.mat')
load('/home/tecrail/EUCAP/r/pdp14.mat')
load('/home/tecrail/EUCAP/r/pdp18.mat')

h1=figure,plot(1e6*(0:19)/(15.36e6),pdp1(1:20),'b')
% title('normalized power delay profile')
xlabel('delay [\mus]'),ylabel('normalized magnitude')
grid on
hold on
plot(1e6*(0:19)/(15.36e6),pdp14(1:20),'r')
plot(1e6*(0:19)/(15.36e6),pdp18(1:20),'m')
plot(1e6*(0:19)/(15.36e6),pdp4(1:20),'k--')
plot(1e6*(0:19)/(15.36e6),pdp1(1:20),'ob')
plot(1e6*(0:19)/(15.36e6),pdp14(1:20),'or')
plot(1e6*(0:19)/(15.36e6),pdp18(1:20),'om')
plot(1e6*(0:19)/(15.36e6),pdp4(1:20),'ok')
hold off
legend('v = 50 km/h (outd.)','v = 100 km/h (outd.)','v = 200 km/h (outd.)','v = 50 km/h (ind.)')
set(findall(h1,'-property','FontSize'),'FontSize',12);%set(findall(h1,'-property','FontName'),'FontName','Times New Roman')
print(h1,'-dpdf','-cmyk','/home/tecrail/EUCAP/r/RepresentacionsPDP_EUCAP2016.pdf')