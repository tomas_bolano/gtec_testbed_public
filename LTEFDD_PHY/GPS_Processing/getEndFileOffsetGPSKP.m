function EndFileOffset = getEndFileOffsetGPSKP(GPSData, tfgps)

EndFileOffset=tfgps-(GPSData(end).time.day_seconds-GPSData(1).time.day_seconds);

end