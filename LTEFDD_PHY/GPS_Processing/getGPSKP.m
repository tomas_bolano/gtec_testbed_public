function PK = getGPSKP(GPSData, ston, t, t_off, invert_ston)
%GETGPSKP Get kilometric points from GPS DATA
%
% Input:
%   GPSData -  GPS data
%   ston - if true south to north measurement, else south to north
%   t - measurement time instants, assuming synchronized with end of GPS data
%   t_off - t offset (for desalignments of the data)
%   invert_ston - invert Kilometric points output if south to north (default true)

if nargin < 5
    invert_ston = true;
end

% Coordenadas UTM da antena
AntX = 347077.9;
AntY = 4103747.4;

% Punto kilometrico da antena
AntKP = 97.075;


utm_x = arrayfun(@(x) x.utm.x, GPSData);
utm_y = arrayfun(@(x) x.utm.y, GPSData);
ant_d = abs((utm_x+1j*utm_y) - (AntX+1j*AntY)); % distance to the antenna

gps_t = arrayfun(@(x) x.time.day_seconds, GPSData); % time in seconds
gps_t = gps_t - min(gps_t);

gps_utm = [utm_x utm_y]; %GPS UTM coordinates
gps_kps = zeros(1,size(gps_utm,1)); %GPS kilometric points
for ii=2:size(gps_utm,1)
    gps_kps(ii)=gps_kps(ii-1)+sqrt(sum((gps_utm(ii,:)-gps_utm(ii-1,:)).^2));
end

[~,MinIndex] = min(ant_d);
gps_kps = gps_kps-gps_kps(MinIndex);
if(ston)
    gps_kps = -gps_kps;
end
gps_kps = (gps_kps + AntKP*1000)/1000;


PK = interp1(gps_t, gps_kps, t + max(gps_t) - max(t) + t_off);

if ston && invert_ston
    PK = PK(end:-1:1);
end

end
