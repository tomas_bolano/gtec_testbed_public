function  GPSData = getGPSData(RutaGPSLog)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%#ok<*AGROW>

RutaActual = [fileparts(mfilename('fullpath')) filesep];
addpath(RutaActual);
 
fileID = fopen(RutaGPSLog);
GPSLog = fread(fileID,'*char')';
fclose(fileID);

GPSLogLines = strsplit(GPSLog, '\n');

% Get $GPRMC lines
GPRMCLines = {};
for ii = 1:length(GPSLogLines)
    if length(GPSLogLines{ii}) >= 6 && ...
       strcmp(GPSLogLines{ii}(1:6), '$GPRMC')
        GPRMCLines{end+1} = GPSLogLines{ii};
    end
end

% Get processed GPS data
GPSData = {};
for ii = 1:length(GPRMCLines)
    GPSData{ii} = TECRAIL_lte_RX_ADIF_GPS_NMEA_Analysis(GPRMCLines{ii});
end

% Concatenate GPS data into struct
GPSData = cat(1,GPSData{:});

end

