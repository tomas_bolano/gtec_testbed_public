%% Inicializacións básicas

clear variables;
close all;
format long g
restoredefaultpath;
clc;

%% Ruta do script

RutaActual = [fileparts(fileparts(fileparts(mfilename('fullpath')))) filesep];


%% Ruta das medidas
MeasurementsFolder = [filesep 'media' filesep 'tecrail' filesep 'measData4' filesep];


%% Nome do ficheiro a procesar
%samplesFilename = '2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.nosplit.usrp.usrpnmea';
%samplesFilename = '2014-12-10--03-55_indoor_etcs_voip_100kmh_n_to_s.nosplit.usrp.usrpnmea';
samplesFilename = '2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp.usrpnmea';
%samplesFilename = '2014-12-10--04-24_indoor_etcs_voip_200kmh_n_to_s.nosplit.usrp.usrpnmea';
%samplesFilename = '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.nosplit.usrp.usrpnmea';
%samplesFilename = '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.nosplit.usrp.usrpnmea';
%samplesFilename = '2014-12-11--00-06_max_throughput_50kmh_n_to_s.nosplit.usrp.usrpnmea';


%% Obter velocidade en km/h do nome de ficheiro
kmh_ii = strfind(samplesFilename, 'kmh');
VelocidadeKmh = samplesFilename(find(samplesFilename(1:kmh_ii)=='_',1,'last')+1:kmh_ii-1);
VelocidadeKmh = str2num(VelocidadeKmh);


%% Definición de variables básicas dependentes do caso de estudio:

addpath([RutaActual 'LTEFDD_PHY/'])
addpath([RutaActual 'LTEFDD_PHY' filesep 'GPS_Processing'])


%% Definicións de parámetros dependentes do caso de estudio
% Coordenadas UTM da antena
AntX = 347077.9;
AntY = 4103747.4;
% Kilometric point of the antenna
AntKP = 97.075;

%% Get GPS data
RutaGPSLog = [MeasurementsFolder samplesFilename];

tic();
GPSData = getGPSData(RutaGPSLog);
toc();


%% Plot trajectory
figure();

subplot(5,1,1);
utm_x = arrayfun(@(x) x.utm.x, GPSData);
utm_y = arrayfun(@(x) x.utm.y, GPSData);

plot(utm_y, utm_x, '.-');
daspect([1 1 1]);
grid on;
xlabel('UTM Y-coordinate [m]')
ylabel('UTM X-coordinate [m]')
title(sprintf('trajectory (case of v=%d km/h)', VelocidadeKmh));
%title(['trajectory (' samplesFilename ')']);
hold on;
plot(AntY,AntX,'xr');
%legend('trajectory', 'antenna');
hold off;

%% Plot speed
subplot(5,1,2);
v = arrayfun(@(x) x.groundspeed.kmh, GPSData); % speed in km/h
t = arrayfun(@(x) x.time.day_seconds, GPSData); % time in seconds
t = t - min(t);

plot(t, v, '.-');
grid on;
xlabel('time [s]');
ylabel('inst. speed [km/h]');
title(sprintf('instantaneous speed (case of v=%d km/h)', VelocidadeKmh));


%% Plot distance to the antenna
subplot(5,1,3);
ant_d = abs((utm_x+1j*utm_y) - (AntX+1j*AntY)); % distance to the antenna

% distance to the antenna with sign
ant_d2 = abs((utm_x+1j*utm_y) - (AntX+1j*AntY)).*...
             sign(-imag((utm_x+1j*utm_y) - (AntX+1j*AntY)));
plot(t, ant_d, '.-');
grid on;
xlabel('time [s]');
ylabel('dist. to antenna [m]');
title(sprintf('distance to the antenna (case of v=%d km/h)', VelocidadeKmh));


%% Plot trajectory + speed
subplot(5,1,4);
surface('YData', [utm_x utm_x],             ...
        'XData', [utm_y utm_y],             ...
        'ZData', [v v], ...
        'CData', [v v],             ...
        'FaceColor', 'none',        ...
        'EdgeColor', 'interp',      ...
        'Marker', 'none');
grid on;
colorbar;
colormap('jet')
daspect([1 1 1]);
xlabel('UTM Y-coordinate [m]')
ylabel('UTM X-coordinate [m]')
title(sprintf('trajectory and instantaneous speed (case of v=%d km/h)', VelocidadeKmh));
hold on;
plot(AntY,AntX,'xr');
%legend('trajectory', 'antenna');
hold off;
%caxis([100 200]);



%% Plot distance on the track

subplot(5,1,5)

vPosition = [utm_x utm_y];
vDistance = zeros(1,size(vPosition,1));
for ii=2:size(vPosition,1)
    vDistance(ii)=vDistance(ii-1)+sqrt(sum((vPosition(ii,:)-vPosition(ii-1,:)).^2));
end

[~,MinIndex]=min(ant_d);
vDistance = (vDistance-vDistance(MinIndex)+AntKP*1000)/1000;

plot(t,vDistance)
hold on
plot([t(MinIndex) t(MinIndex)],[vDistance(MinIndex) vDistance(MinIndex)],'xr')
hold off
grid on
xlabel('time [s]');
ylabel('inst. Kilometric Point [km]');
title(sprintf('instantaneous Kilometric Point (case of v=%d km/h)', VelocidadeKmh));