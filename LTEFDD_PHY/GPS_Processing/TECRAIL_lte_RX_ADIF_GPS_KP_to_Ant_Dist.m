function AntDistancesOut = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(KilometricPointsIn, use_sign)
% Distance to the antenna from kilometric points
%
% If use_sign (default = false), the distance will have sign,
% Distances of the south sectors will be positive,
% distances of the north sectors will be negative

if nargin < 2
    use_sign = false;
end

load('TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist_data')

% A small error is present on the kilometric points calculated, because
% the point of the minimum distance with the antenna is assigned to
% the kilometric point of the antenna. To fix this problem we introduce
% a correction factor of 0.035
FactorCorreccion = 0.035;
AntKP = 97.075 + FactorCorreccion;
KilometricPointsIn = KilometricPointsIn + FactorCorreccion;

[AntennaDistance, ia] = unique(AntennaDistance); %#ok<NODEF>
KilometricPoint = KilometricPoint(ia); %#ok<NODEF>

AntDistancesOut = interp1(KilometricPoint,AntennaDistance,KilometricPointsIn,'spline');

if use_sign 
    AntDistancesOut = AntDistancesOut.*((KilometricPointsIn >= AntKP)*2-1);
end

end