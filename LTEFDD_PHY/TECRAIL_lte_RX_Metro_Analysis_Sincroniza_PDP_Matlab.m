%% Inicializacións básicas

clear variables;
close all;
format long g
restoredefaultpath;
clc;

%% Definición de variables básicas dependentes do caso de estudio:

addpath('/home/tecrail/gtisrc/LTEFDD_PHY/')
TECRAIL_lte_RX_Metro_Analysis_Parametros_outoords_ch0_mode00;

%% Inicializacións avanzadas

% >> Engado partes necesarias do simulador:

addpath('/home/tecrail/gtisrc/gtis/matlab/usrpfileformat/')
addpath('/home/tecrail/gtisrc/gtis/matlab/utils/')
addpath('/home/tecrail/gtisrc/gtis/examples/lte/')

% >> Engado partes necesarias para o salvagardado de datos:

RutaResultados='/home/tecrail/gtisrc/5G/common/ArtigoCapacidadeFerroviario_AWPL2016/MetroResults/';
addpath(RutaResultados);

% >> Variables para a lectura de datos:

basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData4','metro','outdoors','2014-09-19--plainLTEMatlabToolbox-Mode00','1x1LTEMode00');
% basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData4','metro','outdoors','2014-09-19--interpolatedLTETUWienSimulator-Mode02','1x1LTEMode02');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles, samplesFilename);

% >> Definición de constantes para as medidas

Ntx = 2;
Nrx = 2;

Sector = 0:5;

% >> Definición de constantes para LTE (10 MHz)
nSubframesPerFrame = 10;
nSamplesSlot = 15360/2;
nSymbolsSlot = 14/2;
nSamplesSubframe = nSamplesSlot * 2;
nSymbolsSubframe = nSymbolsSlot * 2;
nSamplesFrame = nSamplesSubframe * nSubframesPerFrame;
nSymbolsFrame = nSymbolsSubframe * nSubframesPerFrame;
nHalfSamplesSlot = nSamplesSlot / 2;
nFFT = 2048/2;
usedSubcarriers=600;
nGuardSubcarriers=(nFFT-usedSubcarriers)/2;
windowLength = 40/2;
longCPlength = nFFT*10/128;
shortCPlength = nFFT*9/128;
longOFDMsymbolLength = nFFT + longCPlength;
shortOFDMsymbolLength = nFFT + shortCPlength;
NsymbSlot = 7;
fs=15.36e6;
enb.NDLRB=50;
enb.CyclicPrefix='Normal';
enb.DuplexMode='FDD';

initSampleSymbolsInLteSlot = cumsum([0 longOFDMsymbolLength shortOFDMsymbolLength*ones(1,nSymbolsSlot-2)]);
initSampleSymbolsInLteSubframe = [initSampleSymbolsInLteSlot nSamplesSlot+initSampleSymbolsInLteSlot];
initSampleSymbolsInLteFrame = [];
for ii=1:nSubframesPerFrame
    initSampleSymbolsInLteFrame=[initSampleSymbolsInLteFrame (ii-1)*nSamplesSubframe+initSampleSymbolsInLteSubframe]; %#ok<AGROW>
end

enb.CellRefP = 1;               % One transmit antenna port
enb.LookForAllNCellIDs = false;
enb.Ng = 'One';
enb.CFI = 2;
enb.PHICHDuration = 'Normal';

% >> Constantes dependentes do caso de análise

mostras=nSamplesFrame*TramasPorAnalise;
m0=t0*fs+DesprazamentoInicial-nSamplesSubframe;
mf=tf*fs;
Indices = m0:nSamplesFrame*TramasSeparacion:mf;
% enb.NCellID=Sector(NumeroSector);

% >> Preparación para o salvagardado de datos:

if(exist([IdentificadorResultados '.mat'],'file'))
    load(IdentificadorResultados);
    disp('Results will be overwritten...');
    disp('Press any key to proceed or Ctrl+C to cancel...');
    pause;
else
    PuntosSincronizacion = zeros(1,length(Indices));
    PDPs = zeros(MostrasPDP,length(Indices));
    estChannels = zeros(usedSubcarriers,nSymbolsFrame,length(Indices));
    IndicesCela = zeros(1,length(Indices));
    Indice=0;
    potenciaDatos=zeros(nSymbolsFrame,length(Indices));
    potenciaRuido=zeros(nSymbolsFrame,length(Indices));
    save([RutaResultados IdentificadorResultados],'PuntosSincronizacion','IndicesCela','Indice','estChannels','PDPs','potenciaDatos','potenciaRuido');
end

%% Análise dos datos

tic;

for Indice = 1:length(Indices)
    
    mi = Indices(Indice);
    eltime = toc;
    fprintf('\n[%6.2f %%] Processing data... (Elapsed time: %5.2f seconds)\n',100*Indice/length(Indices),eltime);

    % >> Obteño información do ficheiro

    %fileinfo = GTIS_getInfoFromUSRPMultichannelSignalsFile(rxSignalsInUSRPFormatFilename);

    % >> Lectura do ficheiro
    
    [readSignalComplex, success, ...
        totalNumSamplessProcessedPerChannel, ...
        totalNumBytesProcessedPerChannel, ...
        numChannels] = GTIS_readUSRPMultichannelSignalsFromFile(...
        rxSignalsInUSRPFormatFilename, ...
        mostras, ...
        mi); % Procesarei a canle 0 disto...

    rxSamples = readSignalComplex(Canle,1:end);

%     GTIS_spectrum(rxSamples(1,:), fs);

    N_id_cell=[];
    f_start_idx=[];

    if(ProcesamentoTestbed==1)
        
        [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSamples);

        % Search for PSS and find fine timing offset
        [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
        if(pss_symb ~= 0)
            fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
        else
            fprintf('ERROR: Did not find PSS\n');
            return;
        end

        % SSS
        [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
        if f_start_idx ~= 0
            while(f_start_idx < 0)
                f_start_idx = f_start_idx + nSamplesFrame;
            end
            fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
        else
            fprintf('ERROR: Did not find SSS\n');
        end

        % Construct N_id_cell
        N_id_cell = 3*N_id_1 + N_id_2;
        fprintf(' N id cell = %d\n', N_id_cell);
        
    end
    
    if(ProcesamentoMatlab==1)
        
        % Estima e corrección do offset de frecuencia
            
        fprintf('\nPerforming frequency offset estimation...\n');
        foffset = lteFrequencyOffset(enb, rxSamples.');
        fprintf('Frequency offset: %0.3fHz\n',foffset);
        rxSamples = lteFrequencyCorrect(enb,rxSamples.',foffset);
        
        % Detección da identidade de cela e estima do offset en tempo
        
        fprintf('\nPerforming cell search...\n');
        [N_id_cell, f_start_idx] = lteCellSearch(enb, rxSamples, Sector(NumeroSector));
        fprintf(' N id cell = %d\n', N_id_cell);
        fprintf('0 index is %u, cell_id is %u\n\n',f_start_idx,N_id_cell);
        
    end
    
    if((f_start_idx>0)&&((1+f_start_idx+nSamplesFrame-1)<=(TramasPorAnalise*nSamplesFrame)))

        if(ProcesamentoCanle==1)

            fprintf('\nPerforming time synchronization...\n')

            enb.NCellID = N_id_cell;

            fprintf('Timing offset to frame start: %d samples\n',f_start_idx);
            rxSamples = rxSamples(1+f_start_idx+(0:(nSamplesFrame-1)),:);
            
            fprintf('\nSNR estimation...\n')
            
            for ii=1:nSymbolsFrame
                rxSymbolSamples = rxSamples((1:1024)+initSampleSymbolsInLteFrame(ii));
                rxSubcarriersPadded = circshift(fft(1/sqrt(1024)/(sqrt(1024/600))*rxSymbolSamples),[300,0]);
                rxSubcarriers = rxSubcarriersPadded([1:300,302:601]).';
                potenciaDatos(ii,Indice)=(rxSubcarriers*(rxSubcarriers'))/(length(rxSubcarriers)^2);
                guardSubcarriers = rxSubcarriersPadded(602:end);
                noiseSubcarriers = guardSubcarriers(MostrasSeparacionGardasRuido:(end-MostrasSeparacionGardasRuido)).';
                potenciaRuido(ii,Indice) = (noiseSubcarriers*(noiseSubcarriers'))/(length(noiseSubcarriers)^2);
%                 snr = 10*log10((potenciaDatos(ii,Indice)-potenciaRuido(ii,Indice))/potenciaRuido(ii,Indice))
            end
            

            fprintf('\nOFDM demodulation...\n')

            rxGrid = sqrt(usedSubcarriers)/nFFT*lteOFDMDemodulate(enb, rxSamples);
            
            
%             helperPlotTransmitResourceGrid(enb,rxGrid);

            enb.NSubframe = 0;
            fprintf('\nEstimating channel...\n')
            [estChannel, noiseEst] = lteDLChannelEstimate(enb, cec, rxGrid);

            estChannels(:,:,Indice)=estChannel;

    %         figure('Color','w');
    %         helperPlotChannelEstimate(estChannel);
    %         shading flat;

            if(ProcesamentoPDP==1)

                fprintf('\nEvaluating PDP...\n')

                extendedEstChannel = [zeros(nGuardSubcarriers,nSymbolsFrame);...
                    estChannel;zeros(nGuardSubcarriers,nSymbolsFrame)];
                extendedEstChannel = ifftshift(extendedEstChannel,1);
                timeResponses = sqrt(1024)*ifft(extendedEstChannel);
                timeResponses = timeResponses(1:MostrasPDP,:);
                pdp=diag(abs(timeResponses*(timeResponses')))/nSymbolsFrame;

                PDPs(:,Indice)=pdp;

    %             figure,stem(1e6*(0:(MostrasPDP-1))/fs,pdp(1:MostrasPDP)/max(pdp))
    %             title('normalized power delay profile')
    %             xlabel('delay [\mus]'),ylabel('normalized magnitude')
    %             grid on
    %             hold on
    %             plot(1e6*(0:(MostrasPDP-1))/fs,pdp(1:MostrasPDP)/max(pdp),'r:')

            end

    %         pause;

        end

    end
    
    PuntosSincronizacion(Indice)=f_start_idx;
    IndicesCela(Indice)=N_id_cell;
    
    if(GardadoPorIteracion==1)
        save([RutaResultados IdentificadorResultados],'PuntosSincronizacion','IndicesCela','Indice','estChannels','PDPs','potenciaDatos','potenciaRuido');
    end
    
end

if(GardadoPorIteracion==0)
    save([RutaResultados IdentificadorResultados],'PuntosSincronizacion','IndicesCela','Indice','estChannels','PDPs','potenciaDatos','potenciaRuido');
end