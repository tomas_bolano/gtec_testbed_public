%% Inicializacións básicas

clear variables;
close all;
format long g
restoredefaultpath;
clc;

%% Definición de variables básicas dependentes do caso de estudio:

addpath('/home/tecrail/gtisrc/LTEFDD_PHY/')
TECRAIL_lte_RX_Metro_SeleccionDatos_Parametros_ot_ch0_mode00;

%% Inicializacións avanzadas

% >> Engado partes necesarias para o salvagardado de datos:

RutaResultados='/home/tecrail/gtisrc/5G/common/ArtigoCapacidadeFerroviario_AWPL2016/MetroResults/';
addpath(RutaResultados);
load(ResultsFilename);

%% Procesamento dos datos

% figure,stem(IndicesCela)
% figure,stem(PuntosSincronizacion)

IndicesValidos = ones(1,length(IndicesCela));

% Elimino os valores fóra do sector correcto
IndicesValidos(IndicesCela~=Sectores(1))=0;

% Elimino os valores con sincronización incorrecta

Moda=mode(PuntosSincronizacion);

IndicesInvalidos=find(abs(PuntosSincronizacion-Moda)>MarxeSincronizacion);

IndicesValidos(IndicesInvalidos)=0;

EixoIndices=1:length(IndicesCela);

EixoIndicesValidos=EixoIndices(logical(IndicesValidos));
PuntosSincronizacionValidos=PuntosSincronizacion(logical(IndicesValidos));
IndicesCelaValidos=IndicesCela(logical(IndicesValidos));

% figure,stem(EixoIndicesValidos,IndicesCelaValidos)
% figure,stem(EixoIndicesValidos,PuntosSincronizacionValidos)

%% Salvagardado de datos

save([RutaResultados ResultsFilename '_' IdentificadorFicheirosProcesados],'EixoIndicesValidos','PuntosSincronizacionValidos','IndicesCelaValidos')