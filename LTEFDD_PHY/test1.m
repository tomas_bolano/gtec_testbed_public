
%%
waveform = xvTXSamples(:,1)+1j*xvTXSamples(:,2);

%% For Matlab measurements (using fft)
n = 60;
rxGrid2 = fft(waveform(n+(1:1024))).*exp(1i*2*pi*(80-n)/1024*(0:1024-1).');
%rxGrid2 = rxGrid2.';

%% For Matlab measurements (2)
rxGrid2 = sqrt(usedSubcarriers)/nFFT*lteOFDMDemodulate(enb, waveform);

%%
figure();
plot(real(rxGrid2(:,1)), imag(rxGrid2(:,1)), '.');

%% For Vienna measurements ?
for ii = 25.5:0.01:48.5
    rxGrid2 = fft(waveform(n+(1:1024))).*exp(1i*2*pi*(ii)/1024*(0:1024-1).');
    plot(real(rxGrid2(:,1)), imag(rxGrid2(:,1)), '.');
    title(sprintf('%f',ii));
    drawnow();
    pause(0.000005)
end