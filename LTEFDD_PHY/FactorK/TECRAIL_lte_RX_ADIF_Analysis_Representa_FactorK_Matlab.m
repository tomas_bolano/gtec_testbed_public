%% Inicializacións básicas

clearvars -except FrequencyOffset Indice IndicesCela PuntosSincronizacion estChannels;
close all;
format long g
restoredefaultpath;
clc;

%% Rutas
RutaRepositorio = [fileparts(fileparts(fileparts(mfilename('fullpath')))) filesep];
RutaLtePhy = [RutaRepositorio 'LTEFDD_PHY' filesep];
addpath(RutaLtePhy);
addpath([RutaLtePhy 'FactorK/'])
addpath([RutaLtePhy 'FactorK/Parametros'])
addpath([RutaLtePhy 'GPS_Processing'])
addpath([RutaRepositorio 'gtis/matlab/usrpfileformat/'])
addpath([RutaRepositorio 'gtis/matlab/utils/'])
addpath([RutaRepositorio 'gtis/examples/lte/'])
RutaResultados=[RutaRepositorio '5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/'];
addpath(RutaResultados);
MeasurementsFolder = fullfile(filesep, 'media', 'tecrail', 'measData4',filesep);
%MeasurementsFolder = '/home/tomas/extra/GTEC_5G_Simulator_Results/ArtigoCapacidadeFerrovario_AWPL2016/Results/';
%addpath(MeasurementsFolder);


%% Configuración calculo factor K
kfactor_tapind = 1; % tap index to use
kfactor_normalize = 0; % normalize channel tap used
kfactor_noisedb = -inf;  % noise power in DB for normalized tap (recommended 35 ~ 40)
kfactor_interpguards = 0;  % complete the channel in the guards using interpolation


%% Parámetros
TECRAIL_lte_RX_ADIF_Analysis_FactorK_Parametros_v100_ch0_out;

%% Inicializacións avanzadas
    
% >> Engado partes necesarias para o salvagardado e lectura de datos:

% >> lectura de datos:
if exist('estChannels', 'var')
    fprintf('estChannels already in workspace, skipping loading data\n');
else
    fprintf('loading data... ');
    load(IdentificadorResultados);
    fprintf('OK\n');
end

% >> Constantes propias do caso de estudio
ts = 1/(15.36e6);
fc=2.6e9;
fd = VelocidadeKmh*fc/(3.6*3e8);

% >> Definición de constantes para LTE (10 MHz)
nSamplesSlot = 15360/2;
nSymbolsSlot = 14/2;
nSamplesSubframe = nSamplesSlot * 2;
nSymbolsSubframe = nSymbolsSlot * 2;
nSamplesFrame = nSamplesSubframe * 10;
nSymbolsFrame = nSymbolsSubframe * 10;
nHalfSamplesSlot = nSamplesSlot / 2;
nFFT = 2048/2;
usedSubcarriers=600;
guardSubcarriers=(nFFT-usedSubcarriers)/2;
windowLength = 40/2;
longCPlength = nFFT*10/128;
shortCPlength = nFFT*9/128;
longOFDMsymbolLength = nFFT + longCPlength;
shortOFDMsymbolLength = nFFT + shortCPlength;
NsymbSlot = 7;
fs=15.36e6;

KP2=97.075;

%% Eliminación de tramas adquiridas incorrectamente

IndicesValidos = ones(1,length(IndicesCela));

% Filtro por punto de sincronización así como por índice de cela

% >> Busco o punto de cambio dun sector a outro

for kk=1:(length(IndicesCela)-UmbralLonxitudeTraspaso)
    if(IndicesCela(kk+(0:(UmbralLonxitudeTraspaso-1)))==...
            Sectores(2)*ones(1,UmbralLonxitudeTraspaso))
        ValorTraspaso=kk;
        break;
    end
end

IndicesInvalidos=find(IndicesCela(1:(ValorTraspaso-1))~=Sectores(1));

IndicesValidos(IndicesInvalidos)=0; %#ok<FNDSB>

IndicesInvalidos=find(IndicesCela(ValorTraspaso:end)~=Sectores(2))+ValorTraspaso-1;

IndicesValidos(IndicesInvalidos)=0;

Moda=mode(PuntosSincronizacion);

IndicesInvalidos=find(abs(PuntosSincronizacion-Moda)>MarxeSincronizacion);

IndicesValidos(IndicesInvalidos)=0;

EixoIndices=1:length(IndicesCela);

EixoIndicesValidos=EixoIndices(logical(IndicesValidos));
PuntosSincronizacionValidos=PuntosSincronizacion(logical(IndicesValidos));
IndicesCelaValidos=IndicesCela(logical(IndicesValidos));

%% Cálculo do Doppler Spectrum

factorK = zeros(1,length(EixoIndicesValidos)); 

fprintf('\nEvaluating K factor...\n')

for Indice = 1:length(EixoIndicesValidos)
    IndiceValido = EixoIndicesValidos(Indice);
    estChannel=estChannels(:,:,IndiceValido);
    
    ch = ifftshift(estChannel,1);
    chguards = zeros(423, size(ch,2));

    % interpolate guards
    if kfactor_interpguards
        for ii = 1:size(ch,2)
            chguards(:,ii) = interp1([1 423], ch([300 301], ii), 1:423, 'linear');
        end
    end
    chext = [(ch(1,:)+ch(end,:))/2; ch(1:300,:); chguards; ch(301:end,:)];
    timeResponses = sqrt(1024)*ifft(chext);
    
    tap = timeResponses(kfactor_tapind,:);
    if kfactor_normalize
        pwtap = sum(abs(tap).^2)/numel(tap);
        tap = tap.*(1/sqrt(pwtap));
    end
    
    % add noise
    if kfactor_normalize
        tap = tap + sqrt(10^(kfactor_noisedb/10)/2)*...
              (randn(size(tap)) + 1j*randn(size(tap)));
    else
    end
    
    
    %extendedEstChannel = [zeros(guardSubcarriers,nSymbolsFrame*(TramasPorAnalise-1));...
    %    estChannel;zeros(guardSubcarriers,nSymbolsFrame*(TramasPorAnalise-1))];
    %extendedEstChannel = ifftshift(extendedEstChannel,1);
    %timeResponses = sqrt(1024)*ifft(extendedEstChannel);
    
    % K factor estimation using the method of the Greenstein paper
    g = tap;
    
    Ga = mean(abs(g).^2);
    Gv = sqrt(mean((abs(g).^2 - Ga).^2));
    V2 = sqrt(Ga.^2 - Gv.^2);
    s2 = Ga - V2;
    
    factorK(Indice) = V2/s2;
end

%% Representación gráfica

hh=figure();
t = ts*TramasSeparacion*140*(1024+(6*72+80)/7)*(EixoIndicesValidos-1);

% obtencion puntos kilometricos equivalentes para instantes temporais
FactorDesalinhamento = 0;
RutaGPSLog = [MeasurementsFolder samplesFilename '.usrpnmea'];
GPSData = getGPSData(RutaGPSLog);
puntosKmtsD = getGPSKP(GPSData, 0, t,getEndFileOffsetGPSKP(GPSData, tfgps), FactorDesalinhamento);

plot(puntosKmtsD, 10*log10(factorK));
hold on;
grid on;
plot(puntosKmtsD, smooth(10*log10(factorK), 7), '-', 'LineWidth', 3);
ylabel('K-Factor [dB]');
xlabel('kilometric point [km]');
title(['K-Factor at ' sprintf('%d',VelocidadeKmh) ' km/h'])
xlim([94.39 101.03]) % Valores a man para que concorden nas dúas gráficas (100 e 200 km/h)
ax=axis();
plot([KP2 KP2],[ax(3) ax(4)],'r--')
legend('instantaneous K-Factor values',...
       'averaged K-Factor', ...
       'eNodeB position','location','southeast');
set(findall(gcf,'-property','FontSize'),'FontSize',12);
% print(hh,'-dpdf','-r600',['FactorK' sprintf('%d',VelocidadeKmh) 'kmh.pdf'])