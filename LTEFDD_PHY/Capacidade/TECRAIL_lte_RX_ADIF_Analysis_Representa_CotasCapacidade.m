%% Inicializacións básicas

clear variables;
close all;
format long g
restoredefaultpath;
clc;

%% Definición de variables básicas dependentes do caso de estudio:

addpath('/home/tecrail/gtisrc/LTEFDD_PHY/')
TECRAIL_lte_RX_ADIF_CalculoCapacidade_Parametros_100_0_f;

RutaActual = [fileparts(fileparts(mfilename('fullpath'))) filesep];
addpath([RutaActual 'LTEFDD_PHY/'])
addpath([RutaActual 'LTEFDD_PHY' filesep 'GPS_Processing'])

%% Inicializacións avanzadas

% >> Constantes

fs=15.36e6;
    
% >> Engado partes necesarias para o salvagardado e lectura de datos:

RutaResultados='/home/tecrail/gtisrc/5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/';  
addpath(RutaResultados);
load(ResultsFilename);
load([ResultsFilename(1:(end-4)) '_' IdentificadorFicheirosProcesados ResultsFilename((end-3):end)])
load([ResultsFilename(1:(end-4)) '_' IdentificadorFicheirosCapacidade ResultsFilename((end-3):end)])

%% Representacións

PintaIntervalosConfianza = @(x,lower,upper,color) set(fill([x,x(end:-1:1)],[upper,lower(end:-1:1)],color),'EdgeColor',color,'FaceAlpha',.15,'EdgeAlpha',.4);

styles={'r','m','b'};

figure;

% obtencion puntos kilometricos equivalentes para instantes temporais
FactorDesalinhamento = 0;
RutaGPSLog = [MeasurementsFolder samplesFilename '.usrpnmea'];
GPSData = getGPSData(RutaGPSLog);
DesprazamentoFinalPorIndicesNonValidos = EixoIndicesValidos(end)-length(IndicesCela);
Eixot = (1/fs)*TramasSeparacion*140*(1024+(6*72+80)/7)*(EixoIndicesValidos-1);
puntosKmtsD = getGPSKP(GPSData, 0, Eixot,getEndFileOffsetGPSKP(GPSData, tfgps+DesprazamentoFinalPorIndicesNonValidos), FactorDesalinhamento);

PintaIntervalosConfianza(puntosKmtsD,8*Capacidade(1,:),8*Capacidade(2,:),styles{1});

xlim([puntosKmtsD(1) puntosKmtsD(end)])

grid on
title('capacity bounds')
xlabel('kilometric point [km]')
ylabel('capacity [Mbit/s]')