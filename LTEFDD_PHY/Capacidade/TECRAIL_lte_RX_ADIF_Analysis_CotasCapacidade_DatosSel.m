%% Inicializacións básicas

clear variables;
close all;
format long g
restoredefaultpath;
clc;

%% Definición de variables básicas dependentes do caso de estudio:

addpath('/home/tecrail/gtisrc/LTEFDD_PHY/')
TECRAIL_lte_RX_ADIF_CalculoCapacidade_Parametros;

%% Inicializacións avanzadas

% >> Constantes

c = 3e8;
fc = 2.4e9;
vSI = Velocidade*1000/3600;
fd = vSI*fc/c;

% >> Definición de constantes para LTE (10 MHz)
nSamplesSlot = 15360/2;
nSymbolsSlot = 14/2;
nSamplesSubframe = nSamplesSlot * 2;
nSymbolsSubframe = nSymbolsSlot * 2;
nSamplesFrame = nSamplesSubframe * 10;
nSymbolsFrame = nSymbolsSubframe * 10;
nHalfSamplesSlot = nSamplesSlot / 2;
nFFT = 2048/2;
usedSubcarriers=600;
guardSubcarriers=(nFFT-usedSubcarriers)/2;
windowLength = 40/2;
longCPlength = nFFT*10/128;
shortCPlength = nFFT*9/128;
longOFDMsymbolLength = nFFT + longCPlength;
shortOFDMsymbolLength = nFFT + shortCPlength;
NsymbSlot = 7;
fs=15.36e6;
enb.NDLRB=50;
enb.CyclicPrefix='Normal';
enb.DuplexMode='FDD';

timeInstants = [0;1024+80;1024+80+(1024+72)*1;1024+80+(1024+72)*2;...
        1024+80+(1024+72)*3;1024+80+(1024+72)*4;1024+80+(1024+72)*5;...
        1024+80+(1024+72)*6;1024+80+(1024+72)*6 + 1024+80;...
        1024+80+(1024+72)*7 + 1024+80;1024+80+(1024+72)*8 + 1024+80;...
        1024+80+(1024+72)*9 + 1024+80;1024+80+(1024+72)*10 + 1024+80;...
        1024+80+(1024+72)*11 + 1024+80]/15.36e6;
timeInstants = timeInstants(1:12);
covarianceTime = besselj(0, 2*pi*fd*timeInstants);
Ct = toeplitz(covarianceTime);

% >> Prexeración de constantes dependentes da estructura de pilotos

enb.CyclicPrefix = 'Normal';
enb.NDLRB        = 50;
enb.CellRefP     = 1;
enb.NCellID      = 3;
enb.NSubframe    = 0;
antPort          = 0;
enb.DuplexMode = 'FDD';         % FDD

resourceGrid = lteDLResourceGrid(enb);
ind = lteCellRSIndices(enb,antPort);
rs = lteCellRS(enb,antPort);
resourceGrid (ind) = 1;
Npilots = length(ind(:));

% >> Prexeración de constantes para a covarianza en frecuencia

[kk, ll] = meshgrid(0:nFFT-1, 0:nFFT-1);
F = 1/sqrt(nFFT)*fftshift(exp(-1j*2*pi*kk.*ll/nFFT), 1);
Fl = F(213:end-212, 1:MostrasPDP);
    
% >> Engado partes necesarias para o salvagardado e lectura de datos:

RutaResultados='/home/tecrail/gtisrc/5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/';
addpath(RutaResultados);
load(ResultsFilename);
load([ResultsFilename(1:(end-4)) '_' IdentificadorFicheirosProcesados ResultsFilename((end-3):end)])

Capacidade = zeros(2,length(EixoIndicesValidos));

CapacityResultsFilename = [ResultsFilename(1:(end-4)) '_' IdentificadorFicheirosCapacidade ResultsFilename((end-3):end)];

if(exist(CapacityResultsFilename,'file'))
    load(CapacityResultsFilename);
    disp('Results will be overwritten...');
    disp('Press any key to proceed or Ctrl+C to cancel...');
    pause;
else
    save([RutaResultados CapacityResultsFilename],'Capacidade');
end

%% Procesamento dos datos

xTest = 1/sqrt(2)*(randn(1, 10000)+1j*randn(1, 10000));

tic

for kk=1:length(EixoIndicesValidos)
    
    eltime = toc;
    fprintf('\n[%6.2f %%] Processing data... (Elapsed time: %5.2f seconds)\n',100*kk/length(EixoIndicesValidos),eltime);
    
    % >> Cómputo da covarianza en frecuencia
    
    elementosEstChannel = estChannels(:,:,EixoIndicesValidos(kk));
    varianceChannel = var(elementosEstChannel(:));
    pdpNormalizado = PDPs(:,EixoIndicesValidos(kk))/max(max(PDPs(:,EixoIndicesValidos(kk))));
    Cf = Fl*diag(pdpNormalizado)*Fl'; % TODO: Comprobar se realmente queremos usar o PDP normalizado
    Cf = varianceChannel*Cf/Cf(1,1);
    
    % >> Cómputo da covarianza total
    
    Cfull = kron(Ct, Cf);
    Cfpfp = Cfull(ind, ind);
    Cffp = Cfull(:, ind);
    
    sigmaICI = Cf(1,1)*power_ici(fd, nFFT, fs, 'jakes');
    SNRefectiva = 50;
    sigmaN = Cf(1,1)*10.^(-SNRefectiva/10); % TODO: arranxar isto!
    
    Ce = Cfull - Cffp/(Cfpfp + (sigmaN + sigmaICI)*eye(Npilots))*Cffp';

    sigmaE = real(1/(usedSubcarriers*12)*trace(Ce));
    SNR= 10*log10(real(trace(Cfull)/(8400*(sigmaE+sigmaN + sigmaICI))));
    
    Capacidade(1,kk) = real(mean(log2(1 + (sum(abs(elementosEstChannel(:)).^2))/(sigmaE+sigmaN + sigmaICI)*abs(elementosEstChannel(:)).^2))); 
    Capacidade(2,kk) = Capacidade(1,kk) + ...
        real(mean(log2( (sigmaN+sigmaICI+sigmaE) ./ (sigmaN+(sigmaICI+sigmaE)*abs(xTest).^2) )));
    
end

%% Salvagardado dos datos

save([RutaResultados CapacityResultsFilename],'Capacidade');

%%

% % % % % % % % 
% % % % % % % % 
% % % % % % % % %% Representación de exemplo
% % % % % % % % 
% % % % % % % % % for kk=1:size(PDPs,2)
% % % % % % % % %     PDPs(:,kk)=PDPs(:,kk)/max(PDPs(:,kk));
% % % % % % % % % end
% % % % % % % % 
% % % % % % % % eixoy = ((0:71)*(1/15.36e6))*1e6;
% % % % % % % % eixox = EixoIndicesValidos;
% % % % % % % % PDPsNormalizados = PDPs(:,EixoIndicesValidos)/max(max(PDPs(:,EixoIndicesValidos)));
% % % % % % % % PDPsRecortados = PDPsNormalizados;
% % % % % % % % PDPsRecortados(PDPsRecortados<(10^(-MaximoRangoDBsRepresentacion/10)))=10^(-MaximoRangoDBsRepresentacion/10);
% % % % % % % % figure,surf(eixox,eixoy,10*log10(PDPsRecortados))
% % % % % % % % shading interp
% % % % % % % % view([0 90])
% % % % % % % % colorbar
% % % % % % % % title('normalized power delay profile [dB]'),xlabel('time [s]'),ylabel('relative delay [\mus]')
% % % % % % % % xlim([1 length(EixoIndicesValidos)]);
% % % % % % % % ylim([eixoy(1) eixoy(end)])
% % % % % % % % 
% % % % % % % % %% Miro de facer unha gráfica tendo en conta o retardo de propagación
% % % % % % % % 
% % % % % % % % MostrasRetardo = PuntosSincronizacionValidos-min(PuntosSincronizacionValidos);
% % % % % % % % eixoy = ((0:(71+max(MostrasRetardo)))*(1/15.36e6))*1e6;
% % % % % % % % PDPsRetardados = (10^(-MaximoRangoDBsRepresentacion/10))*ones(length(eixoy),size(PDPsRecortados,2));
% % % % % % % % PDPsRetardados(1:size(PDPsRecortados,1),1:size(PDPsRecortados,2))=PDPsRecortados;
% % % % % % % % for kk=1:length(EixoIndicesValidos)
% % % % % % % %     if(MostrasRetardo(kk)>0)
% % % % % % % %         PDPsRetardados(:,kk)=circshift(PDPsRetardados(:,kk),MostrasRetardo(kk));
% % % % % % % %         PDPsRetardados(1:MostrasRetardo(kk),kk)=(10^(-MaximoRangoDBsRepresentacion/10));
% % % % % % % %     end
% % % % % % % % end
% % % % % % % % figure,surf(eixox,eixoy,10*log10(PDPsRetardados))
% % % % % % % % shading interp
% % % % % % % % view([0 90])
% % % % % % % % colorbar
% % % % % % % % title('normalized power delay profile [dB]'),xlabel('time [s]'),ylabel('relative delay [\mus]')
% % % % % % % % xlim([1 length(EixoIndicesValidos)]);
% % % % % % % % ylim([eixoy(1) eixoy(end)])