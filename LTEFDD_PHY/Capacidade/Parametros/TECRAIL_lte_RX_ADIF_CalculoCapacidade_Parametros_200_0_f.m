ResultsFilename='Results_200kmh_ch0_outdoor.mat';
Velocidade=200;
IdentificadorFicheirosProcesados='Selected';
MostrasPDP = 72;
IdentificadorFicheirosCapacidade='Capacity';

MeasurementsFolder='/media/tecrail/measData4/';
samplesFilename='2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.nosplit.usrp';

t0=0; % Creo que un rango bo sería de 115 a 130 s
tf=200;
t0gps=145;
tfgps=345;

TramasSeparacion = 100;