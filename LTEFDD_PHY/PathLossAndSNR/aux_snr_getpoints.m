function [values, kmpoints, dist] = aux_snr_getpoints(...
    getfilef, getvaluef, getnoisef, gettimef, decimationFactor,...
    passnum, antnum, locationnum, invertDistance)
%AUX_SNR_DATA Summary of this function goes here
%   Detailed explanation goes here

offset = 0;
if(passnum == 2)
    offset = 3.25;
end

% read GPS data
gpsfile = [getfilef(passnum, antnum, locationnum) '.usrpnmea'];
GPSData = getGPSData(gpsfile);

% get time indexes and values
timeIndexes_0 = gettimef(passnum, antnum, locationnum);
values_0 = getvaluef(passnum, antnum, locationnum) - getnoisef(locationnum);

% decimate time indexes and values
timeIndexes = timeIndexes_0(1:decimationFactor:end);
values = values_0(1:decimationFactor:end);

% kilometric points
kmpoints = getGPSKP(GPSData, passnum == 2, timeIndexes, offset);

% distances
dist = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(kmpoints,1);

if invertDistance
    dist = -1*dist;
end

% check returned SNR values
% values not grater or equal than 0 are discarded
gt0 = values >= 0 & ~isnan(values);
values = values(gt0);
kmpoints = kmpoints(gt0);
dist = dist(gt0);

end

