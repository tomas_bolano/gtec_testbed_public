%% Inicializacións

clearvars -except valoresPotenciaDatos3Pasadas valoresPotenciaRuido
close all
format long g
clc


%% Dependencias
RutaActual = [fileparts(fileparts(((mfilename('fullpath'))))) filesep];
addpath([RutaActual 'AntennaModels' filesep]);
addpath([RutaActual 'GPS_Processing' filesep]);
addpath([RutaActual 'FilesTiming' filesep]);
addpath(RutaActual);
%addpath('/home/tecrail/EUCAP/r'); % Ruta cos ficheiros de resultados de potencia de datos e ruido
%MeasurementsFolder = [filesep 'media' filesep 'tecrail' filesep 'measData4' filesep];
MeasurementsFolder = '/home/tomas/extra/GTEC_5G_Simulator_Results/ArtigoCapacidadeFerrovario_AWPL2016/Results/';
addpath(MeasurementsFolder);

saveSNR = 0;


%% Carga datos
if ~exist('valoresPotenciaDatos3Pasadas', 'var')
    load('valoresPotenciaDatos3Pasadas.mat')
end;

if ~exist('valoresPotenciaRuido', 'var')
    load('valoresPotenciaRuido.mat')
end


%% Definición de constantes
KP1=93.4;
KP2=97.075;
KP3=102.0;
vel=50;

dant = 101;

% Parámetros comúns
hT = 20;
hR = 3.5;
tiltN = 0;
tiltS = 1.4;
bwT = 5;
azimuth = 5;
bwA = 65;

% Sector norte
lIN = KP2-((hT-hR)/tand(bwT))/1000;
lMN = inf;
lFN = inf;

% Sector sul
lIS = KP2+((hT-hR)/tand(bwT+tiltS))/1000;
lMS = KP2+((hT-hR)/tand(tiltS))/1000;
lFS = inf;

% Limitacións en azimuth
lIAN = (KP2*1000-tand(90-azimuth)*dant)/1000;
lMAN = (KP2*1000-tand(90-azimuth-bwA)*dant)/1000;
lIAS = (KP2*1000+tand(90-azimuth)*dant)/1000;
lMAS = (KP2*1000+tand(90-azimuth-bwA)*dant)/1000;

% Establezo uns límites de avaliación arbitrarios
lAN = 94.31;
lAS = 100.3;

UmbralRadioCela = 25;

RadiosCelas = zeros(2,4);
iRadiosCelas = 1;

estilos.canle(1).localizacion(2).estilo='r';
estilos.canle(2).localizacion(2).estilo='--';
estilos.canle(1).localizacion(1).estilo='g';
estilos.canle(2).localizacion(1).estilo='k--';

% Coordenadas UTM da antena
AntX = 347077.9;
AntY = 4103747.4;

% Path loss function
plf = @(x, xdata) 10*x(1)*log10(xdata) + x(2);

% GPS files
ficheirosPotenciaDatos3Pasadas.Pasada(1).Canle(1).Localizacion(2).filename='2014-12-11--00-06_max_throughput_50kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(1).Canle(2).Localizacion(2).filename='2014-12-11--00-06_max_throughput_50kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(1).Canle(1).Localizacion(1).filename='2014-12-11--00-06_max_throughput_50kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(1).Canle(2).Localizacion(1).filename='2014-12-11--00-06_max_throughput_50kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(2).Canle(1).Localizacion(1).filename='2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(2).Canle(2).Localizacion(1).filename='2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(2).Canle(1).Localizacion(2).filename='2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(2).Canle(2).Localizacion(2).filename='2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(3).Canle(1).Localizacion(1).filename='2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(3).Canle(2).Localizacion(1).filename='2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(3).Canle(1).Localizacion(2).filename='2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(3).Canle(2).Localizacion(2).filename='2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.nosplit.usrp';

ficheirosPotenciaDatosAltaVelocidade.Velocidade(1).Canle(1).Localizacion(1).filename='2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatosAltaVelocidade.Velocidade(1).Canle(2).Localizacion(1).filename='2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatosAltaVelocidade.Velocidade(1).Canle(1).Localizacion(2).filename='2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatosAltaVelocidade.Velocidade(1).Canle(2).Localizacion(2).filename='2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatosAltaVelocidade.Velocidade(2).Canle(1).Localizacion(1).filename='2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatosAltaVelocidade.Velocidade(2).Canle(2).Localizacion(1).filename='2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatosAltaVelocidade.Velocidade(2).Canle(1).Localizacion(2).filename='2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatosAltaVelocidade.Velocidade(2).Canle(2).Localizacion(2).filename='2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.nosplit.usrp';


%% Funcións relativas aos índices dos valores de ruido e datos
getNoiseIndex = @(IndiceValores)2-1*(IndiceValores==2);


%% Funcións para obtencion de valores e eixo temporal
getfilef = @(kPasada, kCanle, kLocalizacion)...
           ficheirosPotenciaDatos3Pasadas.Pasada(kPasada)...
           .Canle(kCanle).Localizacion(kLocalizacion).filename;
          
getvaluef = @(kPasada, kCanle, kLocalizacion)...
            valoresPotenciaDatos3Pasadas.Pasada(kPasada)...
            .Canle(kCanle).Localizacion(kLocalizacion).valoresMediaMobil;
         
getnoisef = @(kLocalizacion)...
            valoresPotenciaRuido...
           .ValorMedioRuidoPorLocalizacion(getNoiseIndex(kLocalizacion));
         
         
gettimef = @(kPasada, kCanle, kLocalizacion)...
           0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+...
           length(valoresPotenciaDatos3Pasadas.Pasada(kPasada)...
           .Canle(kCanle).Localizacion(kLocalizacion).valoresMediaMobil)...
            /((15.36e6)/1024));


%%
% decimation factor to use for improving the performance of the plots
% and reducing the size of the files when saving the figure
decimationFactor = 3000;


% The variables used will have a name of the form name_ABCN, where
% A can be 'n' or 's' to indicate the sector with the transmitting
% antena (nouth transmitting or south transmitting), B will be 'o' or
% 'i', to indicate if the data is for outdoors antennas or indoors and
% C will be 'n', 's' or 'x' to indicate the sector where the data was
% calulated (north south or 'x' if the data is valid for both). N is an
% optional number wich indicates the antenna number (1 or 2);
%
% Example:
% valores_son referes to values obtained in the north sector, for the
% outdoors antenna, with the south antenna transmitting.


%% Create cell of pass, location, id
% creates a cell with rows of format {pass num, location num, id}, where
% the id is of the form AB, where A is 'n' or 's' to indicate the
% sector of the antenna transmitting and B is 'o' or 'i' to indicate
% if is the outdoors or the indoors case

passlocid = ...
{3, 2, 'no';
 3, 1, 'ni';
 2, 2, 'so';
 2, 1, 'si'};


%% Calculate Path loss data

tic();
fprintf('Calculating values, distances and km points... ');
for passnum = 2:3
    for locnum = 1:2
        for antnum = 1:2
            A = ([passlocid{:,1}] == passnum & [passlocid{:,2}] == locnum);
            id = passlocid{A,3};
            idn = [id 'n'];
            ids = [id 's'];
            antfield = ['ant' num2str(antnum)];
            
            % North sector            
            [values.(idn).(antfield),...
             kmpoints.(idn).(antfield),...
             dist.(idn).(antfield)] =...
                aux_snr_getpoints(getfilef, getvaluef, getnoisef, gettimef,...
                                  decimationFactor, passnum, antnum, locnum, 1);

            % South sector
            % for the south we only invert the distances to make the distances
            % relatives to the south positive
            values.(ids).(antfield) = values.(idn).(antfield);
            kmpoints.(ids).(antfield) = kmpoints.(idn).(antfield);
            dist.(ids).(antfield) = -dist.(idn).(antfield);
        end
    end
end
fprintf('OK\n');
toc();


%% Create cell with the limit points to use to calculate the path loss
% the rows of the cell will have the format {id, ant num, point1, point2},
% where the id is as explained above.
gps2dist = @(dist,usesign) TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(dist,usesign);

pathlosslimpoints = {...
 'non', 1, 206, 2759;
 'non', 2, 206, 2759;

 'nos', 1, gps2dist(lMAS,1), gps2dist(lIAS,1);
 'nos', 2, gps2dist(lMAS,1), gps2dist(lIAS,1);
 
 'nin', 1, 206, 2759;
 'nin', 2, 206, 2759;
 
 'nis', 1, gps2dist(lMAS,1), gps2dist(lAS,1);
 'nis', 2, gps2dist(lMAS,1), gps2dist(lAS,1);
 
 'son', 1, gps2dist(lMAN,0), gps2dist(lIAN,0);
 'son', 2, gps2dist(lMAN,0), gps2dist(lIAN,0);
 
 'sos', 1, gps2dist(lIS,1), gps2dist(lAS,1);
 'sos', 2, gps2dist(lIS,1), gps2dist(lAS,1);
 
 'sin', 1, gps2dist(lMAN,0), gps2dist(lIAN,0);
 'sin', 2, gps2dist(lMAN,0), gps2dist(lIAN,0);
 
 'sis', 1, gps2dist(lIS,1), gps2dist(lAS,1);
 'sis', 2, gps2dist(lIS,1), gps2dist(lAS,1)
};


%% Calculate path loss parameters
fprintf('Fitting path loss... ');
tic();
for ii = 1:size(pathlosslimpoints,1)
    idstr = pathlosslimpoints{ii,1};
    antstr = ['ant' num2str(pathlosslimpoints{ii,2})];
    point1 = pathlosslimpoints{ii,3};
    point2 = pathlosslimpoints{ii,4};
    
    pl_values_ii = values.(idstr).(antstr);
    pl_kmpoints_ii = kmpoints.(idstr).(antstr);
    pl_dist_ii = dist.(idstr).(antstr);
    pl_mask_ii = (pl_dist_ii > point1 & pl_dist_ii < point2);
    
    pl_values_ii = pl_values_ii(pl_mask_ii);
    pl_kmpoints_ii = pl_kmpoints_ii(pl_mask_ii);
    pl_dist_ii = pl_dist_ii(pl_mask_ii);
    
    % fit path loss
    pl_fit_ii = lsqcurvefit(plf, [1 1] , pl_dist_ii, 10*log10(pl_values_ii),...
                            [], [], optimset('Display', 'off'));
    
    % save values
    pl_values.(idstr).(antstr) = pl_values_ii;
    pl_kmpoints.(idstr).(antstr) = pl_kmpoints_ii;
    pl_dist.(idstr).(antstr) = pl_dist_ii;
    pl_fit.(idstr).(antstr) = pl_fit_ii;
end
fprintf('OK\n');
toc();


%% Plot north sector on outdoors

% North sector [non]
figure();
subplot(221);

semilogx(dist.non.ant1, 10*log10(values.non.ant1));
ax = axis();
hold on
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIN lIN],1), [ax(3) ax(4)], 's--r');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIAN lIAN],1), [ax(3) ax(4)], 'o--r');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAN lMAN],1), [ax(3) ax(4)], 'o--m');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAN lAN],1), [ax(3) ax(4)], '-k');
plot(pl_dist.non.ant1, plf(pl_fit.non.ant1, pl_dist.non.ant1), '-k','linewidth',2);
title('North sector (outdoors)')
legend('Received power',...
    'Vertical 3 dB beamwidth',...
    'Horizontal beam center',...
    'Horizontal 3 dB beamwidth',...
    'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on

% South Sector [nos]
subplot(222);

semilogx(dist.nos.ant1, 10*log10(values.nos.ant1));
ax = axis();
hold on
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIAS lIAS],1), [ax(3) ax(4)], '--k');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAS lAS],1), [ax(3) ax(4)], '-k');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAS lMAS],1), [ax(3) ax(4)], '--k');
plot(pl_dist.nos.ant1, plf(pl_fit.nos.ant1, pl_dist.nos.ant1), '-k','linewidth',2);
title('South sector (outdoors)')
legend('Received power',...
    'Analysis limit',...
    'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on


%% Plot north sector on indoors

% North sector [nin]
subplot(223)

semilogx(dist.nin.ant1, 10*log10(values.nin.ant1));
ax = axis();
hold on
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIN lIN],1), [ax(3) ax(4)], 's--r');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIAN lIAN],1), [ax(3) ax(4)], 'o--r');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAN lMAN],1), [ax(3) ax(4)], 'o--m');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAN lAN],1), [ax(3) ax(4)], '-k');
plot(pl_dist.nin.ant1, plf(pl_fit.nin.ant1, pl_dist.nin.ant1), '-k','linewidth',2);
title('North sector (indoors)')
legend('Received power',...
    'Vertical 3 dB beamwidth',...
    'Horizontal beam center',...
    'Horizontal 3 dB beamwidth',...
    'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on

% South sector [nis]
subplot(224)

semilogx(dist.nis.ant1, 10*log10(values.nis.ant1));
ax = axis();
hold on
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAS lAS],1), [ax(3) ax(4)], '-k');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAS lMAS],1), [ax(3) ax(4)], '--k');
plot(pl_dist.nis.ant1, plf(pl_fit.nis.ant1, pl_dist.nis.ant1), '-k','linewidth',2);
title('South sector (indoors)')
legend('Received power',...
    'Analysis limit',...
    'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on


%% Plot south sector on outdoors

% North sector [son]
figure();
subplot(221);

semilogx(dist.son.ant1, 10*log10(values.son.ant1));
ax = axis();
hold on
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIAN lIAN],1), [ax(3) ax(4)], '--k');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAN lAN],1), [ax(3) ax(4)], '-k');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAN lMAN],1), [ax(3) ax(4)], '--k');
plot(pl_dist.son.ant1, plf(pl_fit.son.ant1, pl_dist.son.ant1), '-k','linewidth',2);
title('North sector (outdoors)')
legend('Received power',...
    'Analysis limit',...
    'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on;


% South sector [sos]
subplot(222)

semilogx(dist.sos.ant1, 10*log10(values.sos.ant1));
ax = axis();
hold on
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIS lIS],1), [ax(3) ax(4)], 's--r');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIAS lIAS],1), [ax(3) ax(4)], 'o--r');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAS lMAS],1), [ax(3) ax(4)], 'o--m');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAS lAS],1), [ax(3) ax(4)], '-k');
plot(pl_dist.sos.ant1, plf(pl_fit.sos.ant1, pl_dist.sos.ant1), '-k','linewidth',2);
title('South sector (outdoors)')
legend('Received power',...
    'Vertical 3 dB beamwidth',...
    'Horizontal beam center',...
    'Horizontal 3 dB beamwidth',...
    'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on


%% Plot south sector on indoors

% North sector [sin]
subplot(223)

semilogx(dist.sin.ant1, 10*log10(values.sin.ant1));
ax = axis();
hold on
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIAN lIAN],1), [ax(3) ax(4)], '--k');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAN lAN],1), [ax(3) ax(4)], '-k');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAN lMAN],1), [ax(3) ax(4)], '--k');
plot(pl_dist.sin.ant1, plf(pl_fit.sin.ant1, pl_dist.sin.ant1), '-k','linewidth',2);
title('North sector (indoors)')
legend('Received power',...
    'Analysis limit',...
    'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on


% South sector [sis]
subplot(224);

semilogx(dist.sis.ant1, 10*log10(values.sis.ant1));
ax = axis();
hold on
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIS lIS],1), [ax(3) ax(4)], 's--r');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIAS lIAS],1), [ax(3) ax(4)], 'o--r');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAS lMAS],1), [ax(3) ax(4)], 'o--m');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAS lAS],1), [ax(3) ax(4)], '-k');
plot(pl_dist.sis.ant1, plf(pl_fit.sis.ant1, pl_dist.sis.ant1), '-k','linewidth',2);
title('South sector (indoors)')
legend('Received power',...
    'Vertical 3 dB beamwidth',...
    'Horizontal beam center',...
    'Horizontal 3 dB beamwidth',...
    'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on


%% Path gains with path loss curves in kilometric points

snrid = {'n', 's'}; % north and south
for caseind = ['o' 'i']
    for antind = 1:2
        antstr = ['ant' num2str(antind)];
        idn = ['n' caseind 'n'];
        ids = ['s' caseind 's'];
        
        snr_values_n = values.(idn).(antstr)(kmpoints.(idn).(antstr) < KP2);
        snr_kmpoints_n = kmpoints.(idn).(antstr)(kmpoints.(idn).(antstr) < KP2);
        
        snr_values_s = values.(ids).(antstr)(kmpoints.(ids).(antstr) >= KP2);
        snr_kmpoints_s = kmpoints.(ids).(antstr)(kmpoints.(ids).(antstr) >= KP2);
        
        % save values
        snr_values.(caseind).(antstr) = [snr_values_n snr_values_s];
        snr_kmpoints.(caseind).(antstr) = [snr_kmpoints_n snr_kmpoints_s];
    end
end


%% Plot curves

figure();
plot(snr_kmpoints.o.ant1, 10*log10(snr_values.o.ant1)); % antenna 1 outdoors
hold on;
grid on;
plot(snr_kmpoints.o.ant2, 10*log10(snr_values.o.ant2)); % antenna 2 outdoors
plot(snr_kmpoints.i.ant1, 10*log10(snr_values.i.ant1)); % antenna 1 indoors
plot(snr_kmpoints.i.ant2, 10*log10(snr_values.i.ant2)); % antenna 2 indoors
ylim([0 60]);
xlim([93.5 102]);
title('Received power');
xlabel('Kilometric point');
ylabel('Received power [dB]');



%% Select path loss interval for representation

% configuration cell
% format: id, antenna, max point
% the max point can be a number or 'max'
cutcell = {...
 'non', 1, 'max';
 'non', 2, 'max';
 'nin', 1, 'max';
 'nin', 2, 'max';
 'sos', 1, 'max';
 'sos', 2, 'max';
 'sis', 1, 'max';
 'sis', 2, 'max';
 
 'nos', 1, 1500;
 'nos', 2, 1500;
 'nis', 1, 1500;
 'nis', 2, 1500;
 'son', 1, 1500;
 'son', 2, 1500;
 'sin', 1, 1500;
 'sin', 2, 1500
};

for ii = 1:size(cutcell,1)
    id = cutcell{ii,1};
    antfield = ['ant' num2str(cutcell{ii,2})];
    
    % define selection function
    if strcmp(cutcell{ii,3}, 'max')
        selfun = @(x) x > 0;
    else
        selfun = @(x) x > 0 & x <= cutcell{ii,3};
    end
    
    aux_values = values.(id).(antfield);
    aux_kmpoints = kmpoints.(id).(antfield);
    aux_dist = dist.(id).(antfield);

    selval = selfun(aux_dist);
    values_fix.(id).(antfield) = aux_values(selval);
    kmpoints_fix.(id).(antfield) = aux_kmpoints(selval);
    dist_fix.(id).(antfield) = aux_dist(selval);
end


%% CDF's and variances calculation

% CURRENTLY TESTING
CDF_TEST = 0; % CDF test flat

% smooth factor to obtain the medium scale fading
cdf_med_fading_smt = 41;

% number of bins to calculate the cdf
cdf_nbins = 30;


for ii = 1:size(cutcell,1)
    id = cutcell{ii,1};
    antfield = ['ant' num2str(cutcell{ii,2})];
    
    aux_values = pl_values.(id).(antfield);
    aux_dist = pl_dist.(id).(antfield);
    aux_pl_fit = pl_fit.(id).(antfield);
    
    aux_pl_fading = 10*log10(aux_values) - plf(aux_pl_fit, aux_dist);
    aux_pl_var = var(aux_pl_fading);
    
    % small and medium scale fading
    aux_pl_fading_med = smooth(aux_pl_fading,cdf_med_fading_smt);
    aux_pl_fading_small = aux_pl_fading.' - aux_pl_fading_med;
    
    % calculate CDF of path loss
    [aux_cdf_pl_n ,aux_cdf_pl_edges] = ...
        histcounts(aux_pl_fading, cdf_nbins,...
                   'Normalization', 'probability');
    aux_cdf_pl_var = var(aux_pl_fading);
    
    % calculate CDF of medium scale (shadow) fading
    [aux_cdf_med_n ,aux_cdf_med_edges] = ...
        histcounts(aux_pl_fading_med, cdf_nbins,...
                   'Normalization', 'probability');
    aux_cdf_med_var = var(aux_pl_fading_med);
    
    % calculate CDF of small scale fading
    [aux_cdf_small_n ,aux_cdf_small_edges] = ...
        histcounts(aux_pl_fading_small, cdf_nbins,...
                   'Normalization', 'probability');
    aux_cdf_small_var = var(aux_pl_fading_small);
    
        
    % save variance
    pl_var.(id).(antfield) = aux_pl_var;
    
    % save cdf data
    cdf_data.(id).(antfield).all.N = aux_cdf_pl_n;
    cdf_data.(id).(antfield).all.edges = aux_cdf_pl_edges;
    cdf_data.(id).(antfield).all.var = aux_cdf_pl_var;
    
    cdf_data.(id).(antfield).med.N = aux_cdf_med_n;
    cdf_data.(id).(antfield).med.edges = aux_cdf_med_edges;
    cdf_data.(id).(antfield).med.var = aux_cdf_med_var;
    
    cdf_data.(id).(antfield).small.N = aux_cdf_small_n;
    cdf_data.(id).(antfield).small.edges = aux_cdf_small_edges;
    cdf_data.(id).(antfield).small.var = aux_cdf_small_var;
    
    
    if CDF_TEST
        figure();
        subplot(1,2,1);
        plot(log10(aux_dist), 10*log10(aux_values));
        grid on;
        hold on;
        plot(log10(aux_dist), plf(aux_pl_fit, aux_dist),...
             'k--', 'LineWidth', 2);
        plot(log10(aux_dist), aux_pl_fading);
        plot(log10(aux_dist), aux_pl_fading_med);
        plot(log10(aux_dist), aux_pl_fading_small);

        legend('path loss', 'path loss fitting', 'fading',...
               'medium scale fading', 'small scale fading');
        ylim([-5 60]);
        xlim([1.95 3.8]);
        title([id ' - ' antfield ' | Path Loss']);
        
        subplot(1,2,2);
        stairs([aux_cdf_pl_edges(1) aux_cdf_pl_edges],...
                [0 cumsum(aux_cdf_pl_n) 1]);
        grid on;
        hold on;
        x = -10:0.1:10;
        plot(x, cdf('Normal', x, 0, sqrt(aux_cdf_pl_var)));
        legend('estimated CDF',...
               sprintf('Normal(0, %6.2f)', sqrt(aux_cdf_pl_var)),...
               'Location', 'NorthWest');
        title([id ' - ' antfield ' | CDF (Path Loss)']);
        xlim([-10 10]);
        ylim([0 1]);
        
        P = get(gcf,'position');
        set(gcf,'position', P.*[1 1 2 1] + [0 0 0 0]);
    end
end


%% CDF figures for paper
% default MATLAB colors
colors = get(groot,'DefaultAxesColorOrder');

xlim_n = -12; % negative x limit
xlim_p = 12; % positive limit

x = xlim_n:0.1:xlim_p;
cdfedges = @(x) [xlim_n x(1) x xlim_p];
cdfn = @(x) [0 0 cumsum(x) 1 1];
cdfplot = @(varargin) stairs(cdfedges(varargin{1}.edges),...
                             cdfn(varargin{1}.N), varargin{2:end});

for ii = 1:2
    if ii == 1
        % north transmit antenna case
        figname = 'north transmit antenna';
        id = 'n';
    else
        % south transmit antenna case
        figname = 'south transmit antenna';
        id = 's';
    end
    
    % define the scenario identifiers
    s1 = [id 'on'];
    s2 = [id 'os'];
    s3 = [id 'in'];
    s4 = [id 'is'];

    figure('Name', figname);
    
    % plot first the second subplot
    h2 = subplot(1,2,2);
    data1 = cdf_data.(s3).ant1.all;
    cdfplot(data1, '-', 'MarkerSize', 5, 'LineWidth', 2, 'Color', colors(1,:))
    cdfx = cdf('Normal', x, 0, sqrt(data1.var));
    hold on;
    grid on;
    plot(x, cdfx, '--k', 'LineWidth', 2);

    data2 = cdf_data.(s4).ant1.all;
    cdfplot(data2, '-', 'LineWidth', 2, 'MarkerSize', 5, 'Color', colors(2,:))
    cdfx = cdf('Normal', x, 0, sqrt(data2.var));
    plot(x, cdfx, ':k', 'LineWidth', 2);
    hl2 = legend('north sector',...
               sprintf('$\\mathcal{N}(0,%5.2f)$', sqrt(data1.var)),...
               'south sector',...
               sprintf('$\\mathcal{N}(0,%5.2f)$', sqrt(data2.var)),...
               'Location', 'SouthEast');
    set(hl2,'Interpreter','latex');


    ht2 = title('indoor antenna 1');
    hx2 = xlabel('$X_\sigma\,[\textrm{dB}]$', 'Interpreter', 'latex');
    set(gca,'yticklabel',[]);
    xlim([xlim_n xlim_p]);
    ylim([0 1]);
    
    h1 = subplot(1,2,1);
    data1 = cdf_data.(s1).ant1.all;
    cdfplot(data1, '-', 'MarkerSize', 5, 'LineWidth', 2, 'Color', colors(1,:))
    cdfx = cdf('Normal', x, 0, sqrt(data1.var));
    hold on;
    grid on;
    plot(x, cdfx, '--k', 'LineWidth', 2);

    data2 = cdf_data.(s2).ant1.all;
    cdfplot(data2, '-', 'LineWidth', 2, 'MarkerSize', 5, 'Color', colors(2,:))
    cdfx = cdf('Normal', x, 0, sqrt(data2.var));
    plot(x, cdfx, ':k', 'LineWidth', 2);
    hl1 = legend('north sector',...
               sprintf('$\\mathcal{N}(0,%5.2f)$', sqrt(data1.var)),...
               'south sector',...
               sprintf('$\\mathcal{N}(0,%5.2f)$', sqrt(data2.var)),...
               'Location', 'SouthEast');
    set(hl1,'Interpreter','latex');


    ht1 = title('outdoor antenna 1');
    hy1 = ylabel('$\textrm{P}(X_\sigma < \textrm{abscisa})$', 'Interpreter', 'latex');
    hx1 = xlabel('$X_\sigma\,[\textrm{dB}]$', 'Interpreter', 'latex');
    xlim([xlim_n xlim_p]);
    ylim([0 1]);


    % widen subplots
    P1 = get(h1, 'Position');
    P2 = get(h2, 'Position');
    W = 0.035; % width factor
    H = 0.35; % height factor
    set(h1, 'Position', P1+[0 H W -H]);
    set(h2, 'Position', P2+[-W H W -H]);

    % change legend position and fontsize
    %set(hl1, 'FontSize', 12);
    Pl1 = get(hl1, 'Position');
    %set(hl1, 'Position', Pl1+[0.07 0 0 0]);
    set(hl1, 'Position', Pl1+[0.05 0 0 0]);
    
    %set(hl2, 'FontSize', 12);
    Pl2 = get(hl2, 'Position');
    %set(hl2, 'Position', Pl2+[0.035 0 0 0]);
    set(hl2, 'Position', Pl2+[0.05 0 0 0]);
    
    % change other font sizes
    %set(hy1, 'FontSize', 16);
    %set(hx1, 'FontSize', 16);
end


%% Path Loss figures for paper

% plot superimposed the medium scale fading
PL_MED_FAD = 0;

% smooth factor to obtain the medium scale fading
med_fading_smt = cdf_med_fading_smt;

% default MATLAB colors
colors = get(groot,'DefaultAxesColorOrder');

% Path loss curves for North antenna on
antnum = 1; % RX antenna (1 or 2)
antstr = ['ant' num2str(antnum)];
figure();
plot(log10(dist_fix.non.(antstr)), 10*log10(values_fix.non.(antstr)), '-', 'Color', colors(1,:));
hold on;
grid on;
plot(log10(dist_fix.nin.(antstr)), 10*log10(values_fix.nin.(antstr)), '-', 'Color', colors(2,:));
plot(log10(dist_fix.nos.(antstr)), 10*log10(values_fix.nos.(antstr)), '--', 'Color', colors(1,:));
plot(log10(dist_fix.nis.(antstr)), 10*log10(values_fix.nis.(antstr)), '--', 'Color', colors(2,:));

if PL_MED_FAD
    % plot medium scale fading
    for idsc = {'non', 'nin', 'nos', 'nis'}
        % plot for all path loss
%        plot(log10(dist_fix.(idsc{1}).(antstr)), ...
%             smooth(10*log10(values_fix.(idsc{1}).(antstr)), med_fading_smt),...
%             '--', 'LineWidth', 2, 'Color', colors(4,:));
        % plot for configured intervals
        plot(log10(pl_dist.(idsc{1}).(antstr)), ...
             smooth(10*log10(pl_values.(idsc{1}).(antstr)), med_fading_smt),...
             '--', 'LineWidth', 2, 'Color', colors(4,:));
    end
end


% path loss model curves
plot(log10(pl_dist.non.(antstr)), plf(pl_fit.non.(antstr), pl_dist.non.(antstr)),...
     'k--', 'LineWidth', 2);
plot(log10(pl_dist.nin.(antstr)), plf(pl_fit.nin.(antstr), pl_dist.nin.(antstr)),...
     'k--', 'LineWidth', 2);
plot(log10(pl_dist.nos.(antstr)), plf(pl_fit.nos.(antstr), pl_dist.nos.(antstr)),...
     'k--', 'LineWidth', 2);
 plot(log10(pl_dist.nis.(antstr)), plf(pl_fit.nis.(antstr), pl_dist.nis.(antstr)),...
     'k--', 'LineWidth', 2);
 
xlabel('$\textrm{distance}\,[\log_{10} \textrm{m}]$', 'Interpreter', 'latex');
ylabel('$\textrm{Received power}\,[\textrm{dB}]$', 'Interpreter', 'latex');
title('Path Loss (North antenna transmitting)');
if PL_MED_FAD
    h = legend('north outdoors', 'north indoors',...
               'south outdoors', 'south indoors',...
               'med. scale fading', 'path loss fitting',...
               'Location', [0.76 0.81 0 0]);
else
   h = legend('north outdoors', 'north indoors',...
              'south outdoors', 'south indoors',...
              'path loss fitting');
end
set(h, 'Interpreter', 'latex');

xlim([1.95 3.8]);
ylim([-5 65]);


% Path loss curves for South antenna on
antnum = 1; % RX antenna (1 or 2)
antstr = ['ant' num2str(antnum)];
figure();
plot(log10(dist_fix.sos.(antstr)), 10*log10(values_fix.sos.(antstr)), '-', 'Color', colors(1,:));
hold on;
grid on;
plot(log10(dist_fix.sis.(antstr)), 10*log10(values_fix.sis.(antstr)), '-', 'Color', colors(2,:));
plot(log10(dist_fix.son.(antstr)), 10*log10(values_fix.son.(antstr)), '--', 'Color', colors(1,:));
plot(log10(dist_fix.sin.(antstr)), 10*log10(values_fix.sin.(antstr)), '--', 'Color', colors(2,:));

if PL_MED_FAD
    % plot medium scale fading
    for idsc = {'sos', 'sis', 'son', 'sin'}
        % plot for all path loss
%        plot(log10(dist_fix.(idsc{1}).(antstr)), ...
%             smooth(10*log10(values_fix.(idsc{1}).(antstr)), med_fading_smt),...
%             '--', 'LineWidth', 2, 'Color', colors(4,:));
        % plot for configured intervals
        plot(log10(pl_dist.(idsc{1}).(antstr)), ...
             smooth(10*log10(pl_values.(idsc{1}).(antstr)), med_fading_smt),...
             '--', 'LineWidth', 2, 'Color', colors(4,:));
    end
end

% path loss model curves
plot(log10(pl_dist.sos.(antstr)), plf(pl_fit.sos.(antstr), pl_dist.sos.(antstr)),...
     'k--', 'LineWidth', 2);
plot(log10(pl_dist.sis.(antstr)), plf(pl_fit.sis.(antstr), pl_dist.sis.(antstr)),...
     'k--', 'LineWidth', 2);
plot(log10(pl_dist.son.(antstr)), plf(pl_fit.son.(antstr), pl_dist.son.(antstr)),...
     'k--', 'LineWidth', 2);
 plot(log10(pl_dist.sin.(antstr)), plf(pl_fit.sin.(antstr), pl_dist.sin.(antstr)),...
     'k--', 'LineWidth', 2);
 
xlabel('$\textrm{distance}\,[\log_{10} \textrm{m}]$', 'Interpreter', 'latex');
ylabel('$\textrm{Received power}\,[\textrm{dB}]$', 'Interpreter', 'latex');
title('Path Loss (South antenna transmitting)');
if PL_MED_FAD
    h = legend('north outdoors', 'north indoors',...
               'south outdoors', 'south indoors',...
               'med. scale fading', 'path loss fitting',...
               'Location', [0.76 0.81 0 0]);
else
    h = legend('north outdoors', 'north indoors',...
               'south outdoors', 'south indoors',...
               'path loss fitting');
end
set(h, 'Interpreter', 'latex');

xlim([1.95 3.8]);
ylim([-5 65]);


%% print path loss parameters in LaTeX table
PRINT_LATEX_TABLE_1 = 1; % table using booktabs
PRINT_LATEX_TABLE_2 = 0; % table using booktabs - without intercept value

fid = 1; % file id to print the LaTeX table

if PRINT_LATEX_TABLE_1
    fprintf('#============================#\n');
    fprintf('#        LaTeX table (1)     #\n');
    fprintf('#============================#\n');
    fprintf('\n');
    
    fprintf(fid, '\\resizebox{\\columnwidth}{!}{\n');
    fprintf(fid, '{\n');
    fprintf(fid, '    \\begin{tabular}{@{} c c c c c c c c c c c c c c @{}}\n');
    fprintf(fid, '    \\toprule\n');
    fprintf(fid, ['    & & ',...
             '\\multicolumn{3}{c}{\\bfseries Out Ant. 1}  & ',...
             '\\multicolumn{3}{c}{\\bfseries Out Ant. 2} & ',...
             '\\multicolumn{3}{c}{\\bfseries In Ant. 1} & ',...
             '\\multicolumn{3}{c}{\\bfseries In Ant. 2}\\\\\n']);
    fprintf(fid, '    \\cmidrule(lr){3-5}\n');
    fprintf(fid, '    \\cmidrule(lr){6-8}\n');
    fprintf(fid, '    \\cmidrule(lr){9-11}\n');
    fprintf(fid, '    \\cmidrule(lr){12-14}\n');
    fprintf(fid, ['    \\bfseries RX & \\bfseries TX & $\\gamma$  & $b$ & $\\sigma_x$ & $\\gamma$ & $b$ & $\\sigma_x$ ',...
             '& $\\gamma$ & $b$ & $\\sigma_x$ & $\\gamma$ & $b$ & $\\sigma_x$ \\\\\n']);
    fprintf(fid, '    \\midrule\n');
    
    % North antenna on - measures in north sector
    fprintf(fid, ['    \\multirow{2}{*}{N} & N ',...
             '& %6.2f & %6.2f & %6.2f ',...
             '& %6.2f & %6.2f & %6.2f ',...
             '& %6.2f & %6.2f & %6.2f ',...
             '& %6.2f & %6.2f & %6.2f \\\\\n'],...
             -pl_fit.non.ant1(1), pl_fit.non.ant1(2), sqrt(pl_var.non.ant1),...
             -pl_fit.non.ant2(1), pl_fit.non.ant2(2), sqrt(pl_var.non.ant2),...
             -pl_fit.nin.ant1(1), pl_fit.nin.ant1(2), sqrt(pl_var.nin.ant1),...
             -pl_fit.nin.ant2(1), pl_fit.nin.ant2(2), sqrt(pl_var.nin.ant2));

    % North antenna on -measures in south sector
    fprintf(fid, [' & S ',...
             '& %6.2f & %6.2f & %6.2f ',...
             '& %6.2f & %6.2f & %6.2f ',...
             '& %6.2f & %6.2f & %6.2f ',...
             '& %6.2f & %6.2f & %6.2f \\\\[-5pt]\n'],...
             -pl_fit.nos.ant1(1), pl_fit.nos.ant1(2), sqrt(pl_var.nos.ant1),...
             -pl_fit.nos.ant2(1), pl_fit.nos.ant2(2), sqrt(pl_var.nos.ant2),...
             -pl_fit.nis.ant1(1), pl_fit.nis.ant1(2), sqrt(pl_var.nis.ant1),...
             -pl_fit.nis.ant2(1), pl_fit.nis.ant2(2), sqrt(pl_var.nis.ant2));
    fprintf(fid, '\\multicolumn{14}{@{} c @{}}{\\dotsrule[black]}\\\\[-3pt]\n');

    % South antenna on - measures in north sector
    fprintf(fid, ['    \\multirow{2}{*}{S} & N ',...
             '& %6.2f & %6.2f & %6.2f ',...
             '& %6.2f & %6.2f & %6.2f ',...
             '& %6.2f & %6.2f & %6.2f ',...
             '& %6.2f & %6.2f & %6.2f \\\\\n'],...
             -pl_fit.son.ant1(1), pl_fit.son.ant1(2), sqrt(pl_var.son.ant1),...
             -pl_fit.son.ant2(1), pl_fit.son.ant2(2), sqrt(pl_var.son.ant2),...
             -pl_fit.sin.ant1(1), pl_fit.sin.ant1(2), sqrt(pl_var.sin.ant1),...
             -pl_fit.sin.ant2(1), pl_fit.sin.ant2(2), sqrt(pl_var.sin.ant2));
         
    % South antenna on -measures in south sector
    fprintf(fid, [' & S ',...
             '& %6.2f & %6.2f & %6.2f ',...
             '& %6.2f & %6.2f & %6.2f ',...
             '& %6.2f & %6.2f & %6.2f ',...
             '& %6.2f & %6.2f & %6.2f \\\\\n'],...
             -pl_fit.sos.ant1(1), pl_fit.sos.ant1(2), sqrt(pl_var.sos.ant1),...
             -pl_fit.sos.ant2(1), pl_fit.sos.ant2(2), sqrt(pl_var.sos.ant2),...
             -pl_fit.sis.ant1(1), pl_fit.sis.ant1(2), sqrt(pl_var.sis.ant1),...
             -pl_fit.sis.ant2(1), pl_fit.sis.ant2(2), sqrt(pl_var.sis.ant2));
    fprintf(fid, '    \\bottomrule\n');
    fprintf(fid, '    \\end{tabular}\n');
    fprintf(fid, '}\n');
    fprintf(fid, '}\n');
    
    fprintf('\n\n');
end


if PRINT_LATEX_TABLE_2
    fprintf('#============================#\n');
    fprintf('#        LaTeX table (2)     #\n');
    fprintf('#============================#\n');
    fprintf('\n');
    
    fprintf(fid, '{\n');
    fprintf(fid, '    \\begin{tabular}{@{} c c c c c c c c c c @{}}\n');
    fprintf(fid, '    \\toprule\n');
    fprintf(fid, ['    &  & ',...
             '\\multirow{2}{*}{\\bfseries RX} & ',...
             '\\multicolumn{2}{c}{\\bfseries Out Ant. 1}  & ',...
             '\\multicolumn{2}{c}{\\bfseries Out Ant. 2} & ',...
             '\\multicolumn{2}{c}{\\bfseries In Ant. 1} & ',...
             '\\multicolumn{2}{c}{\\bfseries In Ant. 2}\\\\\n']);
    fprintf(fid, '    \\cmidrule(lr){3-4}\n');
    fprintf(fid, '    \\cmidrule(lr){5-6}\n');
    fprintf(fid, '    \\cmidrule(lr){7-8}\n');
    fprintf(fid, '    \\cmidrule(lr){9-10}\n');
    fprintf(fid, ['    \\bfseries TX & \\bfseries RX & $\\gamma$  & $\\sigma_x$ & $\\gamma$ & $\\sigma_x$ ',...
             '& $\\gamma$ & $\\sigma_x$ & $\\gamma$ & $\\sigma_x$ \\\\\n']);
    fprintf(fid, '    \\midrule\n');
    
    % North antenna on - measures in north sector
    fprintf(fid, ['    \\multirow{2}{*}{N} & N ',...
             '& %6.2f & %6.2f ',...
             '& %6.2f & %6.2f ',...
             '& %6.2f & %6.2f ',...
             '& %6.2f & %6.2f \\\\\n'],...
             -pl_fit.non.ant1(1), sqrt(pl_var.non.ant1),...
             -pl_fit.non.ant2(1), sqrt(pl_var.non.ant2),...
             -pl_fit.nin.ant1(1), sqrt(pl_var.nin.ant1),...
             -pl_fit.nin.ant2(1), sqrt(pl_var.nin.ant2));

    % North antenna on -measures in south sector
    fprintf(fid, [' & S ',...
             '& %6.2f & %6.2f ',...
             '& %6.2f & %6.2f ',...
             '& %6.2f & %6.2f ',...
             '& %6.2f & %6.2f \\\\[-5pt]\n'],...
             -pl_fit.nos.ant1(1), sqrt(pl_var.nos.ant1),...
             -pl_fit.nos.ant2(1), sqrt(pl_var.nos.ant2),...
             -pl_fit.nis.ant1(1), sqrt(pl_var.nis.ant1),...
             -pl_fit.nis.ant2(1), sqrt(pl_var.nis.ant2));
    fprintf(fid, '\\multicolumn{10}{@{} c @{}}{\\dotsrule[black]}\\\\[-3pt]\n');

    % South antenna on - measures in north sector
    fprintf(fid, ['    \\multirow{2}{*}{S} & N ',...
             '& %6.2f & %6.2f ',...
             '& %6.2f & %6.2f ',...
             '& %6.2f & %6.2f ',...
             '& %6.2f & %6.2f \\\\\n'],...
             -pl_fit.son.ant1(1), sqrt(pl_var.son.ant1),...
             -pl_fit.son.ant2(1), sqrt(pl_var.son.ant2),...
             -pl_fit.sin.ant1(1), sqrt(pl_var.sin.ant1),...
             -pl_fit.sin.ant2(1), sqrt(pl_var.sin.ant2));
         
    % South antenna on -measures in south sector
    fprintf(fid, [' & S ',...
             '& %6.2f & %6.2f ',...
             '& %6.2f & %6.2f ',...
             '& %6.2f & %6.2f ',...
             '& %6.2f & %6.2f \\\\\n'],...
             -pl_fit.sos.ant1(1), sqrt(pl_var.sos.ant1),...
             -pl_fit.sos.ant2(1), sqrt(pl_var.sos.ant2),...
             -pl_fit.sis.ant1(1), sqrt(pl_var.sis.ant1),...
             -pl_fit.sis.ant2(1), sqrt(pl_var.sis.ant2));
    fprintf(fid, '    \\bottomrule\n');
    fprintf(fid, '    \\end{tabular}\n');
    fprintf(fid, '}\n');
    
    fprintf('\n\n');
end



