%% Inicializacións

clearvars -except valoresPotenciaDatos3Pasadas valoresPotenciaRuido
close all
format long g
clc

%% Dependencias

RutaActual = [fileparts(fileparts(((mfilename('fullpath'))))) filesep];
addpath([RutaActual 'AntennaModels' filesep]);
addpath([RutaActual 'GPS_Processing' filesep]);
addpath([RutaActual 'FilesTiming' filesep]);
addpath(RutaActual);
addpath('/home/tecrail/EUCAP/r'); % Ruta cos ficheiros de resultados de potencia de datos e ruido
MeasurementsFolder = [filesep 'media' filesep 'tecrail' filesep 'measData4' filesep];

saveSNR = 0;

%% Carga datos

if ~exist('valoresPotenciaDatos3Pasadas', 'var')
    load('valoresPotenciaDatos3Pasadas.mat')
end;

if ~exist('valoresPotenciaRuido', 'var')
    load('valoresPotenciaRuido.mat')
end


%% Definición de constantes

KP1=93.4;
KP2=97.075;
KP3=102.0;
vel=50;

dant = 101;

% Parámetros comúns

hT = 20;
hR = 3.5;
tiltN = 0;
tiltS = 1.4;
bwT = 5;
azimuth = 5;
bwA = 65;

% Sector norte

lIN = KP2-((hT-hR)/tand(bwT))/1000;
lMN = inf;
lFN = inf;

% Sector sul

lIS = KP2+((hT-hR)/tand(bwT+tiltS))/1000;
lMS = KP2+((hT-hR)/tand(tiltS))/1000;
lFS = inf;

% Limitacións en azimuth

lIAN = (KP2*1000-tand(90-azimuth)*dant)/1000;
lMAN = (KP2*1000-tand(90-azimuth-bwA)*dant)/1000;
lIAS = (KP2*1000+tand(90-azimuth)*dant)/1000;
lMAS = (KP2*1000+tand(90-azimuth-bwA)*dant)/1000;

% Establezo uns límites de avaliación arbitrarios

lAN = 94.31;
lAS = 100.3;

UmbralRadioCela = 25;

RadiosCelas = zeros(2,4);
iRadiosCelas = 1;

estilos.canle(1).localizacion(2).estilo='r';
estilos.canle(2).localizacion(2).estilo='--';
estilos.canle(1).localizacion(1).estilo='g';
estilos.canle(2).localizacion(1).estilo='k--';

% Coordenadas UTM da antena
AntX = 347077.9;
AntY = 4103747.4;

FactorDecimado = 100;

% Path loss function

plf = @(x, xdata) 10*x(1)*log10(xdata) + x(2);

%

ficheirosPotenciaDatos3Pasadas.Pasada(1).Canle(1).Localizacion(2).filename='2014-12-11--00-06_max_throughput_50kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(1).Canle(2).Localizacion(2).filename='2014-12-11--00-06_max_throughput_50kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(1).Canle(1).Localizacion(1).filename='2014-12-11--00-06_max_throughput_50kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(1).Canle(2).Localizacion(1).filename='2014-12-11--00-06_max_throughput_50kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(2).Canle(1).Localizacion(1).filename='2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(2).Canle(2).Localizacion(1).filename='2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(2).Canle(1).Localizacion(2).filename='2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(2).Canle(2).Localizacion(2).filename='2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(3).Canle(1).Localizacion(1).filename='2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(3).Canle(2).Localizacion(1).filename='2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(3).Canle(1).Localizacion(2).filename='2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.nosplit.usrp';
ficheirosPotenciaDatos3Pasadas.Pasada(3).Canle(2).Localizacion(2).filename='2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.nosplit.usrp';

ficheirosPotenciaDatosAltaVelocidade.Velocidade(1).Canle(1).Localizacion(1).filename='2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatosAltaVelocidade.Velocidade(1).Canle(2).Localizacion(1).filename='2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatosAltaVelocidade.Velocidade(1).Canle(1).Localizacion(2).filename='2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatosAltaVelocidade.Velocidade(1).Canle(2).Localizacion(2).filename='2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatosAltaVelocidade.Velocidade(2).Canle(1).Localizacion(1).filename='2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatosAltaVelocidade.Velocidade(2).Canle(2).Localizacion(1).filename='2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatosAltaVelocidade.Velocidade(2).Canle(1).Localizacion(2).filename='2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.nosplit.usrp';
ficheirosPotenciaDatosAltaVelocidade.Velocidade(2).Canle(2).Localizacion(2).filename='2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.nosplit.usrp';

%% Funcións relativas aos índices dos valores de ruido e datos

getNoiseIndex = @(IndiceValores)2-1*(IndiceValores==2);

%% Funcións para obtencion de valores e eixo temporal

getValores = @(kPasada, kCanle, kLocalizacion)...
              valoresPotenciaDatos3Pasadas.Pasada(kPasada)...
             .Canle(kCanle).Localizacion(kLocalizacion).valoresMediaMobil;
         
getValoresRuido = @(kLocalizacion)...
                   valoresPotenciaRuido...
                  .ValorMedioRuidoPorLocalizacion(getNoiseIndex(kLocalizacion));
         
         
getEixoTemp = @(kPasada, kCanle, kLocalizacion)...
                0:(1/((15.36e6)/1024)):(-(1/((15.36e6)/1024))+...
                length(valoresPotenciaDatos3Pasadas.Pasada(kPasada)...
                .Canle(kCanle).Localizacion(kLocalizacion).valoresMediaMobil)...
                /((15.36e6)/1024));


%%

kPasada=1;
kCanle=1;
kLocalizacion=2;

FactorDesalinhamento = 0;
if(kPasada==2)
    FactorDesalinhamento = 3.25;
end

% Lectura datos GPS
samplesFilename = [ficheirosPotenciaDatos3Pasadas.Pasada(kPasada).Canle(kCanle).Localizacion(kLocalizacion).filename '.usrpnmea'];
RutaGPSLog = [MeasurementsFolder samplesFilename];
GPSData = getGPSData(RutaGPSLog);

% obtencion eixo temporal e valores
eixoTemporal = getEixoTemp(kPasada, kCanle, kLocalizacion);
valores = getValores(kPasada, kCanle, kLocalizacion)-getValoresRuido(kLocalizacion);

% decimado eixo temporal e valores para mellorar o rendemento das figuras
eixoTemporalD = eixoTemporal(1:FactorDecimado:end);
valoresD = valores(1:FactorDecimado:end);

% obtencion puntos kilometricos equivalentes para instantes temporais
puntosKmtsD = getGPSKP(GPSData, kPasada == 2, eixoTemporalD, FactorDesalinhamento);

% figure();
% plot(puntosKmtsD, 10*log10(valoresD));
% ax = axis();
% hold on;
% plot([KP2 KP2], [ax(3) ax(4)], '-.k');
% 
% plot([lIN lIN], [ax(3) ax(4)], 's--r');
% 
% plot([lIS lIS], [ax(3) ax(4)], 's--r');
% plot([lMS lMS], [ax(3) ax(4)], 's--m');
% 
% plot([lIAN lIAN], [ax(3) ax(4)], 'o--r');
% plot([lMAN lMAN], [ax(3) ax(4)], 'o--m');
% plot([lIAS lIAS], [ax(3) ax(4)], 'o--r');
% plot([lMAS lMAS], [ax(3) ax(4)], 'o--m');
% 
% plot([lAN lAN], [ax(3) ax(4)], '-k');
% plot([lAS lAS], [ax(3) ax(4)], '-k');
% xlabel('Kilometric Point [km]') 
% ylabel('Normalized Received Power [dB]')
% grid on
% 
% 
% legend('Received power',...
%     'Antenna placement',...
%     'Vertical 3 dB beamwidth (North)',...
%     'Vertical 3 dB beamwidth (South)',...
%     'Vertical beam center (South)',...
%     'Horizontal beam center(North)',...
%     'Horizontal 3 dB beamwidth  (North)',...
%     'Horizontal beam center (South)',...
%     'Horizontal 3 dB beamwidth (South)',...
%     'Evaluation limits','location','southeast');

%% Figura para sector norte (Pasada 3) [outdoors]


kPasada=3;
kCanle=1;
kLocalizacion=2;

FactorDesalinhamento = 0;
if(kPasada==2)
    FactorDesalinhamento = 3.25;
end

% Lectura datos GPS
samplesFilename = [ficheirosPotenciaDatos3Pasadas.Pasada(kPasada).Canle(kCanle).Localizacion(kLocalizacion).filename '.usrpnmea'];
RutaGPSLog = [MeasurementsFolder samplesFilename];
GPSData = getGPSData(RutaGPSLog);

% obtencion eixo temporal e valores
eixoTemporal = getEixoTemp(kPasada, kCanle, kLocalizacion);
valores = getValores(kPasada, kCanle, kLocalizacion)-getValoresRuido(kLocalizacion);

% decimado eixo temporal e valores para mellorar o rendemento das figuras
eixoTemporalD = eixoTemporal(1:FactorDecimado:end);
valoresD = valores(1:FactorDecimado:end);

% obtencion puntos kilometricos equivalentes para instantes temporais
puntosKmtsD = getGPSKP(GPSData, kPasada == 2, eixoTemporalD, FactorDesalinhamento);


figure();
subplot(221)

% Puntos evaluacion path loss exponent
Punto1 = 206;
Punto2 = 2759;

distancias = -TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(puntosKmtsD,1);
PuntosMascara = (distancias > Punto1 & distancias < Punto2);

distancias1 = distancias(PuntosMascara);
valoresD1 = valoresD(PuntosMascara);

% fit path loss
x = lsqcurvefit(plf, [1 1] , distancias1, 10*log10(valoresD1))

xNorte = x;



semilogx(distancias, 10*log10(valoresD));
ax = axis();
hold on
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIN lIN],1), [ax(3) ax(4)], 's--r');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIAN lIAN],1), [ax(3) ax(4)], 'o--r');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAN lMAN],1), [ax(3) ax(4)], 'o--m');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAN lAN],1), [ax(3) ax(4)], '-k');
plot(distancias1, plf(x, distancias1), '-k','linewidth',2);
title('North sector (outdoors)')
legend('Received power',...
    'Vertical 3 dB beamwidth',...
    'Horizontal beam center',...
    'Horizontal 3 dB beamwidth',...
    'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on

subplot(222)

% Puntos evaluacion path loss exponent
Punto1 = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(lMAS,1);
Punto2 = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(lIAS,1);

distancias = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(puntosKmtsD,1);
PuntosMascara = (distancias > Punto1 & distancias < Punto2);

distancias1 = distancias(PuntosMascara);
valoresD1 = valoresD(PuntosMascara);

% fit path loss
x = lsqcurvefit(plf, [1 1] , distancias1, 10*log10(valoresD1))

semilogx(+distancias, 10*log10(valoresD));
ax = axis();
hold on
% plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIS lIS],1), [ax(3) ax(4)], 's--r');
% plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMS lMS],1), [ax(3) ax(4)], 's--m');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIAS lIAS],1), [ax(3) ax(4)], '--k');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAS lAS],1), [ax(3) ax(4)], '-k');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAS lMAS],1), [ax(3) ax(4)], '--k');
plot(distancias1, plf(x, distancias1), '-k','linewidth',2);
title('South sector (outdoors)')
legend('Received power',...
    'Analysis limit',...
    'Evaluation limit','location','southeast');
%     'Vertical 3 dB beamwidth',...
%     'Vertical beam center',...
%     'Horizontal 3 dB beamwidth',...
%     'Horizontal beam center',...
%     'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on

subplot(221)
ax1=axis();
subplot(222)
ax2=axis();
subplot(221);
%xlim([min(ax1(1),ax2(1)) max(ax1(2),ax2(2))])
xlim([100 max(ax1(2),ax2(2))])
ylim([min(ax1(3),ax2(3)) max(ax1(4),ax2(4))])
subplot(222);
%xlim([min(ax1(1),ax2(1)) max(ax1(2),ax2(2))])
xlim([100 max(ax1(2),ax2(2))])
ylim([min(ax1(3),ax2(3)) max(ax1(4),ax2(4))])

%% Figura para sector norte (Pasada 3) [indoors]


kPasada=3;
kCanle=1;
kLocalizacion=1;

FactorDesalinhamento = 0;
if(kPasada==2)
    FactorDesalinhamento = 3.25;
end

% Lectura datos GPS
samplesFilename = [ficheirosPotenciaDatos3Pasadas.Pasada(kPasada).Canle(kCanle).Localizacion(kLocalizacion).filename '.usrpnmea'];
RutaGPSLog = [MeasurementsFolder samplesFilename];
GPSData = getGPSData(RutaGPSLog);
        
% obtencion eixo temporal e valores
eixoTemporal = getEixoTemp(kPasada, kCanle, kLocalizacion);
valores = getValores(kPasada, kCanle, kLocalizacion)-getValoresRuido(kLocalizacion);

% decimado eixo temporal e valores para mellorar o rendemento das figuras
eixoTemporalD = eixoTemporal(1:FactorDecimado:end);
valoresD = valores(1:FactorDecimado:end);

% obtencion puntos kilometricos equivalentes para instantes temporais
puntosKmtsD = getGPSKP(GPSData, kPasada == 2, eixoTemporalD, FactorDesalinhamento);


% figure();
subplot(223)

% Puntos evaluacion path loss exponent
Punto1 = 206;
Punto2 = 2759;

distancias = -TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(puntosKmtsD,1);
PuntosMascara = (distancias > Punto1 & distancias < Punto2);

distancias1 = distancias(PuntosMascara);
valoresD1 = valoresD(PuntosMascara);

% fit path loss
x = lsqcurvefit(plf, [1 1] , distancias1, 10*log10(valoresD1))

xNortei=x;

semilogx(distancias, 10*log10(valoresD));
ax = axis();
hold on
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIN lIN],1), [ax(3) ax(4)], 's--r');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIAN lIAN],1), [ax(3) ax(4)], 'o--r');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAN lMAN],1), [ax(3) ax(4)], 'o--m');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAN lAN],1), [ax(3) ax(4)], '-k');
plot(distancias1, plf(x, distancias1), '-k','linewidth',2);
title('North sector (indoors)')
legend('Received power',...
    'Vertical 3 dB beamwidth',...
    'Horizontal beam center',...
    'Horizontal 3 dB beamwidth',...
    'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on

subplot(224)

% Puntos evaluacion path loss exponent
Punto1 = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(lMAS,1);
Punto2 = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(lAS,1);

distancias = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(puntosKmtsD,1);
PuntosMascara = (distancias > Punto1 & distancias < Punto2);

distancias1 = distancias(PuntosMascara);
valoresD1 = valoresD(PuntosMascara);

% fit path loss
x = lsqcurvefit(plf, [1 1] , distancias1, 10*log10(valoresD1))

semilogx(+distancias, 10*log10(valoresD));
ax = axis();
hold on
% plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIS lIS],1), [ax(3) ax(4)], 's--r');
% plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMS lMS],1), [ax(3) ax(4)], 's--m');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAS lAS],1), [ax(3) ax(4)], '-k');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAS lMAS],1), [ax(3) ax(4)], '--k');
plot(distancias1, plf(x, distancias1), '-k','linewidth',2);
title('South sector (indoors)')
legend('Received power',...
    'Analysis limit',...
    'Evaluation limit','location','southeast');
%     'Vertical 3 dB beamwidth',...
%     'Vertical beam center',...
%     'Horizontal 3 dB beamwidth',...
%     'Horizontal beam center',...
%     'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on

subplot(223)
ax1=axis();
subplot(224)
ax2=axis();
subplot(223);
%xlim([min(ax1(1),ax2(1)) max(ax1(2),ax2(2))])
xlim([100 max(ax1(2),ax2(2))])
ylim([min(ax1(3),ax2(3)) max(ax1(4),ax2(4))])
subplot(224);
%xlim([min(ax1(1),ax2(1)) max(ax1(2),ax2(2))])
xlim([100 max(ax1(2),ax2(2))])
ylim([min(ax1(3),ax2(3)) max(ax1(4),ax2(4))])

%% Figura para sector sul (Pasada 2) [outdoors]


kPasada=2;
kCanle=1;
kLocalizacion=2;

FactorDesalinhamento = 0;
if(kPasada==2)
    FactorDesalinhamento = 3.25;
end

% Lectura datos GPS
samplesFilename = [ficheirosPotenciaDatos3Pasadas.Pasada(kPasada).Canle(kCanle).Localizacion(kLocalizacion).filename '.usrpnmea'];
RutaGPSLog = [MeasurementsFolder samplesFilename];
GPSData = getGPSData(RutaGPSLog);

% obtencion eixo temporal e valores
eixoTemporal = getEixoTemp(kPasada, kCanle, kLocalizacion);
valores = getValores(kPasada, kCanle, kLocalizacion)-getValoresRuido(kLocalizacion);

% decimado eixo temporal e valores para mellorar o rendemento das figuras
eixoTemporalD = eixoTemporal(1:FactorDecimado:end);
valoresD = valores(1:FactorDecimado:end);

% obtencion puntos kilometricos equivalentes para instantes temporais
puntosKmtsD = getGPSKP(GPSData, kPasada == 2, eixoTemporalD, FactorDesalinhamento);


figure();
subplot(221)

% Puntos evaluacion path loss exponent
Punto1 = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(lMAN);
Punto2 = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(lIAN);

distancias = -TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(puntosKmtsD,1);
PuntosMascara = (distancias > Punto1 & distancias < Punto2);

distancias1 = distancias(PuntosMascara);
valoresD1 = valoresD(PuntosMascara);

% fit path loss
x = lsqcurvefit(plf, [1 1] , distancias1, 10*log10(valoresD1))

semilogx(distancias, 10*log10(valoresD));
ax = axis();
hold on
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIAN lIAN],1), [ax(3) ax(4)], '--k');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAN lAN],1), [ax(3) ax(4)], '-k');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAN lMAN],1), [ax(3) ax(4)], '--k');
plot(distancias1, plf(x, distancias1), '-k','linewidth',2);
title('North sector (outdoors)')
legend('Received power',...
    'Analysis limit',...
    'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on

subplot(222)

% Puntos evaluacion path loss exponent
Punto1 = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(lIS,1);
Punto2 = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(lAS,1);

distancias = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(puntosKmtsD,1);
PuntosMascara = (distancias > Punto1 & distancias < Punto2);

distancias1 = distancias(PuntosMascara);
valoresD1 = valoresD(PuntosMascara);

% fit path loss
x = lsqcurvefit(plf, [1 1] , distancias1, 10*log10(valoresD1))

xSul = x;

semilogx(+distancias, 10*log10(valoresD));
ax = axis();
hold on
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIS lIS],1), [ax(3) ax(4)], 's--r');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIAS lIAS],1), [ax(3) ax(4)], 'o--r');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAS lMAS],1), [ax(3) ax(4)], 'o--m');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAS lAS],1), [ax(3) ax(4)], '-k');
plot(distancias1, plf(x, distancias1), '-k','linewidth',2);
title('South sector (outdoors)')
legend('Received power',...
    'Vertical 3 dB beamwidth',...
    'Horizontal beam center',...
    'Horizontal 3 dB beamwidth',...
    'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on

subplot(221)
ax1=axis();
subplot(222)
ax2=axis();
subplot(221);
%xlim([min(ax1(1),ax2(1)) max(ax1(2),ax2(2))])
xlim([100 max(ax1(2),ax2(2))])
ylim([min(ax1(3),ax2(3)) max(ax1(4),ax2(4))])
subplot(222);
%xlim([min(ax1(1),ax2(1)) max(ax1(2),ax2(2))])
xlim([100 max(ax1(2),ax2(2))])
ylim([min(ax1(3),ax2(3)) max(ax1(4),ax2(4))])

%% Figura para sector sul (Pasada 2) [indoors]


kPasada=2;
kCanle=1;
kLocalizacion=1;

FactorDesalinhamento = 0;
if(kPasada==2)
    FactorDesalinhamento = 3.25;
end

% Lectura datos GPS
samplesFilename = [ficheirosPotenciaDatos3Pasadas.Pasada(kPasada).Canle(kCanle).Localizacion(kLocalizacion).filename '.usrpnmea'];
RutaGPSLog = [MeasurementsFolder samplesFilename];
GPSData = getGPSData(RutaGPSLog);

% obtencion eixo temporal e valores
eixoTemporal = getEixoTemp(kPasada, kCanle, kLocalizacion);
valores = getValores(kPasada, kCanle, kLocalizacion)-getValoresRuido(kLocalizacion);

% decimado eixo temporal e valores para mellorar o rendemento das figuras
eixoTemporalD = eixoTemporal(1:FactorDecimado:end);
valoresD = valores(1:FactorDecimado:end);

% obtencion puntos kilometricos equivalentes para instantes temporais
puntosKmtsD = getGPSKP(GPSData, kPasada == 2, eixoTemporalD, FactorDesalinhamento);


% figure();
subplot(223)

% Puntos evaluacion path loss exponent
Punto1 = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(lMAN);
Punto2 = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(lIAN);

distancias = -TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(puntosKmtsD,1);
PuntosMascara = (distancias > Punto1 & distancias < Punto2);

distancias1 = distancias(PuntosMascara);
valoresD1 = valoresD(PuntosMascara);

% fit path loss
x = lsqcurvefit(plf, [1 1] , distancias1, 10*log10(valoresD1))

semilogx(distancias, 10*log10(valoresD));
ax = axis();
hold on
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIAN lIAN],1), [ax(3) ax(4)], '--k');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAN lAN],1), [ax(3) ax(4)], '-k');
plot(-TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAN lMAN],1), [ax(3) ax(4)], '--k');
plot(distancias1, plf(x, distancias1), '-k','linewidth',2);
title('North sector (indoors)')
legend('Received power',...
    'Analysis limit',...
    'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on

subplot(224)

% Puntos evaluacion path loss exponent
Punto1 = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(lIS,1);
Punto2 = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(lAS,1);

distancias = TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(puntosKmtsD,1);
PuntosMascara = (distancias > Punto1 & distancias < Punto2);

distancias1 = distancias(PuntosMascara);
valoresD1 = valoresD(PuntosMascara);

% fit path loss
x = lsqcurvefit(plf, [1 1] , distancias1, 10*log10(valoresD1))
xSuli=x;

semilogx(+distancias, 10*log10(valoresD));
ax = axis();
hold on
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIS lIS],1), [ax(3) ax(4)], 's--r');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lIAS lIAS],1), [ax(3) ax(4)], 'o--r');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lMAS lMAS],1), [ax(3) ax(4)], 'o--m');
plot(TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist([lAS lAS],1), [ax(3) ax(4)], '-k');
plot(distancias1, plf(x, distancias1), '-k','linewidth',2);
title('South sector (indoors)')
legend('Received power',...
    'Vertical 3 dB beamwidth',...
    'Horizontal beam center',...
    'Horizontal 3 dB beamwidth',...
    'Evaluation limit','location','southeast');
xlabel('Distance to the antenna [log m]') 
ylabel('Normalized Received Power [dB]')
grid on

subplot(223)
ax1=axis();
subplot(224)
ax2=axis();
subplot(223);
%xlim([min(ax1(1),ax2(1)) max(ax1(2),ax2(2))])
xlim([100 max(ax1(2),ax2(2))])
ylim([min(ax1(3),ax2(3)) max(ax1(4),ax2(4))])
subplot(224);
%xlim([min(ax1(1),ax2(1)) max(ax1(2),ax2(2))])
xlim([100 max(ax1(2),ax2(2))])
ylim([min(ax1(3),ax2(3)) max(ax1(4),ax2(4))])

%% Path gain con curvas de axuste

kPasada=3;
kCanle=1;
kLocalizacion=2;

FactorDesalinhamento = 0;
if(kPasada==2)
    FactorDesalinhamento = 3.25;
end

% Lectura datos GPS
samplesFilename = [ficheirosPotenciaDatos3Pasadas.Pasada(kPasada).Canle(kCanle).Localizacion(kLocalizacion).filename '.usrpnmea'];
RutaGPSLog = [MeasurementsFolder samplesFilename];
GPSData = getGPSData(RutaGPSLog);

% obtencion eixo temporal e valores
eixoTemporal = getEixoTemp(kPasada, kCanle, kLocalizacion);
valores = getValores(kPasada, kCanle, kLocalizacion)-getValoresRuido(kLocalizacion);

puntosKmts = getGPSKP(GPSData, kPasada == 2, eixoTemporal, FactorDesalinhamento);


valores=valores(puntosKmts<KP2);
puntosKmts=puntosKmts(puntosKmts<KP2);

% decimado eixo temporal e valores para mellorar o rendemento das figuras
puntosKmtsDN = puntosKmts(1:FactorDecimado:end);
valoresDN = valores(1:FactorDecimado:end);


kPasada=2;
kCanle=1;
kLocalizacion=2;

FactorDesalinhamento = 0;
if(kPasada==2)
    FactorDesalinhamento = 3.25;
end

% Lectura datos GPS
samplesFilename = [ficheirosPotenciaDatos3Pasadas.Pasada(kPasada).Canle(kCanle).Localizacion(kLocalizacion).filename '.usrpnmea'];
RutaGPSLog = [MeasurementsFolder samplesFilename];
GPSData = getGPSData(RutaGPSLog);

% obtencion eixo temporal e valores
eixoTemporal = getEixoTemp(kPasada, kCanle, kLocalizacion);
valores = getValores(kPasada, kCanle, kLocalizacion)-getValoresRuido(kLocalizacion);

puntosKmts = getGPSKP(GPSData, kPasada == 2, eixoTemporal, FactorDesalinhamento);

valores=valores(puntosKmts>=KP2);
puntosKmts=puntosKmts(puntosKmts>=KP2);

% decimado eixo temporal e valores para mellorar o rendemento das figuras
puntosKmtsDS = puntosKmts(1:FactorDecimado:end);
valoresDS = valores(1:FactorDecimado:end);

puntosKmtsD = [puntosKmtsDN puntosKmtsDS];
valoresD = [valoresDN valoresDS];

% obtencion puntos kilometricos equivalentes para instantes temporais
% puntosKmtsD = getGPSKP(GPSData, kPasada == 2, eixoTemporalD, FactorDesalinhamento);

puntosXCurvaN = linspace(lAN,lIN,1000);
puntosXCurvaS = linspace(lIS,lAS,1000);%linspace(97.48,lIAS,1000);

figure();PuntosMascara = (distancias > Punto1 & distancias < Punto2);

plot(puntosKmtsD, 10*log10(valoresD));

if(saveSNR==1)
    pRuido=getValoresRuido(2);
    puntosKmts=puntosKmtsD;
    pSinal=valoresD;
    save('/home/tecrail/EUCAP/r/DatosPotencia','puntosKmts','pSinal','pRuido')
end

kPasada=3;
kCanle=1;
kLocalizacion=1;

FactorDesalinhamento = 0;
if(kPasada==2)
    FactorDesalinhamento = 3.25;
end

% Lectura datos GPS
samplesFilename = [ficheirosPotenciaDatos3Pasadas.Pasada(kPasada).Canle(kCanle).Localizacion(kLocalizacion).filename '.usrpnmea'];
RutaGPSLog = [MeasurementsFolder samplesFilename];
GPSData = getGPSData(RutaGPSLog);

% obtencion eixo temporal e valores
eixoTemporal = getEixoTemp(kPasada, kCanle, kLocalizacion);
valores = getValores(kPasada, kCanle, kLocalizacion)-getValoresRuido(kLocalizacion);

puntosKmts = getGPSKP(GPSData, kPasada == 2, eixoTemporal, FactorDesalinhamento);


valores=valores(puntosKmts<KP2);
puntosKmts=puntosKmts(puntosKmts<KP2);

% decimado eixo temporal e valores para mellorar o rendemento das figuras
puntosKmtsDN = puntosKmts(1:FactorDecimado:end);
valoresDN = valores(1:FactorDecimado:end);


kPasada=2;
kCanle=1;
kLocalizacion=1;

FactorDesalinhamento = 0;
if(kPasada==2)
    FactorDesalinhamento = 3.25;
end

% Lectura datos GPS
samplesFilename = [ficheirosPotenciaDatos3Pasadas.Pasada(kPasada).Canle(kCanle).Localizacion(kLocalizacion).filename '.usrpnmea'];
RutaGPSLog = [MeasurementsFolder samplesFilename];
GPSData = getGPSData(RutaGPSLog);

% obtencion eixo temporal e valores
eixoTemporal = getEixoTemp(kPasada, kCanle, kLocalizacion);
valores = getValores(kPasada, kCanle, kLocalizacion)-getValoresRuido(kLocalizacion);

puntosKmts = getGPSKP(GPSData, kPasada == 2, eixoTemporal, FactorDesalinhamento);

valores=valores(puntosKmts>=KP2);
puntosKmts=puntosKmts(puntosKmts>=KP2);

% decimado eixo temporal e valores para mellorar o rendemento das figuras
puntosKmtsDS = puntosKmts(1:FactorDecimado:end);
valoresDS = valores(1:FactorDecimado:end);

puntosKmtsD = [puntosKmtsDN puntosKmtsDS];
valoresD = [valoresDN valoresDS];
ax = axis();
hold on;
plot(puntosKmtsD, 10*log10(valoresD));


plot([KP2 KP2], [ax(3) ax(4)], '-.k');

plot([lIN lIN], [ax(3) ax(4)], 's--r');

plot([lIS lIS], [ax(3) ax(4)], 's--r');
plot([lMS lMS], [ax(3) ax(4)], 's--m');

plot([lIAN lIAN], [ax(3) ax(4)], 'o--r');
plot([lMAN lMAN], [ax(3) ax(4)], 'o--m');
plot([lIAS lIAS], [ax(3) ax(4)], 'o--r');
plot([lMAS lMAS], [ax(3) ax(4)], 'o--m');

plot([lAN lAN], [ax(3) ax(4)], '-k');
plot([lAS lAS], [ax(3) ax(4)], '-k');

plot(puntosXCurvaN, plf(xNorte, TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(puntosXCurvaN)), '-k','linewidth',2);
plot(puntosXCurvaS, plf(xSul, TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(puntosXCurvaS)), '-k','linewidth',2);

puntosXCurvaN = linspace(lAN,lIN,1000);
puntosXCurvaS = linspace(lIS,lAS,1000);
plot(puntosXCurvaN, plf(xNortei, TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(puntosXCurvaN)), '-k','linewidth',2);
plot(puntosXCurvaS, plf(xSuli, TECRAIL_lte_RX_ADIF_GPS_KP_to_Ant_Dist(puntosXCurvaS)), '-k','linewidth',2);

% Parte común


xlabel('Kilometric Point [km]') 
ylabel('Normalized Received Power [dB]')
grid on


legend('Received power (outdoors)',...
    'Received power (indoors)',...
    'Antenna placement',...
    'Vertical 3 dB beamwidth (North)',...
    'Vertical 3 dB beamwidth (South)',...
    'Vertical beam center (South)',...
    'Horizontal beam center(North)',...
    'Horizontal 3 dB beamwidth  (North)',...
    'Horizontal beam center (South)',...
    'Horizontal 3 dB beamwidth (South)',...
    'Evaluation limits','location','northeast');

%% SNR

kPasada=3;
kCanle=1;
kLocalizacion=2;

FactorDesalinhamento = 0;
if(kPasada==2)
    FactorDesalinhamento = 3.25;
end

% Lectura datos GPS
samplesFilename = [ficheirosPotenciaDatos3Pasadas.Pasada(kPasada).Canle(kCanle).Localizacion(kLocalizacion).filename '.usrpnmea'];
RutaGPSLog = [MeasurementsFolder samplesFilename];
GPSData = getGPSData(RutaGPSLog);

% obtencion eixo temporal e valores
eixoTemporal = getEixoTemp(kPasada, kCanle, kLocalizacion);
valores = (getValores(kPasada, kCanle, kLocalizacion)-getValoresRuido(kLocalizacion))/getValoresRuido(kLocalizacion);

puntosKmts = getGPSKP(GPSData, kPasada == 2, eixoTemporal, FactorDesalinhamento);


valores=valores(puntosKmts<KP2);
puntosKmts=puntosKmts(puntosKmts<KP2);

% decimado eixo temporal e valores para mellorar o rendemento das figuras
puntosKmtsDN = puntosKmts(1:FactorDecimado:end);
valoresDN = valores(1:FactorDecimado:end);


kPasada=2;
kCanle=1;
kLocalizacion=2;

FactorDesalinhamento = 0;
if(kPasada==2)
    FactorDesalinhamento = 3.25;
end

% Lectura datos GPS
samplesFilename = [ficheirosPotenciaDatos3Pasadas.Pasada(kPasada).Canle(kCanle).Localizacion(kLocalizacion).filename '.usrpnmea'];
RutaGPSLog = [MeasurementsFolder samplesFilename];
GPSData = getGPSData(RutaGPSLog);

% obtencion eixo temporal e valores
eixoTemporal = getEixoTemp(kPasada, kCanle, kLocalizacion);
valores = (getValores(kPasada, kCanle, kLocalizacion)-getValoresRuido(kLocalizacion))/getValoresRuido(kLocalizacion);

puntosKmts = getGPSKP(GPSData, kPasada == 2, eixoTemporal, FactorDesalinhamento);

valores=valores(puntosKmts>=KP2);
puntosKmts=puntosKmts(puntosKmts>=KP2);

% decimado eixo temporal e valores para mellorar o rendemento das figuras
puntosKmtsDS = puntosKmts(1:FactorDecimado:end);
valoresDS = valores(1:FactorDecimado:end);

puntosKmtsD = [puntosKmtsDN puntosKmtsDS];
valoresD = [valoresDN valoresDS];

% obtencion puntos kilometricos equivalentes para instantes temporais
% puntosKmtsD = getGPSKP(GPSData, kPasada == 2, eixoTemporalD, FactorDesalinhamento);

puntosXCurvaN = linspace(lAN,lIN,1000);
puntosXCurvaS = linspace(lIS,lAS,1000);%linspace(97.48,lIAS,1000);

figure();PuntosMascara = (distancias > Punto1 & distancias < Punto2);

plot(puntosKmtsD, 10*log10(valoresD));

% if(saveSNR==1)
%     pRuido=getValoresRuido(2);
%     puntosKmts=puntosKmtsD;
%     pSinal=valoresD;
%     save('/home/tecrail/EUCAP/r/DatosPotencia','puntosKmts','pSinal','pRuido')
% end

kPasada=3;
kCanle=1;
kLocalizacion=1;

FactorDesalinhamento = 0;
if(kPasada==2)
    FactorDesalinhamento = 3.25;
end

% Lectura datos GPS
samplesFilename = [ficheirosPotenciaDatos3Pasadas.Pasada(kPasada).Canle(kCanle).Localizacion(kLocalizacion).filename '.usrpnmea'];
RutaGPSLog = [MeasurementsFolder samplesFilename];
GPSData = getGPSData(RutaGPSLog);

% obtencion eixo temporal e valores
eixoTemporal = getEixoTemp(kPasada, kCanle, kLocalizacion);
valores = (getValores(kPasada, kCanle, kLocalizacion)-getValoresRuido(kLocalizacion))/getValoresRuido(kLocalizacion);

puntosKmts = getGPSKP(GPSData, kPasada == 2, eixoTemporal, FactorDesalinhamento);


valores=valores(puntosKmts<KP2);
puntosKmts=puntosKmts(puntosKmts<KP2);

% decimado eixo temporal e valores para mellorar o rendemento das figuras
puntosKmtsDN = puntosKmts(1:FactorDecimado:end);
valoresDN = valores(1:FactorDecimado:end);


kPasada=2;
kCanle=1;
kLocalizacion=1;

FactorDesalinhamento = 0;
if(kPasada==2)
    FactorDesalinhamento = 3.25;
end

% Lectura datos GPS
samplesFilename = [ficheirosPotenciaDatos3Pasadas.Pasada(kPasada).Canle(kCanle).Localizacion(kLocalizacion).filename '.usrpnmea'];
RutaGPSLog = [MeasurementsFolder samplesFilename];
GPSData = getGPSData(RutaGPSLog);

% obtencion eixo temporal e valores
eixoTemporal = getEixoTemp(kPasada, kCanle, kLocalizacion);
valores = (getValores(kPasada, kCanle, kLocalizacion)-getValoresRuido(kLocalizacion))/getValoresRuido(kLocalizacion);

puntosKmts = getGPSKP(GPSData, kPasada == 2, eixoTemporal, FactorDesalinhamento);

valores=valores(puntosKmts>=KP2);
puntosKmts=puntosKmts(puntosKmts>=KP2);

% decimado eixo temporal e valores para mellorar o rendemento das figuras
puntosKmtsDS = puntosKmts(1:FactorDecimado:end);
valoresDS = valores(1:FactorDecimado:end);

puntosKmtsD = [puntosKmtsDN puntosKmtsDS];
valoresD = [valoresDN valoresDS];
ax = axis();
hold on;
plot(puntosKmtsD, 10*log10(valoresD));

xlabel('Kilometric Point [km]') 
ylabel('SNR [dB]')
grid on

legend('Received power (outdoors)',...
    'Received power (indoors)',...
    'location','northeast');
