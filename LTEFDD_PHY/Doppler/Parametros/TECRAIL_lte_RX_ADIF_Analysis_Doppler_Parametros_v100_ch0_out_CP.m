% Parámetros básicos de execución e sincronización

IdentificadorResultados='DopplerSpectrumResults_100kmh_ch0_outdoor_CP';
samplesFilename='2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp';
VelocidadeKmh=100;
Canle = 1;
t0=128; % Creo que un rango bo sería de 115 a 130 s
tf=139;
t0gps=267;
tfgps=278;
DesprazamentoInicial = 15360+1;
TramasPorAnalise = 13; % Número de tramas lidas. Procesaranse unha menos (a primeira usarase para sincronizar)
ProcesamentoTestbed=0;
ProcesamentoMatlab=1; % Procesamento Matlab ten prioridade no caso en que se executen os dous...
NumeroSector=1:2;
TramasSeparacion=20;

% Parámetros de estima e interpolación de canle

ProcesamentoCanle=0;

ProcesamentoDoppler = 1;

cec.PilotAverage = 'UserDefined';
cec.FreqWindow = 1;
cec.TimeWindow = 1;
cec.InterpType = 'v4';
cec.InterpWindow = 'Centered';
cec.InterpWinSize = 3;

% Parámetros de descarte de tramas incorrectas

UmbralLonxitudeTraspaso = 10;
MarxeSincronizacion = 500;
Sectores=[3 4];

dop_fftsize = 2^13;   % fft size for doppler calculation