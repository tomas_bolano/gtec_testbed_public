%% Inicializacións básicas

clearvars -except FrequencyOffset Indice IndicesCela PuntosSincronizacion estChannels;
close all;
format long g
restoredefaultpath;
clc;

%% Ruta do script

RutaActual = [fileparts(fileparts(mfilename('fullpath'))) filesep];

%% Rutas

RutaRepositorio = [fileparts(fileparts(fileparts(mfilename('fullpath')))) filesep];
RutaLtePhy = [RutaRepositorio 'LTEFDD_PHY' filesep];
addpath(RutaLtePhy);
addpath([RutaLtePhy 'Doppler/'])
addpath([RutaLtePhy 'Doppler/Parametros'])
addpath([RutaLtePhy 'GPS_Processing/'])
RutaResultados=[RutaRepositorio '5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/'];
addpath(RutaResultados);
MeasurementsFolder = [filesep 'media' filesep 'tecrail' filesep 'measData4' filesep];
%MeasurementsFolder = '/home/tomas/extra/GTEC_5G_Simulator_Results/ArtigoCapacidadeFerrovario_AWPL2016/Results/';
%addpath(MeasurementsFolder);

TECRAIL_lte_RX_ADIF_Analysis_Doppler_Parametros_v100_ch0_out;


%% Configuración representación Doppler
dop_normalize = 0; % normalize the channel for doppler calculation
dop_normalize_after = 1; % normalize the doppler after calculation
dop_snrdb = -inf;  % SNR in dB for normalized doppler (recomended 0 ~ 5)
dop_noisedb = -inf;  % noise power in DB for unnormalized doppler (recommended 35 ~ 40)
dop_interpguards = 0;  % complete the channel in the guards using interpolation
dop_samples_c = 1:55; % vector of causal samples indices
dop_samples_ac = 1:5; % vector of anticausal samples indices
dop_noisedb_after = -inf; % noise to add after calculating doppler
dop_yfactor = 2;
dop_dbplotlim = [-45 0];


%% Inicializacións avanzadas
    
% >> lectura de datos:
if exist('estChannels', 'var')
    fprintf('estChannels already in workspace, skipping loading data\n');
else
    fprintf('loading data... ');
    load(IdentificadorResultados);
    fprintf('OK\n');
end

% >> Constantes propias do caso de estudio
ts = 1/(15.36e6);
fc=2.6e9;
fd = VelocidadeKmh*fc/(3.6*3e8);

% >> Definición de constantes para LTE (10 MHz)
nSamplesSlot = 15360/2;
nSymbolsSlot = 14/2;
nSamplesSubframe = nSamplesSlot * 2;
nSymbolsSubframe = nSymbolsSlot * 2;
nSamplesFrame = nSamplesSubframe * 10;
nSymbolsFrame = nSymbolsSubframe * 10;
nHalfSamplesSlot = nSamplesSlot / 2;
nFFT = 2048/2;
usedSubcarriers=600;
guardSubcarriers=(nFFT-usedSubcarriers)/2;
windowLength = 40/2;
longCPlength = nFFT*10/128;
shortCPlength = nFFT*9/128;
longOFDMsymbolLength = nFFT + longCPlength;
shortOFDMsymbolLength = nFFT + shortCPlength;
NsymbSlot = 7;
fs=15.36e6;


%% Eliminación de tramas adquiridas incorrectamente

IndicesValidos = ones(1,length(IndicesCela));

% Filtro por punto de sincronización así como por índice de cela

% >> Busco o punto de cambio dun sector a outro
for kk=1:(length(IndicesCela)-UmbralLonxitudeTraspaso)
    if(IndicesCela(kk+(0:(UmbralLonxitudeTraspaso-1)))==...
            Sectores(2)*ones(1,UmbralLonxitudeTraspaso))
        ValorTraspaso=kk;
        break;
    end
end

IndicesInvalidos=find(IndicesCela(1:(ValorTraspaso-1))~=Sectores(1));

IndicesValidos(IndicesInvalidos)=0; %#ok<FNDSB>

IndicesInvalidos=find(IndicesCela(ValorTraspaso:end)~=Sectores(2))+ValorTraspaso-1;

IndicesValidos(IndicesInvalidos)=0;

Moda=mode(PuntosSincronizacion);

IndicesInvalidos=find(abs(PuntosSincronizacion-Moda)>MarxeSincronizacion);

IndicesValidos(IndicesInvalidos)=0;

EixoIndices=1:length(IndicesCela);

EixoIndicesValidos=EixoIndices(logical(IndicesValidos));
PuntosSincronizacionValidos=PuntosSincronizacion(logical(IndicesValidos));
IndicesCelaValidos=IndicesCela(logical(IndicesValidos));


%% Cálculo do Doppler Spectrum

DopplerSpectrum = zeros(dop_fftsize,length(EixoIndicesValidos)); 

fprintf('\nEvaluating Doppler Spectrum...\n')

for Indice = 1:length(EixoIndicesValidos)
    IndiceValido = EixoIndicesValidos(Indice);
    estChannel = estChannels(:,:,IndiceValido);
    
    ch = ifftshift(estChannel,1);
    chguards = zeros(423, size(ch,2));

    % interpolate guards
    if dop_interpguards
        for ii = 1:size(ch,2)
            chguards(:,ii) = interp1([1 423], ch([300 301], ii), 1:423, 'linear');
        end
    end
    chext = [(ch(1,:)+ch(end,:))/2; ch(1:300,:); chguards; ch(301:end,:)];

    % add noise
    if dop_normalize
        chpw = mean(mean(abs(chext).^2));
        chext = chext./sqrt(chpw);
        noisepw = 1./10^(dop_snrdb/10);
        chext = chext + sqrt(noisepw/2)*(randn(size(chext)) + 1j*randn(size(chext)));
    else
        chext = chext + 10^(dop_noisedb/10)*...
                        (randn(size(chext)) + 1j*randn(size(chext)));
    end

    timeResponses = sqrt(1024)*ifft(chext);
    
    % display energy of the channel
    %disp(sum(sum(abs(timeResponses([1:55 end-10:end],:)).^2)))

    % calculo sumando componentes causales e anticausales
    for ii = dop_samples_c
        DopplerSpectrum(:,Indice) = DopplerSpectrum(:,Indice) + ...
            ts*fft(xcorr((timeResponses(ii,:))), dop_fftsize).';
    end

    for ii = dop_samples_ac
        DopplerSpectrum(:,Indice) = DopplerSpectrum(:,Indice) + ...
            ts*fft(xcorr((timeResponses(end-ii,:))), dop_fftsize).';
    end
    
    % Divide by the number of sums to obtain the mean
    DopplerSpectrum(:,Indice) = DopplerSpectrum(:,Indice)/...
                                (numel(dop_samples_c)+numel(dop_samples_ac));
    
    if(dop_normalize_after)
        DopplerSpectrum(:,Indice) = DopplerSpectrum(:,Indice)./sum(abs(DopplerSpectrum(:,Indice)));
    end
    
    DopplerSpectrum(:,Indice) = DopplerSpectrum(:,Indice) + ...
        sqrt(10^(dop_noisedb_after/10)/2)*...
            (randn(dop_fftsize,1)+1j*randn(dop_fftsize,1));
end


%% Doppler PSD
% Representación gráfica

fmax = 1/(2*ts*(1024+(6*72+80)/7));
f = linspace(-fmax, fmax, size(DopplerSpectrum,1));

figure();
t = ts*TramasSeparacion*140*(1024+(6*72+80)/7)*(EixoIndicesValidos-1);

% obtencion puntos kilometricos equivalentes para instantes temporais
FactorDesalinhamento = 0;
RutaGPSLog = [MeasurementsFolder samplesFilename '.usrpnmea'];
GPSData = getGPSData(RutaGPSLog);
puntosKmtsD = getGPSKP(GPSData, 0, t,getEndFileOffsetGPSKP(GPSData, tfgps), FactorDesalinhamento);

surf(puntosKmtsD ,f,...
    10*log10(abs(fftshift(DopplerSpectrum,1))));
%shading interp; 
shading flat;
ylim(dop_yfactor*[-fd fd])
view([0 90]);
colormap('jet');
caxis(dop_dbplotlim);
%caxis([-45 0]);
xlim([min(puntosKmtsD) max(puntosKmtsD)]);
colorbar
ylabel('Doppler frequency [Hz]');
xlabel('Kilometric Point [km]');
xlim([94.39 101.03])
set(findall(gcf,'-property','FontSize'),'FontSize',12);

%% Doppler mean PSD between two points
% PSD
KP1 = 95;
KP2 = 96.5;

sampleInd1 = find(puntosKmtsD > KP1, 1, 'first');
sampleInd2 = find(puntosKmtsD < KP2, 1, 'last');

psd = abs(fftshift(DopplerSpectrum,1));
psd = mean(psd(:,sampleInd1:sampleInd2), 2);

figure();
plot(f, 10*log10(psd), '-');
grid on;
hold on;
plot(f, smooth(10*log10(psd),7), '-', 'LineWidth', 2);
xlabel('Doppler frequency [Hz]');
ylabel('mean power [dB]');
xlim(dop_yfactor*[-fd fd]);
legend('FFT-based estimate', 'averaged results')
set(findall(gcf,'-property','FontSize'),'FontSize',12);
