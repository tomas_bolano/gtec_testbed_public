%% Inicializacións básicas

clear variables;
close all;
format long g
restoredefaultpath;
clc;

%% Rutas


RutaRepositorio = [fileparts(fileparts(fileparts(mfilename('fullpath')))) filesep];
RutaLtePhy = [RutaRepositorio 'LTEFDD_PHY' filesep];
addpath(RutaLtePhy);
addpath([RutaLtePhy 'Doppler/'])
addpath([RutaLtePhy 'Doppler/Parametros'])
addpath([RutaRepositorio 'gtis/matlab/usrpfileformat/'])
addpath([RutaRepositorio 'gtis/matlab/utils/'])
addpath([RutaRepositorio 'gtis/examples/lte/'])
RutaResultados=[RutaRepositorio '5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/'];
addpath(RutaResultados);
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData4');

%% Definición de variables básicas dependentes do caso de estudio:

TECRAIL_lte_RX_ADIF_Analysis_Doppler_Parametros_v100_ch0_out;

%% Inicializacións avanzadas

% >> Variables para a lectura de datos:

rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles, samplesFilename);

% >> Definición de constantes para as medidas

Ntx = 2;
Nrx = 2;

Sector = [3 4];

fc = 2.6e9;

% >> Definición de constantes para LTE (10 MHz)
nSamplesSlot = 15360/2;
nSymbolsSlot = 14/2;
nSamplesSubframe = nSamplesSlot * 2;
nSymbolsSubframe = nSymbolsSlot * 2;
nSamplesFrame = nSamplesSubframe * 10;
nSymbolsFrame = nSymbolsSubframe * 10;
nHalfSamplesSlot = nSamplesSlot / 2;
nFFT = 2048/2;
usedSubcarriers=600;
guardSubcarriers=(nFFT-usedSubcarriers)/2;
windowLength = 40/2;
longCPlength = nFFT*10/128;
shortCPlength = nFFT*9/128;
longOFDMsymbolLength = nFFT + longCPlength;
shortOFDMsymbolLength = nFFT + shortCPlength;
NsymbSlot = 7;
fs=15.36e6;
enbC.NDLRB=50;
enbC.CyclicPrefix='Normal';
enbC.DuplexMode='FDD';

enbC.CellRefP = 1;               % One transmit antenna port
enbC.LookForAllNCellIDs = false;
enbC.Ng = 'One';
enbC.CFI = 2;
enbC.PHICHDuration = 'Normal';

% >> Constantes dependentes do caso de análise

mostras=nSamplesFrame*TramasPorAnalise;
m0=t0*fs+DesprazamentoInicial-nSamplesSubframe;
mf=tf*fs;
Indices = m0:nSamplesFrame*TramasSeparacion:mf;
% enb.NCellID=Sector(NumeroSector);

% >> Constantes dependentes do Doppler

dopplerMaximo = VelocidadeKmh*fc/(3.6*3e8);

% >> Preparación para o salvagardado de datos:

PuntosSincronizacion = zeros(1,length(Indices));
% PDPs = zeros(MostrasPDP,length(Indices));
estChannels = zeros(usedSubcarriers,nSymbolsFrame*(TramasPorAnalise-1),length(Indices));
IndicesCela = zeros(1,length(Indices));
FrequencyOffset = zeros(1,length(Indices));
Indice=0;
DopplerSpectrum = zeros(dop_fftsize,length(Indices));

if(exist([IdentificadorResultados '.mat'],'file'))
    disp('Results will be overwritten...');
    disp('Press any key to proceed or Ctrl+C to cancel...');
    pause;
else
    save([RutaResultados IdentificadorResultados],'FrequencyOffset','PuntosSincronizacion','PuntosSincronizacion','IndicesCela','Indice','estChannels','-v7.3');
end

%% Análise dos datos

A=tic;

parfor Indice = 1:length(Indices)
    
    tic;
    
    enb = enbC;
    
    mi = Indices(Indice);
    eltime = toc;
    fprintf('\n[%6.2f %%] Processing data... (Elapsed time: %5.2f seconds)\n',100*Indice/length(Indices),eltime);

    % >> Obteño información do ficheiro

    %fileinfo = GTIS_getInfoFromUSRPMultichannelSignalsFile(rxSignalsInUSRPFormatFilename);

    % >> Lectura do ficheiro
    
    [readSignalComplex, success, ...
        totalNumSamplessProcessedPerChannel, ...
        totalNumBytesProcessedPerChannel, ...
        numChannels] = GTIS_readUSRPMultichannelSignalsFromFile(...
        rxSignalsInUSRPFormatFilename, ...
        mostras, ...
        mi); % Procesarei a canle 0 disto...

    rxSamples = readSignalComplex(Canle,1:end);

%     GTIS_spectrum(rxSamples(1,:), fs);

    N_id_cell=[];
    f_start_idx=[];

    if(ProcesamentoTestbed==1)
        
        [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSamples);

        % Search for PSS and find fine timing offset
        [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
        if(pss_symb ~= 0)
            fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
        else
            fprintf('ERROR: Did not find PSS\n');
%             return;
        end

        % SSS
        [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
        if f_start_idx ~= 0
            while(f_start_idx < 0)
                f_start_idx = f_start_idx + nSamplesFrame;
            end
            fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
        else
            fprintf('ERROR: Did not find SSS\n');
        end

        % Construct N_id_cell
        N_id_cell = 3*N_id_1 + N_id_2;
        fprintf(' N id cell = %d\n', N_id_cell);
        
    end
    
    if(ProcesamentoMatlab==1)
        
        % Estima e corrección do offset de frecuencia
            
        fprintf('\nPerforming frequency offset estimation...\n');
        foffset = lteFrequencyOffset(enb, rxSamples.');
        fprintf('Frequency offset: %0.3fHz\n',foffset);
        rxSamplesFrequencyCorrected = lteFrequencyCorrect(enb,rxSamples.',foffset);
        FrequencyOffset(Indice) = foffset;
        
        % Detección da identidade de cela e estima do offset en tempo
        
        fprintf('\nPerforming cell search...\n');
        [N_id_cell, f_start_idx] = lteCellSearch(enb, rxSamplesFrequencyCorrected(1:nSamplesFrame*(TramasPorAnalise-1)), Sector(NumeroSector));
        fprintf(' N id cell = %d\n', N_id_cell);
        fprintf('0 index is %u, cell_id is %u\n\n',f_start_idx,N_id_cell);
        
    end
    
    if((f_start_idx>0)&&((1+f_start_idx+nSamplesFrame-1)<=(TramasPorAnalise*nSamplesFrame)))

        if(ProcesamentoDoppler==1)

            fprintf('\nPerforming time synchronization...\n')

            enb.NCellID = N_id_cell;

            fprintf('Timing offset to frame start: %d samples\n',f_start_idx);
            rxSamples=rxSamples.';
            rxSamples = rxSamples(1+f_start_idx+(0:((TramasPorAnalise-1)*nSamplesFrame-1)),:);

            fprintf('\nOFDM demodulation...\n')

            rxGrid = sqrt(usedSubcarriers)/nFFT*lteOFDMDemodulate(enb, rxSamples);

            enb.NSubframe = 0;
            fprintf('\nEstimating channel...\n')
            [estChannel, noiseEst] = lteDLChannelEstimate(enb, cec, rxGrid);

            estChannels(:,:,Indice)=estChannel;

    %         figure('Color','w');
    %         helperPlotChannelEstimate(estChannel);
    %         shading flat;

%             if(ProcesamentoPDP==1)

                
%                 pdp=diag(abs(timeResponses*(timeResponses')))/nSymbolsFrame;

%                 PDPs(:,Indice)=pdp;

    %             figure,stem(1e6*(0:(MostrasPDP-1))/fs,pdp(1:MostrasPDP)/max(pdp))
    %             title('normalized power delay profile')
    %             xlabel('delay [\mus]'),ylabel('normalized magnitude')
    %             grid on
    %             hold on
    %             plot(1e6*(0:(MostrasPDP-1))/fs,pdp(1:MostrasPDP)/max(pdp),'r:')

%             end

    %         pause;

        end

    end
    
    PuntosSincronizacion(Indice)=f_start_idx;
    IndicesCela(Indice)=N_id_cell;
        
end

toc(A);

%% Salvagardado de datos

save([RutaResultados IdentificadorResultados],'FrequencyOffset','PuntosSincronizacion','PuntosSincronizacion','IndicesCela','Indice','estChannels','-v7.3');