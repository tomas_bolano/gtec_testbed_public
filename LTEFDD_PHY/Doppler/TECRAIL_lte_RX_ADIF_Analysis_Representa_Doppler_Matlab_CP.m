%% Inicializacións básicas

clear variables;
close all;
format long g
restoredefaultpath;
clc;

%% Ruta do script

RutaActual = [fileparts(fileparts(mfilename('fullpath'))) filesep];

NomeFicheiroParametrosBase='TECRAIL_lte_RX_ADIF_Analysis_Doppler_Parametros_v200_ch0_out';

%% Rutas

RutaRepositorio = [fileparts(fileparts(fileparts(mfilename('fullpath')))) filesep];
RutaLtePhy = [RutaRepositorio 'LTEFDD_PHY' filesep];
addpath(RutaLtePhy);
addpath([RutaLtePhy 'Doppler/'])
addpath([RutaLtePhy 'Doppler/Parametros'])
addpath([RutaLtePhy 'GPS_Processing/'])
RutaResultados=[RutaRepositorio '5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/'];
addpath(RutaResultados);
MeasurementsFolder = [filesep 'media' filesep 'tecrail' filesep 'measData4' filesep];

run([NomeFicheiroParametrosBase '_CP']);

TramasSeparacionCP=TramasSeparacion;
t0CP=t0;
tfCP=tf;
t0gpsCP=t0gps;
tfgpsCP=tfgps;
IdentificadorResultadosCP=IdentificadorResultados;

run(NomeFicheiroParametrosBase);

%% Inicializacións avanzadas
    
% >> Engado partes necesarias para o salvagardado e lectura de datos:

load(IdentificadorResultados);

% >> Constantes propias do caso de estudio

ts = 1/(15.36e6);
fc=2.6e9;
fd = VelocidadeKmh*fc/(3.6*3e8);

% >> Definición de constantes para LTE (10 MHz)
nSamplesSlot = 15360/2;
nSymbolsSlot = 14/2;
nSamplesSubframe = nSamplesSlot * 2;
nSymbolsSubframe = nSymbolsSlot * 2;
nSamplesFrame = nSamplesSubframe * 10;
nSymbolsFrame = nSymbolsSubframe * 10;
nHalfSamplesSlot = nSamplesSlot / 2;
nFFT = 2048/2;
usedSubcarriers=600;
guardSubcarriers=(nFFT-usedSubcarriers)/2;
windowLength = 40/2;
longCPlength = nFFT*10/128;
shortCPlength = nFFT*9/128;
longOFDMsymbolLength = nFFT + longCPlength;
shortOFDMsymbolLength = nFFT + shortCPlength;
NsymbSlot = 7;
fs=15.36e6;

%% Eliminación de tramas adquiridas incorrectamente

IndicesValidos = ones(1,length(IndicesCela));

% Filtro por punto de sincronización así como por índice de cela

% >> Busco o punto de cambio dun sector a outro

for kk=1:(length(IndicesCela)-UmbralLonxitudeTraspaso)
    if(IndicesCela(kk+(0:(UmbralLonxitudeTraspaso-1)))==...
            Sectores(2)*ones(1,UmbralLonxitudeTraspaso))
        ValorTraspaso=kk;
        break;
    end
end

IndicesInvalidos=find(IndicesCela(1:(ValorTraspaso-1))~=Sectores(1));

IndicesValidos(IndicesInvalidos)=0; %#ok<FNDSB>

IndicesInvalidos=find(IndicesCela(ValorTraspaso:end)~=Sectores(2))+ValorTraspaso-1;

IndicesValidos(IndicesInvalidos)=0;

Moda=mode(PuntosSincronizacion);

IndicesInvalidos=find(abs(PuntosSincronizacion-Moda)>MarxeSincronizacion);

IndicesValidos(IndicesInvalidos)=0;

EixoIndices=1:length(IndicesCela);

EixoIndicesValidos=EixoIndices(logical(IndicesValidos));
PuntosSincronizacionValidos=PuntosSincronizacion(logical(IndicesValidos));
IndicesCelaValidos=IndicesCela(logical(IndicesValidos));

%% Cálculo do Doppler Spectrum

DopplerSpectrum = zeros(TamanhoFFTRepresentacion,length(EixoIndicesValidos)); 

fprintf('\nEvaluating Doppler Spectrum...\n')

for Indice = 1:length(EixoIndicesValidos)
    
    IndiceValido = EixoIndicesValidos(Indice);
    
    estChannel=estChannels(:,:,IndiceValido);

    extendedEstChannel = [zeros(guardSubcarriers,nSymbolsFrame*(TramasPorAnalise-1));...
        estChannel;zeros(guardSubcarriers,nSymbolsFrame*(TramasPorAnalise-1))];
    extendedEstChannel = ifftshift(extendedEstChannel,1);
    timeResponses = sqrt(1024)*ifft(extendedEstChannel);

    % calculo sumando componentes causales e anticausales

    for ii = 1:mostras_c
        DopplerSpectrum(:,Indice) = DopplerSpectrum(:,Indice) + ...
            1e-10*fft(xcorr((timeResponses(ii,:))), TamanhoFFTRepresentacion).';
    end

    for ii = 1:mostras_ac
        DopplerSpectrum(:,Indice) = DopplerSpectrum(:,Indice) + ...
            1e-10*fft(xcorr((timeResponses(end-ii,:))), TamanhoFFTRepresentacion).';
    end
    
    if(NormalizaDopplerSpectrum)
        DopplerSpectrum(:,Indice) = DopplerSpectrum(:,Indice)./sum(abs(DopplerSpectrum(:,Indice)));
    end
end

%% Doppler PSD
% Representación gráfica

fmax = 1/(2*ts*(1024+(6*72+80)/7));
f = linspace(-fmax, fmax, size(DopplerSpectrum,1));

figure();
t = ts*TramasSeparacion*140*(1024+(6*72+80)/7)*(EixoIndicesValidos-1);

% obtencion puntos kilometricos equivalentes para instantes temporais
FactorDesalinhamento = 0;
RutaGPSLog = [MeasurementsFolder samplesFilename '.usrpnmea'];
GPSData = getGPSData(RutaGPSLog);
puntosKmtsD = getGPSKP(GPSData, 0, t,getEndFileOffsetGPSKP(GPSData, tfgps), FactorDesalinhamento);

surf(puntosKmtsD ,f,...
    10*log10(abs(fftshift(DopplerSpectrum,1))));
shading interp; 
ylim(FactorEixoY*[-fd fd])
view([0 90]);
colormap('jet');
caxis([-45 0]);
xlim([min(puntosKmtsD) max(puntosKmtsD)]);
colorbar
ylabel('Doppler frequency [Hz]');
xlabel('Kilometric Point [km]');


%% Doppler mean PSD
% PSD
KP1 = 94.5;
KP2 = 96.5;

sampleInd1 = find(puntosKmtsD > KP1, 1, 'first');
sampleInd2 = find(puntosKmtsD < KP2, 1, 'last');

psd = abs(fftshift(DopplerSpectrum,1));
psd = mean(psd(:,sampleInd1:sampleInd2), 2);

figure();
plot(f, 10*log10(psd), '-');
xlabel('Doppler frequency [Hz]');
ylabel('mean power [dB]');
xlim(FactorEixoY*[-fd fd])



return
% Lectura da velocidade do GPS

GpsSamplesFilename = [samplesFilename '.usrpnmea'];

RutaGPSLog = [MeasurementsFolder GpsSamplesFilename];

tic();
fileID = fopen(RutaGPSLog);
GPSLog = fread(fileID,'*char')';
fclose(fileID);

GPSLogLines = strsplit(GPSLog, '\n');

% Get $GPRMC lines
GPRMCLines = {};
for ii = 1:length(GPSLogLines)
    if length(GPSLogLines{ii}) >= 6 && ...
       strcmp(GPSLogLines{ii}(1:6), '$GPRMC')
        GPRMCLines{end+1} = GPSLogLines{ii}; %#ok<SAGROW>
    end
end

% Get processed GPS data
GPSData = {};
for ii = 1:length(GPRMCLines)
    GPSData{ii} = TECRAIL_lte_RX_ADIF_GPS_NMEA_Analysis(GPRMCLines{ii}); %#ok<SAGROW>
end

% Concatenate GPS data into struct
GPSData = cat(1,GPSData{:});

figure
subplot(5,1,2);
gpsv = arrayfun(@(x) x.groundspeed.kmh, GPSData); % speed in km/h
gpst = arrayfun(@(x) x.time.day_seconds, GPSData); % time in seconds
gpst = gpst - min(gpst);


subplot(4,1,2:4)
plot(gpst-t0gps, gpsv);
hold on
grid on;
xlabel('time [s]');
ylabel('speed [km/h]');
title(sprintf('speed v=%dkm/h', VelocidadeKmh));



% Estimacion velocidad a partir do doppler
x
[~,MaxDopIndex] = max(fftshift(abs(DopplerSpectrum),1));
MaxDoppler = f(MaxDopIndex);
vdop = abs(MaxDoppler*3e8/2.6e9);




plot(t, vdop*3.6,'--');


legend('GPS velocity','Doppler velocity')
hold off
xlim([min(t) max(t)])

interp_x = gpst-t0gps;
interp_v = gpsv;
interp_vq = interp1(interp_x, interp_v, t);


subplot(4,1,1)

plot(t,abs(interp_vq-vdop*3.6))

xlabel('time [s]')
ylabel('absolute error [km/h]')
grid on
xlim([min(t) max(t)])


hold on
plot([min(t) max(t)],[1 1]*median(abs(interp_vq-vdop*3.6)),'r--')
hold off

legend('Absolute error','Absolute median error')