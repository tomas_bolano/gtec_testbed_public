%% Inicializacións varias

clear variables;
close all;
format long g
clc;

restoredefaultpath

% >> Engado partes necesarias do simulador:

addpath('/home/tecrail/gtisrc/gtis/matlab/usrpfileformat/')
addpath('/home/tecrail/gtisrc/gtis/matlab/utils/')
addpath('/home/tecrail/gtisrc/gtis/examples/lte/')

% >> Engado partes necesarias para o salvagardado de datos:

RutaResultados='/home/tecrail/gtisrc/5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/';
addpath(RutaResultados);
IdentificadorResultados='SynchronizationPoints_100kmh_ch1_outdoor_sector_2';
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData4');
rxSignalsInUSRPFormatFilename=fullfile(basePathForDataFiles,...
    '2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp');

fprintf('\t ==================================================\n');
fprintf('\t\t TECRAIL USRP B210 LTE 10 MHz MIMO sniffer\n');
fprintf('\t\t       ADIF Measurements 9-10-dec-2014     \n');
fprintf('\t ==================================================\n');

%% ESTUDO DO CASO 15: Pasada a 100 km/h, sobre t=155s, canle 1, fóra (cellid = 4)

Ntx = 2;
Nrx = 2;
doFrequencyShift = 0;
interpolationFactors = 1;
decimationFactorForProcessing = interpolationFactors(1);
nZeroSamples = 0;
Canle = 2;

t0=155; % Creo que un rango bo sería de 100 a 130 s
tf=175;
fs=15.36e6;
signalSamplingRate=fs;
mostras=15360*20;
m0=t0*fs+122721-15360;
mf=tf*fs;

Indices = m0:153600:mf;

if(exist([IdentificadorResultados '.mat'],'file'))
    load(IdentificadorResultados);
    disp('Results will be overwritten...');
    disp('Press any key to proceed or Ctrl+C to cancel...');
    pause;
else
    PuntosSincronizacion = zeros(1,length(Indices));
    save([RutaResultados IdentificadorResultados],'PuntosSincronizacion');
end

tic;

for Indice = 1:length(Indices)
    
    mi = Indices(Indice);
    
    eltime = toc;
    
    fprintf('[%6.2f %%] Processing data... (Elapsed time: %5.2f seconds)\n',100*Indice/length(Indices),eltime);

    % >> Obteño información do ficheiro

    %fileinfo = GTIS_getInfoFromUSRPMultichannelSignalsFile(rxSignalsInUSRPFormatFilename);

    % >> Lectura do ficheiro

    [readSignalComplex, success, ...
        totalNumSamplessProcessedPerChannel, ...
        totalNumBytesProcessedPerChannel, ...
        numChannels] = GTIS_readUSRPMultichannelSignalsFromFile(...
        rxSignalsInUSRPFormatFilename, ...
        mostras, ...
        mi); % Procesarei a canle 0 disto...

    rxSamples = readSignalComplex(Canle,1:end);

    % Frequency shift
    if (doFrequencyShift)
        rxSignalsShifted = zeros(size(rxSamples));
        t = 2*pi*fc*(0:(1/signalSamplingRate):((length(rxSamples) - 1)/signalSamplingRate));
        for ii = 1:size(rxSamples, 1),
            rxSignalsShifted(ii, :) = rxSamples(ii, :) .* cos(t);
        end
    else
        rxSignalsShifted = rxSamples;
    end

%     GTIS_spectrum(rxSignalsShifted(1,:), signalSamplingRate);

    % Low-pass filtering and decimation (resampling)
    fprintf('RX: low-pass filtering . . . ');
    rxSignalsDecimated = zeros(Nrx, length(rxSignalsShifted(1, 1:decimationFactorForProcessing:end)));
    if (decimationFactorForProcessing > 1)
        for ii = 1:size(rxSignalsShifted, 1),
            rxSignalsDecimated(ii, :) = ...
                resample(rxSignalsShifted(ii, :), 1, decimationFactorForProcessing);
        end
    else
        rxSignalsDecimated = rxSignalsShifted;
    end
    fprintf('OK\n');
%     GTIS_spectrum(rxSignalsDecimated(1,:), signalSamplingRate);

    % Definitions for 10 MHz
    nSamplesSlot = 15360/2;
    nSamplesSubframe = nSamplesSlot * 2;
    nSamplesFrame = nSamplesSubframe * 10;
    nHalfSamplesSlot = nSamplesSlot / 2;
    nFFT = 2048/2;
    windowLength = 40/2;
    longCPlength = nFFT*10/128;
    shortCPlength = nFFT*9/128;
    longOFDMsymbolLength = nFFT + longCPlength;
    shortOFDMsymbolLength = nFFT + shortCPlength;
    NsymbSlot = 7;

    [start_loc_vec, freq_offset] = find_coarse_time_and_freq_offset_10MHz(rxSignalsDecimated);

    % Search for PSS and find fine timing offset
    pss_symb = 0;
    [start_loc_vec, N_id_2, pss_symb, pss_thresh] = find_pss_and_fine_timing_10MHz(rxSamples, start_loc_vec);
    if(pss_symb ~= 0)
        fprintf('Found PSS-%u in %u (%f)\n', N_id_2, pss_symb, pss_thresh);
    else
        fprintf('ERROR: Did not find PSS\n');
        return;
    end

    % SSS
    f_start_idx = 0;
    [N_id_1, f_start_idx] = find_sss_10MHz(rxSamples, N_id_2, start_loc_vec, pss_thresh);
    if f_start_idx ~= 0
        while(f_start_idx < 0)
            f_start_idx = f_start_idx + nSamplesFrame;
        end
        fprintf('Found SSS-%u, 0 index is %u, cell_id is %u\n', N_id_1, f_start_idx, 3*N_id_1 + N_id_2);
    else
        fprintf('ERROR: Did not find SSS\n');
    end

    % Redefine input and start vector to the 0 index
%     xvRXSamplest2 = rxSignalsDecimated; % !!!!!

    % Construct N_id_cell
    N_id_cell = 3*N_id_1 + N_id_2;
    fprintf(' N id cell = %d\n', N_id_cell);
    
    PuntosSincronizacion(Indice)=f_start_idx;
    save([RutaResultados IdentificadorResultados],'PuntosSincronizacion');

end