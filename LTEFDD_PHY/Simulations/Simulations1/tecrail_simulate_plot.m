%% Inicializacións básicas

clearvars;
%close all;
format long g
restoredefaultpath;
clc;

%% Basic Paths
currentPath = [fileparts(mfilename('fullpath')) filesep];
%addpath([currentPath 'Parameters'])

repositoryPath = [fileparts(fileparts(fileparts(fileparts(currentPath)))) filesep];
simulatorPath = [repositoryPath '5G/common/'];

% add path of 5G simulator
addpath(genpath(simulatorPath));

% add path of simulation folders
simulationsPath = [repositoryPath 'LTEFDD_PHY/Simulations'];
addpath(genpath(simulationsPath));



%% results path
resultsPath=[repositoryPath '5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/'];
addpath(resultsPath);


%% Results file name
%resultsFilename = [resultsPath IdentificadorResultados];
%resultsFilename = 'results.mat';
resultsFilename = 'Results_simulation_100kmh_ch0_outdoor_1.mat';
%resultsFilename = 'Results_simulation_200kmh_ch0_outdoor_1.mat';


%% SNR data
%SNRpath = [currentPath 'other/'];
%addpath(SNRpath);
SNRfile = 'DatosPotencia.mat';

%% load results
% this will load the variable resultsSimulation
load(resultsFilename);

% load parameters
run(resultsSimulation.paramFile);

% get results and indices
results = resultsSimulation.results;
Indices = arrayfun(@(x) x.Indice, results);

% initialize OFDM data and signal
FBMC_parameters = eval(resultsSimulation.ofdmParamFile);
FBMC_constants = FBMC_pregenerateConstants(FBMC_parameters);

% kilometric point of the antenna.
KP2 = 97.075;

% Obtain speed in km/h from filename
speed = IdentificadorResultados(strfind(IdentificadorResultados, 'kmh')+(-3:-1));
speed = str2double(speed);


%% GPS file and paths
GPSpath = '/home/tomas/extra/GTEC_5G_Simulator_Results/ArtigoCapacidadeFerrovario_AWPL2016/Results';
%GPSpath =  fullfile(filesep, 'media', 'tecrail', 'measData4');
GPSCodePath = [repositoryPath 'LTEFDD_PHY/GPS_Processing'];
addpath(GPSpath);
addpath(GPSCodePath);

GPSfilename = [samplesFilename '.usrpnmea'];


%% Find valid indices
syncPoints = arrayfun(@(x) x.PuntosSincronizacion, results);
syncMode = mode(syncPoints);
validFrameInd = find(abs(syncPoints - syncMode) < 500);
invalidFrameInd = setdiff(1:length(syncPoints), validFrameInd);

% plot invalid frames detected
if 0
    figure();
    plot(validFrameInd, syncPoints(validFrameInd), '.-');
    grid on;
    hold on;
    stem(invalidFrameInd, syncPoints(invalidFrameInd));
    legend('valid Frames', 'invalid Frames');
end

% Update indices and results
Indices_v = Indices(validFrameInd);
results_v = results(validFrameInd);


%% Obtain GPS Data
GPSData = getGPSData(GPSfilename);
t_off = getEndFileOffsetGPSKP(GPSData, tfgps);
if speed == 200
    % add an aditional offset of 1.3 seconds for the 200 km/h case
    t_off = t_off + 1.3;
end
GPSKP = getGPSKP(GPSData, 0, Indices_v/15.36e6, t_off);


%% Plot SNR

% load snr data
% this file contains the variables puntosKmts, pSinal and pRuido
load(SNRfile);

SNRdB_results = arrayfun(@(x) x.SNRdB, results_v);
SNRdB_data = 10*log10(pSinal/pRuido);

figure();
plot(puntosKmts, SNRdB_data, '-');
grid on;
hold on;
plot(GPSKP, SNRdB_results, '-');
xlim([min(puntosKmts) max(puntosKmts)]);
legend('SNR (data)', 'SNR (sim)');
xlabel('Kilometric Point');
ylabel('SNR [dB]');
title('SNR');


%% Plot uncoded BER
% matrix of number of erroneous bits
errmat = arrayfun(@(x) x.OFDM_results.uncodedErrorBits, results_v, ...
                  'UniformOutput', false);
errmat = cat(1, errmat{:});

% matrix of number of erroneous coded bits
errmat_c = arrayfun(@(x) x.OFDM_results.codedErrorBits, results_v, ...
                  'UniformOutput', false);
errmat_c = cat(1, errmat_c{:});

% BER results
modOrder = arrayfun(@(x) x.modulationOrder, FBMC_parameters.dataBlock);
modSet = unique(modOrder);

for m = modSet
    ber.(['qam' num2str(m)]) = ...
        mean(errmat(:,modOrder == m),2)./...
        FBMC_constants.dataBlockBitsNumber(find(modOrder == m, 1, 'first'));
end
berfields = fieldnames(ber);

% find and discard frames with too much BER
berlimit = 0.3;

berind_v = true(size(errmat,1),1);
for ii = 1:length(berfields)
    berind_v = berind_v & (ber.(berfields{ii}) < berlimit);
end

errmat = errmat(berind_v,:);
errmat_c = errmat_c(berind_v,:);
for ii = 1:length(berfields)
    ber.(berfields{ii}) = ber.(berfields{ii})(berind_v);
end
GPSKP = GPSKP(berind_v);

% smooth factor to use for the plots
smoothfactor = 7;
            
% Uncoded BER plots
for ii = 1:length(modSet)
    M = modSet(ii);
    berii = ber.(['qam' num2str(M)]);
    
    figure();
    plot(GPSKP, berii, '-');
    grid on;
    hold on;
    plot(GPSKP, smooth(berii,smoothfactor), '-', 'LineWidth', 2);
    %xlim([min(puntosKmts) max(puntosKmts)]);
    xlim([94.39 101.03]);
    legend(sprintf('BER %d-QAM', M));
    xlabel('Kilometric Point');
    ylabel('BER');
    legend('instantaneous BER', 'averaged BER');
    title(sprintf('BER %d-QAM', M));
end


%% Plot smoother BER for paper
figure('Name', sprintf('Uncoded BER (smoothed) - %d km/h', speed))
hold on;
grid on;
smoothfactor = 7;

legendcell = cell(1,length(modSet));
for ii = 1:length(modSet)
    M = modSet(ii);
    berii = ber.(['qam' num2str(M)]);
    plot(GPSKP, smooth(berii,smoothfactor), '-', 'LineWidth', 2);
    legendcell{ii} = sprintf('%d-QAM', M);
end

switch speed
    case 100
        plot([KP2 KP2], [0 0.03], 'r--');
    case 200
        plot([KP2 KP2], [0 0.03], 'r--');
end

hl = legend(legendcell{:}, 'eNodeB position',...
            'Location', 'NorthWest');
hx = xlabel('Kilometric point');
hy = ylabel('BER');
xlim([94.39 101.03]);

set(hl, 'Interpreter', 'latex', 'FontSize', 12);
set(hx, 'Interpreter', 'latex', 'FontSize', 12);
set(hy, 'Interpreter', 'latex', 'FontSize', 12);


%% Plot max throughput
throughput = zeros(size(errmat_c,1), 1);
blocklen = (72+1024)*4/15.36e6;

for ii = 1:length(throughput);
    % find blocks with zero errors for frame ii
    zeroerrind = find(errmat_c(ii,:) == 0);
    
    if isempty(zeroerrind)
        continue;
    end
    
    % find the block with the maximum number of bits received without error
    [maxbits, maxind] = max(FBMC_constants.dataBlockInputUncodedBitsNumber(zeroerrind));
    throughput(ii) = maxbits/blocklen;
end
throughput = throughput*1e-6;

figure();
plot(GPSKP, throughput);
grid on;
hold on;
%plot(GPSKP, smooth(throughput,7), '-', 'LineWidth', 3);
plot([min(GPSKP) max(GPSKP)], mean(throughput)*[1 1], '-', 'LineWidth', 1);
%xlim([min(puntosKmts) max(puntosKmts)]);
xlim([94.39 101.03]);
xlabel('Kilometric Point');
ylabel('Troughput [Mb/s]');
legend('instantaneous throughput', 'mean throughput');
title('Troughput');

