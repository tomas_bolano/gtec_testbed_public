% Pinta figuras para el paper

%% Inicializacións básicas

clearvars;
close all;
format long g
restoredefaultpath;
clc;

%% Basic Paths
currentPath = [fileparts(mfilename('fullpath')) filesep];
%addpath([currentPath 'Parameters'])

repositoryPath = [fileparts(fileparts(fileparts(fileparts(currentPath)))) filesep];
simulatorPath = [repositoryPath '5G/common/'];

% add path of 5G simulator
addpath(genpath(simulatorPath));

% add path of simulation folders
simulationsPath = [repositoryPath 'LTEFDD_PHY/Simulations'];
addpath(genpath(simulationsPath));


%% results path
resultsPath=[repositoryPath '5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/'];
addpath(resultsPath);


%% Results filenames for 100 and 200 km/h
resultsFilename.v100 = 'Results_simulation_100kmh_ch0_outdoor_1.mat';
resultsFilename.v200 = 'Results_simulation_200kmh_ch0_outdoor_1.mat';

% GPS

%% SNR data
%SNRpath = [currentPath 'other/'];
%addpath(SNRpath);
SNRfile = 'DatosPotencia.mat';


%% Generate plot data for 100 and 200 km/h

% kilometric point of the antenna.
KP2 = 97.075;

% Kilometric points limits of the section where the speed is constant for each case
KP0_100 = 94.39;  % starting point section 100 km/h
KPf_100 = 101.03; % final point section 100 km/h

KP0_200 = 96.30;  % starting point section 200 km/h
KPf_200 = 100.62; % final point section 200 km/h


% identifiers for the different speeds
vidcell = {'v100', 'v200'};

for ii = 1:length(vidcell)
    vid = vidcell{ii};
    
    % this will load the variable resultsSimulation
    load(resultsFilename.(vid));

    % load parameters
    run(resultsSimulation.paramFile);

    % get results and indices
    results = resultsSimulation.results;
    Indices = arrayfun(@(x) x.Indice, results);

    % initialize OFDM data and signal
    FBMC_parameters = eval(resultsSimulation.ofdmParamFile);
    FBMC_constants = FBMC_pregenerateConstants(FBMC_parameters);

    % Obtain speed in km/h from filename
    speed = IdentificadorResultados(strfind(IdentificadorResultados, 'kmh')+(-3:-1));
    speed = str2double(speed);


    % GPS file and paths
    GPSpath = '/home/tomas/extra/GTEC_5G_Simulator_Results/ArtigoCapacidadeFerrovario_AWPL2016/Results';
    %GPSpath =  fullfile(filesep, 'media', 'tecrail', 'measData4');
    GPSCodePath = [repositoryPath 'LTEFDD_PHY/GPS_Processing'];
    addpath(GPSpath);
    addpath(GPSCodePath);
    GPSfilename = [samplesFilename '.usrpnmea'];


    % Find valid indices
    syncPoints = arrayfun(@(x) x.PuntosSincronizacion, results);
    syncMode = mode(syncPoints);
    validFrameInd = find(abs(syncPoints - syncMode) < 500);
    invalidFrameInd = setdiff(1:length(syncPoints), validFrameInd);

    % Update indices and results
    Indices_v = Indices(validFrameInd);
    results_v = results(validFrameInd);


    % Obtain GPS Data
    GPSData = getGPSData(GPSfilename);
    t_off = getEndFileOffsetGPSKP(GPSData, tfgps);
    if speed == 200
        % add an aditional offset of 1.3 seconds for the 200 km/h case
        t_off = t_off + 1.3;
    end
    GPSKP = getGPSKP(GPSData, 0, Indices_v/15.36e6, t_off);


    % Generate SNR --------------------------------------------------------
    % load SNR data
    % this file contains the variables puntosKmts, pSinal and pRuido
    load(SNRfile);
    
    % Kilometric points indices for SNR
    GPSKP_snr.(vid) = GPSKP;
    % SNR from simulations
    SNRdB_meas.(vid) = arrayfun(@(x) x.SNRdB, results_v);
    % SNR from measures
    SNRdB_sim.(vid) = 10*log10(pSinal/pRuido);

    % Generate uncoded BER ------------------------------------------------
    % matrix of number of erroneous bits
    errmat_ = arrayfun(@(x) x.OFDM_results.uncodedErrorBits, results_v, ...
                      'UniformOutput', false);
    errmat_ = cat(1, errmat_{:});

    % matrix of number of erroneous coded bits
    errmat_c_ = arrayfun(@(x) x.OFDM_results.codedErrorBits, results_v, ...
                      'UniformOutput', false);
    errmat_c_ = cat(1, errmat_c_{:});

    % BER results
    modOrder = arrayfun(@(x) x.modulationOrder, FBMC_parameters.dataBlock);
    modSet = unique(modOrder);

    for m = modSet
        ber_.(['qam' num2str(m)]) = ...
            mean(errmat_(:,modOrder == m),2)./...
            FBMC_constants.dataBlockBitsNumber(find(modOrder == m, 1, 'first'));
    end
    berfields = fieldnames(ber_);

    % find and discard frames with too much BER
    berlimit = 0.4;

    berind_v = true(size(errmat_,1),1);
    for jj = 1:length(berfields)
        berind_v = berind_v & (ber_.(berfields{jj}) < berlimit);
    end

    errmat.(vid) = errmat_(berind_v,:);
    errmat_c.(vid) = errmat_c_(berind_v,:);
    for jj = 1:length(berfields)
        ber.(berfields{jj}).(vid) = ber_.(berfields{jj})(berind_v);
    end
    GPSKP_ber.(vid) = GPSKP(berind_v);    

    % Generate throughput -------------------------------------------------
    throughput_ = zeros(size(errmat_c_,1), 1);
    blockmax_ = zeros(size(errmat_c_,1), 1);
    blocklen = (72+1024)*4/15.36e6;

    for jj = 1:length(throughput_);
        % find blocks with zero errors for frame ii
        zeroerrind = find(errmat_c_(jj,:) == 0);
    
        if isempty(zeroerrind)
            continue;
        end
    
        % find the block with the maximum number of bits received without error
        [maxbits, maxind] = max(FBMC_constants.dataBlockInputUncodedBitsNumber(zeroerrind));
        blockmax_(jj) = maxind;
        throughput_(jj) = maxbits/blocklen;
    end
    
    % throughput in Mb/s
    throughput.(vid) = throughput_*1e-6;
    % maximum block index that contributes to the throughput
    blockmax.(vid) = blockmax_;
    % constant speeds masks
    const_speed_mask.(vid) = GPSKP_ber.(vid) > KP0_200 & ...
                             GPSKP_ber.(vid) < KPf_200;
end


% Plot results

% default MATLAB colors
colors = get(groot,'DefaultAxesColorOrder');

%% Plot uncoded ber --------------------------------------------------------
sf = 7; % smooth factor
lw = 2; % line width
ul = 0.08; % upper limit

figure();

h1 = subplot(2,1,1);
hold on;
grid on;
h1ant = plot([KP2 KP2], [0 1], '--k');
legendcell = cell(1,length(modSet));
plotcell = cell(1,length(modSet));
for ii = 1:length(modSet)
    M = modSet(ii);
    berii = ber.(['qam' num2str(M)]).v100;
    plotcell{ii} = plot(GPSKP_ber.v100, smooth(berii,sf), '-', 'LineWidth', lw);
    legendcell{ii} = sprintf('%d-QAM', M);
end
xlim([KP0_100 KPf_100]);
ylim([0 ul]);
hl1 = legend([plotcell{:}], legendcell,...
             'Location', 'NorthEast');
hy1 = ylabel('BER');
ht1 = title('$v_{\mathrm{max}} = 100$\,km/h');

xlim([KP0_100 KPf_100]);

set(hl1, 'Interpreter', 'latex', 'FontSize', 12);
set(hy1, 'Interpreter', 'latex', 'FontSize', 12);
set(ht1, 'Interpreter', 'latex', 'FontSize', 12);
%set(gca,'xticklabel',[]);

h2 = subplot(2,1,2);
hold on;
grid on;
plot([KP2 KP2], [0 1], '--k');

% create patchs
x = [KP0_100 KP0_200 KP0_200 KP0_100];
y = [0     0     1     1]*ul;
patch(x,y,colors(6,:), 'EdgeColor', colors(6,:));

x = [KPf_200 KPf_100 KPf_100 KPf_200];
y = [0     0     1     1]*ul;
patch(x,y,colors(6,:), 'EdgeColor', colors(6,:));

for ii = 1:length(modSet)
    M = modSet(ii);
    berii = ber.(['qam' num2str(M)]).v200;
    plot(GPSKP_ber.v200, smooth(berii,sf), '-', 'LineWidth', lw);
end
xlim([KP0_100 KPf_100]);
ylim([0 ul]);
hx2 = xlabel('Kilometric point [km]');
hy2 = ylabel('BER');
ht2 = title('$v_{\mathrm{max}} = 200$\,km/h');

set(hx2, 'Interpreter', 'latex', 'FontSize', 12);
set(hy2, 'Interpreter', 'latex', 'FontSize', 12);
set(ht2, 'Interpreter', 'latex', 'FontSize', 12);

% widen subplots
% P1 = get(h1, 'Position');
% P2 = get(h2, 'Position');
% W = 0.0; % width factor
% H = 0.0; % height factor
% set(h1, 'Position', P1+[0 -H 0 H]);
% set(h2, 'Position', P2+[0 0 0 H]);


%% Plot thoughput ----------------------------------------------------------
sf = 7; % smooth factor
lw = 2; % line width
ul = 65; % upper limit
ll = 20; % lower limit

figure();

h1 = subplot(2,1,1);
hold on;
grid on;
h1ant = plot([KP2 KP2], [0 1], '--k');
h1p1 = plot(GPSKP_ber.v100, throughput.v100, 'LineWidth', 1);
h1p3 = plot([KP0_200 KPf_200],...
            mean(throughput.v100(const_speed_mask.v100))*[1 1], 'LineWidth', 1);
xlim([KP0_100 KPf_100]);
ylim([ll ul]);
hl1 = legend([h1p1 h1p3], {'instantaneous throughput', 'mean throughput'},...
            'Location', 'SouthEast');
hy1 = ylabel('throughput [Mb/s]');
ht1 = title('$v_{\mathrm{max}} = 100$\,km/h');

set(hl1, 'Interpreter', 'latex', 'FontSize', 12);
set(hy1, 'Interpreter', 'latex', 'FontSize', 12);
set(ht1, 'Interpreter', 'latex', 'FontSize', 12);
%set(gca,'xticklabel',[]);

h2 = subplot(2,1,2);
hold on;
grid on;
plot([KP2 KP2], [0 1], '--k');

% create patchs
x = [KP0_100 KP0_200 KP0_200 KP0_100];
y = [ll    ll    ul    ul];
patch(x,y,colors(6,:), 'EdgeColor', colors(6,:));

x = [KPf_200 KPf_100 KPf_100 KPf_200];
y = [ll    ll    ul    ul];
patch(x,y,colors(6,:), 'EdgeColor', colors(6,:));

plot(GPSKP_ber.v200, throughput.v200, 'LineWidth', 1);
plot([KP0_200 KPf_200],...
       mean(throughput.v200(const_speed_mask.v200))*[1 1], 'LineWidth', 1);
xlim([KP0_100 KPf_100]);
ylim([ll ul]);
hx2 = xlabel('Kilometric point [km]');
hy2 = ylabel('throughput [Mb/s]');
ht2 = title('$v_{\mathrm{max}} = 200$\,km/h');

set(hx2, 'Interpreter', 'latex', 'FontSize', 12);
set(hy2, 'Interpreter', 'latex', 'FontSize', 12);
set(ht2, 'Interpreter', 'latex', 'FontSize', 12);

% widen subplots
% P1 = get(h1, 'Position');
% P2 = get(h2, 'Position');
% W = 0.0; % width factor
% H = 0.0; % height factor
% set(h1, 'Position', P1+[0 -H 0 H]);
% set(h2, 'Position', P2+[0 0 0 H]);

%% Plot block error probability for the different CQI ---------------------

CQIind = 1:15; % CQI indices

figure();
h1 = subplot(2,1,1);
hold on;
grid on;

% we calculate the block errors for the same section for both speeds,
% for 100 we use the section where the speed is constant in 200 km/h
frind = GPSKP_ber.v100 > KP0_200 & GPSKP_ber.v100 < KPf_200;
%frind = true(size(GPSKP_ber.v100));
errprob100 = 1-sum(errmat_c.v100(frind,CQIind) == 0)/nnz(frind);
bar(CQIind, errprob100, 'FaceColor', colors(1,:));

set(h1, 'XTick', CQIind); % set X ticks
set(h1, 'YTick', 0:0.2:1); % set Y ticks
hy1 = ylabel('Block error probability');
ht1 = title('$v_{\mathrm{max}} = 100$\,km/h');
set(hy1, 'Interpreter', 'latex', 'FontSize', 12);
set(ht1, 'Interpreter', 'latex', 'FontSize', 12);
ylim([0 1]);


h1 = subplot(2,1,2);
hold on;
grid on;
frind = GPSKP_ber.v200 > KP0_200 & GPSKP_ber.v200 < KPf_200;
%frind = true(size(GPSKP_ber.v200));
errprob200 = 1-sum(errmat_c.v200(frind,CQIind) == 0)/nnz(frind);
bar(CQIind, errprob200, 'FaceColor', colors(1,:));

set(h1, 'XTick', CQIind); % set X ticks
set(h1, 'YTick', 0:0.2:1); % set Y ticks
hy1 = ylabel('Block error probability');
hx1 = xlabel('CQI index');
ht1 = title('$v_{\mathrm{max}} = 200$\,km/h');
set(hx1, 'Interpreter', 'latex', 'FontSize', 12);
set(hy1, 'Interpreter', 'latex', 'FontSize', 12);
set(ht1, 'Interpreter', 'latex', 'FontSize', 12);
ylim([0 1]);
