% Parámetros básicos de execución e sincronización

IdentificadorResultados='Results_simulation_100kmh_ch0_outdoor';
samplesFilename='2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp';
Canle = 1;
t0=0; % Creo que un rango bo sería de 115 a 130 s
tf=335;
t0gps=139;
tfgps=474;
DesprazamentoInicial = 15360+1;
TramasPorAnalise = 2;
NumeroSector=1:2;
TramasSeparacion=25;

% Parámetros de estima e interpolación de canle
cec.PilotAverage = 'UserDefined';
cec.FreqWindow = 1;
cec.TimeWindow = 1;
cec.InterpType = 'v4';
cec.InterpWindow = 'Centered';
cec.InterpWinSize = 3;