% Parámetros básicos de execución e sincronización

IdentificadorResultados='Results_simulation_200kmh_ch0_outdoor';
samplesFilename='2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.nosplit.usrp';
Canle = 1;
t0=0; % Creo que un rango bo sería de 115 a 130 s
tf=200;
t0gps=145;
tfgps=345;
DesprazamentoInicial = 15360+1;
TramasPorAnalise = 2;
NumeroSector=1:2;
TramasSeparacion=25;

% Parámetros de estima e interpolación de canle
cec.PilotAverage = 'UserDefined';
cec.FreqWindow = 1;
cec.TimeWindow = 1;
cec.InterpType = 'v4';
cec.InterpWindow = 'Centered';
cec.InterpWinSize = 3;