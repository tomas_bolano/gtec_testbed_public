%% OFDM simulations

clear variables;
close all;
format long g
restoredefaultpath;
clc;

%% Parameters files

% simulation parameters
paramFile = 'TECRAIL_lte_RX_ADIF_Analysis_Parametros_100_f_0';
%paramFile = 'TECRAIL_lte_RX_ADIF_Analysis_Parametros_200_f_0';

% ofdm generation parameters
ofdmParamFile = 'FBMC_parameters_ofdm2';

%% Paths
currentPath = [fileparts(mfilename('fullpath')) filesep];
%addpath([currentPath 'Parameters'])

repositoryPath = [fileparts(fileparts(fileparts(fileparts(currentPath)))) filesep];
simulatorPath = [repositoryPath '5G/common/'];

% add path of 5G simulator
addpath(genpath(simulatorPath));

% add path of simulation folders
simulationsPath = [repositoryPath 'LTEFDD_PHY/Simulations'];
addpath(genpath(simulationsPath));


%% Configuration
nworkers = 16; % number of workers

% configuration for the channel estimation
% WARNING: FOR no ISI ntaps_ac + ntaps_c must be less than 72
ntaps_c = 55; % number of causal taps to use
ntaps_ac = 10; % number of anticausal taps to use
smoothfactor = 1; % smoothing factor for the channel taps


%% Inicialization

% offset for no ISI window
ofdm_noisi_offset = ntaps_ac+round((72-ntaps_c-ntaps_ac)/2);
assert(ofdm_noisi_offset >= 0 & ofdm_noisi_offset <= 72);

% percentage of cp to use (to avoid anticausal interference)
ofdm_cpfactor = (72-ofdm_noisi_offset)/72;
assert(ofdm_cpfactor >= 0 & ofdm_cpfactor <= 1);

% load parameters
run(paramFile);

% add path of GTIS code
addpath([repositoryPath 'gtis/matlab/usrpfileformat/'])
addpath([repositoryPath 'gtis/matlab/utils/'])
addpath([repositoryPath 'gtis/examples/lte/'])

% add path of 5G simulator
addpath(genpath(simulatorPath));

% measurements path
%basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData4');
basePathForDataFiles = '/home/tomas/extra/measurements/ADIFMeasurements';
rxSignalsInUSRPFormatFilename = fullfile(basePathForDataFiles, samplesFilename);

% Some constants
Ntx = 2;
Nrx = 2;
Sector = [3 4];

% LTE constants (10 MHz)
nSamplesSlot = 15360/2;
nSymbolsSlot = 14/2;
nSamplesSubframe = nSamplesSlot * 2;
nSymbolsSubframe = nSymbolsSlot * 2;
nSamplesFrame = nSamplesSubframe * 10;
nSymbolsFrame = nSymbolsSubframe * 10;
nHalfSamplesSlot = nSamplesSlot / 2;
nFFT = 2048/2;
usedSubcarriers=600;
guardSubcarriers=(nFFT-usedSubcarriers)/2;
windowLength = 40/2;
longCPlength = nFFT*10/128;
shortCPlength = nFFT*9/128;
longOFDMsymbolLength = nFFT + longCPlength;
shortOFDMsymbolLength = nFFT + shortCPlength;
NsymbSlot = 7;
fs=15.36e6;
enbC.NDLRB=50;
enbC.CyclicPrefix='Normal';
enbC.DuplexMode='FDD';

enbC.CellRefP = 1;  % One transmit antenna port
enbC.LookForAllNCellIDs = false;
enbC.Ng = 'One';
enbC.CFI = 2;
enbC.PHICHDuration = 'Normal';


% some other variables
samples = nSamplesFrame*TramasPorAnalise;
s_start = t0*fs+DesprazamentoInicial-nSamplesSubframe; % start sample
s_end = tf*fs; % end sample
Indices = s_start:nSamplesFrame*TramasSeparacion:s_end;
% enb.NCellID=Sector(NumeroSector);

Indice = 0;
if(exist([IdentificadorResultados '.mat'],'file'))
%     load(IdentificadorResultados);
    disp('Results will be overwritten...');
    disp('Press any key to proceed or Ctrl+C to cancel...');
    pause;
end

%% Main Loop

% initialize worker data
workerIndCell = cell(1, nworkers);

N = floor(length(Indices)/nworkers);
for ii = 1:nworkers-1
    workerIndCell{ii} = (ii-1)*N+1:ii*N;
end
workerIndCell{end} = ii*N+1:length(Indices);

% initialize OFDM data and signal
FBMC_parameters = eval(ofdmParamFile);
FBMC_constants = FBMC_pregenerateConstants(FBMC_parameters);
FBMC_txData = FBMC_transmitter(FBMC_parameters, FBMC_constants);

% number of total and used subcarriers
nsubc = FBMC_parameters.basicParameters.subcarriersNumber;
nused = nnz(FBMC_constants.usedSubcarriers.subcarrierMask);

% transmitted signal for the simulations
tx_sim = FBMC_txData.interpolatedSignal;

% Update demodulator function to set the percentage of the cp symbol
% to use in the demodulation
FBMC_parameters.demodulator.demodulatorFunction = ...
    {@FBMC_ofdmcpDemodulator_2, FBMC_constants.totalTimeSymbolsNumber,...
     ofdm_cpfactor};

% noise power - see plot_channel_power script
noisepw_db = 22;
noisepw = 10^(noisepw_db/10);
no = noisepw*nsubc/nused; % noise power spectral density

% variables to save data
resultsCell = cell(1,nworkers);

globaltimer = tic();
parfor ii = 1:nworkers
    workerInd = workerIndCell{ii};
    results = repmat(struct(), 1, length(workerInd));
    FBMC_parameters_par = FBMC_parameters;
    
    timebuf = zeros(1,3);
    for jj = 1:length(workerInd)
        timer = tic();
        
        Indice = workerInd(jj);
        
        enb = enbC;
        sample_jj = Indices(Indice);

        % read measurements file
        [readSignalComplex, success, totalNumSamplessProcessedPerChannel, ...
         totalNumBytesProcessedPerChannel, numChannels] = ...
            GTIS_readUSRPMultichannelSignalsFromFile(...
                rxSignalsInUSRPFormatFilename, samples, sample_jj);
    
        if ~success
            error('error reading file');
        end
        
        if totalNumSamplessProcessedPerChannel < samples
            error('file read function returned less samples than requested');
        end
        
        rxSamples = readSignalComplex(Canle, 1:end);
        
        % estimate and correct frequency offset
        foffset = lteFrequencyOffset(enb, rxSamples.');
        rxSamples = lteFrequencyCorrect(enb,rxSamples.',foffset);
        
        % detect cell id and estimate time offset
        [N_id_cell, f_start_idx] = lteCellSearch(enb, rxSamples, Sector(NumeroSector));
        
        % demodulate received frame
        enb.NCellID = N_id_cell;
        rxSamples = rxSamples(1+f_start_idx+(0:(nSamplesFrame-1)),:);
        rxGrid = sqrt(usedSubcarriers)/nFFT*lteOFDMDemodulate(enb, rxSamples);
        
        % estimate frame channel
        enb.NSubframe = 0;
        [estChannel, noiseEst] = lteDLChannelEstimate(enb, cec, rxGrid);
        
        % convert channel to time domain coefficients
        coef = tecrail_ltechfreq2time(estChannel, ntaps_c, ntaps_ac, smoothfactor);
        
        %% apply channel
        rx_sim = zeros(size(tx_sim));
        for kk = 1:size(coef,2)
            rx_sim(kk:end) = rx_sim(kk:end) + tx_sim(1:end-kk+1).*coef(1:numel(tx_sim)-kk+1,kk);
        end
        
        % add noise
        noisevec = sqrt(no/2)*(randn(size(tx_sim)) + 1j*randn(size(tx_sim)));
        rx_sim_noise = rx_sim + noisevec;
        
        % update synchronization function to correct time and freq offset
        % the freq offset is zero because it was already corrected when
        % estimating the lte channel
        FBMC_parameters_par.synchronization.synchronizationFunction = ...
            {@FBMC_fixedSynchronizator, ntaps_ac, 0};
        
        close all;
        % process received signal
        rxData = FBMC_receiver(FBMC_parameters_par, FBMC_constants, rx_sim_noise,...
                               FBMC_txData.dataBlockCodedBitsNumber, no);
                           
        FBMC_results = FBMC_systemIterationResults(FBMC_parameters_par,...
                           FBMC_constants, FBMC_txData, rxData);

        results(jj).PuntosSincronizacion = f_start_idx;
        results(jj).IndicesCela = N_id_cell;
        results(jj).Indice = sample_jj;
        results(jj).SNRdB = 10*log10(var(rx_sim)/noisepw);
        results(jj).OFDM_results = FBMC_results;

        % TEST
        if 0
            % plot channels
            figure();
            surf(abs(estChannel));
            title('LTE estimated channel');
            shading flat;
            view([0 90]);
            colorbar();

            figure();
            FBMC_plotEstimatedChannel(FBMC_parameters, FBMC_constants,...
                                      FBMC_txData, rxData,...
                                      'PilotPoints', false);
            title('Simulated channel');
            shading flat;
            view([0 90]);
            ylim([-300 300]);
            xlim([0 140]);
            colorbar();
            
            % plot constellations
            const4qam = rxData.equalizedDataGrid(...
                            FBMC_constants.dataBlockMask >= 1 &...
                            FBMC_constants.dataBlockMask <= 4);
            const16qam = rxData.equalizedDataGrid(...
                            FBMC_constants.dataBlockMask >= 5 &...
                            FBMC_constants.dataBlockMask <= 8);
            const64qam = rxData.equalizedDataGrid(...
                            FBMC_constants.dataBlockMask >= 9 &...
                            FBMC_constants.dataBlockMask <= 12);

            figure();   
            plot(real(const4qam), imag(const4qam), '.', 'MarkerSize', 1');
            title('4-QAM');
            
            figure();   
            plot(real(const16qam), imag(const16qam), '.', 'MarkerSize', 1');
            title('16-QAM');
            
            figure();
            plot(real(const64qam), imag(const64qam), '.', 'MarkerSize', 1');
            title('64-QAM');
            
            disp(results(jj).OFDM_results)
            disp(results(jj).SNRdB)
            pause();
        end
        
        time = toc(timer);
        timebuf = [time timebuf(1:end-1)];
        
        msgstr = sprintf('[%2d] (%4d/%4d) %6.2f %%', ii, jj,...
                         length(workerInd), jj/length(workerInd)*100);
        if jj >= length(timebuf)
            msgstr = sprintf('%s | est. time remaining: %6.2f s',...
                             msgstr, mean(timebuf)*(length(workerInd)-jj));
        end
        
        fprintf('%s\n', msgstr);
    end
    
    resultsCell{ii} = results;
end
toc(globaltimer);

%% save results
resultsSimulation.paramFile = paramFile;
resultsSimulation.ofdmParamFile = ofdmParamFile;
resultsSimulation.results = cat(2, resultsCell{:});
resultsSimulation.no = no;
resultsSimulation.noisepw = noisepw;

save(IdentificadorResultados, 'resultsSimulation');

