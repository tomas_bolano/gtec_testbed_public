% simulate2 plot
% plot different results of estimate channels

clearvars -except resultsSimulation estchantime correctsymbols...
                  toffvec toffvec0 toffvec1 tauoff toffreg toffregvec...
                  estchanguards;
%close all;
format long g
restoredefaultpath;
%clc;



%% Initialize paths
currentPath = [fileparts(mfilename('fullpath')) filesep];
addpath([currentPath 'processing'])

% path where results are stored
%addpath([currentPath 'results'])
addpath('/home/tomas/extra/artigomedidastren_wcmc2017/results/estchan_100kmh/',...
        '/home/tomas/extra/artigomedidastren_wcmc2017/results/estchan_200kmh/');





%% Configuration

% file with the saved channels and associated data
%estchan_results = 'estchan_100kmh_kp1';
%estchan_results = 'estchan_100kmh_kp2';
%estchan_results = 'estchan_100kmh_kp3';
%estchan_results = 'estchan_200kmh_kp2';

estchan_results = 'estchan_100kmh_kp1';

% Plot configuration
plot_chan = 0;
plot_kfac = 0;
plot_dop  = 0;
plot_pdp  = 1;
plot_ds   = 0;
plot_pw   = 0;
plot_1st_tap = 0; % first tap

% Channel guard completion configuration
% available values:
%   copy - copy the channel in the outer subcarriers to the guards
%   inter - interpolate the channel on the outer subcarriers
%   0 - do nothing
guardmethod = 0;
%guardmethod = 'hanning';
%guardmethod = 'copy';
%guardmethod = 'interp';
guardmethod = 'mmse';
guard_debug = 0;

% options for mmse channel guard completion
guard_mmse_N = 6;
guard_mmse_ntaps_c = 240;
guard_mmse_ntaps_ac = 130;
guard_mmse_no = 1e-2;
guard_mmse_onlyguards = 0;

% method for covariance estimation for mmse
%guard_mmse_covcalc = 'default';
%guard_mmse_covcalc = 'hanning';
guard_mmse_covcalc = 'hanning_used';
%guard_mmse_covcalc = 'interp';

% dithering options
dither_type = 'SNR'; % dithering type value indicates the SNR in dB
%dither_type = 'noise'; % dithering type value indicates the noise in dB
%dither_value = inf;
dither_value = 70; % For SNR, 70 dB is a good value when using mmse
%dither_value = 50; % For noise, 50 dB is a good value when using mmse (?)

% delay offset correction
% when this option is one the offset of the frames will be set so the LoS
% delay for each frame is 0
offsetcorrect = 0;

% Channel configuration
chan_showf = 'real';
chan_tapi = 1;

% K-Factor configuration
kfac_alpha = 140*1.5;
kfac_tapind = 1;
kfac_step = 140*1;

% Doppler PSD configuration
dop_step = 256;
dop_alpha = 1024;
%dop_alpha = 512; % for 200 km/h
dop_nfft = 1024*8;
dop_fmax = 400;
%dop_fmax = 800; % for 200 km/h

dop_ntaps_c = 30;  % only used for the dop_tapind vector
dop_ntaps_ac = 15; % only used for the dop_tapind vector
dop_tapind = [1:dop_ntaps_c 1024-dop_ntaps_ac+1:1024];

%dop_tapind = [1];

dop_normalize = 1;
dop_window = 'none'; % name of window, or 'none'
dop_calctype = 'fft';

% PDP configuration
pdp_step = 128;
pdp_alpha = 140;
pdp_ntaps_c = 75;
pdp_ntaps_ac = 30;
pdp_normalize = 1;
pdp_plotwithdelay = 0;

% Delay spread configuration
ds_step = 128;
ds_alpha = 140;
ds_ntaps_c = 65;
ds_ntaps_ac = 10;



%% Load channels

% load saved channels
% this wil load the variable resultsSimulation
reloaded_channels = 0;
if exist('resultsSimulation', 'var')
    res = input('reload estimated channels [y|N] ', 's');
else
    res = 'y';
end

if strcmpi(res, 'y')
    reloaded_channels = 1;
    timer = tic();
    fprintf('Loading estimated channels... ');
    load(estchan_results);
    fprintf('OK (%5.2f s)\n', toc(timer));
end


%% Obtain channels in time domain
if ~exist('estchantime', 'var') || reloaded_channels
    res = 'y';
else
    res = input('Recalculate channel in time domain [y|N] ', 's');
end

if strcmpi(res, 'y')
    timer = tic();
    fprintf('Finding corectly synchronized frames... ');
    correctframes = process_findcorrectframes(resultsSimulation);
    fprintf('OK (%5.2f s)\n', toc(timer));

    timer = tic();
    fprintf('Estimating offsets... ');
    syncidx = arrayfun(@(x) x.f_start_idx, resultsSimulation.results);
    toffvec0 = process_calculate_time_off(syncidx);
    fprintf('OK (%5.2f s)\n', toc(timer));
    
    % do a linear regression of the offsets
    toffreg = robustfit(1:length(toffvec0), toffvec0);
    toffregvec = toffreg(2)*(1:length(toffvec0)) + toffreg(1);
    
    if offsetcorrect
        % Correct the offset of the frames so the LoS delay is 0
        
        % the offsets in toffvec give us a first approximation of the
        % actual LoS offset of the signal
        toffvec = toffvec0 - toffregvec;
        
        % Estimate the LoS offset of the frames, for this we use the first
        % approximation given by toffvec
        fprintf('Estimating mean offset... ');
        if resultsSimulation.parameters.speed == 100 &&...
           resultsSimulation.sectionkpind == 2
           tauoff_m = 0.0015;
           tauoff_b = -2.75;
           tauoff = tauoff_m*(1:size(resultsSimulation.estchan,3)) + tauoff_b;
        elseif resultsSimulation.parameters.speed == 200 &&...
               resultsSimulation.sectionkpind == 2
           tauoff_m = 0.0015;
           tauoff_b = -2.75;
           tauoff = tauoff_m*(1:size(resultsSimulation.estchan,3)) + tauoff_b;
        else
            [tauoff, tauoff_m, tauoff_b] = ...
                process_calculate_reg_time_off(resultsSimulation.estchan,...
                    toffvec, correctframes, 5);
        end
        fprintf('OK (%5.2f s)\n', toc(timer));
        
        % estimated total LoS offsets
        toffvec1 = toffvec - tauoff;
        
        % For the case of offsetcorrect = 1 the channels delays are first
        % corrected and then the guard are added. This is important since
        % the delays are not integer numbers and adding them after
        % completing the guard band can introduce some effects due to
        % "discontinuities" in the phase.
        timer = tic();
        fprintf('Correcting channel delays...');
        estchan_off0 = zeros(size(resultsSimulation.estchan));
        w = [-300:-1 1:300]; % discrete frequency indexes
        for ii = 1:size(estchan_off0,3)
            estchan_off0(:,:,ii) = bsxfun(@times,...
                resultsSimulation.estchan(:,:,ii),...
                exp(-1j*2*pi*w.'/1024*toffvec1(ii)));
        end
        fprintf('OK (%5.2f s)\n', toc(timer));
        
        % complete guard bands with the selected method
        fprintf('Completing guard bands... ');
        estchanguards = process_complete_guards(estchan_off0,...
                            guardmethod, guard_mmse_N, guard_mmse_ntaps_c,...
                            guard_mmse_ntaps_ac, guard_mmse_no,...
                            guard_mmse_onlyguards, guard_mmse_covcalc,...
                            guard_debug);
        fprintf('OK (%5.2f s)\n', toc(timer));
        
        % clear estchan_off0, since it is no longer neccesary
        clear estchan_off0;
        
        % Obtain time domain channels
        timer = tic();
        fprintf('Obtaining time domain channels... ');
        estchantime = process_chan_freq2time(estchanguards, [], ...
                                             dither_value, dither_type);
        fprintf('OK (%5.2f s)\n', toc(timer));
    else
        % Complete the guard bands using the selected method
        timer = tic();
        fprintf('Completing guard bands... ');
        estchanguards = process_complete_guards(resultsSimulation.estchan,...
                            guardmethod, guard_mmse_N, guard_mmse_ntaps_c,...
                            guard_mmse_ntaps_ac, guard_mmse_no,...
                            guard_mmse_onlyguards, guard_mmse_covcalc,...
                            guard_debug);
        fprintf('OK (%5.2f s)\n', toc(timer));
        
        % Obtain time domain channels. Here the offsets in toffvec0 are
        % integers numbers
        timer = tic();
        fprintf('Obtaining time domain channels... ');
        estchantime = process_chan_freq2time(estchanguards, toffvec0,...
                                             dither_value, dither_type);
        fprintf('OK (%5.2f s)\n', toc(timer));
    end
    
    % from correctframes obtain a vector of the correct symbols
    correctsymbols = correctframes;
    correctsymbols = repmat(correctsymbols, 140, 1);
    correctsymbols = correctsymbols(:);
end


%% TEST
% 
% ch_test = resultsSimulation.estchan(:,:,300);
% ch_test = bsxfun(@times, ch_test, exp(1j*2*pi*[-300:-1 1:300]/1024*-4.7).');
% ch_time_test = ifft(ch_test,1024);
% 
% figure();
% plot(linspace(-512,512,1024), ifftshift(mean(abs(ch_time_test),2).^2));
% xlim([-5 20]);

%%
% figure();
% for ii = 1:20
%     plot(abs(estchanguards(:,ii,15)))
%     %plot(fftshift(abs(estchanguards(:,ii,1))))
%     pause();
% end

%% Figure of delays kappa^l
if 0
    figure();
    plot(-(toffvec0 - toffvec0(1)), '.');
    grid on;
    hold on;
    plot(-(toffregvec - toffvec0(1)), '--');
    xlabel('frame index ($l$)', 'Interpreter', 'latex');
    ylabel('delay ($\kappa^l$) [samples]', 'Interpreter', 'latex');
    h = legend('delay for each frame', 'regression line fit',...
       'Location', 'northwest');
    set(h, 'Interpreter', 'latex');
end


%% Plot figures

% auxiliar functions to plot CDFs
cdfedges = @(x) [min(x)-2 x(1) x max(x)+2];
cdfn = @(x) [0 0 cumsum(x) 1 1];
cdfplot = @(varargin) stairs(cdfedges(varargin{1}), cdfn(varargin{2}),...
                             varargin{3:end});


% obtain vectors of times and KPs
x = 70 + (1:140:size(estchantime,1));
xq = 1:size(estchantime,1);

t = interp1(x, resultsSimulation.t_v, xq, 'spline');
GPSKP = interp1(x, resultsSimulation.GPSKP_v, xq, 'spline');


%% Channel ----------------------------------------------------------------
if plot_chan
    figure();
    chanf = str2func(chan_showf);
    plot(GPSKP, chanf(estchantime(:, chan_tapi)));
    title(sprintf('channel tap %d', chan_tapi));
    xlabel('Kilometric point [km]');
    ylabel(sprintf('amplitude - %s', chan_showf));
    xlim([min(GPSKP) max(GPSKP)]);
end


%% K-Factor ---------------------------------------------------------------
if plot_kfac
    timer = tic();
    fprintf('Calculating K-Factor... ');
    [kfacvec, kfacidx] = process_k_factor(estchantime, correctsymbols,...
                                          kfac_step, kfac_alpha, kfac_tapind);
    fprintf('OK (%5.2f s)\n', toc(timer));

    figure();
    plot(GPSKP(kfacidx), 10*log10(kfacvec));
    grid on;
    title('K-Factor');
    xlabel('Kilometric point [km]');
    ylabel('K-Factor [dB]');

    % K-Factor CDF
    kfac_nbins = 100;
    [kfac_cdf_n, kfac_cdf_edges] = ...
            histcounts(10*log10(kfacvec), kfac_nbins,...
                       'Normalization', 'probability');

    kfac_mean = mean(10*log10(kfacvec));
    kfac_var = var(10*log10(kfacvec));
    fprintf('K-Factor:\n');
    fprintf('    mean: %f\n', kfac_mean);
    fprintf('    var:  %f\n', kfac_var);

    x = linspace(-30, 45, 1000);
    cdfx = cdf('Normal', x, kfac_mean, sqrt(kfac_var));

    figure();
    cdfplot(kfac_cdf_edges, kfac_cdf_n);
    hold on;
    grid on;
    plot(x, cdfx, '--k', 'LineWidth', 2);
    title('K-Factor CDF');
    xlabel('K-Factor [dB]');
    ylabel('Probability (K-Factor < abscisa)');
    ylim([0 1]);
    xlim([-30 45]);
end


%% Doppler PSD ------------------------------------------------------------
if plot_dop
    timer = tic();
    fprintf('Calculating Doppler PSD... ');
    [dop_psdmat, dopidx, dop_f] = process_doppler_psd(estchantime,...
        correctsymbols, dop_step, dop_alpha, dop_nfft, dop_fmax,...
        dop_tapind, dop_normalize, dop_window, dop_calctype);
    fprintf('OK (%5.2f s)\n', toc(timer));


    figure('name', 'Doppler PSD');
    surf(GPSKP(dopidx), dop_f, 10*log10(dop_psdmat));
    shading interp
    view([0 90]);
    grid off;
    h_xlabel = xlabel('Kilometric point [km]', 'Interpreter', 'Latex');
    h_ylabel = ylabel('Doppler frequency [Hz]', 'Interpreter', 'Latex');
    colorbar();
    %xlim([min(GPSKP(dopidx)) max(GPSKP(dopidx))]);
    xlim(resultsSimulation.sectionkp);
    caxis([-55 -5]);
    
    set(findall(gcf,'-property','FontSize'),'FontSize',14);
    set(h_xlabel,'FontSize',18);
    set(h_ylabel,'FontSize',18);

    
    % mean PSD
    figure('name', 'Mean Doppler PSD');
    plot(dop_f, 10*log10(mean(dop_psdmat,2)));
    grid on;
    h_xlabel = xlabel('Doppler frequency [Hz]', 'Interpreter', 'Latex');
    h_ylabel = ylabel('mean power [dB]', 'Interpreter', 'Latex');
    xlim([min(dop_f) max(dop_f)]);
    
    set(findall(gcf,'-property','FontSize'),'FontSize',14);
    set(h_xlabel,'FontSize',18);
    set(h_ylabel,'FontSize',18);
    ylim([-60 0]);
    
    % mean PSD
    if resultsSimulation.sectionkpind == 2
        dopkp = GPSKP(dopidx);
    
        figure('name', 'Mean Doppler PSD (KP < 98.40)');
        plot(dop_f, 10*log10(mean(dop_psdmat(:, dopkp < 98.40),2)));
        grid on;
        xlabel('Doppler frequency [Hz]');
        ylabel('mean power [dB]');
        xlim([min(dop_f) max(dop_f)]);
    end
end


%% PDP --------------------------------------------------------------------
if plot_pdp
    timer = tic();
    fprintf('Calculating PDP... ');
    [pdpmat, pdpidx, pdp_t] = process_pdp(estchantime,...
        correctsymbols, pdp_step, pdp_alpha, pdp_ntaps_c, pdp_ntaps_ac,...
        pdp_normalize);
    fprintf('OK (%5.2f s)\n', toc(timer));


    figure('name', 'PDP');
    surf(GPSKP(pdpidx), pdp_t*1e6, 10*log10(pdpmat));
    shading interp
    view([0 90]);
    grid on;
    h_xlabel = xlabel('Kilometric point [km]', 'Interpreter', 'Latex');
    h_ylabel = ylabel('Delay [$\mu s$]', 'Interpreter', 'Latex');
    colorbar();
    %xlim([min(GPSKP(pdpidx)) max(GPSKP(pdpidx))]);
    xlim(resultsSimulation.sectionkp);
    ylim([min(pdp_t) max(pdp_t)]*1e6);
    if pdp_normalize
        caxis([-50 -5]);
    end
    
    set(findall(gcf,'-property','FontSize'),'FontSize',14);
    set(h_xlabel,'FontSize',18);
    set(h_ylabel,'FontSize',18);
    
    % TEST
    %x = 0:size(pdpmat,1)-1;
    %plot(x, 10*log10(pdpmat(:,1)));
    %grid on;
    %hold on;
    %N = 2;
    %x = 0:size(pdpmat,1)*N-1;
    %plot(x/N, 10*log10(interp(pdpmat(:,1),2)));
    
    % mean PDP
    figure();
    plot(pdp_t*1e6, 10*log10(mean(pdpmat,2)));
    grid on;
    title('Mean PDP [dB]');
    xlabel('Delay [\mus]');
    ylabel('mean power [dB]');
    xlim([min(pdp_t) max(pdp_t)]*1e6);
    
%     %% mean PDP
%     figure();
%     plot(pdp_t*1e6, mean(pdpmat,2));
%     hold on;
%     x = 0:0.001:5;
%     plot(x, 0.8*exp(-x/0.035));
%     grid on;
%     title('Mean PDP');
%     xlabel('Delay [\mus]');
%     ylabel('mean power');
%     xlim([min(pdp_t) max(pdp_t)]*1e6);
%     
%     %% mean PDP
%     figure();
%     plot(pdp_t*1e6, 10*log10(mean(pdpmat,2)));
%     hold on;
%     x = 0:0.001:5;
%     plot(x, 10*log10(0.8*exp(-x/0.035)));
%     grid on;
%     title('Mean PDP');
%     xlabel('Delay [\mus]');
%     ylabel('mean power');
%     xlim([min(pdp_t) max(pdp_t)]*1e6);
end


%% Delay Spread -----------------------------------------------------------
if plot_ds
    timer = tic();
    fprintf('Calculating delay spread... ');
    [dsvec, dsidx] = process_delay_spread(estchantime,...
        correctsymbols, ds_step, ds_alpha, ds_ntaps_c, ds_ntaps_ac);
    fprintf('OK (%5.2f s)\n', toc(timer));


    figure();
    plot(GPSKP(dsidx), dsvec*1e6);
    shading interp
    grid on;
    title('Delay spread');
    xlabel('Kilometric point [km]');
    ylabel('Delay spread [\mus]');
    %xlim([min(GPSKP(pdpidx)) max(GPSKP(pdpidx))]);
    %ylim([min(pdp_t) max(pdp_t)]*1e6);
    
    % Delay spread CDF
    ds_nbins = 100;
    [ds_cdf_n, ds_cdf_edges] = histcounts(dsvec*1e6, ds_nbins,...
                                          'Normalization', 'probability');
                   
    ds_mean = mean(dsvec*1e6);
    ds_var = var(dsvec*1e6);
    ds_mean_lognormal = log(ds_mean.^2./sqrt(ds_var+ds_mean.^2));
    ds_var_lognormal =  log(1 + ds_var/ds_mean.^2);
    
    fprintf('Delay spread:\n');
    fprintf('    mean [μs]: %f\n', ds_mean);
    fprintf('    var  [μs]: %f\n', ds_var);
    fprintf('\n');
    fprintf('    μ and σ² assuming log-normal:\n');
    fprintf('    μ:  %f\n', ds_mean_lognormal);
    fprintf('    σ²: %f\n', ds_var_lognormal);
    fprintf('\n');
    fprintf(['NOTE: the small differences between the empirical CDF and\n',...
             'the theoretical one may be due to the fact that because the \n',...
             'channel is not ergodic, the mean and variance of the distribution\n',...
             'may change even during an small path as the one selected']);
    
    x = linspace(-0.8, 0.8, 1000);
    %cdfx = cdf('Normal', x, ds_mean, sqrt(ds_var));
    cdfx = cdf('lognormal', x, ds_mean_lognormal, sqrt(ds_var_lognormal));
    cdflim1 = x(find(cdfx > 1e-3, 1, 'first'));
    cdflim2 = x(find(cdfx < 1-1e-3, 1, 'last'));

    figure();
    cdfplot(ds_cdf_edges, ds_cdf_n);
    hold on;
    grid on;
    plot(x, cdfx, '--k', 'LineWidth', 2);
    title('Delay spread CDF');
    xlabel('Delay spread [\mus]');
    ylabel('Probability (Delay spread [\mus] < abscisa)');
    ylim([0 1]);
    xlim([cdflim1 cdflim2]);
    
    
    % Delay spread PDF
    figure();
    histogram(dsvec*1e6, 50, 'Normalization', 'pdf');
    %pdfx = pdf('Normal', x, ds_mean, sqrt(ds_var));
    pdfx = pdf('lognormal', x, ds_mean_lognormal, sqrt(ds_var_lognormal));
    hold on;
    grid on;
    plot(x, pdfx, '--k', 'LineWidth', 2);
    title('Delay spread PDF');
    xlabel('Delay spread [\mus]');
    ylabel('Probability density');
    xlim([0 cdflim2+0.05]);
end


%% Received power ---------------------------------------------------------
if plot_pw
    chpw = zeros(1,size(resultsSimulation.estchan,3));
    for ii = 1:size(resultsSimulation.estchan,3)
        chpw(ii) = mean(sum(abs(resultsSimulation.estchan(:,:,ii)).^2));
    end
    chpwdb = 10*log10(chpw);
    
    figure();
    plot(resultsSimulation.GPSKP_v, chpwdb);
    grid on;
    title('Received power');
    xlabel('Kilometric point [km]');
    ylabel('Received power [dB]');
    xlim([min(GPSKP) max(GPSKP)]);
    
    % CDF of received power    
    pw_nbins = 100;
    [pw_cdf_n, pw_cdf_edges] = ...
            histcounts(chpwdb, pw_nbins,...
                       'Normalization', 'probability');

    pw_mean = mean(chpwdb);
    pw_var = var(chpwdb);
    fprintf('Received power:\n');
    fprintf('    mean: %f\n', pw_mean);
    fprintf('    var:  %f\n', pw_var);
    
    x = linspace(75, 105, 1000);
    cdfx = cdf('Normal', x, pw_mean, sqrt(pw_var));

    figure();
    cdfplot(pw_cdf_edges, pw_cdf_n);
    hold on;
    grid on;
    plot(x, cdfx, '--k', 'LineWidth', 2);
    title('Received power CDF');
    xlabel('Received power [dB]');
    ylabel('Probability (Received power < abscisa)');
    ylim([0 1]);
    xlim([75 100]);    
end


%% Channel tap ------------------------------------------------------------
if plot_1st_tap
    figure();
    %foff = 0;
    foff = 240;
    tp = t - min(t);
    %ch = estchantime(:,1).*exp(1j*2*pi*(foff)*tp.');
    %ch = estchantime(:,1);
    %plot(t, abs(estchantime(:,1)));
    plot(GPSKP, abs(estchantime(:,1).*exp(1j*2*pi*(foff)*tp.')));
    hold on;
    grid on;
    %plot(t, abs(estchantime(:,2)));
    %plot(t, abs(estchantime(:,1024)));
    %plot(t, abs(estchantime(:,1023)));
    %plot(t, abs(estchantime(:,1022)));
    tii = 1:140:length(tp);
    plot(GPSKP(tii), abs(estchantime(tii)), 'o');
    
    %figure();
    %plot(toffvec)
    
%     % histogram
%     figure();
%     histogram(tap, 100, 'Normalization', 'pdf');
%     grid on;
%     hold on;
%     %pd = fitdist(tap, 'Normal');
%     pd = fitdist(tap, 'Rician');
%     x_values = linspace(0, 10e4, 1000);
%     y = pdf(pd,x_values);
%     plot(x_values, y, 'LineWidth', 2)
end


return

%% Plot channel

figure();
%foff = 0;
foff = 243.5;
tp = t - min(t);
%ch = estchantime(:,1).*exp(1j*2*pi*(foff)*tp.');
ch = estchantime(:,1);
plot(t, abs(ch));
hold on;
grid on;
plot(t, abs(estchantime(:,2)));
plot(t, abs(estchantime(:,1024)));
plot(t, abs(estchantime(:,1023)));
plot(t, abs(estchantime(:,1022)));
tii = 1:140:length(tp);
plot(t(tii), abs(ch(tii)), 'o');


%% Plot channel

figure();
%foff = 0;
foff = 243.5;
tp = t - min(t);
%ch = estchantime(:,1).*exp(1j*2*pi*(foff)*tp.');
ch = estchantime(:,1);
hold on;
grid on;
plot(t, abs(estchantime(:,1024)));
tii = 1:140:length(tp);
plot(t(tii), abs(ch(tii)), 'o');


%% Plot channel power
figure();
%foff = 0;
foff = 243.5;
tp = t - min(t);
ch = abs(estchantime(:,1));
plot(t, (ch));
hold on;
grid on;
tii = 1:140:length(tp);
plot([min(t) max(t)], (mean(ch)*[1 1]));

