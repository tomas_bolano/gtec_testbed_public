%%
clc
clear estchantime

tic;
[estchantime, correctsymbols] = process_get_chan_time(resultsSimulation);
toc;

%%
N = 1;

figure();
plot(real(estchantime(:,N)));
hold on;
grid on;
idx = 1:140:size(estchantime,1);
plot(idx, real(estchantime(idx,N)), 'o');
