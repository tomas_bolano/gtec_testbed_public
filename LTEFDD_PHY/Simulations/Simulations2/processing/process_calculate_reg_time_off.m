function [tau, m, b] = process_calculate_reg_time_off(estchan,...
    toff, correctframes, step)
%PROCESS_CALCULATE_MEAN_TIME_OFF
% Calculates linear regression of time offset from tau = 0
% For each frame l searchs a delay tau(l) so the delay tau(l)+toff(l) 
% corresponds to the LoS component delay. Returns the line from the
% linear regression of tau(l)
%
% Input parameters:
%   estchan - matrix of estimated channels
%   toff - delays for each frame. If empty it will be ignored
%   correctframes - matrix of correct frames. If an empty vector is passed
%       this parameters will be ignored. Default: [].
%       TODO: currently this parameters is not used, but maybe it should
%   step - step value. Default 10

if nargin < 2 || isempty(toff); toff = zeros(1, size(estchan,3)); end
if nargin < 3; correctframes = []; end
if nargin < 4; step = 10; end

t0test = -10:0.5:10;
w = [-300:-1 1:300].';

chanind = 1:step:size(estchan,3);
tauvec = zeros(1, length(chanind));

for ii = 1:length(chanind);
    % estimate offset for 1st tap (t0 when DC is max)
    kk = chanind(ii);
    minfun = @(t0) -abs(sum(estchan(:,1,kk).*exp(1j*2*pi*w/1024*(t0-toff(kk)))));
    [~,mint0I] = min(arrayfun(minfun, t0test));
    tauvec(ii) = fminbnd(minfun, t0test(mint0I)-0.5 , t0test(mint0I)+0.5);
    
    % TEST
    if 0
        x = linspace(-10, 10, 1000);
        plot(x, arrayfun(minfun, x));
        grid on;
        pause();
    end
end

chanind = chanind(ismember(chanind, find(correctframes)));
tauvec = tauvec(ismember(chanind, find(correctframes)));

% linear regression
reg = robustfit(chanind, tauvec);
m = reg(2);
b = reg(1);

tau = m*(1:size(estchan,3)) + b;

end

