function estchantime = process_chan_freq2time(estchan, toff, ...
    dither_value, dither_type)
%PROCESS_GET_CHAN_TIME From the results return the channel in time domain
%
% estchantime is matrix of Nx1024 elements of the channel in time
% domain. correctsymbols is a boolean vector of N elements indicating
% the symbols of frames thar were synchronized correctly.
%
% toff is the amount of time offset to correct for each frame. Default []
%
% dither_value and dither_type indicates the noise value and type used for
% dithering. dither_type can be "SNR" or "noise" (default). For dither_type
% "SNR" the dither_value (default -inf) input will indicate the SNR in dB
% value for each frame. For dither_type "noise" the dither_value will
% indicate the noise power in dB for each frame.

if nargin < 2, toff = []; end
if nargin < 3, dither_value = -inf; end
if nargin < 4, dither_type = 'noise'; end

assert(isempty(toff) || (numel(toff) == size(estchan,3)));

%w = [0:300 zeros(1,423) -300:-1].';
w = [0:511 -512:-1].';
estchantime = zeros(size(estchan));

for ii = 1:size(estchan,3)
    if ~isempty(toff)
        estchan_ii = bsxfun(@times, estchan(:,:,ii), exp(-1j*2*pi*w/1024*toff(ii)));
    else
        estchan_ii = estchan(:,:,ii);
    end
    
    switch lower(dither_type)
        case 'snr'
            pw = (1/numel(ii))*sum(sum(abs(estchan_ii).^2));
            no = pw/10.^(dither_value/10);
        case 'noise'
            no = 10.^(dither_value/10);
        otherwise
            error('invalid dither_type');
    end
    
    estchan_ii_noise = estchan_ii + sqrt(no/2)*(...
        randn(size(estchan_ii)) + 1j*randn(size(estchan_ii)));
    estchantime(:,:,ii) = sqrt(1024)*ifft(estchan_ii_noise);
end

% concatenate channels in the 2nd dimension
estchantime = reshape(permute(estchantime, [2 3 1]), [], size(estchantime,1), 1);

end

