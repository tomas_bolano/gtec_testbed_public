function [psdmat, idx, f] = process_doppler_psd(estchantime,...
    correctsymbols, step, alpha, nfft, psdfmax, tapind,...
    normalize, window, calctype)
%PROCESS_K_FACTOR Calculate Doppler PSD
%
% Input parameters:
%   estchantime - Nx1024 matrix of estimated channels in time for each
%                 OFDM symbol
%   correctsymbols - vector of N boolean values indicating symbols
%       of correctly synchronized frames.
%   step - step as number of symbols
%   alpha - half of the size of the window of OFDM symbols to use to
%       calculate the K-Factor. The number of symbols used to calculate
%       each point will be 2*alpha+1.
%   nfft - fft size
%   psdfmax - maximum frequency to return. 0 to return values up to the
%       maximum resolution.
%   tapind - indexes of the taps to use
%   normalize - normalize doppler power spectal density
%   window - name of the windowing function to use, or 'none' for no window.
%       default is 'none'.
%   calctype - calculation type to obtain the Doppler PSD. Possible values
%       are 'corr' for autocorrelation and 'fft' for a simple fft.
%       default is 'corr'


%% Initialize input variables

assert(size(estchantime,1) == length(correctsymbols), ...
       'invalid dimensions of input parameters');
assert(psdfmax >= 0, 'dspfmax must be a positive number');

assert(length(unique(tapind)) == length(tapind),...
       'There are repeated taps indexes');

if nargin < 8, window = 'none'; end
if nargin < 9, calctype = 'corr'; end

% get window function from string
if strcmpi(window, 'none')
    winfun = @(x) 1;
else
    winfun = str2func(window);
end


%% Calculate PSD

fs = 15.36e6;
chan_ts = (1024+(80+72*6)/7)/fs;
fmax = 1/(2*chan_ts);
fall = linspace(-fmax, fmax, nfft);
if psdfmax ~= 0
    fmask = fall >= -psdfmax & fall <= psdfmax;
else
    fmask = true(1,length(fall));
end
f = fall(fmask);

idxvec = alpha+1:step:size(estchantime,1)-alpha;
psdmat = zeros(nnz(fmask), length(idxvec));
idxvalid = false(1,length(idxvec));

% get window vector
if strcmpi(calctype, 'corr')
    winvec = winfun(2*(2*alpha+1)-1);
elseif strcmpi(calctype, 'fft')
    winvec = winfun(2*alpha+1);
end


for kk = 1:length(idxvec)
    nidx = idxvec(kk);
    idx1 = nidx - alpha;
    idx2 = nidx + alpha;
    
    % continue condition
    if ~all(correctsymbols(idx1:idx2)), continue; end
    
    % calculate Doppler PSD
    psdvec = zeros(1,nfft);
    
    if strcmpi(calctype, 'corr')
        for ii = tapind
            psdvec = psdvec + ...
                chan_ts*fft(xcorr((estchantime(idx1:idx2,ii))).*winvec, nfft).';
        end
    elseif strcmpi(calctype, 'fft')
        for ii = tapind
            psdvec = psdvec + abs(fft(estchantime(idx1:idx2,ii).*winvec, nfft).').^2;
        end
    else
        error('Invalid calctype value');
    end
    
    % Divide by the number of sums to obtain the mean
    psdvec = abs(psdvec)/(length(tapind));

    if normalize
        psdvec = psdvec./sum(abs(psdvec));
    end
    
    psdvec = fftshift(psdvec);
    psdmat(:,kk) = psdvec(fmask);
    idxvalid(kk) = true;
end

psdmat = psdmat(:,idxvalid);
idx = idxvec(idxvalid);

end

