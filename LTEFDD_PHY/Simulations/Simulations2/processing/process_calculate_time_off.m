function toffvec = process_calculate_time_off(syncidx)
%PROCESS_CALCULATE_TIME_OFF Calculates offsets of frames Returns a vector
% of number of samples of delay for each frame. The first delay is always
% set to 0.
%
% Input parameters:
%   syncidx - syncronization indexes for the frames

toffvec = [0 cumsum(diff(syncidx))];
%toffvec = syncidx - syncidx(1); % equivalent

end

