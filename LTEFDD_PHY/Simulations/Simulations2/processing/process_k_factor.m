function [kfacvec, idx] = process_k_factor(estchantime, correctsymbols,...
    step, alpha, tapind)
%PROCESS_K_FACTOR Calculate K-Factor
%
% Input parameters:
%   estchantime - Nx1024 matrix of estimated channels in time for each
%                 OFDM symbol
%   correctsymbols - vector of N boolean values indicating symbols
%       of correctly synchronized frames.
%   step - step as number of symbols
%   alpha - half of the size of the window of OFDM symbols to use to
%       calculate the K-Factor. The number of symbols used to calculate
%       each point will be 2*alpha+1.
%   tapind - vector of the tap indexes to calculate the K-Factor (default 1)


%% Initialize input variables

if nargin < 3, tapind = 1; end

assert(size(estchantime,1) == length(correctsymbols), ...
       'invalid dimensions of input parameters');


%% Calculate K-Factor

idxvec = alpha+1:step:size(estchantime,1)-alpha;

kfacvec = zeros(length(idxvec), numel(tapind));
idxvalid = false(1,length(idxvec));

for kk = 1:length(idxvec)
    nidx = idxvec(kk);
    idx1 = nidx - alpha;
    idx2 = nidx + alpha;
    
    % continue condition
    if ~all(correctsymbols(idx1:idx2)), continue; end
    
    % calculate K-Factor for the selected taps
    for ii = 1:length(tapind)
        tapind_ii = tapind(ii);
        tap = estchantime(idx1:idx2, tapind_ii);
        
        % K-Factor estimation using the method of the Greenstein paper
        g = tap;
        Ga = mean(abs(g).^2);
        Gv = sqrt(mean((abs(g).^2 - Ga).^2));
        V2 = sqrt(Ga.^2 - Gv.^2);
        s2 = Ga - V2;

        kfacvec(kk, ii) = abs(V2/s2);
    end
    
    idxvalid(kk) = true;
end

kfacvec = kfacvec(idxvalid,:);
idx = idxvec(idxvalid);

end

