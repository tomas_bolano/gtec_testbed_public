function cmask = process_findcorrectframes(resultsSimulation)
%PROCESS_SELECTVALID  find correctly synchronized frames

idxvec = arrayfun(@(x) x.f_start_idx, resultsSimulation.results);
cmask = abs(idxvec - mode(idxvec)) < 80;

end
