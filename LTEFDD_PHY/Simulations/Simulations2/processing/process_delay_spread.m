function [dsvec, idx] = process_delay_spread(estchantime,...
    correctsymbols, step, alpha, ntaps_c, ntaps_ac)
%PROCESS_K_FACTOR Calculate delay spread
%
% Input parameters:
%   estchantime - Nx1024 matrix of estimated channels in time for each
%                 OFDM symbol
%   correctsymbols - vector of N boolean values indicating symbols
%       of correctly synchronized frames.
%   step - step as number of symbols
%   alpha - half of the size of the window of OFDM symbols to use to
%       calculate the K-Factor. The number of symbols used to calculate
%       each point will be 2*alpha+1.
%   ntaps_c - number of causal taps to use
%   ntaps_ac - number of anticausal taps to use


%% Initialize input variables

assert(size(estchantime,1) == length(correctsymbols), ...
       'invalid dimensions of input parameters');


%% Calculate delay spread

fs = 15.36e6;
chan_ts = 1/fs;

tapmask = false(1, size(estchantime,2));
tapmask(1:ntaps_c) = true;
tapmask(end-ntaps_ac+1:end) = true;
t = [0:ntaps_c-1, -ntaps_ac:-1]*chan_ts;

idxvec = alpha+1:step:size(estchantime,1)-alpha;
dsvec = zeros(1, length(idxvec));
idxvalid = false(1,length(idxvec));

for kk = 1:length(idxvec)
    nidx = idxvec(kk);
    idx1 = nidx - alpha;
    idx2 = nidx + alpha;
    
    % continue condition
    if ~all(correctsymbols(idx1:idx2)), continue; end
    
    % calculate PDP   
    pdpvec = mean(abs(estchantime(idx1:idx2,:)).^2);
    pdpvec = pdpvec(tapmask);
    
    tau2 = sum(pdpvec.*t.^2)/sum(pdpvec);
    tau  = sum(pdpvec.*t)/sum(pdpvec);
       
    dsvec(kk) = sqrt(tau2 - tau.^2);
    idxvalid(kk) = true;
end

dsvec = dsvec(:,idxvalid);
idx = idxvec(idxvalid);

end

