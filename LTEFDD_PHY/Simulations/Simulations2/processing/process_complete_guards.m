function estchan_out = process_complete_guards(estchan, method, ...
    mmse_N, mmse_ntaps_c, mmse_ntaps_ac, mmse_no, mmse_onlyguards,...
    mmse_covcalc, debug)
%PROCESS_COMPLETE_GUARDS Complete guards subcarriers
%
% Input parameters:
%   estchan - matrix of estimated channels.
%
%   method - indicates the method to use to complete the guards.
%       Available values are:
%       0        complete guards with zeros.
%       'hanning' apply hanning window and complete guards with zeros.
%       'copy'   copy the values of the border.
%       'interp' interpolate the values of the border.
%       'mmse'   use an mmse algorithm.
%
%   mmse_N - For the method mmse this parameter indicates the factor to
%       downscale the number of points to use. (default 12).
%   mmse_ntaps_c - number of causal taps to consider for mmse (default 100).
%   mmse_ntaps_ac - number of anticausal taps to consider for mmse (default 40).
%   mmse_no - noise variance for the mmse algorithm (default 1e1)
%   mmse_onlyguards - the mmse algorithm also estimate the channel, it
%       should be similar for the used subcarriers, but it is not
%       guaranteed depending on the options used. This option indicates if
%       the returned channels are the ones estimated by the mmse algorithm
%       or only the guards of the estimated channels are used to complete
%       the original estimated channels. (default 0)
%   mmse_covcalc - method for calculating the covariance. Available values are
%       'default' - use the default channel with zero guards (default)
%       'hanning_used' - hanning window over used subcarriers
%       'hanning' - hanning window over all subcarriers
%       'interp'  - interpolate guard subcarriers 
%
%   debug - the channels will be printed in a figure. (default 0)   
%
% Output parameters:
%   estchan - matrix of estimated channels with guards completed

if nargin < 3; mmse_N = 12; end
if nargin < 4; mmse_ntaps_c = 100; end
if nargin < 5; mmse_ntaps_ac = 40; end
if nargin < 6; mmse_no = 1e1; end
if nargin < 7; mmse_onlyguards = 0; end
if nargin < 8; mmse_covcalc = 'default'; end
if nargin < 9; debug = 0; end


estchan_out = zeros(1024, size(estchan,2), size(estchan,3));

for ii = 1:size(estchan,3)
    ch = ifftshift(estchan(:,:,ii),1);
    
    chguards = zeros(423, size(ch,2));
    
    timer = tic();
    if ii == 1; fprintf('\n'); end;
    fprintf('    Copmpleting guards - frame %3d... ', ii);
        
    % interpolate guards
    if strcmpi(method, 'interp')
        for jj = 1:size(ch,2)
            chguards(:,jj) = interp1([1 423], ch([300 301], jj), 1:423, 'linear');
        end
        chext = [(ch(1,:)+ch(end,:))/2; ch(1:300,:); chguards; ch(301:end,:)];
    elseif strcmpi(method, 'copy')
        chguards(1:211,:) = repmat(ch(300,:), 211, 1);
        chguards(212:end,:) = repmat(ch(301,:), 212, 1);
        chext = [(ch(1,:)+ch(end,:))/2; ch(1:300,:); chguards; ch(301:end,:)];
    elseif strcmpi(method, 'mmse')
        chext0 = [(ch(1,:)+ch(end,:))/2; ch(1:300,:); chguards; ch(301:end,:)];
        chext = zeros(size(chext0));
        
        % tap mask for MMSE
        nsubc = 1024;
        ntaps_c = mmse_ntaps_c;
        ntaps_ac = mmse_ntaps_ac;
        tapmask = false(1,nsubc);
        tapmask([1:ntaps_c end-ntaps_ac+1:end]) = true;

        h_lmmse = zeros(nnz(tapmask), size(chext0,2));
        h_lmmse_full = zeros(size(chext0));
        
        % time domain channel for estimating covariance matrix
        if strcmpi(mmse_covcalc, 'default')
            h_ls = ifft(chext0);
        elseif strcmpi(mmse_covcalc, 'hanning_used')
            hw = hanning(601);
            chext0_hw = [bsxfun(@times, chext0(1:301,:), hw(301:end));...
                         chguards;...
                         bsxfun(@times, chext0(end-300+1:end,:), hw(1:300))];
            h_ls = ifft(chext0_hw);
        elseif strcmpi(mmse_covcalc, 'hanning')
            hw = hanning(1024);
            chext0_hw = bsxfun(@times, chext0, fftshift(hw));
            h_ls = ifft(chext0_hw);
        elseif strcmpi(mmse_covcalc, 'interp')
            for jj = 1:size(ch,2)
                chguards(:,jj) = interp1([1 423], ch([300 301], jj), 1:423, 'linear');
            end
            chext_interp = [(ch(1,:)+ch(end,:))/2; ch(1:300,:); chguards; ch(301:end,:)];
            h_ls = ifft(chext_interp);
        else
            error('invalid mmse_covcalc value');
        end
        
        %h_ls = ifft(chext0);
        C_full = mean(abs(h_ls.^2),2);
        C_taps = C_full(tapmask);
        %C_diag = C_taps*sqrt(sum(abs(C_full).^2)/sum(abs(C_taps).^2));
        C_diag = C_taps;
        C = spdiags(C_diag, 0,...
                    ntaps_c+ntaps_ac, ntaps_c+ntaps_ac);
        
        if ii == 1
            % Initialization of variables
            % Only done for the first iteration
            
            % DFT matrices
            F = dftmtx(nsubc);
            FL = F(:,tapmask);

            % NOTE: using all the channel points makes operations too slow,
            % we reduce the number of points used by a factor N.
            %N = 12;
            xmask0 = false(nsubc,1);
            xmask0(2:mmse_N:end) = true;
            xmask0(301) = true;
            xmask0(end-300+1) = true;
            xmask = [true(1,301), false(1,1024-601), true(1,300)].';
            xmask = xmask & xmask0;

            no = mmse_no; % small noise value
            No = no*speye(nnz(xmask));

            % create identity matrix and remove guard bands
            X = speye(nsubc, nsubc);
            X = X(xmask,:);
            
            % Truncated FFT matrix
            H = X*FL;
        end
        
        % Required matrices for MMSE
        A = C*H';
        B = A/(H*A + No);
        
        for jj = 1:size(chext,2)
            % MMSE channel
            %h_lmmse(:,jj) = (C*H'/(H*C*H' + no*I)))*chext0(xmask,jj);
            %h_lmmse(:,jj) = (C*H')*((H*C*H' + no*I)\chext0(xmask,jj));
            %h_lmmse(:,jj) = A*((H*A + No)\chext0(xmask,jj));
            h_lmmse(:,jj) = B*chext0(xmask,jj);
            h_lmmse_full(tapmask,jj) = h_lmmse(:,jj);
            
            chext(:,jj) = fft(h_lmmse_full(:,jj));
            if mmse_onlyguards
                chext(1:301,jj) = chext0(1:301);
                chext(725:1024) = chext0(725:1024);
            end
        end
    elseif strcmpi(method, 'hanning')
        hw = hanning(601);
        chext = [bsxfun(@times, ch(1:301,:), hw(301:end)); chguards;...
                 bsxfun(@times, ch(end-300+1:end,:), hw(1:300))];
    elseif method == 0
        chext = [(ch(1,:)+ch(end,:))/2; ch(1:300,:); chguards; ch(301:end,:)];
    else
        error('method - invalid value');
    end
    
    fprintf('OK (%7.2f s)\n', toc(timer));
    estchan_out(:,:,ii) = chext;
    
    
    %% TEST
    if debug
        if ii == 1
            figure();
        end
        f = -512:511;
        for jj = 1:size(chext,2)
            hold off;
            plot(f, fftshift(abs(chext(:,jj))));
            grid on;
            hold on;
            if exist('chext0', 'var')
                plot(f, fftshift(abs(chext0(:,jj))), '--');
            end
            if exist('xmask', 'var')
                p = fftshift(abs(chext0(:,jj)));
                plot(f(fftshift(xmask)), p(fftshift(xmask)), 'x');
            end
            legend('abs channel');
            title(sprintf('Abs channel, frame %d - symbol %d', ii, jj));
            pause();
            
            break
        end
    end    
end

end

