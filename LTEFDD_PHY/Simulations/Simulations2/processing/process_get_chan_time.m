function [estchantime, correctsymbols] = process_get_chan_time(...
    resultsSimulation, completeguards, toff, noisedb)
%PROCESS_GET_CHAN_TIME From the results return the channel in time domain
%
% the parameters completeguards and noisedb are the same as the ones
% of the function process_chan_freq2time.
%
% estchantime is matrix of Nx1024 elements of the channel in time
% domain. correctsymbols is a boolean vector of N elements indicating
% the symbols of frames thar were synchronized correctly.
%
% completeguards indicates if the guards should be completed and the
% method of completion. 0 to not complete, 'interp' to use interpolation,
% 'copy' to just copy the border values. (default 0)
%
% toff is the amount of time offset to correct for each frame. Default []
%
% noisedb is the amount of noise in dB to be applied before the ifft
% (default -inf)

if nargin < 2, completeguards = 0; end
if nargin < 3, toff = []; end
if nargin < 4, noisedb = -inf; end

estchantime = process_chan_freq2time(resultsSimulation.estchan,...
    completeguards, toff, noisedb);
estchantime = reshape(permute(estchantime, [2 3 1]), [], size(estchantime,1), 1);

correctsymbols = process_findcorrectframes(resultsSimulation);
correctsymbols = repmat(correctsymbols, 140, 1);
correctsymbols = correctsymbols(:);

end

