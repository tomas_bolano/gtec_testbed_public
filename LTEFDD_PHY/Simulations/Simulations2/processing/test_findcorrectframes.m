% Plot synchronization values and the ones that are found correct.
% A resultsSimulation variable has to be present in the workspace

idxvec = arrayfun(@(x) x.f_start_idx, resultsSimulation.results);
cmask = process_findcorrectframes(resultsSimulation);

figure();
ii = 1:length(idxvec);
plot(ii, idxvec);
hold on;
grid on;
plot(ii(~cmask), idxvec(~cmask), 'o');

if all(cmask)
    fprintf('All frames synchronized correctly!\n');
end

