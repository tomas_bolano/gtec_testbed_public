function p = tecrail_simulate2_parameters100()
% Common parameters for simulations of 100 km/h

p.speed = 100;
p.samplesFilename = ...
    '2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp';

p.channel = 1;

p.t0 = 0;
p.tf = 335;
p.t0gps = 139;
p.tfgps = 475;

p.initoffset = 15360+1;
p.framesep = 1; % do not skip frames for this simulation

% parameters for channel estimation
p.cec.PilotAverage = 'UserDefined';
p.cec.FreqWindow = 1;
p.cec.TimeWindow = 1;
p.cec.InterpType = 'v4';
p.cec.InterpWindow = 'Centered';
p.cec.InterpWinSize = 3;
    
end
