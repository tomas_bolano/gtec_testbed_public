% simulate2 plot
% plot different results of BER from simulations

clearvars;
%close all;
format long g
restoredefaultpath;
clc;


%% Initialize paths
currentPath = [fileparts(mfilename('fullpath')) filesep];
addpath([currentPath 'processing'])
addpath([currentPath 'results'])

repositoryPath = [fileparts(fileparts(fileparts(fileparts(currentPath)))) filesep];
simulatorPath = [repositoryPath '5G/common/'];

% add path of 5G simulator
addpath(genpath(simulatorPath));

% add path of simulation folders
simulationsPath = [repositoryPath 'LTEFDD_PHY/Simulations'];
addpath(genpath(simulationsPath));


%% Configuration

% files with the saved simulations results
sim_results_files = ...
{
%'simchan_100kmh_kp1_fcorrect_sameSNR'
%'simchan_100kmh_kp2_fcorrect_sameSNR'
%'simchan_100kmh_kp3_fcorrect_sameSNR'
%'simchan_100kmh_kp1_fcorrect'
%'simchan_100kmh_kp1_fcorrect'
'simchan_100kmh_kp2_fcorrect'
%'simchan_100kmh_kp2_fcorrect'
%'simchan_100kmh_kp3_fcorrect'
};

% remove points of doppler change
removedopchange = 0;

% vector of constellations to plot
% available values: 4, 16, 64, 256
M = [16];

% BER in dB scale
plotdb = 1;

% confidence intervals options
plotci = 0;

bcaEstimator = @(x) mean(x);
bcaAlpha = 0.05;
bcaNIters = 400;


%% Load results

timer = tic();
fprintf('Loading simulation results... ');
sim_results = cell(size(sim_results_files));
for ii = 1:length(sim_results_files)
    load(sim_results_files{ii});
    sim_results{ii} = resultsSimulation2;
end
fprintf('OK (%5.2f s)\n', toc(timer));

% extract FBMC_parameters and generate FBMC_constants
FBMC_parameters = sim_results{1}.FBMC_parameters;
FBMC_constants = FBMC_pregenerateConstants(FBMC_parameters);


%% Doppler change points

dopchange = cell(1,3);

dopchange{1} = [...
    0       100.265
    100.29  100.305
    100.335 100.35
    100.38  100.395
    100.43  100.445
    100.48  100.495];

dopchange{2} = [...
    98.415  98.43
    98.48   98.495
    98.545  98.56
    98.605  inf];

dopchange{3} = [...
    98.92   98.935
    98.98   98.995
    99.04   99.055
    99.1    99.115];
    

%% Plot figures

colors = get(groot,'DefaultAxesColorOrder'); % default MATLAB colors
markers = '+soxv^';

modOrder = arrayfun(@(x) x.modulationOrder, FBMC_parameters.dataBlock);

figure();
legendcell = cell(length(M), length(sim_results));

for resii = 1:length(sim_results)
    results = sim_results{resii};
    
    correctFrames = results.correctFrames;
    sectionkp = results.resultsSimulation1.sectionkp;
    sectionkpind = results.resultsSimulation1.sectionkpind;
    SNRdB = results.SNRdB;
    
    if length(removedopchange) > 1
        removedopchange_ii =  removedopchange(resii);
    else
        removedopchange_ii =  removedopchange;
    end
    
    GPSKP = results.resultsSimulation1.GPSKP_v;
    if removedopchange_ii
        GPSKP_mask = false(1,length(GPSKP));
        dc = dopchange{sectionkpind};
        for jj = 1:size(dc,1)
            GPSKP_mask = GPSKP_mask | (GPSKP > dc(jj,1) & GPSKP < dc(jj,2));
        end
        GPSKP_mask = ~GPSKP_mask;
    else
        GPSKP_mask = true(1,length(GPSKP));
    end
    
    simresults = results.results(GPSKP_mask & correctFrames,:);
    
    for Mii = 1:length(M)
        % total bits transmitted for modulation
        nbits = sum(FBMC_constants.dataBlockBitsNumber(modOrder == M(Mii)));
        
        % matrix of number of erroneous bits
        errmat = arrayfun(@(x) sum(x.uncodedErrorBits(modOrder == M(Mii))),...
                          simresults);
        
        % calculate BER per SNR
        bermat = errmat./nbits;       
        bervec = bcaEstimator(bermat);

        % plot BER curve
        if plotci
            %ci = zeros(2, length(bervec));
            %for ii = 1:length(bervec)
            %    ci(:,ii) = bootci(bcaNIters, {bcaEstimator, bermat(:,ii)}, 'alpha', bcaAlpha);
            %end
            ci = bootci(bcaNIters, {bcaEstimator, bermat}, 'alpha', bcaAlpha);
            errorbar(SNRdB, bervec, bervec-ci(1,:), bervec-ci(2,:),...
                     'Color', colors(Mii,:), 'Marker', markers(resii));
        else
            plot(SNRdB, bervec,...
                 'Color', colors(Mii,:), 'Marker', markers(resii));
        end
        grid on;
        hold on;
        
        if removedopchange_ii
            legendcell{Mii, resii} = sprintf('%d-QAM; [%6.2f - %6.2f] no dop change', M(Mii),...
                                             sectionkp(1), sectionkp(2));
        else
            legendcell{Mii, resii} = sprintf('%d-QAM; [%6.2f - %6.2f]', M(Mii),...
                                             sectionkp(1), sectionkp(2));
        end
    end
end


% print title, axes and legend of figure
%title('BER');
xlabel('SNR [dB]');
ylabel('BER');
legend(legendcell{:});

if plotdb
    set(gca, 'YScale', 'log');
end


