% Extract the data from the estimated channels results
% i.e., extracts all the parameters except the estimated channels

%% Configuration

% speed - 100 or 200 km/h
%v = 100;
v = 200;

%% Search files
if v == 100
    resultspath = '/home/tomas/extra/artigomedidastren_wcmc2017/results/estchan_100kmh/';
elseif v == 200
    resultspath = '/home/tomas/extra/artigomedidastren_wcmc2017/results/estchan_200kmh/';
else
    error('Invalid speed');
end

files = dir(resultspath);
files = files(~arrayfun(@(x) x.isdir, files)); % remove directories

if numel(files) == 0
    error('no files found');
end

%% Process files
estchan_data_cell = cell(1,numel(files));

for ii = 1:numel(files)
    timer = tic();
    fprintf('Loading %s... ', files(ii).name);
    load([resultspath '/' files(ii).name]);
    fprintf('OK (%6.2f)\n', toc(timer));
    
    ind = str2double(files(ii).name(18:end-4));
    data = rmfield(resultsSimulation, 'estchan');
    estchan_data_cell{ind} = data;
    
    clear resultsSimulation;
end

% concatenate data in struct
estchan_data = cat(1, estchan_data_cell{:});

% save data
filename = sprintf('estchan_%dkmh_data', v);
save(filename, 'estchan_data');

