
syncpoints_cell = cellfun(@(x) arrayfun(@(y) y.f_start_idx, x), ...
        arrayfun(@(x) x.results, estchan_data, 'UniformOutput', false),...
        'UniformOutput', false);
syncpoints_ind = [1; 1+cumsum(cellfun(@(x) numel(x), syncpoints_cell(1:end-1)))];
syncpoints = cat(2, syncpoints_cell{:});

figure()
a = process_calculate_time_off(syncpoints);
a = a + 127;
a = a + 5; % propagation delay when the train passes by the antenna

% small adjustment for the south sector
%a(9251:end) = a(9251:end) -4; % for 100 km/h
a(5337:end) = a(5337:end) -4; % for 100 km/h

plot(a)
grid on;
hold on;
plot(syncpoints_ind, a(syncpoints_ind), 'o');
%ylim([0 160]);