% simulate2 plot
% estimate and plot K-Factor, Delay Spread and Doppler PSD

%clearvars;
close all;
format long g
restoredefaultpath;
%clc;



%% Initialize paths
currentPath = fileparts(mfilename('fullpath'));
addpath([fileparts(currentPath) filesep 'processing']);

% path where results are stored
%addpath([currentPath 'results'])
addpath('/home/tomas/extra/artigomedidastren_wcmc2017/results/estchan_100kmh/',...
        '/home/tomas/extra/artigomedidastren_wcmc2017/results/estchan_200kmh/');



%% Base Configuration

% speed of the data to plot
%v = 100;
v = 200;

% results to calculate
kfac_calculate = 1; % K-Factor
dop_calculate = 1;  % Doppler PSD
ds_calculate = 1;   % Delay Spread


%% Specific configuration

assert(kfac_calculate || dop_calculate || ds_calculate,...
       'you should specify at least one parameter to calculate');

filename_kfac = sprintf('estchan_%dkmh_kfactor_results', v);
filename_ds = sprintf('estchan_%dkmh_delayspread_results', v);
filename_dop = sprintf('estchan_%dkmh_doppsd_results', v);

filename_kfac_exists = false;
filename_ds_exists = false;
filename_dop_exists = false;

if(kfac_calculate && exist([filename_kfac '.mat'],'file'))
    filename_kfac_exists = true;
    fprintf('K-factor results will be overwritten...\n');
end

if(ds_calculate && exist([filename_ds '.mat'],'file'))
    filename_ds_exists = true;
    fprintf('Delay spread results will be overwritten...\n');
end

if(dop_calculate && exist([filename_dop '.mat'],'file'))
    filename_dop_exists = true;
    fprintf('Delay spread results will be overwritten...\n');
end

if filename_kfac_exists || filename_ds_exists || filename_dop_exists
    input('Press any key to proceed or Ctrl+C to cancel...');
end


Nres = 26;
estchan_results = cell(1,Nres);
for ii = 1:Nres
    estchan_results{ii} = sprintf('estchan_%dkmh_kp%d', v, ii);
end

% Channel guard completion configuration
% available values:
%   copy - copy the channel in the outer subcarriers to the guards
%   inter - interpolate the channel on the outer subcarriers
%   0 - do nothing
%guardmethod = 0;
%guardmethod = 'hanning';
%guardmethod = 'copy';
%guardmethod = 'interp';
guardmethod = 'mmse';
guard_debug = 0;

% options for mmse channel guard completion
guard_mmse_N = 6;
guard_mmse_ntaps_c = 240;
guard_mmse_ntaps_ac = 130;
guard_mmse_no = 1e-2;
guard_mmse_onlyguards = 0;

% method for covariance estimation for mmse
%guard_mmse_covcalc = 'default';
%guard_mmse_covcalc = 'hanning';
guard_mmse_covcalc = 'hanning_used';
%guard_mmse_covcalc = 'interp';

% For the parameters estimated in this script (K-Factor, delay spread,
% Doppler PSD) we do not use dithering.
% dithering options
dither_type = 'SNR'; % dithering type value indicates the SNR in dB
%dither_type = 'noise'; % dithering type value indicates the noise in dB
dither_value = inf;
%dither_value = 70; % For SNR, 70 dB is a good value when using mmse


% K-Factor configuration
kfac_alpha = 140*1.5;
kfac_tapind = 1;
kfac_step = 140*1;


% Doppler PSD configuration
dop_step = 256;
dop_nfft = 1024*8;
if v == 100
    dop_alpha = 1024;
    dop_fmax = 400;
else % 200 km/h
    dop_alpha = 512;
    dop_fmax = 800;
end

dop_ntaps_c = 30;  % only used for the dop_tapind vector
dop_ntaps_ac = 15; % only used for the dop_tapind vector
dop_tapind = [1:dop_ntaps_c 1024-dop_ntaps_ac+1:1024];
%dop_tapind = [1];

dop_normalize = 1;
dop_window = 'none'; % name of window, or 'none'
dop_calctype = 'fft';


% Delay spread configuration
ds_step = 128;
ds_alpha = 140;
ds_ntaps_c = 65;
ds_ntaps_ac = 10;


%% Process channels

kfac_cell = cell(1,Nres);
kfac_GPSKP_cell = cell(1,Nres);

ds_cell = cell(1,Nres);
ds_GPSKP_cell = cell(1,Nres);

dop_cell = cell(1,Nres);
dop_GPSKP_cell = cell(1,Nres);
dop_f = [];

for ii = 1:Nres
    % load saved channels
    timer = tic();
    fprintf('Loading %s... ', estchan_results{ii});
    load(estchan_results{ii});
    fprintf('OK (%5.2f s)\n', toc(timer));

    timer = tic();
    fprintf('Finding corectly synchronized frames... ');
    correctframes = process_findcorrectframes(resultsSimulation);
    fprintf('OK (%5.2f s)\n', toc(timer));

    timer = tic();
    fprintf('Estimating offsets... ');
    syncidx = arrayfun(@(x) x.f_start_idx, resultsSimulation.results);
    toffvec0 = process_calculate_time_off(syncidx);
    fprintf('OK (%5.2f s)\n', toc(timer));

    if resultsSimulation.sectionkpind == 11
        ind = 1:length(toffvec0);
        toffvec0(364:366) = toffvec0(364:366) - 4;
        toffvec0(ind > 395) = toffvec0(ind > 395) - 3;

        % do a polinomial fit of the offsets
        toffreg = polyfit(1:length(toffvec0), toffvec0, 2);
        toffregvec = toffreg(1)*(1:length(toffvec0)).^2 +...
                     toffreg(2)*(1:length(toffvec0)) +  toffreg(3);

        % the offsets in toffvec give us a first approximation of the
        % actual LoS offset of the signal
        toffvec = toffvec0 - toffregvec;
    else
        % do a linear regression of the offsets
        toffreg = robustfit(1:length(toffvec0), toffvec0);
        toffregvec = toffreg(2)*(1:length(toffvec0)) + toffreg(1);

        % the offsets in toffvec give us a first approximation of the
        % actual LoS offset of the signal
        toffvec = toffvec0 - toffregvec;
    end

    % Estimate the LoS offset of the frames, for this we use the first
    % approximation given by toffvec
    fprintf('Estimating mean offset... ');
    if resultsSimulation.sectionkpind == 17
       tauoff_m = 0.0001;
       tauoff_b = -1.90;
       tauoff = tauoff_m*(1:size(resultsSimulation.estchan,3)) + tauoff_b;
    else
        [tauoff, tauoff_m, tauoff_b] = ...
            process_calculate_reg_time_off(resultsSimulation.estchan,...
                toffvec, correctframes, 5);
    end
    fprintf('OK (%5.2f s)\n', toc(timer));

    % estimated total LoS offsets
    toffvec1 = toffvec - tauoff;

    % For the case of offsetcorrect = 1 the channels delays are first
    % corrected and then the guard are added. This is important since
    % the delays are not integer numbers and adding them after
    % completing the guard band can introduce some effects due to
    % "discontinuities" in the phase.
    timer = tic();
    fprintf('Correcting channel delays...');
    estchan_off0 = zeros(size(resultsSimulation.estchan));
    w = [-300:-1 1:300]; % discrete frequency indexes
    for jj = 1:size(estchan_off0,3)
        estchan_off0(:,:,jj) = bsxfun(@times,...
            resultsSimulation.estchan(:,:,jj),...
            exp(-1j*2*pi*w.'/1024*toffvec1(jj)));
    end
    fprintf('OK (%5.2f s)\n', toc(timer));

    % complete guard bands with the selected method
    fprintf('Completing guard bands... ');
    estchanguards = process_complete_guards(estchan_off0,...
                        guardmethod, guard_mmse_N, guard_mmse_ntaps_c,...
                        guard_mmse_ntaps_ac, guard_mmse_no,...
                        guard_mmse_onlyguards, guard_mmse_covcalc,...
                        guard_debug);
    fprintf('OK (%5.2f s)\n', toc(timer));

    % clear estchan_off0, since it is no longer neccesary
    clear estchan_off0;

    % Obtain time domain channels
    timer = tic();
    fprintf('Obtaining time domain channels... ');
    estchantime = process_chan_freq2time(estchanguards, [], ...
                                         dither_value, dither_type);
    fprintf('OK (%5.2f s)\n', toc(timer));


    % from correctframes obtain a vector of the correct symbols
    correctsymbols = correctframes;
    correctsymbols = repmat(correctsymbols, 140, 1);
    correctsymbols = correctsymbols(:);

    % obtain vectors of times and KPs
    x = 70 + (1:140:size(estchantime,1));
    xq = 1:size(estchantime,1);

    t = interp1(x, resultsSimulation.t_v, xq, 'spline');
    GPSKP = interp1(x, resultsSimulation.GPSKP_v, xq, 'spline');
    
    % Calculate K-Factor
    if kfac_calculate
        timer = tic();
        fprintf('Calculating K-Factor... ');
        [kfacvec, kfacidx] = process_k_factor(estchantime, correctsymbols,...
                                              kfac_step, kfac_alpha, kfac_tapind);
        fprintf('OK (%5.2f s)\n', toc(timer));
        
        % save data
        kfac_GPSKP_cell{ii} = GPSKP(kfacidx);
        kfac_cell{ii} = kfacvec;
    end
    
    % Calculate Delay Spread
    if ds_calculate
        timer = tic();
        fprintf('Calculating delay spread... ');
        [dsvec, dsidx] = process_delay_spread(estchantime,...
            correctsymbols, ds_step, ds_alpha, ds_ntaps_c, ds_ntaps_ac);
        fprintf('OK (%5.2f s)\n', toc(timer));
        
        % save data
        ds_GPSKP_cell{ii} = GPSKP(dsidx);
        ds_cell{ii} = dsvec;
    end
    
    % calculate Doppler PSD
    if dop_calculate
        timer = tic();
        fprintf('Calculating Doppler PSD... ');
        [dop_psdmat, dopidx, dop_f] = process_doppler_psd(estchantime,...
            correctsymbols, dop_step, dop_alpha, dop_nfft, dop_fmax,...
            dop_tapind, dop_normalize, dop_window, dop_calctype);
        fprintf('OK (%5.2f s)\n', toc(timer));
        
        % save data
        dop_GPSKP_cell{ii} = GPSKP(dopidx);
        dop_cell{ii} = dop_psdmat;
    end
end


%% save results

if kfac_calculate
    % save K-Factor results
    estchan_kfac_results.v = v;
    estchan_kfac_results.kfac_cell = kfac_cell;
    estchan_kfac_results.kfac_GPSKP_cell = kfac_GPSKP_cell;
    
    save(filename_kfac, 'estchan_kfac_results');
end

if ds_calculate
    % save Delay Spread results
    estchan_ds_results.v = v;
    estchan_ds_results.ds_cell = ds_cell;
    estchan_ds_results.ds_GPSKP_cell = ds_GPSKP_cell;
    
    save(filename_ds, 'estchan_ds_results');
end

if dop_calculate
    % save Doppler PSD results
    estchan_dop_results.v = v;
    estchan_dop_results.dop_cell = dop_cell;
    estchan_dop_results.dop_GPSKP_cell = dop_GPSKP_cell;
    estchan_dop_results.dop_f = dop_f;
    
    save(filename_dop, 'estchan_dop_results');
end


%% Plot K-Factor results

if exist('estchan_kfac_results', 'var')
    % concatenate results
    GPSKP_cat = cat(2, estchan_kfac_results.kfac_GPSKP_cell{:});
    kfac_cat = cat(1, estchan_kfac_results.kfac_cell{:}).';
    
    if estchan_kfac_results.v == 100
        ndw = 50; % downsampling factor
    else
        ndw = 25;
    end
    
    GPSKP_cat = GPSKP_cat(1:ndw:end);
    kfac_cat = kfac_cat(1:ndw:end);

    fig = figure('name', sprintf('K-Factor %d km/h', estchan_kfac_results.v));
    plot(GPSKP_cat, 10*log10(kfac_cat));
    grid on;
    hold on;
    
    %plot averaged K-Factor
    smooth_fac = 20;
    kfac_smooth = smooth(10*log10(kfac_cat), smooth_fac);
    plot(GPSKP_cat(floor(smooth_fac/4):end-floor(smooth_fac/4)),...
         kfac_smooth(floor(smooth_fac/4):end-floor(smooth_fac/4)),...
         'LineWidth', 3);
    
    
    h_xlabel = xlabel('Kilometric point [km]', 'Interpreter', 'Latex');
    h_ylabel = ylabel('K-Factor [dB]', 'Interpreter', 'Latex');
    
    KPant = 97.075; % KP of antenna
    KP0_200 = 96.30;  % starting point section 200 km/h
    KPf_200 = 100.62; % final point section 200 km/h
    
    plot(KPant*[1 1], ylim, 'r--');
    %xlim([min(GPSKP(pdpidx)) max(GPSKP(pdpidx))]);
    %xlim(resultsSimulation.sectionkp);
    xlim([min(GPSKP_cat) max(GPSKP_cat)]);
    %ylim([min(PDP_delay) max(PDP_delay)]*1e6);
    
    if estchan_kfac_results.v == 200
        xl = xlim;
        yl = ylim;
        KP0_100 = xl(1);
        KPf_100 = xl(2);
        x = [KP0_100 KP0_200 KP0_200 KP0_100];
        y = [yl(1) yl(1) yl(2) yl(2)];
        color = [0.2 0.6 1];
        patch(x,y, color, 'EdgeColor', color);

        x = [KPf_200 KPf_100 KPf_100 KPf_200];
        y = [yl(1) yl(1) yl(2) yl(2)];
        patch(x,y,color, 'EdgeColor', color);
    end

    hl = legend('Instantaneous K-Factor values',...
                'averaged K-Factor',...
                'eNodeB position',...
                'Location', 'SouthEast');
    set(hl, 'Interpreter', 'Latex');
    
    set(findall(gcf,'-property','FontSize'),'FontSize',14);
    %set(hl,'FontSize',14);
    set(h_xlabel,'FontSize',18);
    set(h_ylabel,'FontSize',18);
else
    warning('Could not plot K-factor results: variable not in workspace');
end


%% Plot Delay spread results

if exist('estchan_kfac_results', 'var')
    % concatenate results
    GPSKP_cat = cat(2, estchan_ds_results.ds_GPSKP_cell{:});
    ds_cat = cat(2, estchan_ds_results.ds_cell{:});

    if estchan_ds_results.v == 100
        ndw = 50; % downsampling factor
    else
        ndw = 25;
    end
    
    GPSKP_cat = GPSKP_cat(1:ndw:end);
    ds_cat = ds_cat(1:ndw:end);

    fig = figure('name', sprintf('Delay Spread %d km/h', estchan_ds_results.v));
    plot(GPSKP_cat, ds_cat*1e6);
    grid on;
    hold on;
    
    %plot averaged delay spread
    smooth_fac = 20;
    ds_smooth = smooth(ds_cat*1e6, smooth_fac);
    plot(GPSKP_cat(floor(smooth_fac/4):end-floor(smooth_fac/4)),...
         ds_smooth(floor(smooth_fac/4):end-floor(smooth_fac/4)),...
         'LineWidth', 3);
    
    h_xlabel = xlabel('Kilometric point [km]', 'Interpreter', 'Latex');
    h_ylabel = ylabel('Delay Spread [$\mu$s]', 'Interpreter', 'Latex');
    %xlim([min(GPSKP(pdpidx)) max(GPSKP(pdpidx))]);
    %xlim(resultsSimulation.sectionkp);
    xlim([min(GPSKP_cat) max(GPSKP_cat)]);
    ylim([0 0.6]);
    
    KPant = 97.075; % KP of antenna
    KP0_200 = 96.30;  % starting point section 200 km/h
    KPf_200 = 100.62; % final point section 200 km/h
    
    xl = xlim();
    yl = ylim();
    plot(xlim, [1 1]*mean(ds_cat)*1e6, 'LineWidth', 2);
    
    plot(KPant*[1 1], ylim, 'r--');

    set(findall(gcf,'-property','FontSize'),'FontSize',14);
    set(h_xlabel,'FontSize',18);
    set(h_ylabel,'FontSize',18);
    
    if estchan_ds_results.v == 200
        xl = xlim;
        yl = ylim;
        KP0_100 = xl(1);
        KPf_100 = xl(2);
        x = [KP0_100 KP0_200 KP0_200 KP0_100];
        y = [yl(1) yl(1) yl(2) yl(2)];
        color = [0.2 0.6 1];
        patch(x,y, color, 'EdgeColor', color);

        x = [KPf_200 KPf_100 KPf_100 KPf_200];
        y = [yl(1) yl(1) yl(2) yl(2)];
        patch(x,y,color, 'EdgeColor', color);
    end
    
    hl = legend('Instantaneous value',...
                'averaged value',...
                'mean value',...
                'eNodeB position',...
                'Location', 'NorthEast');

    set(hl, 'Interpreter', 'Latex');
else
    warning('Could not plot K-factor results: variable not in workspace');
end


%% Plot Doppler PSD results

if exist('estchan_kfac_results', 'var')
    % concatenate results
    GPSKP_cat = cat(2, estchan_dop_results.dop_GPSKP_cell{:});
    dop_cat = cat(2, estchan_dop_results.dop_cell{:});
    dop_f = estchan_dop_results.dop_f;
    
    ndw = 10; % downsampling factor
    noisedB = -50;
    
    GPSKP_cat = GPSKP_cat(1:ndw:end);
    dop_cat = dop_cat(:,1:ndw:end);
    dop_cat = dop_cat + ...
              abs(10.^(noisedB/10)*(randn(size(dop_cat)) + 1j*(randn(size(dop_cat)))));

    fig = figure('name', sprintf('Doppler PSD %d km/h', estchan_ds_results.v));
    surf(GPSKP_cat, dop_f, 10*log10(dop_cat));
    grid on;
    h_xlabel = xlabel('Kilometric point [km]', 'Interpreter', 'Latex');
    h_ylabel = ylabel('Doppler frequency [Hz]', 'Interpreter', 'Latex');
    
    h_cb = colorbar;
    ylabel(h_cb, 'Doppler PSD [dB]', 'Interpreter', 'Latex');
    
    view([0 90]);
    shading interp;
    %xlim([min(GPSKP(pdpidx)) max(GPSKP(pdpidx))]);
    %xlim(resultsSimulation.sectionkp);
    xlim([min(GPSKP_cat) max(GPSKP_cat)]);
    
    %ylim([0 0.6]);
    caxis([-60 -5]);

    set(findall(gcf,'-property','FontSize'),'FontSize',14);
    set(h_xlabel,'FontSize',18);
    set(h_ylabel,'FontSize',18);
else
    warning('Could not plot K-factor results: variable not in workspace');
end


return
% Save detailed K-Factor image
%kfac_img_filename = 'K-Factor';
%set(fig, 'PaperPosition', [1 1 80 20]);
%print(fig, kfac_img_filename, '-dpng', '-r600');
