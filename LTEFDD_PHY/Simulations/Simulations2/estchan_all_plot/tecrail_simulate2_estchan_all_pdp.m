% simulate2 plot
% estimate and plot PDP

%clearvars;
close all;
format long g
restoredefaultpath;
%clc;



%% Initialize paths
currentPath = fileparts(mfilename('fullpath'));
addpath(currentPath);
addpath([fileparts(currentPath) filesep 'processing']);

% path where results are stored
%addpath([currentPath 'results'])
addpath('/home/tomas/extra/artigomedidastren_wcmc2017/results/estchan_100kmh/',...
        '/home/tomas/extra/artigomedidastren_wcmc2017/results/estchan_200kmh/');



%% Base Configuration

% speed of the data to plot
%v = 100;
v = 200;


%% Specific Configuration

filename = sprintf('estchan_%dkmh_pdp_results', v);

if(exist([filename '.mat'],'file'))
    fprintf('Results will be overwritten...\n');
    input('Press any key to proceed or Ctrl+C to cancel...');
end

Nres = 26;
estchan_results = cell(1,Nres);
for ii = 1:Nres
    estchan_results{ii} = sprintf('estchan_%dkmh_kp%d', v, ii);
end

% Channel guard completion configuration
% available values:
%   copy - copy the channel in the outer subcarriers to the guards
%   inter - interpolate the channel on the outer subcarriers
%   0 - do nothing
%guardmethod = 0;
%guardmethod = 'hanning';
%guardmethod = 'copy';
%guardmethod = 'interp';
guardmethod = 'mmse';
guard_debug = 0;

% options for mmse channel guard completion
guard_mmse_N = 6;
guard_mmse_ntaps_c = 240;
guard_mmse_ntaps_ac = 130;
guard_mmse_no = 1e-2;
guard_mmse_onlyguards = 0;

% method for covariance estimation for mmse
%guard_mmse_covcalc = 'default';
%guard_mmse_covcalc = 'hanning';
guard_mmse_covcalc = 'hanning_used';
%guard_mmse_covcalc = 'interp';

% dithering options
%dither_type = 'SNR'; % dithering type value indicates the SNR in dB
dither_type = 'noise'; % dithering type value indicates the noise in dB
%dither_value = inf;
%dither_value = 70; % For SNR, 70 dB is a good value when using mmse
dither_value = 50; % For noise, 50 dB is a good value when using mmse (?)
%dither_value = -inf;

% PDP configuration
pdp_step = 128;
pdp_alpha = 140;
pdp_ntaps_c = 330;
pdp_ntaps_ac = 30;
pdp_normalize = 0;
pdp_plotwithdelay = 0;


%% Process channels

load(sprintf('estchan_%dkmh_data', v));
syncpoints_cell = cellfun(@(x) arrayfun(@(y) y.f_start_idx, x), ...
        arrayfun(@(x) x.results, estchan_data, 'UniformOutput', false),...
        'UniformOutput', false);
syncpoints_ind = [1; 1+cumsum(cellfun(@(x) numel(x), syncpoints_cell(1:end)))];
syncpoints = cat(2, syncpoints_cell{:});

toffvec_all = process_calculate_time_off(syncpoints);
toffvec_all = toffvec_all + 127; % propagation delay of first frame
toffvec_all = toffvec_all + 5; % propagation delay when the train passes by the antenna

% small adjustment for the south sector
if v == 100
    toffvec_all(9251:end) = toffvec_all(9251:end) -4; % for 100 km/h
else % v = 200
    toffvec_all(5337:end) = toffvec_all(5337:end) -4; % for 100 km/h
end


GPSKP_cell = cell(1,Nres);
PDP_cell = cell(1,Nres);
PDP_delay = [];

for ii = 1:Nres
    % load saved channels
    timer = tic();
    fprintf('Loading %s... ', estchan_results{ii});
    load(estchan_results{ii});
    fprintf('OK (%5.2f s)\n', toc(timer));

    timer = tic();
    fprintf('Finding corectly synchronized frames... ');
    correctframes = process_findcorrectframes(resultsSimulation);
    fprintf('OK (%5.2f s)\n', toc(timer));

    % Complete the guard bands using the selected method
    timer = tic();
    fprintf('Completing guard bands... ');
    estchanguards = process_complete_guards(resultsSimulation.estchan,...
                        guardmethod, guard_mmse_N, guard_mmse_ntaps_c,...
                        guard_mmse_ntaps_ac, guard_mmse_no,...
                        guard_mmse_onlyguards, guard_mmse_covcalc,...
                        guard_debug);
    fprintf('OK (%5.2f s)\n', toc(timer));
    
    % array of offsets for this channel section
    toffvec0 = toffvec_all(syncpoints_ind(ii):syncpoints_ind(ii+1)-1);

    % Obtain time domain channels. Here the offsets in toffvec0 are
    % integers numbers
    timer = tic();
    fprintf('Obtaining time domain channels... ');
    estchantime = process_chan_freq2time(estchanguards, toffvec0,...
                                         dither_value, dither_type);
    fprintf('OK (%5.2f s)\n', toc(timer));

    % from correctframes obtain a vector of the correct symbols
    correctsymbols = correctframes;
    correctsymbols = repmat(correctsymbols, 140, 1);
    correctsymbols = correctsymbols(:);

    % obtain vectors of times and KPs
    x = 70 + (1:140:size(estchantime,1));
    xq = 1:size(estchantime,1);

    t = interp1(x, resultsSimulation.t_v, xq, 'spline');
    GPSKP = interp1(x, resultsSimulation.GPSKP_v, xq, 'spline');

    % Calculate PDP
    timer = tic();
    fprintf('Calculating PDP... ');
    [pdpmat, pdpidx, pdp_t] = process_pdp(estchantime,...
        correctsymbols, pdp_step, pdp_alpha, pdp_ntaps_c, pdp_ntaps_ac,...
        pdp_normalize);
    fprintf('OK (%5.2f s)\n', toc(timer));
    
    % Save PDP data
    PDP_delay = pdp_t;
    GPSKP_cell{ii} = GPSKP(pdpidx);
    PDP_cell{ii} = pdpmat;
end

% create results struct
estchan_pdp_results.PDP_delay = PDP_delay;
estchan_pdp_results.GPSKP_cell = GPSKP_cell;
estchan_pdp_results.PDP_cell = PDP_cell;


% save results
save(filename, 'estchan_pdp_results');


%% Plot PDP results

% concatenate results
GPSKP_cat = cat(2, estchan_pdp_results.GPSKP_cell{:});
PDP_cat = cat(2, estchan_pdp_results.PDP_cell{:});
PDP_delay = estchan_pdp_results.PDP_delay;

ndw = 10; % downsampling factor
GPSKP_cat = GPSKP_cat(1:ndw:end);
PDP_cat = PDP_cat(:,1:ndw:end);

fig = figure('name', sprintf('PDP %d km/h', estchan_pdp_results.v));
surf(GPSKP_cat, PDP_delay*1e6, 10*log10(PDP_cat));
shading interp
%shading flat
view([0 90]);
grid on;
h_xlabel = xlabel('Kilometric point [km]', 'Interpreter', 'Latex');
h_ylabel = ylabel('Delay [$\mu$s]', 'Interpreter', 'Latex');

h_cb = colorbar();
ylabel(h_cb, 'PDP [dB]', 'Interpreter', 'Latex');

%xlim([min(GPSKP(pdpidx)) max(GPSKP(pdpidx))]);
%xlim(resultsSimulation.sectionkp);
xlim([min(GPSKP_cat) max(GPSKP_cat)]);
%ylim([min(PDP_delay) max(PDP_delay)]*1e6);
ylim([0 18]);
%caxis([40 115]);

set(findall(gcf,'-property','FontSize'),'FontSize',14);
set(h_xlabel,'FontSize',18);
set(h_ylabel,'FontSize',18);


return
%% Save detailed PDP image
pdp_img_filename = 'pdp_normalized';
set(fig, 'PaperPosition', [1 1 80 20]);
print(fig, pdp_img_filename, '-dpng', '-r600');
