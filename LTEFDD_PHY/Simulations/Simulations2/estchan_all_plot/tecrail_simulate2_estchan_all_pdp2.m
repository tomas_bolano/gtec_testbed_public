% simulate2 plot
% estimate and plot normalized PDP with offset corrected

%clearvars;
close all;
format long g
restoredefaultpath;
%clc;



%% Initialize paths
currentPath = [fileparts(mfilename('fullpath'))];
addpath([fileparts(currentPath) filesep 'processing']);

% path where results are stored
%addpath([currentPath 'results'])
addpath('/home/tomas/extra/artigomedidastren_wcmc2017/results/estchan_100kmh/',...
        '/home/tomas/extra/artigomedidastren_wcmc2017/results/estchan_200kmh/');



%% Configuration

filename = 'estchan_pdp2_results';

if(exist([filename '.mat'],'file'))
    fprintf('Results will be overwritten...\n');
    input('Press any key to proceed or Ctrl+C to cancel...');
end

Nres = 26;
estchan_results = cell(1,Nres);
for ii = 1:Nres
    estchan_results{ii} = sprintf('estchan_100kmh_kp%d', ii);
end

% Channel guard completion configuration
% available values:
%   copy - copy the channel in the outer subcarriers to the guards
%   inter - interpolate the channel on the outer subcarriers
%   0 - do nothing
%guardmethod = 0;
%guardmethod = 'hanning';
%guardmethod = 'copy';
%guardmethod = 'interp';
guardmethod = 'mmse';
guard_debug = 0;

% options for mmse channel guard completion
guard_mmse_N = 6;
guard_mmse_ntaps_c = 240;
guard_mmse_ntaps_ac = 130;
guard_mmse_no = 1e-2;
guard_mmse_onlyguards = 0;

% method for covariance estimation for mmse
%guard_mmse_covcalc = 'default';
%guard_mmse_covcalc = 'hanning';
guard_mmse_covcalc = 'hanning_used';
%guard_mmse_covcalc = 'interp';

% dithering options
dither_type = 'SNR'; % dithering type value indicates the SNR in dB
%dither_type = 'noise'; % dithering type value indicates the noise in dB
%dither_value = inf;
dither_value = 70; % For SNR, 70 dB is a good value when using mmse

% PDP configuration
pdp_step = 128;
pdp_alpha = 140;
pdp_ntaps_c = 75;
pdp_ntaps_ac = 30;
pdp_normalize = 1;
pdp_plotwithdelay = 0;


%% Process channels

GPSKP_cell = cell(1,Nres);
PDP_cell = cell(1,Nres);
PDP_delay = [];

for ii = 1:Nres
    % load saved channels
    timer = tic();
    fprintf('Loading %s... ', estchan_results{ii});
    load(estchan_results{ii});
    fprintf('OK (%5.2f s)\n', toc(timer));

    timer = tic();
    fprintf('Finding corectly synchronized frames... ');
    correctframes = process_findcorrectframes(resultsSimulation);
    fprintf('OK (%5.2f s)\n', toc(timer));

    timer = tic();
    fprintf('Estimating offsets... ');
    syncidx = arrayfun(@(x) x.f_start_idx, resultsSimulation.results);
    toffvec0 = process_calculate_time_off(syncidx);
    fprintf('OK (%5.2f s)\n', toc(timer));

    if resultsSimulation.sectionkpind == 11
        ind = 1:length(toffvec0);
        toffvec0(364:366) = toffvec0(364:366) - 4;
        toffvec0(ind > 395) = toffvec0(ind > 395) - 3;

        % do a polinomial fit of the offsets
        toffreg = polyfit(1:length(toffvec0), toffvec0, 2);
        toffregvec = toffreg(1)*(1:length(toffvec0)).^2 +...
                     toffreg(2)*(1:length(toffvec0)) +  toffreg(3);

        % the offsets in toffvec give us a first approximation of the
        % actual LoS offset of the signal
        toffvec = toffvec0 - toffregvec;
    else
        % do a linear regression of the offsets
        toffreg = robustfit(1:length(toffvec0), toffvec0);
        toffregvec = toffreg(2)*(1:length(toffvec0)) + toffreg(1);

        % the offsets in toffvec give us a first approximation of the
        % actual LoS offset of the signal
        toffvec = toffvec0 - toffregvec;
    end

    % Estimate the LoS offset of the frames, for this we use the first
    % approximation given by toffvec
    fprintf('Estimating mean offset... ');
    if resultsSimulation.sectionkpind == 17
       tauoff_m = 0.0001;
       tauoff_b = -1.90;
       tauoff = tauoff_m*(1:size(resultsSimulation.estchan,3)) + tauoff_b;
    else
        [tauoff, tauoff_m, tauoff_b] = ...
            process_calculate_reg_time_off(resultsSimulation.estchan,...
                toffvec, correctframes, 5);
    end
    fprintf('OK (%5.2f s)\n', toc(timer));

    % estimated total LoS offsets
    toffvec1 = toffvec - tauoff;

    % For the case of offsetcorrect = 1 the channels delays are first
    % corrected and then the guard are added. This is important since
    % the delays are not integer numbers and adding them after
    % completing the guard band can introduce some effects due to
    % "discontinuities" in the phase.
    timer = tic();
    fprintf('Correcting channel delays...');
    estchan_off0 = zeros(size(resultsSimulation.estchan));
    w = [-300:-1 1:300]; % discrete frequency indexes
    for jj = 1:size(estchan_off0,3)
        estchan_off0(:,:,jj) = bsxfun(@times,...
            resultsSimulation.estchan(:,:,jj),...
            exp(-1j*2*pi*w.'/1024*toffvec1(jj)));
    end
    fprintf('OK (%5.2f s)\n', toc(timer));

    % complete guard bands with the selected method
    fprintf('Completing guard bands... ');
    estchanguards = process_complete_guards(estchan_off0,...
                        guardmethod, guard_mmse_N, guard_mmse_ntaps_c,...
                        guard_mmse_ntaps_ac, guard_mmse_no,...
                        guard_mmse_onlyguards, guard_mmse_covcalc,...
                        guard_debug);
    fprintf('OK (%5.2f s)\n', toc(timer));

    % clear estchan_off0, since it is no longer neccesary
    clear estchan_off0;

    % Obtain time domain channels
    timer = tic();
    fprintf('Obtaining time domain channels... ');
    estchantime = process_chan_freq2time(estchanguards, [], ...
                                         dither_value, dither_type);
    fprintf('OK (%5.2f s)\n', toc(timer));


    % from correctframes obtain a vector of the correct symbols
    correctsymbols = correctframes;
    correctsymbols = repmat(correctsymbols, 140, 1);
    correctsymbols = correctsymbols(:);

    % obtain vectors of times and KPs
    x = 70 + (1:140:size(estchantime,1));
    xq = 1:size(estchantime,1);

    t = interp1(x, resultsSimulation.t_v, xq, 'spline');
    GPSKP = interp1(x, resultsSimulation.GPSKP_v, xq, 'spline');

    % Calculate PDP
    timer = tic();
    fprintf('Calculating PDP... ');
    [pdpmat, pdpidx, pdp_t] = process_pdp(estchantime,...
        correctsymbols, pdp_step, pdp_alpha, pdp_ntaps_c, pdp_ntaps_ac,...
        pdp_normalize);
    fprintf('OK (%5.2f s)\n', toc(timer));
    
    % Save PDP data
    PDP_delay = pdp_t;
    GPSKP_cell{ii} = GPSKP(pdpidx);
    PDP_cell{ii} = pdpmat;
end

% create results struct
estchan_pdp2_results.PDP_delay = PDP_delay;
estchan_pdp2_results.GPSKP_cell = GPSKP_cell;
estchan_pdp2_results.PDP_cell = PDP_cell;


% save results
save(filename, 'estchan_pdp2_results');


%% Plot PDP results

% concatenate results
GPSKP_cat = cat(2, estchan_pdp2_results.GPSKP_cell{:});
PDP_cat = cat(2, estchan_pdp2_results.PDP_cell{:});
PDP_delay = estchan_pdp2_results.PDP_delay;

ndw = 1; % downsampling factor
GPSKP_cat = GPSKP_cat(1:ndw:end);
PDP_cat = PDP_cat(:,1:ndw:end);

fig = figure('name', 'PDP');
surf(GPSKP_cat, PDP_delay*1e6, 10*log10(PDP_cat));
shading interp
%shading flat
view([0 90]);
grid on;
h_xlabel = xlabel('Kilometric point [km]', 'Interpreter', 'Latex');
h_ylabel = ylabel('Delay [$\mu$s]', 'Interpreter', 'Latex');

h_cb = colorbar();
ylabel(h_cb, 'PDP [dB]', 'Interpreter', 'Latex');


%xlim([min(GPSKP(pdpidx)) max(GPSKP(pdpidx))]);
%xlim(resultsSimulation.sectionkp);
xlim([min(GPSKP_cat) max(GPSKP_cat)]);
ylim([min(PDP_delay) max(PDP_delay)]*1e6);
caxis([-50 -5]);

set(findall(gcf,'-property','FontSize'),'FontSize',14);
set(h_xlabel,'FontSize',18);
set(h_ylabel,'FontSize',18);


return
%% Save detailed PDP image
pdp_img_filename = 'pdp_normalized';
set(fig, 'PaperPosition', [1 1 80 20]);
print(fig, pdp_img_filename, '-dpng', '-r600');
