% simulate2
% simulate channels for selected sections

clearvars -except resultsSimulation;
close all;
format long g
restoredefaultpath;
clc;


%% Initialize paths
currentPath = [fileparts(mfilename('fullpath')) filesep];
%addpath([currentPath 'Parameters'])
%addpath([currentPath 'results'])

repositoryPath = [fileparts(fileparts(fileparts(fileparts(currentPath)))) filesep];
simulatorPath = [repositoryPath '5G/common/'];

% add path of 5G simulator
addpath(genpath(simulatorPath));

% add path of simulation folders
simulationsPath = [repositoryPath 'LTEFDD_PHY/Simulations'];
addpath(genpath(simulationsPath));


%% Configuration

% file with the saved channels and associated data
%estchan_results = 'estchan_100kmh_kp1_fcorrect';
%estchan_results = 'estchan_100kmh_kp2_fcorrect';
estchan_results = 'estchan_100kmh_kp3_fcorrect';

% parameters of the simulator to use
FBMC_parameters = FBMC_parameters_ofdm2;

% calculate throughput results
calcthroughput = 0;

% range of SNR to use
SNRdBvec = 0:40;

% use mean SNR
% instead of applying the same SNR to all the frames
meanSNR = 1;

% number of workers for the parfor loop
nworkers = 16; 

% configuration for the channel estimation
% WARNING: FOR no ISI ntaps_ac + ntaps_c must be less than 72
ntaps_c = 55; % number of causal taps to use
ntaps_ac = 10; % number of anticausal taps to use
smoothfactor = 1; % smooth factor for the channel taps (1 for no smooth)


%% Saved filename
resultsfile = ['simchan_' estchan_results(9:end)];

if ~meanSNR
    resultsfile = [resultsfile '_sameSNR'];
end

if exist(['./' resultsfile '.mat'],'file')
    fprintf('Results will be overwritten...\n');
    input('Press any key to proceed or Ctrl+C to cancel...');
end


%% Inicialization

% load saved channels
% this wil load the variable resultsSimulation
%if exist('resultsSimulation', 'var')
%    res = input('reload estimated channels [y|N] ', 's');
%    if strcmpi(res, 'y')
%        load(estchan_results);
%    end
%else
%    load(estchan_results);
%end
fprintf('Loading estimated channels... ');
load(estchan_results);
fprintf('OK\n');

% offset for no ISI window
ofdm_noisi_offset = ntaps_ac+round((72-ntaps_c-ntaps_ac)/2);
assert(ofdm_noisi_offset >= 0 & ofdm_noisi_offset <= 72);

% percentage of cp to use (to avoid anticausal interference)
ofdm_cpfactor = (72-ofdm_noisi_offset)/72;
assert(ofdm_cpfactor >= 0 & ofdm_cpfactor <= 1);

% if calcthroughput is false disable the decoding in the receiver
if ~calcthroughput
    for ii = 1:length(FBMC_parameters.dataBlock)
        FBMC_parameters.dataBlock(ii).FECDecoderFunction = {@FBMC_nullFECDecoder};
    end
end

% LTE constants (10 MHz)
nSamplesSlot = 15360/2;
nSymbolsSlot = 14/2;
nSamplesSubframe = nSamplesSlot * 2;
nSymbolsSubframe = nSymbolsSlot * 2;
nSamplesFrame = nSamplesSubframe * 10;
nSymbolsFrame = nSymbolsSubframe * 10;
nHalfSamplesSlot = nSamplesSlot / 2;
nFFT = 2048/2;
usedSubcarriers=600;
guardSubcarriers=(nFFT-usedSubcarriers)/2;
windowLength = 40/2;
longCPlength = nFFT*10/128;
shortCPlength = nFFT*9/128;
longOFDMsymbolLength = nFFT + longCPlength;
shortOFDMsymbolLength = nFFT + shortCPlength;
NsymbSlot = 7;
fs=15.36e6;
enbC.NDLRB=50;
enbC.CyclicPrefix='Normal';
enbC.DuplexMode='FDD';

enbC.CellRefP = 1;  % One transmit antenna port
enbC.LookForAllNCellIDs = false;
enbC.Ng = 'One';
enbC.CFI = 2;
enbC.PHICHDuration = 'Normal';


%% Main Loop

% detect correctly synchronized frames
correctFrames = process_findcorrectframes(resultsSimulation);

% vector of indices
indices = 1:length(resultsSimulation.GPSKP_v);

% initialize worker data
workerIndCell = cell(1, nworkers);

N = floor(length(indices)/nworkers);
for ii = 1:nworkers-1
    workerIndCell{ii} = (ii-1)*N+1:ii*N;
end
workerIndCell{end} = ii*N+1:length(indices);

% initialize OFDM data and signal
FBMC_constants = FBMC_pregenerateConstants(FBMC_parameters);
FBMC_txData = FBMC_transmitter(FBMC_parameters, FBMC_constants);

% number of total and used subcarriers
nsubc = FBMC_parameters.basicParameters.subcarriersNumber;
nused = nnz(FBMC_constants.usedSubcarriers.subcarrierMask);

% transmitted signal for the simulations
tx_sim = FBMC_txData.interpolatedSignal;

% Update demodulator function to set the percentage of the cp symbol
% to use in the demodulation
FBMC_parameters.demodulator.demodulatorFunction = ...
    {@FBMC_ofdmcpDemodulator_2, FBMC_constants.totalTimeSymbolsNumber,...
     ofdm_cpfactor};
 
% update synchronization function to correct time and freq offset
% the freq offset is zero because it was already corrected when
% estimating the lte channel
FBMC_parameters.synchronization.synchronizationFunction = ...
    {@FBMC_fixedSynchronizator, ntaps_ac, 0};

% variables to save data
resultsCell = cell(1,nworkers);
noCell = cell(1,nworkers);
SNRdBCell = cell(1,nworkers);

if meanSNR
    % calculate mean channel power
    nsymb = FBMC_parameters.basicParameters.timeSymbolsNumber + 2;
    chpw = zeros(1, size(resultsSimulation.estchan,3));
    for ii = 1:size(resultsSimulation.estchan,3)
        % the calculation gives more accurate results for our code
        % if we sum a small value (1 - 2) to the used subcarriers.
        chpw(ii) = mean(sum(abs(resultsSimulation.estchan(:,1:nsymb,ii)).^2)/...
                        (usedSubcarriers + 1.5));
    end
    meanpw = mean(chpw);
    novec = meanpw*nsubc./(nused*10.^(SNRdBvec./10));
end

% slice estimated channels to send them to the different workers
estchanCell = cell(workerIndCell);
for ii = 1:length(workerIndCell)
    estchanCell{ii} = resultsSimulation.estchan(:,:,workerIndCell{ii});
end

% remove estchan from resultsSimulation to free memory
resultsSimulation.estchan = [];

globaltimer = tic();
parfor ii = 1:nworkers
    workerInd = workerIndCell{ii};
    FBMC_parameters_par = FBMC_parameters;
    
    % initialize structure to save results
    results = repmat(struct('uncodedErrorBits', [],...
                            'codedErrorBits', [],...
                            'EVMdB', []),...
                     length(workerInd), length(SNRdBvec));

    % variables to save no and SNR values
    no_mat = zeros(length(workerInd), length(SNRdBvec));
    SNRdB_mat = zeros(length(workerInd), length(SNRdBvec));
    
    timebuf = zeros(1,3);
    for jj = 1:length(workerInd)
        timer = tic();
        
        % get estimated channel matrix
        index = workerInd(jj);
        estchan = estchanCell{ii}(:,:,jj);
        
        if ~correctFrames(index)
            fprintf('[%2d] Skipping frame %d\n', ii, index);
            continue;
        end
        
        fprintf('[%2d] Processing index %d\n', ii, index);
                
        % convert channel to time domain coefficients
        coef = tecrail_ltechfreq2time(estchan, ntaps_c, ntaps_ac, smoothfactor);
        
        % apply channel
        rx_sim = zeros(size(tx_sim));
        for kk = 1:size(coef,2)
            rx_sim(kk:end) = rx_sim(kk:end) + ...
                             tx_sim(1:end-kk+1).*coef(1:numel(tx_sim)-kk+1,kk);
        end
        
        % obtain received power
        rxpw = var(rx_sim);
        
        % process received signal for different values of SNR
        for kk = 1:length(SNRdBvec)
            if meanSNR
                no = novec(kk);
            else
                SNRdB = SNRdBvec(kk);
                no = rxpw*nsubc/(nused*10^(SNRdB/10));
            end
            
            % add noise
            noisevec = sqrt(no/2)*(randn(size(rx_sim)) + 1j*randn(size(rx_sim)));
            rx_sim_noise = rx_sim + noisevec;
            
            % process received signal
            rxData = FBMC_receiver(FBMC_parameters_par, FBMC_constants,...
                         rx_sim_noise, FBMC_txData.dataBlockCodedBitsNumber, no);

            FBMC_results = FBMC_systemIterationResults(FBMC_parameters_par,...
                               FBMC_constants, FBMC_txData, rxData);
            
            % save results
            results(jj,kk) = FBMC_results;
            
            % save no and SNR
            SNRdB = 10*log10(nsubc/nused*rxpw./no);
            SNRdB_mat(jj,kk) = SNRdB;
            no_mat(jj,kk) = no;
        end
                
        time = toc(timer);
        timebuf = [time timebuf(1:end-1)];
        
        msgstr = sprintf('[%2d] (%4d/%4d) %6.2f %%', ii, jj,...
                         length(workerInd), jj/length(workerInd)*100);
        if jj >= length(timebuf)
            msgstr = sprintf('%s | est. time remaining: %6.2f s',...
                             msgstr, mean(timebuf)*(length(workerInd)-jj));
        end
        
        fprintf('%s\n', msgstr);
    end
    
    resultsCell{ii} = results;
    
    SNRdBCell{ii} = SNRdB_mat;
    noCell{ii} = no_mat;
end
toc(globaltimer);


%% concatenate results
results = cat(1, resultsCell{:});
SNRdB_mat = cat(1, SNRdBCell{:});
no_mat = cat(1, noCell{:});


%% save results
resultsSimulation2.FBMC_parameters = FBMC_parameters;

% copy data from resultsSimulations (except estimated channels)
rs1fn = fieldnames(resultsSimulation);
for ii = 1:length(rs1fn)
    if ~strcmpi(rs1fn{ii}, 'estchan')
        resultsSimulation2.resultsSimulation1.(rs1fn{ii}) = ...
            resultsSimulation.(rs1fn{ii});
    end
end

% correctFrames stores the indexes of frames synchroniced correctly,
% results for frames not synchronized correctly are not calculated
resultsSimulation2.correctFrames = correctFrames;

resultsSimulation2.meanSNR = meanSNR;

resultsSimulation2.calcthroughput = calcthroughput;
resultsSimulation2.SNRdB = SNRdBvec;
resultsSimulation2.results = results;
resultsSimulation2.SNRdB_mat = SNRdB_mat;
resultsSimulation2.no_mat = no_mat;

save(resultsfile, 'resultsSimulation2');

