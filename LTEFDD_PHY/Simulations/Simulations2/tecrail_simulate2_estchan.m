% TECRAIL_SIMULATE2_ESTCHAN
% estimate channels for selected sections

% If EXTERNCONF_EXEC = 1 then the basic configuration of the script
% is considered to be defined on an external script
if ~exist('EXTERNCONF_EXEC', 'var')
    clearvars;
    clc;
end

close all;
format long g
restoredefaultpath;


%% Initialize paths
currentPath = [fileparts(mfilename('fullpath')) filesep];
%addpath([currentPath 'Parameters'])

repositoryPath = [fileparts(fileparts(fileparts(fileparts(currentPath)))) filesep];
simulatorPath = [repositoryPath '5G/common/'];

% add path of 5G simulator
addpath(genpath(simulatorPath));

% add path of simulation folders
simulationsPath = [repositoryPath 'LTEFDD_PHY/Simulations'];
addpath(genpath(simulationsPath));

% add path of GPS scripts and files
%GPSpath = '/home/tomas/extra/GTEC_5G_Simulator_Results/ArtigoCapacidadeFerrovario_AWPL2016/Results';
GPSpath =  fullfile(filesep, 'media', 'tecrail', 'measData4');
GPSCodePath = [repositoryPath 'LTEFDD_PHY/GPS_Processing'];
addpath(GPSpath);
addpath(GPSCodePath);

% add path of data files
datafilesPath = fullfile(filesep, 'media', 'tecrail', 'measData4');
%datafilesPath = '/home/tomas/extra/measurements/ADIFMeasurements';
addpath(datafilesPath);

% add path of GTIS code
addpath([repositoryPath 'gtis/matlab/usrpfileformat/']);
addpath([repositoryPath 'gtis/matlab/utils/']);
addpath([repositoryPath 'gtis/examples/lte/']);


%% initialize parameters

if ~exist('EXTERNCONF_EXEC', 'var') || EXTERNCONF_EXEC == 0
    % general parameters for file processing
    %p = tecrail_simulate2_parameters100;
    p = tecrail_simulate2_parameters200;

    % section and cell id to estimate channels
    sectionkpind = 2; % section of kilometric points index
    switch sectionkpind
        case 1, sectionkp = [100.25 100.5]; cell_id = 4;
        case 2, sectionkp = [98.375 98.625]; cell_id = 4;
        case 3, sectionkp = [98.875 99.125]; cell_id = 4;
        case 4, sectionkp = [99.375 99.625]; cell_id = 4;
        otherwise, error('invalid section index');
    end

    % correct frequency offset
    fcorrect = 0;

    % number of workers to use in the parfor
    nworkers = 16;
end


%% Saved filename
if fcorrect
    resultsfile = sprintf('estchan_%dkmh_kp%d_fcorrect',...
                          p.speed, sectionkpind);
else
    resultsfile = sprintf('estchan_%dkmh_kp%d',...
                          p.speed, sectionkpind);
end

if(exist(['./' resultsfile '.mat'],'file'))
    disp('Results will be overwritten...');
    disp('Press any key to proceed or Ctrl+C to cancel...');
    pause;
end


%% LTE constants (10 MHz)
nSamplesSlot = 15360/2;
nSymbolsSlot = 14/2;
nSamplesSubframe = nSamplesSlot * 2;
nSymbolsSubframe = nSymbolsSlot * 2;
nSamplesFrame = nSamplesSubframe * 10;
nSymbolsFrame = nSymbolsSubframe * 10;
nHalfSamplesSlot = nSamplesSlot / 2;
nFFT = 2048/2;
usedSubcarriers = 600;
guardSubcarriers=(nFFT-usedSubcarriers)/2;
windowLength = 40/2;
longCPlength = nFFT*10/128;
shortCPlength = nFFT*9/128;
longOFDMsymbolLength = nFFT + longCPlength;
shortOFDMsymbolLength = nFFT + shortCPlength;
NsymbSlot = 7;
fs=15.36e6;

enbC.NDLRB=50;
enbC.CyclicPrefix='Normal';
enbC.DuplexMode='FDD';

enbC.CellRefP = 1;  % One transmit antenna port
enbC.LookForAllNCellIDs = false;
enbC.Ng = 'One';
enbC.CFI = 2;
enbC.PHICHDuration = 'Normal';


%% Samples indices and times
s_start = p.t0*fs+p.initoffset-nSamplesSubframe; % start sample
s_end = p.tf*fs; % end sample
indices = s_start:nSamplesFrame*p.framesep:s_end; % indices array
t = indices/fs; % times array


%% Obtain GPS Data
GPSfilename = [p.samplesFilename '.usrpnmea'];
GPSData = getGPSData(GPSfilename);
t_off = getEndFileOffsetGPSKP(GPSData, p.tfgps);
if p.speed == 200
    % Time calibration
    % We add to the offset an small amount so the kilometric points
    % for 100 and 200 km/h match
    t_off = t_off + 2.06;
end
GPSKP = getGPSKP(GPSData, 0, t, t_off);


%% Select valid indices for the selected section
kpmask = GPSKP >= sectionkp(1) & GPSKP < sectionkp(2);

GPSKP_v = GPSKP(kpmask); % valid kilometric points
indices_v = indices(kpmask); % valid indices
t_v = t(kpmask); % valid times


%% Main Loop

% initialize worker data
workerIndCell = cell(1, nworkers);

N = floor(length(indices_v)/nworkers);
for ii = 1:nworkers-1
    workerIndCell{ii} = (ii-1)*N+1:ii*N;
end
workerIndCell{end} = ii*N+1:length(indices_v);

% variables to save data
resultsCell = cell(1,nworkers);
estchanCell = cell(1,nworkers);

% samples per read
samples = nSamplesFrame*(p.framesep+1);

globaltimer = tic();
parfor ii = 1:nworkers
    workerInd = workerIndCell{ii};
    results = repmat(struct(), 1, length(workerInd));
    estchan = zeros(usedSubcarriers, nSymbolsFrame, length(workerInd));
    
    timebuf = zeros(1,5);
    for jj = 1:length(workerInd)
        timer = tic();
        
        enb = enbC;
        index = workerInd(jj);
        sample_jj = indices_v(index);

        % read measurements file
        [readSignalComplex, success, totalNumSamplessProcessedPerChannel, ...
         totalNumBytesProcessedPerChannel, numChannels] = ...
            GTIS_readUSRPMultichannelSignalsFromFile(...
                p.samplesFilename, samples, sample_jj);
    
        if ~success
            error('error reading file');
        end
        
        if totalNumSamplessProcessedPerChannel < samples
            error('file read function returned less samples than requested');
        end
        
        rxSamples = readSignalComplex(p.channel, 1:end).';
        
        % estimate and correct frequency offset
        foffset = lteFrequencyOffset(enb, rxSamples);
        rxSamples_fc = lteFrequencyCorrect(enb,rxSamples,foffset);
        
        % detect cell id and estimate time offset
        [N_id_cell, f_start_idx] = lteCellSearch(enb, rxSamples_fc, cell_id);
        
        if fcorrect
            % use the frequency corrected signal for demodulation
            rxSamples = rxSamples_fc;
        end
        
        % demodulate received frame
        enb.NCellID = N_id_cell;
        rxSamples = rxSamples(1+f_start_idx+(0:(nSamplesFrame-1)),:);
        rxGrid = sqrt(usedSubcarriers)/nFFT*lteOFDMDemodulate(enb, rxSamples);
        
        % estimate and frame channel
        enb.NSubframe = 0;
        [estchan(:,:,jj), noiseEst] = lteDLChannelEstimate(enb, p.cec, rxGrid);
        
        % save some additional data
        results(jj).foffset = foffset;
        results(jj).f_start_idx = f_start_idx;
        results(jj).n_id_cell = N_id_cell;
        
        % print progress information
        time = toc(timer);
        timebuf = [time timebuf(1:end-1)];
        
        msgstr = sprintf('[%2d] (%4d/%4d) %6.2f %%', ii, jj,...
                         length(workerInd), jj/length(workerInd)*100);
        if jj >= length(timebuf)
            msgstr = sprintf('%s | est. time remaining: %6.2f s',...
                             msgstr, mean(timebuf)*(length(workerInd)-jj));
        end
        
        fprintf('%s\n', msgstr);
    end
    
    resultsCell{ii} = results;
    estchanCell{ii} = estchan;
end
toc(globaltimer);

% concatenate results and estimated channels
results = cat(2, resultsCell{:});
estchan = cat(3, estchanCell{:});


%% save results
resultsSimulation.parameters = p;

resultsSimulation.results = results;
resultsSimulation.estchan = estchan;
resultsSimulation.GPSKP_v = GPSKP_v;
resultsSimulation.t_v = t_v;

resultsSimulation.fcorrect = fcorrect;
resultsSimulation.cell_id = cell_id;
resultsSimulation.sectionkp = sectionkp;
resultsSimulation.sectionkpind = sectionkpind;


save(resultsfile, 'resultsSimulation');

