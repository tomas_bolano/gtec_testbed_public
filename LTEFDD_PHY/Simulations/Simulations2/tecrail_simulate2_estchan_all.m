% TECRAIL_SIMULATE2_ESTCHAN_ALL
% estimate channels for consecutive sections by calling
% tecrail_simulate2_estchan for each section

%% Initialize basic path
currentPath = [fileparts(mfilename('fullpath')) filesep];

% add parameters files path
parametersPath = [currentPath 'Parameters'];
addpath(genpath(parametersPath));


%% Generate kilometric points and cell ids for all the regions
sectionkp1 = [94.5 94.75];
kp_ant = 97.075; % kilometric point where antenna is located
nkp = 26; % number of sections

sectionkp_list = zeros(nkp,2);
sectionkp_list(1,:) = sectionkp1;
for ii = 2:nkp
    sectionkp_list(ii,:) = sectionkp_list(ii-1,:) + 0.25;
end

cellid_list = cell(nkp,1);
for ii = 1:nkp
    if sectionkp_list(ii,2) < kp_ant - 0.15
        cellid_list{ii} = 3;
    elseif sectionkp_list(ii,1) > kp_ant + 0.15
        cellid_list{ii} = 4;
    else
        cellid_list{ii} = [3 4];
    end
end

%% Basic parameters for channel estimation
% general parameters for file processing
p = tecrail_simulate2_parameters100;
%p = tecrail_simulate2_parameters200;

% correct frequency offset
fcorrect = 0;

% number of workers to use in the parfor
nworkers = 16;


%% Iterative call to tecrail_simulate2_estchan

% Important, this variable is set to one so the configuration defined
% before on this file takes effect on the tecrail_simulate2_estchan script
EXTERNCONF_EXEC = 1; %#ok<NASGU>

for ii = 1:nkp
    sectionkp = sectionkp_list(ii,:);
    cell_id = cellid_list{ii};
    
    tecrail_simulate2_estchan();
end

% Disable EXTERNCONF_EXEC
EXTERNCONF_EXEC = 0;

