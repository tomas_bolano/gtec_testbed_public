clearvars -except estChannels;
%close all;
clc;

%% Rutas
RutaActual = [fileparts(mfilename('fullpath')) filesep];
RutaRepositorio = [RutaActual '../../'];


%RutaResultados=[RutaRepositorio '5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/'];
RutaResultados='/home/tomas/extra/GTEC_5G_Simulator_Results/ArtigoCapacidadeFerrovario_AWPL2016/Results';
speed = 200; % 100 or 200 km/h

% Results and GPS filenames
switch speed
    case 100
        ResultsFilename = 'Results_PDP_100kmh_ch0_outdoor.mat';
    case 200
        ResultsFilename = 'Results_PDP_200kmh_ch0_outdoor.mat';
end

% load results file
%if ~exist('estChannels', 'var')
    load([RutaResultados filesep ResultsFilename]);
%end

% chanel configuration
%chIndex = 120; % index of the channel
ntaps_c = 55; % causal taps to use
ntaps_ac = 5; % anticausal taps to use
smoothfactor = 1; % factor of smooth for the channel taps

% sample OFDM signal to transmit
nsubc = 1024;
nused = 600;
cplen = 72;
M = 4;
nsymb = 140;
subcmask = [false true(1,nused/2) false(1,nsubc-nused-1) true(1,nused/2)];

tx_data = qammod(randi([0 M-1], nused, nsymb), M);
tx_f = zeros(nsubc, nsymb);
tx_f(subcmask,:) = tx_data;
tx_t = ifft(tx_f);
tx_t = [tx_t(end-cplen+1:end,:); tx_t];
tx_t = tx_t(:);

% normalize tx signal power
tx_t = tx_t/sqrt(var(tx_t));

rxpw = zeros(1, size(estChannels,3));

tic();
parfor ii = 1:size(estChannels,3)

    % matrix of LTE channel estimation
    estchan = estChannels(:,:,ii);

    % convert channel from freq domain to time domain taps
    coef = tecrail_ltechfreq2time(estchan, ntaps_c, ntaps_ac, smoothfactor);
    
    % Apply channel
    y = zeros(size(tx_t));
    for jj = 1:size(coef,2)
        y(jj:end) = y(jj:end) + tx_t(1:end-jj+1).*coef(1:numel(tx_t)-jj+1,jj);
    end

    % Receive OFDM and estimate channel
    rx_t1 = [zeros(ntaps_ac,1); y(1:end-ntaps_ac)];
    rx_t = reshape(rx_t1, nsubc+cplen, []);
    fftstart = ceil((cplen)*0.8);
    phasecorrect = exp(1j*2*pi*(cplen-fftstart)/nsubc*(0:nsubc-1));
    rx_t = rx_t(fftstart+1:end-(cplen-fftstart),:);
    %rx_f = bsxfun(@times, phasecorrect.', fft(rx_t));
    
    rxpw(ii) = var(rx_t(:));    
    fprintf('%d/%d\n', ii, size(estChannels,3));
end
toc();

%%
if ~exist('DatosPotencia.mat', 'file')
    figure();
    plot(10*log10(rxpw), '-');
    title('signal power');
    return
end

load('DatosPotencia.mat');

figure();
puntosKmts_1 = puntosKmts(20:end);
pSinal_1 = 10*log10(pSinal(20:end));
pRuido_1 = 10*log10(pRuido);

plot(puntosKmts_1, pSinal_1 - pRuido_1);
hold on;
grid on;
switch speed
    case 100
        kp1_offset = 0.25;
        kp2_offset = -0.6;
        noisepw = 22;
    case 200
        kp1_offset = 1.5;
        kp2_offset = 0.5;
        noisepw = 22;
end
kmtsPoints = linspace(min(puntosKmts_1) - kp1_offset,...
                      max(puntosKmts_1) - kp2_offset, numel(rxpw));
                  
plot(kmtsPoints, 10*log10(rxpw) - noisepw, '--');

xlim([min(kmtsPoints), max(kmtsPoints)])
xlabel('Kilometric point');
ylabel('SNR [dB]');
legend('Measured', 'Simulated');
title(sprintf('SNR for %d km/h', speed))
