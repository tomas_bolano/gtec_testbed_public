clearvars -except estChannels;
%close all;
clc;

%% Rutas
RutaActual = [fileparts(mfilename('fullpath')) filesep];
RutaRepositorio = [RutaActual '../../'];


%RutaResultados=[RutaRepositorio '5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/'];
RutaResultados='/home/tomas/extra/GTEC_5G_Simulator_Results/ArtigoCapacidadeFerrovario_AWPL2016/Results';
%ResultsFilename='Results_PDP_200kmh_ch0_outdoor.mat';
ResultsFilename='Results_PDP_100kmh_ch0_outdoor.mat';
%ResultsFilename='Results_PDP_50kmh_ch0_outdoor_matlab_n.mat';

if ~exist('estChannels', 'var')
    load([RutaResultados filesep ResultsFilename]);
end

%chIndex = 120; % index of the channel
chIndex = 40; % index of the channel
ntaps_c = 55; % causal taps to use
ntaps_ac = 5; % anticausal taps to use
smoothfactor = 5; % factor of smooth for the channel taps


% matrix of LTE channel estimation
estchan = estChannels(:,:,chIndex);

% convert channel from freq domain to time domain taps
[coef1, coeforig1] = tecrail_ltechfreq2time(estchan, ntaps_c, ntaps_ac, 1);
[coef, coeforig] = tecrail_ltechfreq2time(estchan, ntaps_c, ntaps_ac, smoothfactor);

% plot channel
figure();
surf(abs(estChannels(:,:,chIndex)));
shading flat;
xlabel('Time [symbol]');
ylabel('Freq [subcarrier]');
view([0 90]);

% plot channel taps
figure()
hold on;
grid on;
for ii = 1:3
    stem(real(coef1(1:1024:end,ntaps_ac+ii)));
    set(gca, 'ColorOrder', circshift(get(gca, 'ColorOrder'), 1));
    plot(real(coef(1:1024:end,ntaps_ac+ii)), '-');
end
xlabel('Time');
ylabel('Amplitude (real part)');
title('Estimated channel taps (time)');
legend('Original estimation', 'smoothed estimation');

% plot channel taps
figure()
hold on;
grid on;
for ii = 1
    plot((-ntaps_ac:ntaps_c-1)/15.36,(real(coef(ii,:))), '-s');
end
xlabel('Delay [us]');
ylabel('Amplitude (real part)');
title('Estimated channel taps (delay)');


% plot PDP
figure()
hold on;
grid on;
for ii = 1
    pdp = mean(abs(coeforig).^2, 2);
    pdp = pdp/max(pdp);
    pdp = circshift(pdp, ntaps_ac);
    plot((-ntaps_ac:ntaps_c-1)/15.36, 10*log10(pdp), '-s');
end
xlabel('Delay [us]');
ylabel('Amplitude [dB]');
title('PDP');

%% Generate sample OFDM signal
nsubc = 1024;
nused = 600;
cplen = 72;
M = 4;
nsymb = 140;
subcmask = [false true(1,nused/2) false(1,nsubc-nused-1) true(1,nused/2)];

tx_data = qammod(randi([0 M-1], nused, nsymb), M);
tx_f = zeros(nsubc, nsymb);
tx_f(subcmask,:) = tx_data;
tx_t = ifft(tx_f);
tx_t = [tx_t(end-cplen+1:end,:); tx_t];
tx_t = tx_t(:);


%% Apply filter
y = zeros(size(tx_t));
for ii = 1:size(coef,2)
    y(ii:end) = y(ii:end) + tx_t(1:end-ii+1).*coef(1:numel(tx_t)-ii+1,ii);
end


%% Receive OFDM and estimate channel
rx_t1 = [zeros(ntaps_ac,1); y(1:end-ntaps_ac)];
rx_t = reshape(rx_t1, nsubc+cplen, []);
fftstart = ceil((cplen)*0.8);
phasecorrect = exp(1j*2*pi*(cplen-fftstart)/nsubc*(0:nsubc-1));
rx_t = rx_t(fftstart+1:end-(cplen-fftstart),:);
rx_f = bsxfun(@times, phasecorrect.', fft(rx_t));
H = rx_f./tx_f;

figure();
H = H(subcmask,:);
surf(abs(fftshift(H,1)));
%surf(10*log10(abs(ifftshift(H,1))));
shading flat;
xlabel('Time [symbol]');
ylabel('Freq [subcarrier]');
%ylim([213 813]);
xlim([0 140]);
view([0 90]);



