clearvars -except estChannels;
close all;
clc;

% path of function FBMC_auxComputeChannelMatrix2
addpath('/home/tecrail/gtisrc/5G/common/FBMC_signalProcessingMatrices/FBMC_channelModels/FBMC_auxFunctions/FBMC_functions/');

RutaResultados='/home/tecrail/gtisrc/5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/';
ResultsFilename='Results_200kmh_ch0_outdoor.mat';

if ~exist('estChannels', 'var')
    load([RutaResultados filesep ResultsFilename]);
end

%chIndex = 120; % index of the channel
chIndex = 50; % index of the channel
ntaps_c = 60; % causal taps to use
ntaps_ac = 10; % anticausal taps to use
ntaps = ntaps_c+ntaps_ac; % total number of taps

nFFT = 1024;
ltesubframesamplesnum = 15360;
lteframesamplesnum = 153600;
lteframensymb = 140;
lteslotcplen = [80 72*ones(1,6)];

lteslotsamples = cumsum([0 nFFT+80 nFFT+72*ones(1,7-2)]);
ltesubframesamples = [lteslotsamples 15360/2+lteslotsamples];
ltesubframesperframe = 10;
lteframesamples = [];
for ii=1:ltesubframesperframe
    lteframesamples=[lteframesamples (ii-1)*ltesubframesamplesnum+ltesubframesamples]; %#ok<AGROW>
end

ltesubframecplen = lteframesamplesnum;

% plot channel
figure();
surf(abs(estChannels(:,:,chIndex)));
shading flat;
xlabel('Time [symbol]');
ylabel('Freq [subcarrier]');
view([0 90]);

% obtain time domain channel
%ch = ifftshift(estChannels(:,:,chIndex),1);
%ch = [(ch(1,:)+ch(end,:))/2; ch]; % add channel for dc subcarrier
%h = ifft(ch);

% channel with guards
% Problem with this aproximation: the channel is a low pass filter
%ch = ifftshift(estChannels(:,:,chIndex),1);
%ch = [(ch(1,:)+ch(end,:))/2; ch(1:300,:); zeros(423,140); ch(301:end,:)];
%h = ifft(ch);

% channel with nearest values in guards
ch = ifftshift(estChannels(:,:,chIndex),1);
chguards = zeros(423, 140);
for ii = 1:140
    chguards(:,ii) = interp1([1 423], ch([300 301], ii), 1:423, 'spline');
end
ch = [(ch(1,:)+ch(end,:))/2; ch(1:300,:); chguards; ch(301:end,:)];
h = ifft(ch);

% figure();
% surf(abs(ifftshift(ch)));
% shading flat;
% title('Channel completed');


% Smooth channel 
% Only for first ntap_c and last ntap_ac
hsmooth = zeros(ntaps,140);
kk = 0;
tic();
for ii = [1:ntaps_c size(h,1)-ntaps_ac+1:size(h,1)]
    kk = kk + 1;
    hsmooth(kk,:) = smooth(h(ii,:), 5);
    
    % the smoothing procedure may reduce the channel energy
    % we correct this problem here
    eg2 = sum(abs(h(ii,:)).^2);
    eg1 = sum(abs(hsmooth(kk,:)).^2);
    hsmooth(kk,:) = hsmooth(kk,:)*sqrt(eg2/eg1);
end
toc();

figure()
hold on;
grid on
for ii = 1:3
    stem(real(h(ii,:)));
    set(gca, 'ColorOrder', circshift(get(gca, 'ColorOrder'), 1));
    plot(real(hsmooth(ii,:)), '-');
    %fr = fit((1:140).',real(h(ii,:)).','poly4');
    %plot(fr);
end
xlabel('Time [symbol]');
ylabel('Amplitude (real part)');
title('Estimated channel taps');


%% interpolate with low pass filter
% interpfactor = floor(lteframesamplesnum/lteframensymb);
% nsamp = size(ch,2)*interpfactor;
% chext = zeros(nsamp, ntaps);
% 
% % interpolate anticausal taps
% tic();
% for ii = 1:ntaps_ac
%     chext(:,ii) = interp(ch(end-ntaps_ac+ii,:), interpfactor);
% end
% 
% % interpolate causal taps
% for ii = 1:ntaps_c
%     chext(:,ntaps_ac+ii) = interp(ch(ii,:), interpfactor);
% end
% toc();

%% interpolate with splines
xq = 1:lteframesamplesnum;
x = lteframesamples + (1024+repmat(lteslotcplen,1,2*10))/2;
chext = zeros(numel(xq), ntaps);

% matrix of taps to interpolate
%hmat = h;
hmat = hsmooth;

tic();
for ii = 1:ntaps_ac
    %disp(size(hmat,1)-ntaps_ac+ii)
    chext(:,ii) = interp1(x, hmat(end-ntaps_ac+ii,:), xq, 'linear', 'extrap').';
end

% interpolate causal taps
for ii = 1:ntaps_c
    %disp(ii)
    chext(:,ntaps_ac+ii) = interp1(x, hmat(ii,:), xq, 'linear', 'extrap').';
end
toc();

% figure();
% plot(real(chext(:,ntaps_ac+1)));
% hold on;
% grid on;
%plot(real(chext(:,ntaps_ac+2)));
%plot(real(chext(:,ntaps_ac+3)));


% create canonical channel matrix
%delays = -ntaps_ac:ntaps_c-1;
%tic();
%H = FBMC_auxComputeChannelMatrix2(chext, delays, 1, 1, ntaps, ntaps_ac);
%toc();

%% Generate sample OFDM signal
nsubc = 1024;
nused = 600;
cplen = 72;
M = 4;
nsymb = 140;
subcmask = [false true(1,nused/2) false(1,nsubc-nused-1) true(1,nused/2)];

tx_data = qammod(randi([0 M-1], nused, nsymb), M);
tx_f = zeros(nsubc, nsymb);
tx_f(subcmask,:) = tx_data;
tx_t = ifft(tx_f);
tx_t = [tx_t(end-cplen+1:end,:); tx_t];
tx_t = tx_t(:);


%% Apply filter
y = zeros(size(tx_t));
for ii = 1:ntaps
    y(ii:end) = y(ii:end) + tx_t(1:end-ii+1).*chext(1:numel(tx_t)-ii+1,ii);
end


%% Receive OFDM and estimate channel
rx_t1 = [zeros(ntaps_ac,1); y(1:end-ntaps_ac)];
rx_t = reshape(rx_t1, nsubc+cplen, []);
fftstart = ceil((cplen)*0.8);
phasecorrect = exp(1j*2*pi*(cplen-fftstart)/nsubc*(0:nsubc-1));
rx_t = rx_t(fftstart+1:end-(cplen-fftstart),:);
rx_f = bsxfun(@times, phasecorrect.', fft(rx_t));
H = rx_f./tx_f;

figure();
H = H(subcmask,:);
surf(abs(fftshift(H,1)));
%surf(10*log10(abs(ifftshift(H,1))));
shading flat;
xlabel('Time [symbol]');
ylabel('Freq [subcarrier]');
%ylim([213 813]);
xlim([0 140]);
view([0 90]);



