function [FBMC_parameters] = FBMC_parameters_ofdm2()
%FBMC_parametersDefinition


%% id
FBMC_parameters.id = 'v2';


%% Basic parameters

% Number of subcarriers
FBMC_parameters.basicParameters.subcarriersNumber = 1024;

% Number of time-symbols
FBMC_parameters.basicParameters.timeSymbolsNumber = 62;


%% Data blocks parameters

% Data block configuration
% The datablock parameters is an array of structs that configures the
% different data blocks that will carry the frame. Each element of the
% array configures one datablock of the frame. The frame will have the same
% number of datablocks as the number of elements of this array.
% The fields of each datablock are the following:
%   modulation
%       Symbol modulation to use, 'qam' or 'pam'.
%   modulationOrder
%       Modulation order.
%   modulationNormalize
%       Boolean flag that indicates if the modulation used in the data block
%       will be normalized (mean energy of the constellation set to  one).
%   modulationEnergyFactor
%       Factor used to multiply the energy of each symbol. The energy of
%       the symbols will have modulationEnergyFactor times its original
%       energy. If modulationNormalize was set to true, then the mean
%       energy of the constellation will be modulationEnergyFactor.
%       Note that to execute simulations all the symbol must have the same
%       mean energy, so we can assume that the SNR will be the same for all
%       data blocks.       
%   uncodedBitsNumber
%       Number of uncoded bits to transmit in the data block.
%   FECCoderFunction
%       Forward error correcting coding function to use, specified as a
%       cell with format {function, param1,... paramN}.
%   FECDecoderFunction
%       Forward error correcting decoding function to use, specified as a
%       cell with format {function, param1,... paramN}.
%   mappingFunction
%       Function used to map the data block to some of the available
%       data resources of the frame (see the FBMC_dataBlocks folder),
%       with format {function, param1,... paramN}.
%   symbolInterleaver
%       Symbol interleaver to use for the data blocks, 'rnd' for random
%       interleaver or 'none' for no interleaver. Might be removed in the
%       future.
%   symbolInterleaverData
%       Data for the symbol interleaver. Cell with the format
%       {param1, ... paramN}. Currently only 'rnd' requieres as param the
%       seed for the random interleaver .
%


%--------------------%
% Main Data blocks   %
%--------------------%
N = 4;

% from LTE TS 136 213 V13.0.0, table 7.2.3-2: 4-bit CQI Table 2
dataBlockModOrders = [4 4 4 16 16 16 64 64 64 64 64 256 256 256 256];
dataBlockCodeRates = [78 193 449 378 490 616 466,...
                      567 666 772 873 711 797 885 948]/1024;

for k = 1:numel(dataBlockModOrders)
    FBMC_parameters.dataBlock(k).modulation             = 'qam';
    FBMC_parameters.dataBlock(k).modulationOrder        = dataBlockModOrders(k);
    FBMC_parameters.dataBlock(k).modulationNormalize    = true;
    FBMC_parameters.dataBlock(k).modulationEnergyFactor = 1;
    FBMC_parameters.dataBlock(k).uncodedBitsNumber      = dataBlockCodeRates(k);
    FBMC_parameters.dataBlock(k).FECCoderFunction       = {@FBMC_MATLABLTEFECCoder};
    FBMC_parameters.dataBlock(k).FECDecoderFunction     = {@FBMC_MATLABLTEFECDecoder};
    FBMC_parameters.dataBlock(k).mappingFunction        = ...
        {@FBMC_dataBlockSquareMapper, (k-1)*(N), (k-1)*(N)+N-1, 'min', 'max'};
end

%--------------------------------------%
% Additional Data block - wont be used %
%--------------------------------------%
k = numel(dataBlockModOrders)+1;
FBMC_parameters.dataBlock(k).modulation             = 'qam';
FBMC_parameters.dataBlock(k).modulationOrder        = 64;
FBMC_parameters.dataBlock(k).modulationNormalize    = true;
FBMC_parameters.dataBlock(k).modulationEnergyFactor = 1;
FBMC_parameters.dataBlock(k).uncodedBitsNumber      = 'max';

FBMC_parameters.dataBlock(k).FECCoderFunction       = {@FBMC_nullFECCoder};
FBMC_parameters.dataBlock(k).FECDecoderFunction     = {@FBMC_nullFECDecoder};
FBMC_parameters.dataBlock(k).mappingFunction        = ...
    {@FBMC_dataBlockSquareMapper, (k-1)*N, (k-1)*N+1, 'min', 'max'};


%% Symbols interleaving

% Interleaving of block symbols. As the interleaving is done by symbols,
% interleaving blocks with different modulations should not be done.
% TODO documentation
% datablocks is here a vector of data block indexes

for k = 1:numel(FBMC_parameters.dataBlock)
    FBMC_parameters.symbolsInterleaving(k).dataBlocks = 1;
    FBMC_parameters.symbolsInterleaving(k).interleaver = 'rnd';
    FBMC_parameters.symbolsInterleaving(k).interleaverData = {0};
end



%% Pulse shapping parameters

% Cyclic Prefix lenght for OFDM
FBMC_parameters.pulseShapping.cyclicPrefixLength = 72;

% Pulse overlapping Factor
% The overapping factor is definned as L/T,
% where L is the pulse lenght and T the pulse period.
FBMC_parameters.pulseShapping.overlappingFactor = 4; 

% Pulse generation function
% with format {function, param1, ... paramN}
FBMC_parameters.pulseShapping.pulseGenerationFunction =...
    {@FBMC_generateNullPulse};


%% Signal generation

% Signal sampling time
FBMC_parameters.signalGeneration.dt = 1/(15.36e6);


%% Frame generation

% Insert DC null
FBMC_parameters.frameGeneration.insertDCNull = 1;

% Number of side guard subcarriers
FBMC_parameters.frameGeneration.sideGuardSubcarriers = [212, 211];


%% Pilots

% Pilot generation function
% with format {function, param1, ... paramN}
FBMC_parameters.pilots.pilotsGenerationFunction = ...
    {@FBMC_hexPilotIndexer, [0,1,1,1], 4, 4, 3};
    %{@FBMC_squarePilotIndexer, [0,1,1,1]};    

% Frequency spacing of the pilots
FBMC_parameters.pilots.freqSpacing = 6;

% Time spacing of the pilots
FBMC_parameters.pilots.timeSpacing = 4;

% Pilot modulation ('qam' or 'pam')
FBMC_parameters.pilots.modulation = 'qam';

% Pilot modulation order
FBMC_parameters.pilots.modulationOrder = 4;

% Pilots modulation normalized
FBMC_parameters.pilots.modulationNormalize = true;

% Pilots energy factor
FBMC_parameters.pilots.modulationEnergyFactor = 2;


% Number of auxiliary pilots to use for FBMC.
%   Possible values:
%       0 for no auxiliary pilots,
%       1 for plain auxiliary pilot method.
%       2, 4 or 8 for CAP (coded auxiliary pilot method).
FBMC_parameters.pilots.numberAuxiliaryPilots = 0;

% % Number of rows/columns from the center to calculate the interference
% for FBMC. The size of the interference matrix will be
% (1+2*auxPilotMatrixFreqOffset)x(1+2*auxPilotMatrixTimeOffset)
% Not used when numberAuxiliaryPilots is 0
FBMC_parameters.pilots.auxPilotMatrixFreqOffset = 1;
FBMC_parameters.pilots.auxPilotMatrixTimeOffset = 8;


%% Channel model for simulations

% NOTE: For performing simulations it is required that the modulationNormalize
% and modulationEnergyFactor parameters for each dataBlock are set to 1, this
% way we can assume that the energy and power per symbol is approximately the
% same.

% Channel model functions
% with format {function1, function2,... , functionN}
FBMC_parameters.channelModel.channelGenerationFunctions = ...
    {@FBMC_stdchanChannelModel};
    
% Relative speed in m/s for the channel model
FBMC_parameters.channelModel.relativeSpeed = 0.001;

% Carrier frequency used for the channel model
FBMC_parameters.channelModel.carrierFrequency = 2.5e9;

% Signal to noise ratio metric to use for the additive noise. Available values:
% 'SNR': signal to noise ratio
% 'EbNo': Energy per bit per power spectral density (considering uncoded bits)
% 'coded_EbNo': Energy per bit per power spectral density (considering coded bits)
FBMC_parameters.channelModel.noise.snrType = 'SNR';

% Method used to add the noise. Available values:
% 'fixNo': Considers a fixed value of No of 1 and changes the energy of the
%          transmitted signal accordingly. For this method the channel
%          response must be normalized.
% 'varNo': Considers a variable value of No. After passing the signal
%          through the channel, its energy its calculated and No is set
%          accodingly. With this method, specifying a value of inf in the
%          dBValue parameter no noise will be added.
%
% For an AWGN channel, if no pilots and no channel estimation is used, then
% 'varNo' should be used to obtain a correct evaluation of the EVM.
FBMC_parameters.channelModel.noise.addingMethod = 'fixNo';

% SNR dB value using the metric defined in snrType
FBMC_parameters.channelModel.noise.snr_dB = 25;

% If the noise type specified is one of the EbNo types, different blocks
% might have different values of EbNo if their number of bits are
% different. To calculate the level of SNR desired we must specify a
% reference block to use.
FBMC_parameters.channelModel.noise.referenceBlock = 1;

% Channel type for the stdchan channel model (FBMC_stdchanChannelmodel)
% This is specified as a parameter because the channel object will be
% generated first and then will be reused for each iteration.
FBMC_parameters.channelModel.stdchanChannelModel.Type = '3gppTUx';

% Index for the winner2PedroAndXose channel model
% (FBMC_winner2PedroAndXoseChannelModel)
FBMC_parameters.channelModel.winner2PedroAndXoseChannelModel.Index = 16;


%% Channel estimation

% Channel estimation function
% with format {function, param1, ... paramN}
%FBMC_parameters.channelEstimation.channelEstimationFunction = ...
%    {@FBMC_squareChannelEstimator, 'linear'};
FBMC_parameters.channelEstimation.channelEstimationFunction = ...
    {@FBMC_MMSEChannelEstimator};


%% Channel equalization

% Channel equalization function
% with format {function, param1, ... paramN}
FBMC_parameters.channelEqualization.channelEqualizationFunction = ...
    {@FBMC_MMSEChannelEqualizator};


%% Post-processing of the transmitted signal

% TX postprocessing functions to apply in order
% with format
% {{function1, param1, param2, ...};...
%  {function2, param1, param2, ...};...}
FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions = ...
    {{@FBMC_normalizePwSignalProcessing}
     {@FBMC_nullSignalProcessing}};


%% Pre-processing of the received signal

% RX preprocessing functions to apply in order
% with format
% {{function1, param1, param2, ...};...
%  {function2, param1, param2, ...};...}
FBMC_parameters.rxSignalPreProcessing.rxPreProcessingFunctions = ...
    {{@FBMC_nullSignalProcessing}};


%% Modulator

% Modulator function
% with format {function, param1, ... paramN}
FBMC_parameters.modulator.modulatorFunction = ...
    {@FBMC_ofdmcpModulator_2};


%% Demodulator

% Demodulator function
% with format {function, param1, ... paramN}
FBMC_parameters.demodulator.demodulatorFunction = ...
    {@FBMC_ofdmcpDemodulator_2};


%% Synchronization

% Preamble generation function
% with format {function, param1, ... paramN}
FBMC_parameters.synchronization.preambleGenerationFunction = ...
    {@FBMC_ofdmSCPreambleGenerator};

% Zeros after preamble
FBMC_parameters.synchronization.zerosAfterPreamble = 0;

% Preamble length
% NOTE that this parameters is used by the preamble generation function.
% The preamble lenght depends on the selected function and can be different
% of the value set in the preambleLength field. The correct way to obtain
% the dimensions of the preamble is to get the size of the preamble mask
% returned by this function.
FBMC_parameters.synchronization.preambleLength = 1;

% Preamble modulation ('qam' or 'pam')
FBMC_parameters.synchronization.modulation = 'qam';

% Preamble modulation order
FBMC_parameters.synchronization.modulationOrder = 4;

% Preamble modulation normalized
FBMC_parameters.synchronization.modulationNormalize = 1;

% Pilots energy multiplier factor
FBMC_parameters.synchronization.modulationEnergyFactor = 2;

% Synchronization function
% with format {function, param1, ... paramN}
FBMC_parameters.synchronization.synchronizationFunction = ...
    {@FBMC_ofdmSCSynchronizator, 5, 0.3, 0.2, 0.15};        


%% High speed emulation

% Interpolation factor
FBMC_parameters.highSpeedEmulation.interpolationFactor = 1;

% Displacement frequency to apply to the signal in Hertz when interpolation
% is applied. This parameters specifies the new center frequency of the
% signal in Hertz. If 0 then no displacement is applied.
%
% Motivation: When interpolating, the signal shrinks in frequency and some
% subcarriers might move to occupy part of the DC subcarrier. When we use
% amplifiers with DC leakage we can avoid this by displacing the signal in
% frequency centering the signal in a new frequency. 
FBMC_parameters.highSpeedEmulation.displacementFreq = 3e6;

% Filter order for the interpolation
FBMC_parameters.highSpeedEmulation.filterOrder = [];


%% Results evaluation

% Boolean flag to evaluate the uncoded BER
FBMC_parameters.resultsEvaluation.uncodedBER = 1;

% Boolean flag to evaluate the coded BER
FBMC_parameters.resultsEvaluation.codedBER = 1;

% Boolean flag to evaluate the EVM
FBMC_parameters.resultsEvaluation.EVM = 1;

% Saturation value used to calculate the EVM
FBMC_parameters.resultsEvaluation.EVMsaturationVec = 3;
