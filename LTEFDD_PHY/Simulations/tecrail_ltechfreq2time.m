function [COEF, c_orig] = tecrail_ltechfreq2time(estchan, ntaps_c, ntaps_ac, smoothfactor)
%TECRAIL_LTEFREQ2TIME convert channel in frequency to time domain
%
% inputs:
%   estchan - estimated LTE channel of size 600x140
%   ntaps_c - number of causal taps to return
%   ntaps_ac - number of anticausal taps to return
%   smoothfactor - smoothing factor for the channel taps.
%                  set to 1 for no smoothing. (default 5)
%
% output:
%   COEF - time coefficients of the channel. Matrix of
%          153600x(ntaps_ac+ntaps_c) elements. The first ntaps_ac columns
%          will correspond to the anticausal elements. The last ntaps_c
%          columns will correspond to the causal elements.


assert(all(size(estchan) == [600 140]), 'Invalid estchan size');
assert(ntaps_c > 0 && mod(ntaps_c,1) == 0,...
       'ntaps_c must be an integer greater than zero');
assert(ntaps_ac >= 0 && mod(ntaps_ac,1) == 0,...
       'ntaps_ac must be an integer greater or equal than zero');
   
if nargin < 4
    smoothfactor = 5;
end

% total number of taps
ntaps = ntaps_c+ntaps_ac; 

% vector of initial sample number of each LTE symbol
nsubc = 1024;
lteslotcplen = [80 72*ones(1,6)];
ltenslots = 20;
lteframesamplesnum = 153600;
lteframesamples = cumsum([0 repmat(lteslotcplen, 1, ltenslots)]) + ...
                   nsubc*(0:numel(lteslotcplen)*ltenslots);
lteframesamples = lteframesamples(1:end-1);

% Obtain channel time coefficients
% complete channel guards with nearest values
ch = ifftshift(estchan,1);
chguards = zeros(423, 140);
for ii = 1:140
    chguards(:,ii) = interp1([1 423], ch([300 301], ii), 1:423, 'linear');
end
ch = [(ch(1,:)+ch(end,:))/2; ch(1:300,:); chguards; ch(301:end,:)];
%ch = [(ch(1,:)+ch(end,:))/2; ch(1:300,:); zeros(size(chguards)); ch(301:end,:)];
%ch = [(ch(1,:)+ch(end,:))/2; ch(1:300,:);...
%       chguards(1:211,:); zeros(1024,140); chguards(212:end,:); ch(301:end,:)];
h = ifft(ch);

% Smooth channel 
% Only for first ntap_c and last ntap_ac
hsmooth = zeros(ntaps,140);
kk = 0;
for ii = [1:ntaps_c size(h,1)-ntaps_ac+1:size(h,1)]
    kk = kk + 1;
    if smoothfactor > 1
        hsmooth(kk,:) = smooth(h(ii,:), smoothfactor);

        % the smoothing procedure may reduce the channel energy
        % we correct this problem here
        eg2 = sum(abs(h(ii,:)).^2);
        eg1 = sum(abs(hsmooth(kk,:)).^2);
        hsmooth(kk,:) = hsmooth(kk,:)*sqrt(eg2/eg1);
    else
        hsmooth(kk,:) = h(ii,:);
    end
end
c_orig = hsmooth;

% interpolation of channel
xq = 1:lteframesamplesnum;
x = lteframesamples + (1024+repmat(lteslotcplen, 1, ltenslots))/2;
COEF = zeros(lteframesamplesnum, ntaps);

% interpolate anticausal taps
for ii = 1:ntaps_ac
    COEF(:,ii) = interp1(x, hsmooth(end-ntaps_ac+ii,:), xq, 'linear', 'extrap').';
end

% interpolate causal taps
for ii = 1:ntaps_c
    COEF(:,ntaps_ac+ii) = interp1(x, hsmooth(ii,:), xq, 'linear', 'extrap').';
end

end
