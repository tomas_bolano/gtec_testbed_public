% Phased Array Antenna Radiation Pattern
% Code obtained from the following url:
% https://smallsats.org/2013/05/13/phased-array-antenna-radiation-pattern-and-array-configuration/

N = 16; % number of isotropic radiators
f_EISCAT = 2.6e9; % Centre frequency
c = 3*10^8; % Light Speed m/s
l = c/f_EISCAT;
%d = 0.08; % m
d = 0.03; % m
alfa = -90:0.1:90; % Zenith angle

Ed = abs(sin(N*pi*d/l*sind(alfa))./sin(pi*d/l*sind(alfa)))/N;
% Ed - Array Factor, Radiation patern for isotropic radiators

figure();
subplot(2,2,[1 3]);
plot(alfa,Ed);
grid on;
axis([-90 90 0 1]);
xlabel('Zenith angle [deg]');
ylabel('Normalized radiation pattern');
title(sprintf('Distance = %3.2f m, F = %5.3f GHz', d, f_EISCAT*1e-9));
subplot(2,2,[2 4]);
polar(alfa*pi/180,Ed);
hold on;
polar((alfa+180)*pi/180,Ed);
xlabel(sprintf('Polar plot for the radiation pattern d/λ = %5.3f', d/l));
hold off;

%% compare with specification 
imagefile = '/home/tomas/Documentos/medidas_antequera/radiation_pattern_1.png';
C = imread(imagefile);
x = 1:size(C,1);
y = 1:size(C,2);

x = x-mean(x);
y = y-mean(y);

s = 0.89*max(x);
x = x/s;
y = y/s;

I = alfa > 12.1 & alfa < 24.7;
%Ed(I) = Ed(I)*2.5;

figure();
image(x,y,C);
hold on;
polar(alfa*pi/180,Ed,'--r');

xlim([-0.1 1]);
ylim([-1 1]*0.5);
pbaspect([1 1 1]);
daspect([1 1 1]);




