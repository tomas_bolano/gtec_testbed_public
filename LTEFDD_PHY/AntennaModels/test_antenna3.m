imagefile = '/home/tomas/Documentos/medidas_antequera/radiation_pattern_1.png';
C = imread(imagefile);
x = 1:size(C,1);
y = 1:size(C,2);

x = x-mean(x);
y = y-mean(y);

s = 0.89*max(x);
x = x/s;
y = y/s - 0.01;

alfa = -90:0.01:90;
val = zeros(size(alfa));

% value, upper and lower limits
% limmat = [...
%     87.90   -7.4     11
%     19.43    11      21.44
%     29.17    21.44   33.87
%     19.3     33.87  -55
%     49      -16     -7.4
%     22      -40     -27
%     15      -53     -40];
limmat = [...
    87.90   -11      11
    49      -18     -6
    18      -28     -18
    25      -40     -28
    15      -55     -40
    20       11      21
    30       21      30
    18       30      45];

for ii = 1:size(limmat,1)
    I = alfa > -limmat(ii,3) & alfa < -limmat(ii,2);
    xv = linspace(-1,1,nnz(I));
    a = abs(sinc(xv)*limmat(ii,1));
    Ivalid = val(I) < a;
    I(I~=0) = Ivalid;
    val(I) = a(Ivalid);
end
val = val./max(val);


figure();
subplot(2,2,[1 3]);
plot(alfa,val);
grid on;
%axis([-90 90 0 1]);
xlabel('Zenith angle [deg]');
ylabel('Normalized radiation pattern');

subplot(2,2,[2 4]);
%image(x,y,C);
%hold on;
polar(alfa*pi/180, val,'r');
%xlim([-0.1 1]);
%ylim([-1 1]*0.5);
pbaspect([1 1 1]);
daspect([1 1 1]);
