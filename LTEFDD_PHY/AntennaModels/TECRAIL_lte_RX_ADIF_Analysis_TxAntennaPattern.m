function gains = TECRAIL_lte_RX_ADIF_Analysis_TxAntennaPattern(degrees)

%% Constantes para a definición do diagrama de radiación

Orixe = [652 640];
PuntosDiagrama = [1022 672;...
    1158 666;...
    1195 656;...
    1212 640];

%figure();
%plot(Orixe(1), Orixe(2), 'x');
%daspect([1 1 1]);
%hold on;
%grid on;
%plot(PuntosDiagrama(:,1), PuntosDiagrama(:,2), '-x');

x = PuntosDiagrama(:,1);
v = PuntosDiagrama(:,2);
PuntosInterpX = linspace(min(x), max(x), 1000);
PuntosInterpY = interp1(x, v, PuntosInterpX, 'pchip');

angles = atand(((PuntosInterpY-Orixe(2))./(PuntosInterpX-Orixe(1))));
values = abs(sqrt(sum([PuntosInterpX.'-Orixe(1) PuntosInterpY.'-Orixe(2)].^2, 2)));
values = (values - max(values));
values = values./min(values)*-3;

%plot(xq, iv, '--');

gains = zeros(size(degrees));
for ii = 1:length(degrees)
    [~,I]  = min(abs(abs(degrees(ii)) - angles));
    gains(ii) = values(I);
end


end