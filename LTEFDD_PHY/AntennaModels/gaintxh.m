function G = gaintxh(theta)
%GAINTXH
%#ok<*NODEF>

load('points_h');

centerH = -81 - 1j*80;
pointsH = -points_h(:,2) - 1j*points_h(:,1);
pointsH = pointsH-centerH;
pointsH = pointsH./max(abs(pointsH));

abs_valH = abs(pointsH);
angle_valH = angle(pointsH);
[~,I] = min(abs_valH);
angle_valH(I) = pi;

theta = mod(theta,2*pi);
A = theta > pi & theta <= 2*pi;
theta(A) = -theta(A) + 2*pi;

G = interp1(angle_valH, abs_valH, theta, 'linear');

end

