clc
clear
close all

% Horizontal radiation pattern test

file = '/home/tomas/Documentos/medidas_antequera/points_h';
load(file);

centerH = -81 - 1j*80;
pointsH = -points_h(:,2) - 1j*points_h(:,1);
pointsH = pointsH-centerH;
pointsH = pointsH./max(abs(pointsH));

abs_valH = abs(pointsH);
angle_valH = angle(pointsH);
[~,I] = min(abs_valH);
angle_valH(I) = pi;

figure();
plot(real(pointsH), imag(pointsH), '.-');
daspect([1 1 1]);
title('Horizontal radiation pattern (half)');

figure()
plot(angle_valH, abs_valH, '.-');
%daspect([1 1 1]);
xlabel('angle');
ylabel('gain');
title('Horizontal Gain by angle');

%% Vertical radiation pattern test

for ii = 1:6
    file = sprintf('/home/tomas/Documentos/medidas_antequera/points_v%d', ii);
    load(file);
    
end

centerV = 80 - 1j*81;
pointsV = zeros(size(points_v1,1), 6);
pointsV(:,1) = points_v1(:,1) - 1j*points_v1(:,2);
pointsV(:,2) = points_v2(:,1) - 1j*points_v2(:,2);
pointsV(:,3) = points_v3(:,1) - 1j*points_v3(:,2);
pointsV(:,4) = points_v4(:,1) - 1j*points_v4(:,2);
pointsV(:,5) = points_v5(:,1) - 1j*points_v5(:,2);
pointsV(:,6) = points_v6(:,1) - 1j*points_v6(:,2);

pointsV = pointsV-centerV;
pointsV = pointsV./max(abs(pointsV(:,1)));

figure();
plot(real(pointsV), imag(pointsV), '.-');
daspect([1 1 1]);
title('Vertical radiation pattern (half)');

abs_valV = abs(pointsV);
angle_valV = angle(pointsV);

figure()
plot(angle_valV, abs_valV, '.-');
%daspect([1 1 1]);
xlabel('angle');
ylabel('gain');
title('Horizontal Gain by angle');

%% TEST

angle = -90:0.001:90;
gain = zeros(size(angle));

I1 = angle >= -7.4 & angle <= 11;
x = linspace(-1,1,nnz(I1));
gain(I1) = 87.90*sinc(x);

I1 = angle >= 11 & angle <= 21.44;
x = linspace(-1,1,nnz(I1));
gain(I1) = 19.43*sinc(x);



figure();
plot(angle, gain, '.-');


