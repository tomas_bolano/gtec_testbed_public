function G = gaintxv(theta)
%GAINTXV

limmat = [...
    87.90   -11      11
    49      -18     -6
    18      -28     -18
    25      -40     -28
    15      -55     -40
    20       11      21
    30       21      30
    18       30      45];

alfa = -90:0.01:90;
val = zeros(size(alfa));
for ii = 1:size(limmat,1)
    I = alfa > -limmat(ii,3) & alfa < -limmat(ii,2);
    xv = linspace(-1,1,nnz(I));
    a = abs(sinc(xv)*limmat(ii,1));
    Ivalid = val(I) < a;
    I(I~=0) = Ivalid;
    val(I) = a(Ivalid);
end
val = val./max(val);

alfa = mod(alfa(end:-1:1)+90,360);
theta = mod(theta, 2*pi);
G = interp1(alfa*pi/180, val, theta, 'linear');

end

