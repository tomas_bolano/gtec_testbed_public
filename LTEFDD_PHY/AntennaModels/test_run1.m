%% Inicializacións básicas

clear variables;
close all;
format long g
%restoredefaultpath;
clc;

%% Ruta do script

RutaActual = [fileparts(fileparts(fileparts(mfilename('fullpath')))) filesep];


%% Ruta das medidas
MeasurementsFolder = '/home/tomas/Documentos/medidas_antequera/';


%% Nome do ficheiro a procesar
%samplesFilename = '2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.nosplit.usrp.usrpnmea';
%samplesFilename = '2014-12-10--03-55_indoor_etcs_voip_100kmh_n_to_s.nosplit.usrp.usrpnmea';
samplesFilename = '2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp.usrpnmea';
%samplesFilename = '2014-12-10--04-24_indoor_etcs_voip_200kmh_n_to_s.nosplit.usrp.usrpnmea';
%samplesFilename = '2014-12-11--00-46_max_throughput_50kmh_n_to_s_no_s.nosplit.usrp.usrpnmea';
%samplesFilename = '2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.nosplit.usrp.usrpnmea';
%samplesFilename = '2014-12-11--00-06_max_throughput_50kmh_n_to_s.nosplit.usrp.usrpnmea';


%% Obter velocidade en km/h do nome de ficheiro
kmh_ii = strfind(samplesFilename, 'kmh');
VelocidadeKmh = samplesFilename(find(samplesFilename(1:kmh_ii)=='_',1,'last')+1:kmh_ii-1);
VelocidadeKmh = str2num(VelocidadeKmh);


%% Definición de variables básicas dependentes do caso de estudio:
addpath([RutaActual 'LTEFDD_PHY/'])
addpath([RutaActual 'LTEFDD_PHY' filesep 'GPS_Processing'])


%% Definicións de parámetros dependentes do caso de estudio
% Coordenadas UTM da antena
AntX = 347077.9;
AntY = 4103747.4;
% Kilometric point of the antenna
AntKP = 97.075;

% antenna and train haigh
AntH = 20;
TrainH = 3.5;

%% Get GPS data
RutaGPSLog = [MeasurementsFolder samplesFilename];
tic();
GPSData = getGPSData(RutaGPSLog);
toc();


%% Plot trajectory

figure();
utm_x = arrayfun(@(x) x.utm.x, GPSData);
utm_y = arrayfun(@(x) x.utm.y, GPSData);

plot(utm_y, utm_x, '.-');
daspect([1 1 1]);
grid on;
xlabel('UTM Y-coordinate [m]')
ylabel('UTM X-coordinate [m]')
title(sprintf('trajectory (case of v=%d km/h)', VelocidadeKmh));
%title(['trajectory (' samplesFilename ')']);
hold on;
plot(AntY,AntX,'xr');
%legend('trajectory', 'antenna');
hold off;


%% Plot speed
% subplot(5,1,2);
% v = arrayfun(@(x) x.groundspeed.kmh, GPSData); % speed in km/h
t = arrayfun(@(x) x.time.day_seconds, GPSData); % time in seconds
t = t - min(t);

% interpolate utm positions
nsamples = 1000;
t2 = linspace(min(t), max(t), nsamples);
utm_x = interp1(t, utm_x, t2);
utm_y = interp1(t, utm_y, t2);
t = t2;
% 
% plot(t, v, '.-');
% grid on;
% xlabel('time [s]');
% ylabel('inst. speed [km/h]');
% title(sprintf('instantaneous speed (case of v=%d km/h)', VelocidadeKmh));


%% Plot distance to the antenna
figure();
ant_d = abs((utm_x+1j*utm_y) - (AntX+1j*AntY)); % distance to the antenna

% distance to the antenna with sign
ant_d2 = abs((utm_x+1j*utm_y) - (AntX+1j*AntY)).*...
             sign(-imag((utm_x+1j*utm_y) - (AntX+1j*AntY)));
plot(t, ant_d, '.-');
grid on;
xlabel('time [s]');
ylabel('dist. to antenna [m]');
title(sprintf('distance to the antenna (case of v=%d km/h)', VelocidadeKmh));


%% Plot horizontal and vertical angles with the antenna

figure();
angle_h = angle((utm_x+1j*utm_y) - (AntX+1j*AntY));
angle_h = angle_h + 1.8*pi;
angle_v = atan((AntH-TrainH)./ant_d);
subplot(2,1,1);
plot(t, angle_h);
xlabel('time [s]');
ylabel('hor. angle');
subplot(2,1,2);
plot(t, angle_v);
xlabel('time [s]');
ylabel('ver. angle');


%% Plot horizontal and vertical antenna gain

figure();
subplot(2,1,1);
plot(t, gaintxh(angle_h));
xlabel('time [s]');
ylabel('hor. gain');
subplot(2,1,2);
plot(t, gaintxv(angle_v+pi/2));
xlabel('time [s]');
ylabel('ver. gain');


%% TEST with model

c = 3e8;
cf = 2.6e9;
lambda = c/cf; % wavelength

% Generate OFDM signal
nsubc = 1024;
nused = 600;
cplen = 72;
nsymb = 5;
M = 4;
subcmask = [false, true(1,nused/2), false(1,nsubc-nused-1), true(1,nused/2)];

tx_data = qammod(randi([0 M-1], nused, nsymb), M);
tx_grid = zeros(nsubc,nsymb);
tx_grid(subcmask,:) = tx_data;
tx_grid_cp = [tx_grid(end-cplen+1,:); tx_grid];
tx_signal = nsubc*ifft(tx_grid_cp);
tx_signal = tx_signal(:);


% 2-ray channel model

l = sqrt((AntH-TrainH)^2 + ant_d.^2); % length of los path
xx = sqrt((AntH+TrainH)^2 + ant_d.^2); % length of reflected path

theta = atan((AntH + TrainH)./ant_d); % angle of reflected path
eg = 2; % ground electric permeability

Xh = sqrt(eg - cos(theta).^2);
Xv = sqrt(eg - cos(theta).^2)/eg;
X = (Xh+Xv)./2;
gamma = (sin(theta)-X)./(sin(theta)+X);

pw = zeros(1,length(gamma));
for ii = 1:length(pw)
    r_los = lambda*sqrt(gaintxh(angle_h(ii))*gaintxv(angle_v(ii)+pi/2))/(4*pi)*...
            tx_signal*exp(1j*2*pi*l(ii)/lambda)./ant_d(ii);
    tau = c*(xx(ii)-ant_d(ii));
    r_gr  = lambda*gamma(ii)*sqrt(gaintxh(angle_h(ii))*gaintxv(angle_v(ii)+pi/2))/(4*pi)*...
            tx_signal*exp(1j*2*pi*xx(ii)/lambda)./xx(ii);
    pw(ii) = var(r_los + r_gr);
end


figure();
plot(t, 10*log10(pw));
xlabel('time');
ylabel('power');
title('2-path model');

return
% N-ray channel model

N = 13; % number of additional paths
l = sqrt((AntH-TrainH)^2 + ant_d.^2); % length of los path

theta = atan((AntH + TrainH)./ant_d); % angle of reflected path
eg = 2; % ground electric permeability

Xh = sqrt(eg - cos(theta).^2);
Xv = sqrt(eg - cos(theta).^2)/eg;
X = (Xh+Xv)./2;
gamma = (sin(theta)-X)./(sin(theta)+X);

pw = zeros(1,length(gamma));
for ii = 1:length(pw)
    r_los = lambda*sqrt(gaintxh(angle_h(ii))*gaintxv(angle_v(ii)+pi/2))/(4*pi)*...
            tx_signal*exp(1j*2*pi*l(ii)/lambda)./ant_d(ii);
    tau = c*(xx(ii)-ant_d(ii));
    r_gr  = lambda*gamma(ii)*sqrt(gaintxh(angle_h(ii))*gaintxv(angle_v(ii)+pi/2))/(4*pi)*...
            tx_signal*exp(1j*2*pi*xx(ii)/lambda)./xx(ii);
    pw(ii) = var(r_los + r_gr);
end


figure();
plot(t, 10*log10(pw));
xlabel('time');
ylabel('power');
title('2-path model');

