% Parámetros básicos de execución e sincronización

IdentificadorResultados='SynchronizationPoints_Metro_outdoors_ch0_mode00';
basePathForDataFiles = fullfile(filesep, 'media', 'tecrail', 'measData4','metro','outdoors','2014-09-19--plainLTEMatlabToolbox-Mode00','1x1LTEMode00');
samplesFilename='2014-09-19--03-00_pers_rx_lte_mode00.nosplit.usrp';
Canle = 1;
t0=0; % Creo que un rango bo sería de 115 a 130 s
tf=40;
DesprazamentoInicial = 15360+1;
TramasPorAnalise = 2;
ProcesamentoTestbed=0;
ProcesamentoMatlab=1; % Procesamento Matlab ten prioridade no caso en que se executen os dous...
NumeroSector=1:6;
TramasSeparacion=500;
GardadoPorIteracion=0; % Pode causar lentitude de execución para medidas longas

% Parámetros de estima e interpolación de canle

ProcesamentoCanle=1;

cec.PilotAverage = 'UserDefined';
cec.FreqWindow = 1;
cec.TimeWindow = 1;
cec.InterpType = 'v4';
cec.InterpWindow = 'Centered';
cec.InterpWinSize = 1;

% Parámetros de estima do PDP

ProcesamentoPDP = 1; % Require procesamento canle
MostrasPDP = 1024;

MostrasSeparacionGardasRuido = 100;