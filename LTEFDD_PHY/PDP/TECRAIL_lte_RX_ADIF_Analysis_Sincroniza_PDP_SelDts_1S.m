%% Inicializacións básicas

clear variables;
close all;
format long g
restoredefaultpath;
clc;

%% Rutas

RutaActual = [fileparts(mfilename('fullpath')) filesep];
addpath([RutaActual 'Parameters'])

RutaRepositorio = [RutaActual '../../'];

addpath([RutaActual '..'])


%% Definición de variables básicas dependentes do caso de estudio:

TECRAIL_lte_RX_ADIF_SeleccionDatos_Parametros_50_f_s_0;

%% Inicializacións avanzadas

% >> Engado partes necesarias para o salvagardado de datos:

RutaResultados=[RutaRepositorio '5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/'];
addpath(RutaResultados);
load(ResultsFilename);

% >> Definición de constantes para LTE (10 MHz)
nSamplesSlot = 15360/2;
nSymbolsSlot = 14/2;
nSamplesSubframe = nSamplesSlot * 2;
nSymbolsSubframe = nSymbolsSlot * 2;
nSamplesFrame = nSamplesSubframe * 10;
nSymbolsFrame = nSymbolsSubframe * 10;
nHalfSamplesSlot = nSamplesSlot / 2;

%% Procesamento dos datos

% figure,stem(IndicesCela)
% figure,stem(PuntosSincronizacion)

IndicesValidos = ones(1,length(IndicesCela));

% Filtro por punto de sincronización así como por índice de cela

% >> Selecciono o índice de cela que corresponde

if(sum(IndicesCela==Sectores(1))>sum(IndicesCela==Sectores(2)))
    SectorValido = Sectores(1);
else
    SectorValido = Sectores(2);
end

IndicesInvalidos=find(IndicesCela~=SectorValido);

IndicesValidos(IndicesInvalidos)=0;

Moda=mode(PuntosSincronizacion);

IndicesInvalidos=find(abs(PuntosSincronizacion-Moda)>MarxeSincronizacion);

IndicesValidos(IndicesInvalidos)=0;

IndicesInvalidos=find(PuntosSincronizacion<=0);

IndicesValidos(IndicesInvalidos)=0;

IndicesInvalidos=find((1+PuntosSincronizacion+nSamplesFrame-1)>(TramasPorAnalise*nSamplesFrame));

IndicesValidos(IndicesInvalidos)=0;

EixoIndices=1:length(IndicesCela);

EixoIndicesValidos=EixoIndices(logical(IndicesValidos));
PuntosSincronizacionValidos=PuntosSincronizacion(logical(IndicesValidos));
IndicesCelaValidos=IndicesCela(logical(IndicesValidos));

% figure,stem(EixoIndicesValidos,IndicesCelaValidos)
% figure,stem(EixoIndicesValidos,PuntosSincronizacionValidos)

%% Salvagardado de datos

save([RutaResultados ResultsFilename '_' IdentificadorFicheirosProcesados],'EixoIndicesValidos','PuntosSincronizacionValidos','IndicesCelaValidos')