%% Inicializacións básicas

clear variables;
close all;
format long g
restoredefaultpath;
clc;

%% Rutas
RutaActual = [fileparts(mfilename('fullpath')) filesep];
addpath([RutaActual 'Parameters'])
RutaRepositorio = [RutaActual '../../'];
addpath([RutaActual '..'])
addpath([RutaRepositorio 'LTEFDD_PHY' filesep 'GPS_Processing'])

%% Configuración representación PDP
calculatepdp = 1;   % recalculate the PDP using the channel estimations
normalizepdp = 0;   % normalize the PDP
calculatepdp_snrdb = 5;     % SNR in dB for normalized PDP (recomended 0 ~ 5)
calculatepdp_noisedb = 35;  % noise power in DB for unnormalized PDP (recommended 35 ~ 40)
calculatepdp_interpguards = 0;  % complete the channel in the guards using interpolation


%% Configuración cálculo delay spread
% The delay spread is calculated using the PDP. This options configure
% if the PDP should be re-evaluated and how.
ds_calculatepdp = 1;   % recalculate the PSP using the channel estimations
ds_normalizepdp = 0;   % normalize the PDP
ds_calculatepdp_snrdb = 5;    % SNR in dB for normalized PDP
ds_calculatepdp_noisedb = 0;  % noise power in DB for unnormalized PDP
ds_calculatepdp_interpguards = 1;  % complete the channel in the guards using interpolation

ds_ntaps_c = 60; % number of causal taps to use for the delay spread
ds_ntaps_ac = 10; % number of anticausal taps to use for the delay spread


%% Definición de variables básicas dependentes do caso de estudio:
TECRAIL_lte_RX_ADIF_RepresentaPDPs_Parametros_100_f_0;


%% Inicializacións avanzadas

% >> Constantes
fs=15.36e6;
    
% >> Engado partes necesarias para o salvagardado e lectura de datos:

RutaResultados=[RutaRepositorio '5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/'];
%RutaResultados='/home/tomas/extra/GTEC_5G_Simulator_Results/ArtigoCapacidadeFerrovario_AWPL2016/Results/';
addpath(RutaResultados);
load(ResultsFilename);
load([ResultsFilename '_' IdentificadorFicheirosProcesados])


% save original PDPs
PDPs_1 = PDPs;
if calculatepdp
    tic();
    fprintf('Recalculating PDP for plotting... ');
    Indices = 1:size(estChannels,3);
    PDPs = zeros(MostrasPDP,length(Indices));
    for Indice = 1:length(Indices)
        ch = ifftshift(estChannels(:,:,Indice),1);
        chguards = zeros(423, size(ch,2));
        
        % interpolate guards
        if calculatepdp_interpguards
            for ii = 1:size(ch,2)
                chguards(:,ii) = interp1([1 423], ch([300 301], ii), 1:423, 'linear');
            end
        end
        chext = [(ch(1,:)+ch(end,:))/2; ch(1:300,:); chguards; ch(301:end,:)];
        
        % add noise
        if normalizepdp
            chpw = mean(mean(abs(ch).^2));
            noisepw = chpw./10^(calculatepdp_snrdb/10);
            chext = chext + sqrt(noisepw/2)*(randn(size(chext)) + 1j*randn(size(chext)));
        else
            chext = chext + 10^(calculatepdp_noisedb/10)*...
                            (randn(size(chext)) + 1j*randn(size(chext)));
        end
        
        timeResponses = sqrt(1024)*ifft(chext, MostrasPDP);
        timeResponses = timeResponses(1:MostrasPDP,:);
        PDPs(:,Indice) = mean(abs(timeResponses).^2, 2);
        
        %fprintf('------------------------------------\n');
        %fprintf('Channel power: %f\n', mean(mean(abs(chext).^2)));
        %fprintf('PDP total power: %f\n', sum(PDPs(:,Indice))/1024);
    end
    b = toc();
    fprintf('OK (%6.2f s)\n', b);
end

if normalizepdp
    PDPs = bsxfun(@times, PDPs, 1./sum(PDPs));
end


%% PDP - representación de exemplo

% for kk=1:size(PDPs,2)
%     PDPs(:,kk)=PDPs(:,kk)/max(PDPs(:,kk));
% end

eixoy = ((0:(MostrasPDP-1))*(1/15.36e6))*1e6;
delay = 18;
eixoyR = ((0:(MostrasPDP-1))-delay)*(1/15.36e6)*1e6;
eixox = EixoIndicesValidos;

% obtencion puntos kilometricos equivalentes para instantes temporais
FactorDesalinhamento = 0;
RutaGPSLog = [MeasurementsFolder samplesFilename '.usrpnmea'];
%RutaGPSLog = [RutaResultados samplesFilename '.usrpnmea'];
GPSData = getGPSData(RutaGPSLog);
DesprazamentoFinalPorIndicesNonValidos = EixoIndicesValidos(end)-length(IndicesCela);
Eixot = (1/fs)*TramasSeparacion*140*(1024+(6*72+80)/7)*(EixoIndicesValidos-1);
puntosKmtsD = getGPSKP(GPSData, 0, Eixot,getEndFileOffsetGPSKP(GPSData, tfgps+DesprazamentoFinalPorIndicesNonValidos), FactorDesalinhamento);
eixox=puntosKmtsD;

% PDPsNormalizados = PDPs(:,EixoIndicesValidos)/max(max(PDPs(:,EixoIndicesValidos)));
% PDPsRecortados = PDPsNormalizados;
% PDPsRecortados(PDPsRecortados<(10^(-MaximoRangoDBsRepresentacion/10)))=10^(-MaximoRangoDBsRepresentacion/10);
PDPsRecortados = PDPs(:,EixoIndicesValidos);
figure,surf(eixox,eixoyR,10*log10(circshift(PDPsRecortados,delay,1)))
colormap('jet');
shading interp
view([0 90])
colorbar
title('normalized power delay profile [dB]'),xlabel('kilometric point [km]'),ylabel('relative delay [\mus]')
%xlim([1 length(EixoIndicesValidos)]);
% xlim([eixox(1) eixox(end)]);
xlim([94.39 101.03]);
ylim([eixoyR(1) 10])
caxis([-35 0]);
%caxis(10*log10(max(max(abs(PDPsRecortados))))+[-MaximoRangoDBsRepresentacion 0]);
c=caxis;
set(findall(gcf,'-property','FontSize'),'FontSize',12);

%% PDP over an extended region
% only if normalized
if normalizepdp
    KP1 = 94; % kilometric point 1
    KP2 = 96; % kilometric point 2
    
    KP1ind = find(puntosKmtsD >= KP1, 1, 'first');
    KP2ind = find(puntosKmtsD <= KP2, 1, 'first');
    
        
end


%% PDP - representación tendo en conta o retardo de propagación

MostrasRetardo = PuntosSincronizacionValidos-min(PuntosSincronizacionValidos);
%eixoy = ((0:((MostrasPDP-1)+max(MostrasRetardo)))*(1/15.36e6))*1e6;
eixoy = ((0:((MostrasPDP-1)))*(1/15.36e6))*1e6;

% PDPsRetardados = (10^(-MaximoRangoDBsRepresentacion/10))*ones(length(eixoy),size(PDPsRecortados,2));

%PDPsRetardados = c(1)*ones(length(eixoy),size(PDPsRecortados,2));
%PDPsRetardados(1:size(PDPsRecortados,1),1:size(PDPsRecortados,2))=PDPsRecortados;
PDPsRetardados = PDPsRecortados;
minPoint = find(MostrasRetardo == 0);
minDelay = floor(fs*(100/3e8)); % delay for 100 meters
for kk=1:length(EixoIndicesValidos)
    if kk > minPoint
        delayFix = numCorreccionRetardo;
    else
        delayFix = 0;
    end
    PDPsRetardados(:,kk) = circshift(PDPsRetardados(:,kk),...
                                     MostrasRetardo(kk)-delayFix+minDelay);
    %PDPsRetardados(1:MostrasRetardo(kk),kk)=(10^(-MaximoRangoDBsRepresentacion/10));
end
figure,surf(eixox,eixoy,10*log10(PDPsRetardados))
%colormap('jet')
%shading interp
shading flat
%shading faceted
view([0 90])
colorbar
if normalizepdp
    %caxis([-40 5]);
else
    %caxis(c);
    %caxis([72 113]);
end
title('normalized power delay profile [dB]'),xlabel('kilometric point [km]'),ylabel('relative delay [\mus]')
% xlim([1 length(EixoIndicesValidos)]);
% xlim([eixox(1) eixox(end)]);
xlim([94.39 101.03]);
ylim([eixoy(1) eixoy(MostrasPdpRepresentacion)])
set(findall(gcf,'-property','FontSize'),'FontSize',12);

%% Cálculo do RMS delay spread

% old calculation
PDPs_1_recortados =  PDPs_1(:,EixoIndicesValidos);
PDPsRecortadosC = circshift(PDPs_1_recortados,round(MostrasPDP/2));

DopplerSpread=zeros(1,length(EixoIndicesValidos));
for ii=1:length(EixoIndicesValidos)
    DopplerSpread(ii)=sqrt(sum(PDPsRecortadosC(:,ii).*(((eixoy*1e-6).^2).'))...
        /sum(PDPsRecortadosC(:,ii))-(sum(PDPsRecortadosC(:,ii).*((eixoy*1e-6).'))...
        /sum(PDPsRecortadosC(:,ii))).^2);
end


% set PDPs variable to original PDPs
PDPs = PDPs_1;

if ds_calculatepdp
    tic();
    fprintf('Recalculating PDP for delay spread... ');
    Indices = 1:size(estChannels,3);
    PDPs = zeros(MostrasPDP,length(Indices));
    for Indice = 1:length(Indices)
        ch = ifftshift(estChannels(:,:,Indice),1);
        chguards = zeros(423, 140);
        
        % interpolate guards
        if ds_calculatepdp_interpguards
            for ii = 1:140
                chguards(:,ii) = interp1([1 423], ch([300 301], ii), 1:423, 'linear');
            end
        end
        chext = [(ch(1,:)+ch(end,:))/2; ch(1:300,:); chguards; ch(301:end,:)];
        
        % add noise
        if ds_normalizepdp
            chpw = mean(mean(abs(ch).^2));
            noisepw = chpw./10^(ds_calculatepdp_snrdb/10);
            chext = chext + sqrt(noisepw/2)*(randn(size(chext)) + 1j*randn(size(chext)));
        else
            chext = chext + 10^(ds_calculatepdp_noisedb/10)*...
                            (randn(size(chext)) + 1j*randn(size(chext)));
        end
        
        timeResponses = sqrt(1024)*ifft(chext);
        timeResponses = timeResponses(1:MostrasPDP,:);
        PDPs(:,Indice) = mean(abs(timeResponses).^2, 2);
        
        %fprintf('------------------------------------\n');
        %fprintf('Channel power: %f\n', mean(mean(abs(chext).^2)));
        %fprintf('PDP total power: %f\n', sum(PDPs(:,Indice))/1024);
    end
    b = toc();
    fprintf('OK (%6.2f s)\n', b);
end

if ds_normalizepdp
    PDPs = bsxfun(@times, PDPs, 1./sum(PDPs));
end


PDPs_1_recortados =  PDPs(:,EixoIndicesValidos);
ds_tau = (-ds_ntaps_ac:ds_ntaps_c-1)/fs;
delaySpread = zeros(1, length(EixoIndicesValidos));

for ii = 1:length(EixoIndicesValidos)
    pdp_ii = circshift(PDPs_1_recortados(:,ii), ds_ntaps_ac);
    pdp_ii = pdp_ii(1:ds_ntaps_ac+ds_ntaps_c).';
    
    chpw = sum(pdp_ii);
    T1 = sum(pdp_ii/chpw.*ds_tau);
    T2 = sum(pdp_ii/chpw.*ds_tau.^2);
    delaySpread(ii) = sqrt(T2-T1^2);
end

%%
figure()
%plot(eixox,DopplerSpread*1e6);
grid on;
hold on;
plot(eixox,delaySpread*1e6);
plot(eixox,smooth(delaySpread*1e6,7), 'LineWidth', 2);
plot([min(eixox) max(eixox)],mean(delaySpread*1e6)*[1 1], 'LineWidth', 2);
% xlim([1 length(EixoIndicesValidos)]);
% xlim([eixox(1) eixox(end)]);
xlim([94.39 101.03]);
ylim([0 0.5]);
grid on
xlabel('kilometric point [km]')
ylabel('RMS delay Spread [\mu{}s]')
title('delay spread');
legend('instantaneous value', 'averaged value', 'mean value');
%legend('old calculation', 'new calculation');

set(findall(gcf,'-property','FontSize'),'FontSize',12);
