% Parámetros básicos de execución e sincronización

IdentificadorResultados='SynchronizationPoints_50kmh_ch1_indoor_matlab_s';
samplesFilename='2014-12-11--00-25_max_throughput_50kmh_s_to_n_no_n.nosplit.usrp';
Canle = 2;
t0=0; % Creo que un rango bo sería de 115 a 130 s
tf=615;
DesprazamentoInicial = 15360+1;
TramasPorAnalise = 2;
ProcesamentoTestbed=0;
ProcesamentoMatlab=1; % Procesamento Matlab ten prioridade no caso en que se executen os dous...
NumeroSector=1:2;
TramasSeparacion=100;
GardadoPorIteracion=0; % Pode causar lentitude de execución para medidas longas

% Parámetros de estima e interpolación de canle

ProcesamentoCanle=1;

cec.PilotAverage = 'UserDefined';
cec.FreqWindow = 1;
cec.TimeWindow = 1;
cec.InterpType = 'v4';
cec.InterpWindow = 'Centered';
cec.InterpWinSize = 1;

% Parámetros de estima do PDP

ProcesamentoPDP = 1; % Require procesamento canle
MostrasPDP = 1024;