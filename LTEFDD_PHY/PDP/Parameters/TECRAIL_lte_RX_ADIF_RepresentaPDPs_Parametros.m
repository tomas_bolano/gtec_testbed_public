ResultsFilename='Results_100kmh_ch0_outdoor.mat';
UmbralLonxitudeTraspaso = 10;
MarxeSincronizacion = 200;
IdentificadorFicheirosProcesados='Selected';
Sectores=[3 4];
MaximoRangoDBsRepresentacion = 40;

% Parámetros de estima do PDP (debe ser coherente cos ficheiros que analizan os datos)

MostrasPDP = 1024;
MostrasPdpRepresentacion = 300;