ResultsFilename='Results_PDP_200kmh_ch1_outdoor';
UmbralLonxitudeTraspaso = 10;
MarxeSincronizacion = 200;
IdentificadorFicheirosProcesados='Selected';
Sectores=[3 4];
MaximoRangoDBsRepresentacion = 40;

% Parámetros de estima do PDP (debe ser coherente cos ficheiros que analizan os datos)

MostrasPDP = 1024;
MostrasPdpRepresentacion = 300;
TramasSeparacion = 100;

MeasurementsFolder='/media/tecrail/measData4/';
samplesFilename='2014-12-10--04-25_outdoor_etcs_voip_200kmh_n_to_s.nosplit.usrp';

t0=0; % Creo que un rango bo sería de 115 a 130 s
tf=200;
t0gps=145;
tfgps=345;

% problema cando o tren cruza a antena cando se ten en conta o retardo de
% propagación. Hay unhas mostras de desvío. Corríxese sumando unha constante
% a partires do minimo.
numCorreccionRetardo = 3;