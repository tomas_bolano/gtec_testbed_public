%% Inicializacións básicas

clear variables;
close all;
format long g
restoredefaultpath;
clc;

%% Rutas

RutaActual = [fileparts(mfilename('fullpath')) filesep];
addpath([RutaActual 'Parameters'])

RutaRepositorio = [RutaActual '../../'];

addpath([RutaActual '..'])

%% Definición de variables básicas dependentes do caso de estudio:

TECRAIL_lte_RX_ADIF_SeleccionDatos_Parametros_100_f_0;

%% Inicializacións avanzadas

% >> Engado partes necesarias para o salvagardado de datos:

RutaResultados=[RutaRepositorio '5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/'];
addpath(RutaResultados);
load(ResultsFilename);

%% Procesamento dos datos

% figure,stem(IndicesCela)
% figure,stem(PuntosSincronizacion)

IndicesValidos = ones(1,length(IndicesCela));

% Filtro por punto de sincronización así como por índice de cela

% >> Busco o punto de cambio dun sector a outro

for kk=1:(length(IndicesCela)-UmbralLonxitudeTraspaso)
    if(IndicesCela(kk+(0:(UmbralLonxitudeTraspaso-1)))==...
            Sectores(2)*ones(1,UmbralLonxitudeTraspaso))
        ValorTraspaso=kk;
        break;
    end
end

IndicesInvalidos=find(IndicesCela(1:(ValorTraspaso-1))~=Sectores(1));

IndicesValidos(IndicesInvalidos)=0;

IndicesInvalidos=find(IndicesCela(ValorTraspaso:end)~=Sectores(2))+ValorTraspaso-1;

IndicesValidos(IndicesInvalidos)=0;

Moda=mode(PuntosSincronizacion);

IndicesInvalidos=find(abs(PuntosSincronizacion-Moda)>MarxeSincronizacion);

IndicesValidos(IndicesInvalidos)=0;

EixoIndices=1:length(IndicesCela);

EixoIndicesValidos=EixoIndices(logical(IndicesValidos));
PuntosSincronizacionValidos=PuntosSincronizacion(logical(IndicesValidos));
IndicesCelaValidos=IndicesCela(logical(IndicesValidos));

% figure,stem(EixoIndicesValidos,IndicesCelaValidos)
% figure,stem(EixoIndicesValidos,PuntosSincronizacionValidos)

%% Salvagardado de datos

save([RutaResultados ResultsFilename '_' IdentificadorFicheirosProcesados],'EixoIndicesValidos','PuntosSincronizacionValidos','IndicesCelaValidos')