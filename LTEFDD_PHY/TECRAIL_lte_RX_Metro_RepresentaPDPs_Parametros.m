ResultsFilename='Results_Metro_outdoors_ch0_mode00.mat';
UmbralLonxitudeTraspaso = 10;
MarxeSincronizacion = 200;
IdentificadorFicheirosProcesados='Selected';
Sectores=[1];
MaximoRangoDBsRepresentacion = 40;

% Parámetros de estima do PDP (debe ser coherente cos ficheiros que analizan os datos)

MostrasPDP = 1024;
MostrasPdpRepresentacion = 300;
TramasSeparacion = 100;

MeasurementsFolder='/media/tecrail/measData4/';
samplesFilename='2014-12-10--03-54_outdoor_etcs_voip_100kmh_n_to_s.nosplit.usrp';

t0=0;
tf=40;
t0gps=139;
tfgps=474;