%% Inicializacións básicas

clear variables;
close all;
format long g
restoredefaultpath;
clc;

%% Definición de variables básicas dependentes do caso de estudio:

addpath('/home/tecrail/gtisrc/LTEFDD_PHY/')
TECRAIL_lte_RX_ADIF_RepresentaPDPs_Parametros;

%% Inicializacións avanzadas
    
% >> Engado partes necesarias para o salvagardado e lectura de datos:

RutaResultados='/home/tecrail/gtisrc/5G/common/ArtigoCapacidadeFerroviario_AWPL2016/Results/';
addpath(RutaResultados);
load(ResultsFilename);
load([ResultsFilename(1:(end-4)) '_' IdentificadorFicheirosProcesados ResultsFilename((end-3):end)])

%% Representación de exemplo

% for kk=1:size(PDPs,2)
%     PDPs(:,kk)=PDPs(:,kk)/max(PDPs(:,kk));
% end

% eixoy = ((0:71)*(1/15.36e6))*1e6;
% eixox = 1:length(EixoIndicesValidos); % Non correcto pero evita problemas coa interpolación
% figure,surf(eixox,eixoy,10*log10(PDPs(:,EixoIndicesValidos)))
% shading interp
% view([0 90])
% colorbar
% title('power delay profile'),xlabel('time [s]'),ylabel('relative delay [\mus]')
% xlim([1 length(EixoIndicesValidos)]);
% ylim([eixoy(1) eixoy(end)])

figure;

for kk=1:1:length(EixoIndicesValidos)
    helperPlotChannelEstimate(estChannels(:,:,EixoIndicesValidos(kk)));
    title(['Índice = ' num2str(EixoIndicesValidos(kk))]);
    pause
end