function FBMC_blockMap = FBMC_dataBlockEqualMapper(FBMC_parameters,...
    FBMC_constants, nBlocks, blockIndex)
%FBMC_DATABLOCKSQUAREMAPPER Data block equal mapper
%
% Divides the time-frequency grid in nBlocks blocks of the same
% size and returns the block map for the block with index block_index.
% block_index may takes values from 1 to nBlocks.
%

%% Parameters extraction

Snum = FBMC_parameters.basicParameters.subcarriersNumber;
Dnum = FBMC_parameters.basicParameters.timeSymbolsNumber;


%% Calculate and return the mapping

%nElements = Snum*Dnum;
nElements = nnz(FBMC_constants.dataMask);
nElementsIndexes = find(FBMC_constants.dataMask);

assert(nElements/nBlocks > 1,...
       sprintf('Not enough elements to be splitted in %d parts.', nBlocks));
assert(mod(nBlocks,1) == 0 && mod(blockIndex,1) == 0,...
       'nBlocks and block_index must be integers.');
assert(nBlocks > 0,...
       'nBlocks must be greater than 0.');
assert(blockIndex >= 1 && blockIndex <= nBlocks,...
       'blockIndex must be greater or equal than 1 and less or equal than nBlocks.');


if blockIndex == nBlocks
    startInd = floor(nElements/nBlocks)*(blockIndex-1)+1;
    endInd = nElements;
else
    startInd = floor(nElements/nBlocks)*(blockIndex-1)+1;
    endInd = floor(nElements/nBlocks)*(blockIndex);
end



FBMC_blockMap = false(Snum, Dnum);
FBMC_blockMap(nElementsIndexes(startInd:endInd)) = true;

end

