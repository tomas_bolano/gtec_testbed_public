function FBMC_blockMap = FBMC_dataBlockSquareMapper(FBMC_parameters,...
    ~, tmin, tmax, fmin, fmax)
%FBMC_DATABLOCKSQUAREMAPPER Data block square mapper
%
% Assigns a square region of the time frequency grid between the
% minimum and maximum time symbol indexes indicated by tmin and tmax, an
% the minimum and maximum frequency subcarriers indicated by fmin and fmax.
%
% tmin may take values from 0 to the total time symbols minus one, or 'min'
% to specify the minimum value (0).
%
% tmax may take values from 0 to the total time symbols minus one, or 'max'
% to specify the maximum value.
%
% fmin may take values from -N/2 to N-2-1, where N is the number of
% subcarriers, or 'min' to specify the minimum value.
%
% fmax may take values from -N/2 to N-2-1, where N is the number of
% subcarriers, or 'max' to specify the maximum value.
%

%% Parameters extraction

Snum = FBMC_parameters.basicParameters.subcarriersNumber;
Dnum = FBMC_parameters.basicParameters.timeSymbolsNumber;


%% Calculate and return the mapping

% Obtain the relative numeration of the subcarriers from
% -N/2 to N/2-1, where N is the number of subcarriers.
freqRelativeNumbering = ifftshift([(0:Snum/2-1) (-Snum/2:1:-1)]).';
freqNumbering = 1:Snum;

FBMC_blockMap = false(Snum, Dnum);

if strcmpi(tmin, 'min')
    tmin = 0;
end

if strcmpi(tmax, 'max')
    tmax = Dnum-1;
end

if strcmpi(fmin, 'min')
    fmin = -Snum/2;
end

if strcmpi(fmax, 'max')
    fmax = Snum/2-1;
end

assert(tmin <= tmax, 'tmin must be less than tmax');
assert(fmin <= fmax, 'fmin must be less than fmax');

assert(tmin >= 0, 'tmin must be greater or equal than 0');
assert(tmax <= Dnum-1, ['tmax must be less or equal than M-1, ',...
                        'where M is the number of time symbols']);

assert(fmin >= -Snum/2, ['fmin must be greater or equal than N/2, ',...
                         'where N is the number of subcarriers']);
assert(fmax <= Snum/2, ['fmax must be less or equal than N/2, ',...
                        'where N is the number of subcarriers']);

                    
FBMC_blockMap(freqNumbering(ismember(freqRelativeNumbering, fmin:fmax)),...
              (tmin:tmax)+1) = true;

end

