function [FBMC_outSignal, FBMC_additionalData] = FBMC_rayleighChannelModel(~,...
    FBMC_pregeneratedConstants, FBMC_inSignal, ~)
%FBMC_STDCHANCHANNELMODEL Channel model using the MATLAB stdchan function

FBMC_additionalData = [];
FBMC_outSignal = filter(FBMC_pregeneratedConstants.channelModel.rayleighchanChannelObject,...
                        FBMC_inSignal);

end