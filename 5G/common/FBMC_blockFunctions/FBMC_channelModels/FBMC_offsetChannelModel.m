function [FBMC_outSignal, FBMC_additionalData] = FBMC_offsetChannelModel(FBMC_parameters,~,FBMC_inSignal,~)
%FBMC_OFFSETCHANNELMODEL Introduce an offset in time and frequency

t_offset = FBMC_parameters.channelModel.offsetChannelModel.offsetTime;
f_offset = FBMC_parameters.channelModel.offsetChannelModel.offsetFrequency;
 
FBMC_additionalData = [];
FBMC_outSignal = FBMC_inSignal.*exp((0:length(FBMC_inSignal)-1).'*1j*2*pi*f_offset);   
FBMC_outSignal = [zeros(t_offset,1); FBMC_outSignal];

end

