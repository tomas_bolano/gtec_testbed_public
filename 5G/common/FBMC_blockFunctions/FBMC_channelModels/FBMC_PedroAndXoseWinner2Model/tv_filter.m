function [y_out] = tv_filter(y, filtered_h, delta_t)

NumTimeSamples = size(filtered_h, 2);
traceDuration = (NumTimeSamples)*delta_t;

t = 0:delta_t:delta_t*(NumTimeSamples-1);
t_interp = linspace(0, traceDuration, length(y));
STEPS = 8;
signal_step_size = length(y)/STEPS;

y_out = zeros(length(y) + size(filtered_h,1) - 1, 1);
for ss = 1:STEPS
    H_time_interp = zeros(size(filtered_h,1), signal_step_size);
    signal_step_ind = (ss-1)*signal_step_size+1:ss*signal_step_size;
    t_step_interp = t_interp(signal_step_ind);
    
    for ii = 1:size(filtered_h,1)
        H_time_interp(ii, :) = interp1(t, filtered_h(ii,:), t_step_interp, 'spline');
    end

    y_step = zeros(signal_step_size + size(filtered_h,1) - 1, 1);
    for ii = 1:size(H_time_interp, 1)
        y_step = y_step + [zeros(ii-1, 1); y(signal_step_ind).*H_time_interp(ii,:).'; zeros(size(H_time_interp, 1)-ii, 1)];
    end

    y_out = y_out + [zeros((ss-1)*signal_step_size, 1); y_step; zeros((STEPS-ss)*signal_step_size, 1)];
end
