clear;
close all;


v_MS = 431; % Velocidad de la MS en km/h

% Frecuencia de muestreo en banda base.
fs = 15.36e6;
% fs = 30.72e6;
% fc = 5.25e9;
fc = 2.6e9;
ts = 1/fs;
TTI = 0.001;
traceDuration = 4.0/20;
% traceDuration = 2.0/20;
% Número de Resource Blocks LTE.
numRBs = 100;
lambda = 2.998e8/fc;
sample_density = 128;
v_MS = v_MS*1000/3600;
ts_channel = lambda/(2*sample_density*v_MS);

% Definición de los arrays de antenas que utilizarán las estaciones. Estos
% arrays se pueden generar en una fase de preprocesado, ya que es muy
% costosa computacionalmente. La función arrayparset por defecto devuelve 6
% arrays de ejemplo con las siguientes configuraciones:
% - Arrays lineales (ULA) con 2/4/8 antenas.
% - Arrays circulares (UCA) con 4/8 antenas.
if exist('arrays.mat', 'file') == 0
    Arrays = arrayparset;
    save('arrays.mat', 'Arrays');
else
    load('arrays.mat');
end
% Array=AntennaArray('UCA',16,0.05,'FP-ECS',FP);

MsAAIdx = 1;
BsAAIdxCell = {4; 4};

layoutpar = layoutparset(MsAAIdx, BsAAIdxCell, 1, Arrays);
layoutpar.ScenarioVector = 16;
layoutpar.Pairing = [2; 3];
layoutpar.Stations(1).Pos = [-150; 2; 32];
layoutpar.Stations(2).Pos = [150; -2; 32];
layoutpar.Stations(3).Pos = [20; 0; 1.5];
layoutpar.Stations(3).Velocity = [v_MS; 0; 0];


wimpar = wimparset;
% wimpar.PathLossModelUsed = 'yes';
wimpar.CenterFrequency = fc;
wimpar.NumTimeSamples = round(traceDuration/ts_channel);
wimpar.SampleDensity = sample_density;

if exist('BULK.mat', 'file') == 0
    disp('Modelo de canle: non hai condici�ns inciais, xero unhas novas...');
    [~, ~, BULK] = wim(wimpar, layoutpar);
    muestras = (BULK.delays/ts);
    maximum_tau = round(2*max(BULK.delays)/ts);
    instants = (-0:maximum_tau).';
    muestras_rep = muestras(ones(size(instants)), :) - instants(:,ones(size(muestras)));
    tail = zeros(length(instants)-1, 1);
    save('BULK.mat', 'BULK','muestras','maximum_tau','isntants','muestras_rep','tail');
else
    disp('Modelo de canle: atopadas condici�ns iniciais, usareinas...');
    load('BULK.mat');
end

% % [~, ~, BULK] = wim(wimpar, layoutpar);
% % muestras = (BULK.delays/ts);
% % maximum_tau = round(2*max(BULK.delays)/ts);
% % instants = (-0:maximum_tau).';
% % muestras_rep = muestras(ones(size(instants)), :) - instants(:,ones(size(muestras)));
% % tail = zeros(length(instants)-1, 1);



% % % % % folder = '../../Resultados/SinaisSynopsys/2013--11--25/';
for files = 0:19
    filename = sprintf('SinalTX%02d.mat', files);
    load(strcat(folder, filename));
%     layoutpar.Stations(3).Pos = layoutpar.Stations(3).Pos + [traceDuration*v_MS*1000/3600 0 0];
    [H, DELAYS, BULK] = wim(wimpar, layoutpar, BULK);
    H1 = H{1};
    H_time = squeeze(H1(1,1,:,:));
    if ~strcmp(wimpar.PathLossModelUsed, 'yes')
        H_time = H_time./mean(sqrt(sum(abs(H_time).^2)));
    end
    
    filtered_h = sinc(muestras_rep)*H_time;

    y_out = tv_filter(y, filtered_h, BULK.delta_t);
    y_out = y_out + [tail; zeros(length(y), 1)];
    tail = y_out(end-size(filtered_h, 1)+2:end);
    y_out = y_out(1:end-size(filtered_h, 1)+1);
    filename = sprintf('SinalTX%02d_%s_%dkmh.mat', files, ScenarioMapping(layoutpar.ScenarioVector), round(v_MS*3600/1000));
    save(strcat(folder, filename), 'y_out');
end
