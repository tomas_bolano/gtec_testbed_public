clear;
close all;

% A1 NLoS
P = [-2.2 -6.6 -2.1 -5.8 -3.3 -4.7 -4.1 -8.2 -3.0 -5.2 -7.0 -4.6 -6.8 -8.6 -10.0 -12.1 -12.4 -11.8 -20.4 -16.6];
tau = [0 5 5 5 15 15 15 20 20 25 30 35 40 45 80 85 110 115 150 175]*1e-9;
% A1 LoS
% P = [0 -15.1 -16.9 -15.8 -13.5 -15.1 -17.3 -19.1 -19.2 -23.5 -18.3 -23.3 -29.1 -14.2 -21.6 -23.4];
% tau = [0 5 10 10 25 50 55 60 65 75 75 115 115 145 195 350];

P_nat = 10.^(P/10);

Pm = sum(P_nat);
Tm = sum(P_nat.*tau)/Pm;
Stau = sqrt(sum(P_nat.*(tau.^2))/Pm - Tm^2);

% A1 NLoS
ds_mu = -7.42;
ds_sigma = 0.19;
r_ds = 3;

ds = 10^ds_mu;

random_taus = sort(-ds*r_ds*log(rand(1, 1000)));
powers = exp(-random_taus*(r_ds-1)/(r_ds*ds)).*10.^(randn(1, 1000)/10);
powers = powers./sum(powers);


Pm = sum(powers);
Tm2 = sum(powers.*random_taus)/Pm;
Stau2 = sqrt(sum(powers.*(random_taus.^2))/Pm - Tm2^2);
