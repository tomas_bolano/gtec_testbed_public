function [ySaida,BULK]=simulations_channel_filter(yEntrada,xvInt,Vel,BULKe,channel_model_index,fs,fc)
% clear;
% close all;
if(nargin<4)
    BULKe=[];
end

if(nargin<5)
    channel_model_index=16;
end

if(nargin<6)
    fs = 200e6;
end

if(nargin<7)
    fc = 2.5e9;
end


[RutaFuncion,~] = fileparts(mfilename('fullpath'));

% disp('Modelo de canle: Establecendo par�metros...')

ySaida=zeros(size(yEntrada,1),size(yEntrada,2));

v_MS = Vel; % Velocidad de la MS en m/s

% Frecuencia de muestreo en banda base.

% fs = 30.72e6;
% fc = 5.25e9;

ts = 1/fs;
TTI = 0.001;
traceDuration = (1e-3)*xvInt/20; %%suaves%%
%traceDuration = (1e-3)*xvInt;%%suaves%%
% traceDuration = 2.0/20;
lambda = 2.998e8/fc;
sample_density = 1024;
% v_MS = v_MS*1000/3600;
ts_channel = lambda/(2*sample_density*v_MS);

% Definición de los arrays de antenas que utilizarán las estaciones. Estos
% arrays se pueden generar en una fase de preprocesado, ya que es muy
% costosa computacionalmente. La función arrayparset por defecto devuelve 6
% arrays de ejemplo con las siguientes configuraciones:
% - Arrays lineales (ULA) con 2/4/8 antenas.
% - Arrays circulares (UCA) con 4/8 antenas.

% disp('Modelo de canle: Definindo antenas...')

if exist('arrays.mat', 'file') == 0
    Arrays = arrayparset;
    save('arrays.mat', 'Arrays');
else
    load('arrays.mat');
end
% Array=AntennaArray('UCA',16,0.05,'FP-ECS',FP);

% disp('Modelo de canle: Definindo escenario...')

MsAAIdx = 1;
BsAAIdxCell = {4; 4};

layoutpar = layoutparset(MsAAIdx, BsAAIdxCell, 1, Arrays);
layoutpar.ScenarioVector = channel_model_index;
layoutpar.Pairing = [2; 3];
bsHeight=32;
if(channel_model_index==21)
    bsHeight=2;
end
% disp(sprintf('Modelo de canle: �ndice do modelo -> %d, altura da estaci�n base -> %d [m]',channel_model_index,bsHeight));
layoutpar.Stations(1).Pos = [-150; 2; bsHeight];
layoutpar.Stations(2).Pos = [150; -2; bsHeight];
layoutpar.Stations(3).Pos = [20; 0; 1.5];
layoutpar.Stations(3).Velocity = [v_MS; 0; 0];

% disp('Modelo de canle: Establecendo par�metros com�ns...')

wimpar = wimparset;
% wimpar.PathLossModelUsed = 'yes';
wimpar.CenterFrequency = fc;
wimpar.NumTimeSamples = round(traceDuration/ts_channel);
wimpar.SampleDensity = sample_density;

% disp('Modelo de canle: Obtendo condici�ns Iniciais...')
% 
if(isempty(BULKe))
    % disp('Modelo de canle: non recibo condici�ns iniciais de entrada...')
    if exist([RutaFuncion filesep 'BULK.mat'], 'file') == 0
        % disp('Modelo de canle: non hai condici�ns inciais, xero unhas novas...');
        [~, ~, BULK] = wim(wimpar, layoutpar);
        save([RutaFuncion filesep 'BULK.mat'], 'BULK');
    else
        % disp('Modelo de canle: atopadas condici�ns iniciais, usareinas...');
        load([RutaFuncion filesep 'BULK.mat']);
    end
else
    % disp('Modelo de canle: recibo condici�ns iniciais de entrada...')
    BULK=BULKe;
end

muestras = (BULK.delays/ts);
    maximum_tau = round(2*max(BULK.delays)/ts);
    instants = (-0:maximum_tau).';
    muestras_rep = muestras(ones(size(instants)), :) - instants(:,ones(size(muestras)));
tail = zeros(length(instants)-1, 1);

% [~, ~, BULK] = wim(wimpar, layoutpar);
% muestras = (BULK.delays/ts);
% maximum_tau = round(2*max(BULK.delays)/ts);
% instants = (-0:maximum_tau).';
% muestras_rep = muestras(ones(size(instants)), :) - instants(:,ones(size(muestras)));
% tail = zeros(length(instants)-1, 1);
% folder = '../../Resultados/SinaisSynopsys/2013--11--25/';
Iteracion=1;
NumeroExecucions=20;

% we have to make sure that the y singnals are a multiple of 8 because of tv_filter
STEP = 8; % defined in tv_filter
ylen = (length(yEntrada)/NumeroExecucions-mod(length(yEntrada)/NumeroExecucions,STEP));
lastChunkLen = length(yEntrada)-ylen*(NumeroExecucions-1);
lastChunkFixedLen = (lastChunkLen-mod(lastChunkLen,8))+8*(mod(lastChunkLen,8)~=0);

for files = 0:1:NumeroExecucions-1
    % disp(['[' sprintf('%3.5f',100*Iteracion/NumeroExecucions) '%] Filtrando...']);Iteracion=Iteracion+1;
%     filename = sprintf('SinalTX%02d.mat', files);
%     load(strcat(folder, filename));
% % % % % % % % % % % %     layoutpar.Stations(3).Pos =
% layoutpar.Stations(3).Pos + [traceDuration*v_MS*1000/3600 0 0];
    %y=yEntrada((1:(length(yEntrada)/20))+files*(length(yEntrada)/20));
    if files == NumeroExecucions-1
       y = [yEntrada((1:ylen)+files*ylen); zeros(lastChunkFixedLen-ylen,1)];
    else
       y = yEntrada((1:ylen)+files*ylen);
    end
    
    [H, ~, BULK] = wim(wimpar, layoutpar, BULK);
    H1 = H{1};
    H_time = squeeze(H1(1,1,:,:));
    if ~strcmp(wimpar.PathLossModelUsed, 'yes')
        H_time = H_time./mean(sqrt(sum(abs(H_time).^2)));
    end
    
    filtered_h = sinc(muestras_rep)*H_time;

    y_out = tv_filter(y, filtered_h, BULK.delta_t);
    y_out = y_out + [tail; zeros(length(y), 1)];
    tail = y_out(end-size(filtered_h, 1)+2:end);
    y_out = y_out(1:end-size(filtered_h, 1)+1);
%     filename = sprintf('SinalTX%02d_%s_%dkmh.mat', files, ScenarioMapping(layoutpar.ScenarioVector), round(v_MS*3600/1000));
%     save(strcat(folder, filename), 'y_out');
    if files == NumeroExecucions-1
        ySaida((NumeroExecucions-1)*ylen+1:end)=y_out(1:lastChunkLen);
    else
        ySaida((1:ylen)+files*ylen)=y_out; %%suaves%%
    end
end