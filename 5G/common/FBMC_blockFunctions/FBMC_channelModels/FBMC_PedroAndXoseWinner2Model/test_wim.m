clear;
close all;

v_MS = 350; % Velocidad de la MS en km/h
fc = 900e6;
% Definición de los arrays de antenas que utilizarán las estaciones. Estos
% arrays se pueden generar en una fase de preprocesado, ya que es muy
% costosa computacionalmente. La función arrayparset por defecto devuelve 6
% arrays de ejemplo con las siguientes configuraciones:
% - Arrays lineales (ULA) con 2/4/8 antenas.
% - Arrays circulares (UCA) con 4/8 antenas.
if exist('arrays.mat', 'file') == 0
    Arrays = arrayparset;
    save('arrays.mat', 'Arrays');
else
    load('arrays.mat');
end
% Array=AntennaArray('UCA',16,0.05,'FP-ECS',FP);


% Definición de las estaciones. Los números hacen referencia al
% identificador dentro de la estructura Arrays, indicando el array de
% antenas que tendrá cada estación.
% Para definir por ejemplo cinco MS:
% MsAAIdx = [1 1 3 1 2];
MsAAIdx = 1;
% Lo mismo para las BS. En este caso es un cell array, ya que una BS puede
% tener varios sectores.
% Este ejemplo son tres BS, la primera con dos sectores, el primero con el
% array 1 y el segundo con el array 2:
% BsAAIdxCell = {[1 2], [1], [2 4]};
BsAAIdxCell = {4; 4};
% Se inicializa la geometría. El tercer parámetro indica cuantos enlaces
% BS-MS se generarán aleatoriamente.
layoutpar = layoutparset(MsAAIdx, BsAAIdxCell, 1, Arrays);
% figure, NTlayout(layoutpar);
% Escenario activo. Consultar ScenarioMapping.m
layoutpar.ScenarioVector = 19;
% El link activo es entre la segunda BS y la MS
layoutpar.Pairing = [2; 3];
% Configuracion de la primera BS
layoutpar.Stations(1).Pos = [-300; 2; 32];
% Configuracion de la segunda BS
layoutpar.Stations(2).Pos = [300; 2; 32];
% Configuracion de la MS
layoutpar.Stations(3).Pos = [200; 0; 1.5];
layoutpar.Stations(3).Velocity = [v_MS*1000/3600; 0; 0];
% figure, NTlayout(layoutpar);

wimpar = wimparset;
% wimpar.PathLossModelUsed = 'yes';
wimpar.CenterFrequency = fc;
wimpar.NumTimeSamples = 50000;
[H, DELAYS, BULK] = wim(wimpar, layoutpar);
% layoutpar.Stations(3).Pos = layoutpar.Stations(3).Pos + [wimpar.NumTimeSamples*BULK.delta_t*v_MS*1000/3600; 0; 0];

% Este cell array contiene en cada casilla el canal obtenido para cada
% enlace entre BS y MS. En nuestro caso solo hay un enlace.
H1 = H{1};
% El canal es MIMO, por tanto seleccionamos un par de antenas para mostrar
% las gráficas.
H_time = squeeze(H1(1,1,:,:));
if ~strcmp(wimpar.PathLossModelUsed, 'yes')
    % Normalización de energía en caso de que no se use el modelo de
    % pathloss.
    H_time = H_time./mean(sqrt(sum(abs(H_time).^2)));
end
% Este emulador parece que puede sacar varios taps con el mismo delay, 
% además de desordenados. Lo conveniente será sumarlos.
% ii = 1;
% jj = 1;
% [DELAYS_sorted, ind] = sort(DELAYS);
% H_sorted = H_time(ind, :);
% H_purged = zeros(size(H_sorted));
% DELAYS_purged = zeros(size(DELAYS_sorted));
% while ii <= length(DELAYS_sorted)
%     rep = DELAYS_sorted(ii) == DELAYS_sorted;
%     H_purged(jj, :) = sum(H_sorted(rep, :), 1);
%     DELAYS_purged(jj) = DELAYS_sorted(ii);
%     jj = jj+1;
%     ii = ii+sum(rep);
% end
% DELAYS_purged = DELAYS_purged(1:jj-1);
% H_purged = H_purged(1:jj-1, :);

% Eje de tiempo. La tasa de muestreo de canal a la que saca los
% coeficientes este emulador es BULK.delta_t. Este valor lo calcula
% automáticamente mediante: wavelength/(velocidadMS*wimpar.SampleDensity).
% El valor wimpar.SampleDensity por defecto es 2, y el valor de
% wavelength se calcula a partir de wimpar.CenterFrequency, que por defecto
% es 5.2 GHz.
t = 0:BULK.delta_t:BULK.delta_t*(wimpar.NumTimeSamples-1);
% Eje de delay.
tau = 0:5e-9:max(DELAYS)+5e-9;

tag = ScenarioMapping(layoutpar.ScenarioVector);

% figure,
% plot(t, 10*log10(abs(H_time)).');
% grid on;
% xlabel('Tiempo [s]');
% ylabel('Potencia [dB]');
% title('Evolución en tiempo de los taps');
% print('-dpng', strcat('taps_',tag,'.png'));
% 
fs = 15.36e6;
ts = 1/fs;
muestras = (BULK.delays/ts);
maximum_tau = round(1.5*max(BULK.delays)/ts);
instants = (-2:0.1:maximum_tau).';
muestras_rep = muestras(ones(size(instants)), :) - instants(:,ones(size(muestras)));
filtered_h = sinc(muestras_rep)*H_time;
% % filtered_h = sinc((ts_tau(:, ones(size(DELAYS'))) - DELAYS(ones(size(ts_tau)), :))/(4*wimpar.DelaySamplingInterval))*H_time;

ts_tau = instants*ts;
PDP = sum(abs(filtered_h), 2)/wimpar.NumTimeSamples;
taps = zeros(length(PDP));
for ii = 1:length(DELAYS)
    [~, idx] = min(abs(ts_tau - DELAYS(ii)));
    taps(idx) = taps(idx)+ BULK.path_powers(ii);
end
% taps(ts_tau == DELAYS) = BULK.path_powers;
figure,
% plot(ts_tau, 10*log10(abs(PDP)));
plot(ts_tau, abs(PDP));
hold on;
stem(ts_tau, taps, 'r');
grid on;
title('Power Delay Profile');
xlabel('Delay [s]');
ylabel('Power [dB]');
print('-dpng', strcat('pdp_',tag,'.png'))

% H_freq = fft(H_time.');
% dopp = fftshift(sum(abs(H_freq(:, 1:end)), 2));
% ff = linspace(-1/BULK.delta_t/2, 1/BULK.delta_t/2, length(dopp));
% figure,
% plot(ff, 10*log10(dopp./max(dopp)));
% xlabel('Frecuencia [Hz]');
% ylabel('Potencia [dB]');
% title('Espectro Doppler');
% grid on;
% print('-dpng', strcat('doppler_',tag,'.png'))

fd = (v_MS*1000/3600)*fc/2.998e8;
H_time_t = H_time.';
vv = pwelch(H_time_t(:),[],[],200,1/BULK.delta_t);
vv = vv ./ max(vv);
ff = linspace(-1/BULK.delta_t/2, 1/BULK.delta_t/2, 200);
jakes = 1./(pi*fd*sqrt(1-(ff./fd).^2));
jakes(ff > fd) = 10^(-100/10);
jakes(ff < -fd) = 10^(-100/10);
jakes = jakes ./ max(jakes);
figure,
plot(ff, 10*log10(fftshift(vv)));
hold on;
plot(ff, 10*log10(abs(jakes)), 'r--');
xlabel('Frequency [Hz]');
ylabel('Power [dB]');
title('Doppler Spectrum');
grid on;
print('-dpng', strcat('doppler_',tag,'.png'))

% filtered_h = filtered_h(1:ts/wimpar.DelaySamplingInterval:end, :);

% freq_response = fft(filtered_h);
% figure,
% plot(t, 10*log10(abs(freq_response)).');
% grid on;
% xlabel('Frecuencia [Hz]');
% ylabel('Potencia [dB]');
% title('Respuesta en frecuencia de los taps');
% Matriz para pintar cada tap en su delay correspondiente.
% H3D = zeros(length(tau), length(t));
% H3D(round(DELAYS_purged/5e-9)+1, :) = H_purged;


% [tt, ttau] = meshgrid(t, ts_tau(1:ts/wimpar.DelaySamplingInterval:end));
% figure,
% mesh(tt, ttau, abs(filtered_h));
% grid on;
% xlabel('Tiempo [s]');
% ylabel('Delay [s]');
% title('Representación 3D de la respuesta al impulso');


% PDP = diag(H3D*H3D');
% PDP = PDP ./ max(PDP);
% figure,
% plot(tau, PDP);
% % hold on;
% % plot(tau, BULK.path_powers);
% grid on;
% xlabel('Delay [s]');
% ylabel('Potencia [dB]');
% title('PDP del canal generado');
