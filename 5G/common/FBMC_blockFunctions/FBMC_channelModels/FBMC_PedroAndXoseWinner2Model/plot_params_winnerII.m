clear;
close all;

mean_DS = -7.49;
sigma_DS = 0.55;


mean_DS_nat = 10^(mean_DS);
max_DS_nat = 10^(mean_DS+sigma_DS);
min_DS_nat = 10^(mean_DS-sigma_DS);
max2_DS_nat = 10^(mean_DS+2*sigma_DS);
min2_DS_nat = 10^(mean_DS-2*sigma_DS);

tt = linspace(0, max2_DS_nat*3, 1000);

figure;
plot(tt, exp(-tt/mean_DS_nat));
hold on;
grid on;
plot(tt, exp(-tt/max_DS_nat), 'r--');
plot(tt, exp(-tt/max2_DS_nat), 'g--');
plot(tt, exp(-tt/min_DS_nat), 'r--')
plot(tt, exp(-tt/min2_DS_nat), 'g--');
title('PDP of RMa, Hilly, Cutting and Tunnel');
legend('\mu', '\mu +/- \sigma','\mu +/- 2\sigma');
print('-dpng', 'PDP_RMa.png');

%%
mean_DS = -7.12;
sigma_DS = 0.33;

mean_DS_nat = 10^(mean_DS);
max_DS_nat = 10^(mean_DS+sigma_DS);
min_DS_nat = 10^(mean_DS-sigma_DS);
max2_DS_nat = 10^(mean_DS+2*sigma_DS);
min2_DS_nat = 10^(mean_DS-2*sigma_DS);

% tt = linspace(0, max_DS_nat*10, 200);

figure;
plot(tt, exp(-tt/mean_DS_nat));
hold on;
grid on;
plot(tt, exp(-tt/max_DS_nat), 'r--');
plot(tt, exp(-tt/max2_DS_nat), 'g--');
plot(tt, exp(-tt/min_DS_nat), 'r--')
plot(tt, exp(-tt/min2_DS_nat), 'g--');
title('PDP of Viaduct');
legend('\mu', '\mu +/- \sigma', '\mu +/- 2\sigma');
print('-dpng', 'PDP_Viaduct.png');

%%
v = 150;
fc = 2.6e9;
fd = fc*v/2.998e8;
fs = 2*fd;

mean_AS = 1.52;
sigma_AS = 0.24;

mean_AS_nat = pi*10^(mean_AS)/1.4/180;
max_AS_nat = pi*10^(mean_AS+sigma_AS)/1.4/180;
min_AS_nat = pi*10^(mean_AS-sigma_AS)/1.4/180;
max2_AS_nat = pi*10^(mean_AS+2*sigma_AS)/1.4/180;
min2_AS_nat = pi*10^(mean_AS-2*sigma_AS)/1.4/180;

ff = linspace(-pi, pi, 1000);
figure,
plot(180*ff/pi, wrapped_normal(ff, 0, mean_AS_nat));
hold on;
plot(180*ff/pi, wrapped_normal(ff, 0, max_AS_nat), 'r--');
plot(180*ff/pi, wrapped_normal(ff, 0, max2_AS_nat), 'g--');
plot(180*ff/pi, wrapped_normal(ff, 0, min_AS_nat), 'r--');
plot(180*ff/pi, wrapped_normal(ff, 0, min2_AS_nat), 'g--');
legend('\mu', '\mu +/- \sigma', '\mu +/- 2\sigma');
title('Angle of Arrival distribution in RMa, Hilly, Cutting and Tunnel scenarios');
grid on;
xlabel('Degrees (º)');
print('-dpng', 'AoA_RMa.png');

mean_AS = 1.65;
sigma_AS = 0.25;

mean_AS_nat = pi*10^(mean_AS)/1.4/180;
max_AS_nat = pi*10^(mean_AS+sigma_AS)/1.4/180;
min_AS_nat = pi*10^(mean_AS-sigma_AS)/1.4/180;
max2_AS_nat = pi*10^(mean_AS+2*sigma_AS)/1.4/180;
min2_AS_nat = pi*10^(mean_AS-2*sigma_AS)/1.4/180;

ff = linspace(-pi, pi, 1000);
figure,
plot(180*ff/pi, wrapped_normal(ff, 0, mean_AS_nat));
hold on;
plot(180*ff/pi, wrapped_normal(ff, 0, max_AS_nat), 'r--');
plot(180*ff/pi, wrapped_normal(ff, 0, max2_AS_nat), 'g--');
plot(180*ff/pi, wrapped_normal(ff, 0, min_AS_nat), 'r--');
plot(180*ff/pi, wrapped_normal(ff, 0, min2_AS_nat), 'g--');
legend('\mu', '\mu +/- \sigma', '\mu +/- 2\sigma');
title('Angle of Arrival distribution in Viaduct scenarios');
grid on;
xlabel('Degrees (º)');
print('-dpng', 'AoA_Viaduct.png');

% ff = linspace(-fs, fs, 200);
% jakes = 1./(pi*fd*sqrt(1-(ff./fd).^2));
% jakes(ff > fd) = 0;
% jakes(ff < -fd) = 0;
% jakes = jakes ./ max(jakes);
% figure,
% plot(ff, jakes);
% grid on;

