clear;
close all;


h = rayleighchan(1e-4, 100, [0 0.55e-4 1.1e-4], [0 0 0]);

h.StoreHistory = 1;          % Allow states to be stored
y = filter(h, ones(1000,1));  % Process samples through channel
plot(h);
