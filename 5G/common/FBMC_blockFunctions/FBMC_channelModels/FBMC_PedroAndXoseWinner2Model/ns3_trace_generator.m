clear;
close all;

v_MS = 350; % Velocidad de la MS en km/h

% Frecuencia de muestreo en banda base.
fs = 15.36e6;
% fc = 5.25e9;
fc = 2.5e9;
ts = 1/fs;
TTI = 0.001;
% traceDuration = 25.0; % en segundos
traceDuration = 274.0;
% To avoid large consumption of memory, traces of channel are generated in
% subtraces.
subTraceDuration = 25.0;
% Número de Resource Blocks LTE.
numRBs = 100;
lambda = 2.998e8/fc;
sample_density = 4;
v_MS = v_MS*1000/3600;
ts_channel = lambda/(2*sample_density*v_MS);

% Definición de los arrays de antenas que utilizarán las estaciones. Estos
% arrays se pueden generar en una fase de preprocesado, ya que es muy
% costosa computacionalmente. La función arrayparset por defecto devuelve 6
% arrays de ejemplo con las siguientes configuraciones:
% - Arrays lineales (ULA) con 2/4/8 antenas.
% - Arrays circulares (UCA) con 4/8 antenas.
if exist('arrays.mat', 'file') == 0
    Arrays = arrayparset;
    save('arrays.mat', 'Arrays');
else
    load('arrays.mat');
end
% Array=AntennaArray('UCA',16,0.05,'FP-ECS',FP);
%%
% Definición de las estaciones. Los números hacen referencia al
% identificador dentro de la estructura Arrays, indicando el array de
% antenas que tendrá cada estación.
% Para definir por ejemplo cinco MS:
% MsAAIdx = [1 1 3 1 2];
MsAAIdx = 1;
% Lo mismo para las BS. En este caso es un cell array, ya que una BS puede
% tener varios sectores.
% Este ejemplo son tres BS, la primera con dos sectores, el primero con el
% array 1 y el segundo con el array 2:
% BsAAIdxCell = {[1 2], [1], [2 4]};
BsAAIdxCell = {4; 4};
% Se inicializa la geometría. El tercer parámetro indica cuantos enlaces
% BS-MS se generarán aleatoriamente.
layoutpar = layoutparset(MsAAIdx, BsAAIdxCell, 1, Arrays);
% figure, NTlayout(layoutpar);
% Escenario activo. Consultar ScenarioMapping.m
layoutpar.ScenarioVector = 21;
% El link activo es entre la segunda BS y la MS
layoutpar.Pairing = [2; 3];
% Configuracion de la primera BS
layoutpar.Stations(1).Pos = [-150; 2; 32];
% Configuracion de la segunda BS
layoutpar.Stations(2).Pos = [150; -2; 32];
% Configuracion de la MS
layoutpar.Stations(3).Pos = [20; 0; 1.5];
layoutpar.Stations(3).Velocity = [v_MS; 0; 0];
% figure, NTlayout(layoutpar);

wimpar = wimparset;
% wimpar.PathLossModelUsed = 'yes';
wimpar.CenterFrequency = fc;
% wimpar.NumTimeSamples = round(traceDuration/ts_channel);
wimpar.NumTimeSamples = round(subTraceDuration/ts_channel);
% wimpar.SampleDensity = 4;
[H, DELAYS, BULK] = wim(wimpar, layoutpar);
%%
% Este cell array contiene en cada casilla el canal obtenido para cada
% enlace entre BS y MS. En nuestro caso solo hay un enlace.
H1 = H{1};
% El canal es MIMO, por tanto seleccionamos un par de antenas para mostrar
% las gráficas.
H_time = squeeze(H1(1,1,:,:));
if ~strcmp(wimpar.PathLossModelUsed, 'yes')
    % Normalización de energía en caso de que no se use el modelo de
    % pathloss.
    H_time = H_time./mean(sqrt(sum(abs(H_time).^2)));
end
% Segun la duración total de la traza y el espaciado entre tramas, se
% calcula el número de tramas que caben en la traza.
num_frames = round(traceDuration/TTI);
% Según el número de tramas que hay en la traza, se elige un espaciado
% dentro del canal para sacar suficientes muestras.
frame_spacing = floor(size(H_time, 2)/round(subTraceDuration/TTI));
current_channel = 1;

numSamples = TTI / ts;
sig = zeros(numSamples, 1); % Signal
sig(1) = 1; % dirac impulse

[psdsig,F] = pwelch(sig,[],[],numRBs,fs,'twosided');
ppssdd = zeros(numRBs, num_frames);
% channel_spread = max(DELAYS)/wimpar.DelaySamplingInterval;
muestras = (BULK.delays/ts);
maximum_tau = round(2*max(BULK.delays)/ts);
instants = (-4:maximum_tau).';
muestras_rep = muestras(ones(size(instants)), :) - instants(:,ones(size(muestras)));
for ii=1:num_frames
    if current_channel > length(H_time)
        [H, DELAYS, BULK] = wim(wimpar, layoutpar, BULK);
        H1 = H{1};
        H_time = squeeze(H1(1,1,:,:));
        if ~strcmp(wimpar.PathLossModelUsed, 'yes')
            H_time = H_time./mean(sqrt(sum(abs(H_time).^2)));
        end
        current_channel = 1;
        fprintf('Recalculando nuevos coeficientes (trama %d/%d) ...\n', ii, num_frames);
    end
    H_current = H_time(:, current_channel);
    % Se procesa la respuesta al impulso para obtener una versión
    % interpolada mediante una convolucion con una sinc. Así se representa
    % mejor el que los taps estén en posiciones que son fracciones de la
    % tasa de muestreo de la señal a procesar.
%     ts_tau = (-40*wimpar.DelaySamplingInterval:wimpar.DelaySamplingInterval:2*max(DELAYS))';
%     filtered_h = sinc((ts_tau(:, ones(size(DELAYS'))) - DELAYS(ones(size(ts_tau)), :))/(4*wimpar.DelaySamplingInterval))*H_current;

    filtered_h = sinc(muestras_rep)*H_current;
    
    % La tasa de muestreo debe ser la de la propia señal.
%     filtered_h = filtered_h(1:ts/wimpar.DelaySamplingInterval:end);
    y = filter(filtered_h, 1, sig);
    
    [psdy,F] = pwelch(y,[],[],numRBs,fs);      

    ppssdd(:,ii) = psdy ./ psdsig;
    current_channel = current_channel + frame_spacing;
end

%% FILE GENERATION
tag = sprintf('%s_%dkmh', ScenarioMapping(layoutpar.ScenarioVector), round(v_MS*3600/1000));
if strcmp(wimpar.PathLossModelUsed, 'yes')
    tag = strcat(tag, '_PL');
end

file = fopen(strcat('fading_trace_',tag,'.fad'),'wt');

for j=1:numRBs
    for s=1:num_frames
        fprintf(file, '%g ',10.*log10(ppssdd(j, s)));

    end
    fprintf(file, '\n');
end

fclose(file);

