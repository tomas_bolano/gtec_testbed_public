function [FBMC_outSignal, FBMC_additionalData] = FBMC_channelFunction(FBMC_parameters,...
        FBMC_pregeneratedConstants, FBMC_inSignal, FBMC_additionalInputData)
%FBMC_CHANNELFUNCTION Wrapper to apply the defined channels in FBMC_parameters.
%

if nargin < 4
    FBMC_additionalInputData = [];
end

FBMC_outSignal = FBMC_inSignal;
channels = FBMC_parameters.channelModel.channelGenerationFunctions;

if isempty(channels)
    FBMC_additionalData = FBMC_additionalInputData;
    return;
end

if ~iscell(channels)
    channels = {channels};
end

for i = 1:length(channels)
    [FBMC_outSignal, FBMC_additionalData] =...
        channels{i}(FBMC_parameters, FBMC_pregeneratedConstants,...
                    FBMC_outSignal, FBMC_additionalInputData);
end

end

