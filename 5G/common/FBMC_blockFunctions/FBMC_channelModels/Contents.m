% FBMC_CHANNELMODELS Channel models functions
%
% This directory includes functions of channel models for the transmitted
% signals.
%
%
% Interface of the synchronization functions 
% ------------------------------------------
%
% All the channel models functions have the same interface, which is:
%
% [FBMC_rxSignal, FBMC_channelData] = FBMC_channelModel(FBMC_parameters,...
%       FBMC_pregeneratedConstants, FBMC_txSignal, FBMC_channelData)
%
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_pregeneratedConstants
%       FBMC pregenerated Constants
%
%   FBMC_txSignal
%       Signal to be processed.
%
%   FBMC_channelData
%       Some channels may accept some input data which can be passed using
%       this parameter.
%
% Output parameters:
%
%   FBMC_rxSignal
%       Signal after passing through the channel.
%
%   FBMC_channelData
%       Input parameter FBMC_channelData updated (id needed) that can be
%       used for another invocation of the function.
%

