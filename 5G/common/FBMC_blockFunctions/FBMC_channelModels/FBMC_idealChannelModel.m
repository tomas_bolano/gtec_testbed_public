function [FBMC_outSignal, FBMC_additionalData] = FBMC_idealChannelModel(~, ~, FBMC_inSignal, ~)
%FBMC_IDEALCHANNELMODEL Ideal channel model

FBMC_additionalData = [];
FBMC_outSignal = FBMC_inSignal;

end

