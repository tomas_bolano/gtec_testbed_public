function channelstr = FBMC_channel2str(FBMC_parameters)
%FBMC_CHANNEL2STR Get a string identifier of the channel

iscellmember = @(A,B) any(cellfun(@(x) isequal(x,A), B));

channelFunctions = FBMC_parameters.channelModel.channelGenerationFunctions;

if iscellmember(@FBMC_stdchanChannelModel, channelFunctions)
    channelstr = FBMC_parameters.channelModel.stdchanChannelModel.Type;
elseif iscellmember(@FBMC_winner2PedroAndXoseChannelModel, channelFunctions)
    channelstr = 'winner2';
else
    channelstr = 'AWGN';
end

end

