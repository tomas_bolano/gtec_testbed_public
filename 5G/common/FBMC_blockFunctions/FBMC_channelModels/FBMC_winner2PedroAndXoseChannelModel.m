function [FBMC_outSignal, FBMC_additionalData] = FBMC_winner2PedroAndXoseChannelModel(FBMC_parameters,...
    ~, FBMC_inSignal, FBMC_additionalInputData)
%FBMC_WINNER2PEDROANDXOSECHANNELMODEL Channel model using the MATLAB stdchan function


%% Parameters extraction

xvInt = FBMC_parameters.highSpeedEmulation.interpolationFactor;
Vel = FBMC_parameters.channelModel.relativeSpeed;
channelModelIndex = FBMC_parameters.channelModel.winner2PedroAndXoseChannelModel.Index;
fs =  1/FBMC_parameters.signalGeneration.dt;
fc = FBMC_parameters.channelModel.carrierFrequency;


%% Apply channel

% The additional data for this channel is the BULK data
BULKe = FBMC_additionalInputData;

[FBMC_outSignal,FBMC_additionalData] = ...
    simulations_channel_filter(FBMC_inSignal,xvInt,Vel,BULKe,channelModelIndex,fs,fc);


end

