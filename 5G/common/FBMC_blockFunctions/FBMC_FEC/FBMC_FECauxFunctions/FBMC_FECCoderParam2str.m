function str = FBMC_FECCoderParam2str(FBMC_FECparamCell)
%FBMC_FECCODERPARAM2STR Get a description of the FEC coder
%
% FBMC_FECparamCell is a cell with the FEC function to use and the
% parameters to pass to it as specified in the parameters files.
% FBMC_FECparamCell has the format {function, param1, ... paramN}
% where the function must be a FEC coding function.

fecstr = func2str(FBMC_FECparamCell{1});

switch fecstr
    case 'FBMC_MATLABLTEFECCoder'
        str = 'LTE turbo code';
    case 'FBMC_ConvFECCoder'
        str = sprintf('Convolutional code, rate %s', FBMC_FECparamCell{2});
    case 'FBMC_nullFECCoder'
        str = 'none';
    otherwise
        str = fecstr;
end

end
