function npad = FBMC_802_11nPadding(nbits, ratestr)
% FBMC_802_11NPADDING Calculates the number of padding bits for anumber of bits.

% Obtain pucturing pattern
% The puncturing pattern will be a 2xN matrix
% The output of the base convolutional coder will be 2 sequences of bits of
% the same lenght of the input sequence of bits.
punctMat = FBMC_802_11ConvCoderPunctMatrix(ratestr);

% Calculate padding needed
npad = ceil(nbits/size(punctMat,2))*size(punctMat,2) - nbits;

end
