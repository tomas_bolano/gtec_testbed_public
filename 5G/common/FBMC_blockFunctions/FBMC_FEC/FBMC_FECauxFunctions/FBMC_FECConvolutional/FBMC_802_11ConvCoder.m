function y = FBMC_802_11ConvCoder(data, ratestr)
% FBMC_802_11ConvCoder Convolutional encoder of the IEEE 802.11 standard

% generator polynomials
g0 = [1 0 1 1 0 1 1];
g1 = [1 1 1 1 0 0 1];

y = mod(conv2(data.', [g0; g1]),2);
y = y(:, 1:length(data));

if strcmp(ratestr,'1/2')
    y = reshape(y, [], 1);
    return;
end

punctMat = FBMC_802_11ConvCoderPunctMatrix(ratestr);

if mod(size(y,2)/size(punctMat,2),1) ~= 0
    error('Data size is not multiple of the puncturing pattern size');
end

punctFullMat = repmat(punctMat, 1, ceil(size(y,2)/size(punctMat,2)));
y = y(punctFullMat(:,1:size(y,2)));

end

