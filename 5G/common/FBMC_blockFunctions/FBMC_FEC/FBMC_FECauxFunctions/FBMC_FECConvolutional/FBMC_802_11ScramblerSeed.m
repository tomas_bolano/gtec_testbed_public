function state = FBMC_802_11ScramblerSeed(data)
% FBMC_802_11SCRAMBLERSEED Recovers the scrambler seed from the data

state = data;
%assert(all(size(state) == [1 7]));

for i = 1:7
    state = [xor(state(end),state(3)); state(1:end-1)];
end

end

