function punctmat = FBMC_802_11ConvCoderPunctMatrix(ratestr)
%FBMC_802_11CONVCODERPUNCTMATRIX Obtain the pucturing pattern as defined by the IEEE 802.11 Standard

switch ratestr
    case '1/2'
        punctmat = true;
    case '2/3'
        punctmat = logical([1 1; 1 0]);
    case '3/4'
        punctmat = logical([1 1 0; 1 0 1]);
    case '5/6'
        punctmat = logical([1 1 0 1 0; 1 0 1 0 1]);
    otherwise
        error('Invalid rate specified.');
end

end

