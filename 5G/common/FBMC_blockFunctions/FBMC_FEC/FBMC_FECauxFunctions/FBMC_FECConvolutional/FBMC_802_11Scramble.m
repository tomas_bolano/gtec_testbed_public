function y = FBMC_802_11Scramble(data, init)
%FBMC_802_11Scramble Scramble bits as defined by the IEEE 802.11 standard

state = init;
%assert(all(size(state) == [7 1]));

scrambled = zeros(numel(data),1);
for i = 1:numel(data)
    xor1 = xor(state(1),state(4));
    scrambled(i) = xor(xor1, data(i));
    state = [state(2:end); xor1];
end

y = scrambled;                                                               
                                                                                
end

