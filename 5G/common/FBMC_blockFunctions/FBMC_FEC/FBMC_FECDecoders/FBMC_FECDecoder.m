function FBMC_rxDecodedData = FBMC_FECDecoder(FBMC_parameters,...
    FBMC_constants, FBMC_dataBlockNumber, FBMC_rxCodedBits)
%FBMC_FECDECODER Wrapper to call the fec decoding function defined in FBMC_parameters.
%

dataBlockParam = FBMC_parameters.dataBlock(FBMC_dataBlockNumber);

FBMC_rxDecodedData = FBMC_execCell(dataBlockParam.FECDecoderFunction,...
    {FBMC_parameters, FBMC_constants, FBMC_dataBlockNumber, FBMC_rxCodedBits});

end
