function FBMC_rxDecodedData = FBMC_nullFECDecoder(~, FBMC_constants,...
    FBMC_dataBlockIndex, FBMC_rxCodedBits)
%FBMC_NULLFECDECODER Do not decode.

numBits = FBMC_constants.dataBlockInputUncodedBitsNumber(FBMC_dataBlockIndex);

FBMC_rxDecodedData.decoded = 0;
FBMC_rxDecodedData.error = 0;
FBMC_rxDecodedData.errordata = [];
FBMC_rxDecodedData.criticalError = 0;
FBMC_rxDecodedData.criticalErrorData = [];
FBMC_rxDecodedData.criticalErrorMsg = '';
FBMC_rxDecodedData.bits = FBMC_rxCodedBits(1:numBits) > 0; 
FBMC_rxDecodedData.additionalData = [];

end

