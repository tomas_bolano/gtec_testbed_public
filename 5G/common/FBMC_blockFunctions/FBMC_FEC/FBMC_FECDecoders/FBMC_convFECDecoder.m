function FBMC_txDecodedData = FBMC_convFECDecoder(~, FBMC_constants,...
    FBMC_dataBlockNumber, FBMC_rxSoftBits, ratestr, scrSeed, deintlvr,...
    deintlvrConf, tblen, dectype)
%FBMC_CONVFECCODER Convolutional FEC Decoder
%
% Encodes the data using a convolutional encoding based on the one used by
% the IEEE 802.11 WI-FI standard.
%
% Specific input Parameters:
%   ratestr
%       String indicating the rate of the coding, available values:
%       '1/2', '2/3', '3/4', and '5/6'.
%   scrSeed
%       Scrambler seed. It must be an empty vector or a binary vector of 7
%       elements. If scrSeed is empty then the seed will be determined from
%       the received data.
%   deintlvr
%       Deinterleaver use, current available deinterleavers are:
%       * 'rnd': random deinterleaver using the randdeintrlv.
%       * 'none': no deinterleaver
%   deintlvrConf
%       Cell of the required parameters for the deinterleaver function
%       selected. See the documentation of each function for details.
%   tblen
%       Integer that specifies the traceback length of the viterbi decoder.
%       If the code rate is 1/2, a typical value for is about five times
%       the constraint length of the code. If not specified a value of 40
%       is used.
%   dectype
%       Type of decision to use for the decoder. Available types are:
%       * 'soft': use the soft bits values as received (default).
%       * 'hard': use hard bits (0 or 1).
%

if nargin < 9
    tblen = 40;
end

if nargin < 10
    dectype = 'soft';
end

% Number of uncoded bits
FBMC_nDataUncodedBits =  FBMC_constants.dataBlockInputUncodedBitsNumber(FBMC_dataBlockNumber);

% Number of coded bits
FBMC_nDataCodedBits =  FBMC_constants.dataBlockBitsNumber(FBMC_dataBlockNumber);

% Calculate number of padding bits
npad = FBMC_802_11nPadding(16+FBMC_nDataUncodedBits, ratestr);

% Extract the coded frame data from the sequence received
FBMC_codedSoftBits = FBMC_rxSoftBits(1:ceil((16+FBMC_nDataUncodedBits)/str2num(ratestr))+npad);

% Deinterleave data
switch deintlvr
    case 'rnd'
        deinterleavedBits = randdeintrlv(FBMC_codedSoftBits, deintlvrConf{:});
    case 'none'
        deinterleavedBits = FBMC_codedSoftBits;
    otherwise
        error('Interleaver not supported.');
end


% Decode data
trellis = poly2trellis(7,[133 171]);

if strcmpi(dectype, 'soft')
    if strcmp(ratestr,'1/2')
        decodedBits = vitdec(-deinterleavedBits, trellis, tblen, 'trunc' , 'unquant');
    else
        punctmat = FBMC_802_11ConvCoderPunctMatrix(ratestr);
        decodedBits = vitdec(-deinterleavedBits, trellis, tblen, 'trunc', 'unquant',...
                             reshape(punctmat,1,[]));
    end
elseif strcmpi(dectype, 'hard')
    hardBits(deinterleavedBits >= 0) = 1;
    hardBits(deinterleavedBits < 0) = 0;
    hardBits = hardBits.';
    if strcmp(ratestr,'1/2')
            decodedBits = vitdec(hardBits, trellis, tblen, 'trunc' , 'hard');
    else
        punctmat = FBMC_802_11ConvCoderPunctMatrix(ratestr);
        decodedBits = vitdec(hardBits, trellis, tblen, 'trunc', 'hard',...
                             reshape(punctmat,1,[]));
    end
else
    error('Invalid dectype value');
end    
    

% Descramble data
if isempty(scrSeed)
    scrSeed = FBMC_802_11ScramblerSeed(decodedBits(1:7));
else
    assert(numel(scrSeed) == 7, 'scrSeed must be a vector of 7 elements');
end
scrSeed = reshape(scrSeed, 7, 1);


% For descrambling the scrambler itself is executed againç
descrambledData = FBMC_802_11Scramble(decodedBits, scrSeed);


% Remove padding from end and service field from start (16 bits)
depaddedData = descrambledData(16+1:end-npad);


% Assign outputs
FBMC_txDecodedData.decoded = 1;
FBMC_txDecodedData.error = [];
FBMC_txDecodedData.errordata = [];
FBMC_txDecodedData.criticalError = 0;
FBMC_txDecodedData.criticalErrorData = [];
FBMC_txDecodedData.criticalErrorMsg = '';
FBMC_txDecodedData.errordata = [];
FBMC_txDecodedData.bits = depaddedData;
FBMC_txDecodedData.additionalData = [];

end
