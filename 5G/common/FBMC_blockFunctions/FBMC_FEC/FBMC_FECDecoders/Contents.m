% FBMC_FECDECODERS FEC Decoding functions
%
% This directory includes the correcponding fec decoders for the fec
% coders defined in the directory FBMC_fecCoders.
% See each file for further details.
%
% Interface of the FEC decoding functions 
% ---------------------------------------
%
% All the FEC decoding functions have the same interface, which is:
%
% FBMC_rxDecodedData = FBMC_fecDecoding(FBMC_parameters,...
%       FBMC_constants, FBMC_dataBlockIndex, FBMC_rxSoftBits,...
%       param1, ..., paramN)
%
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_constants
%       FBMC pregenerated Constants.
%
%   FBMC_dataBlockIndex
%       Index of the data block to decode
%
%   FBMC_txSoftBits
%       Coded soft bits to decode.
%
%   param1, ..., paramN
%       Aditional specific parameters for each function.
%
%
% Output parameters: 
%   FBMC_DecodedData
%       Structure of the decoded data containing the following fields:
%           * decoded
%               Boolean flag that indicates if the data was decoded.
%               If false then the bits returned are the same input bits.
%               If true then the data was decoded. This will be false when
%               using the FBMC_nullFecDecoder function.
%           * error
%               Boolean flag that indicates if some error happened during
%               decoding. May be empty if the decoding function can not
%               detect if an error ocurred.
%           * errordata
%               If the error field is 1 this field will contain some
%               specific information about the error ocurred. If the error
%               field is 0 then the contents are undefined.
%           * criticalError
%               Boolean flag that indicates if some critial error happened
%               during decoding and the data could not be decoded. If true
%               then the input bits could not be decoded and the bits field
%               will be empty. If false then no error happened.
%           * criticalErrorData
%               If the criticalError field is 1 this field will contain some
%               specific information about the critical error ocurred. If
%               the error field is 0 then the contents are undefined.
%           * criticalErrorMsg
%               String description of the critical error.
%           * bits
%               Vector of the decoded bits. If the error flas is 1 then
%               this field will be empty. If decoded is 1 then this field
%               will contain the decoded bitd, else will contain the input
%               bits.
%           * additionalData
%               Any other data that the function could return. The contents
%               of this field will depend on the particular decoding
%               function.
%
