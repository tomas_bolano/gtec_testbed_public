function FBMC_txDecodedData = FBMC_MATLABLTEFECDecoder(~, FBMC_constants,...
    FBMC_dataBlockNumber, FBMC_txSoftBits, nTurboIt)
%FBMC_MATLABLTEFECCODER LTE Turbo decoder FEC usgin MATLAB's LTE toolbox functions
%
% Decodes the data encoded by the LTE FEC coder.
%
% Specific input Parameters:
%   nTurboIt
%       Number of iterations to use for the turbo decoder. Default: 5


if nargin < 5
    nTurboIt = 5;
end

transportBlkSize = FBMC_constants.dataBlockInputUncodedBitsNumber(FBMC_dataBlockNumber);

rv = 0;

DLSCHInfo = lteDLSCHInfo(transportBlkSize);
rxCW = FBMC_txSoftBits;

rxRateMatched = lteRateRecoverTurbo(rxCW, transportBlkSize, rv);
rxDecoded = lteTurboDecode(rxRateMatched, nTurboIt);
[rxDesegmented, errCodeBlock] = lteCodeBlockDesegment(rxDecoded);
[rxCRCDecoded, errCRC] = lteCRCDecode(rxDesegmented(DLSCHInfo.F+1:end), '24A');


% Assign outputs
FBMC_txDecodedData.decoded = 1;
FBMC_txDecodedData.error = (errCRC ~= 0) || any(errCodeBlock);
FBMC_txDecodedData.errordata.errCRC = errCRC;
FBMC_txDecodedData.errordata.errCodeBlock = errCodeBlock;
FBMC_txDecodedData.criticalError = 0;
FBMC_txDecodedData.criticalErrorData = [];
FBMC_txDecodedData.criticalErrorMsg = '';
FBMC_txDecodedData.bits = rxCRCDecoded;
FBMC_txDecodedData.additionalData = [];


end

