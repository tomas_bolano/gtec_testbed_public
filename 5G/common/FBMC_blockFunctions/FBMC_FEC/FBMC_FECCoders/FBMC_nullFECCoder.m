function FBMC_txCodedData = FBMC_nullFECCoder(~, ~, ~, FBMC_txUncodedBits)
%FBMC_NULLFECCODER Do not code.

FBMC_txCodedData.coded = false;
FBMC_txCodedData.bits = FBMC_txUncodedBits;
FBMC_txCodedData.additionalData = [];

end

