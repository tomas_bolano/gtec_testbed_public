function FBMC_txCodedData = FBMC_FECCoder(FBMC_parameters,...
    FBMC_constants, FBMC_dataBlockNumber, FBMC_txUncodedBits)
%FBMC_FECCODER Wrapper to call the fec coding function defined in FBMC_parameters.
%

dataBlockParam = FBMC_parameters.dataBlock(FBMC_dataBlockNumber);

FBMC_txCodedData = FBMC_execCell(dataBlockParam.FECCoderFunction,...
                    {FBMC_parameters, FBMC_constants, FBMC_dataBlockNumber,...
                     FBMC_txUncodedBits});

end
