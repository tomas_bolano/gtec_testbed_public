% FBMC_FECCODERS FEC Coding functions
%
% This directory includes functions used to apply fec codes to tx bits.
% See each file for further details.
%
% Interface of the FEC coding functions 
% --------------------------------------
%
% All the FEC coding functions have the same interface, which is:
%
% FBMC_txCodedData = FBMC_fecCoding(FBMC_parameters,...
%       FBMC_constants, FBMC_dataBlockIndex, FBMC_txUncodedBits,...
%       param1, ..., paramN)
%
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_constants
%       FBMC pregenerated Constants.
%
%   FBMC_dataBlockIndex
%       Index of the data block to encode
%
%   FBMC_txUncodedBits
%       Uncoded bits to apply the fec.
%
%   param1, ..., paramN
%       Aditional specific parameters for each function.
%
%
% Output parameters: 
%   FBMC_codedData
%       Structure containing the following fields:
%       	* coded
%               Boolean flag that indicates if coding was aplied to
%               the input data. If false then the input bits were not
%               coded. If true the coded bits are returned in the
%               codedBits field.
%
%           * bits
%               Vector of the coded bits if the coded flag is set to 1. If
%               the coded flag is set to 0 then this vector will be the
%               FBMC_txUncodedBits input vector.
%
%           * additionalData
%               Any other data that the function could return. The contents
%               of this field will depend on the particular coding function.
