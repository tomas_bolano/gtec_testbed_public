function FBMC_txCodedData = FBMC_MATLABLTEFECCoder(~,...
    FBMC_constants, FBMC_dataBlockNumber, FBMC_txUncodedBits)
%FBMC_MATLABLTEFECCODER LTE Turbo coder FEC usgin MATLAB's LTE toolbox functions
%
% Encodes the data using the LTE FEC procedure defined in 3GPP TS 36.212

rv = 0;

codedTransBlkSize = FBMC_constants.dataBlockBitsNumber(FBMC_dataBlockNumber);

dlschBits = FBMC_txUncodedBits;
dlschBitsCRC = lteCRCEncode(dlschBits, '24A');
dlschBitsSegmented = lteCodeBlockSegment(dlschBitsCRC);
dlschBitsCoded = lteTurboEncode(dlschBitsSegmented);    
dlschBitsRateMatched = lteRateMatchTurbo(dlschBitsCoded, codedTransBlkSize, rv);

FBMC_txCodedData.coded = 1;
FBMC_txCodedData.bits = dlschBitsRateMatched;
FBMC_txCodedData.additionalData = [];

end

