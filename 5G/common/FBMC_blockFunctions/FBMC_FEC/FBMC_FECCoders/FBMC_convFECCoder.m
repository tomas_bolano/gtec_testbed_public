function FBMC_txCodedData = FBMC_convFECCoder(~, ~,...
    FBMC_dataBlockIndex, FBMC_txUncodedBits,...
    ratestr, scrSeed, intlvr, intlvrConf)
%FBMC_CONVFECCODER Convolutional FEC Coder
%
% Encodes the data using a convolutional encoding based on the one used by
% the IEEE 802.11 WI-FI standard. The length of the encoded bit sequency
% will be 16 + (N+npad)*rate, where N is the length of the input sequence,
% npad is the number of padding bits and rate is the coding rate specified
% by the ratestr parameter.
%
% Specific input Parameters:
%   ratestr
%       String indicating the rate of the coding, available values:
%       '1/2', '2/3', '3/4', and '5/6'. For the cases '2/3', '3/4'
%       and '5/6' a puncturing procedure is used and a padding may be
%       required to adapt the data lenght to the required rate.
%   scrSeed
%       Scrambler seed. It must be an empty vector or a binary vector of 7
%       elements. If scrSeed is empty then a random seed will be generated.
%   intlvr
%       Interleaver to use, current available interleavers are:
%       * 'rnd': random interleaver using the randintrlv.
%       * 'none': no interleaver
%   intlvrConf
%       Cell of the required parameters for the interleaver function
%       selected. See the documentation of each function for details.
%


if isempty(scrSeed)
    scrSeed = randi([0,1], 1, 7);
else
    assert(numel(scrSeed) == 7, 'scrSeed must be a vector of 7 elements');
end

scrSeed = reshape(scrSeed, 7, 1);

% Initial data
% This data will have the following format:
%
% +---------+--------------+---- +
% | SERVICE |     DATA     | PAD |
% +---------+--------------+-----+
%
% Where the service field is as defined in the 802.11 standard, and the pad
% should be added to adap the sequence to the punctured used in the encoder.
npad = FBMC_802_11nPadding(16+numel(FBMC_txUncodedBits), ratestr);
assert(npad >= 0);
paddedData = [zeros(16, 1); FBMC_txUncodedBits; zeros(npad, 1)];

% Scramble data
scrambledData = FBMC_802_11Scramble(paddedData, scrSeed);

% Encode data
codedData = FBMC_802_11ConvCoder(scrambledData, ratestr);

% Interleave data
switch intlvr
    case 'rnd'
        interleavedData = randintrlv(codedData, intlvrConf{:});
    case 'none'
        interleavedData = codedData;
    otherwise
        error('Interleaver not supported.');
end

FBMC_txCodedData.coded = 1;
FBMC_txCodedData.bits = interleavedData;
FBMC_txCodedData.additionalData = [];

end
