function coderstr = FBMC_FECCoder2String(FBMC_parameters)
%FBMC_FECCODER2STRING Get a string identifier of the coder

FECCoder = FBMC_parameters.data.txFECCoderFunction{1};

if isequal(FECCoder, @FBMC_nullFecCoder)
    coderstr = 'none';
elseif isequal(FECCoder, @FBMC_convFECCoder)
    coderstr = 'Convolutional code';
else
    coderstr = func2str(FECCoder);
end

end

