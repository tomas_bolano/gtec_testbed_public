% FBMC_PILOTINDEXERS Pilot indexing functions
%
% This directory includes functions used to generate a grid of pilots. Each
% function will create a different pilot pattern. 
% See each file for further details.
%
% Interface of the pilot indexing functions 
% -----------------------------------------
%
% All the pilot indexing functions have the same interface, which is:
%
% pilotData = FBMC_pilotIndexer(FBMC_parameters, param1, ..., paramN)
%
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   param1, ..., paramN
%       Aditional specific parameters for each function.
%
%
% Output parameters: 
%   pilotData
%       Structure containing all the data for the pilot pattern generated.
%       Contains the following fields:
%
%       * NonZeroPilotMask
%           Logical NxM matrix, where N is the number of subcarriers and M the
%           number of time symbols as specified in FBMC_parameters. Mask of non
%           zero values to insert with pilots. The elements set to one indicate
%           the frequency-time elements where the non zero pilots will be
%           placed.
%
%        * NonZeroPilotValues
%           Vector of non zero pilot values that will be inserted in the data
%           grid on the positions indicated by NonZeroPilotMask.
%
%       * ActualPilotMask
%           Logical NxM matrix, where N is the number of subcarriers and M the
%           number of time symbols as specified in FBMC_parameters. Mask of the
%           pilots. The elements set to one indicate the frequency-time
%           elements where the non zero pilots will be placed.
%
%       * ActualPilotValues
%           Vector of non zero pilot values that will be inserted in the data
%           grid on the positions indicated by NonZeroPilotMask.
%
%       * ActualPilotXGrid
%           Pilots time domain indexes matrix in meshgrid format, or an empty
%           matrix if the pilots can't be represented in a meshgrid format.
%
%       * ActualPilotYGrid
%           Pilots frequency domain indexes matrix in meshgrid format, or an
%           empty matrix if the pilots can't be represented in a meshgrid
%           format.
%
%       * ZeroPilotMask
%           Logical NxM matrix, where N is the number of subcarriers and M the
%           number of time symbols as specified in FBMC_parameters. Mask of
%           zero elements. The elements set to one indicate the frequency-time
%           elements which will be left zero.
%

