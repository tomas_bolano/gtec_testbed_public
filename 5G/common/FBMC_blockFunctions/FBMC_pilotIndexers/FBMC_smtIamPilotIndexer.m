function pilotData = FBMC_smtIamPilotIndexer(FBMC_parameters)
%FBMC_SMTIAMPILOTINDEXER Generate a grid of pilots for the IAM method for SMT.
%
% See also FBMC_pilotIndexers

% TODO completar este c�digo

%% Parameters extraction

Snum = FBMC_parameters.basicParameters.subcarriersNumber;
Dnum = FBMC_parameters.basicParameters.timeSymbolsNumber;

%% Initializations

Snump = Snum;
if((FBMC_parameters.pilots.removePilotsFromGuards==1)&&(~isempty(FBMC_parameters.frameGeneration.sideGuardSubcarriers)))
    Snump = Snum - sum(FBMC_parameters.frameGeneration.sideGuardSubcarriers);
end
FreqSpacing = FBMC_parameters.pilots.freqSpacing;
TimeSpacing = FBMC_parameters.pilots.timeSpacing;

%% Calculations

FreqBlocks = floor((Snump-1)/(FreqSpacing-1))+1;
TotalFreqWidth = (FreqBlocks-1)*(FreqSpacing-1) + 3;
if(TotalFreqWidth>Snump)
    FreqBlocks = FreqBlocks - 1;
    TotalFreqWidth = (FreqBlocks-1)*(FreqSpacing-1) + 3;
end
SidePadding = floor((Snump - TotalFreqWidth)/2);

FreqIndexesNeighboors = SidePadding + 2 +(0:(FreqBlocks-1))*(FreqSpacing-1);
if((FBMC_parameters.pilots.removePilotsFromGuards==1)&&(~isempty(FBMC_parameters.frameGeneration.sideGuardSubcarriers)))
    FreqIndexesNeighboors = FreqIndexesNeighboors + FBMC_parameters.frameGeneration.sideGuardSubcarriers(1);
end

FreqIndexesPilotsPlusNeighboors = zeros(1,3*length(FreqIndexesNeighboors));
FreqIndexesPilotsPlusNeighboors(2:3:end) = FreqIndexesNeighboors;
FreqIndexesPilotsPlusNeighboors(1:3:end) = FreqIndexesPilotsPlusNeighboors(2:3:end) - 1;
FreqIndexesPilotsPlusNeighboors(3:3:end) = FreqIndexesPilotsPlusNeighboors(2:3:end) + 1;

NumberOfTimeIndexes = floor((Dnum-3)/(TimeSpacing-1))+1;
TimeIndexes=zeros(1,3*NumberOfTimeIndexes);
TimeIndexes(1:3:end)=1+(0:(NumberOfTimeIndexes-1))*(TimeSpacing-1);
TimeIndexes(2:3:end)=1+TimeIndexes(1:3:end);
TimeIndexes(3:3:end)=1+TimeIndexes(2:3:end);

NonZeroElems = (length(TimeIndexes)/3)*length(FreqIndexesPilotsPlusNeighboors);

if(FBMC_parameters.pilots.reducePAPR==1)
    NonZeroElems = NonZeroElems - (length(TimeIndexes)/3)*(2*length(FreqIndexesPilotsPlusNeighboors)/3);
end

ZeroElems = (2*length(TimeIndexes)/3)*length(FreqIndexesNeighboors);

if(FBMC_parameters.pilots.reducePAPR==1)
    ZeroElems = ZeroElems + (length(TimeIndexes)/3)*(2*length(FreqIndexesPilotsPlusNeighboors)/3);
end

origNonZeroPilotValues = spalloc(Dnum,Snum,NonZeroElems);

PilotsRow = ones(1,length(FreqIndexesPilotsPlusNeighboors));
PilotsRow(3:4:end) = -1;
PilotsRow(4:4:end) = -1;

if(FBMC_parameters.pilots.reducePAPR==1)
    PilotsRow(1:3:end)=0;
    PilotsRow(3:3:end)=0;
end

origNonZeroPilotValues(TimeIndexes(2:12:end),FreqIndexesPilotsPlusNeighboors)=repmat(PilotsRow,length(TimeIndexes(2:12:end)),1);
origNonZeroPilotValues(TimeIndexes(5:12:end),FreqIndexesPilotsPlusNeighboors)=repmat(PilotsRow,length(TimeIndexes(5:12:end)),1);
origNonZeroPilotValues(TimeIndexes(8:12:end),FreqIndexesPilotsPlusNeighboors)=repmat(-PilotsRow,length(TimeIndexes(8:12:end)),1);
origNonZeroPilotValues(TimeIndexes(11:12:end),FreqIndexesPilotsPlusNeighboors)=repmat(-PilotsRow,length(TimeIndexes(11:12:end)),1);

ZeroPilotsPositions = spalloc(Dnum,Snum,ZeroElems);
ZeroPilotsPositions(TimeIndexes(1:3:end),FreqIndexesNeighboors)=ones(length(TimeIndexes)/3,length(FreqIndexesNeighboors));
ZeroPilotsPositions(TimeIndexes(3:3:end),FreqIndexesNeighboors)=ones(length(TimeIndexes)/3,length(FreqIndexesNeighboors));

if(FBMC_parameters.pilots.reducePAPR==1)
    ZeroPilotsPositions(TimeIndexes(2:12:end),FreqIndexesPilotsPlusNeighboors)=repmat(~PilotsRow,length(TimeIndexes(2:12:end)),1);
    ZeroPilotsPositions(TimeIndexes(5:12:end),FreqIndexesPilotsPlusNeighboors)=repmat(~PilotsRow,length(TimeIndexes(5:12:end)),1);
    ZeroPilotsPositions(TimeIndexes(8:12:end),FreqIndexesPilotsPlusNeighboors)=repmat(~PilotsRow,length(TimeIndexes(8:12:end)),1);
    ZeroPilotsPositions(TimeIndexes(11:12:end),FreqIndexesPilotsPlusNeighboors)=repmat(~PilotsRow,length(TimeIndexes(11:12:end)),1);
end

ActualPilotsPositions = spalloc(Dnum,Snum,ceil(ZeroElems/2));
ActualPilotsPositions(TimeIndexes(1:3:end)+1,FreqIndexesNeighboors)=origNonZeroPilotValues(TimeIndexes(1:3:end)+1,FreqIndexesNeighboors);

% if(strcmp(FBMC_parameters.modulator.type,'CMT')==0)
%     ActualPilotsPositions = ActualPilotsPositions * sqrt(2)*(1+1j)/2;
%     ZeroPilotsPositions = [];
%     NonZeroPilotValues = ActualPilotsPositions;
% end

%% Outputs calculation

origNonZeroPilotValues=origNonZeroPilotValues.';
pilotData.NonZeroPilotMask = origNonZeroPilotValues~=0;
pilotData.NonZeroPilotValues = origNonZeroPilotValues(pilotData.NonZeroPilotMask(:));

ActualPilotsPositions=ActualPilotsPositions.';
pilotData.ActualPilotMask = ActualPilotsPositions~=0;
pilotData.ActualPilotValues = ActualPilotsPositions(pilotData.ActualPilotMask(:));

pilotData.ZeroPilotMask = (ZeroPilotsPositions.')~=0;

[pilotData.ActualPilotXGrid, pilotData.ActualPilotYGrid] = ...
    meshgrid(TimeIndexes(1:3:end)+1, FreqIndexesNeighboors);

