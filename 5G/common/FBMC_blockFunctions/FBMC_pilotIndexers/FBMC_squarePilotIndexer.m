function pilotData = FBMC_squarePilotIndexer(FBMC_parameters, displacement)
% FBMC_SQUAREPILOTINDEXER Generate a square grid of random pilots
%
% displacement is a 4 element vector of the form
%   [initTimeDisplacement endTimeDisplacement,...
%    initFreqDisplacement endFreqDisplacement] 
%
% See also FBMC_pilotIndexers


%% Parameters extraction

Snum = FBMC_parameters.basicParameters.subcarriersNumber;
Dnum = FBMC_parameters.basicParameters.timeSymbolsNumber;

% pilot modulation parameters
modulation = FBMC_parameters.pilots.modulation;
modulationOrder = FBMC_parameters.pilots.modulationOrder;
modulationNormalize = FBMC_parameters.pilots.modulationNormalize;
modulationEnergyFactor = FBMC_parameters.pilots.modulationEnergyFactor;

guardNum = FBMC_parameters.frameGeneration.sideGuardSubcarriers;

freqSpacing = FBMC_parameters.pilots.freqSpacing;
timeSpacing = FBMC_parameters.pilots.timeSpacing;

initTimeDisplace = displacement(1);
endTimeDisplace = displacement(2);
initFreqDisplace = displacement(3);
endFreqDisplace = displacement(4);

assert(ismember(numel(guardNum), [1 2]),...
       'FBMC_parameters.frameGeneration.sideGuardSubcarriers must have 1 or 2 elements');
assert(all(guardNum < Snum),...
       ['The elements of FBMC_parameters.frameGeneration.sideGuardSubcarriers must '...
        'be less than FBMC_parameters.basicParameters.subcarriersNumber']);


%% Calculate Pilot values

if numel(guardNum) == 1
    guardNum = repmat(guardNum, 1, 2);
end

SubcData = FBMC_getUsedSubcarriers(FBMC_parameters);

pilotData.NonZeroPilotMask = logical(sparse(Snum,Dnum));
pilotData.NonZeroPilotValues = [];
pilotData.ZeroPilotMask = logical(sparse(Snum, Dnum));

DCIndex = SubcData.DCIndex;
freqind = [guardNum(1)+1+initFreqDisplace:freqSpacing:DCIndex-2 ...
           flip(Snum-guardNum(2)-endFreqDisplace:-freqSpacing:DCIndex+2)];
timeind = 1+initTimeDisplace:timeSpacing:Dnum-endTimeDisplace;

pilotData.ActualPilotMask = logical(sparse(Snum, Dnum));
pilotData.ActualPilotMask(freqind,timeind) = true;

numPilots = nnz(pilotData.ActualPilotMask);

pilotData.ActualPilotValues = FBMC_symbolModulator(...
        randi([0 modulationOrder-1],numPilots,1), modulation,...
        modulationOrder, 'integer', modulationNormalize,...
        modulationEnergyFactor);

[pilotData.ActualPilotXGrid, pilotData.ActualPilotYGrid] = meshgrid(timeind, freqind);

return

%test
surf(SMTdata.Xdata, SMTdata.Ydata,...
     abs(Heq_int(SMTdata.Ydata(:,1),SMTdata.Xdata(1,:))));
hold on;
shading flat;
stem3(Xpilotv, Ypilotv, abs(Heq),...
      'm', 'fill', 'LineStyle', 'none');
ylabel('Subcarrier number');
xlabel('Time');
zlabel('Amplitude');
title('Interpolated channel and pilot points');

end

