function pilotData = FBMC_hexPilotIndexer(FBMC_parameters, displacement,...
    ts1, ts2, fs)
% FBMC_HEXPILOTINDEXER Generate a hexagonal grid of random pilots
%
% displacement is a 4 element vector of the form
%   [initTimeDisplacement endTimeDisplacement,...
%    initFreqDisplacement endFreqDisplacement] 
%
% See also FBMC_pilotIndexers


%% Parameters extraction

Snum = FBMC_parameters.basicParameters.subcarriersNumber;
Dnum = FBMC_parameters.basicParameters.timeSymbolsNumber;

% pilot modulation parameters
modulation = FBMC_parameters.pilots.modulation;
modulationOrder = FBMC_parameters.pilots.modulationOrder;
modulationNormalize = FBMC_parameters.pilots.modulationNormalize;
modulationEnergyFactor = FBMC_parameters.pilots.modulationEnergyFactor;

guardNum = FBMC_parameters.frameGeneration.sideGuardSubcarriers;

freqSpacing = fs;
timeSpacing1 = ts1;
timeSpacing2 = ts2;

initTimeDisplace = displacement(1);
endTimeDisplace = displacement(2);
initFreqDisplace = displacement(3);
endFreqDisplace = displacement(4);

assert(ismember(numel(guardNum), [1 2]),...
       'FBMC_parameters.frameGeneration.sideGuardSubcarriers must have 1 or 2 elements');
assert(all(guardNum < Snum),...
       ['The elements of FBMC_parameters.frameGeneration.sideGuardSubcarriers must '...
        'be less than FBMC_parameters.basicParameters.subcarriersNumber']);


%% Calculate Pilot values

if numel(guardNum) == 1
    guardNum = repmat(guardNum, 1, 2);
end

SubcData = FBMC_getUsedSubcarriers(FBMC_parameters);

pilotData.NonZeroPilotMask = logical(sparse(Snum,Dnum));
pilotData.NonZeroPilotValues = [];
pilotData.ZeroPilotMask = logical(sparse(Snum, Dnum));

DCIndex = SubcData.DCIndex;
freqind = [guardNum(1)+1+initFreqDisplace:freqSpacing:DCIndex-2 ...
           flip(Snum-guardNum(2)-endFreqDisplace:-freqSpacing:DCIndex+2)];

timeind = 1+initTimeDisplace;
i = 1;
while true
    if mod(i,2) ~= 0
        k = timeind(end) + timeSpacing1;
    else
        k = timeind(end) + timeSpacing2;
    end
    i = i+1;
    
    if k > (Dnum-endTimeDisplace)
        break;
    end
    
    timeind(end+1) = k;
end


pilotData.ActualPilotMask = logical(sparse(Snum, Dnum));

for i = 1:numel(freqind)
    if mod(i,2) == 0
        pilotData.ActualPilotMask(freqind(i),timeind(1:2:end)) = true;
    else
        pilotData.ActualPilotMask(freqind(i),timeind(2:2:end)) = true;
    end
end


numPilots = nnz(pilotData.ActualPilotMask);

pilotData.ActualPilotValues = FBMC_symbolModulator(...
        randi([0 modulationOrder-1],numPilots,1), modulation,...
        modulationOrder, 'integer', modulationNormalize,...
        modulationEnergyFactor);

[pilotData.ActualPilotXGrid, pilotData.ActualPilotYGrid] = meshgrid(timeind, freqind);

return

%test
surf(SMTdata.Xdata, SMTdata.Ydata,...
     abs(Heq_int(SMTdata.Ydata(:,1),SMTdata.Xdata(1,:))));
hold on;
shading flat;
stem3(Xpilotv, Ypilotv, abs(Heq),...
      'm', 'fill', 'LineStyle', 'none');
ylabel('Subcarrier number');
xlabel('Time');
zlabel('Amplitude');
title('Interpolated channel and pilot points');

end

