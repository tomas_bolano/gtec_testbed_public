function pilotData = FBMC_nullPilotIndexer(FBMC_parameters)
% FBMC_NULLPILOTINDEXER Generate a grid without pilots
%
% See also FBMC_pilotIndexers

%% Parameters extraction

Snum = FBMC_parameters.basicParameters.subcarriersNumber;
Dnum = FBMC_parameters.basicParameters.timeSymbolsNumber;


%% Calculate Pilot values

pilotData.NonZeroPilotMask = logical(sparse(Snum,Dnum));
pilotData.NonZeroPilotValues = [];
pilotData.ActualPilotMask = logical(sparse(Snum, Dnum));
pilotData.ActualPilotValues = [];
pilotData.ActualPilotXGrid = [];
pilotData.ActualPilotYGrid = [];
pilotData.ZeroPilotMask = logical(sparse(Snum, Dnum));

end