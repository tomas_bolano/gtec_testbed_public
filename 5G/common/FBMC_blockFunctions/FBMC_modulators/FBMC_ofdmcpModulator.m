function FBMC_txSignal = FBMC_ofdmcpModulator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_dataGrid)
%FBMC_OFDMCPMODULATOR OFDM with cyclic prefix modulator
%
% See also FBMC_modulators

%% Parameters extraction

subcarriersNumber =  FBMC_parameters.basicParameters.subcarriersNumber;
txPulse = FBMC_pregeneratedConstants.pulse.TxPulse;
dN_OFDM_CP =  subcarriersNumber+FBMC_parameters.pulseShapping.cyclicPrefixLength; % Mostras por cada T

assert(subcarriersNumber == size(FBMC_dataGrid,1),...
       'The number of rows of FBMC_dataGrid must be the number of subcarriers.');


%% Signal modulation

timeSymbolsNumber =  size(FBMC_dataGrid,2);
Lpulso=length(txPulse);

intExt = floor(Lpulso/subcarriersNumber);
remExt = rem(Lpulso,intExt);

if(remExt==0)
    extMatrix = repmat(sparse(1:subcarriersNumber,1:subcarriersNumber,1),[intExt 1]);
else
    extMatrix = [repmat(sparse(1:subcarriersNumber,1:subcarriersNumber,1),[intExt 1]);sparse(1:remExt,1:remExt,1,remExt,subcarriersNumber)];
end

FBMC_txSignal = zeros(Lpulso+(timeSymbolsNumber-1)*dN_OFDM_CP,1);

for k=1:timeSymbolsNumber
    X_OFDM_CP=subcarriersNumber*ifft(FBMC_dataGrid(:,k),subcarriersNumber);
    FBMC_txSignal((1:Lpulso)+(k-1)*dN_OFDM_CP)=...
        FBMC_txSignal((1:Lpulso)+(k-1)*dN_OFDM_CP)+txPulse.*(extMatrix*X_OFDM_CP);
end


