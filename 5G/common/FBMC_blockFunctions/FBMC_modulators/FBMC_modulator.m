function FBMC_txOutput = FBMC_modulator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_dataGrid)
%FBMC_MODULATOR Wrapper to call the modulation function defined in FBMC_parameters.
%

FBMC_txOutput = FBMC_execCell(FBMC_parameters.modulator.modulatorFunction,...
    {FBMC_parameters, FBMC_pregeneratedConstants, FBMC_dataGrid});

end

