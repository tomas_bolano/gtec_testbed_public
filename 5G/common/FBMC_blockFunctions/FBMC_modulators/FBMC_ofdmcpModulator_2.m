function FBMC_txSignal = FBMC_ofdmcpModulator_2(FBMC_parameters, ~, FBMC_dataGrid)
%FBMC_OFDMCPMODULATOR_2 Basic OFDM with cyclic prefix modulator
%
% See also FBMC_modulators

%% Parameters extraction

nsubc =  FBMC_parameters.basicParameters.subcarriersNumber;
dN_CP = FBMC_parameters.pulseShapping.cyclicPrefixLength;

assert(nsubc == size(FBMC_dataGrid,1),...
       'The number of rows of FBMC_dataGrid must be the number of subcarriers.');


%% Signal modulation

FBMC_txSignal = sqrt(nsubc)*ifft(FBMC_dataGrid);
FBMC_txSignal = [FBMC_txSignal(end-dN_CP+1:end,:); FBMC_txSignal];
FBMC_txSignal = FBMC_txSignal(:);


