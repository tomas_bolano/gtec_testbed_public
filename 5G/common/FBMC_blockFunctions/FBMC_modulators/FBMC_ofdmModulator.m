function FBMC_txSignal = FBMC_ofdmModulator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_dataGrid)
%FBMC_OFDMMODULATOR OFDM modulator
%
% See also FBMC_modulators

%% Parameters extraction

subcarriersNumber =  FBMC_parameters.basicParameters.subcarriersNumber;
txPulse = FBMC_pregeneratedConstants.pulse.TxPulse;
dN_OFDM =  FBMC_parameters.basicParameters.subcarriersNumber;

assert(subcarriersNumber == size(FBMC_dataGrid,1),...
       'The number of rows of FBMC_dataGrid must be the number of subcarriers.');
   
%% Signal modulation

timeSymbolsNumber =  size(FBMC_dataGrid,2);
Lpulso=length(txPulse);

intExt = floor(Lpulso/subcarriersNumber);
remExt = rem(Lpulso,intExt);

if(remExt==0)
    extMatrix = repmat(sparse(1:subcarriersNumber,1:subcarriersNumber,1),[intExt 1]);
else
    extMatrix = [repmat(sparse(1:subcarriersNumber,1:subcarriersNumber,1),[intExt 1]);sparse(1:remExt,1:remExt,1,remExt,subcarriersNumber)];
end

FBMC_txSignal = zeros(Lpulso+(timeSymbolsNumber-1)*dN_OFDM,1);

for k=1:timeSymbolsNumber
    X_OFDM=subcarriersNumber*ifft(FBMC_dataGrid(:,k),subcarriersNumber);
    FBMC_txSignal((1:Lpulso)+(k-1)*dN_OFDM)=...
        FBMC_txSignal((1:Lpulso)+(k-1)*dN_OFDM)+txPulse.*(extMatrix*X_OFDM);
end

