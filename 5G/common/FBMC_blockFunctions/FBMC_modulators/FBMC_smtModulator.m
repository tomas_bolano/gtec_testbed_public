function FBMC_txSignal = FBMC_smtModulator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_dataGrid)
%FBMC_SMTMODULATOR SMT modulator
%
% See also FBMC_modulators


%% Parameters extraction

subcarriersNumber =  FBMC_parameters.basicParameters.subcarriersNumber;
txPulse = FBMC_pregeneratedConstants.pulse.TxPulse;

assert(subcarriersNumber == size(FBMC_dataGrid,1),...
       'The number of rows of FBMC_dataGrid must be the number of subcarriers.');


%% Signal modulation

timeSymbolsNumber = size(FBMC_dataGrid,2);
T = round(subcarriersNumber/2); % pulse half period
L = length(txPulse); % pulse length

intExt = floor(L/subcarriersNumber);
remExt = rem(L,intExt);

if(remExt==0)
    extMatrix = repmat(sparse(1:subcarriersNumber,1:subcarriersNumber,1),[intExt 1]);
else
    extMatrix = [repmat(sparse(1:subcarriersNumber,1:subcarriersNumber,1),[intExt 1]);sparse(1:remExt,1:remExt,1,remExt,subcarriersNumber)];
end

FBMC_txSignal = zeros(L+(timeSymbolsNumber-1)*subcarriersNumber/2,1);

for k=1:timeSymbolsNumber
    expvec = exp(1i*((k-1)+(0:(subcarriersNumber-1)).')*pi/2);
    expvec = abs(real(expvec)) + 1j*abs(imag(expvec));
    X_CMT=subcarriersNumber*ifft(FBMC_dataGrid(:,k).*expvec);
    FBMC_txSignal((1:L)+(k-1)*T)=...
        FBMC_txSignal((1:L)+(k-1)*T)+txPulse.*(extMatrix*X_CMT);
end

FBMC_txSignal = sqrt(1/2)*FBMC_txSignal;

