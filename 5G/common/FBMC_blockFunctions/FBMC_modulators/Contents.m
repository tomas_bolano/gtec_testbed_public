% FBMC_MODULATORS Modulation functions
%
% This directory includes functions to modulate signals.
% See each file for further details.
%
% Interface of the modulation functions 
% -------------------------------------
%
% All the modulation functions have the same interface, which is:
%
% FBMC_txSignal = FBMC_modulator(FBMC_parameters,...
%       FBMC_pregeneratedConstants, FBMC_dataGrid, param1, ..., paramN)
%
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_pregeneratedConstants
%       FBMC pregenerated Constants
%
%   FBMC_dataGrid
%       NxM matrix, where N is the number of subcarriers as specified in
%       FBMC_parameters and M is an arbitrary number of time symbols.
%       Time-frequency grid of data to transmit.
%
%   param1, ..., paramN
%       Aditional specific parameters for each function.
%
% Output parameters: 
%   FBMC_txSignal
%       Vector of the modulated time signal.
%
