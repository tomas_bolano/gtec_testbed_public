function preambleData = FBMC_smtPreambleGenerator(FBMC_parameters)
%FBMC_SMTPREAMBLEGENERATOR Generates a preamble for SMT.
%
% The preamble generated is intended to be used with the FBMC_smtModulator, 
% FBMC_smtDemodulator and FBMC_preambleSynchronizator functions.
%
% See also FBMC_preambleGenerators, FBMC_smtModulator, FBMC_smtDemodulator, 
% FBMC_preambleSynchronizator


%% Parameters extraction

Snum = FBMC_parameters.basicParameters.subcarriersNumber;
Npre = FBMC_parameters.synchronization.preambleLength;

% preamble modulation parameter
modulation = FBMC_parameters.synchronization.modulation;
modulationOrder = FBMC_parameters.synchronization.modulationOrder;
modulationNormalize = FBMC_parameters.synchronization.modulationNormalize;
modulationEnergyFactor = FBMC_parameters.synchronization.modulationEnergyFactor;


assert(Npre > 2,...
    'FBMC_parameters.synchronization.preambleLength must be greater than 2');


%% Calculate Preamble values

SubcData = FBMC_getUsedSubcarriers(FBMC_parameters);

preambleMask = false(Snum, Npre);
preambleMask(SubcData.subcarrierMask,1:2:end) = true;
preambleMask(1:2:end,1:2:end) = false;

numPreamble = nnz(preambleMask)/2;

symbols = FBMC_symbolModulator(...
            randi([0 modulationOrder-1],numPreamble,1), modulation,...
            modulationOrder, 'integer', modulationNormalize,...
            modulationEnergyFactor);

preambleValues = repmat(symbols, 2, 1);

preambleData.preambleMask = preambleMask;
preambleData.preambleValues = preambleValues;

[preambleYGrid, preambleXGrid] = ind2sub(size(preambleMask), find(preambleMask));
preambleData.preambleXGrid = reshape(preambleXGrid, [], ceil(Npre/2));
preambleData.preambleYGrid = reshape(preambleYGrid, [], ceil(Npre/2));

end

