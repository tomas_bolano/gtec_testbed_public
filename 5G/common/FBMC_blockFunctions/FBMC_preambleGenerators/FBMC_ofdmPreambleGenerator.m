function preambleData = FBMC_ofdmPreambleGenerator(FBMC_parameters)
%FBMC_OFDMPREAMBLEGENERATOR Generates a preamble for OFDM.
%
% The preamble generated is intended to be used with the FBMC_ofdmModulator or
% FBMC_ofdmcpModulator, FBMC_ofdmDemodulator or FBMC_ofdmcpDemodulator and
% FBMC_preambleSynchronizator functions.
%
% See also FBMC_ofdmModulator, FBMC_ofdmcpModulator, FBMC_ofdmDemodulator,
%   FBMC_ofdmcpDemodulator, FBMC_preambleSynchronizator.

%% Parameters extraction

Snum = FBMC_parameters.basicParameters.subcarriersNumber;
Npre = FBMC_parameters.synchronization.preambleLength;

% preamble modulation parameter
modulation = FBMC_parameters.synchronization.modulation;
modulationOrder = FBMC_parameters.synchronization.modulationOrder;
modulationNormalize = FBMC_parameters.synchronization.modulationNormalize;
modulationEnergyFactor = FBMC_parameters.synchronization.modulationEnergyFactor;


%% Calculate Preamble values

SubcData = FBMC_getUsedSubcarriers(FBMC_parameters);

preambleMask = false(Snum, Npre);
preambleMask(SubcData.subcarrierMask,:) = true;
preambleMask(1:2:end,:) = false;

numPreamble = nnz(preambleMask(:,1));

symbols = FBMC_symbolModulator(...
            randi([0 modulationOrder-1],numPreamble,1), modulation,...
            modulationOrder, 'integer', modulationNormalize,...
            modulationEnergyFactor);

preambleValues = repmat(symbols, Npre, 1);

preambleData.preambleMask = preambleMask;
preambleData.preambleValues = preambleValues;

[preambleYGrid, preambleXGrid] = ind2sub(size(preambleMask), find(preambleMask));
preambleData.preambleXGrid = reshape(preambleXGrid, [], Npre);
preambleData.preambleYGrid = reshape(preambleYGrid, [], Npre);

end

