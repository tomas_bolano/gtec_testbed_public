% FBMC_PREAMBLEGENERATORS Preamble generation functions
%
% This directory includes functions used to generate a preamble for the FBMC
% signal. This preamble will be used at the receiver for synchronization. Each
% function will create a different preamble pattern.
% See each file for further details.
%
%
% Interface of the preamble generation functions 
% ----------------------------------------------
%
% All the preamble generation functions have the same interface, which is:
%
% preambleData = FBMC_preambleGenerator(FBMC_parameters, param1, ..., paramN)
%
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   param1, ..., paramN
%       Aditional specific parameters for each function.
%
%
% Output parameters:
%   preambleData
%       Structure containing all the data for the pilot pattern generated.
%       Contains the following fields:
%
%       * preambleMask
%       Logical NxM matrix, where N is the number of subcarriers and M the
%       lenght of the preamble as specified in FBMC_parameters. Mask of the
%       preamble pilot symbols. The elements set to one indicate the
%       frequency-time elements where the preamble symbols will be placed.
%
%       * preambleValues
%       Vector of the preamble symbol values that will be inserted in the
%       preamble on the positions indicated by preambleMask.
%
%       * preambleXGrid
%           Preamble time domain indexes matrix of the symbols in meshgrid
%           format, or an empty matrix if the symbols can't be represented in a
%           meshgrid format.
%
%       * preambleYGrid
%           Preamble frequency domain indexes matrix of the symbols in meshgrid
%           format, or an empty matrix if the symbols can't be represented in a
%           meshgrid format.
%
