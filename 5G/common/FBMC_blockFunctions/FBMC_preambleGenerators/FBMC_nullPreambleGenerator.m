function preambleData = FBMC_nullPreambleGenerator(FBMC_parameters)
%FBMC_NULLPREAMBLEGENERATOR Generates an empty preamble.
%
% See also FBMC_preambleGenerators


%% Parameters extraction

Snum = FBMC_parameters.basicParameters.subcarriersNumber;

%% Calculate Preamble values

preambleData.preambleMask = zeros(Snum,0);
preambleData.preambleValues = [];
preambleData.preambleXGrid = [];
preambleData.preambleYGrid = [];

end

