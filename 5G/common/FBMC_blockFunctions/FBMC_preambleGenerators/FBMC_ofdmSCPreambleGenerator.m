function preambleData = FBMC_ofdmSCPreambleGenerator(FBMC_parameters)
%FBMC_OFDMSCPREAMBLEGENERATOR Generates a preamble for OFDM.
% This function generates a preamble as described in the Schmidl and
% Cox paper. This preamble is intended to be used with the synchronization
% function FBMC_ofdmSCSynchronizator.
%
% The preamble generated is intended to be used with the FBMC_ofdmModulator or
% FBMC_ofdmcpModulator, FBMC_ofdmDemodulator or FBMC_ofdmcpDemodulator and
% FBMC_preambleSynchronizator functions.
%
% See also FBMC_ofdmModulator, FBMC_ofdmcpModulator, FBMC_ofdmDemodulator,
%   FBMC_ofdmcpDemodulator, FBMC_preambleSynchronizator.

%% Parameters extraction

Snum = FBMC_parameters.basicParameters.subcarriersNumber;
Npre = FBMC_parameters.synchronization.preambleLength;

% preamble modulation parameter
modulation = FBMC_parameters.synchronization.modulation;
modulationOrder = FBMC_parameters.synchronization.modulationOrder;
modulationNormalize = FBMC_parameters.synchronization.modulationNormalize;
modulationEnergyFactor = FBMC_parameters.synchronization.modulationEnergyFactor;


%% Calculate Preamble values

subcData = FBMC_getUsedSubcarriers(FBMC_parameters);

preambleMask = false(Snum, Npre+1);
preambleMask(subcData.subcarrierMask,:) = true;
preambleMask(1:2:end,1:end-1) = false;

numPreamble = nnz(preambleMask);

symbols = FBMC_symbolModulator(...
            randi([0 modulationOrder-1],numPreamble,1), modulation,...
            modulationOrder, 'integer', modulationNormalize,...
            modulationEnergyFactor);

preambleValues = repmat(symbols, Npre, 1);

preambleData.preambleMask = preambleMask;
preambleData.preambleValues = preambleValues;

preambleData.preambleXGrid = [];
preambleData.preambleYGrid = [];

end

