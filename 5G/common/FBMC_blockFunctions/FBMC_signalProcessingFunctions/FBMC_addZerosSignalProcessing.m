function FBMC_outSignal = FBMC_addZerosSignalProcessing(~, ~,...
    FBMC_inSignal, startZeros, endZeros)
%FBMC_ADDZEROSSIGNALPROCESSING Add zeros to the start and end of the signal.
%
% Adds startZeros zeros at the start of FBMC_inSignal and adds endZeros zeros
% at the end. If startZeros and endZeros are not passed they are set to cero.


if nargin < 5
    endZeros = 0;
end

if nargin < 4
    startZeros = 0;
end


FBMC_outSignal = [zeros(startZeros,1); FBMC_inSignal; zeros(endZeros,1)];

end
