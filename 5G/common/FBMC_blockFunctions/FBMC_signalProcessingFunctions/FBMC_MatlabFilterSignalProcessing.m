function FBMC_outSignal = FBMC_MatlabFilterSignalProcessing(~, ~,...
    FBMC_inSignal, filterObject)
%FBMC_FIRFILTERSIGNALPROCESSING Filter the signal using a MATLAB filter object.
%   This function filters a signal using a dsp.FIRFilter.

FBMC_outSignal = step(filterObject, FBMC_inSignal);

end
