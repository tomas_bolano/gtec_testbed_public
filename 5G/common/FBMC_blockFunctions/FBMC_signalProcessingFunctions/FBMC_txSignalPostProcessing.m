function FBMC_outSignal = FBMC_txSignalPostProcessing(FBMC_parameters,...
        FBMC_pregeneratedConstants, FBMC_inSignal)
%FBMC_TXSIGNALPOSTPROCESSING Wrapper to apply the tx post processing functions defined in FBMC_parameters.
%

txPostProcFunctions = FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions;
FBMC_outSignal = FBMC_inSignal;

if isempty(txPostProcFunctions)
    return;
end

for i = 1:length(txPostProcFunctions)
    FBMC_outSignal = txPostProcFunctions{i}{1}(...
                        FBMC_parameters, FBMC_pregeneratedConstants,...
                        FBMC_outSignal, txPostProcFunctions{i}{2:end});
end

end

