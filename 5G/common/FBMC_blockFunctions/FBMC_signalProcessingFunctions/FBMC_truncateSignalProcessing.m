function FBMC_outSignal = FBMC_truncateSignalProcessing(~, ~,...
    FBMC_inSignal, startTruncation, endTruncation)
%FBMC_TRUNCATESIGNALPROCESSING Truncates the signal.
%
% Removes startTruncation samples from the start of FBMC_inSignal and
% endTruncation samples from the end. If startTruncation or endTruncation
% are not passed they are set to cero.


if nargin < 5
    endTruncation = 0;
end

if nargin < 4
    startTruncation = 0;
end

FBMC_outSignal = FBMC_inSignal(startTruncation+1:end-endTruncation);

end

