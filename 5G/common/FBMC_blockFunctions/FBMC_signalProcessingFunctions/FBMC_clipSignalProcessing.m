function FBMC_outSignal = FBMC_clipSignalProcessing(~, ~,...
    FBMC_inSignal, upperThreshold, lowerThreshold)
%FBMC_CLIPSIGNALPROCESSING Perform clipping to the signal.
%
% Clips the signal using the upper and lower Thresholds indicated in the
% parameters upperThreshold and lowerThreshold. If only upperThreshols is
% passed then the value -upperThreshold will be used for the lower threshold.
%

if nargin < 5
    lowerThreshold = -upperThreshold;
end

FBMC_outSignal = FBMC_inSignal;
FBMC_outSignal(FBMC_outSignal > upperThreshold) = upperThreshold;
FBMC_outSignal(FBMC_outSignal < lowerThreshold) = lowerThreshold;

end
