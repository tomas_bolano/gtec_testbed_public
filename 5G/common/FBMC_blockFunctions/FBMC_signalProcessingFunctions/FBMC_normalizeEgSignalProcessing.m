function FBMC_outSignal = FBMC_normalizeEgSignalProcessing(~,~, FBMC_inSignal)
%FBMC_NORMALIZESIGNALPROCESSING Normalize the signal energy

FBMC_outSignal = FBMC_inSignal/sqrt(FBMC_inSignal'*FBMC_inSignal);

end

