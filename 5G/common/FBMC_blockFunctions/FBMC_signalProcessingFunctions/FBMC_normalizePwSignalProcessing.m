function FBMC_outSignal = FBMC_normalizePwSignalProcessing(~,~, FBMC_inSignal)
%FBMC_NORMALIZESIGNALPROCESSING Normalize the signal power

pw = 1/length(FBMC_inSignal)*(FBMC_inSignal'*FBMC_inSignal);
FBMC_outSignal = FBMC_inSignal/sqrt(pw);

end

