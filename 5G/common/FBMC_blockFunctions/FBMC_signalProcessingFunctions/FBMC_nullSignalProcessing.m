function FBMC_outSignal = FBMC_nullSignalProcessing(~, ~, FBMC_inSignal)
%FBMC_NULLSIGNALPROCESSING Do not process the signal. Return the same input signal.

FBMC_outSignal = FBMC_inSignal;

end
