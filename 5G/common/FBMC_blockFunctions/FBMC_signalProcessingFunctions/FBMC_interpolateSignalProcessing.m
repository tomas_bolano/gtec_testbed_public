function FBMC_outSignal = FBMC_interpolateSignalProcessing(~, ~, FBMC_inSignal, varargin)
%FBMC_INTERPOLATESIGNALPROCESSING Interpolates the signal.
%   Wrapper function for the matlab interp function
%

FBMC_outSignal = interp(FBMC_inSignal, varargin{:});

end
