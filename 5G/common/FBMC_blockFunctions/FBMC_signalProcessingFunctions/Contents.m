% FBMC_SIGNALPROCESSINGFUNCTIONS Signal processing functions
%
% This directory includes functions to be used as post-processing functions
% in the transmitter or pre-processing functions in the receiver. These
% include functions for normalizing, clipping, filtering, etc.
% See each file for further details.
%
% Interface of the signal processing functions
% --------------------------------------------
%
% All the signal processing functions have some mandatory parameters,
% and then they can also have some specific parameters:
%
% outSignal = FBMC_signalProcessing(...
%       FBMC_parameters, FBMC_pregeneratedConstants, param1, ..., paramN)
%
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_pregeneratedConstants
%      FBMC pregenerated Constants
%
%   param1, ..., paramN
%       Additional specific parameters for each function
%
%
% Output parameters: 
%   outSignal
%       Output signal
%

