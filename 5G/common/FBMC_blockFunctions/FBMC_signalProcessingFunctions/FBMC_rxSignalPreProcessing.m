function FBMC_outSignal = FBMC_rxSignalPreProcessing(FBMC_parameters,...
        FBMC_pregeneratedConstants, FBMC_inSignal)
%FBMC_RXSIGNALPREPROCESSING Wrapper to apply the rx pre processing functions defined in FBMC_parameters.
%

rxPreProcFunctions = FBMC_parameters.rxSignalPreProcessing.rxPreProcessingFunctions;
FBMC_outSignal = FBMC_inSignal;

if isempty(rxPreProcFunctions)
    return;
end

for i = 1:length(rxPreProcFunctions)
    FBMC_outSignal = rxPreProcFunctions{i}{1}(...
                        FBMC_parameters, FBMC_pregeneratedConstants,...
                        FBMC_outSignal, rxPreProcFunctions{i}{2:end});
end

end

