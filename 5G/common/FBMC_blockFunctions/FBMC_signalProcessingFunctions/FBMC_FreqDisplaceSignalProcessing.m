function FBMC_outSignal = FBMC_FreqDisplaceSignalProcessing(~, ~,...
    FBMC_inSignal, freqDisp)
%FBMC_FREQDISPLACESIGNALPROCESSING Add a frequency displacement to the signal
% The frequency displacement must be in Hertzs.

FBMC_outSignal = FBMC_inSignal.*exp(1j*2*pi*freqDisp*(0:length(FBMC_inSignal)-1).');

end
