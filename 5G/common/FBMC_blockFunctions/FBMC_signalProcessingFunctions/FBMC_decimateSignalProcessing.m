function FBMC_outSignal = FBMC_decimateSignalProcessing(~, ~, FBMC_inSignal, varargin)
%FBMC_DECIMATESIGNALPROCESSING Decimates the signal.
%   Wrapper function for the matlab decimate function
%

FBMC_outSignal = decimate(FBMC_inSignal, varargin{:});

end
