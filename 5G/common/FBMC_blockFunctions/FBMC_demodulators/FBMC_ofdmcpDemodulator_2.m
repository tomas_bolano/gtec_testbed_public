function FBMC_rxDataGrid = FBMC_ofdmcpDemodulator_2(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_rxSignal, timeSymbolsNumber,...
    cpfactor)
%FBMC_OFDMCPDEMODULATOR_2 Basic OFDM with cyclic prefix demodulator
%
% OFDM demodulator for the modulator FBMC_ofdmcpModulator.
%
% See also FBMC_demodulators, FBMC_ofdmcpModulator

if nargin < 5
    cpfactor = 0.8;
end

%% Parameters extraction

nsubc =  FBMC_parameters.basicParameters.subcarriersNumber;
cplen = FBMC_parameters.pulseShapping.cyclicPrefixLength;
ofdmlen = nsubc+cplen;

demodp = ceil(cplen*cpfactor);
phasevec = exp(1j*2*pi*(cplen-demodp)/nsubc*(0:nsubc-1));

if nargin < 4
    timeSymbolsNumber =  FBMC_pregeneratedConstants.totalTimeSymbolsNumber;
end


%% Signal demodulation

FBMC_rxDataGrid = FBMC_rxSignal(1:timeSymbolsNumber*ofdmlen);
FBMC_rxDataGrid = reshape(FBMC_rxDataGrid, ofdmlen, timeSymbolsNumber);
FBMC_rxDataGrid = 1/sqrt(nsubc)*fft(FBMC_rxDataGrid(demodp+1+(0:nsubc-1),:)).*...
                                    repmat(phasevec.', 1, size(FBMC_rxDataGrid,2));

