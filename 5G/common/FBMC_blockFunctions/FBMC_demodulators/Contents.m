% FBMC_DEMODULATORS Demodulation functions
%
% This directory includes functions to demodulate signals.
% See each file for further details.
%
%
% Interface of the demodulation functions 
% ---------------------------------------
%
% All the demodulation functions have the same interface, which is:
%
% FBMC_rxDataGrid = FBMC_demodulator(FBMC_parameters,...
%       FBMC_pregeneratedConstants, FBMC_rxSignal, timeSymbolsNumber, ...
%       param1, ..., paramN)
%
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_pregeneratedConstants
%       FBMC pregenerated Constants
%
%   FBMC_rxSignal
%       Vector of the received signal
%
%   timeSymbolsNumber (Optional)
%       Number of time symbols to demodulate. If not specified, the number
%       of time symbols that will be demodulated will be the number specified
%       in FBMC_pregeneratedConstants.totalSymbolsNumbers.
%
%   param1, ..., paramN
%       Aditional specific parameters for each function.
%
%
% Output parameters: 
%   FBMC_rxDataGrid
%       Time-frequency grid of the demodulated data. NxM matrix, where N is the
%       number of subcarriers as specified in FBMC_parameters and M is the
%       number of time symbols demodulated.
%

