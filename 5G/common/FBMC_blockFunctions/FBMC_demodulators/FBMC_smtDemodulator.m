function FBMC_rxDataGrid = FBMC_smtDemodulator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_rxSignal, timeSymbolsNumber)
%FBMC_SMTDEMODULATOR SMT demodulator
%
% SMT demodulator for the modulator FBMC_smtModulator.
%
% See also FBMC_demodulators, FBMC_smtModulator


%% Parameters extraction

subcarriersNumber =  FBMC_parameters.basicParameters.subcarriersNumber;
rxPulse = FBMC_pregeneratedConstants.pulse.RxPulse;

if nargin < 4
    timeSymbolsNumber =  FBMC_pregeneratedConstants.totalTimeSymbolsNumber;
end


%% Signal demodulation

T =  round(subcarriersNumber/2); % pulse half period
L = length(rxPulse); % pulse length

FBMC_rxDataGrid = zeros(subcarriersNumber,timeSymbolsNumber);

intExt = floor(L/subcarriersNumber);
remExt = rem(L,intExt);

if(remExt==0)
    sumMatrix = repmat(sparse(1:subcarriersNumber,1:subcarriersNumber,1),[1 intExt]);
else
    sumMatrix = [repmat(sparse(1:subcarriersNumber,1:subcarriersNumber,1),[1 intExt]) sparse(1:remExt,1:remExt,1,subcarriersNumber,remExt)];
end

for k=1:timeSymbolsNumber
    expvec = exp(-1i*((k-1)+(0:(subcarriersNumber-1)).')*pi/2);
    expvec = abs(real(expvec)) - 1j*abs(imag(expvec));
    FBMC_rxDataGrid(:,k) = fft(sumMatrix*(FBMC_rxSignal((1:L)+(k-1)*T).*rxPulse)).*expvec;
end