function FBMC_rxDataGrid = FBMC_demodulator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_rxSignal)
%FBMC_DEMODULATOR Wrapper to call the demodulation function defined in FBMC_parameters.
%

FBMC_rxDataGrid = FBMC_execCell(FBMC_parameters.demodulator.demodulatorFunction,...
    {FBMC_parameters, FBMC_pregeneratedConstants, FBMC_rxSignal});

end

