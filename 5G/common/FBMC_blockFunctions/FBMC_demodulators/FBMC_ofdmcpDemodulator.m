function FBMC_rxDataGrid = FBMC_ofdmcpDemodulator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_rxSignal, timeSymbolsNumber)
%FBMC_OFDMCPDEMODULATOR OFDM with cyclic prefix demodulator
%
% OFDM demodulator for the modulator FBMC_ofdmcpModulator.
%
% See also FBMC_demodulators, FBMC_ofdmcpModulator

%% Parameters extraction

subcarriersNumber =  FBMC_parameters.basicParameters.subcarriersNumber;
rxPulse = FBMC_pregeneratedConstants.pulse.RxPulse;
dN_OFDM_CP =  subcarriersNumber+FBMC_parameters.pulseShapping.cyclicPrefixLength; % Mostras por cada T

if nargin < 4
    timeSymbolsNumber =  FBMC_pregeneratedConstants.totalTimeSymbolsNumber;
end


%% Signal demodulation

FBMC_rxDataGrid = zeros(subcarriersNumber,timeSymbolsNumber);
Lpulso=length(rxPulse);

intExt = floor(Lpulso/subcarriersNumber);
remExt = rem(Lpulso,intExt);

if(remExt==0)
    sumMatrix = repmat(sparse(1:subcarriersNumber,1:subcarriersNumber,1),[1 intExt]);
else
    sumMatrix = [repmat(sparse(1:subcarriersNumber,1:subcarriersNumber,1),[1 intExt]) sparse(1:remExt,1:remExt,1,subcarriersNumber,remExt)];
end

for k=1:timeSymbolsNumber
    FBMC_rxDataGrid(:,k) = fft(sumMatrix*(FBMC_rxSignal((1:Lpulso)+(k-1)*dN_OFDM_CP).*rxPulse),subcarriersNumber)*FBMC_parameters.signalGeneration.dt;
end