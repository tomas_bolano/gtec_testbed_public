function FBMC_rxDataGrid = FBMC_smtModifiedDemodulator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_rxSignal, timeSymbolsNumber)
%FBMC_SMTMODIFIEDDEMODULATOR Modified SMT demodulator
%
% SMT demodulator for the modulator FBMC_smtModifiedModulator.
%
% See also FBMC_demodulators, FBMC_smtModifiedModulator


%% Parameters extraction

subcarriersNumber =  FBMC_parameters.basicParameters.subcarriersNumber;
rxPulse = FBMC_pregeneratedConstants.pulse.RxPulse;

if nargin < 4
    timeSymbolsNumber =  FBMC_pregeneratedConstants.totalTimeSymbolsNumber;
end


%% Signal demodulation

dN_SMT =  round(subcarriersNumber/2);

FBMC_rxDataGrid = zeros(subcarriersNumber,timeSymbolsNumber);

Lpulso=length(FBMC_pregeneratedConstants.pulse.RxPulse);

intExt = floor(Lpulso/subcarriersNumber);
remExt = rem(Lpulso,intExt);

if(remExt==0)
    sumMatrix = repmat(sparse(1:subcarriersNumber,1:subcarriersNumber,1),[1 intExt]);
else
    sumMatrix = [repmat(sparse(1:subcarriersNumber,1:subcarriersNumber,1),[1 intExt]) sparse(1:remExt,1:remExt,1,subcarriersNumber,remExt)];
end

for k=1:timeSymbolsNumber
    FBMC_rxDataGrid(:,k) = fft(sumMatrix*(FBMC_rxSignal((1:Lpulso)+(k-1)*dN_SMT).*rxPulse)).*...
                         exp(-1i*((k-1)+(0:(subcarriersNumber-1)).')*pi/2);
end

