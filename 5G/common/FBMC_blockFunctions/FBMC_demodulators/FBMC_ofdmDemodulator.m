function FBMC_rxDataGrid = FBMC_ofdmDemodulator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_rxSignal, timeSymbolsNumber)
%FBMC_OFDMDEMODULATOR OFDM demodulator
%
% OFDM demodulator for the modulator FBMC_ofdmModulator.
%
% See also FBMC_demodulators, FBMC_ofdmModulator

%% Parameters extraction

subcarriersNumber =  FBMC_parameters.basicParameters.subcarriersNumber;
rxPulse = FBMC_pregeneratedConstants.pulse.RxPulse;
dN_OFDM =  FBMC_parameters.basicParameters.subcarriersNumber;

if nargin < 4
    timeSymbolsNumber =  FBMC_pregeneratedConstants.totalTimeSymbolsNumber;
end


%% Signal demodulation

FBMC_rxDataGrid = zeros(subcarriersNumber,timeSymbolsNumber);
Lpulso=length(rxPulse);

intExt = floor(Lpulso/subcarriersNumber);
remExt = rem(Lpulso,intExt);

if(remExt==0)
    sumMatrix = repmat(sparse(1:subcarriersNumber,1:subcarriersNumber,1),[1 intExt]);
else
    sumMatrix = [repmat(sparse(1:subcarriersNumber,1:subcarriersNumber,1),[1 intExt]) sparse(1:remExt,1:remExt,1,subcarriersNumber,remExt)];
end

for k=1:timeSymbolsNumber
    FBMC_rxDataGrid(:,k) = fft(sumMatrix*(FBMC_rxSignal((1:Lpulso)+(k-1)*dN_OFDM).*rxPulse),subcarriersNumber)*FBMC_parameters.signalGeneration.dt;
end