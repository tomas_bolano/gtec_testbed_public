function FBMC_syncOutput = FBMC_fixedSynchronizator(~, ~,...
    FBMC_rxSignal, offsetTime, offsetFreq)
%FBMC_FIXEDSYNCHRONIZATOR Summary of this function goes here
%   Detailed explanation goes here


%% Parameters extraction
% 
% offsetTime = FBMC_parameters.synchronization.fixedSynchronizator.offsetTime;
% offsetFreq = FBMC_parameters.synchronization.fixedSynchronizator.offsetFrequency;

%% Synchronize signal

FBMC_syncOutput.detected = 1;

FBMC_outSignal = FBMC_rxSignal;

if offsetTime > 0
    FBMC_outSignal= [FBMC_outSignal(offsetTime+1:end); zeros(offsetTime,1)];
else
    FBMC_outSignal = [zeros(abs(offsetTime),1); FBMC_outSignal(1:end-abs(offsetTime))];
end

if offsetFreq ~= 0
    FBMC_outSignal = FBMC_outSignal.*exp(1j*2*pi*offsetFreq*(0:length(FBMC_outSignal)-1).'); 
end

FBMC_syncOutput.synchronizedSignal = FBMC_outSignal;
FBMC_syncOutput.additionalData = [];

end

