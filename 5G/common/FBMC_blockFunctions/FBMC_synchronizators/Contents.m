% FBMC_SYNCHRONIZATORS Synchronization functions
%
% This directory includes functions to synchronize signals.
% See each file for further details.
%
% Interface of the synchronization functions 
% ------------------------------------------
%
% All the synchronization functions have the same interface, which is:
%
% [FBMC_syncOutput, FBMC_additionalData] = FBMC_synchronizator(FBMC_parameters, ...
%       FBMC_pregeneratedConstants, FBMC_rxSignal, FBMC_additionalData, ...
%       param1, ..., paramN)
%
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_pregeneratedConstants
%       FBMC pregenerated Constants
%
%   FBMC_rxSignal
%       Vector of the received signal
%
%   param1, ..., paramN
%       Aditional specific parameters for each function.
%
%
% Output parameters: 
%   FBMC_syncOutput
%       Structure containing the results of the synchronization procedure.
%       Contains the following fields:
%           * detected
%               Boolean flag that indicates if the synchronization function
%               detected the presence of a frame in the signal and synchronized
%               it. The synchronized signal will be returned in the
%               synchronizedSignal field.
%
%           * synchronizedSignal
%               Vector of the synchronized time signal. (Only when the detected
%               flag is set to 1).
%
%           * additionalData
%               Any other data that the function could return. The contents
%               of this field will depend on the particular synchronization
%               function.
%

