function FBMC_syncOutput = FBMC_nullSynchronizator(~,~, FBMC_rxSignal)
%FBMC_NULLSYNCHRONIZATOR Do not synchronize. Returns the same input signal.
%
% See also FBMC_synchronizators


FBMC_syncOutput.detected = 1;
FBMC_syncOutput.synchronizedSignal = FBMC_rxSignal;
FBMC_syncOutput.additionalData.estimatedTimeOffset = 0;
FBMC_syncOutput.additionalData.estimatedFrequencyOffset = 0;

end

