function FBMC_syncOutput = FBMC_preambleSynchronizator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_rxSignal,...
    threshold, Nrep, repT, t_off_adj, invPhaseEst)
%FBMC_PREAMBLESYNCHRONIZATOR Synchronize an FBMC signal with preamble
%
% Specific inputs:
%   threshold   - synchornization threashold (from 0 to 1)
%   Nrep        - Number of repetitions to search
%   repT        - period of the repetitions in the signal
%   t_off_adj   - time offset adjustment
%   invPhaseEst - inverse the phase estimation (boolean flag)
%
% See also FBMC_synchronizators


%% Parameters extraction

systemId = FBMC_pregeneratedConstants.metadata.systemId;
if strcmp(systemId, 'OFDM')
    L = FBMC_parameters.basicParameters.subcarriersNumber +...
        FBMC_parameters.pulseShapping.cyclicPrefixLength;
else
    L = numel(FBMC_pregeneratedConstants.pulse.TxPulse); % pulse length
end

K = FBMC_parameters.pulseShapping.overlappingFactor; % pulse overlapping factor

demodFun = FBMC_parameters.demodulator.demodulatorFunction; % demodulator function

% Preamble modulation parameters
% modulation = FBMC_parameters.synchronization.modulation;
% modulationOrder = FBMC_parameters.synchronization.modulationOrder;
% modulationNormalize = FBMC_parameters.synchronization.modulationNormalize;
% modulationEnergyFactor = FBMC_parameters.synchronization.modulationEnergyFactor;


preambleMask = FBMC_pregeneratedConstants.preamble.preambleMask;
preambleValues = FBMC_pregeneratedConstants.preamble.preambleValues;

totalTimeSymbols = FBMC_pregeneratedConstants.totalTimeSymbolsNumber;
syncThreshold = threshold;

% syncThreshold = FBMC_parameters.synchronization.synchronizationThreshold;
% 
% % Number of repetitions to search
% Nrep = FBMC_parameters.synchronization.preambleSynchronizator.repetitionNumber-1;
% % period of the repetitions in the signal
% repT = FBMC_parameters.synchronization.preambleSynchronizator.repetitionPeriod;
% % time offset adjustment
% t_off_adj = FBMC_parameters.synchronization.preambleSynchronizator.offsetTimeAdjustment;
% % flag to mark if we want to inverse the phase estimation
% invPhaseEst = FBMC_parameters.synchronization.preambleSynchronizator.inversePhaseEstimation;

assert(Nrep >= 1,...
       ['FBMC_parameters.synchronization.smtPreambleSynchronizator.repetitionNumber ',...
        'must be greater or equal than 2.']);


%% Synchronize signal

% set return value
FBMC_syncOutput.detected = 0;   % frame detected flag
FBMC_syncOutput.synchronizedSignal = []; % synchronized signal
FBMC_syncOutput.additionalData = []; % other information of the synchronization

rx_sig = FBMC_rxSignal;
framedetect = 0;

% Fast synchronization algorithm
N = L;
minborderdist = N/32;
maxi = floor((length(rx_sig)-repT*(1+Nrep))/(1+N-Nrep*repT));
max_corr_ind = 1;
max_corr = 0;


for i = 1:maxi
    ind = (i-1)*(N-Nrep*repT+1)+(1:N);
    adjustcorr = Nrep*repT+(i-1)*(N-Nrep*repT+1)-1;
    r = conj(rx_sig(ind+repT)).*rx_sig(ind);
    q = rx_sig(ind).*conj(rx_sig(ind))+...
        rx_sig(ind+repT).*conj(rx_sig(ind+repT));
    sumr = conv(r, ones(1,Nrep*repT), 'valid');
    sumq = conv(q, ones(1,Nrep*repT), 'valid');
    corrfun = 2*(abs(sumr)./sumq);
    [max_corr_aux, max_corr_ind_aux] = max(corrfun);

    if max_corr_aux > syncThreshold
        framedetect = 1;

        if max_corr_aux > max_corr
            max_corr = max_corr_aux;
            max_corr_ind = max_corr_ind_aux+adjustcorr;
            if max_corr_ind_aux < length(corrfun)-minborderdist
                break
            end
        else
            break
        end
    end
end

if ~framedetect
    return;
end

% calculate the time and frequency offset estimates
phaseFactor = 2*logical(invPhaseEst)-1;
t_est1 = max_corr_ind - t_off_adj - Nrep*repT; % fisr time offset estimate
f_est = 1/(2*pi*repT)*angle(rx_sig(max_corr_ind-Nrep*repT+1:max_corr_ind)'*...
        (phaseFactor*rx_sig(max_corr_ind+(1-Nrep)*repT+1:max_corr_ind+repT)));
                        
% correct frequency offset
rx_sig_sync = rx_sig.*exp((0:length(rx_sig)-1).'*-1j*2*pi*f_est);


% fine time estimation ----------------------------------------------------

% FFT of the pilot preamble
% TODO L/(2*K) is T/2, maybe we should obtain the pulse period in another way
%signalMinLength = (L+(totalTimeSymbols-1)*(round(L/(2*K))));

switch systemId
    case 'OFDM'
        signalMinLength = L*totalTimeSymbols;
    case 'SMT_PHYDYAS'
        signalMinLength = (L+(totalTimeSymbols-1)*(round(L/(2*K))));
    case 'SMT_Hermite'
        signalMinLength = (L+(totalTimeSymbols-1)*(round(L/(4*K))));
    otherwise
        error('system %s not supported.', systemId);
end

max_t = numel(rx_sig_sync)-signalMinLength; % max time offset

if t_est1 < 0
    warning('The estimated time is less than 0 (%d)', t_est1);
    t_est1 = 0;
elseif t_est1 > max_t
    warning('the estimated time exceeds the maximum permitted.')
    t_est1 = max_t;
end

% demodulate the preamble of the signal
rx_preamble = FBMC_execCell(demodFun,...
                {FBMC_parameters, FBMC_pregeneratedConstants}, ...
                {rx_sig_sync(t_est1+1:end), size(preambleMask,2)});
                   
% do an fftshift of the preamble
rx_preamble = fftshift(rx_preamble,1);

% preamble channel estimation
H_sync = zeros(size(preambleMask));
H_sync(preambleMask) = rx_preamble(preambleMask)./preambleValues;

% remove zero time symbols and do an average
H_sync = mean(H_sync(:,1:2:end),2);

% fine time estimation
% As our preamble has zeros inserted between data, the  absolute value
% of the fft obtained repeats with period pi, so we need to pick one half
% of the fft and also there is no need to do an fftshift.
H_fft = fft(H_sync);
[~, p] = max(abs(H_fft(end/4+1:end*3/4)));

% p should be at the fft center if the estimated time offset is
% correct, else it will be displaced from the center. We calculate
% the position displaced by p-round(size(H_fft,1)/4).
t_est = t_est1+(-p+round(size(H_fft,1)/4))+1;

if t_est < 0
    warning('The fine estimated time is less than 0 (%d)', t_est);
    t_est = 0;
elseif t_est > max_t
    warning('the estimated time exceeds the maximum permitted.')
    t_est = max_t;
end


FBMC_syncOutput.detected = framedetect;   % frame detected flag (1)
FBMC_syncOutput.synchronizedSignal = [rx_sig_sync(t_est+1:end);...
                                      zeros(signalMinLength-(numel(rx_sig_sync)-t_est),1)];
% TODO complete additional data
FBMC_syncOutput.additionalData.firstEstimatedTimeOffset = t_est1;
FBMC_syncOutput.additionalData.fineEstimatedTimeOffset = t_est;
FBMC_syncOutput.additionalData.estimatedFrequencyOffset = f_est;
FBMC_syncOutput.additionalData.corrfun = corrfun;
FBMC_syncOutput.additionalData.H_sync = H_sync;

% test
% figure();
% plot(corrfun);
% 
% figure();
% stem(real(H_sync));
% hold on;
% stem(imag(H_sync), '-r');
% legend('real', 'imag');
% title('Channel estimated for fine synchronization');
% 
% figure();
% stem(abs(H_sync));
% title('channel response');
% title('Channel estimated for fine synchronization');

end

