function FBMC_syncSignal = FBMC_synchronizator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_inSignal)
%FBMC_SYNCHRONIZATOR Wrapper to call the synchronization function defined in FBMC_parameters.
%

FBMC_syncSignal = FBMC_execCell(...
    FBMC_parameters.synchronization.synchronizationFunction,...
    {FBMC_parameters, FBMC_pregeneratedConstants, FBMC_inSignal});

end

