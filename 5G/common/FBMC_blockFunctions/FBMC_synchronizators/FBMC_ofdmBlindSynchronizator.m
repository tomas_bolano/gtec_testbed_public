function FBMC_syncOutput = FBMC_ofdmBlindSynchronizator(...
    FBMC_parameters, FBMC_constants, FBMC_rxSignal,...
    max_f_int, threshold_cp, threshold_p, max_n_pilots,...
    max_n_symbols)
%FBMC_OFDMBLINDSYNCHRONIZATOR Synchronize an OFDM signal
%
% Specific inputs:
%   max_f_int - half of max integer frequency offset to detect (default 5)
%   threshold_cp - threshold for the cp metric (default 0.3)
%   threshold_p - threshold for the 'pilot' metric (default 0.5)
%   max_n_pilot - maximum number of symbols to use for the pilot metric (default 5)
%   max_n_symbols - maximum number of OFDM symbols to use for the cp metric (default 30)
%
% See also FBMC_synchronizators

if nargin < 8
    max_n_symbols = 30;
end

if nargin < 7
    max_n_pilots = 5;
end

if nargin < 6
    threshold_p = 0.5;
end

if nargin < 5
    threshold_cp = 0.2;
end

if nargin < 4
    max_f_int = 5;
end


%% Parameters

nsymb = FBMC_parameters.basicParameters.timeSymbolsNumber;
nsubc = FBMC_parameters.basicParameters.subcarriersNumber;
cplen = FBMC_parameters.pulseShapping.cyclicPrefixLength;
preamblemask = FBMC_constants.preamble.preambleMask;
signalMinLength = (nsubc+cplen)*(nsymb+2);
npreamble = size(preamblemask, 2) +  FBMC_parameters.synchronization.zerosAfterPreamble;

signal_len = (nsubc+cplen)*nsymb;

if max_n_symbols <= 0 || max_n_symbols > nsymb
    max_n_symbols = nsymb;
end


%% Synchronize signal

% set return value
FBMC_syncOutput.detected = 0;   % frame detected flag
FBMC_syncOutput.synchronizedSignal = []; % synchronized signal
FBMC_syncOutput.additionalData = []; % other information of the synchronization

% signal to synchronize
syncsig = FBMC_rxSignal - mean(FBMC_rxSignal);

% As a first synchronization metric we perform a correlation
% with the cyclic prefixes of all the symbols
cpmask = zeros(nsubc+cplen, max_n_symbols);
cpmask(1:cplen,:) = 1;
cpmask = cpmask(:);

cp1 = conj(syncsig(1:end-nsubc)).*syncsig(nsubc+1:end);
sumcp1 = conv(cp1, cpmask, 'valid');
sumcp2 = conv(abs(syncsig(1:end-nsubc)).^2, cpmask, 'valid');
cpmetric = abs(sumcp1)./sumcp2;

% select all points that are greater than the specified threshold
%threshold_cp = 0.4;
[X,I] = sort(cpmetric,'descend');
thpoints = I(X > threshold_cp);

if numel(thpoints) < 2
    % frame not detected
    return;
end

% group points by distance. The correlation metric of the cyclic prefix
% has several local minima, each at a distance of nsubc. The points
% selected previously can be grouped by the distance between them. Then,
% for each group we find the one that maximizes the syncronization metric.
thpoints = sort(thpoints);
ptdiff = diff(thpoints);
ptlim = unique([0; find(ptdiff > nsubc/2); numel(ptdiff)]);

% points that maximize the cp metric
M = zeros(1, length(ptlim)-1);
for ii = 1:length(ptlim)-1
    test_points = thpoints(ptlim(ii)+1:ptlim(ii+1));
    [~,I] = max(cpmetric(test_points));
    M(ii) = test_points(I);
end

% sort points in M by their amplitude in the sumcp metric
M_val = abs(sumcp1(M));
[~,I] = sort(M_val, 'descend');
M = M(I);

% remove invalid ponints considering the frame lenght
invalid_points = M + signal_len > length(FBMC_rxSignal);
M = M(~invalid_points);

% frequency offset values to search
% sorted by absolute value
foff_values = -max_f_int:max_f_int;
[~,I] = sort(abs(foff_values));
foff_values = foff_values(I);

pilotmask = FBMC_constants.pilots.ActualPilotMask;

pilotvalues = zeros(size(pilotmask));
pilotvalues(pilotmask) = FBMC_constants.pilots.ActualPilotValues;

pilotXpos = full(any(FBMC_constants.pilots.ActualPilotMask));
pilotXposSelect = find(pilotXpos, max_n_pilots);
n_symb_selec = length(pilotXposSelect);

% indices of pilots for selected symbols
pilotind_cell = cell(1,n_symb_selec);
for ii = 1:n_symb_selec
    pilotind_cell{ii} = find(pilotmask(:,pilotXposSelect(ii)));
end

cpfactor = 0.8;
demodp = ceil(cplen*cpfactor);
phasevec = exp(1j*2*pi*(cplen-demodp)/nsubc*(0:nsubc-1));

syncmetric = 0;

% search the points in M
for ii = 1:length(M)   
    sync_est_t = max(M(ii) - cplen - 1, 1);
    sync_est_f = angle(sumcp1(M(ii)))/(2*pi);
    
    % symbols with pilots
    ofdm_t = zeros(nsubc,n_symb_selec);
    for jj = 1:n_symb_selec
        ofdm_s_ind = sync_est_t +...
                     (nsubc+cplen)*(npreamble + pilotXposSelect(jj) - 1) +...
                      demodp + (0:nsubc-1);
        
        ofdm_s = FBMC_rxSignal(ofdm_s_ind);
        ofdm_s = ofdm_s.*exp(-1j*2*pi*sync_est_f/1024*(0:nsubc-1).');
        ofdm_t(:,jj) = ofdm_s;
    end

    % convert symbols to frequency domain
    ofdm_f = fftshift(fft(ofdm_t).*repmat(phasevec.',1,n_symb_selec),1)./...
             sqrt(nsubc);

    % search for pilot match
    for jj = 1:length(foff_values)
        syncmetric_vec = zeros(1,n_symb_selec);
        f_jj = foff_values(jj);
 
        for kk = 1:n_symb_selec
            estchan = ofdm_f(pilotind_cell{kk}+f_jj, kk)./...
                      pilotvalues(pilotind_cell{kk}, pilotXposSelect(kk));
            syncmetric_vec(kk) = abs(sum(estchan(1:end-1).*conj(estchan(2:end)))./...
                             sum(abs(estchan).^2));
        end
        syncmetric = mean(syncmetric_vec);

        if syncmetric >= threshold_p
            sync_est_f = sync_est_f + foff_values(jj);
            break;
        end
    end

    if syncmetric >= threshold_p
        break;
    end
end

if syncmetric < threshold_p
    % frame not detected
    return; 
end

FBMC_syncOutput.detected = 1;
FBMC_syncOutput.synchronizedSignal = ...
    [syncsig(sync_est_t+1:end).*...
     exp(-1j*2*pi*sync_est_f/nsubc*(0:length(syncsig)-sync_est_t-1).');...
     zeros(signalMinLength-(length(syncsig)-sync_est_t),1)];
 
FBMC_syncOutput.additionalData.estimatedTimeOffset = sync_est_t;
FBMC_syncOutput.additionalData.estimatedFrequencyOffset = sync_est_f;
FBMC_syncOutput.additionalData.syncmetric = syncmetric;


end

