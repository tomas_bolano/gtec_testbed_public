function FBMC_channelEstimation = FBMC_interpChannelEstimator(FBMC_parameters,...
    FBMC_constants, FBMC_rxDataGrid, ~, interpMethod)
%FBMC_SQUARECHANNELESTIMATOR LS channel estimator for a square grid of pilots.
%


%% Parameters extraction

if nargin < 4
    interpMethod = 'spline';
end

preambleData = FBMC_constants.preamble;
pilotData = FBMC_constants.pilots;

Npreamble = size(preambleData.preambleMask,2);
Nzeros = FBMC_parameters.synchronization.zerosAfterPreamble;

assert(~isempty(pilotData.ActualPilotXGrid) && ...
       ~isempty(pilotData.ActualPilotYGrid), ...
       'The current scheme of pilots cannot be used with this function.');


%% Channel estimation

[preamble, dataGrid] = FBMC_separatePreamble(FBMC_parameters,...
        FBMC_constants, FBMC_rxDataGrid);
    
Yq = find(FBMC_constants.usedSubcarriers.subcarrierMask);

if ~isempty(preambleData.preambleXGrid)
    % preamble query points
    preXq = repmat(preambleData.preambleXGrid(1,:),...
                   size(pilotData.ActualPilotXGrid,1),1);

    % estimate the channel in the preamble
    Hpre = preamble(preambleData.preambleMask)./preambleData.preambleValues;
    Hpre = reshape(Hpre, size(preambleData.preambleXGrid));

    % interpolate the preamble and get the values for the frequency positions of the pilots
    if size(preXq,2) > 1
        Hpre_int = interp2(preambleData.preambleXGrid, preambleData.preambleYGrid,...
                           Hpre, preXq, Yq, interpMethod);
    else
        Hpre_int = interp1(preambleData.preambleYGrid, Hpre, Yq, interpMethod);
    end
else
    preXq = [];
    Hpre_int = [];
end

% Pilot mask without guard bands 
usedPilotMask = pilotData.ActualPilotMask(Yq,:);

% Create a grid and put the pilots in its position
usedPilotValuesGrid = sparse(size(usedPilotMask,1),size(usedPilotMask,2));
usedPilotValuesGrid(usedPilotMask) = FBMC_constants.pilots.ActualPilotValues;

% Data grid without guard bands 
usedDataGrid = dataGrid(Yq,:);

% Y positions of the pilot mask with pilots
pilotYpos = find(any(usedPilotMask,2));

% Create matrix for interpolated channel
Hall = zeros(numel(Yq), size(usedPilotMask, 2));

% Interpolate by rows first
for i = 1:numel(pilotYpos);
    xqmask = full(usedPilotMask(pilotYpos(i),:));
    
    xq = 1:size(Hall,2);
    x = find(xqmask);
    
    % estimate channel for pilot positions
    v = usedDataGrid(pilotYpos(i),x)./usedPilotValuesGrid(pilotYpos(i),x);

    if ~isempty(Hpre_int)
        x = [preXq(1,:)-Nzeros-Npreamble, x];
        v = [Hpre_int(pilotYpos(i),:), v];
    end    
    
    Hall(pilotYpos(i),:) = interp1(x, v, xq, 'spline');    
end

% Interpolate by columns
for i = 1:size(Hall,2)
    xq = 1:size(Hall,1);
    x = pilotYpos;
    v = Hall(x,i);
    
    Hall(:,i) = interp1(x, v, xq, 'spline');
end

% Full estimated channel
Hall_int = zeros(size(dataGrid));
Hall_int(FBMC_constants.usedSubcarriers.subcarrierMask, :) = Hall;

% figure();
% surf(abs(Hall_int));
% shading flat;
% return

FBMC_channelEstimation = Hall_int;

% TEST
% N = 5;
% figure();
% plot(abs(Hall_int(:,N)));
% grid on;
% hold on;
% 
% figure();
% surf(abs(Hall_int));
% shading flat;
% title('MMSE channel estimated');

end

