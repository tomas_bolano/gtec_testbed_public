function FBMC_channelEstimation = FBMC_MMSEStaticChannelEstimator(FBMC_parameters,...
    FBMC_constants, FBMC_rxDataGrid, no)
%FBMC_MMSESTATICCHANNELESTIMATOR MMSE channel estimator for static measurements
%

%% Parameters extraction

if nargin < 4 || isempty(no)
    error('noise spectral density ("no") not specified');
end

nsubc = FBMC_parameters.basicParameters.subcarriersNumber;
subcmask = FBMC_constants.usedSubcarriers.subcarrierMask;
nused = nnz(subcmask);

preambleData = FBMC_constants.preamble;
pilotData = FBMC_constants.pilots;

%Npreamble = size(preambleData.preambleMask,2);
Nzeros = FBMC_parameters.synchronization.zerosAfterPreamble;

dcnull = FBMC_parameters.frameGeneration.insertDCNull;
nguards = FBMC_parameters.frameGeneration.sideGuardSubcarriers;

assert(~isempty(pilotData.ActualPilotXGrid) && ...
       ~isempty(pilotData.ActualPilotYGrid), ...
       'The current scheme of pilots cannot be used with this function.');


%% Channel estimation

% we use an MMSE estimation to estimate the channel per symbols (freq.
% dimension), then we interpolate the symbols in the time dimension.
% For this interpolation we use the spline method.
interpmethod = 'spline';

% separate preamble if any
[preamble, dataGrid] = FBMC_separatePreamble(FBMC_parameters,...
                                      FBMC_constants, FBMC_rxDataGrid);

% used subcarriers positions
Yq = find(FBMC_constants.usedSubcarriers.subcarrierMask);

% pilot mask
pilotMask = pilotData.ActualPilotMask;

% Create a grid and put the pilots in its position
usedPilotValuesGrid = sparse(size(pilotMask,1),size(pilotMask,2));
usedPilotValuesGrid(pilotMask) = FBMC_constants.pilots.ActualPilotValues;

% Data grid without guard bands 
%usedDataGrid = dataGrid(Yq,:);

% Y positions of the pilot mask with pilots
pilotYpos = find(any(pilotMask,2));
pilotXpos = find(any(pilotMask,1));

% Calculate mean channel by subcarrier 
H_mean = zeros(nsubc,1);
H_ls = zeros(nsubc,1);
nxpilot = zeros(1,numel(pilotYpos));
for ii = 1:numel(pilotYpos);
    pilotXpos_ii = pilotMask(pilotYpos(ii),:);
    nxpilot(ii) = nnz(pilotXpos_ii);
    
    % estimate channel for pilot positions
    h = dataGrid(pilotYpos(ii), pilotXpos_ii)./...
        usedPilotValuesGrid(pilotYpos(ii), pilotXpos_ii);
    H_mean(pilotYpos(ii)) = mean(h);
end

% interpolate channel
xq = Yq;
x  = pilotYpos;
v  = H_mean(pilotYpos);
H_ls(Yq) = interp1(x, v, xq, interpmethod, 'extrap').';


% Estimation of the channel covariance matrix
% we first estimate the channel impulse responses

% number of columns/rows of the covariance matrix
% select only a few causal and anticausal taps
% here we use some magic numbers: 75 causal taps and 10 anticausal
ntaps_c = min(75, ceil(size(pilotMask,1)/2)) ; % causal taps
ntaps_ac = min(10, ceil(size(pilotMask,1)/2)); % anticausal taps
ntaps = ntaps_c + ntaps_ac;

% used taps mask
tapmask = false(1,nsubc);
tapmask([1:ntaps_c end-ntaps_ac+1:end]) = true;

% complete channel to nsubc extrapolating extreme values
H_ls_ext = H_ls.*hanning(numel(H_ls));
if dcnull
    H_ls_ext(nsubc/2+1) = (H_ls(nused/2-1)+H_ls(nused/2+1))/2;
end
H_ls_ext(1:nguards(1),:) = repmat(H_ls(1,:), nguards(1), 1);
H_ls_ext(end-nguards(2)+1:end) = repmat(H_ls(end), nguards(2), 1);
H_ls_ext = ifftshift(H_ls_ext, 1);

% estimate time domain channel and covariance matrix
h_ls = ifft(H_ls_ext);
C = diag(mean(abs(h_ls(tapmask).^2),2)); % covariance matrix

% DFT matrices
F = dftmtx(nsubc);
FL = F(:,tapmask);


% MMSE channel estimation
xmask = pilotYpos;
xd = ones(size(xmask));

% create diagonal matrix of x (ones)
X = zeros(nsubc,1);
X(xmask) = xd;
X = spdiags(X, 0, nsubc, nsubc);

% remove rows without pilots
X = X(xmask,:);

% pilots vector
y = H_mean(pilotYpos);

% MMSE estimation of the channel (time domain)
% NOTE: I dont know if the noise should be no or no/2
H = X*FL;
h_lmmse = (C*H'/(H*C*H' + no/mean(nxpilot)*eye(size(H,1))))*y;

H_lmmse = zeros(nsubc,1);
H_lmmse(tapmask) = h_lmmse;
H_lmmse = fft(H_lmmse);
H_lmmse_used = H_lmmse(subcmask);

% complete channel
H_int = repmat(H_lmmse_used, 1, size(pilotMask, 2));

Hall_int = zeros(nsubc, size(pilotMask, 2));
Hall_int(subcmask,:) = H_int;

% Full estimated channel
FBMC_channelEstimation = Hall_int;

% TEST
% figure();
% plot(abs(H_ls(:,1)));
% grid on;
% hold on;
% plot(abs(H_lmmse(:,1)));
% title('abs');
% 
% figure();
% surf(abs(Hall_int));
% shading flat;
% title('MMSE channel estimated');


end
