function FBMC_channelEstimationOutput = channelEstimator(FBMC_parameters, FBMC_pregeneratedConstants, FBMC_rxOutput)

%% Parameters mapping

subcarriersNumber =  FBMC_parameters.basicParameters.subcarriersNumber;
timeSymbolsNumber =  FBMC_parameters.basicParameters.timeSymbolsNumber;


ActualPilotsPositions = FBMC_pregeneratedConstants.pilots.ActualPilotMask;
ActualPilotsValues = FBMC_pregeneratedConstants.pilots.ActualPilotValues;

InterferenceCalibrationFilename = FBMC_parameters.receiver.channelEstimation.interferenceCalibrationFilename;
interpolation_method = FBMC_parameters.channelEstimation.interpolationMethod;

%% Signal flipping

% if(FBMC_parameters.transmissionType.baseband==0)
%     y_CMT_block=FBMC_flipSubcarriers(FBMC_parameters,y_CMT_block);
% end

%% Initializations

%% Pre-saving of interference values (use with CAUTION)

if(FBMC_parameters.channelEstimation.calibrateInterferenceValues==1)
    y_CMT_interference_block = y_CMT_block;
    save(FBMC_parameters.channelEstimation.interferenceCalibrationFilename,'y_CMT_interference_block');
    return;
end

%% Channel estimation

% Compute power of pilots of SNR calculation

FBMC_parametersO.receiverAnalysis.pilotsPower=((y_CMT_block(find(ActualPilotsPositions(:))))'*(y_CMT_block(find(ActualPilotsPositions(:)))))/((length(y_CMT_block(find(ActualPilotsPositions(:)))))^2);

if(isfield(FBMC_parameters.receiverAnalysis,'justAnalizePower'))
    if(FBMC_parameters.receiverAnalysis.justAnalizePower==1)
        Hest_interpolated_x2=[];
        return;
    end
end

% Channel estimation based on the expected interference values

load(InterferenceCalibrationFilename);
Hest = spalloc(size(y_CMT_block,1),size(y_CMT_block,2),length(find(ActualPilotsPositions(:))));
Hest(find(ActualPilotsPositions(:))) = (y_CMT_block(find(ActualPilotsPositions(:))))./(y_CMT_interference_block(find(ActualPilotsPositions(:))));

% Representation of the received pilots

y_CMT_blockr=reshape(y_CMT_block,subcarriersNumber,timeSymbolsNumber).';

y_CMT_interference_blockr=reshape(y_CMT_interference_block,subcarriersNumber,timeSymbolsNumber).';
Hest=reshape(Hest,subcarriersNumber,timeSymbolsNumber).';

if(FBMC_parameters.channelEstimation.representations.estimatedPilots==1)
    figure,surf(abs(Hest))
    xlabel('subcarrier index'),ylabel('symbol index'),zlabel('amplitude'),title('received pilots')
end

% Channel interpolation (x1)

[iHx_est_init, iHy_est_init]=ind2sub(size(ActualPilotsPositions),find(ActualPilotsPositions(:),1,'first'));
iHy_est = (find(ActualPilotsPositions(iHx_est_init,:))).';
iHx_est = find(ActualPilotsPositions(:,iHy_est_init));

if((FBMC_parameters.contents.bandwidthReduction>0)&&(FBMC_parameters.pilots.removePilotsFromGuards==1))
    iHx_int = ((1+FBMC_parameters.contents.bandwidthReduction):(size(Hest,2)-FBMC_parameters.contents.bandwidthReduction))';
else
    iHx_int = (1:size(Hest,2))';
end
iHy_int = (1:size(Hest,1))';

Hest_nsp = zeros(length(iHx_est),length(iHy_est));
for index = 1:length(iHy_est)
    Hest_nsp(:,index)=Hest(iHy_est(index),iHx_est);
end

[iHx_est, iHy_est] = meshgrid(iHx_est, iHy_est);
[iHx_int, iHy_int] = meshgrid(iHx_int, iHy_int);

Hest_interpolated = interp2(iHx_est, iHy_est, Hest_nsp.', iHx_int, iHy_int, interpolation_method);
if((FBMC_parameters.contents.bandwidthReduction>0)&&(FBMC_parameters.pilots.removePilotsFromGuards==1))
    Hest_interpolated = [ones(timeSymbolsNumber,FBMC_parameters.contents.bandwidthReduction) Hest_interpolated ones(timeSymbolsNumber,FBMC_parameters.contents.bandwidthReduction)];
end

if(FBMC_parameters.channelEstimation.representations.estimatedChannel==1)
    figure,surf(abs(Hest_interpolated))
    xlabel('subcarrier index'),ylabel('symbol index'),zlabel('amplitude'),title('estimated channel magnitude')
end

% Channel interpolation (x2)
if((FBMC_parameters.contents.bandwidthReduction>0)&&(FBMC_parameters.pilots.removePilotsFromGuards==1))
    iHx_int_x2 = (((.5+FBMC_parameters.contents.bandwidthReduction)):.5:((size(Hest,2)-FBMC_parameters.contents.bandwidthReduction)+.5))';
else
    iHx_int_x2 = (.5:.5:(size(Hest,2)+.5))';
end
iHy_int_x2 = (1:size(Hest,1))';

[iHx_int_x2, iHy_int_x2] = meshgrid(iHx_int_x2, iHy_int_x2);

Hest_interpolated_x2 = interp2(iHx_est, iHy_est, Hest_nsp.', iHx_int_x2, iHy_int_x2, interpolation_method);

if((FBMC_parameters.contents.bandwidthReduction>0)&&(FBMC_parameters.pilots.removePilotsFromGuards==1))
    nColumnsBefore = length(.5:.5:FBMC_parameters.contents.bandwidthReduction);
    nColumnsAfter = length(((size(Hest,2)-FBMC_parameters.contents.bandwidthReduction)+1):.5:(size(Hest,2)+.5));
    Hest_interpolated_x2 = [ones(timeSymbolsNumber,nColumnsBefore) Hest_interpolated_x2 ones(timeSymbolsNumber,nColumnsAfter)];
end

if(FBMC_parameters.channelEstimation.representations.estimatedChannelDoubleDensity==1)
    figure,surf(abs(Hest_interpolated_x2))
    xlabel('subcarrier index'),ylabel('symbol index'),zlabel('amplitude'),title('estimated channel magnitude (interpolated by two)')
end