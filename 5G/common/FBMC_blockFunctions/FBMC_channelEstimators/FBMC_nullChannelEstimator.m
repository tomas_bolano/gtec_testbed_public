function FBMC_channelEstimation = FBMC_nullChannelEstimator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_rxDataGrid, ~)
%FBMC_NULLCHANNELESTIMATOR Do not estimate the channel. Returns an empty matrix.
%

FBMC_channelEstimation = [];

end

