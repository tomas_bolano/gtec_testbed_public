% function fullInterferenceMatrix = FBMC_smtHermiteComputeFullInterferenceMatrix(FBMC_parameters)

%% Parameters mapping

subcarriersNumber =  FBMC_parameters.basicParameters.subcarriersNumber;
timeSymbolsNumber =  FBMC_parameters.basicParameters.timeSymbolsNumber;

NonZeroPilotMask=FBMC_pregeneratedConstants.pilots.NonZeroPilotMask;
NonZeroPilotValues=FBMC_pregeneratedConstants.pilots.NonZeroPilotValues;
ActualPilotMask=FBMC_pregeneratedConstants.pilots.ActualPilotMask;
ActualPilotValues=FBMC_pregeneratedConstants.pilots.ActualPilotValues;
ZeroPilotMask=FBMC_pregeneratedConstants.pilots.ZeroPilotMask;

%% Auxiliary signals loading

load(FBMC_parameters.pilots.iamInterferenceMagnitudeMatrixFilename);
% InterferencePilotMatrix;
load([FBMC_parameters.pilots.iamInterferenceSignMatrixFilename '_Even']);
InterferencePilotEvenSignMatrix=InterferencePilotSignMatrix;
load([FBMC_parameters.pilots.iamInterferenceSignMatrixFilename '_Odd']);
InterferencePilotOddSignMatrix=InterferencePilotSignMatrix;

InterferencePilotEvenMatrix = InterferencePilotMatrix.*InterferencePilotEvenSignMatrix;
InterferencePilotOddMatrix = InterferencePilotMatrix.*InterferencePilotOddSignMatrix;

interferencePilotMatrixSize = size(InterferencePilotMatrix);
maxDespl = floor(interferencePilotMatrixSize/2);
rowsNumber = maxDespl(1);
columnsNumber = maxDespl(2);

%%

fullInterferenceMatrix = zeros(subcarriersNumber,timeSymbolsNumber);

indexesNonZeroPilotMask=find(NonZeroPilotMask);

for indexNonZeroPilotMask=1:length(indexesNonZeroPilotMask)
    
    index=indexesNonZeroPilotMask(indexNonZeroPilotMask);
    NonZeroPilotColumn=floor((index-1)/subcarriersNumber)+1;
    NonZeroPilotRow=index-(NonZeroPilotColumn-1)*subcarriersNumber;
    nonZeroPilotValue=NonZeroPilotValues(indexNonZeroPilotMask);
%     indexNonZeroPilotMask
%     index
%     NonZeroPilotRow
%     NonZeroPilotColumn
%     
    fullMatrixRowIndexes = NonZeroPilotRow+(-rowsNumber:rowsNumber);
    interferencePilotMatrixRowIndexes=1:(2*rowsNumber+1);
    
    interferencePilotMatrixRowIndexes(fullMatrixRowIndexes>subcarriersNumber)=[];
    fullMatrixRowIndexes(fullMatrixRowIndexes>subcarriersNumber)=[];
    interferencePilotMatrixRowIndexes(fullMatrixRowIndexes<1)=[];
    fullMatrixRowIndexes(fullMatrixRowIndexes<1)=[];
    
    fullMatrixColumnIndexes = NonZeroPilotColumn+(-columnsNumber:columnsNumber);
    interferencePilotMatrixColumnIndexes=1:(2*columnsNumber+1);
    
    interferencePilotMatrixColumnIndexes(fullMatrixColumnIndexes>timeSymbolsNumber)=[];
    fullMatrixColumnIndexes(fullMatrixColumnIndexes>timeSymbolsNumber)=[];
    interferencePilotMatrixColumnIndexes(fullMatrixColumnIndexes<1)=[];
    fullMatrixColumnIndexes(fullMatrixColumnIndexes<1)=[];
    
    if(mod(NonZeroPilotRow,2))
        InterferencePilotMatrix=InterferencePilotOddMatrix;
    else
        InterferencePilotMatrix=InterferencePilotEvenMatrix;
    end
    fullInterferenceMatrix(fullMatrixRowIndexes,fullMatrixColumnIndexes)=...
        fullInterferenceMatrix(fullMatrixRowIndexes,fullMatrixColumnIndexes)+...
        nonZeroPilotValue*...
        InterferencePilotMatrix(interferencePilotMatrixRowIndexes,interferencePilotMatrixColumnIndexes);
end

fullInterferenceMatrix=fullInterferenceMatrix.*ActualPilotMask*1i;
fullInterferenceMatrix(ActualPilotMask)=fullInterferenceMatrix(ActualPilotMask)+ActualPilotValues;