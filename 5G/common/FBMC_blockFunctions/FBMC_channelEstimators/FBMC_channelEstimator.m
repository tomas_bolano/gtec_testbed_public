function FBMC_channelEstimation = FBMC_channelEstimator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_rxDataGrid, no)
%FBMC_CHANNELESTIMATOR Wrapper to call the channel estimation function defined in FBMC_parameters.
%

FBMC_channelEstimation = FBMC_execCell(...
    FBMC_parameters.channelEstimation.channelEstimationFunction, ...
        {FBMC_parameters, FBMC_pregeneratedConstants, FBMC_rxDataGrid, no});

end
