% FBMC_CHANNELESTIMATORS Channel estimation functions
%
% This directory includes functions used to estimate the channel.
% See each file for further details.
%
% Interface of the channel estimation functions 
% ---------------------------------------------
%
% All the channel estimation coding functions have the same interface,
% which is:
%
% FBMC_channelEstimation = FBMC_channelEstimator(FBMC_parameters,...
%       FBMC_pregeneratedConstants, FBMC_rxDataGrid, param1, ..., paramN)
%
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_pregeneratedConstants
%       FBMC pregenerated Constants.
%
%   FBMC_rxDataGrid
%       Grid of the data to equalizate.
%
%   param1, ..., paramN
%       Aditional specific parameters for each function.
%
%
% Output parameters: 
%   FBMC_channelEstimation
%       Grid of the channel estimated coefficients.
%
