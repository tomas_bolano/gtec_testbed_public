function FBMC_channelEstimation = FBMC_squareChannelEstimator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_rxDataGrid, ~, interpMethod, no)
%FBMC_SQUARECHANNELESTIMATOR LS channel estimator for a square grid of pilots.
%


%% Parameters extraction

if nargin < 4
    interpMethod = 'spline';
end

preambleData = FBMC_pregeneratedConstants.preamble;
pilotData = FBMC_pregeneratedConstants.pilots;

Npreamble = size(preambleData.preambleMask,2);
Nzeros = FBMC_parameters.synchronization.zerosAfterPreamble;

assert(~isempty(pilotData.ActualPilotXGrid) && ...
       ~isempty(pilotData.ActualPilotYGrid), ...
       'The current scheme of pilots cannot be used with this function.');


%% Channel estimation

[preamble, dataGrid] = FBMC_separatePreamble(FBMC_parameters,...
        FBMC_pregeneratedConstants, FBMC_rxDataGrid);

if ~isempty(preambleData.preambleXGrid)
    % preamble query points
    preXq = repmat(preambleData.preambleXGrid(1,:),...
                   size(pilotData.ActualPilotXGrid,1),1);
    preYq = repmat(pilotData.ActualPilotYGrid(:,1),...
                   1, size(preambleData.preambleXGrid,2));

    % estimate the channel in the preamble
    Hpre = preamble(preambleData.preambleMask)./preambleData.preambleValues;
    Hpre = reshape(Hpre, size(preambleData.preambleXGrid));

    % interpolate the preamble and get the values for the frequency positions of the pilots
    if size(preXq,2) > 1
        Hpre_int = interp2(preambleData.preambleXGrid, preambleData.preambleYGrid,...
                           Hpre, preXq, preYq, interpMethod);
    else
        Hpre_int = interp1(preambleData.preambleYGrid, Hpre, preYq, interpMethod);
    end
else
    preXq = [];
    preYq = [];
    Hpre_int = [];
end

% Generate pilot meshgrid matrices for interpolation including the preamble
pilotXv = [preXq-Nzeros-Npreamble, pilotData.ActualPilotXGrid];
pilotYv = [preYq pilotData.ActualPilotYGrid];

% estimated data channel for pilot positions
Hdata = dataGrid(pilotData.ActualPilotMask)./pilotData.ActualPilotValues;
Hdata = reshape(Hdata, size(pilotData.ActualPilotXGrid));

% estimated preamble and data channel for pilot positions
Hall = [Hpre_int Hdata];

dataXGrid = FBMC_pregeneratedConstants.usedSubcarriers.subcarrierXGrid;
dataYGrid = FBMC_pregeneratedConstants.usedSubcarriers.subcarrierYGrid;

Hall_int = zeros(size(dataGrid));
Hall_int(dataYGrid(:,1), dataXGrid(1,:)) = ...
    interp2(pilotXv, pilotYv, Hall, dataXGrid, dataYGrid, interpMethod); 

FBMC_channelEstimation = Hall_int;

% TEST
% N = 5;
% figure();
% plot(abs(Hall_int(:,N)));
% grid on;
% hold on;
% 
% figure();
% surf(abs(Hall_int));
% shading flat;
% title('MMSE channel estimated');

end

