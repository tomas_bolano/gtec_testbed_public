function FBMC_channelEstimation = FBMC_MMSEChannelEstimator(FBMC_parameters,...
    FBMC_constants, FBMC_rxDataGrid, no)
%FBMC_SQUARECHANNELESTIMATOR LS channel estimator for a square grid of pilots.
%


%% Parameters extraction

if nargin < 4 || isempty(no)
    error('noise spectral density ("no") not specified');
end

nsubc = FBMC_parameters.basicParameters.subcarriersNumber;
subcmask = FBMC_constants.usedSubcarriers.subcarrierMask;
nused = nnz(subcmask);

preambleData = FBMC_constants.preamble;
pilotData = FBMC_constants.pilots;

%Npreamble = size(preambleData.preambleMask,2);
Nzeros = FBMC_parameters.synchronization.zerosAfterPreamble;

dcnull = FBMC_parameters.frameGeneration.insertDCNull;
nguards = FBMC_parameters.frameGeneration.sideGuardSubcarriers;

assert(~isempty(pilotData.ActualPilotXGrid) && ...
       ~isempty(pilotData.ActualPilotYGrid), ...
       'The current scheme of pilots cannot be used with this function.');


%% Channel estimation

% we use an MMSE estimation to estimate the channel per symbols (freq.
% dimension), then we interpolate the symbols in the time dimension.
% For this interpolation we use the spline method.
interpmethod = 'spline';
%interpmethod = 'linear';

% separate preamble if any
[preamble, dataGrid] = FBMC_separatePreamble(FBMC_parameters,...
                                      FBMC_constants, FBMC_rxDataGrid);

% used subcarriers positions
Yq = find(FBMC_constants.usedSubcarriers.subcarrierMask);

% pilot mask
pilotMask = pilotData.ActualPilotMask;

% Create a grid and put the pilots in its position
usedPilotValuesGrid = sparse(size(pilotMask,1),size(pilotMask,2));
usedPilotValuesGrid(pilotMask) = FBMC_constants.pilots.ActualPilotValues;

% Data grid without guard bands 
%usedDataGrid = dataGrid(Yq,:);

% Y positions of the pilot mask with pilots
%pilotYpos = find(any(pilotMask,2));
pilotXpos = find(any(pilotMask,1));

% first an LS estimation is done to find the cov. matrix of the channel
% LS by columns (for symbols with pilots)
H_ls = zeros(size(pilotMask,1), length(pilotXpos));
for ii = 1:numel(pilotXpos);
    xqmask = full(pilotMask(:,pilotXpos(ii)));
    
    xq = Yq;
    x = find(xqmask);
    
    % estimate channel for pilot positions
    v = dataGrid(x,pilotXpos(ii))./...
        usedPilotValuesGrid(x,pilotXpos(ii));
    
    % interpolate to obtain an estimated full freq response
    chest = interp1(x, v, xq, interpmethod, 'extrap').';
    
    H_ls(Yq,ii) = chest;
end

% Estimation of the channel covariance matrix
% we first estimate the channel impulse responses

% number of columns/rows of the covariance matrix
% select only a few causal and anticausal taps
% here we use some magic numbers: 75 causal taps and 10 anticausal
ntaps_c = min(75, ceil(size(pilotMask,1)/2)) ; % causal taps
ntaps_ac = min(10, ceil(size(pilotMask,1)/2)); % anticausal taps
ntaps = ntaps_c + ntaps_ac;

% used taps mask
tapmask = false(1,nsubc);
tapmask([1:ntaps_c end-ntaps_ac+1:end]) = true;

% complete channel to nsubc extrapolating extreme values
H_ls_ext = H_ls;
if dcnull
    H_ls_ext(nsubc/2+1,:) = (H_ls(nsubc/2-1,:)+H_ls(nsubc/2+1,:))/2;
end
H_ls_ext(1:nguards(1),:) = repmat(H_ls(1,:), nguards(1), 1);
H_ls_ext(end-nguards(2)+1:end,:) = repmat(H_ls(end,:), nguards(2), 1);
H_ls_ext = ifftshift(H_ls_ext, 1);

% estimate time domain channel and covariance matrix
h_ls = ifft(H_ls_ext);
C = diag(mean(abs(h_ls(tapmask,:).^2),2)); % covariance matrix

% DFT matrices
F = dftmtx(nsubc);
FL = F(:,tapmask);

% MMSE by columns of the received grid
h_lmmse = zeros(ntaps, length(pilotXpos));
for ii = 1:numel(pilotXpos);
    xmask = full(pilotMask(:,pilotXpos(ii)));
    xd = usedPilotValuesGrid(xmask,pilotXpos(ii));
    
    % create diagonal matrix of x (transmitted pilots)
    X = zeros(nsubc,1);
    X(xmask) = xd;
    X = spdiags(X, 0, nsubc, nsubc);
    
    % remove rows without pilots
    X = X(xmask,:);
    
    % received data vector
    y = dataGrid(xmask,pilotXpos(ii));
    
    % MMSE estimation of the channel (time domain)
    % NOTE: i dont know if the noise should be no or no/2
    H = X*FL;
    h_lmmse(:,ii) = (C*H'/(H*C*H' + no*eye(size(H,1))))*y;
end
H_lmmse = zeros(nsubc, length(pilotXpos));
H_lmmse(tapmask,:) = h_lmmse;
H_lmmse = fft(H_lmmse);
H_lmmse_used = H_lmmse(subcmask,:);

% If a preamble is used then estimate the channel from the preamble
if ~isempty(preambleData.preambleValues)
    h_lmmse_pre = zeros(ntaps, size(preambleData.preambleMask,2));
    
    preamblegrid = zeros(size(preambleData.preambleMask));
    preamblegrid(preambleData.preambleMask) = preambleData.preambleValues;
    for ii = 1:size(preambleData.preambleMask,2)
        xmask = full(preambleData.preambleMask(:,ii));
        xd = preamblegrid(xmask,ii);
        
        % create diagonal matrix of x (transmitted preamble data)
        X = zeros(nsubc,1);
        X(xmask) = xd;
        X = spdiags(X, 0, nsubc, nsubc);
        
        % remove rows without data
        X = X(xmask,:);
        
        % received data vector
        y = preamble(xmask,ii);
        
        % MMSE estimation of the channel (time domain)
        % NOTE: i dont know if the noise should be no or no/2
        H = X*FL;
        h_lmmse_pre(:,ii) = (C*H'/(H*C*H' + no*eye(size(H,1))))*y;
    end
    H_lmmse_pre = zeros(nsubc, size(preambleData.preambleMask,2));
    H_lmmse_pre(tapmask,:) = h_lmmse_pre;
    H_lmmse_pre = fft(H_lmmse_pre);
    H_lmmse_pre_used = H_lmmse_pre(subcmask, any(preambleData.preambleMask));

    %pilotXpos_pre = -size(preambleData.preambleMask,2)+1-Nzeros:0;
    pilotXpos_pre = fliplr(-find(any(preambleData.preambleMask))+1-Nzeros);
else
    pilotXpos_pre = [];
    H_lmmse_pre_used = [];
end


% Interpolate by rows
H_int = zeros(nused, size(pilotMask, 2));
H_lmmse_int = [H_lmmse_pre_used, H_lmmse_used];
pilotXpos_int = [pilotXpos_pre, pilotXpos];
for ii = 1:size(H_int,1)
    xq = 1:size(H_int,2);
    x = pilotXpos_int;
    v = H_lmmse_int(ii,:);
    
    % smooth channel in time
    % WRONG - introduces non gaussian distortions (ISI?)
    %v_eg = sum(abs(v).^2);
    %v = smooth(v,3);
    %v = v*sqrt(v_eg/sum(abs(v).^2));
    
    H_int(ii,:) = interp1(x, v, xq, interpmethod, 'extrap');
end

Hall_int = zeros(nsubc, size(pilotMask, 2));
Hall_int(subcmask,:) = H_int;

% Full estimated channel
FBMC_channelEstimation = Hall_int;

% TEST
% figure();
% plot(abs(H_ls(:,1)));
% grid on;
% hold on;
% plot(abs(H_lmmse(:,1)));
% title('abs');
% 
% figure();
% surf(abs(Hall_int));
% shading flat;
% title('MMSE channel estimated');


end
