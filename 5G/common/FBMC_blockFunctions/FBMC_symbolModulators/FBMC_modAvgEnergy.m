function y = FBMC_modAvgEnergy(modulation, M)
% FBMC_MODAVGENERGY Mean energy of the modulation

switch lower(modulation)
    case {'pam', 'ask'}
        y = (M^2 - 1)/3;
    case 'qam'
        y = 2*(M - 1)/3;
    otherwise
        error('Modulation not supported.');
end

end

