% TEST FBMC_symbolModulator
close all

nsymb = 10000; % number of symbols to generate

%% parameters to use for the modulation
normalize = 1;
energyfactor = 1;

%% TEST PAM modulation
M = [2 4 8 16 64]; % Modulation levels to test
for ii = 1:length(M);
    bits = randi([0 1], 1, nsymb*log2(M(ii)));
    symbols = FBMC_symbolModulator(bits, 'pam', M(ii), 'bit', normalize, energyfactor);
    
    figure();
    plot(real(symbols), imag(symbols), '.');
    grid on;
    title(sprintf('PAM M = %d', M(ii)));
end

%% TEST QAM modulation
M = [4 16 64 256 1024]; % Modulation levels to test
for ii = 1:length(M);
    bits = randi([0 1], 1, nsymb*log2(M(ii)));
    symbols = FBMC_symbolModulator(bits, 'qam', M(ii), 'bit', normalize, energyfactor);
    
    figure();
    plot(real(symbols), imag(symbols), '.');
    grid on;
    title(sprintf('PAM M = %d', M(ii)));
end

