function y = FBMC_qammod(x, M, symbolOrder)
%FBMC_QAMMOD QAM modulation
%
% Modulates the integer message y using a QAM modulation.
% 
% Input parameters:
%   x
%       Integer message vector with elements from 0 to M-1.
%
%   M
%       Alphabet size. Must be an integer power of 2.
%
%   symbolOrder
%       Specifies how the function assigns binary words to corresponding
%       integers. If symbol_order is set to 'gray' (default) the function
%       uses gray ordering. If symbol ordering is set to bin the function
%       uses a natural binary order.
%   
%
% Output parameters:
%   y
%       QAM modulated output signal.
%


if nargin < 3                                                      
    symbolOrder = 'gray';
end

if strcmpi(symbolOrder, 'gray')
    x = FBMC_gray2bin(x, 'qam', M);
end

assert(mod(sqrt(M),1) == 0, 'The square root of M is not integer.');
assert(mod(log2(M),1) == 0, 'M is not an integer power of 2.');

A = 2*(1:sqrt(M)) - 1 - sqrt(M);
[A1, A2] = meshgrid(A);
const = (A1(1:M) + 1i*A2(1:M));
y = const(x + 1);

if (size(x,2) == 1)
    y = y.';
end

end
