function y = FBMC_bin2gray(x, modulation, M)
%FBMC_BIN2GRAY Convert positive integers into corresponding Gray-encoded integers

map = [];
assert(mod(log2(M),1) == 0, 'M is not an integer power of 2.');

switch lower(modulation)
    case {'pam', 'psk'}
        bin = 0:M-1;
        map = bitxor(bin, bitshift(bin, -1));

    case 'qam'
        assert(mod(sqrt(M),1) == 0, 'The square root of M is not integer.');
        
        % Obtain gray coding for sqrt(M)-PAM
        bin = 0:sqrt(M)-1;
        map = bitxor(bin, bitshift(bin, -1));
        
        % Replicate map sqrt(M) times for Inphase and Quadrature
        mapQ = repmat(map.', 1, sqrt(M));
        mapI = repmat(map, sqrt(M), 1);

        % Joint Q and I maps
        map = mapQ + mapI*2^(log2(M)/2);
        map = map(:).';
end

y = map(x+1);

if (size(x,2) == 1)
    y = y.';
end

end
