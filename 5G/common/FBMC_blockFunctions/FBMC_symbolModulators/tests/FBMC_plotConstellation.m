clear
close all

%% Configuration

mod = 'qam'; % 'qam' or 'pam'
symbolOrder = 'gray'; % 'gray or 'bin'

M = 64;

xlabeloff = -0.3;
ylabeloff = -0.3;

%% Code


switch mod
    case 'qam'
        modfun = @FBMC_qammod;
        axislim = sqrt(M)*[-1 1 -1 1];
    case 'pam'
        modfun = @FBMC_pammod;
        axislim = [-M M -1 1];
end

figure();

hold on;
axis(axislim);


for i = 0:M-1
   %j = bin2gray(i, 'qam', M);
   %symbol = pammod(i, M, 0, 'gray');
   symbol = modfun(i, M, symbolOrder);
   bitsstr = dec2bin(i,log2(M));
   scatter(real(symbol), imag(symbol), 'b');
   text(real(symbol)+xlabeloff, imag(symbol)+ylabeloff, bitsstr);
   grid on;
end


hold off;

