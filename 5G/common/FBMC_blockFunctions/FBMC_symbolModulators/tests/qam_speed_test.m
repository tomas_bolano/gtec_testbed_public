clc
clear
close all

niter = 1000;
N = 1000;


for M = [4 16 64 256]
    
    FBMC_printSeparatorLine(80, sprintf('qammod/qamdemod M = %d', M));

    x = randi([0 M-1], 1, N);
    y = qammod(x, M);
    
    tic;
    for i = 1:niter
        qammod(x, M, 0, 'gray');
    end
    e = toc;
    fprintf('qammod:                  %f s\n', e);

    tic;
    for i = 1:niter
        qamdemod(y, M, 0, 'gray');
    end
    e = toc;
    fprintf('qamdemod:                %f s\n', e); 

    tic;
    for i = 1:niter
        FBMC_qammod(x, M);
    end
    e = toc;
    fprintf('FBMC_qammod:             %f s\n', e);

    tic;
    for i = 1:niter
        FBMC_qamdemod(y, M, 'integer', 'gray', 1);
    end
    e = toc;
    fprintf('FBMC_qamdemod (integer): %f s\n', e);
    
    tic;
    for i = 1:niter
        FBMC_qamdemod(y, M, 'bit', 'gray', 1);
    end
    e = toc;
    fprintf('FBMC_qamdemod (bit):     %f s\n', e);
    
    tic;
    for i = 1:niter
        FBMC_qamdemod(y, M, 'llr', 'gray', 1);
    end
    e = toc;
    fprintf('FBMC_qamdemod (llr):     %f s\n', e);
end
