clc
clear
close all

niter = 1000;
N = 1000;


for M = [2 4 8 16 64]
    
    FBMC_printSeparatorLine(80, sprintf('pammod/pamdemod M = %d', M));

    x = randi([0 M-1], 1, N);
    y = pammod(x, M);
    
    tic;
    for i = 1:niter
        pammod(x, M, 0, 'gray');
    end
    e = toc;
    fprintf('pammod:                  %f s\n', e);

    tic;
    for i = 1:niter
        pamdemod(y, M, 0, 'gray');
    end
    e = toc;
    fprintf('pamdemod:                %f s\n', e); 

    tic;
    for i = 1:niter
        FBMC_pammod(x, M);
    end
    e = toc;
    fprintf('FBMC_pammod:             %f s\n', e);

    tic;
    for i = 1:niter
        FBMC_pamdemod(y, M, 'integer', 'gray', 1);
    end
    e = toc;
    fprintf('FBMC_pamdemod (integer): %f s\n', e);
    
    tic;
    for i = 1:niter
        FBMC_pamdemod(y, M, 'bit', 'gray', 1);
    end
    e = toc;
    fprintf('FBMC_pamdemod (bit):     %f s\n', e);
    
    tic;
    for i = 1:niter
        FBMC_pamdemod(y, M, 'llr', 'gray', 1);
    end
    e = toc;
    fprintf('FBMC_pamdemod (llr):     %f s\n', e);
end
