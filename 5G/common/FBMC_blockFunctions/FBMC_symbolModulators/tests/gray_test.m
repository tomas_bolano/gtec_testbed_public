clear
close all

%% Test PAM gray2bin bin2gray
fprintf('Testing PAM FBMC_gray2bin and FBMC_bin2gray... ');
assert(all(FBMC_gray2bin(FBMC_bin2gray(0:1, 'pam', 2), 'pam', 2) == 0:1));
assert(all(FBMC_gray2bin(FBMC_bin2gray(0:3, 'pam', 4), 'pam', 4) == 0:3));
assert(all(FBMC_gray2bin(FBMC_bin2gray(0:7, 'pam', 8), 'pam', 8) == 0:7));
assert(all(FBMC_gray2bin(FBMC_bin2gray(0:15, 'pam', 16), 'pam', 16) == 0:15));
fprintf('OK\n');


%% Test QAM gray2bin bin2gray
fprintf('Testing QAM FBMC_gray2bin and FBMC_bin2gray... ');
assert(all(FBMC_gray2bin(FBMC_bin2gray(0:3, 'qam', 4), 'qam', 4) == 0:3));
assert(all(FBMC_gray2bin(FBMC_bin2gray(0:15, 'qam', 16), 'qam', 16) == 0:15));
assert(all(FBMC_gray2bin(FBMC_bin2gray(0:63, 'qam', 64), 'qam', 64) == 0:63));
assert(all(FBMC_gray2bin(FBMC_bin2gray(0:255, 'qam', 256), 'qam', 256) == 0:255));
fprintf('OK\n');


%% Test PSK gray2bin bin2gray
fprintf('Testing PSK FBMC_gray2bin and FBMC_bin2gray... ');
assert(all(FBMC_gray2bin(FBMC_bin2gray(0:1, 'psk', 2), 'psk', 2) == 0:1));
assert(all(FBMC_gray2bin(FBMC_bin2gray(0:3, 'psk', 4), 'psk', 4) == 0:3));
assert(all(FBMC_gray2bin(FBMC_bin2gray(0:7, 'psk', 8), 'psk', 8) == 0:7));
assert(all(FBMC_gray2bin(FBMC_bin2gray(0:15, 'psk', 16), 'psk', 16) == 0:15));
fprintf('OK\n');

