clear
close all

%% Test FBMC_qamdemod integer output
N = 1000;

coding = {'bin', 'gray'};
vectorType = {'row', 'column', 'matrix'};

for j = 1:2
    for M = [4 16 64 256 1024]
        fprintf('Testing integer output for FBMC_qamdemod for %s coded %d-QAM... ', coding{j}, M);
        for k = 1:numel(vectorType)
            switch vectorType{k}
                case 'row',    x = randi([0 M-1], 1, N);
                case 'column', x = randi([0 M-1], N, 1);
                case 'matrix', x = randi([0 M-1], round(sqrt(N)));
                otherwise
                    error('invalid vectorType');
            end                
            z = FBMC_qamdemod(FBMC_qammod(x, M, coding{j}), M, 'integer', coding{j});
            assert(all(z(:) == x(:)));
            assert(all(size(z) == size(x)));
        end
        fprintf('OK\n');
    end
end


%% Test FBMC_qamdemod bit output
N = 1000;

coding = {'bin', 'gray'};
vectorType = {'row', 'column', 'matrix'};

for j = 1:2
    for M = [4 16 64 256 1024]
        fprintf('Testing bit output for FBMC_qamdemod for %s coded %d-QAM... ', coding{j}, M);
        for k = 1:numel(vectorType)
            switch vectorType{k}
                case 'row',    x = randi([0 M-1], 1, N);
                case 'column', x = randi([0 M-1], N, 1);
                case 'matrix', x = randi([0 M-1], round(sqrt(N)));
                otherwise
                    error('invalid vectorType');
            end
            x_bits = reshape(de2bi(x, 'left-msb', log2(M)).', 1, []);
            z = FBMC_qamdemod(FBMC_qammod(x, M, coding{j}), M, 'bit', coding{j});
            assert(all(x_bits == z)); 
        end
        fprintf('OK\n');
    end
end


%% Test FBMC_qamdemod llr output
N = 1000;

coding = {'bin', 'gray'};
vectorType = {'row', 'column', 'matrix'};

for j = 1:numel(coding)
    for M = [4 16 64 256 1024]
        fprintf('Testing llr output for FBMC_qamdemod for %s coded %d-QAM... ', coding{j}, M);
        for k = 1:numel(vectorType)
            switch vectorType{k}
                case 'row',    x = randi([0 M-1], 1, N);
                case 'column', x = randi([0 M-1], N, 1);
                case 'matrix', x = randi([0 M-1], round(sqrt(N)));
                otherwise
                    error('invalid vectorType');
            end
            x_bits = reshape(de2bi(x, 'left-msb', log2(M)).', 1, []);
            [z1] = FBMC_qamdemod(FBMC_qammod(x, M, coding{j}), M, 'llr', coding{j}, 0);
            [z2] = FBMC_qamdemod(FBMC_qammod(x, M, coding{j}), M, 'llr', coding{j}, 1);
            assert(all(z1 == (z1(1)/z2(1))*z2));
            z1_bits = z1 >= 0;
            z2_bits = z2 >= 0;
            assert(all(x_bits == z1_bits));
            assert(all(x_bits == z2_bits));
            
        end
        fprintf('OK\n');
    end
end

