clear
close all

niter = 1000;
mod = 'qam';
M = 64;
N = 10000;
x = randi([0 M-1], 1, N);

FBMC_printSeparatorLine(80, 'gray2bin/bin2gray times');

tic;
for i = 1:niter
    gray2bin(x, mod, M);
end
e = toc;
fprintf('gray2bin:           %f s\n', e);

tic;
for i = 1:niter
    bin2gray(x, mod, M);
end
e = toc;
fprintf('bin2gray:           %f s\n', e);

tic;
for i = 1:niter
    FBMC_gray2bin(x, mod, M);
end
e = toc;
fprintf('FBMC_gray2bin:      %f s\n', e);

tic;
for i = 1:niter
    FBMC_bin2gray(x, mod, M);
end
e = toc;
fprintf('FBMC_bin2gray:      %f s\n', e);