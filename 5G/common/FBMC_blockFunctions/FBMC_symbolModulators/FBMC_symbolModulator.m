function FBMC_txSymbols = FBMC_symbolModulator(x, modulation,...
    modulationOrder, inputType, normalize, energyFactor)
%FBMC_SYMBOLMODULATOR Maps a sequence of bits to symbols
%
% This function maps the input sequence of bits inBits into a sequence of
% symbols of the requested modulation. 
%
% Input Parameters:
%   x
%       Message to map into symbols.
%
%   modulation
%       Modulation to use. Available values are 'QAM' and 'PAM'.
%   
%   modulationOrder
%       Order of the modulation.
%
%   inputType
%       Type of the input x. Available values are 'bit' and 'integer'.
%       If 'bit' the input x must be a sequence of the bits to modulate.
%       If 'integer' the input x must be a sequence of integer from 0 to 
%       modulationOrder-1. The default is 'bit'.
%
%   normalize
%       If true the constellation used will be normalized (the mean energy
%       will be set to 1). The default is true.
%
%   energyFactor
%       Factor to multiply the energy of the symbols. The energy of the
%       symbols will have energyFactor times its original energy.
%       The default is 1.
%


if nargin < 4
    inputType = 'bit';
end

if nargin < 5
    normalize = 1;
end

if nargin < 6
    energyFactor = 1;
end


switch inputType
    case 'bit'
        x = reshape(x, log2(modulationOrder), []).';
        message = bi2de(uint16(x), 'left-msb');
    case 'integer'
        message = x;
    otherwise
        error('Invalid input type');
end

switch lower(modulation)
    case 'qam'
        symbols = FBMC_qammod(message, modulationOrder);
    case 'pam'
        symbols = FBMC_pammod(message, modulationOrder);
    otherwise
        error('Modulation not supported.');
end

if normalize
    modAvgEnergy = FBMC_modAvgEnergy(modulation, modulationOrder);
    symbols = (1/sqrt(modAvgEnergy))*symbols;
end

if energyFactor ~= 1
    symbols = sqrt(energyFactor)*symbols;
end

FBMC_txSymbols = symbols;

end
