function y = FBMC_gray2bin(x, modulation, M)
%FBMC_BIN2GRAY Convert positive integers into corresponding Gray-encoded integers

bin = 0:M-1;
bin2graymap = FBMC_bin2gray(bin, modulation, M); % Obtain bin2gray mapping
map(bin2graymap+1) = bin; % Reverse mapping

y = map(x+1); % Assign output

if (size(x,2) == 1)
    y = y.';
end

end
