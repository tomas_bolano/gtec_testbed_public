function y = FBMC_pammod(x, M, symbolOrder)
%FBMC_PAMMOD PAM modulation
%
% Modulates the integer message y using a PAM modulation.
% 
% Input parameters:
%   x
%       Integer message vector with elements from 0 to M-1.
%
%   M
%       Alphabet size. Must be an integer power of 2.
%
%   symbolOrder
%       Specifies how the function assigns binary words to corresponding
%       integers. If symbol_order is set to 'gray' (default) the function
%       uses gray ordering. If symbol ordering is set to bin the function
%       uses a natural binary order.
%   
%
% Output parameters:
%   y
%       PAM modulated output signal.
%


if nargin < 3                                                      
    symbolOrder = 'gray';
end

if strcmpi(symbolOrder, 'gray')
    x = FBMC_gray2bin(x, 'pam', M);                         
end

assert(mod(log2(M),1) == 0, 'M is not an integer power of 2.');

const = -M+1:2:M-1;
y = const(x + 1);

if (size(x,2) == 1)
    y = y.';
end

end
