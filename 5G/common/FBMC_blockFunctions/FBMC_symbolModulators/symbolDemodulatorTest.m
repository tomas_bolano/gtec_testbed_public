% TEST FBMC_symbolDemodulator
close all;
clc;

nsymb = 10000; % number of symbols to generate

%% parameters to use for the modulation
normalize = 1;
energyfactor = 1;

%% TEST PAM modulation
M = [2 4 8 16 64]; % Modulation levels to test
for ii = 1:length(M);
    txbits = randi([0 1], 1, nsymb*log2(M(ii)));
    symbols = FBMC_symbolModulator(txbits, 'pam', M(ii), 'bit', normalize, energyfactor);
    rxbits = FBMC_symbolDemodulator(symbols, 'pam', M(ii), normalize, energyfactor, 'bit');
    
    assert(all(txbits == rxbits));
end

%% TEST QAM modulation
M = [4 16 64 256 1024]; % Modulation levels to test
for ii = 1:length(M);
    txbits = randi([0 1], 1, nsymb*log2(M(ii)));
    symbols = FBMC_symbolModulator(txbits, 'qam', M(ii), 'bit', normalize, energyfactor);
    rxbits = FBMC_symbolDemodulator(symbols, 'qam', M(ii), normalize, energyfactor, 'bit');
    
    assert(all(txbits == rxbits));
end

fprintf('OK\n');

