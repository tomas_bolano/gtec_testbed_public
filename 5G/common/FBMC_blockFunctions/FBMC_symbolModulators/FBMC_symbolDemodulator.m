function FBMC_out = FBMC_symbolDemodulator(inSymbols, modulation,...
    modulationOrder, normalize, energyFactor, outputType, symbolOrder)
%FBMC_SYMBOLDEMODULATOR Demodulate symbols of a constellation
%
% This function maps the input sequence of bits inBits into a sequence of
% symbols of the requested modulation. 
%
% Input Parameters:
%   inSymbols
%       Sequence of symbols to demodulate.
%
%   modulation
%       Modulation to use. Available values are 'QAM' and 'PAM'.
%   
%   modulationOrder
%       Order of the modulation.
%
%   normalize
%       If true the constellation used will be normalized (the mean energy
%       will be set to 1). (Default: true).
%
%   energyFactor
%       Multiplicative factor to use for the energy of the constellation
%       used. The energy of the symbols of the constellation used will have
%       energyFactor times its original energy. (Default: 1).
%
%   outputType
%       Output type, one of the following: 'integer', 'bit', 'llr'.
%       (Default: 'integer').
%
%   symbolOrder
%       Specifies how the function assigns binary words to corresponding
%       integers. If symbol_order is set to 'gray' (default) the function
%       uses gray ordering. If symbol ordering is set to bin the function
%       uses a natural binary order.


if nargin < 4
    normalize = 1;
end

if nargin < 5
    energyFactor = 1;
end

if nargin < 6
    outputType = 'integer';
end

if nargin < 7
    symbolOrder = 'gray';
end


if normalize
    modAvgEnergy = FBMC_modAvgEnergy(modulation, modulationOrder);
    inSymbols = sqrt(modAvgEnergy)*inSymbols;
end

if energyFactor ~= 1
    inSymbols = 1/sqrt(energyFactor)*inSymbols;
end

switch lower(modulation)
    case 'qam'
        FBMC_out = FBMC_qamdemod(inSymbols, modulationOrder, outputType, symbolOrder);
    case 'pam'
        FBMC_out = FBMC_pamdemod(inSymbols, modulationOrder, outputType, symbolOrder);
    otherwise
        error('Modulation not supported.');
end

end
