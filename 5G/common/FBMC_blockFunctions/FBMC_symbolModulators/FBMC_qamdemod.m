function z = FBMC_qamdemod(y, M, outputType, symbolOrder, simplifiedllr)
%FBMC_QAMDEMOD QAM demodulation
%
% Demodulates the complex envelope y of a quadrature amplitude modulated
% signal.
% 
% Input parameters:
%   y
%       Complex envelope y of a quadrature amplitude modulated signal.
%
%   M
%       Alphabet size. Must be an integer power of 2.
%
%   outputType
%       Output type, one of the following: 'integer', 'bit', 'llr'.
%       The default is 'integer'.
%
%   symbolOrder
%       Specifies how the function assigns binary words to corresponding
%       integers. If symbol_order is set to 'gray' (default) the function
%       uses gray ordering. If symbol ordering is set to bin the function
%       uses a natural binary order.
%
%   simpllr
%       Only for outputType 'llr'. Use a simplified LLR calculation to
%       calculate the soft bits (if available) (default = 1).
%   
%
% Output parameters:
%   z
%       Demodulated message. The values returned depend on the output type
%       specified in the outputType parameter, as shown in the following
%       table:
%       
%       +-----------+-----------------------------------------------------+
%       | ouputType | z                                                   |
%       +-----------+-----------------------------------------------------+
%       | integer   | Demodulated integer values from 0 to M-1            |
%       | bit       | Demodulated bits                                    |
%       | llr       | Approximate log-likelihood ratio value for each bit |
%       +-----------+-----------------------------------------------------+
%
%       TODO add some comments about the llr output
%

if nargin < 3
    outputType = 'integer';
end

if nargin < 4
    symbolOrder = 'gray';
end

if nargin < 5
    simplifiedllr = 1;
end

assert(mod(sqrt(M),1) == 0, 'The square root of M is not integer.');
assert(mod(log2(M),1) == 0, 'M is not an integer power of 2.');

assert(ismember(outputType, {'integer', 'bit', 'llr'}),...
       'specified outputType is not valid.');

% Demodulate bit message using a hard decision rule for a gray coding
% when M is 4, 16 or 64 and return
if strcmpi(outputType, 'bit') && strcmpi(symbolOrder, 'gray')
    if M == 4
        z = zeros(1, log2(M)*numel(y));
        z(1:2:end) = real(y) >= 0;
        z(2:2:end) = imag(y) >= 0;
        return;
    elseif M == 16
        z = zeros(1, log2(M)*numel(y));
        z(1:4:end) = real(y) >= 0;
        z(2:4:end) = real(y) >= -2 & real(y) <= 2;
        z(3:4:end) = imag(y) >= 0;
        z(4:4:end) = imag(y) >= -2 & imag(y) <= 2;
        return;
    elseif M == 64
        z = zeros(1, log2(M)*numel(y));
        z(1:6:end) = real(y) >= 0;
        z(2:6:end) = real(y) >= -4 & real(y) <= 4;
        z(3:6:end) = (real(y) >= -6 & real(y) <= -2) |...
                     (real(y) >= 2 & real(y) <= 6);
        z(4:6:end) = imag(y) >= 0;
        z(5:6:end) = imag(y) >= -4 & imag(y) <= 4;
        z(6:6:end) = (imag(y) >= -6 & imag(y) <= -2) |...
                     (imag(y) >= 2 & imag(y) <= 6);
        return;
    end    
end


% Demodulate integer or bit message using a hard decision rule and return
if strcmpi(outputType, 'integer') || strcmpi(outputType, 'bit')  
    zI = round((real(y) + sqrt(M)-1)./2);
    zI(zI > (sqrt(M)-1)) = sqrt(M)-1;
    zI(zI < 0) = 0;
    
    zQ = round((imag(y) + sqrt(M)-1)./2);
    zQ(zQ > (sqrt(M)-1)) = sqrt(M)-1;
    zQ(zQ < 0) = 0;
    
    z = zQ + sqrt(M)*zI;
    
    if strcmpi(symbolOrder, 'gray')
        z = FBMC_bin2gray(z, 'qam', M);
    end
    
    if strcmpi(outputType, 'bit')
        z = reshape(de2bi(z, 'left-msb').', 1, []);
    end
    
    return;
end


% Specific procedures to calculate soft bits when using gray mapping
% Reference:
% Filippo Tosato and Paola Bisaglia, "Simplified Soft-Output Demapper for
% Binary Interleaved COFDM with Application to HIPERLAN/2".
z = [];
y = y(:).'; % make y a row vector

if simplifiedllr && strcmpi(symbolOrder, 'gray')
    if M == 4
        z = [real(y); imag(y)];
        z = z(:).';        
    elseif M == 16
        z = [real(y.'), -abs(real(y.')) + 2, imag(y.'), -abs(imag(y.')) + 2];
        
        z(real(y) < -2, 1) = 2*(real(y(real(y) < -2)) + 1);
        z(real(y) > 2, 1)  = 2*(real(y(real(y) > 2)) - 1);
        
        z(imag(y) < -2, 3) = 2*(imag(y(imag(y) < -2)) + 1);
        z(imag(y) > 2, 3)  = 2*(imag(y(imag(y) > 2)) - 1);
        
        z = z.';
        z = z(:).';
    elseif M == 64
        z = zeros(numel(y), log2(M));
        
        % 1st bit
        z(abs(real(y)) <= 2, 1) = real(y(abs(real(y)) <= 2));
        z(real(y) > 2 & real(y) <= 4, 1) = 2*(real(y(real(y) > 2 & real(y) <= 4)) - 1);
        z(real(y) > 4 & real(y) <= 6, 1) = 3*(real(y(real(y) > 4 & real(y) <= 6)) - 2);
        z(real(y) > 6, 1) = 4*(real(y(real(y) > 6)) - 3);
        z(real(y) >= -4 & real(y) < -2, 1) = 2*(real(y(real(y) >= -4 & real(y) < -2)) + 1);
        z(real(y) >= -6 & real(y) < -4, 1) = 3*(real(y(real(y) >= -6 & real(y) < -4)) + 2);
        z(real(y) < -6, 1) = 4*(real(y(real(y) < -6)) + 3);
        
        % 2nd bit
        z(abs(real(y)) <= 2, 2) = 2*(-abs(real(y(abs(real(y)) <= 2))) + 3);
        z(abs(real(y)) > 2 & abs(real(y)) <= 6, 2) = 4 - abs(real(y(abs(real(y)) > 2 & abs(real(y)) <= 6)));
        z(abs(real(y)) > 6, 2) = 2*(-abs(real(y(abs(real(y)) > 6))) + 5);
        
        % 3rd bit
        z(abs(real(y)) <= 4, 3) = abs(real(y(abs(real(y)) <= 4))) - 2;
        z(abs(real(y)) > 4, 3) = -abs(real(y(abs(real(y)) > 4))) + 6;
        
        % 4th bit
        z(abs(imag(y)) <= 2, 4) = imag(y(abs(imag(y)) <= 2));
        z(imag(y) > 2 & imag(y) <= 4, 4) = 2*(imag(y(imag(y) > 2 & imag(y) <= 4)) - 1);
        z(imag(y) > 4 & imag(y) <= 6, 4) = 3*(imag(y(imag(y) > 4 & imag(y) <= 6)) - 2);
        z(imag(y) > 6, 4) = 4*(imag(y(imag(y) > 6)) - 3);
        z(imag(y) >= -4 & imag(y) < -2, 4) = 2*(imag(y(imag(y) >= -4 & imag(y) < -2)) + 1);
        z(imag(y) >= -6 & imag(y) < -4, 4) = 3*(imag(y(imag(y) >= -6 & imag(y) < -4)) + 2);
        z(imag(y) < -6, 4) = 4*(imag(y(imag(y) < -6)) + 3);
        
        % 5th bit
        z(abs(imag(y)) <= 2, 5) = 2*(-abs(imag(y(abs(imag(y)) <= 2))) + 3);
        z(abs(imag(y)) > 2 & abs(imag(y)) <= 6, 5) = 4 - abs(imag(y(abs(imag(y)) > 2 & abs(imag(y)) <= 6)));
        z(abs(imag(y)) > 6, 5) = 2*(-abs(imag(y(abs(imag(y)) > 6))) + 5);
        
        % 6th bit
        z(abs(imag(y)) <= 4, 6) = abs(imag(y(abs(imag(y)) <= 4))) - 2;
        z(abs(imag(y)) > 4, 6) = -abs(imag(y(abs(imag(y)) > 4))) + 6;
        
        z = z.';
        z = z(:).';
    end
end


% General procedure to calculate soft bits
if isempty(z)
    % Construct matrices Sx0, Sx1 and Sy0 and Sy1
    Sx0 = -inf(sqrt(M), 1, log2(M));
    Sx1 = -inf(sqrt(M), 1, log2(M));
    
    Sy0 = -inf(sqrt(M), 1, log2(M));
    Sy1 = -inf(sqrt(M), 1, log2(M));
    
    const = FBMC_qammod(0:M-1, M, symbolOrder);
    bits = de2bi(0:M-1, 'left-msb');
    
    for k = 0:log2(M)-1
        vx0 = unique(real(const(bits(:,k+1) == 0)));
        vx1 = unique(real(const(bits(:,k+1) == 1)));
            
        vy0 = unique(imag(const(bits(:,k+1) == 0)));
        vy1 = unique(imag(const(bits(:,k+1) == 1)));
            
        Sx0(1:numel(vx0), 1, k+1) = vx0;
        Sx1(1:numel(vx0), 1, k+1) = vx1;
        
        Sy0(1:numel(vy0), 1, k+1) = vy0;
        Sy1(1:numel(vy1), 1, k+1) = vy1;
    end
    
    Sx0_ext = repmat(Sx0, 1, numel(y), 1);
    Sx1_ext = repmat(Sx1, 1, numel(y), 1);
    Sy0_ext = repmat(Sy0, 1, numel(y), 1);
    Sy1_ext = repmat(Sy1, 1, numel(y), 1);
    
    min_x0 = squeeze(min(abs(bsxfun(@minus, real(y), Sx0_ext))).^2);
    min_x1 = squeeze(min(abs(bsxfun(@minus, real(y), Sx1_ext))).^2);
    
    min_y0 = squeeze(min(abs(bsxfun(@minus, imag(y), Sy0_ext))).^2);
    min_y1 = squeeze(min(abs(bsxfun(@minus, imag(y), Sy1_ext))).^2);
    
    z = (min_x0 - min_x1 + min_y0 - min_y1).';
    z = z(:).';
end


% General procedure to calculate soft bits
% This procedure considers all the points in the modulation
% It is slow but correct.
% if isempty(z)
%     
%     
%     % Construct the matrix S where S(:,k,i) contains the symbols
%     % for the bit in position k (1 to log2(M)) that has the value
%     % i-1 (0 or 1)
%     S = zeros(M/2, log2(M), 2);
% 
%     const = FBMC_qammod(0:M-1, M, symbolOrder);
%     bits = de2bi(0:M-1, 'left-msb');
% 
%     for k = 0:log2(M)-1
%         for bi = [0 1]
%             S(:, k+1, bi+1) = const(bits(:,k+1) == bi);
%         end
%     end
% 
%     z = zeros(1, log2(M)*numel(y));
%     l = 1;
%     for i = 1:numel(y)
%         for k = 1:log2(M)
%             z(l) = min(abs(y(i)-S(:,k,1)).^2) - min(abs(y(i)-S(:,k,2)).^2);
%             l = l+1;
%         end
%     end
% end

end
