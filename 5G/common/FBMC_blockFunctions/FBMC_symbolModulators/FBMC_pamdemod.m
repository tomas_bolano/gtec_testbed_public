function z = FBMC_pamdemod(y, M, outputType, symbolOrder, simplifiedllr)
%FBMC_PAMDEMOD PAM demodulation
%
% Demodulates the complex envelope y of a pulse amplitude modulated
% signal.
% 
% Input parameters:
%   y
%       Complex envelope y of a pulse amplitude modulated signal.
%
%   M
%       Alphabet size. Must be an integer power of 2.
%
%   outputType
%       Output type, one of the following: 'integer', 'bit', 'llr'.
%       The default is 'integer'.
%
%   symbolOrder
%       Specifies how the function assigns binary words to corresponding
%       integers. If symbol_order is set to 'gray' (default) the function
%       uses gray ordering. If symbol ordering is set to bin the function
%       uses a natural binary order.
%
%   simpllr
%       Only for outputType 'llr'. Use a simplified LLR calculation to
%       calculate the soft bits (if available) (default = 1).
%   
%
% Output parameters:
%   z
%       Demodulated message. The values returned depend on the output type
%       specified in the outputType parameter, as shown in the following
%       table:
%       
%       +-----------+-----------------------------------------------------+
%       | ouputType | z                                                   |
%       +-----------+-----------------------------------------------------+
%       | integer   | Demodulated integer values from 0 to M-1            |
%       | bit       | Demodulated bits                                    |
%       | llr       | Approximate log-likelihood ratio value for each bit |
%       +-----------+-----------------------------------------------------+
%
%       TODO add some comments about the llr output
%

if nargin < 3
    outputType = 'integer';
end

if nargin < 4
    symbolOrder = 'gray';
end

if nargin < 5
    simplifiedllr = 1;
end

assert(mod(log2(M),1) == 0, 'M is not an integer power of 2.');

assert(ismember(outputType, {'integer', 'bit', 'llr'}),...
       'specified outputType is not valid.');


% Discard the imaginary part from the input
y = real(y);

% Demodulate bit message using a hard decision rule for a gray coding
% when M is 2, 4, or 8 and return
if strcmpi(outputType, 'bit') && strcmpi(symbolOrder, 'gray')
    if M == 2
        z = y >= 0;
        z = z(:).';
        return;
    elseif M == 4
        z = zeros(1, log2(M)*numel(y));
        z(1:2:end) = y >= 0;
        z(2:2:end) = y >= -2 & y <= 2;
        return;
    elseif M == 8
        z = zeros(1, log2(M)*numel(y));
        z(1:3:end) = y >= 0;
        z(2:3:end) = y >= -4 & y <= 4;
        z(3:3:end) = (y >= -6 & y <= -2) | (y >= 2 & y <= 6);
        return;
    end    
end


% Demodulate integer or bit message using a hard decision rule and return
if strcmpi(outputType, 'integer') || strcmpi(outputType, 'bit')
    z = round((y + M-1)./2);
    z(z > (M-1)) = M-1;
    z(z < 0) = 0;

    if strcmpi(symbolOrder, 'gray')
        z = FBMC_bin2gray(z, 'pam', M);
    end
    
    if strcmpi(outputType, 'bit')
        z = reshape(de2bi(z, 'left-msb').', 1, []);
    end
    
    return;
end

    
% Specific procedures to calculate soft bits when using gray mapping
% Reference:
% Filippo Tosato and Paola Bisaglia, "Simplified Soft-Output Demapper for
% Binary Interleaved COFDM with Application to HIPERLAN/2".
z = [];
y = y(:).'; % make y a row vector

if simplifiedllr && strcmpi(symbolOrder, 'gray')
    if M == 2
        z = y;
    elseif M == 4
        z = [y.', -abs(y.') + 2];
        
        z(y < -2, 1) = 2*(y(y < -2) + 1);
        z(y > 2, 1)  = 2*(y(y > 2) - 1);

        z = z.';
        z = z(:).';
    elseif M == 8
        z = zeros(numel(y), log2(M));
        
        % 1st bit
        z(abs(y) <= 2, 1) = real(y(abs(y) <= 2));
        z(y > 2 & y <= 4, 1) = 2*(real(y(y > 2 & y <= 4)) - 1);
        z(y > 4 & y <= 6, 1) = 3*(real(y(y > 4 & y <= 6)) - 2);
        z(y > 6, 1) = 4*(real(y(y > 6)) - 3);
        z(y >= -4 & y < -2, 1) = 2*(real(y(y >= -4 & y < -2)) + 1);
        z(y >= -6 & y < -4, 1) = 3*(real(y(y >= -6 & y < -4)) + 2);
        z(y < -6, 1) = 4*(real(y(y < -6)) + 3);
        
        % 2nd bit
        z(abs(y) <= 2, 2) = 2*(-abs(real(y(abs(y) <= 2))) + 3);
        z(abs(y) > 2 & abs(y) <= 6, 2) = 4 - abs(real(y(abs(y) > 2 & abs(y) <= 6)));
        z(abs(y) > 6, 2) = 2*(-abs(real(y(abs(y) > 6))) + 5);
        
        % 3rd bit
        z(abs(y) <= 4, 3) = abs(real(y(abs(y) <= 4))) - 2;
        z(abs(y) > 4, 3) = -abs(real(y(abs(y) > 4))) + 6;
        
        z = z.';
        z = z(:).';
    end
end


% General procedure to calculate soft bits
if isempty(z)
    % Construct matrices Sx0 and Sx1
    Sx0 = -inf(M/2, 1, log2(M));
    Sx1 = -inf(M/2, 1, log2(M));
    
    const = FBMC_pammod(0:M-1, M, symbolOrder);
    bits = de2bi(0:M-1, 'left-msb');
    
    for k = 0:log2(M)-1
        vx0 = unique(real(const(bits(:,k+1) == 0)));
        vx1 = unique(real(const(bits(:,k+1) == 1)));
            
        Sx0(1:numel(vx0), 1, k+1) = vx0;
        Sx1(1:numel(vx0), 1, k+1) = vx1;
    end
    
    Sx0_ext = repmat(Sx0, 1, numel(y), 1);
    Sx1_ext = repmat(Sx1, 1, numel(y), 1);
    
    min_x0 = squeeze(min(abs(bsxfun(@minus, y, Sx0_ext)), [], 1).^2);
    min_x1 = squeeze(min(abs(bsxfun(@minus, y, Sx1_ext)), [], 1).^2);
    
    z = (min_x0 - min_x1).';
    z = z(:).';
end

end
