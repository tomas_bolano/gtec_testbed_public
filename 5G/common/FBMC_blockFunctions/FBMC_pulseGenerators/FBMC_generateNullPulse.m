function [TxPulse, RxPulse] = FBMC_generateNullPulse(~)
%%FBMC_GENERATENULLPULSE Generate emptypulses.
%
% See also FBMC_pulseGenerators

TxPulse = [];
RxPulse = [];

end

