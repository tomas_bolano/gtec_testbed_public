function [TxPulse, RxPulse] = FBMC_generateOFDMPulse(FBMC_parameters)
%FBMC_GENERATEOFDMPULSE Generate pulses for OFDM.
%
% The OFDM pulses are just square pulses. The pulses for transmission and
% reception (txPulse and rxPulse) are the same.
%
% See also FBMC_pulseGenerators

%% Parameters extraction

OversamplingT=FBMC_parameters.pulseShapping.overlappingFactor;
T=FBMC_parameters.basicParameters.subcarriersNumber*FBMC_parameters.signalGeneration.dt;
dt=FBMC_parameters.signalGeneration.dt;
N=round(T/dt);

%% Generation of TX and RX Pulses

Length = length(-OversamplingT*T:dt:OversamplingT*T);
TxPulse = zeros([Length 1]);
middlepoint = (Length+1)/2;
TxPulse(middlepoint-N/2+1:middlepoint+N/2)=1;
TxPulse = TxPulse/sqrt(sum(TxPulse.*TxPulse)*dt);
RxPulse = TxPulse;