function [txPulse, rxPulse] = FBMC_generateSRRCPulse(FBMC_parameters, rolloff)
%FBMC_GENERATESRRCPULSE Generate a square-root-raised-cosine pulse.
%
% The pulses for transmission and reception (txPulse and rxPulse) are the same.
% If the rolloff is not specified a default of 0.2 will be used.
%
% See also FBMC_pulseGenerators

if nargin < 2
    rolloff = 0.2;
end

%% Parameters extraction
K = FBMC_parameters.pulseShapping.overlappingFactor;
S = FBMC_parameters.basicParameters.subcarriersNumber;


%% Generation of TX and RX Pulses

L = K*S;
t = (round(-L/2):round(L/2)-1)/S; % t/Ts (Ts: simbol period)

txPulse = (sin(pi*t*(1-rolloff)) + 4*rolloff*t.*cos(pi*t*(1+rolloff)))./...
    (pi*t.*(1-(4*rolloff*t).^2));
txPulse(t == 0) = (1-rolloff+4*rolloff/pi);
txPulse(abs(t) == 1/(4*rolloff)) = rolloff/sqrt(2)*(...
    (1+2/pi)*sin(pi/(4*rolloff))+(1-2/pi)*cos(pi/(4*rolloff)));
txPulse = 1/sqrt(S)*txPulse.';

rxPulse = txPulse;

end

