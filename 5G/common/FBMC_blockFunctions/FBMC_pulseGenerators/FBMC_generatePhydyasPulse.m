function [txPulse, rxPulse] = FBMC_generatePhydyasPulse(FBMC_parameters)
%FBMC_GENERATEPHYDYASPULSE Generate the PHYDYAS pulse.
%
% The pulses for transmission and reception (txPulse and rxPulse) are the same.
%
% See also FBMC_pulseGenerators


%% Parameters extraction

K = FBMC_parameters.pulseShapping.overlappingFactor;
S = FBMC_parameters.basicParameters.subcarriersNumber;

%% Generation of TX and RX Pulses

switch K
    case 3, Hcoef = [1 0.911438 0.411438];
    case 4, Hcoef = [1 0.971960 sqrt(2)/2 0.235147];
    otherwise, error('invalid value of pulseShapping.overlappingFactor parameter (%d)', K);
end

L = K*S;
txPulse = ones(1, L)/L;
for k = 1:K-1
    txPulse = txPulse + 2/L*(-1)^k*Hcoef(k+1)*cos(2*pi*k*(0:L-1)/(L));
end
txPulse = txPulse.'*sqrt(1/(txPulse*txPulse.')); % normalize pulse energy
rxPulse = txPulse;

end

