function [TxPulse, RxPulse] = FBMC_generateOFDMCPPulse(FBMC_parameters)
%FBMC_GENERATEOFDMCPPULSE Generate pulses for OFDM with cyclic prefix.
%
% The OFDM pulses are just square pulses.
%
% See also FBMC_pulseGenerators


%% Parameters extraction

OversamplingT=FBMC_parameters.pulseShapping.overlappingFactor;
T=FBMC_parameters.basicParameters.subcarriersNumber*FBMC_parameters.signalGeneration.dt;
dt=FBMC_parameters.signalGeneration.dt;
CP_L = FBMC_parameters.pulseShapping.cyclicPrefixLength;
N=round(T/dt);

%% Generation of RX Pulse

Length = length(-OversamplingT*T:dt:OversamplingT*T);
RxPulse = zeros([Length 1]);
middlepoint = (Length+1)/2;
RxPulse(middlepoint-N/2+1:middlepoint+N/2)=1;
RxPulse = RxPulse/sqrt(sum(RxPulse.*RxPulse)*dt);

%% Generation of TX Pulse

middlepoint = (Length+1)/2;
TxPulse = zeros([Length 1]);

TxPulse(round(middlepoint-N/2+1)-CP_L:round(middlepoint+N/2))=1; %%%%%% novo
TxPulse = TxPulse/sqrt(N*dt);