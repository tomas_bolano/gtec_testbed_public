function [TxPulse, RxPulse] = FBMC_generateHermitePulse(FBMC_parameters)
%FBMC_GENERATEHERMITEPULSE Generate the Hermite pulse.
%
% The pulses for transmission and reception (txPulse and rxPulse) are the same.
%
% See also FBMC_pulseGenerators


%% Parameters extraction

OversamplingT=FBMC_parameters.pulseShapping.overlappingFactor;
T=FBMC_parameters.basicParameters.subcarriersNumber*FBMC_parameters.signalGeneration.dt;
dt=FBMC_parameters.signalGeneration.dt;

t_filter = -OversamplingT*T:dt:OversamplingT*T;

%% Generation of TX and RX Pulses

% Procedure according to the paper of Haas et.al.
D0=FBMC_computeHermitePolynomials(0,sqrt(2*pi)*(t_filter./(T/sqrt(2)))).*exp(-pi*(t_filter./(T/sqrt(2))).^2);
D4=FBMC_computeHermitePolynomials(4,sqrt(2*pi)*(t_filter./(T/sqrt(2)))).*exp(-pi*(t_filter./(T/sqrt(2))).^2);
D8=FBMC_computeHermitePolynomials(8,sqrt(2*pi)*(t_filter./(T/sqrt(2)))).*exp(-pi*(t_filter./(T/sqrt(2))).^2);
D12=FBMC_computeHermitePolynomials(12,sqrt(2*pi)*(t_filter./(T/sqrt(2)))).*exp(-pi*(t_filter./(T/sqrt(2))).^2);
D16=FBMC_computeHermitePolynomials(16,sqrt(2*pi)*(t_filter./(T/sqrt(2)))).*exp(-pi*(t_filter./(T/sqrt(2))).^2);
H0=1.1850899;
H4=-1.9324881e-3*H0;
H8=-7.3110588e-6 *H0;
H12=-3.1542096e-9 * H0;
H16=9.6634138e-13 * H0;
TxPulse = D0.*H0+D4.*H4+D8.*H8+D12.*H12+D16.*H16;
%TxPulse = TxPulse'/sqrt(sum(TxPulse.*TxPulse)*dt);
TxPulse = TxPulse.'*sqrt(1/(TxPulse*TxPulse.')); % normalize pulse energy
RxPulse = TxPulse;

