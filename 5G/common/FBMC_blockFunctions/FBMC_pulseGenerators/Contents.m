% FBMC_PULSEGENERATORS Pulse generation functions
%
% This directory includes functions used to generate pulses for transmission
% and reception. Each function will create different pulses.
% See each file for further details.
%
% You can use the scripts FBMC_plotPulses and FBMC_ambiguityPlot in the 
% FBMC_misc directory to plot the time/frequency responses and ambiguity
% functions of the pulses.
%
%
% Interface of the pulse generation functions 
% ----------------------------------------------
%
% All the pulse generation functions have the same interface, which is:
%
% [txPulse, rxPulse] = FBMC_generatePulse(FBMC_parameters, param1, ..., paramN)
%
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   param1, ..., paramN
%       Aditional specific parameters for each function.
%
%
% Output parameters: 
%   txPulse
%       Pulse for the transmitter.
%
%   rxPulse
%       Pulse for the receiver.
%

