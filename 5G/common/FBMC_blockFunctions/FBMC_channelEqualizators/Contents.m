% FBMC_CHANNELEQUALIZATORS Channel equalization functions
%
% This directory includes functions used to equalize the received signal.
% See each file for further details.
%
% Interface of the channel equalization functions 
% -----------------------------------------------
%
% All the channel equalization coding functions have the same interface,
% which is:
%
% FBMC_equalizedDataGrid = FBMC_channelEqualizator(FBMC_parameters,...
%       FBMC_pregeneratedConstants, FBMC_channelEstimation,...
%       FBMC_rxActualDataGrid, no, param1, ..., paramN)
%
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_pregeneratedConstants
%       FBMC pregenerated Constants.
%
%   FBMC_channelEstimation
%       Grid of the estimated channel coefficients, as returned by the
%       channel estimation functions
%
%   FBMC_rxActualDataGrid
%       Grid of the data to equalizate.
%
%   no
%       Power espectral density of the noise
%
%   param1, ..., paramN
%       Aditional specific parameters for each function.
%
%
% Output parameters: 
%   FBMC_equalizedDataGrid
%       Grid of the equalized data.
%
