function FBMC_equalizedDataGrid = FBMC_nullChannelEqualizator(~,~, ~,...
    FBMC_rxActualDataGrid, ~)
%FBMC_NULLCHANNELEQUALIZATOR Do not equalizate. Returns the same input data grid.
%

FBMC_equalizedDataGrid = FBMC_rxActualDataGrid;

end

