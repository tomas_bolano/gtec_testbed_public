function FBMC_equalizedDataGrid = FBMC_MMSEChannelEqualizator(...
    ~, FBMC_constants, FBMC_channelEstimation, FBMC_rxActualDataGrid, no)
%FBMC_ZFCHANNELEQUALIZATOR Zero forzing equalizator
%

if nargin < 4 || isempty(no)
    error('noise spectral density ("no") not specified');
end

subcmask = FBMC_constants.usedSubcarriers.subcarrierMask;
nused = nnz(subcmask);

% Assume all the symbols are normalized (mean energy 1)
% In that case the covariance matrix of the symbols will be the identity
C = speye(nused);
No = speye(nused)*no;

usedDataGrid = FBMC_rxActualDataGrid(subcmask,:);
usedChannelEst = FBMC_channelEstimation(subcmask,:);
equalizedData = zeros(size(usedDataGrid));

% I dont know why but the equalizator works best with No/2
% with No it performs worst than the zero forzing one.
for ii = 1:size(FBMC_rxActualDataGrid,2)
    H = spdiags(usedChannelEst(:,ii), 0, nused, nused);
    x = usedDataGrid(:,ii);
    equalizedData(:,ii) =  C*H'*((H*C*H'+No/2)\x);
end

FBMC_equalizedDataGrid = zeros(size(FBMC_rxActualDataGrid));
FBMC_equalizedDataGrid(subcmask,:) = equalizedData;

end

