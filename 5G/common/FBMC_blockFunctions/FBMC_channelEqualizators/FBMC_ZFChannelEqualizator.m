function FBMC_equalizedDataGrid = FBMC_ZFChannelEqualizator(~,~,...
    FBMC_channelEstimation, FBMC_rxActualDataGrid, ~)
%FBMC_ZFCHANNELEQUALIZATOR Zero forzing equalizator
%

FBMC_equalizedDataGrid = FBMC_rxActualDataGrid./FBMC_channelEstimation;

end

