function [FBMC_usedSubcarriers] = FBMC_getUsedSubcarriers(FBMC_parameters)
%FBMC_GETUSEDSUBCARRIERS Generate data about the subcarriers.
%
% [FBMC_usedSubcarriers] = FBMC_getUsedSubcarriers(FBMC_parameters)
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%
% Output parameters:
%   FBMC_USEDSUBCARRIERS
%       Structure with the following fields:
%           * guardMask
%               Logical Nx1 vector, where N is the number of subcarriers. Mask
%               of the guard subcarriers.
%
%           * DCIndex
%               Index of the DC subcarrier
%
%           * DCMask
%               Logical Nx1 vector, where N is the number of subcarriers. Mask
%               of the DC subcarrier.
%
%           * subcarrierMask
%               Logical Nx1 vector, where N is the number of subcarriers. Mask
%               of the subcarriers used.
%
%           * subcarrierNumbers
%               Numerical Nx1 vector, where N is the number of subcarriers.
%               Each element indicates the index of the corresponding
%               subcarrier, from 1 (DC frequency) to the number of subcarriers
%               (2*pi-1/N frequency).
%
%           * subcarrierRelativeNumbers
%               Numerical Nx1 vector, where N is the number of subcarriers.
%               Each element indicates the index of the corresponding
%               subcarrier relative to the DC component, where the DC is
%               the 0, from -N/2 to N/2-1.
%
%           * subcarrierFullMask
%               Logical NxM matrix, where N is the number of subcarriers and M
%               the number of time symbols. Mask of the used frequency-time
%               elements of the grid.
%
%           * subcarrierXGrid
%               Subcarriers used time domain indexes matrix in meshgrid format,
%               or an empty matrix if the pilots can't be represented in a
%               meshgrid format.
%
%           * subcarrierYGrid
%               Subcarriers used frequency domain indexes matrix in meshgrid
%               format, or an empty matrix if the pilots can't be represented
%               in a meshgrid format.
%           

% TODO maybe change the name of the function because we now are returned
% also some data wich includes the time domain.

%% Parameters extraction

Snum = FBMC_parameters.basicParameters.subcarriersNumber;
Dnum = FBMC_parameters.basicParameters.timeSymbolsNumber;
guardNum = FBMC_parameters.frameGeneration.sideGuardSubcarriers;
DCNull = FBMC_parameters.frameGeneration.insertDCNull;

assert(ismember(numel(guardNum), [1 2]),...
       'FBMC_parameters.frameGeneration.sideGuardSubcarriers must have 1 or 2 elements');
assert(all(guardNum < Snum),...
       ['The elements of FBMC_parameters.frameGeneration.sideGuardSubcarriers must '...
        'be less than FBMC_parameters.basicParameters.subcarriersNumber']);

%% Calculate and return the grid data

if numel(guardNum) == 1
    guardNum = repmat(guardNum, 1, 2);
end

FBMC_usedSubcarriers.guardMask = false(Snum,1);
FBMC_usedSubcarriers.guardMask(1:guardNum(1)) = true;
FBMC_usedSubcarriers.guardMask(end+1-guardNum(2):end) = true;

FBMC_usedSubcarriers.DCIndex = ceil((Snum+1)/2);
FBMC_usedSubcarriers.DCMask = false(Snum,1);
FBMC_usedSubcarriers.DCMask(FBMC_usedSubcarriers.DCIndex) = true;

FBMC_usedSubcarriers.subcarrierMask = ~FBMC_usedSubcarriers.guardMask;
FBMC_usedSubcarriers.subcarrierNumbers = ifftshift(1:Snum).';

FBMC_usedSubcarriers.subcarrierRelativeNumbers = ifftshift([(0:Snum/2-1) (-Snum/2:1:-1)]).';

if DCNull
    FBMC_usedSubcarriers.subcarrierMask(FBMC_usedSubcarriers.DCIndex) = false;
end

assert(any(FBMC_usedSubcarriers.subcarrierMask),...
       'There is no subcarriers in use with this configuration.');

FBMC_usedSubcarriers.subcarrierFullMask = false(Snum,Dnum);
FBMC_usedSubcarriers.subcarrierFullMask(FBMC_usedSubcarriers.subcarrierMask,:) = 1;

[subcarrierXGrid, subcarrierYGrid] = ...
    meshgrid(1:Dnum, find(FBMC_usedSubcarriers.subcarrierMask));

FBMC_usedSubcarriers.subcarrierXGrid = subcarrierXGrid;
FBMC_usedSubcarriers.subcarrierYGrid = subcarrierYGrid;

end

