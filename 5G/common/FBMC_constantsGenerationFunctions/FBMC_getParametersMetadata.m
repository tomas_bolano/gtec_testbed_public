function [FBMC_ParametersMetadata] = FBMC_getParametersMetadata(FBMC_parameters)
%FBMC_GETPARAMETERSMETADATA Generate metadata about the FBMC_parameters.
%
% [FBMC_ParametersMetadata] = FBMC_getParametersMetadata(FBMC_parameters)
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
% Output parameters:
%   FBMC_ParametersMetadata
%       Structure with the following fields:
%           * systemType
%               'OFDM' or 'SMT', inferred from the modulator used.
%           * systemSubtype
%               Empty string for OFDM. For SMT this field indicates the type of
%               pulse used, possible values are 'Hermite' and 'PHYDYAS'.
%           * systemDesc
%               Description of the system. Concatenation of systemType and
%               systemSubtype separated by space.
%           * systemId
%               Concatenation of systemType and systemSubtype separated by '_'.
%               This identifies the system type completely.
%           * channel
%               String of the channel used.
%


%% Get the system type

% cell of pairs {modulator function, system type}
modulatorFunctions = {@FBMC_ofdmcpModulator, 'OFDM',;
                      @FBMC_ofdmcpModulator_2, 'OFDM';
                      @FBMC_ofdmModulator, 'OFDM';
                      @FBMC_smtModifiedModulator, 'SMT'
                      @FBMC_smtModulator, 'SMT'};
            
modulatorFun = FBMC_parameters.modulator.modulatorFunction{1};

systemType = '';
for i = 1:length(modulatorFunctions)
    if isequal(modulatorFunctions{i,1}, modulatorFun)
        systemType = modulatorFunctions{i,2};
        break;
    end
end

if (isempty(systemType))
    warning('Could not infer the system type');
end


%% If the system is SMT get the type of SMT used according to the pulse

% cell of pairs {pulse function, SMT system type}
pulseFunctions = {@FBMC_generateHermitePulse, 'Hermite';
                  @FBMC_generatePhydyasPulse, 'PHYDYAS'
                  @FBMC_generateSRRCPulse, 'SRRC'};

pulseFun = FBMC_parameters.pulseShapping.pulseGenerationFunction{1};

systemSubtype = '';
if strcmp(systemType, 'SMT')
    for i = 1:length(pulseFunctions)
        if isequal(pulseFunctions{i,1}, pulseFun)
            systemSubtype = pulseFunctions{i,2};
            break;
        end
    end
    
    if (isempty(systemSubtype))
        warning('Could not infer the SMT system type');
    end
end


%% Generate system id (concatenate type and subtype)

if isempty(systemSubtype)
    systemDesc = systemType;
    systemId = systemType;
else
    systemDesc = [systemType ' ' systemSubtype];
    systemId = [systemType '_' systemSubtype];
end


%% Get the channel used
channel = FBMC_channel2str(FBMC_parameters);


%% Assign output parameters

FBMC_ParametersMetadata.systemType = systemType;
FBMC_ParametersMetadata.systemSubtype = systemSubtype;
FBMC_ParametersMetadata.systemDesc = systemDesc;
FBMC_ParametersMetadata.systemId = systemId;
FBMC_ParametersMetadata.channel = channel;


end

