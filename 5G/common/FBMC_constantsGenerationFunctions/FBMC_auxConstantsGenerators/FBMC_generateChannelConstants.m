function FBMC_channelConstants = FBMC_generateChannelConstants(FBMC_parameters)
%FBMC_GENERATECHANNELCONSTANTS Generates the channel model constants.
%
% This functions generates the channel related constants from the parameters.
%
% It uses the following parameters fields:
%    FBMC_parameters.channelModel.relativeSpeed
%    FBMC_parameters.channelModel.carrierFrequency
%    FBMC_parameters.channelModel.channelGenerationFunctions
%    FBMC_parameters.signalGeneration.dt
%    FBMC_parameters.channelModel.stdchanChannelModel.Type
%

iscellmember = @(A,B) any(cellfun(@(x) isequal(x,A), B));

FBMC_channelConstants.maximumDopplerShift = ...
    FBMC_parameters.channelModel.relativeSpeed*...
    FBMC_parameters.channelModel.carrierFrequency/3e8;

if iscellmember(@FBMC_stdchanChannelModel,...
                FBMC_parameters.channelModel.channelGenerationFunctions)
    channelObject = stdchan(FBMC_parameters.signalGeneration.dt,...
        FBMC_channelConstants.maximumDopplerShift,....
        FBMC_parameters.channelModel.stdchanChannelModel.Type);
    channelObject.NormalizePathGains = 1;

    FBMC_channelConstants.stdchanChannelObject = channelObject;
    
elseif iscellmember(@FBMC_rayleighChannelModel,...
                FBMC_parameters.channelModel.channelGenerationFunctions)
    channelObject = rayleighchan(FBMC_parameters.signalGeneration.dt,...
        FBMC_channelConstants.maximumDopplerShift,....
        FBMC_parameters.channelModel.pathDelays*FBMC_parameters.signalGeneration.dt,...
        FBMC_parameters.channelModel.averagePathGain);
    channelObject.NormalizePathGains = 1;
    
    channelObject.StoreHistory=1;
    channelObject.ResetBeforeFiltering=0;

    FBMC_channelConstants.rayleighchanChannelObject = channelObject; % TODO: comentar con Tomás se cambiar isto!!
            
end

end

