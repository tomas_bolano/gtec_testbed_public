function FBMC_interferenceConstants = FBMC_generateInterferenceConstants(...
    FBMC_parameters, FBMC_pilotData, interfMat, interfSignMat)
%FBMC_GENERATEINTERFERENCECONSTANTS Generate aux. pilot and interference data.
%
% FBMC_interferenceConstants = FBMC_generateInterferenceConstants(...
%       FBMC_parameters, FBMC_pilotData, interfMat, interfSignMat)
%
% Creates several data needed for the auxiliary pilots methods and returns
% it in FBMC_interferenceConstants as a struct.
%
% Input parameters:
%
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_pilotData
%       struct of pilot data containing (at least) the following fields, as
%       returned by the pilot indexer functions (<a href="matlab:help FBMC_PILOTINDEXERS">FBMC_pilotIndexers</a>):
%           * NonZeroPilotMask
%           * ActualPilotMask
%           * ZeroPilotMask
%
%   interfMatx
%       Matrix of interferences caused by symbols over a pilot for a pilot in
%       odd time, odd freq, as returned by the function <a href="matlab:help FBMC_GENERATEINTERFERENCEMATRIX_2">FBMC_generateInterferenceMatrix_2</a>.
%       
%
%   interfSignMat
%       3-dimensional matrix of signs for the interfMat interference matrix, as
%       returned by the function <a href="matlab:help FBMC_GENERATEINTERFERENCEMATRIX_2">FBMC_generateInterferenceMatrix_2</a>.
%
%
% Output parameters:
%
%   FBMC_interferenceConstants
%       Structure containing the required interference data for the SMT
%       transmitter and receiver to use the auxiliary pilots method. Contains
%       the following fields:
%           * interfMatrix
%               interference Matrix. Same as the interfMatrix input.
%
%           * interfSignMatrix 
%               3-dimensional matrix of signs for the interference matrix. Same
%               as the interfSignMatrix input.
%
%           * interfMatrixSigned
%               3-dimensional matrix result of multiplying element by element
%               each 2D matrix of interfSignMatrix by the interference matrix.
%
%           * auxPilotMask
%               3x3 auxiliary pilot submask. The matrix that will be returned
%               is [0 0 0; 1 0 0; 0 0 0], where the center element corresponds
%               to the pilot and the element set to one indicates the position
%               reserved to calculate the auxiliary pilot or coded auxiliary
%               pilots. When using a single auxiliary pilot this position will
%               be the position of the auxiliary pilot itself.
%
%           * auxPilotInterfMask
%               This is the auxPilotMask matrix with zeros added around it to
%               be of the same size as interfMatrix.
%
%           * auxPilotCodedMask
%               3x3 submask of the coded auxiliary pilots when using the Coded
%               Auxiliary Pilot (CAP) method, else it will be the same as
%               auxPilotMask. The element in the center corresponds to the
%               pilot position and the elements set to one indicates the
%               position of the coded pilots (or the only pilot).
%
%           * auxPilotInterfCodedMask
%               This is the auxPilotCodedMask matrix with zeros added around it
%               to be of the same size as interfMatrix.
%
%           * auxPilotUncodedMask
%               3x3 submask of the uncoded data when using the Coded Auxiliary
%               Pilot (CAP) method, else it will be an all zeros matrix. The
%               element in the center corresponds to the pilot position and the
%               elements set to one indicates the data around the pilot that
%               will be used for the CAP method. In case the CAP method is not
%               used then no data is required and that is why in this case the
%               matrix will be all zeros.
%
%           * auxPilotInterfUncodedMask
%               This is the auxPilotUncodedMask matrix with zeros added around
%               it to be of the same size as interfmat.
%
%           * codingMatrix
%               3-dimensional matrix of the codification matrices for the
%               Coding Auxiliary Pilot (CAP) method.
%
%           * pilotCoord
%               Coordinates of the pilots with format [X Y], where X and Y are
%               column vectors.
%
%           * auxPilotFullMask
%               Mask of the positions for the full grid that must be zero to
%               add later the auxiliary pilots.
%
%           * auxPilotFullCodedMask
%                When using the CAP method this mask indicates the positions of
%                the coded auxiliary pilots for the full grid. When using a
%                single auxiliry pilot this mask indicates the position of the
%                auxiliary pilotss for the full grid.
%
%           * pilotTypeInd
%               1xN vector where N is the number of pilots. Each element
%               corresponds to the coordinate indicated in pilotCoord and
%               specifies the 3rd dimension index of the interfSignMatrix,
%               interfMatrixSigned or codingMatrix matrices for the
%               corresponding pilot.
%
%           * dataLimits
%               Matrix of index limits of the data grid for each row of
%               pilotCoord. Each row is of the form [xmin xmax ymin ymax].
%
%           * interfLimits
%               Matrix of index limits of the interfMat matrix for each row of
%               pilotCoord. Each row is of the form [xmin xmax ymin ymax].
%
%
% See also FBMC_PILOTINDEXERS, FBMC_GENERATEINTERFERENCEMATRIX, FBMC_GENERATEINTERFERENCEMATRIX_2


%% Parameters extraction

Snum = FBMC_parameters.basicParameters.subcarriersNumber;
Dnum = FBMC_parameters.basicParameters.timeSymbolsNumber;
%guardNum = FBMC_parameters.frameGeneration.sideGuardSubcarriers;
auxPilotNum = FBMC_parameters.pilots.numberAuxiliaryPilots;
%interfMatFile = FBMC_parameters.pilots.interferencePilotMatrixFilename;
%interfSignFile = FBMC_parameters.pilots.interferencePilotSignMatrixFilename;

% values for pilots
nonZeroPilotMask = FBMC_pilotData.NonZeroPilotMask;
actualPilotMask = FBMC_pilotData.ActualPilotMask;
zeroPilotMask = FBMC_pilotData.ZeroPilotMask;

% load interference matrices
%load(interfMatFile);
%load(interfSignFile);

% rename interference matrices
%interfMat = interferenceMatrix;
%interfSignMat = interferenceSignMatrix;

sizeInterfMat = size(interfMat);
sizeInterfSignMat = size(interfSignMat);

assert(all(mod(sizeInterfMat, 2)),...
       'The dimensions of interfMat must be odd');
assert(all(sizeInterfMat >= [3 3]),...
       'interfMat must be bigger than 3x3');
assert(all(sizeInterfMat == sizeInterfSignMat([1 2])),...
       ['interfMat and interfSignMat must ',...
        'have the same number of rows and colums']);
assert(numel(sizeInterfSignMat) == 3 && sizeInterfSignMat(3) == 4,...
       ['interfSignMat must be a 3-dimensional matrix ',...
        'with 4 elements in the dimension 3']);


%% Calculate and return the auxiliary pilot data

interfMatCentCol = (size(interfMat,2)-1)/2+1; % center column of interfMat
interfMatCentRow = (size(interfMat,1)-1)/2+1; % center row of the interfMat
interfMatOffCols = (1:size(interfMat,2))-interfMatCentCol; % offset cols from the center
interfMatOffRows = (1:size(interfMat,1))-interfMatCentRow; % offset rows from the center


% interfMatrix and interfSignMatrix
FBMC_interferenceConstants.interfMatrix = interfMat;
FBMC_interferenceConstants.interfSignMatrix = interfSignMat;


% interfMatrixSigned
interfMatSigned = repmat(interfMat, [1 1 4]).*interfSignMat;
FBMC_interferenceConstants.interfMatrixSigned = interfMatSigned;


% auxPilotMask and auxPilotInterfMask
auxPilotMask = logical([0 0 0; 1 0 0; 0 0 0]);
auxPilotInterfMask = false(size(interfMat));
auxPilotInterfMask((-1:1)+interfMatCentRow, (-1:1)+interfMatCentCol) = auxPilotMask;

FBMC_interferenceConstants.auxPilotMask = auxPilotMask;
FBMC_interferenceConstants.auxPilotInterfMask = auxPilotInterfMask;


% auxPilotCodedMask / auxPilotInterfCodedMask
auxPilotCodedMask = auxPilotMask;
switch auxPilotNum
    case 1 % same as auxPilotMask matrix
    case 2
        auxPilotCodedMask(2, [1 3]) = true;
    case 4
        auxPilotCodedMask(2, [1 3]) = true;
        auxPilotCodedMask([1 3], 2) = true;
    case 8
        auxPilotCodedMask(1:3, 1:3) = true;
        auxPilotCodedMask(2, 2) = false;
    otherwise
        error('FBMC_parameters.pilots.numberAuxiliaryPilots must be 0, 1, 2, 4 or 8');
end

% IMPORTANT CHECK
% the element set as one in auxPilotMask (position reserved to calculate the
% auxiliary pilot or coded auxiliary pilots) must be also in auxPilotCodedMask.
assert(any(any(and(auxPilotCodedMask,auxPilotMask))),...
    'The boolean value set in auxPilotMask must be also in auxPilotCodedMask');

auxPilotInterfCodedMask = false(size(interfMat));
auxPilotInterfCodedMask((-1:1)+interfMatCentRow, (-1:1)+interfMatCentCol) = auxPilotCodedMask;

FBMC_interferenceConstants.auxPilotCodedMask = auxPilotCodedMask;
FBMC_interferenceConstants.auxPilotInterfCodedMask = auxPilotInterfCodedMask;


% auxPilotUncodedMask / auxPilotInterfUncodedMask
auxPilotUncodedMask = xor(auxPilotMask, auxPilotCodedMask);
auxPilotInterfUncodedMask = xor(auxPilotInterfMask, auxPilotInterfCodedMask);

FBMC_interferenceConstants.auxPilotUncodedMask = auxPilotUncodedMask;
FBMC_interferenceConstants.auxPilotInterfUncodedMask = auxPilotInterfUncodedMask;


% codingMatrix
if auxPilotNum > 1
    C = zeros(auxPilotNum, auxPilotNum-1, 4); % coding Matrix
    
    for i = 1:4
        g = interfMatSigned(interfMatCentRow+(-1:1),interfMatCentCol+(-1:1),i);
        g = g([2 8 4 6 1 3 7 9]); % interference coefficients
        
        switch auxPilotNum
            case 2
                C(1,1,i) = sqrt(1/(1+g(1)^2/g(2)^2));
                C(2,1,i) = -g(1)/g(2)*C(1,1,i);
            case 4
                C(1,1,i) = sqrt(1/(1+g(1)^2/g(2)^2));
                C(2,1,i) = -g(1)/g(2)*C(1,1,i);

                C(3,2,i) = sqrt(1/(1+g(3)^2/g(4)^2));
                C(4,2,i) = -g(3)/g(4)*C(3,2,i);

                a = -g(4)*(g(1)^2+g(2)^2)/(g(1)*(g(4)^2+g(3)^2));
                C(1,3,i) = sqrt(1/(1+g(2)^2/g(1)^2+a^2*(1+g(3)^2/g(4)^2)));
                C(2,3,i) = g(2)/g(1)*C(1,3,i);
                C(3,3,i) = g(3)/g(4)*a*C(1,3,i);
                C(4,3,i) = a*C(1,3,i);
                
                C(:,:,i) = C([1 3 4 2],:,i); % reorder rows
            case 8
                C(1,1,i) = sqrt(1/(1+g(1)^2/g(2)^2));
                C(2,1,i) = -g(1)/g(2)*C(1,1,i);

                C(3,2,i) = +sqrt(1/(1+g(3)^2/g(4)^2));
                C(4,2,i) = -g(3)/g(4)*C(3,2,i);

                C(5,3,i) = +sqrt(1/(1+g(5)^2/g(6)^2));
                C(6,3,i) = -g(5)/g(6)*C(5,3,i);

                C(7,4,i) = +sqrt(1/(1+g(7)^2/g(8)^2));
                C(8,4,i) = -g(7)/g(8)*C(7,4,i);

                a = -g(4)*(g(1)^2+g(2)^2)/(g(1)*(g(4)^2+g(3)^2));
                C(1,5,i) = sqrt(1/(1+g(2)^2/g(1)^2+a^2*(1+g(3)^2/g(4)^2)));
                C(2,5,i) = g(2)/g(1)*C(1,5,i);
                C(3,5,i) = g(3)/g(4)*a*C(1,5,i);
                C(4,5,i) = a*C(1,5,i);

                b = -g(8)*(g(5)^2+g(6)^2)/(g(5)*(g(8)^2+g(7)^2));
                C(5,6,i) = sqrt(1/(1+g(6)^2/g(5)^2+b^2*(1+g(7)^2/g(6)^2)));
                C(6,6,i) = g(6)/g(5)*C(5,6,i);
                C(7,6,i) = g(7)/g(8)*b*C(5,6,i);
                C(8,6,i) = b*C(5,6,i);

                n = -g(8)/g(1)*sum(g(1:4).^2)/sum(g(5:8).^2);
                C(1,7,i) = sqrt(1/(1+g(2)^2/g(1)^2+g(4)^2/g(1)^2*(1+g(4)^2/g(3)^2)+...
                                n^2*(1+(g(5)^2+g(6)^2+g(7)^2)/g(8)^2)));
                C(2,7,i) = g(2)/g(1)*C(1,7,i);
                C(3,7,i) = g(4)^2/(g(3)*g(1))*C(1,7,i);
                C(4,7,i) = g(4)/g(1)*C(1,7,i);
                C(5,7,i) = g(5)/g(8)*n*C(1,7,i);
                C(6,7,i) = g(6)/g(8)*n*C(1,7,i);
                C(7,7,i) = g(8)/g(7)*n*C(1,7,i);
                C(8,7,i) = n*C(1,7,i);

                C(:,:,i) = C([5 1 6 3 4 7 2 8],:,i); % reorder rows
        end
    end
    codingMatrix = C;
else
    codingMatrix = [];
end

FBMC_interferenceConstants.codingMatrix = codingMatrix;


% pilotCoord
[pilotYCoord, pilotXCoord] = ind2sub(size(actualPilotMask), find(actualPilotMask));
pilotCoord = [pilotXCoord pilotYCoord];
FBMC_interferenceConstants.pilotCoord = pilotCoord;

% check that the auxiliary pilots can be added to the current pilot pattern
% maybe these conditions can be less strict in some cases but doing so might
% require to change a lot of code.
assert(all(pilotXCoord > 1),...
    ['Could not add auxiliary pilots to the current pilot scheme, ',...
     'there are pilots on the first time symbol']);
 
assert(all(pilotXCoord < size(actualPilotMask,2)),...
    ['Could not add auxiliary pilots to the current pilot scheme, ',...
     'there are pilots on the last time symbol']);
 
assert(all(pilotYCoord > 1),...
    ['Could not add auxiliary pilots to the current pilot scheme, ',...
     'there are pilots on the first subcarrier']);
 
assert(all(pilotYCoord < size(actualPilotMask,1)),...
    ['Could not add auxiliary pilots to the current pilot scheme, ',...
     'there are pilots on the last subcarrier']);


% auxPilotFullUncodedMask / auxPilotFullCodedMask
auxPilotFullMask = logical(sparse(size(actualPilotMask,1),size(actualPilotMask,2)));
auxPilotFullCodedMask = logical(sparse(size(actualPilotMask,1),size(actualPilotMask,2)));
for i = 1:size(pilotCoord,1)
    auxPilotFullMask((-1:1)+pilotCoord(i,2), (-1:1)+pilotCoord(i,1)) = ...
        auxPilotMask;
    auxPilotFullCodedMask((-1:1)+pilotCoord(i,2), (-1:1)+pilotCoord(i,1)) = ...
        auxPilotCodedMask;
end
FBMC_interferenceConstants.auxPilotFullMask = auxPilotFullMask;
FBMC_interferenceConstants.auxPilotFullCodedMask = auxPilotFullCodedMask;

% check that the uncoded/coded pilots can be added to the current pilot pattern
assert(any(any(auxPilotFullCodedMask & zeroPilotMask)) == 0,...
    ['Could not add auxiliary pilots to the current pilot scheme, ',...
     'auxiliary pilots and zeros positions conflict']);
assert(any(any(auxPilotFullCodedMask & nonZeroPilotMask)) == 0,...
    ['Could not add auxiliary pilots to the current pilot scheme, ',...
     'auxiliary pilots and non zeros positions conflict']);


% pilotPosType
pilotTypeInd = sum(bsxfun(@times, [2 1], mod(pilotCoord,2)),2)+1;
FBMC_interferenceConstants.pilotTypeInd = pilotTypeInd;


% dataLimits
dataLimits = [max(1, pilotCoord(:,1)+min(interfMatOffCols))...
              min(Dnum, pilotCoord(:,1)+max(interfMatOffCols))...
              max(1, pilotCoord(:,2)+min(interfMatOffRows))...
              min(Snum, pilotCoord(:,2)+max(interfMatOffRows))];
FBMC_interferenceConstants.dataLimits = dataLimits;


% interfLimits
interfLimits = [max(1, interfMatCentCol-pilotCoord(:,1)+1)...
                min(size(interfMat,2), Dnum+interfMatCentCol-pilotCoord(:,1))...
                max(1, interfMatCentRow-pilotCoord(:,2)+1)...
                min(size(interfMat,1), Snum+interfMatCentRow-pilotCoord(:,2))];
FBMC_interferenceConstants.interfLimits = interfLimits;


end

