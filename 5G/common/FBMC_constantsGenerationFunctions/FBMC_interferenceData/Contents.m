% FBMC_AUX Auxiliary functions
%
% Files
%   FBMC_generateInterferenceConstants - Generate aux. pilot and interference data.
%   FBMC_generateInterferenceMatrix    - Calculates the interference matrix for SMT.
%   FBMC_generateInterferenceMatrix_2  - Returns the interference data for the aux. pilots.
%   FBMC_getUsedSubcarriers            - Calculate data about the used subcarriers of the data grid.
