function [interfMat, interfSignMat] = FBMC_generateInterferenceMatrix_2(...
    FBMC_parameters, FBMC_pregeneratedConstants)
%FBMC_GENERATEINTERFERENCEMATRIX_2 Returns the interference data for the aux. pilots.
%
% [interfMat, interfSignMat] = FBMC_generateInterferenceMatrix_2(...
%       FBMC_parameters, FBMC_pregeneratedConstants)
%
% Calculates the matrix of interferences caused by symbols over a pilot for a
% pilot in odd time, odd freq, and the matrix of signs that indicates how the
% values of the interferences changes for the other time, freq cases (even,
% even; even, odd; odd, even).
%
% Improved version of <a href="matlab:help FBMC_GENERATEINTERFERENCEMATRIX">FBMC_generateInterferenceMatrix</a>. This version only
% needs to perform 4 iterations and its much faster.
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_pregeneratedConstants
%       FBMC pregenerated Constants
%
%
% Output parameters:
%   interfMatrix
%       Interference Matrix for a symbol in odd time, odd freq.
%
%   interfSignMatrix
%       3-dimensional matrix of 2D sign masks for the interference matrix. To
%       obtain the interferences caused by the surrounding data on a pilot we
%       will need to multiply interfMatrix by the corresponding 2D matrix on
%       this matrix depending on the pilot position. The 2D sign masks are
%       organized as follows:
%           * interfSignMatrix(:,:,1) - sign mask for even time, even freq.
%           * interfSignMatrix(:,:,2) - sign mask for even time, odd freq.
%           * interfSignMatrix(:,:,3) - sign mask for odd time, even freq.
%           * interfSignMatrix(:,:,4) - sign mask for odd time, odd freq.


%% Parameters extraction

subcarriers = FBMC_parameters.basicParameters.subcarriersNumber;
modFun = FBMC_parameters.modulator.modulatorFunction; % modulator function
demodFun = FBMC_parameters.demodulator.demodulatorFunction; % demodulator function
maxFreqOff = FBMC_parameters.pilots.auxPilotMatrixFreqOffset; % maximum subcarrier offset
maxTimeOff = FBMC_parameters.pilots.auxPilotMatrixTimeOffset; % maximum time offset


%% Calculate the interference data

nData = 2*maxTimeOff+2; % number of time symbols

genInterfMat = cell(4,1); % generated interf by a pilot

interfSize = [1+2*maxFreqOff 1+2*maxTimeOff];
interfCenter = (interfSize-1)/2+1;

recInterfMat = repmat({zeros(interfSize)},4,1); % received interf by a pilot

% create selectMat, matrix for selecting certain elements of interference
selectMat = repmat({false(interfSize)},4,1);

freqOff = -maxFreqOff:maxFreqOff;
timeOff = -maxTimeOff:maxTimeOff;

% main loop
for timeInd=[0 1] % relative time index
    for freqInd=[0 1] % relative frequency index
        freqSymbInd = round(subcarriers/2) + freqInd;
        timeSymbInd = nData/2 + timeInd;
        interfInd = 2*mod(timeSymbInd,2)+mod(freqSymbInd,2)+1; % index for this case
        
        % initialize selectMat for the index interfInd
        selectMat{interfInd}(mod(interfCenter(1)+mod(freqSymbInd,2),2)+1:2:end,...
                             mod(interfCenter(2)+mod(timeSymbInd,2),2)+1:2:end) = 1;
        selectMat{interfInd}(interfCenter(1),interfCenter(2)) = 0;
        
        % transmitter
        txDataGrid = zeros(subcarriers,nData);
        txDataGrid(freqSymbInd,timeSymbInd) = 1;
        txSignal = FBMC_execCell(modFun, ...
            {FBMC_parameters,FBMC_pregeneratedConstants,txDataGrid});
        
        % receiver
        rxDataGrid = FBMC_execCell(demodFun,...
            {FBMC_parameters,FBMC_pregeneratedConstants, txSignal,nData});

        % save interference
        genInterfMat{interfInd} = rxDataGrid(freqSymbInd+freqOff,...
                                             timeSymbInd+timeOff);
    end
end

% Assign interference values for the case (time, subcarrier) = (even, even)
recInterfMat{1}(selectMat{1}) = genInterfMat{4}(selectMat{1});
recInterfMat{1}(selectMat{2}) = genInterfMat{3}(selectMat{2});
recInterfMat{1}(selectMat{3}) = genInterfMat{2}(selectMat{3});
recInterfMat{1}(selectMat{4}) = genInterfMat{1}(selectMat{4});

% Assign interference values for the case (time, subcarrier) = (even, odd)
recInterfMat{2}(selectMat{1}) = genInterfMat{3}(selectMat{1});
recInterfMat{2}(selectMat{2}) = genInterfMat{4}(selectMat{2});
recInterfMat{2}(selectMat{3}) = genInterfMat{1}(selectMat{3});
recInterfMat{2}(selectMat{4}) = genInterfMat{2}(selectMat{4});

% Assign interference values for the case (time, subcarrier) = (odd, even)
recInterfMat{3}(selectMat{1}) = genInterfMat{2}(selectMat{1});
recInterfMat{3}(selectMat{2}) = genInterfMat{1}(selectMat{2});
recInterfMat{3}(selectMat{3}) = genInterfMat{4}(selectMat{3});
recInterfMat{3}(selectMat{4}) = genInterfMat{3}(selectMat{4});

% Assign interference values for the case (time, subcarrier) = (odd, odd)
recInterfMat{4}(selectMat{1}) = genInterfMat{1}(selectMat{1});
recInterfMat{4}(selectMat{2}) = genInterfMat{2}(selectMat{2});
recInterfMat{4}(selectMat{3}) = genInterfMat{3}(selectMat{3});
recInterfMat{4}(selectMat{4}) = genInterfMat{4}(selectMat{4});

% Now we need to flip the interference matrices by columns and rows
recInterfMat = cellfun(@(x) fliplr(flip(x)), recInterfMat, 'UniformOutput', false);

% We concatenate the matrices inside the cell and calculate the signs matrix
interfMat = cat(3,recInterfMat{:});
interfMat(mod(maxFreqOff,2)+1:2:end, mod(maxTimeOff,2)+1:2:end, :) = 0;
interfMat = 1j*imag(interfMat);

interfSignMat = bsxfun(@rdivide, interfMat, interfMat(:,:,4));
interfSignMat(isnan(interfSignMat)) = 1;
interfSignMat = real(round(interfSignMat));

interfMat = interfMat(:,:,4);
          
end

