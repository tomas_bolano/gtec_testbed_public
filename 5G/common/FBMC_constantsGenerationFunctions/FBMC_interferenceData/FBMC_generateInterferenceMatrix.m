function [interfMat, interfSignMat] = FBMC_generateInterferenceMatrix(...
    FBMC_parameters, FBMC_pregeneratedConstants)
%FBMC_GENERATEINTERFERENCEMATRIX Calculates the interference matrix for SMT.
%
% [interfMat, interfSignMat] = FBMC_generateInterferenceMatrix(...
%       FBMC_parameters, FBMC_pregeneratedConstants)
%
% Calculates the matrix of interferences caused by symbols over a pilot for a
% pilot in odd time, odd freq, and the matrix of signs that indicates how the
% values of the interferences changes for the other time, freq cases (even,
% even; even, odd; odd, even).
%
% There is a new version of this function, FBMC_generateInterferenceMatrix_2,
% which is much faster and should be used instead. This is leaved here as a
% reference because the algorithm used in the new code is more complicated.
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_pregeneratedConstants
%       FBMC pregenerated Constants
%
%
% Output parameters:
%   interfMatrix
%       Interference Matrix for a symbol in odd time, odd freq.
%
%   interfSignMatrix
%       3-dimensional matrix of 2D sign masks for the interference matrix. To
%       obtain the interferences caused by the surrounding data on a pilot we
%       will need to multiply interfMatrix by the corresponding 2D matrix on
%       this matrix depending on the pilot position. The 2D sign masks are
%       organized as follows:
%           * interfSignMatrix(:,:,1) - sign mask for even time, even freq.
%           * interfSignMatrix(:,:,2) - sign mask for even time, odd freq.
%           * interfSignMatrix(:,:,3) - sign mask for odd time, even freq.
%           * interfSignMatrix(:,:,4) - sign mask for odd time, odd freq.


%% Parameters extraction

subcarriers = FBMC_parameters.basicParameters.subcarriersNumber;
modFun = FBMC_parameters.modulator.modulatorFunction; % modulator function
demodFun = FBMC_parameters.demodulator.demodulatorFunction; % demodulator function
maxFreqOff = FBMC_parameters.pilots.auxPilotMatrixFreqOffset; % maximum subcarrier offset
maxTimeOff = FBMC_parameters.pilots.auxPilotMatrixTimeOffset; % maximum time offset


%% Calculate the interference data

nData = 2*maxTimeOff+2; % number of time symbols

interfMat = zeros(1+2*maxFreqOff, 1+2*maxTimeOff, 4);
interfCenter = (size(interfMat)-1)/2+1;

% main loop
for timeInd=[0 1] % relative time index
    for freqInd=[0 1] % relative frequency index
        freqSymbInd = round(subcarriers/2) + freqInd;
        timeSymbInd = nData/2 + timeInd;
        interfInd = 2*mod(timeSymbInd,2)+mod(freqSymbInd,2)+1; % index for this case
        
        for freqOff = -maxFreqOff:maxFreqOff
            for timeOff = -maxTimeOff:maxTimeOff
                if freqOff == 0 && timeOff == 0
                    continue;
                end
                
                % transmitter
                txDataGrid = zeros(subcarriers,nData);
                txDataGrid(freqSymbInd+freqOff,timeSymbInd+timeOff) = 1;
                txSignal = modFun(FBMC_parameters,FBMC_pregeneratedConstants,txDataGrid);
                
                % receiver
                rxDataGrid = demodFun(FBMC_parameters,FBMC_pregeneratedConstants,...
                                txSignal,timeSymbInd);

                % save interference
                interfMat(freqOff+interfCenter(1),timeOff+interfCenter(2),...
                    interfInd) = rxDataGrid(freqSymbInd,timeSymbInd);
            end 
        end
    end
end

interfMat(mod(maxFreqOff,2)+1:2:end, mod(maxTimeOff,2)+1:2:end, :) = 0;
interfMat = 1j*imag(interfMat);

interfSignMat = bsxfun(@rdivide, interfMat, interfMat(:,:,4));
interfSignMat(isnan(interfSignMat)) = 1;
interfSignMat = real(round(interfSignMat));

interfMat = interfMat(:,:,4);
          
end

