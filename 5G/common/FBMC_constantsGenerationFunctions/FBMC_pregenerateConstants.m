function [FBMC_pregeneratedConstants] = FBMC_pregenerateConstants(FBMC_parameters)
%FBMC_PREGENERATECONSTANTS
%
% TODO document this function


%% System metadata

FBMC_pregeneratedConstants.metadata = FBMC_getParametersMetadata(FBMC_parameters);


%% pulse

[FBMC_pregeneratedConstants.pulse.TxPulse,...
 FBMC_pregeneratedConstants.pulse.RxPulse] = ...
    FBMC_execCell(FBMC_parameters.pulseShapping.pulseGenerationFunction,...
                  {FBMC_parameters});
              

%% symbol period

% Number of samples between two consecutive symbols
if strcmpi(FBMC_pregeneratedConstants.metadata.systemType, 'ofdm')
    symbolPeriod = FBMC_parameters.pulseShapping.cyclicPrefixLength +...
                   FBMC_parameters.basicParameters.subcarriersNumber;
elseif strcmpi(FBMC_pregeneratedConstants.metadata.systemType, 'smt')
    symbolPeriod = FBMC_parameters.basicParameters.subcarriersNumber/2;
else
    error('System type not recogniced');
end

FBMC_pregeneratedConstants.symbolPeriod = symbolPeriod;


%% usedSubcarriers

FBMC_pregeneratedConstants.usedSubcarriers = FBMC_getUsedSubcarriers(FBMC_parameters);


%% preamble

FBMC_pregeneratedConstants.preamble = FBMC_execCell(...
    FBMC_parameters.synchronization.preambleGenerationFunction, {FBMC_parameters});

% check that no preamble data exist outside the used subcarriers
preambleSize = size(FBMC_pregeneratedConstants.preamble.preambleMask);
assert(any(any(...
           repmat(~FBMC_pregeneratedConstants.usedSubcarriers.subcarrierMask,1,preambleSize(2)) &...
           FBMC_pregeneratedConstants.preamble.preambleMask)) == 0,...
       'The preamble pattern generated creates pilots outside the used subcarriers');

   
%% totalTimeSymbolsNumber

% total tyme symbols number. The tyme symbols number including the preamble,
% the zeros after the preamble and the data.
FBMC_pregeneratedConstants.totalTimeSymbolsNumber = ...
    size(FBMC_pregeneratedConstants.preamble.preambleMask,2) + ...
    FBMC_parameters.synchronization.zerosAfterPreamble + ...
    FBMC_parameters.basicParameters.timeSymbolsNumber;


%% pilots

% general pilot data
FBMC_pregeneratedConstants.pilots = ...
    FBMC_execCell(FBMC_parameters.pilots.pilotsGenerationFunction, {FBMC_parameters});


%% auxpilots

% auxiliary pilot data
if FBMC_parameters.pilots.numberAuxiliaryPilots ~= 0
    % generate interference data
    % for this we call the function FBMC_generateInterferenceMatrix which
    % calls the modulator and demodulator functions. Since the modulator and
    % demodulator only use parameters already defined we can do this. Anyway,
    % the worst thing that can happen is that we get an error.
    [interfMat, interfSignMat] = FBMC_generateInterferenceMatrix_2(...
            FBMC_parameters, FBMC_pregeneratedConstants);

    FBMC_pregeneratedConstants.auxpilots = ...
        FBMC_generateInterferenceConstants(FBMC_parameters, ...
            FBMC_pregeneratedConstants.pilots,interfMat,interfSignMat);
end

% check that no pilots exist outside the used subcarriers
if FBMC_parameters.pilots.numberAuxiliaryPilots ~= 0
    assert(any(any(~FBMC_pregeneratedConstants.usedSubcarriers.subcarrierFullMask &...
                   (FBMC_pregeneratedConstants.pilots.ActualPilotMask | ...
                    FBMC_pregeneratedConstants.pilots.NonZeroPilotMask | ...
                    FBMC_pregeneratedConstants.auxpilots.auxPilotFullCodedMask))) == 0, ...
           ['The pilot pattern generated creates pilots or ',...
            'needs auxiliary pilots outside the used subcarriers']);
else
    assert(any(any(~FBMC_pregeneratedConstants.usedSubcarriers.subcarrierFullMask &...
                   (FBMC_pregeneratedConstants.pilots.ActualPilotMask | ...
                    FBMC_pregeneratedConstants.pilots.NonZeroPilotMask))) == 0, ...    
           'The pilot pattern generated creates pilots outside the used subcarriers'); 
end

% check that adjacent aux. pilots do not enter in the interf. matrix window
if FBMC_parameters.pilots.numberAuxiliaryPilots ~= 0
    interfMatSize = size(FBMC_pregeneratedConstants.auxpilots.interfMatrix);
    if (interfMatSize(1)+1)/2 >= FBMC_parameters.pilots.freqSpacing
        warning('Auxiliary pilots might overlap with the interference matrix in frequency.');
    end

    if (interfMatSize(2)+1)/2 >= FBMC_parameters.pilots.timeSpacing
        warning('Auxiliary pilots might overlap with the interference matrix in time.');
    end
end


%% dataMask

% data mask (of uncoding data)
% This mask will indicate the positions of the grid to insert our data.
FBMC_pregeneratedConstants.dataMask = ...
    FBMC_pregeneratedConstants.usedSubcarriers.subcarrierFullMask;

FBMC_pregeneratedConstants.dataMask = ...
    FBMC_pregeneratedConstants.dataMask & ...
    ~(FBMC_pregeneratedConstants.pilots.ActualPilotMask | ...
      FBMC_pregeneratedConstants.pilots.ZeroPilotMask | ...
      FBMC_pregeneratedConstants.pilots.NonZeroPilotMask);

if FBMC_parameters.pilots.numberAuxiliaryPilots ~= 0
    FBMC_pregeneratedConstants.dataMask = ...
        FBMC_pregeneratedConstants.dataMask & ...
        ~FBMC_pregeneratedConstants.auxpilots.auxPilotFullMask;
end

% Make the matrix full
FBMC_pregeneratedConstants.dataMask = full(FBMC_pregeneratedConstants.dataMask);


%% data input size
% FBMC_pregeneratedConstants.dataInputSize = [nnz(FBMC_pregeneratedConstants.dataMask) 1];
% FBMC_pregeneratedConstants.dataInputBits = ...
%     nnz(FBMC_pregeneratedConstants.dataMask)*...
%     log2(FBMC_parameters.basicParameters.modulationOrder);


%% dataBlockMasks

% mask of data blocks
% This mask will indicate the positions of the differents data blocks
% The blocks will be numbered to. A number greater than cero will indicate
% that the symbols belongs to the corresponding block. A 0 will mean
% that the data symbol is not used for data. A -1 will mean that the data
% symbol is used for data but it was not assigned to any block.

% Store each individual block data mask in a cell array first
dataBlockMaskCell = cell(1, numel(FBMC_parameters.dataBlock));
for i = 1:numel(FBMC_parameters.dataBlock)
    % The mapping function receive also the FBMC_pregeneratedConstants so
    % they can use the variables defined until this point. Anyway, the
    % worst thing that can happen is that we get an error.
    dataBlockMaskCell{i} = FBMC_execCell(FBMC_parameters.dataBlock(i).mappingFunction,...
                                {FBMC_parameters, FBMC_pregeneratedConstants});
                        
    % Restrict the elements of the block to the available data elements
    dataBlockMaskCell{i} = dataBlockMaskCell{i} & FBMC_pregeneratedConstants.dataMask;
end

% Initialize dataBlockMask
dataBlockMask = -FBMC_pregeneratedConstants.dataMask;

% Add blocks numbering to the block mask
for i = 1:numel(dataBlockMaskCell)
    assert(all(dataBlockMask(dataBlockMaskCell{i}) ~= 0), ...
           'Trying to assign a block element to a non usable element');
    assert(all(dataBlockMask(dataBlockMaskCell{i}) == -1), ...
           'Trying to assign a block element to an already assigned element');
       
    dataBlockMask(dataBlockMaskCell{i}) = i;
end

% final check for the data block mask (just in case)
assert(all(dataBlockMask(~FBMC_pregeneratedConstants.dataMask) == 0) &...
       all(dataBlockMask(FBMC_pregeneratedConstants.dataMask) ~= 0),...
       'Error on the datablockmask generation');

if any(dataBlockMask(FBMC_pregeneratedConstants.dataMask) == -1)
   warning('Elements not assigned to any block on the data grid'); 
end

FBMC_pregeneratedConstants.dataBlockMask = dataBlockMask;

% Create array of number of symbols and bits available in each block,
% and number of uncoded bits to transmit in each block.
dataBlockSymbolNumber = zeros(1, numel(FBMC_parameters.dataBlock));
dataBlockBitNumber = zeros(1, numel(FBMC_parameters.dataBlock));
dataBlockInputUncodedBits = zeros(1, numel(FBMC_parameters.dataBlock));

for i = 1:numel(FBMC_parameters.dataBlock)
    dataBlockSymbolNumber(i) = nnz(dataBlockMask == i);
    dataBlockBitNumber(i) = dataBlockSymbolNumber(i)*...
        log2(FBMC_parameters.dataBlock(i).modulationOrder);
    uncodedBitsParam = FBMC_parameters.dataBlock(i).uncodedBitsNumber;
    
    if strcmpi(uncodedBitsParam, 'max')
        dataBlockInputUncodedBits(i) = dataBlockBitNumber(i);
    elseif (uncodedBitsParam < 1)
        dataBlockInputUncodedBits(i) = round(dataBlockBitNumber(i)*uncodedBitsParam);
    else
        dataBlockInputUncodedBits(i) = uncodedBitsParam;
    end
    
    assert(dataBlockInputUncodedBits(i) <= dataBlockBitNumber(i),...
           sprintf(['The number of uncodedBits requested for the ',...
                    'block %d exceeds the block capacity.'], i));
    assert(dataBlockInputUncodedBits(i) > 0,...
           sprintf(['The number of uncodedBits requested for the block %d ',...
                    'is equal or less than cero.'], i));
    assert(mod(dataBlockInputUncodedBits(i),1) == 0,...
           sprintf(['The number of uncodedBits requested for the block %d ',...
                    'is not integer.'], i));
end

FBMC_pregeneratedConstants.dataBlockSymbolsNumber = dataBlockSymbolNumber;
FBMC_pregeneratedConstants.dataBlockBitsNumber = dataBlockBitNumber;
FBMC_pregeneratedConstants.dataBlockInputUncodedBitsNumber = dataBlockInputUncodedBits;

% Store also the total symbol and bits number
FBMC_pregeneratedConstants.dataBlockTotalSymbolsNumber = sum(dataBlockSymbolNumber);
FBMC_pregeneratedConstants.dataBlockTotalBitsNumber = sum(dataBlockBitNumber);
FBMC_pregeneratedConstants.dataBlockTotalUncodedBitsNumber = sum(dataBlockInputUncodedBits);



%% channelModel

FBMC_pregeneratedConstants.channelModel = FBMC_generateChannelConstants(FBMC_parameters);


