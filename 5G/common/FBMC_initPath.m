function FBMC_path = FBMC_initPath()
%FBMC_INITPATH Initialize the path adding all the subfolders.

restoredefaultpath;
FBMC_path = genpath(fileparts(mfilename('fullpath')));
addpath(FBMC_path);

end

