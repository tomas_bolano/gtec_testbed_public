function FBMC_outSignal = FBMC_multiuserSumSignals(varargin)
%FBMC_MULTIUSERSUMSIGNALS Sum the signals specified in the parameters

maxLength = 0;
for i = 1:length(varargin)
    maxLength = max(maxLength, length(varargin{i}));
end

FBMC_outSignal = zeros(maxLength,1);

for i = 1:length(varargin)
   FBMC_outSignal(1:length(varargin{i})) = ...
       FBMC_outSignal(1:length(varargin{i})) + varargin{i};
end

end

