function FBMC_parameters = FBMC_multiuserSetFreqDisplacementParameter(...
    FBMC_parameters, newFreqDisplacement)
%FBMC_MULTIUSERUPDATEFRQDISPLACEMENTPARAMETER Summary of this function goes here
%
% TODO document function


txPostProc = FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions;
rxPreProc = FBMC_parameters.rxSignalPreProcessing.rxPreProcessingFunctions;

txPostProcInd = 0;
rxPreProcInd = 0;

% search FBMC_FreqDisplaceSignalProcessing in txPostProc
for i = 1:length(txPostProc)
    if isequal(txPostProc{i}{1}, @FBMC_FreqDisplaceSignalProcessing)
        txPostProcInd = i;
        break;
    end
end

% search FBMC_FreqDisplaceSignalProcessing in rxPreProc
for i = 1:length(rxPreProc)
    if isequal(rxPreProc{i}{1}, @FBMC_FreqDisplaceSignalProcessing)
        rxPreProcInd = i;
        break;
    end
end

assert(rxPreProcInd ~= 0 && txPostProcInd ~= 0,...
       ['FBMC_FreqDisplaceSignalProcessing function not found in parameters',...
        ' for tx post-pocessing and/or rx pre-processing']);

txPostProc{txPostProcInd}{2} = newFreqDisplacement;
rxPreProc{rxPreProcInd}{2} = -newFreqDisplacement;

FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions = txPostProc;
FBMC_parameters.rxSignalPreProcessing.rxPreProcessingFunctions = rxPreProc;

end

