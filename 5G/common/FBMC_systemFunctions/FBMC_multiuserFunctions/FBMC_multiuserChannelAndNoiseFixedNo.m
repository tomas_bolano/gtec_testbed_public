function [FBMC_channelOutput, FBMC_channelDataCell] = FBMC_multiuserChannelAndNoiseFixedNo(...
    FBMC_parametersCell, FBMC_pregeneratedConstantsCell, FBMC_txDataCell, ...
    FBMC_channelDataCell, No)
%FBMC_CHANNEL Apply a channel to the transmitted signal
%
% TODO documentation

if nargin < 5
    No = 1;
end

numSignals = length(FBMC_parametersCell);
FBMC_rxSignal = cell(1,numSignals);
FBMC_rxSignalEg = cell(1,numSignals);

for i = 1:numSignals
    type = FBMC_parametersCell{i}.channelModel.noise.calculationType;
    noisedB = FBMC_parametersCell{i}.channelModel.noise.dBvalue;
    noiseNat = 10^(noisedB/10);

    % Apply channel
    [FBMC_rxSignal{i}, FBMC_channelDataCell{i}] = ...
        FBMC_channelFunction(FBMC_parametersCell{i}, FBMC_pregeneratedConstantsCell{i},...
                             FBMC_txDataCell{i}.interpolatedSignal, FBMC_channelDataCell{i});
                         
    % Calculate the energy that the signal must have
    switch type
        case 'EbNo'
            modOrder = FBMC_parametersCell{i}.basicParameters.modulationOrder;
            numbits = numel(FBMC_txDataCell{i}.message)*log2(modOrder);
            FBMC_rxSignalEg{i} = noiseNat*No*numbits;
        case 'SNR'
            % when interpolating the signal energy is the same but the power changes
            % because the lenght of the signal changes.
            interpFactor = FBMC_parametersCell{i}.highSpeedEmulation.interpolationFactor;
            FBMC_rxSignalEg{i} = noiseNat*No*length(FBMC_rxSignal{i})/interpFactor;
        otherwise
            error('invalid simType value');
    end
    
    % scale signal to have energy Es
    FBMC_rxSignal{i} = sqrt(FBMC_rxSignalEg{i})*FBMC_rxSignal{i}/...
                            sqrt((FBMC_rxSignal{i}'*FBMC_rxSignal{i}));
end

% Combine signals
FBMC_outSignal = FBMC_multiuserSumSignals(FBMC_rxSignal{:});

% AWGN noise
FBMC_outSignal = FBMC_outSignal + sqrt(No/2)*randn(size(FBMC_outSignal))+...
                               1j*sqrt(No/2)*randn(size(FBMC_outSignal));


%% Assign output values
FBMC_channelOutput.signal = FBMC_outSignal;
FBMC_channelOutput.No = No;
FBMC_channelOutput.rxSignals = FBMC_rxSignal;
FBMC_channelOutput.rxSignalsEg = FBMC_rxSignalEg;


end

