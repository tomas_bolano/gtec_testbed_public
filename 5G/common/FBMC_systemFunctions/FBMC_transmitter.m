function FBMC_txData = FBMC_transmitter(FBMC_parameters, FBMC_constants)
%FBMC_TRANSMITTER Generate the FBMC transmission signal.
%
% Given the input FBMC_parameters and FBMC_pregeneratedConstants, this
% function obtains the data to transmit and modulates the signal for
% transmission. The Output value will be an struct with several of the
% data generated in the transmissor.
%
% In the frame to transmit can be different data blocks to transmit the
% data. In each block a modulation and FEC code may be used as configured
% in the parameters. For each block the data to be transmitted will be
% randomly generated. This data is encoded using a FEC code, and then
% padded with random bits to obtain a sequence of bits of the required
% length for the transmission frame. Finally the coded bits will be
% modulated into symbols of the requested modulation and mapped to the
% corresponding block of the frame. This process is shown in the diagram
% below.
%
%  +-------------------------+
%  |  Uncoded bits (m bits)  |
%  +-------------------------+
%              | Fordward Error correction code
%              v
%  +-----------------------------------+
%  |        Coded bits (n bits)        |
%  +-----------------------------------+
%              | Bits padding
%              v
%  +-----------------------------------+------------------+
%  |        Coded bits (n bits)        | padding (p bits) |
%  +-----------------------------------+------------------+
%              |
%              v
%  +-------------------------+
%  |        Symbols          |
%  +-------------------------+
%              |
%              v
%      Map to frame block
%
%
% If no FEC coding is used (when using the FBMC_nullFecCoding function),
% then the uncoded data will be the transmitted data (but note that padding
% may be still applied if there is no enought bits to fill the frame).
%
% We assume that at the receiver the sequence of coded bits plus paggind
% will be passed directly the FEC decoder, and the FEC decoder will,
% knowing the original number of uncoded bits will extrat the coded bits
% to decode from the full sequence.
%
%
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_constants
%       Pregenerated constants from the parameters.
%
% Output parameters:
%   FBMC_txData
%       Struct with the following fields:
%           * dataBlock
%               Array of structs with the data transmitted in each block.
%               Each struct element has the following fields:
%               - uncodedBits
%                   Vector of the transmitted uncoded bits.
%               - codedData
%                   Struct of the coded data as returned by the FBMC_fecCoder
%                   function.
%               - codedPaddedData
%                   Struct of the transmitted coded data, as returnen by the
%                   FBMC_padTxBits function.
%               - symbols
%                   Vector of the modulated symbols transmitted. This vector
%                   will have the same size as the message vector, and its
%                   elements will be obtained from the message by modulating
%                   the data to the create the symbols of the used constellation.
%           * dataBlockCodedBitsNumber
%               Array with the length of the coded bits sequence for each
%               data block (without the padding).
%           * dataGrid
%               Time-Frequency data grid to transmit, before coding the
%               auxiliary pilots and appending the preamble.
%           * auxPilotDataGrid
%               Time-Frequency data grid to transmit with the auxiliary pilots
%               inserted, before appending the preamble.
%           * preambleDataGrid
%               Time-Frequency data grid to transmit with the preamble appended.
%           * shiftedDataGrid
%               Time-Frequency data grid obtained by doing an ifftshift to the
%               columns of the preambleDataGrid grid. This will be the data
%               grid that the modulator will receive.
%           * modulatedSignal
%               FBMC signal generated by the modulator
%           * signal
%               FBMC signal after post processing.
%           * signalEnergy
%               Energy of the signal.
%           * signalPower
%               Power of the signal, i.e. energy divided by the number of
%               elements of the signal.
%           * signalEb
%               Energy per bit of the signal.
%           * interpolatedSignal
%               Signal interpolated for high speed emulation.
%


%% Data to transmit for each block

FBMC_dataBlock = struct();
FBMC_dataBlockCodedBitsNumber = zeros(1, numel(FBMC_parameters.dataBlock));

for i = 1:numel(FBMC_parameters.dataBlock)
    dataBlockParam = FBMC_parameters.dataBlock(i);
    dataBlockSizeBits = FBMC_constants.dataBlockBitsNumber(i);
    dataBlockInputUncodedBits = FBMC_constants.dataBlockInputUncodedBitsNumber(i);
    
    % Generate an array of random bits as the uncoded bits
    uncodedBits = randi([0 1], dataBlockInputUncodedBits, 1);
    
    % Code the bits (if the fec coder is not the null coder)
    codedData = FBMC_FECCoder(FBMC_parameters, FBMC_constants, i, uncodedBits);
    
    % Add the neccesary padding and modulate the symbols
    codedPaddedData = FBMC_padTxBits(codedData.bits, dataBlockSizeBits);
    symbols = FBMC_symbolModulator(codedPaddedData.bits,...
                                   dataBlockParam.modulation,...
                                   dataBlockParam.modulationOrder, 'bit',...
                                   dataBlockParam.modulationNormalize,...
                                   dataBlockParam.modulationEnergyFactor);

    FBMC_dataBlock(i).uncodedBits = uncodedBits;
    FBMC_dataBlock(i).codedData = codedData;
    FBMC_dataBlock(i).codedPaddedData = codedPaddedData;
    FBMC_dataBlock(i).symbols = symbols;
    
    FBMC_dataBlockCodedBitsNumber(i) = numel(codedData.bits);
end


%% Multi-modulation transmitter

% Interleave data block symbols
FBMC_txSymbolCell = FBMC_dataBlockInterleaver(FBMC_parameters,...
                                              {FBMC_dataBlock.symbols});

% Generate time-frequency grid (without auxiliary pilots)
FBMC_dataGrid = FBMC_dataGridGenerator(FBMC_parameters,...
                                       FBMC_constants,...
                                       FBMC_txSymbolCell);
                                   
% FBMC_dataGrid(FBMC_pregeneratedConstants.usedSubcarriers.DCIndex+1,1) = 1;

% Add auxiliary pilots
FBMC_auxPilotDataGrid = FBMC_dataGridAuxPilotCoder(FBMC_parameters,...
                                                   FBMC_constants,...
                                                   FBMC_dataGrid);

% Add preamble
FBMC_preambleDataGrid = FBMC_addPreamble(FBMC_parameters,...
                                         FBMC_constants,...
                                         FBMC_auxPilotDataGrid);

% apply ifftshift
FBMC_shiftedDataGrid = ifftshift(FBMC_preambleDataGrid,1);

% Modulation
FBMC_txModulatedSignal = FBMC_modulator(FBMC_parameters, FBMC_constants,...
                                        FBMC_shiftedDataGrid);


%% Tx signal post-processing

FBMC_txSignal = FBMC_txSignalPostProcessing(FBMC_parameters,...
                     FBMC_constants, FBMC_txModulatedSignal);


%% Energy and power calculation
FBMC_txSignalEg = FBMC_txSignal'*FBMC_txSignal;
FBMC_txSignalPw = FBMC_txSignalEg/length(FBMC_txSignal);
FBMC_txSignalEb = FBMC_txSignalEg/FBMC_constants.dataBlockTotalBitsNumber;


%% High speed emulation - interpolation
FBMC_txInterpolatedSignal = FBMC_highSpeedEmulation_interpolation(...
                                FBMC_parameters, FBMC_txSignal);


%% Assign output values

% create frameDataInfo field
% frameDataInfo.uncodedLen = length(FBMC_txUncodedBits);
% frameDataInfo.codedLen = length(FBMC_txCodedData.bits);
% frameDataInfo.coded = FBMC_txCodedData.coded;

% Assign output
%FBMC_txData.frameDataInfo = frameDataInfo;
%FBMC_txData.uncodedBits = FBMC_txUncodedBits;
%FBMC_txData.codedData = FBMC_txCodedData;
%FBMC_txData.codedPaddedData = FBMC_txCodedPaddedData;
%FBMC_txData.message = FBMC_txMessage;
%FBMC_txData.symbols = FBMC_txSymbols;

FBMC_txData.dataBlock = FBMC_dataBlock;
FBMC_txData.dataBlockCodedBitsNumber = FBMC_dataBlockCodedBitsNumber;
FBMC_txData.dataGrid = FBMC_dataGrid;
FBMC_txData.pilotDataGrid = FBMC_auxPilotDataGrid;
FBMC_txData.preambleDataGrid = FBMC_preambleDataGrid;
FBMC_txData.shiftedDataGrid = FBMC_shiftedDataGrid;
FBMC_txData.modulatedSignal = FBMC_txModulatedSignal;
FBMC_txData.signal = FBMC_txSignal;
FBMC_txData.signalEnergy = FBMC_txSignalEg;
FBMC_txData.signalPower = FBMC_txSignalPw;
FBMC_txData.signalEb = FBMC_txSignalEb;
FBMC_txData.interpolatedSignal = FBMC_txInterpolatedSignal;


end

