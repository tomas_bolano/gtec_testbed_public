%% Initializations

clear;
close all;
format long g
%format short
clc

%% set random number generator seed
rng(12521)


%% Configuration for the simulation
FBMC_parameters = {FBMC_MultiuserExampleHermiteParametersDefinition;
                   FBMC_MultiuserExampleOFDMUnfilteredParametersDefinition};

% % Parameters modifications
FBMC_parameters{1} = FBMC_multiuserSetFreqDisplacementParameter(FBMC_parameters{1}, -0.15);


noiseVec = [60, 60]; % EbNo or SNR in dB
simType = 'EbNo'; % 'EbNo', 'SNR' or 'PwNo'
receiverInd = 2;


%% Plotting results configuration

plot_freq_txSignal = 1;
plot_freq_chSignal = 1;
plot_freq_rxSignal = 1;
plot_est_channel   = 1;
plot_est_channel_first_symbol = 1;

% configuration for the pwelch functions
window = 1024;
noverlap = 512;
nfft = 1024;

% styles for plots of multiple figures
styles = {'b-', 'r-', 'm-'};


%% Simulation

numParameters = length(FBMC_parameters);

assert(numel(noiseVec) == numParameters,...
       ['noiseLevelDBMatrix must have the same number of elements as the number',...
        ' of elements in FBMC_parametersCell']);

assert(min(receiverInd) >= 1 && max(receiverInd) <= numParameters,...
       'receiverIndexes must be a vector of indexes from 1 to %d', numParameters);

assert(numel(unique(cellfun(@(x) x.signalGeneration.dt, FBMC_parameters))) == 1,...
       'all the parameters must have the same value for signalGeneration.dt');


FBMC_pregeneratedConstants = cell(1, numParameters);
FBMC_txData = cell(1, numParameters);
FBMC_noiseValues = cell(1, numParameters);

for j = 1:numParameters
    FBMC_pregeneratedConstants{j} = FBMC_pregenerateConstants(FBMC_parameters{j});
    FBMC_parameters{j} = FBMC_setNoiseCalculationType(FBMC_parameters{j}, simType);
    FBMC_txData{j} = FBMC_transmitter(FBMC_parameters{j}, FBMC_pregeneratedConstants{j});
    FBMC_noiseValues{j} = FBMC_noise2NatEbNo(FBMC_txData{j},...
        FBMC_parameters{j}.basicParameters.modulationOrder, simType, noiseVec(j));
end
    
for j = 1:numParameters
    fprintf('>>> User %d\n', j)
    FBMC_printSimulationInformation(FBMC_parameters{j}, FBMC_pregeneratedConstants{j});
    FBMC_printSignalInformation(FBMC_parameters{j}, FBMC_pregeneratedConstants{j}, FBMC_txData{j});
    FBMC_printChannelInformation(FBMC_parameters{j}, FBMC_pregeneratedConstants{j});
    FBMC_printTxPostProcInformation(FBMC_parameters{j}, FBMC_pregeneratedConstants{j});
    FBMC_printRxPreProcInformation(FBMC_parameters{j}, FBMC_pregeneratedConstants{j});
    fprintf('\n');
end
   
FBMC_channelData = cell(1,numParameters);  %  always empty at the moment

% Update noise parameters
for j = 1:numParameters
    FBMC_parameters{j} = FBMC_setNoiseValue(FBMC_parameters{j}, noiseVec(j));
end

% Apply channel
[FBMC_channelOutput, FBMC_channelData] = FBMC_multiuserChannelAndNoiseFixedNo(...
    FBMC_parameters, FBMC_pregeneratedConstants, FBMC_txData, FBMC_channelData, 1);

% Execute the selected receiver using the combined signal
FBMC_rxData = FBMC_receiver(FBMC_parameters{receiverInd},...
                            FBMC_pregeneratedConstants{receiverInd},...
                            FBMC_channelOutput.signal);

if ~FBMC_rxData.detected
    error('Signal not detected');
end

FBMC_results = FBMC_systemIterationResults(FBMC_parameters{receiverInd},...
    FBMC_pregeneratedConstants{receiverInd}, FBMC_txData{receiverInd}, FBMC_rxData);

fprintf('>>> Results for user %d\n', receiverInd);
FBMC_printSeparatorLine(80);

fprintf('Total data symbols: %d\n', numel(FBMC_txData{receiverInd}.message));
fprintf('Erroneous symbols received: %d\n', ...
    nnz(FBMC_txData{receiverInd}.message ~= FBMC_rxData.message));
fprintf('Symbol error rate: %g\n', ...
    nnz(FBMC_txData{receiverInd}.message ~= FBMC_rxData.message)/...
        numel(FBMC_txData{receiverInd}.message));

FBMC_printSeparatorLine(80);

modOrder = FBMC_parameters{j}.basicParameters.modulationOrder;
fprintf('Total data bits: %d\n', numel(FBMC_txData{j}.message)*log2(modOrder));
fprintf('Erroneous bits received: %d\n', FBMC_results.errorBits);
fprintf('Bit error rate: %g\n', ...
    FBMC_results.errorBits/(numel(FBMC_txData{j}.message)*log2(modOrder)));
fprintf('Analytical Bit error rate (AWGN):     %g\n', ...
    berawgn(FBMC_noiseValues{j}.EbNoDB, FBMC_pregeneratedConstants{j}.metadata.modulation, modOrder));
fprintf('Analytical Bit error rate (Rayleigh): %g\n', ...
    berfading(FBMC_noiseValues{j}.EbNoDB, FBMC_pregeneratedConstants{j}.metadata.modulation, modOrder,1));

FBMC_printSeparatorLine(80);
fprintf('\n');


%% plot results

if plot_freq_txSignal
    figure();
    legendCell = cell(1,numParameters);
    for i = 1:numParameters
        [pxx,f] = pwelch(FBMC_txData{i}.signal, window, noverlap, nfft,...
                        1/FBMC_parameters{i}.signalGeneration.dt, 'centered');
        semilogy(f, abs(pxx)/max(abs(pxx)), styles{mod(i-1,numel(styles))+1});
        legendCell{i} = sprintf('%d - %s', i, FBMC_pregeneratedConstants{i}.metadata.systemDesc);
        hold on;
    end
    grid on;
    title('power spectral density estimate of post processed TX signal (normalized)');
    xlabel('frequency [Hz]');
    ylabel('power/frequency [dB/Hz/sample]');
    legend(legendCell{:});
end


if plot_freq_chSignal
    figure();
    legendCell = cell(1,length(FBMC_parameters));
    for i = 1:length(FBMC_parameters)
        [pxx,f] = pwelch(FBMC_channelOutput.rxSignals{i}, window, noverlap, nfft,...
                    1/FBMC_parameters{i}.signalGeneration.dt, 'centered');
        semilogy(f, abs(pxx), styles{mod(i-1,numel(styles))+1});
        legendCell{i} = FBMC_pregeneratedConstants{i}.metadata.systemDesc;
        hold on;
    end
    grid on;
    title('power spectral density estimate of TX signals after channel (NOT normalized)');
    xlabel('frequency [Hz]');
    ylabel('power/frequency [dB/Hz/sample]');
    legend(legendCell{:});
end


if plot_freq_rxSignal
    figure();
    [pxx,f] = pwelch(FBMC_channelOutput.signal, window, noverlap, nfft,...
                     1/FBMC_parameters{receiverInd}.signalGeneration.dt, 'centered');
    semilogy(f, abs(pxx)/max(abs(pxx)), styles{1});
    grid on;
    title('power spectral density estimate of RX signal (normalized)');
    xlabel('frequency [Hz]');
    ylabel('power/frequency [dB/Hz/sample]');
end


if plot_est_channel && ~isempty(FBMC_rxData.channelEstimation)
    figure();
    scShift = -(FBMC_parameters{receiverInd}.basicParameters.subcarriersNumber/2+1);
    
    % plot estimated channel coefficients
    [fullXGrid, fullYGrid] = meshgrid(1:size(FBMC_rxData.channelEstimation,2),...
                                      1:size(FBMC_rxData.channelEstimation,1));
    surf(fullXGrid, fullYGrid+scShift, abs(FBMC_rxData.channelEstimation));
    hold on;
    shading flat;
    
    % plot superimposed pilot positions
    stem3(FBMC_pregeneratedConstants{receiverInd}.pilots.ActualPilotXGrid,...
          FBMC_pregeneratedConstants{receiverInd}.pilots.ActualPilotYGrid+scShift,...
          reshape(abs(FBMC_rxData.channelEstimation(...
                      FBMC_pregeneratedConstants{receiverInd}.pilots.ActualPilotMask)),...
                  size(FBMC_pregeneratedConstants{receiverInd}.pilots.ActualPilotXGrid)),...
          'm', 'fill', 'LineStyle', 'none');

    % plot superimposed erroneous symbols recived
    modOrder = FBMC_parameters{receiverInd}.basicParameters.modulationOrder;
    rxSymbolGrid = zeros(size(FBMC_rxData.decodedDataGrid));
    rxSymbolGrid(FBMC_pregeneratedConstants{receiverInd}.dataMask) = ...
        FBMC_parameters{receiverInd}.basicParameters.modulationFunction(FBMC_rxData.message,modOrder);

    txSymbolGrid = FBMC_txData{receiverInd}.dataGrid;
    txSymbolGrid(~FBMC_pregeneratedConstants{receiverInd}.dataMask) = 0;
     
    errorSymbols = nan(size(txSymbolGrid));
    errorSymbols(rxSymbolGrid ~= txSymbolGrid) = ...
        abs(FBMC_rxData.channelEstimation(rxSymbolGrid ~= txSymbolGrid));
     
    [dataX, dataY] = meshgrid(1:size(txSymbolGrid,2), 1:size(txSymbolGrid,1));
    stem3(dataX, dataY+scShift, errorSymbols, 'r', 'fill', 'LineStyle', 'none');
       
    % add labels and legend
    ylabel('Subcarrier number');
    xlabel('Time');
    zlabel('Amplitude');
    title('Interpolated channel and pilot points');
    legend('estimated channel coefficients',...
           'pilot points superimposed',...
           'erroneous symbols superimposed',...
           'location', 'NorthEast');
end


if plot_est_channel_first_symbol
    figure();
    scShift = -(FBMC_parameters{receiverInd}.basicParameters.subcarriersNumber/2+1);

    xind = (1:size(FBMC_rxData.channelEstimation,1));
    estChan1 = FBMC_rxData.channelEstimation(:,1);
    plot(xind+scShift, abs(estChan1));
    
    hold on;
    pilotInd = find((any(FBMC_pregeneratedConstants{receiverInd}.pilots.ActualPilotMask.')));
    plot(pilotInd+scShift, abs(estChan1(pilotInd)), 'ro');
    
    title('Estimated channel for the first symbol');
    xlabel('Subcarrier number');
    ylabel('Amplitude');
    legend('Estimated channel', 'Superimposed pilot points');
    
    figure();
    plot(xind+scShift, real(estChan1));
    hold on;
    plot(pilotInd+scShift, real(estChan1(pilotInd)), 'ro');
    
    title('Estimated channel for the first symbol (real part)');
    legend('Estimated channel (real part)', 'Superimposed pilot points');
end

