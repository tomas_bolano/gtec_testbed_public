function [FBMC_parameters] = FBMC_MultiuserExampleOFDMUnfilteredParametersDefinition()
%FBMC_parametersDefinition 


%% Basic parameters
FBMC_parameters.basicParameters.subcarriersNumber = 1024; % Number of subcarriers
FBMC_parameters.basicParameters.timeSymbolsNumber = 67;   % Number of time-symbols (TODO might change)
FBMC_parameters.basicParameters.modulationFunction = @FBMC_qammod;
FBMC_parameters.basicParameters.demodulationFunction = @FBMC_qamdemod;
FBMC_parameters.basicParameters.modulationOrder = 4;


%% Pulse shapping parameters

FBMC_parameters.pulseShapping.overlappingFactor = 4; % Number of overlapping pulses at each side (not used)
FBMC_parameters.pulseShapping.cyclicPrefixLength = 72; % For OFDM-CP
FBMC_parameters.pulseShapping.pulseGenerationFunction = @FBMC_generateOFDMCPPulse; % Pointer to filter generation function (TODO change)


%% Signal generation

interpolationFactor = 3;
FBMC_parameters.signalGeneration.dt = 1/(interpolationFactor*(15.36e6));


%% Data generation

FBMC_parameters.frameGeneration.sideGuardSubcarriers = [212,211]; % (TODO paper change)
FBMC_parameters.frameGeneration.insertDCNull = 1;


%% Pilots

FBMC_parameters.pilots.pilotsGenerationFunction = @FBMC_squarePilotIndexer;

FBMC_parameters.pilots.freqSpacing = 8;
FBMC_parameters.pilots.timeSpacing = 5;

FBMC_parameters.pilots.pilotsEnergyFactor = 2; % (TODO paper change)

% >>> auxiliary pilot data ----------------------------------------------------
% Number of auxiliary pilots to use. 0 for no auxiliary pilots,
% 1 for plain auxiliary pilot method. 2, 4 or 8 for CAP (coded
% auxiliary pilot method). (not used)
FBMC_parameters.pilots.numberAuxiliaryPilots = 0;

% Auxiliary pilots interference matrix parameters
% Number of rows/columns from the center to calculate the interference.
% The size of the interference matrix will be
% (1+2*auxPilotMatrixFreqOffset)x(1+2*auxPilotMatrixTimeOffset)
FBMC_parameters.pilots.auxPilotMatrixFreqOffset = 1; % (not used)
FBMC_parameters.pilots.auxPilotMatrixTimeOffset = 4; % (not used)

FBMC_parameters.channelModel.SNRdB = inf;
 
% >>> [squarePilotIndexer specific configuration] -----------------------------
FBMC_parameters.pilots.squarePilotIndexer.initTimeDisplace = 0;
FBMC_parameters.pilots.squarePilotIndexer.endTimeDisplace = 1;
FBMC_parameters.pilots.squarePilotIndexer.initFreqDisplace = 1;
FBMC_parameters.pilots.squarePilotIndexer.endFreqDisplace = 1;


%% Channel modeling

% channel models to apply in order (TODO change)
FBMC_parameters.channelModel.channelGenerationFunctions = ...
        {@FBMC_idealChannelModel};
    
FBMC_parameters.channelModel.relativeSpeed = 0.001;
FBMC_parameters.channelModel.carrierFrequency = 2.5e9;

% Additive noise configuration
FBMC_parameters.channelModel.noise.calculationType = 'SNR'; % 'EbNo', or 'SNR'
FBMC_parameters.channelModel.noise.dBvalue = inf;

% channel type for the stdchan channel model % (TODO paper change)
FBMC_parameters.channelModel.stdchanChannelModel.Type = 'itur3GIBx';

% configuration for the winner2PedroAndXose channel model
FBMC_parameters.channelModel.winner2PedroAndXoseChannelModel.Index = 21;


%% Channel estimation

FBMC_parameters.channelEstimation.channelEstimationFunction = @FBMC_squareChannelEstimator;
FBMC_parameters.channelEstimation.interpolationMethod = 'spline';


%% Channel equalization

FBMC_parameters.channelEqualization.channelEqualizationFunction = @FBMC_ZFChannelEqualizator;


%% [txSignalPostProcessing] Post-processing of the transmitted signal

% TX postprocessing functions to apply in order
%
% The format is of this parameter is:
% {{function1, param1, param2, ...};...
%  {function2, param1, param2, ...};... % Additional functions
%  }
%
% The prototipe of each function must be
% function(FBMC_prameters, FBMC_pregeneratedConstants, param1, param2, ...)

%
% The format is of this parameter is:
% {{function1, param1, param2, ...};...
%  {function2, param1, param2, ...};... % Additional functions
%  }
%
% The prototipe of each function must be
% function(FBMC_prameters, FBMC_pregeneratedConstants, param1, param2, ...)


% TX postprocessing functions to apply in order
%
% The format is of this parameter is:
% {{function1, param1, param2, ...};...
%  {function2, param1, param2, ...};... % Additional functions
%  }
%
% The prototipe of each function must be
% function(FBMC_prameters, FBMC_pregeneratedConstants, param1, param2, ...)

filter = FBMC_OFDMFilteredFIRFilterGenerationExample();
delay = floor(filter.order/2);

% FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions = ...
%     {{@FBMC_addZerosSignalProcessing, 0, delay};
%      {@FBMC_MatlabFilterSignalProcessing, filter};
%      {@FBMC_truncateSignalProcessing, delay, 0};
%      {@FBMC_interpolateSignalProcessing, interpolationFactor,8,0.8};
%      {@FBMC_FreqDisplaceSignalProcessing, 11e6*FBMC_parameters.signalGeneration.dt}};

FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions = ...
    {{@FBMC_interpolateSignalProcessing, interpolationFactor, 15, 0.8};
     {@FBMC_FreqDisplaceSignalProcessing, 11e6*FBMC_parameters.signalGeneration.dt}};

%% [rxSignalPreprocessing] Pre-processing of the received signal

% RX preprocessing functions to apply in order.
% The format os this field is the same as txSignalPostProcessing.
FBMC_parameters.rxSignalPreProcessing.rxPreProcessingFunctions = ...
    {{@FBMC_FreqDisplaceSignalProcessing, -11e6*FBMC_parameters.signalGeneration.dt};
     {@FBMC_decimateSignalProcessing, interpolationFactor, 22, 'fir'}};


%% Modulator

FBMC_parameters.modulator.modulatorFunction = @FBMC_ofdmcpModulator_2;


%% Demodulator

FBMC_parameters.demodulator.demodulatorFunction = @FBMC_ofdmcpDemodulator_2;


%% Synchronization

FBMC_parameters.synchronization.synchronizationFunction = @FBMC_nullSynchronizator; % (TODO change)
FBMC_parameters.synchronization.preambleGenerationFunction = @FBMC_nullPreambleGenerator;
FBMC_parameters.synchronization.zerosAfterPreamble = 0;
FBMC_parameters.synchronization.preambleLength = 4;
FBMC_parameters.synchronization.preambleSymbolsEnergyFactor = 1^2;
FBMC_parameters.synchronization.synchronizationThreshold = 0.4; % treshold for the synchronization metric

% NOTE the preamble lenght depends on the selected preamble generation function
% and can be different of the value set in the preambleLength field. The
% correct way to obtain the dimensions of the preamble is to get the size of
% the preamble mask returned by this function.

% configuration for the smtPreambleSynchronizator synchronization function
FBMC_parameters.synchronization.smtPreambleSynchronizator.repetitionNumber = 3;
FBMC_parameters.synchronization.smtPreambleSynchronizator.repetitionPeriod = ...
    round(FBMC_parameters.basicParameters.subcarriersNumber/2);
FBMC_parameters.synchronization.smtPreambleSynchronizator.offsetTimeAdjustment = ...
    round(3.5*FBMC_parameters.basicParameters.subcarriersNumber/2);
FBMC_parameters.synchronization.smtPreambleSynchronizator.inversePhaseEstimation = 0;

% configuration for the fixedSynchronizator synchronization function.
FBMC_parameters.synchronization.fixedSynchronizator.offsetTime = 0; % (TODO change)
FBMC_parameters.synchronization.fixedSynchronizator.offsetFrequency = 0;


%% High speed emulation

FBMC_parameters.highSpeedEmulation.interpolationFactor = 1;
FBMC_parameters.highSpeedEmulation.filterOrder = [];

%% Results evaluation
FBMC_parameters.resultsEvaluation.EVMsaturationVec = 3;
