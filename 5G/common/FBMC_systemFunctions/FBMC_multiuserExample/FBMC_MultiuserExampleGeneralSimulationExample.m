%% Initializations

clear;
close all;
format long g
%format short
clc

%% set random number generator seed
rng(12521)


%% Parameters
FBMC_parametersCell = {FBMC_MultiuserExampleHermiteParametersDefinition;
                       FBMC_MultiuserExampleOFDMUnfilteredParametersDefinition};
                   
% Example
FBMC_parametersCell{1} = FBMC_multiuserSetFreqDisplacementParameter(FBMC_parametersCell{1}, 0);                   
                   
                   
XNoMat = repmat(1:10,2,1); % EbNo or SNR in dB
simType = 'EbNo';          % 'EbNo', 'SNR' or 'PwNo'
receiversInd = 2;
niter = 50;



%% Plotting results configuration

plot_freq_txSignal = 1;
plot_freq_rxSignal = 1;
plot_freq_preProc_rxSignal = 1;


% configuration for the pwelch functions
window = 1024;
noverlap = 512;
nfft = 1024;


%% Simulation
FBMC_simulationResults = FBMC_multiuserSystemSimulationByNoise(...
    FBMC_parametersCell, XNoMat, receiversInd, simType, niter, 1);

% TODO print results
