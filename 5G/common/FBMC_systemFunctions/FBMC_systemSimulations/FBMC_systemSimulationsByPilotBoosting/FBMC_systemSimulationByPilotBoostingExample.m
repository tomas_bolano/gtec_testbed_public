%close all;
clear;
clc

%% Basic configuration for the simulation
pilotEgFactor = 1:10; % Energy factor for the pilots
snrType = 'EbNo'; % 'EbNo' or 'SNR'
snr_dB =  5; % EbNo or SNR in dB
noiseAddingType = 'fixNo';
nIter = 10; % maximum number of iterations per EbNo

% parameters used for the comparison
FBMC_parameters_cell = {FBMC_systemExampleOFDMParametersDefinition,...
                        FBMC_systemExampleHermiteParametersDefinition,...
                        FBMC_systemExamplePHYDYASParametersDefinition};

% in-place modification to the parameters
for i = 1:length(FBMC_parameters_cell)
    % do not use synchronization
    FBMC_parameters_cell{i}.synchronization.synchronizationFunction = {@FBMC_nullSynchronizator};
    FBMC_parameters_cell{i}.synchronization.preambleGenerationFunction = {@FBMC_nullPreambleGenerator};
    
    % channel setup
    FBMC_parameters_cell{i}.channelModel.channelGenerationFunctions = {@FBMC_idealChannelModel};
    %FBMC_parameters_cell{i}.channelModel.channelGenerationFunctions = {@FBMC_stdchanChannelModel};
    %FBMC_parameters_cell{i}.channelModel.stdchanChannelModel.Type = '3gppTUx';
    %FBMC_parameters_cell{i}.channelModel.relativeSpeed = 200/3.6;
end


%% Simulation

FBMC_results = cell(1, numel(FBMC_parameters_cell));

tic();
for paramInd = 1:length(FBMC_parameters_cell)
    fprintf('Executing simulation for parameter %d\n', paramInd);
    paramMetadata = FBMC_getParametersMetadata(FBMC_parameters_cell{paramInd});

    for i = 1:length(pilotEgFactor)
        fprintf('-> Eg factor %f\n', pilotEgFactor(i));
        
        % Set pilot energy factor in the parameters
        if strcmpi(paramMetadata.systemType, 'SMT')
           FBMC_parameters_cell{paramInd}.pilots.modulationEnergyFactor = 2*pilotEgFactor(i);
        else
            FBMC_parameters_cell{paramInd}.pilots.modulationEnergyFactor = pilotEgFactor(i);
        end
        
        % run the simulation
        FBMC_resultsByNoise = FBMC_systemSimulationByNoise(FBMC_parameters_cell{paramInd},...
                       snr_dB, snrType, noiseAddingType, nIter, 0);
        
        % save the results
%         FBMC_ber_cell{paramInd}(:,i) = FBMC_resultsByNoise.results.errorBitsMat/...
%                                        FBMC_resultsByNoise.bitsPerIter;
%         FBMC_detected_cell{paramInd}(i) = FBMC_resultsByNoise.results.detectedVec;
        
        detectedVec(i) = FBMC_resultsByNoise.YAxisData.detectedVec;
        resultsMat(:,i) = FBMC_resultsByNoise.YAxisData.resultsMat;
    end
    
    % Construct a results structure suitable for the
    % FBMC_uncodedBERPlottingDataGeneration function
    FBMC_results{paramInd}.parameters = FBMC_resultsByNoise.parameters;
    FBMC_results{paramInd}.constants = FBMC_resultsByNoise.constants;
    FBMC_results{paramInd}.nIter = FBMC_resultsByNoise.nIter;
    FBMC_results{paramInd}.XAxisDataType = 'pilotBoost';
    FBMC_results{paramInd}.XAxisData = pilotEgFactor;
    FBMC_results{paramInd}.YAxisData.detectedVec = detectedVec;
    FBMC_results{paramInd}.YAxisData.resultsMat = resultsMat;
    FBMC_results{paramInd}.noiseData = FBMC_resultsByNoise.XAxisData.noiseValues;
end
toc();


%% Generate data to plot

bcaEstimator = @(x)mean(x);
bcaAlpha = 0.05;
bcaNIters = 400;

fprintf('Generating uncoded BER data to plot... ');
uncodedBERPlottingData = FBMC_uncodedBERPlottingDataGeneration(...
    FBMC_results, bcaEstimator, bcaAlpha, bcaNIters);
fprintf('OK\n');


%% Plot results

linealScalePlot = 1;
plotStyles = {};


figure();
hold on;

fprintf('Plotting uncoded and coded BER... ');

% Plot uncoded BER results
if isempty(plotStyles), k = []; else k = 1; end

for i = 1:numel(uncodedBERPlottingData)
    errorbar(uncodedBERPlottingData(i).XAxisData,...
             uncodedBERPlottingData(i).bervec,...
             uncodedBERPlottingData(i).lowerCI,...
             uncodedBERPlottingData(i).upperCI,...
             plotStyles{k});

    % ik k is [], then the modulus will be [] too
    k = mod(k, numel(plotStyles)) + 1;
end

if ~linealScalePlot
    %set(get(h,'Parent'), 'YScale', 'log');
    set(gca, 'YScale', 'log');

    % set axis y limits
    bervec = uncodedBERPlottingData.bervec;
    ymax = 10.^(floor(max(bervec(~isinf(bervec) & bervec > 0))));
    ymin = 10.^(floor(log10(min((bervec(~isinf(bervec) & bervec > 0))))));

    if isempty(ymax)
        ymax = 1;
    end

    if isempty(ymin)
        ymin = 0;
    end

    ylim([ymin ymax]);
end

legend(uncodedBERPlottingData.legend);

title('Uncoded Bit Error Rate');
xlabel(sprintf('Pilot energy factor'));
ylabel('BER');
grid on;
hold off;

fprintf('OK\n');

