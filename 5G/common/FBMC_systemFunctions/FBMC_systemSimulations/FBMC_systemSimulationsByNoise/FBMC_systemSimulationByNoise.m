function FBMC_simulationResults = FBMC_systemSimulationByNoise(FBMC_parameters,...
    noiseLevelDBVector, snrType, addingMethod, niter, printReport)
%FBMC_SYSTEMSIMULATIONBYNOISE Execute a simulation iterating through different Noise levels.
%
% TODO documentation


if nargin < 6
    printReport = 0;
end

noiseVec = noiseLevelDBVector;

rng(0);
FBMC_constants = FBMC_pregenerateConstants(FBMC_parameters);
FBMC_parameters = FBMC_setNoiseSNRType(FBMC_parameters, snrType, addingMethod);
FBMC_txData = FBMC_transmitter(FBMC_parameters, FBMC_constants);


assert(numel(FBMC_parameters.dataBlock) == 1,...
       'For simulations only 1 datablock is supported');


% FBMC_noiseData is a struct with the following fields:
%   snrType
%       SNR metric used to calculate the other values.
%   referencleBlock
%       Index of the block used as reference to calculaten the other values
%   noiseValues
%       Array of structs whith SNR and EbNo values
FBMC_noiseData.snrType = snrType;
FBMC_noiseData.referenceBlock = FBMC_parameters.channelModel.noise.referenceBlock;
FBMC_noiseData.noiseValues = struct();

if printReport == 1
    FBMC_printSimulationInformation(FBMC_parameters, FBMC_constants);
    FBMC_printSignalInformation(FBMC_parameters, FBMC_constants, FBMC_txData);
    FBMC_printChannelInformation(FBMC_parameters, FBMC_constants);
    FBMC_printDataBlocksInformation(FBMC_parameters, FBMC_constants);
end

% Vector of detected frames per noise value
detectedVec = zeros(1, length(noiseVec));

% Initialize matrix of struct of the results
% resultsMat = repmat(struct(), niter, length(noiseVec));
% 
% % Initialize fields of the structs
% resultsMat(1).uncodedErrorBits = [];
% resultsMat(1).codedErrorBits = [];
% resultsMat(1).EVMdB = [];

resultsCell = cell(1,length(noiseVec));

 %FBMC_channelData = [];  %  always empty at the moment
% FBMC_progressReportData = [];

noiseValues = [];

parfor noiseInd = 1:length(noiseVec)
    FBMC_channelDataPar = [];
    FBMC_signalDataPar = [];
    FBMC_parametersPar = FBMC_setNoiseSNRValue(FBMC_parameters, noiseVec(noiseInd));
    
    rng(0, 'twister');
    FBMC_constantsPar = FBMC_pregenerateConstants(FBMC_parametersPar);
    
    resultsCell{noiseInd} = repmat(struct('uncodedErrorBits', [], 'codedErrorBits', [], 'EVMdB', []), niter, 1);
    
    if printReport == 2
        fprintf('%s %4.1f | started  [%s]\n', snrType, noiseVec(noiseInd), datestr(now, 'HH:MM:SS.FFF'));
    end

    for i = 1:niter
        % print iteration progress
        %if printReport == 1
        %    FBMC_progressReportString = sprintf('%s %4.1f', snrType, noiseVec(noiseInd));
        %    FBMC_progressReportData = FBMC_progressReport(length(noiseVec), niter,...
        %        noiseInd, i, FBMC_progressReportData, FBMC_progressReportString, 1, 1);
        %end
        
        % Pass signal through channel and add noise
        [FBMC_channelSystemData, FBMC_channelDataPar, FBMC_signalDataPar] =...
            FBMC_channelAndNoise(FBMC_parametersPar, FBMC_constantsPar,...
                FBMC_txData, FBMC_channelDataPar, FBMC_signalDataPar);
        
        % execute receiver
        FBMC_rxData = FBMC_receiver(FBMC_parametersPar, FBMC_constantsPar,...
                                    FBMC_channelSystemData.signal,...
                                    FBMC_txData.dataBlockCodedBitsNumber,...
                                    FBMC_channelSystemData.No);
                                
        %symbmask = FBMC_constants.dataMask;
        %10*log10(var(FBMC_rxData.dataGrid(symbmask)) - 1)                         

        % I fthe signal was not detected try again
        if ~FBMC_rxData.detected
            continue;
        else
            detectedVec(noiseInd) = 1 + detectedVec(noiseInd);
        end

        % obtain results
        FBMC_results = FBMC_systemIterationResults(FBMC_parametersPar,...
            FBMC_constantsPar, FBMC_txData, FBMC_rxData);
        
        % check if an error ocurred decoding
        decodeErrorFlags = arrayfun(@(x) x.decodedData.criticalError, FBMC_rxData.dataBlock);
        if any(decodeErrorFlags)
            decodeErrorInd = find(decodeErrorFlags);
            error('Error decoding block %d: %s', decodeErrorInd,...
                FBMC_rxData.dataBlock(decodeErrorInd).decodedData.criticalErrorMsg);
        end
        
        % save results
        resultsCell{noiseInd}(detectedVec(noiseInd)) = FBMC_results;
        %resultsMat(detectedVec(noiseInd), noiseInd) = FBMC_results;
    end
    
    noiseValues(noiseInd).symbNo = FBMC_signalDataPar.txNoiseSymbPw;
    noiseValues(noiseInd).symbPw = FBMC_signalDataPar.txSignalSymbPw;
    noiseValues(noiseInd).symbEb = FBMC_signalDataPar.txSignalSymbEb;
    noiseValues(noiseInd).symbUncEb = FBMC_signalDataPar.txSignalSymbUncodedEb;
    
    noiseValues(noiseInd).snr_dB = FBMC_channelSystemData.snr_dB;
    noiseValues(noiseInd).EbNo_dB = FBMC_channelSystemData.EbNo_dB;
    noiseValues(noiseInd).UncodedEbNo_dB = FBMC_channelSystemData.UncodedEbNo_dB;
    
    if printReport == 2
        fprintf('%s %4.1f | finished [%s]\n', snrType, noiseVec(noiseInd), datestr(now, 'HH:MM:SS.FFF'));
    end
end

FBMC_noiseData.noiseValues = noiseValues;

% Assign output values
resultsMat = cell2mat(resultsCell);

% Basic values
FBMC_simulationResults.parameters = FBMC_parameters;
FBMC_simulationResults.constants = FBMC_constants;
FBMC_simulationResults.nIter = niter;

% X Axis data and data Type
FBMC_simulationResults.XAxisDataType = 'noise';
FBMC_simulationResults.XAxisData = FBMC_noiseData;

% Y Axis data
FBMC_simulationResults.YAxisData.detectedVec = detectedVec;
FBMC_simulationResults.YAxisData.resultsMat = resultsMat;

end

