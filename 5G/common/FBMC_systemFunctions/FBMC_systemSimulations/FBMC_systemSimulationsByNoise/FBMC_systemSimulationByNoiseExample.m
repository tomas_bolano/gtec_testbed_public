%% Initializations

clear;
%close all;
format long g
%format short
%clc

%% set random number generator seed
rng(12320)


%% Basic configuration for the simulation
XNoVec = 0:2.5:40; % EbNo or SNR in dB
snrType = 'EbNo'; % 'EbNo' or 'SNR'
noiseAddingType = 'varNo';
niter = 200; % maximum number of iterations per EbNo
saveResults = false; % Save results in a .mat file



%% Parameter initialization

%FBMC_parameters = FBMC_systemExampleOFDMParametersDefinition();
addpath('/home/tomas/gtec_testbed/LTEFDD_PHY/Simulations');
%FBMC_parameters = FBMC_parameters_ofdm;
%FBMC_parameters = FBMC_PIMRC2016_Measurements_NewParameters_OFDM_v2();
%FBMC_parameters = FBMC_PIMRC2016_Measurements_NewParameters_FOFDM();
%FBMC_parameters = FBMC_PIMRC2016_Measurements_NewParameters_PHYDYAS();
%FBMC_parameters = FBMC_PIMRC2016_Measurements_NewParameters_PHYDYAS_v2();
%FBMC_parameters = FBMC_PIMRC2016_Measurements_NewParameters_Hermite_v2
%FBMC_parameters = FBMC_systemExampleHermiteParametersDefinition();

FBMC_parameters = FBMC_systemExampleOFDMParametersDefinition();
%FBMC_parameters = FBMC_systemExampleHermiteParametersDefinition();

%FBMC_parameters = FBMC_thesisExampleOFDMParameters;


md = FBMC_getParametersMetadata(FBMC_parameters);


% Set single datablock configuration

% LTE CQIs
dataBlockModOrders = [4 4 4 4 4 4 16 16 16 64 64 64 64 64 64];
dataBlockCodeRates = [78 120 193 308 449 602 378 490 616,...
                      466 567 666 772 873 948]/1024;
                  
CQIind = 1;

FBMC_parameters.dataBlock = struct();
if strcmp(md.systemType, 'SMT')
    FBMC_parameters.dataBlock.modulation             = 'pam';
    FBMC_parameters.dataBlock.modulationOrder        = dataBlockModOrders(CQIind)/2;
else
    FBMC_parameters.dataBlock.modulation             = 'qam';
    FBMC_parameters.dataBlock.modulationOrder        = dataBlockModOrders(CQIind);
end
FBMC_parameters.dataBlock.modulationNormalize    = true;
FBMC_parameters.dataBlock.modulationEnergyFactor = 1;
FBMC_parameters.dataBlock.uncodedBitsNumber      = dataBlockCodeRates(CQIind);
FBMC_parameters.dataBlock.FECCoderFunction       = {@FBMC_nullFECCoder};
FBMC_parameters.dataBlock.FECDecoderFunction     = {@FBMC_nullFECDecoder};
FBMC_parameters.dataBlock.mappingFunction        = ...
    {@FBMC_dataBlockSquareMapper, 'min', 'max', 'min', 'max'};
    
    
FBMC_parameters.symbolsInterleaving = struct();
FBMC_parameters.symbolsInterleaving.dataBlocks = 1;
FBMC_parameters.symbolsInterleaving.interleaver = 'rnd';
FBMC_parameters.symbolsInterleaving.interleaverData = {0};

%FBMC_parameters.pulseShapping.cyclicPrefixLength = 0;

%FBMC_parameters.dataBlock(4).modulationOrder = 16;

% FBMC_parameters.dataBlock(1).uncodedBitsNumber  = 0.5;
% FBMC_parameters.dataBlock(1).FECCoderFunction   = {@FBMC_MATLABLTEFECCoder};
% FBMC_parameters.dataBlock(1).FECDecoderFunction = {@FBMC_MATLABLTEFECDecoder};
% 
% FBMC_parameters.dataBlock(2).uncodedBitsNumber  = 0.75;
% FBMC_parameters.dataBlock(2).FECCoderFunction   = {@FBMC_MATLABLTEFECCoder};
% FBMC_parameters.dataBlock(2).FECDecoderFunction = {@FBMC_MATLABLTEFECDecoder};


%FBMC_parameters.pilots.pilotsGenerationFunction = {@FBMC_nullPilotIndexer};
FBMC_parameters.pilots.pilotsEnergyFactor = 1;
%FBMC_parameters.pilots.numberAuxiliaryPilots = 0;
FBMC_parameters.pulseShapping.cyclicPrefixLength = 72;

%FBMC_parameters.channelModel.channelGenerationFunctions = {@FBMC_idealChannelModel};
%FBMC_parameters.channelEstimation.channelEstimationFunction = {@FBMC_nullChannelEstimator};
%FBMC_parameters.channelEqualization.channelEqualizationFunction = {@FBMC_nullChannelEqualizator};

%FBMC_parameters.channelEstimation.channelEstimationFunction = {@FBMC_interpChannelEstimator};
%FBMC_parameters.channelEstimation.channelEstimationFunction = {@FBMC_squareChannelEstimator, 'spline'};

FBMC_parameters.channelEstimation.channelEstimationFunction = {@FBMC_MMSEChannelEstimator};
FBMC_parameters.channelEqualization.channelEqualizationFunction = {@FBMC_MMSEChannelEqualizator};

%FBMC_parameters.channelEstimation.channelEstimationFunction = {@FBMC_nullChannelEstimator};
%FBMC_parameters.channelEqualization.channelEqualizationFunction = {@FBMC_nullChannelEqualizator};

FBMC_parameters.synchronization.preambleGenerationFunction = {@FBMC_nullPreambleGenerator};
FBMC_parameters.synchronization.synchronizationFunction = {@FBMC_nullSynchronizator};

FBMC_parameters.channelModel.channelGenerationFunctions = {@FBMC_idealChannelModel};
FBMC_parameters.channelModel.channelGenerationFunctions = {@FBMC_stdchanChannelModel};
%FBMC_parameters.channelModel.stdchanChannelModel.Type = '3gppRAx';
%FBMC_parameters.channelModel.stdchanChannelModel.Type = 'itur3GIAx';
FBMC_parameters.channelModel.stdchanChannelModel.Type = 'itur3GVAx';
FBMC_parameters.channelModel.relativeSpeed = 1e-3/3.6;

FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions = ...
    {{@FBMC_nullSignalProcessing}};


%% Simulation

tic();
FBMC_results = FBMC_systemSimulationByNoise(FBMC_parameters, XNoVec, snrType, noiseAddingType, niter, true);
toc();

if saveResults
    channelName = FBMC_channel2str(FBMC_parameters);
    systemMetadata = FBMC_getParametersMetadata(FBMC_parameters);
    
    fileName = sprintf('FBMC_results_by_noise_%s_%s_I%d_S%d%s', ...
                       systemMetadata.systemId, channelName,...
                       FBMC_parameters.highSpeedEmulation.interpolationFactor,...
                       round(FBMC_parameters.channelModel.relativeSpeed*3.6),...
                       datestr(now, '[YYYY-mm-dd_HH-MM-SS]'));
    save(fileName, 'FBMC_results');
end




%% plot results

FBMC_plotResultsByNoise(FBMC_results,...
                        'linealScalePlot', false,...
                        'codedBERPlot', 1,...
                        'codedXAxisUnits', snrType,...
                        'uncodedXAxisUnits', snrType,...
                        'uncodedStyles', {'-s'},...
                        'evmStyles', {'-s'});
                    
