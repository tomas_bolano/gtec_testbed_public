clear;
close all;


%% Script configuration -------------------------------------------------------
% file selection pattern
pattern = 'FBMC_*.mat';

% filter by date options
filterByDate = 0;
%initDate = datenum('12-05-17 00:00:00','dd-mm-yyyy HH:MM:SS');
initDate = datenum('22-11-2015 2:00:00','dd-mm-yyyy HH:MM:SS');
endDate = datenum('30-11-2015 15:00:00','dd-mm-yyyy HH:MM:SS');


%% Load results
files = dir(pattern);
if numel(files) == 0
    fprintf('No files matched\n');
    return;
end


% load data into a vector of structs
fprintf('Loading data into a vector...\n');
loadedFiles = 0;
for i = 1:numel(files)
    load(files(i).name)

    if filterByDate
        if files(i).datenum > endDate || ...
           files(i).datenum < initDate
            continue
        end
    end

    if ~exist('FBMC_results', 'var')
       warning('The file %s does not contain a variable named FBMC_results', files(i).name);
       continue;
    end
    
    if ~strcmpi(FBMC_results.XAxisDataType, 'noise')
        warning('The file %s does not correspond to a simulation by noise', files(i).name);
        continue;
    end
  
    loadedFiles = loadedFiles+1;
    datavec(loadedFiles).fileData = files(i); %#ok<SAGROW>
    datavec(loadedFiles).FBMC_results = FBMC_results; %#ok<SAGROW>
    fprintf('Loaded %s ...\n', files(i).name);
    clear FBMC_results;
end


if loadedFiles == 0
    fprintf('No valid results found\n');
    return;
else
    fprintf('Loaded %d files\n', loadedFiles);
end


%% Plot results
tic();
FBMC_plotResultsByNoise({datavec.FBMC_results},...
                        'linealScalePlot', true,...
                        'uncodedXAxisUnits', 'SNR',...
                        'uncodedStyles', {'-s'},...
                        'evmStyles', {'-s'},...
                        'bcaNIters', 20);
toc();