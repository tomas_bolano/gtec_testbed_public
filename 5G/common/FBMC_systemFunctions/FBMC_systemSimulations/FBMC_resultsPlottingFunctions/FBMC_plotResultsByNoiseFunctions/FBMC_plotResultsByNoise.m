function FBMC_plotResultsByNoise(results, varargin)
% FBMC_PLOTRESULTSBYNOISE Function to plot results
%
% This function plots the results of the results structure.
% Aditional parameters can be specified using
% Name,Value pairs.
%
% Name-Value Pairs arguments:
%
%
% 'uncodedBERPlot'
%   Plot a figure of the uncoded BER. Default is true.
%
% 'analitycalBERPlot'
%   Plot the analitycal BER superimposed on top on the uncoded BER figure.
%   Default is true.
%
% 'uncodedXAxisUnits'
%   SNR units to use in the X (horizontal) axis for the uncoded BER results.
%   Available values are 'EbNo' and 'SNR'. Default is 'SNR';
%
% 'codedBERPlot'
%   Plot a figure of the coded BER. Default is true.
%
% 'codedUncodedBERPlot'
%   Plot a figure of the coded BER and the uncoded BER superimposed (if
%   the coded BER was plotted). Default is true.
%
% 'codedXAxisUnits'
%   SNR units to use in the X (horizontal) axis for the coded BER results.
%   Available values are 'EbNo', 'codedEbNo' and 'SNR'. Note that when
%   using 'codedEbNo' the results of uncoded BER can not be superimposed.
%   Default is 'SNR'.
%
% 'evmPlot'
%   Plot a figure of the EVM. Default is true.
%
% 'evmXAxisUnits'
%   SNR units to use in the X (horizontal) axis for the EVM results.
%   Available values are 'EbNo' and 'SNR'. Default is 'SNR';
%
% 'linealScalePlot'
%   Use lineal scale plots insteal of log plots. Default is false.
%
% 'bcaEstimator'
%   Function to estimate the confidence intervals. Default is mean.
%
% 'bcaAlpha'
%   Confident level to use for estimating the confidence intervals.
%   Default is 0.05.
%
% 'bcaNIters'
%   Number of iterations to estimate the confidence intervals.
%   Default is 400.
%
% 'uncodedStyles'
%   Cell of line styles to use for plotting the uncoded BER data.
%
% 'codedStyles'
%   Cell of line styles to use for plotting the coded BER data.
%
% 'analitycalFadingStyles'
%   Cell of line styles to use for plotting the analitycal fading BER.
%
% 'analitycalAWGNStyles'
%   Cell of line styles to use for plotting the analitycal awgn BER.
%
% 'evmStyles'
%   Cell of line styles to use for plotting the EVM data.
%

%#ok<*AGROW>


%% Check resultsCell is a cell
%assert(iscell(resultsCell), 'Results input must be a cell array');


%% Initialize variables

% assign default values to non created variables
uncodedBERPlot = true;
analitycalBERPlot = true;
uncodedXAxisUnits = 'SNR';

codedBERPlot = true;
codedUncodedBERPlot = true;
codedXAxisUnits = 'SNR';

evmPlot = true;
evmXAxisUnits = 'SNR';

linealScalePlot = false;

bcaEstimator = @(x)mean(x);
bcaAlpha = 0.05;
bcaNIters = 400;

uncodedStyles = {};
codedStyles = {};
analitycalFadingStyles = {};
analitycalAWGNStyles = {};
evmStyles = {};


% assign values specified in Name-Value pair parameters
i = 1;
while i <= numel(varargin)
    if i+1 > numel(varargin)
        error('Value missing from Name-Value pair');
    end
    value = varargin{i+1};
    switch lower(varargin{i})
        case 'uncodedberplot'
            uncodedBERPlot = value;
        case 'analitycalberplot'
            analitycalBERPlot = value;
        case 'uncodedxaxisunits'
            uncodedXAxisUnits = value;
        case 'codedberplot'
            codedBERPlot = value;
        case 'codeduncodedberplot'
            codedUncodedBERPlot = value;
        case 'codedxaxisunits'
            codedXAxisUnits = value;
        case 'evmplot'
            evmPlot = value;
        case 'evmxaxisunits'
            evmXAxisUnits = value;
        case 'linealscaleplot'
            linealScalePlot = value;
        case 'bcaestimator'
            bcaEstimator = value;
        case 'bcaalpha'
            bcaAlpha = value;
        case 'bcaniters'
            bcaNIters = value;
        case 'uncodedstyles'
            uncodedStyles = value;
        case 'codedstyles'
            codedStyles = value;
        case 'analitycalfadingstyles'
            analitycalFadingStyles = value;
        case 'analitycalawgnstyles'
            analitycalAWGNStyles = value;
        case 'evmstyles'
            evmStyles = value;
        otherwise
            error('Parameter name %s not valid', varargin{i});
    end
    i = i+2;
end


%% Generate uncoded BER data to plot
fprintf('Generating uncoded BER data to plot... ');

uncodedBERPlottingData = FBMC_uncodedBERPlottingDataGeneration(...
    results, bcaEstimator, bcaAlpha, bcaNIters);

fprintf('OK\n');


%% Generate analitycal BER data to plot
fprintf('Generating analitycal BER data to plot... ');

analitycalBERPlottingData = FBMC_analitycalBERPlottingDataGeneration(...
    results);

fprintf('OK\n');


%% Generate coded BER data to plot
fprintf('Generating coded BER data to plot... ');

codedBERPlottingData = FBMC_codedBERPlottingDataGeneration(...
    results, bcaEstimator, bcaAlpha, bcaNIters);

fprintf('OK\n');



%% Generate EVM data to plot
fprintf('Generating EVM data to plot... ');

EVMPlottingData = FBMC_EVMPlottingDataGeneration(...
    results, bcaEstimator, bcaAlpha, bcaNIters);

fprintf('OK\n');


%% Plot uncoded BER results

if uncodedBERPlot
    figure();
    hold on;
    
    fprintf('Plotting uncoded and coded BER... ');
    
    % Plot analitycal BER first
    if isempty(analitycalAWGNStyles),   j = []; else j = 1; end
    if isempty(analitycalFadingStyles), k = []; else k = 1; end

    if analitycalBERPlot        
        for i = 1:numel(analitycalBERPlottingData)
            % FBMC_selectNoise is defined at the end of this function
            plot(FBMC_selectNoise(analitycalBERPlottingData(i).noiseData, uncodedXAxisUnits),...
                 analitycalBERPlottingData(i).berAWGNVec,...
                 analitycalAWGNStyles{j});
            j = mod(j, numel(analitycalAWGNStyles)) + 1;
        end
        
        for i = 1:numel(analitycalBERPlottingData)
            plot(FBMC_selectNoise(analitycalBERPlottingData(i).noiseData, uncodedXAxisUnits),...
                 analitycalBERPlottingData(i).berFadingVec,...
                 analitycalFadingStyles{k});         
            k = mod(k, numel(analitycalFadingStyles)) + 1;  
        end
    end
    
    % Plot uncoded BER results
    if isempty(uncodedStyles), k = []; else k = 1; end
    
    for i = 1:numel(uncodedBERPlottingData)
        errorbar(FBMC_selectNoise(uncodedBERPlottingData(i).XAxisData, uncodedXAxisUnits),...
                 uncodedBERPlottingData(i).bervec,...
                 uncodedBERPlottingData(i).lowerCI,...
                 uncodedBERPlottingData(i).upperCI,...
                 uncodedStyles{k});

        % ik k is [], then the modulus will be [] too
        k = mod(k, numel(uncodedStyles)) + 1;
    end
    
    if ~linealScalePlot
        %set(get(h,'Parent'), 'YScale', 'log');
        set(gca, 'YScale', 'log');

        % set axis y limits
        bervec = uncodedBERPlottingData.bervec;
        ymax = 10.^(floor(max(bervec(~isinf(bervec) & bervec > 0))));
        ymin = 10.^(floor(log10(min((bervec(~isinf(bervec) & bervec > 0))))));
                           
        if isempty(ymax)
            ymax = 1;
        end
        
        if isempty(ymin)
            ymin = 0;
        end
                
        ylim([ymin ymax]);
    end

    % add legend
    if analitycalBERPlot
       legend([{analitycalBERPlottingData.legendAWGN},...
              {analitycalBERPlottingData.legendFading},...
              {uncodedBERPlottingData.legend}]);
    else
            legend({uncodedBERPlottingData.legend});
    end
    
    title('Uncoded Bit Error Rate');
    xlabel(sprintf('%s [dB]', uncodedXAxisUnits));
    ylabel('BER');
    grid on;
    hold off;
    
    fprintf('OK\n');
end


%% Plot EVM

if evmPlot
    figure();
    hold on;
    
    fprintf('Plotting EVM... ');
    
    % Plot coded BER results
    if isempty(evmStyles), k = []; else k = 1; end
    
    for i = 1:numel(EVMPlottingData)
        errorbar(FBMC_selectNoise(EVMPlottingData(i).XAxisData, evmXAxisUnits),...
                 EVMPlottingData(i).evmvec,...
                 EVMPlottingData(i).lowerCI,...
                 EVMPlottingData(i).upperCI,...
                 evmStyles{k});

        % ik k is [], them the modulus will be [] too
        k = mod(k, numel(evmStyles)) + 1;
    end

    % add legend
    legend({EVMPlottingData.legend});
    
    title('EVM');
    xlabel(sprintf('%s [dB]', evmXAxisUnits));
    ylabel('EVM');
    grid on;
    hold off;
    
    fprintf('OK\n');    
end


%% Plot coded BER results

if codedBERPlot
    figure();
    hold on;
    
    fprintf('Plotting coded BER... ');
    
    % Plot coded BER results
    if isempty(codedStyles), k = []; else k = 1; end
    
    for i = 1:numel(codedBERPlottingData)
        errorbar(FBMC_selectNoise(codedBERPlottingData(i).XAxisData, codedXAxisUnits),...
                 codedBERPlottingData(i).bervec,...
                 codedBERPlottingData(i).lowerCI,...
                 codedBERPlottingData(i).upperCI,...
                 codedStyles{k});

        % ik k is [], them the modulus will be [] too
        k = mod(k, numel(codedStyles)) + 1;
    end
    
    if ~linealScalePlot
        %set(get(h,'Parent'), 'YScale', 'log');
        set(gca, 'YScale', 'log');
    end

    % add legend
    legend({codedBERPlottingData.legend});
    
    title('Coded Bit Error Rate');
    xlabel(sprintf('%s [dB]', codedXAxisUnits));
    ylabel('BER');
    grid on;
    hold off;
    
    fprintf('OK\n');
end


%% Plot uncoded and coded BER

if ~isempty(codedBERPlot) && codedUncodedBERPlot && ...
   ~strcmpi(codedXAxisUnits, 'codedEbNo')

    figure();
    hold on;
    
    fprintf('Plotting uncoded BER... ');
    
    % Plot analitycal BER first
    if isempty(analitycalAWGNStyles),   j = []; else j = 1; end
    if isempty(analitycalFadingStyles), k = []; else k = 1; end

    if analitycalBERPlot
        for i = 1:numel(analitycalBERPlottingData)
            % FBMC_selectNoise is defined at the end of this function
            plot(FBMC_selectNoise(analitycalBERPlottingData(i).noiseData, codedXAxisUnits),...
                 analitycalBERPlottingData(i).berAWGNVec,...
                 analitycalAWGNStyles{j});
            j = mod(j, numel(analitycalAWGNStyles)) + 1;
        end
        
        for i = 1:numel(analitycalBERPlottingData)
            plot(FBMC_selectNoise(analitycalBERPlottingData(i).noiseData, codedXAxisUnits),...
                 analitycalBERPlottingData(i).berFadingVec,...
                 analitycalFadingStyles{k});         
            k = mod(k, numel(analitycalFadingStyles)) + 1;  
        end
    end
    
    % Plot uncoded BER results
    if isempty(uncodedStyles), k = []; else k = 1; end
    
    for i = 1:numel(uncodedBERPlottingData)
        errorbar(FBMC_selectNoise(uncodedBERPlottingData(i).XAxisData, codedXAxisUnits),...
                 uncodedBERPlottingData(i).bervec,...
                 uncodedBERPlottingData(i).lowerCI,...
                 uncodedBERPlottingData(i).upperCI,...
                 uncodedStyles{k});

        % ik k is [], them the modulus will be [] too
        k = mod(k, numel(uncodedStyles)) + 1;
    end
    
    % Plot coded BER results
    if isempty(codedStyles), k = []; else k = 1; end
    
    for i = 1:numel(codedBERPlottingData)
        errorbar(FBMC_selectNoise(codedBERPlottingData(i).XAxisData, codedXAxisUnits),...
                 codedBERPlottingData(i).bervec,...
                 codedBERPlottingData(i).lowerCI,...
                 codedBERPlottingData(i).upperCI,...
                 codedStyles{k});

        % ik k is [], them the modulus will be [] too
        k = mod(k, numel(codedStyles)) + 1;
    end
      
    if ~linealScalePlot
        %set(get(h,'Parent'), 'YScale', 'log');
        set(gca, 'YScale', 'log');

        % set axis y limits
        bervec = [uncodedBERPlottingData.bervec codedBERPlottingData.bervec];
        ymax = 10.^(floor(max(bervec(~isinf(bervec) & bervec > 0))));
        ymin = 10.^(floor(log10(min((bervec(~isinf(bervec) & bervec > 0))))));
                           
        if isempty(ymax)
            ymax = 1;
        end
        
        if isempty(ymin)
            ymin = 0;
        end
                
        ylim([ymin ymax]);
    end

    % add legend
    uncodedBERlegend = strcat({'Uncoded '}, {uncodedBERPlottingData.legend});
    codedBERlegend = strcat({'Coded '}, {codedBERPlottingData.legend});
    
    if analitycalBERPlot
       legend([{analitycalBERPlottingData.legendAWGN},...
               {analitycalBERPlottingData.legendFading},...
               uncodedBERlegend,...
               codedBERlegend]);
    else
       legend([uncodedBERlegend,...
               codedBERlegend]);
    end
    
    title('Coded and uncoded Bit Error Rate');
    xlabel(sprintf('%s [dB]', codedXAxisUnits));
    ylabel('BER');
    grid on;
    hold off;
    
    fprintf('OK\n');
end


end


function noiseVec = FBMC_selectNoise(noiseData, snrType)
    switch lower(snrType)
        case 'snr'
            noiseVec = noiseData.snr_dB;
        case 'ebno'
            noiseVec = noiseData.EbNo_dB;
        case 'codedebno'
            noiseVec = noiseData.codedEbNo_dB;
        otherwise
            error('Invalid snr value');
    end
end