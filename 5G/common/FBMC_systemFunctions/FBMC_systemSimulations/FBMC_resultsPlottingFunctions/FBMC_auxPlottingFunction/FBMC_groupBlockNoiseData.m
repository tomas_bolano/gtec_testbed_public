function FBMC_blockNoiseData = FBMC_groupBlockNoiseData(FBMC_results, blockNum)
% FBMC_GROUPBLOCKNOISEDATA Groups the noise data for a block

FBMC_blockNoiseData.snrType = FBMC_results.XAxisData.snrType;
FBMC_blockNoiseData.referenceBlock = FBMC_results.XAxisData.referenceBlock;

FBMC_blockNoiseData.snr_dB = cellfun(@(x) x(blockNum), {FBMC_results.XAxisData.noiseValues.snr_dB});
FBMC_blockNoiseData.EbNo_dB = cellfun(@(x) x(blockNum), {FBMC_results.XAxisData.noiseValues.EbNo_dB});
FBMC_blockNoiseData.codedEbNo_dB = cellfun(@(x) x(blockNum), {FBMC_results.XAxisData.noiseValues.codedEbNo_dB});

end
