function throughputPlottingData = FBMC_throughputPlottingDataGeneration(...
    resultsCell, bcaEstimator, bcaAlpha, bcaNIters)
%FBMC_THROUGHPUTPLOTTINGDATAGENERATION Generates coded BER data to plot
%#ok<*AGROW>

throughputPlottingData = [];

for i = 1:length(resultsCell)
    FBMC_results = resultsCell{i};
    
    if ~FBMC_results.parameters.resultsEvaluation.codedBER
        continue
    end
    
    % generate signal to obtain its lenght
    parameters = FBMC_results.parameters;
    
    % disable preamble to obtain the signal lenght
    parameters.synchronization.preambleGenerationFunction = ...
        {@FBMC_nullPreambleGenerator};
    
    FBMC_txData = FBMC_transmitter(parameters, FBMC_results.constants);
    
    % obtain frame duration in seconds
    framelen = length(FBMC_txData.signal)*parameters.signalGeneration.dt;
    
    % throughput matrix
    tpmat = zeros(max(FBMC_results.YAxisData.detectedVec),...
                  length(FBMC_results.YAxisData.detectedVec));
              
    % assign throughput per frame to tpmat
    for j = 1:size(tpmat,2)
        for k = 1:FBMC_results.YAxisData.detectedVec(j)
            % find the block without errors with the highest number of bits
            maxbits = max(FBMC_results.constants.dataBlockInputUncodedBitsNumber(...
                FBMC_results.YAxisData.resultsMat(k,j).codedErrorBits == 0));
            
            if isempty(maxbits)
                continue;
            end
    
            % find the index of the block
            blocki = find(FBMC_results.constants.dataBlockInputUncodedBitsNumber == maxbits,...
                          1, 'first');
                      
            % obtain coding rate
            codingrate = FBMC_results.constants.dataBlockInputUncodedBitsNumber(blocki)/...
                         FBMC_results.constants.dataBlockBitsNumber(blocki);
                     
            % obtain total number of bits of the frame if the constellation
            % size and coding rate is the same for all blocks
            totalbits = round(sum(FBMC_results.constants.dataBlockSymbolsNumber*...
                        sqrt(FBMC_results.parameters.dataBlock(blocki).modulationOrder)*...
                        codingrate));
            
            tpmat(k,j) = totalbits/framelen;
            
            if j == 12
                a = 1;
            end
        end
    end

    detectedvec = FBMC_results.YAxisData.detectedVec;
    tpvec = zeros(1, size(tpmat,2));
    confidenceInterval = zeros(2, size(tpmat,2));

    for jj = 1:size(tpmat, 2);
        %tpvec(jj) = bcaEstimator(tpmat(1:detectedvec(jj), jj));
        tpvec(jj) = bcaEstimator(tpmat(:, jj));
        confidenceInterval(:,jj) = bootci(bcaNIters, ...
            {bcaEstimator, tpmat(1:detectedvec(jj), jj)}, 'alpha', bcaAlpha);
    end

    if strcmpi(FBMC_results.XAxisDataType, 'noise')
        % We store the noise data for the specific block
        XAxisData.snrType = FBMC_results.XAxisData(1).snrType;
        XAxisData.referenceBlock = FBMC_results.XAxisData(1).referenceBlock;
        XAxisData.snr_dB = arrayfun(@(x) x.snr_dB(blocki), FBMC_results.XAxisData.noiseValues);
        XAxisData.EbNo_dB = arrayfun(@(x) x.EbNo_dB(blocki), FBMC_results.XAxisData.noiseValues);
        XAxisData.codedEbNo_dB = arrayfun(@(x) x.codedEbNo_dB(blocki), FBMC_results.XAxisData.noiseValues);
    else
        XAxisData = FBMC_results.XAxisData;
    end

    throughputPlottingData(i).XAxisData = XAxisData;
    throughputPlottingData(i).XAxisDataType = FBMC_results.XAxisDataType;
    throughputPlottingData(i).tpvec = tpvec;
    throughputPlottingData(i).lowerCI = tpvec - confidenceInterval(1,:);
    throughputPlottingData(i).upperCI = tpvec - confidenceInterval(2,:);

    % Create legend string
    legendstr = sprintf('%s %d-%s %s, B %d, I %d',...
                        FBMC_results.constants.metadata.systemDesc,...
                        FBMC_results.parameters.dataBlock(blocki).modulationOrder,...
                        upper(FBMC_results.parameters.dataBlock(blocki).modulation),...
                        FBMC_results.constants.metadata.channel,...
                        blocki,...
                        FBMC_results.parameters.highSpeedEmulation.interpolationFactor);

    if ~strcmpi(FBMC_results.XAxisDataType, 'speed')
        legendstr = [legendstr sprintf(', S: %4.2f m/s',...
                     FBMC_results.parameters.channelModel.relativeSpeed)];
    end

    throughputPlottingData(i).legend = legendstr;

    % Store also a copy of the results data for each case to plot
    throughputPlottingData(i).results = FBMC_results;
end

end

