function FBMC_groupResults = FBMC_groupUncodedResultsByMod(FBMC_results)
%FBMC_GROUPUNCODEDRESULTSBYMOD Groups uncoded BER and EVM results by modulation
%
%

%#ok<*AGROW>
%#ok<*BDSCA>

FBMC_groupResults = [];

% float equality comparison function 
floateq = @(x,y) (x + 10e3*eps > y) & (x - 10e3*eps < y);

% Iterate and add results
% We search the blocks for each modulation and collect the results. When we
% add the results we also check that the SNR and EbNo values for the block
% we are adding are the same as the ones of the blocks added.
for i = 1:numel(FBMC_results.parameters.dataBlock)
    dataBlockParam = FBMC_results.parameters.dataBlock(i);
    
    if strcmpi(FBMC_results.XAxisDataType, 'noise')
        XAxisData = FBMC_groupBlockNoiseData(FBMC_results, i);
        XAxisData = rmfield(XAxisData, 'codedEbNo_dB');
    else
        XAxisData = FBMC_results.XAxisData;
    end
    
    % Create a matrices of results for the current block
    uncodedBER = zeros(FBMC_results.nIter, numel(FBMC_results.YAxisData.detectedVec));
    EVMdB = zeros(FBMC_results.nIter, numel(FBMC_results.YAxisData.detectedVec));
    
    %resultsMat = repmat(struct(), FBMC_results.nIter,...
    %                    numel(FBMC_results.results.detectedVec));
    
    % Initialize uncodedBER and EVMdB
    for j = 1:size(uncodedBER, 2)
        for k = 1:FBMC_results.YAxisData.detectedVec(j)
            uncodedBER(k,j) = ...
                FBMC_results.YAxisData.resultsMat(k,j).uncodedErrorBits(i)./...
                FBMC_results.constants.dataBlockBitsNumber(i);
            
            EVMdB(k,j) = ...
                FBMC_results.YAxisData.resultsMat(k,j).EVMdB(i);
        end
    end
    
    
    if ~isempty(FBMC_groupResults)
        % Search in FBMC_groupResults if the modulation for the block i
        % already exists
        searchModMask = ...
            strcmpi(dataBlockParam.modulation, {FBMC_groupResults.modulation}) &...
            dataBlockParam.modulationOrder == [FBMC_groupResults.modulationOrder];
        
        if any(searchModMask)
            groupi = find(searchModMask);

            FBMC_groupResults(groupi).blocks = [FBMC_groupResults(groupi).blocks i];
            
            if strcmpi(FBMC_results.XAxisDataType, 'noise')
                assert(all(floateq(FBMC_groupResults(groupi).XAxisData.EbNo_dB,...
                                   XAxisData.EbNo_dB)),...
                       'EbNo_dB fields do not match');
                assert(all(floateq(FBMC_groupResults(groupi).XAxisData.snr_dB,...
                                   XAxisData.snr_dB)),...
                       'snr_dB fields do not match');
            end
            
            % Extend results matrices
            uncodedBER2 = [FBMC_groupResults(groupi).YAxisData.uncodedBER;...
                zeros(FBMC_results.nIter, numel(FBMC_results.YAxisData.detectedVec))];
            
            EVMdB2 = [FBMC_groupResults(groupi).YAxisData.EVMdB;...
                zeros(FBMC_results.nIter, numel(FBMC_results.YAxisData.detectedVec))];
            
            % Add new results
            for j = 1:size(uncodedBER2, 2)
                % we store in k the current total valid results stored
                k = FBMC_groupResults(groupi).YAxisData.detectedVec(j)+1;
                for l = 1:FBMC_results.YAxisData.detectedVec(j)
                    uncodedBER2(k,j) = ...
                        FBMC_results.YAxisData.resultsMat(l,j).uncodedErrorBits(i)./...
                        FBMC_results.constants.dataBlockBitsNumber(i);

                    EVMdB2(k,j) = ...
                        FBMC_results.YAxisData.resultsMat(l,j).EVMdB(i);
                    
                    k = k+1;
                end
            end
            
            % Save results
            FBMC_groupResults(groupi).YAxisData.uncodedBER = uncodedBER2;
            FBMC_groupResults(groupi).YAxisData.EVMdB = EVMdB2;

            % Update detectedVec for the block i
            FBMC_groupResults(groupi).YAxisData.detectedVec = ...
                FBMC_groupResults(groupi).YAxisData.detectedVec +...
                FBMC_results.YAxisData.detectedVec;
            
            continue;
        end
    end
    
    % Add a new alement at the end
    FBMC_groupResults(end+1).modulation = dataBlockParam.modulation;
    
    % Add the rest of the fields to the end element
    FBMC_groupResults(end).modulationOrder = dataBlockParam.modulationOrder;
    FBMC_groupResults(end).blocks = i;
    FBMC_groupResults(end).XAxisData = XAxisData;
    FBMC_groupResults(end).XAxisDataType = FBMC_results.XAxisDataType;
    
    % If the simulation is not iterating by noise there will be also a
    % noiseData field.
    if ~strcmpi(FBMC_results.XAxisDataType, 'noise')
        FBMC_groupResults(end).noiseData = FBMC_results.noiseData;
    end
    
    % detectedVec stores the number of valid elements in the result
    % matrices for a given noise value (elements in columns)
    FBMC_groupResults(end).YAxisData.detectedVec = FBMC_results.YAxisData.detectedVec;
    
    % Add results
    FBMC_groupResults(end).YAxisData.uncodedBER = uncodedBER;
    FBMC_groupResults(end).YAxisData.EVMdB = EVMdB;         
end

end
