function FBMC_groupResults = FBMC_createGroupedUncodedResults(FBMC_resultsCell)
%FBMC_CREATEGGROUPEDUNCODEDRESULTS Groups several uncoded BER and EVM results
%
%

%#ok<*AGROW>
%#ok<*BDSCA>


FBMC_groupResults = cell(1,length(FBMC_resultsCell));


for i = 1:length(FBMC_resultsCell)
    FBMC_groupResults{i} = FBMC_groupUncodedResultsByMod(FBMC_resultsCell{i});
    
    % Add parameters and constants
    [FBMC_groupResults{i}.parameters] = deal(FBMC_resultsCell{i}.parameters);
    [FBMC_groupResults{i}.constants] = deal(FBMC_resultsCell{i}.constants);
end

% Concatenate results into a struct array
FBMC_groupResults = [FBMC_groupResults{:}];

end
