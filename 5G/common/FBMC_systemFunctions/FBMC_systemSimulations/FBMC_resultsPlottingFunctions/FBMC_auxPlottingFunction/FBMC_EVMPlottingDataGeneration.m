function EVMBERPlottingData = FBMC_EVMPlottingDataGeneration(...
    results, bcaEstimator, bcaAlpha, bcaNIters)
%FBMC_EVMPLOTTINGDATAGENERATION Generates EVM data to plot
%#ok<*AGROW>

EVMBERPlottingData = [];
k = 1;


if ~results.parameters.resultsEvaluation.EVM
    return
end

evmmat = 10.^(arrayfun(@(x) x.EVMdB, results.YAxisData.resultsMat)/10);
evmvec = zeros(1, size(evmmat,2));
detectedvec = results.YAxisData.detectedVec;
confidenceInterval = zeros(2, size(evmmat,2));

for j = 1:size(evmmat, 2);
    evmvec(j) = bcaEstimator(evmmat(1:detectedvec(j), j));
    confidenceInterval(:,j) = bootci(bcaNIters, ...
        {bcaEstimator, evmmat(1:detectedvec(j), j)}, 'alpha', bcaAlpha);
end

if strcmpi(results.XAxisDataType, 'noise')
    % We store the noise data for the specific block
    XAxisData.snrType = results.XAxisData.snrType;
    XAxisData.referenceBlock = results.XAxisData.referenceBlock;
    XAxisData.snr_dB = arrayfun(@(x) x.snr_dB, results.XAxisData.noiseValues);
    XAxisData.EbNo_dB = arrayfun(@(x) x.EbNo_dB, results.XAxisData.noiseValues);
    %XAxisData.codedEbNo_dB = arrayfun(@(x) x.UncodedEbNo_dB, results.XAxisData.noiseValues);
else
    XAxisData = results.XAxisData;
end

EVMBERPlottingData(k).XAxisData = XAxisData;
EVMBERPlottingData(k).XAxisDataType = results.XAxisDataType;
EVMBERPlottingData(k).evmvec = 10*log10(evmvec);
EVMBERPlottingData(k).lowerCI = 10*log10(evmvec)-10*log10(confidenceInterval(1,:));
EVMBERPlottingData(k).upperCI = 10*log10(evmvec)-10*log10(confidenceInterval(2,:));

% Create legend string
try
    paramId = results.parameters.id;
    systemId = results.constants.metadata.systemDesc;
    systemIdStr = sprintf('%s(%s)', systemId, paramId);
catch
    systemId = results.constants.metadata.systemDesc;
    systemIdStr = sprintf('%s', systemId);
end

legendstr = sprintf('%s %d-%s %s, I %d',...
                    systemIdStr,...
                    results.parameters.dataBlock.modulationOrder,...
                    upper(results.parameters.dataBlock.modulation),...
                    results.constants.metadata.channel,...
                    results.parameters.highSpeedEmulation.interpolationFactor,...
                    results.parameters.channelModel.relativeSpeed);

if ~strcmpi(results.XAxisDataType, 'speed')
    legendstr = [legendstr sprintf(', S: %4.2f m/s',...
                 results.parameters.channelModel.relativeSpeed)];
end

EVMBERPlottingData(k).legend = legendstr;

% Store also a copy of the results data for each case to plot
EVMBERPlottingData(k).results = results;


end

