function uncodedBERPlottingData = FBMC_uncodedBERPlottingDataGeneration(...
    results, bcaEstimator, bcaAlpha, bcaNIters)
%FBMC_UNCODEDBERPLOTTINGDATAGENERATION2 Generates uncoded BER data to plot
%#ok<*AGROW>

uncodedBERPlottingData = [];
k = 1;
 
if ~results.parameters.resultsEvaluation.uncodedBER
    return
end

bermat = arrayfun(@(x) x.uncodedErrorBits, results.YAxisData.resultsMat)/...
         results.constants.dataBlockTotalBitsNumber;
detectedvec = results.YAxisData.detectedVec;
bervec = zeros(1, size(bermat,2));
confidenceInterval = zeros(2, size(bermat,2));

for j = 1:size(bermat, 2);
    bervec(j) = bcaEstimator(bermat(1:detectedvec(j), j));
    confidenceInterval(:,j) = bootci(bcaNIters, ...
        {bcaEstimator, bermat(1:detectedvec(j), j)}, 'alpha', bcaAlpha);
end

if strcmpi(results.XAxisDataType, 'noise')
    % We store the noise data for the specific block
    XAxisData.snrType = results.XAxisData(1).snrType;
    XAxisData.referenceBlock = results.XAxisData(1).referenceBlock;
    XAxisData.snr_dB = arrayfun(@(x) x.snr_dB, results.XAxisData.noiseValues);
    XAxisData.EbNo_dB = arrayfun(@(x) x.EbNo_dB, results.XAxisData.noiseValues);
    %XAxisData.codedEbNo_dB = arrayfun(@(x) x.UncodedEbNo_dB, results.XAxisData.noiseValues);
else
    XAxisData = results.XAxisData;
end

uncodedBERPlottingData(k).XAxisData = XAxisData;
uncodedBERPlottingData(k).XAxisDataType = results.XAxisDataType;
uncodedBERPlottingData(k).bervec = bervec;
uncodedBERPlottingData(k).lowerCI = bervec - confidenceInterval(1,:);
uncodedBERPlottingData(k).upperCI = bervec - confidenceInterval(2,:);

% Create legend string
try
    paramId = results.parameters.id;
    systemId = results.constants.metadata.systemDesc;
    systemIdStr = sprintf('%s(%s)', systemId, paramId);
catch
    systemId = results.constants.metadata.systemDesc;
    systemIdStr = sprintf('%s', systemId);
end

legendstr = sprintf('%s %d-%s %s, I %d',...
                    systemIdStr,...
                    results.parameters.dataBlock.modulationOrder,...
                    upper(results.parameters.dataBlock.modulation),...
                    results.constants.metadata.channel,...
                    results.parameters.highSpeedEmulation.interpolationFactor);

if ~strcmpi(results.XAxisDataType, 'speed')
    legendstr = [legendstr sprintf(', S: %4.2f m/s',...
                 results.parameters.channelModel.relativeSpeed)];
end

uncodedBERPlottingData(k).legend = legendstr;

% Store also a copy of the results data for each case to plot
uncodedBERPlottingData(k).results = results;


end


