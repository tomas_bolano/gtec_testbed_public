function codedBERPlottingData = FBMC_codedBERPlottingDataGeneration(...
    results, bcaEstimator, bcaAlpha, bcaNIters)
%FBMC_CODEDBERPLOTTINGDATAGENERATION Generates coded BER data to plot
%#ok<*AGROW>

codedBERPlottingData = [];
k = 1;

    
if ~results.parameters.resultsEvaluation.codedBER
    return
end

% extract matrix of error bits from results
codedErrorBitsMat = arrayfun(@(x) x.codedErrorBits,...
                             results.YAxisData.resultsMat);

bermat = codedErrorBitsMat/...
         results.constants.dataBlockInputUncodedBitsNumber;
detectedvec = results.YAxisData.detectedVec;
bervec = zeros(1, size(bermat,2));
confidenceInterval = zeros(2, size(bermat,2));

for jj = 1:size(bermat, 2);
    bervec(jj) = bcaEstimator(bermat(1:detectedvec(jj), jj));
    confidenceInterval(:,jj) = bootci(bcaNIters, ...
        {bcaEstimator, bermat(1:detectedvec(jj), jj)}, 'alpha', bcaAlpha);
end

if strcmpi(results.XAxisDataType, 'noise')
    % We store the noise data for the specific block
    XAxisData.snrType = results.XAxisData(1).snrType;
    XAxisData.referenceBlock = results.XAxisData(1).referenceBlock;
    XAxisData.snr_dB = arrayfun(@(x) x.snr_dB, results.XAxisData.noiseValues);
    XAxisData.EbNo_dB = arrayfun(@(x) x.EbNo_dB, results.XAxisData.noiseValues);
    %XAxisData.codedEbNo_dB = arrayfun(@(x) x.UncodedEbNo_dB, results.XAxisData.noiseValues);
else
    XAxisData = results.XAxisData;
end

codedBERPlottingData(k).XAxisData = XAxisData;
codedBERPlottingData(k).XAxisDataType = results.XAxisDataType;
codedBERPlottingData(k).bervec = bervec;
codedBERPlottingData(k).lowerCI = bervec - confidenceInterval(1,:);
codedBERPlottingData(k).upperCI = bervec - confidenceInterval(2,:);

% Create legend string
legendstr = sprintf('%s %d-%s %s, I %d',...
                    results.constants.metadata.systemDesc,...
                    results.parameters.dataBlock.modulationOrder,...
                    upper(results.parameters.dataBlock.modulation),...
                    results.constants.metadata.channel,...
                    results.parameters.highSpeedEmulation.interpolationFactor);

if ~strcmpi(results.XAxisDataType, 'speed')
    legendstr = [legendstr sprintf(', S: %4.2f m/s',...
                 results.parameters.channelModel.relativeSpeed)];
end

codedBERPlottingData(k).legend = legendstr;

% Store also a copy of the results data for each case to plot
codedBERPlottingData(k).results = results;


end

