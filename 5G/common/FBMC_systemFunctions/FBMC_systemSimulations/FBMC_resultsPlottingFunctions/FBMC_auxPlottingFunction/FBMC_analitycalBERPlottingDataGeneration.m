function analitycalBERPlottingData = ...
    FBMC_analitycalBERPlottingDataGeneration(results)
%FBMC_ANALITYCALBERPLOTTINGDATAGENERATION2 Generates analitycal BER data to plot
%
% Currently only for simulations interating by noise
%
%#ok<*AGROW>

% Array to store analitycal BER data
analitycalBERPlottingData = [];
i = 1;


if ~results.parameters.resultsEvaluation.uncodedBER
    return
end

try
    paramId = results.parameters.id;
    systemId = results.constants.metadata.systemDesc;
    systemIdStr = sprintf('%s(%s)', systemId, paramId);
catch
    systemId = results.constants.metadata.systemDesc;
    systemIdStr = sprintf('%s', systemId);
end

analitycal_mod = results.parameters.dataBlock.modulation;
analitycal_modorder = results.parameters.dataBlock.modulationOrder;
analitycal_modstr = sprintf('%d-%s', analitycal_modorder, analitycal_mod);

noiseData.snr_dB = [results.XAxisData.noiseValues.snr_dB];
noiseData.EbNo_dB = [results.XAxisData.noiseValues.EbNo_dB];

% if strcmp(systemId, 'OFDM') && ...
%    (strcmp(results.XAxisData.snrType, 'SNR') || ...
%     ~results.parameters.channelModel.noise.considerOFDMCP)
%     %adjust EbNo in OFDM to not consider the CP
%     cplen = results.parameters.pulseShapping.cyclicPrefixLength;
%     nsubc = results.parameters.basicParameters.subcarriersNumber;
%     noiseData.EbNo_dB = 10*log10(10.^(noiseData.EbNo_dB/10)*(nsubc/(nsubc+cplen)));
% end

%     if strcmp(results.XAxisData.snrType, 'SNR')
%         % For SNR, remove the overhead of the pilots on the energy
%         results.constants.dataMask
%         results.constants.pilots.ActualPilotMask
%         noiseData.EbNo_dB 
%     end

berFadingVec = berfading(noiseData.EbNo_dB, analitycal_mod, analitycal_modorder, 1);
berAWGNVec = berawgn(noiseData.EbNo_dB, analitycal_mod, analitycal_modorder, 1);

analitycalBERPlottingData(i).noiseData = noiseData;
analitycalBERPlottingData(i).berFadingVec = berFadingVec;
analitycalBERPlottingData(i).berAWGNVec = berAWGNVec;

legendFadingStr = [strrep(systemIdStr, '_', ' ') ' ',...
                   analitycal_modstr ' Rayleigh analytical'];
legendAWGNStr = [strrep(systemIdStr, '_', ' ') ' ',...
                 analitycal_modstr ' AWGN analytical'];

analitycalBERPlottingData(i).legendFading = legendFadingStr;
analitycalBERPlottingData(i).legendAWGN = legendAWGNStr;


end

