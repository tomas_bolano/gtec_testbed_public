function FBMC_plotResultsBySpeed(results, varargin)
% FBMC_PLOTRESULTSBYSPEED Function to plot results
%
% This function plots the results of the results structures in the 
% resultsCell input cell. Aditional parameters can be specified using
% Name,Value pairs.
%
% Name-Value Pairs arguments:
%
%
% 'uncodedBERPlot'
%   Plot a figure of the uncoded BER. Default is true.
%
% 'codedBERPlot'
%   Plot a figure of the coded BER. Default is true.
%
% 'codedUncodedBERPlot'
%   Plot a figure of the coded BER and the uncoded BER superimposed (if
%   the coded BER was plotted). Default is true.
%
% 'evmPlot'
%   Plot a figure of the EVM. Default is true.
%
% 'linealScalePlot'
%   Use lineal scale plots insteal of log plots. Default is false.
%
% 'bcaEstimator'
%   Function to estimate the confidence intervals. Default is mean.
%
% 'bcaAlpha'
%   Confident level to use for estimating the confidence intervals.
%   Default is 0.05.
%
% 'bcaNIters'
%   Number of iterations to estimate the confidence intervals.
%   Default is 400.
%
% 'uncodedStyles'
%   Cell of line styles to use for plotting the uncoded BER data.
%
% 'codedStyles'
%   Cell of line styles to use for plotting the coded BER data.
%
% 'evmStyles'
%   Cell of line styles to use for plotting the EVM data.
%

%#ok<*AGROW>



%% Initialize variables

% assign default values to non created variables
uncodedBERPlot = true;

codedBERPlot = true;
codedUncodedBERPlot = true;

evmPlot = true;

linealScalePlot = false;

bcaEstimator = @(x)mean(x);
bcaAlpha = 0.05;
bcaNIters = 400;

uncodedStyles = {};
codedStyles = {};
evmStyles = {};


% assign values specified in Name-Value pair parameters
i = 1;
while i <= numel(varargin)
    if i+1 > numel(varargin)
        error('Value missing from Name-Value pair');
    end
    value = varargin{i+1};
    switch lower(varargin{i})
        case 'uncodedberplot'
            uncodedBERPlot = value;
        case 'codedberplot'
            codedBERPlot = value;
        case 'codeduncodedberplot'
            codedUncodedBERPlot = value;
        case 'evmplot'
            evmPlot = value;
        case 'linealscaleplot'
            linealScalePlot = value;
        case 'bcaestimator'
            bcaEstimator = value;
        case 'bcaalpha'
            bcaAlpha = value;
        case 'bcaniters'
            bcaNIters = value;
        case 'uncodedstyles'
            uncodedStyles = value;
        case 'codedstyles'
            codedStyles = value;
        case 'evmstyles'
            evmStyles = value;
        otherwise
            error('Parameter name %s not valid', varargin{i});
    end
    i = i+2;
end


%% Generate uncoded BER data to plot
fprintf('Generating uncoded BER data to plot... ');

uncodedBERPlottingData = FBMC_uncodedBERPlottingDataGeneration(...
    results, bcaEstimator, bcaAlpha, bcaNIters);

fprintf('OK\n');


%% Generate coded BER data to plot
codedBERPlottingData = FBMC_codedBERPlottingDataGeneration(...
    results, bcaEstimator, bcaAlpha, bcaNIters);

fprintf('OK\n');


%% Generate EVM data to plot
fprintf('Generating EVM data to plot... ');

EVMPlottingData = FBMC_EVMPlottingDataGeneration(...
    results, bcaEstimator, bcaAlpha, bcaNIters);

fprintf('OK\n');


%% Plot uncoded BER results

if uncodedBERPlot
    figure();
    hold on;
    
    fprintf('Plotting uncoded and coded BER... ');
    
    % Plot uncoded BER results
    if isempty(uncodedStyles), k = []; else k = 1; end
    
    for i = 1:numel(uncodedBERPlottingData)
        errorbar(3.6*uncodedBERPlottingData(i).XAxisData.speedValues*...
                 uncodedBERPlottingData(i).XAxisData.interpolationFactor,...
                 uncodedBERPlottingData(i).bervec,...
                 uncodedBERPlottingData(i).lowerCI,...
                 uncodedBERPlottingData(i).upperCI,...
                 uncodedStyles{k});

        % ik k is [], then the modulus will be [] too
        k = mod(k, numel(uncodedStyles)) + 1;
    end
    
    if ~linealScalePlot
        %set(get(h,'Parent'), 'YScale', 'log');
        set(gca, 'YScale', 'log');

        % set axis y limits
        bervec = uncodedBERPlottingData.bervec;
        ymax = 10.^(floor(max(bervec(~isinf(bervec) & bervec > 0))));
        ymin = 10.^(floor(log10(min((bervec(~isinf(bervec) & bervec > 0))))));
                           
        if isempty(ymax)
            ymax = 1;
        end
        
        if isempty(ymin)
            ymin = 0;
        end
                
        ylim([ymin ymax]);
    end
    
    legend(uncodedBERPlottingData.legend);

    title('Uncoded Bit Error Rate');
    xlabel(sprintf('emulated speed [km/h]'));
    ylabel('BER');
    grid on;
    hold off;
    
    fprintf('OK\n');
end


%% Plot EVM

if evmPlot
    figure();
    hold on;
    
    fprintf('Plotting EVM... ');
    
    % Plot coded BER results
    if isempty(evmStyles), k = []; else k = 1; end
    
    for i = 1:numel(EVMPlottingData)
        errorbar(3.6*EVMPlottingData(i).XAxisData.speedValues*...
                 EVMPlottingData(i).XAxisData.interpolationFactor,...
                 EVMPlottingData(i).evmvec,...
                 EVMPlottingData(i).lowerCI,...
                 EVMPlottingData(i).upperCI,...
                 evmStyles{k});

        % ik k is [], them the modulus will be [] too
        k = mod(k, numel(evmStyles)) + 1;
    end

    % add legend
    legend({EVMPlottingData.legend});
    
    title('EVM');
    xlabel(sprintf('emulated speed [km/h]'));
    ylabel('EVM');
    grid on;
    hold off;
    
    fprintf('OK\n');    
end


%% Plot coded BER results

if codedBERPlot
    figure();
    hold on;
    
    fprintf('Plotting coded BER... ');
    
    % Plot coded BER results
    if isempty(codedStyles), k = []; else k = 1; end
    
    for i = 1:numel(codedBERPlottingData)
        errorbar(3.6*codedBERPlottingData(i).XAxisData.speedValues*...
                 codedBERPlottingData(i).XAxisData.interpolationFactor,...
                 codedBERPlottingData(i).bervec,...
                 codedBERPlottingData(i).lowerCI,...
                 codedBERPlottingData(i).upperCI,...
                 codedStyles{k});

        % ik k is [], them the modulus will be [] too
        k = mod(k, numel(codedStyles)) + 1;
    end
    
    if ~linealScalePlot
        %set(get(h,'Parent'), 'YScale', 'log');
        set(gca, 'YScale', 'log');
    end

    % add legend
    legend({codedBERPlottingData.legend});
    
    title('Coded Bit Error Rate');
    xlabel(sprintf('emulated speed [km/h]'));
    ylabel('BER');
    grid on;
    hold off;
    
    fprintf('OK\n');
end


%% Plot uncoded and coded BER

if ~isempty(codedBERPlot) && codedUncodedBERPlot

    figure();
    hold on;
    
    fprintf('Plotting uncoded BER... ');
    
    % Plot uncoded BER results
    if isempty(uncodedStyles), k = []; else k = 1; end
    
    for i = 1:numel(uncodedBERPlottingData)
        errorbar(3.6*uncodedBERPlottingData(i).XAxisData.speedValues*...
                 uncodedBERPlottingData(i).XAxisData.interpolationFactor,...
                 uncodedBERPlottingData(i).bervec,...
                 uncodedBERPlottingData(i).lowerCI,...
                 uncodedBERPlottingData(i).upperCI,...
                 uncodedStyles{k});

        % ik k is [], them the modulus will be [] too
        k = mod(k, numel(uncodedStyles)) + 1;
    end
    
    % Plot coded BER results
    if isempty(codedStyles), k = []; else k = 1; end
    
    for i = 1:numel(codedBERPlottingData)
        errorbar(3.6*codedBERPlottingData(i).XAxisData.speedValues*...
                 codedBERPlottingData(i).XAxisData.interpolationFactor,...
                 codedBERPlottingData(i).bervec,...
                 codedBERPlottingData(i).lowerCI,...
                 codedBERPlottingData(i).upperCI,...
                 codedStyles{k});

        % ik k is [], them the modulus will be [] too
        k = mod(k, numel(codedStyles)) + 1;
    end
    
    if ~linealScalePlot
        %set(get(h,'Parent'), 'YScale', 'log');
        set(gca, 'YScale', 'log');

        % set axis y limits
        bervec = [uncodedBERPlottingData.bervec codedBERPlottingData.bervec];
        ymax = 10.^(floor(max(bervec(~isinf(bervec) & bervec > 0))));
        ymin = 10.^(floor(log10(min((bervec(~isinf(bervec) & bervec > 0))))));
                           
        if isempty(ymax)
            ymax = 1;
        end
        
        if isempty(ymin)
            ymin = 0;
        end
                
        ylim([ymin ymax]);
    end

    % add legend
    uncodedBERlegend = strcat({'Uncoded '}, {uncodedBERPlottingData.legend});
    codedBERlegend = strcat({'Coded '}, {codedBERPlottingData.legend});
    
    legend([uncodedBERlegend, codedBERlegend]);

    title('Coded and uncoded Bit Error Rate');
    xlabel(sprintf('emulated speed [km/h]'));
    ylabel('BER');
    grid on;
    hold off;
    
    fprintf('OK\n');
end


end
