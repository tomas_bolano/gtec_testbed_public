clear;
close all;


%% Script configuration -------------------------------------------------------
% file selection pattern
pattern = 'FBMC_*.mat';

% filter by date options
filterByDate = 0;
%initDate = datenum('12-05-17 00:00:00','dd-mm-yyyy HH:MM:SS');
initDate = datenum('22-11-2015 2:00:00','dd-mm-yyyy HH:MM:SS');
endDate = datenum('30-11-2015 15:00:00','dd-mm-yyyy HH:MM:SS');


%% Initialize variables -------------------------------------------------------
styles = {'sb-', 'sm-', 'sk-',...
          'xb-', 'xm-', 'xk-',...
          '*b-', '*m-', '*k-',...
          'ob-', 'om-', 'ok-',...
          '^b-', '^m-', '^k-'};
linewidth = 1;
logscale = 0;

files = dir(pattern);
if numel(files) == 0
    fprintf('No files matched\n');
    return;
end


bcaEstimator = @(x)mean(x);
bcaRelativeErrorEstimator = @(x,y)mean(100*(x-y)./y);
bcaAlpha = 0.05; % Confidence level
bcaNIters = 400; % Numer of bootstrap iterations


% load data into a vector of structs
fprintf('Loading data into a vector...\n');
loadedFiles = 0;
for i = 1:numel(files)
    load(files(i).name)

    if filterByDate
        if files(i).datenum > endDate || ...
           files(i).datenum < initDate
            continue
        end
    end

    if ~exist('FBMC_results', 'var')
       warning('The file %s does not contain a variable named FBMC_results', files(i).name);
       continue;
    end
    
    if ~strcmp(FBMC_results.type, 'bySpeed')
        warning('The file %s does not correspond to a simulation by Speed', files(i).name);
        continue;
    end
  
    loadedFiles = loadedFiles+1;
    datavec(loadedFiles).fileData = files(i); %#ok<SAGROW>
    datavec(loadedFiles).FBMC_results = FBMC_results; %#ok<SAGROW>
    fprintf('Loaded %s ...\n', files(i).name);
    clear FBMC_results;
end


if loadedFiles == 0
    fprintf('No valid results found\n');
    return;
else
    fprintf('Loaded %d files\n', loadedFiles);
end




%% plot BER curves ------------------------------------------------------------
fprintf('Plotting BER results ...\n');

figure();
legendData = cell(1,loadedFiles);
for i = 1:loadedFiles
    FBMC_results = datavec(i).FBMC_results;
    
    bermat = FBMC_results.results.errorBitsMat/FBMC_results.bitsPerIter;
    bervec = bcaEstimator(bermat);
    confidenceInterval = bootci(bcaNIters,{bcaEstimator,bermat},'alpha',bcaAlpha);
    
    emulatedKmH = FBMC_results.speedVec*...
                  FBMC_results.parameters.highSpeedEmulation.interpolationFactor*3.6;
    h = errorbar(emulatedKmH, bervec,...
                 bervec-confidenceInterval(1,:),...
                 bervec-confidenceInterval(2,:),...
                 styles{mod(i-1,numel(styles))+1},'LineWidth', linewidth);
    if logscale
        set(get(h,'Parent'), 'YScale', 'log')
    end
    hold on
    
    legendData{i} = sprintf('%s %s - int:%d - mod order:%d %s',...
                            FBMC_results.metadata.systemDesc,...
                            FBMC_results.metadata.channel,...
                            FBMC_results.parameters.highSpeedEmulation.interpolationFactor,...
                            FBMC_results.parameters.basicParameters.modulationOrder,...
                            FBMC_results.metadata.modulation);
end
clear simulationData;

grid on;
hold off;
legend(legendData{:});
title(sprintf('Bit Error Rate for %s = %d',...
              datavec(1).FBMC_results.noiseData.noiseValueType,...
              datavec(1).FBMC_results.noiseData.noiseDB));
xlabel('Emulated Speed [km/h]');
ylabel('BER'); 


%% plot EVM curves ------------------------------------------------------------
fprintf('Plotting EVM results ...\n');

figure();
legendData = cell(1,loadedFiles);
for i = 1:loadedFiles
    FBMC_results = datavec(i).FBMC_results;

    evmmat = 10.^(FBMC_results.results.EVMdBMat/10);
    evmvec = bcaEstimator(evmmat);
    confidenceInterval = bootci(bcaNIters,{bcaEstimator,evmmat},'alpha',bcaAlpha);
    
    emulatedKmH = FBMC_results.speedVec*...
                  FBMC_results.parameters.highSpeedEmulation.interpolationFactor*3.6;
              
    h = errorbar(emulatedKmH, 10*log10(evmvec),...
                 10*log10(evmvec)-10*log10(confidenceInterval(1,:)),...
                 10*log10(evmvec)-10*log10(confidenceInterval(2,:)),...
                 styles{mod(i-1,numel(styles))+1},'LineWidth', linewidth);
    %set(get(h,'Parent'), 'YScale', 'log')
    hold on
    
    legendData{i} = sprintf('%s %s - int:%d - mod order:%d %s',...
                            FBMC_results.metadata.systemDesc,...
                            FBMC_results.metadata.channel,...
                            FBMC_results.parameters.highSpeedEmulation.interpolationFactor,...
                            FBMC_results.parameters.basicParameters.modulationOrder,...
                            FBMC_results.metadata.modulation);
end
grid on;
hold off;
legend(legendData{1:loadedFiles});
title(sprintf('Error Vector Magnitude for %s = %d',...
               datavec(1).FBMC_results.noiseData.noiseValueType,...
               datavec(1).FBMC_results.noiseData.noiseDB));
xlabel('Emulated Speed [km/h]');
ylabel('EVM [dB]'); 


%% plot relative error curves -------------------------------------------------
return
fprintf('Plotting Relative Error results ...\n');

systems = arrayfun(@(x) x.FBMC_results.metadata.systemDesc, datavec,...
                   'UniformOutput', false);
systemcases = unique(systems);

h1=figure('Visible', 'off');
h2=figure('Visible', 'off');

for i=1:numel(systemcases)
    % Boolean mask of this case in datavec
    caseMask = strcmp(systems, systemcases(i));
    
    % obtain all the possible interpolation values for this case
    allInterpValues = arrayfun(...
        @(x) x.FBMC_results.parameters.highSpeedEmulation.interpolationFactor, datavec);
    uniqueInterpValues = unique(allInterpValues);
    
    if numel(uniqueInterpValues) < 2
        continue
    end
    
    % indexes of the element of datavec for each interpolation value
    % a zero will mean that there is no data for that interpolation value
    interpInd = zeros(1,max(uniqueInterpValues)); 
    for j = 1:max(uniqueInterpValues)
        interpMask = allInterpValues == j & caseMask;
        if nnz(interpMask) == 0
            continue
        end
        if nnz(interpMask) > 1
            warning(['There is more than one interpolation value of %d for ',...
                     '%s'], j, systemcases{i});
        end
        interpInd(j) = find(interpMask, 1);
    end
    
    % matrix of interpolation values to compare
    %interpCmpCases = [2,1; 3,2; 3,1];
    interpCmpCases = fliplr(nchoosek(uniqueInterpValues, 2));
    
    legendData = cell(1,size(interpCmpCases,2));
    plotInd = 0;
    for j = 1:size(interpCmpCases,1)
        if interpInd(interpCmpCases(j,1)) == 0 ||...
           interpInd(interpCmpCases(j,2)) == 0
            continue;
        end
        plotInd = plotInd + 1;
        
        fileData1 = datavec(interpInd(interpCmpCases(j,1))).fileData;
        fileData2 = datavec(interpInd(interpCmpCases(j,2))).fileData;
            
        dataCase1 = datavec(interpInd(interpCmpCases(j,1))).FBMC_results;
        dataCase2 = datavec(interpInd(interpCmpCases(j,2))).FBMC_results;
        
        emulatedKmHCase1 = dataCase1.speedVec*...
                           dataCase1.parameters.highSpeedEmulation.interpolationFactor*3.6;
        emulatedKmHCase2 = dataCase2.speedVec*...
                           dataCase2.parameters.highSpeedEmulation.interpolationFactor*3.6;
        
        [~, speedIndCase1, speedIndCase2] = ...
            intersect(round(emulatedKmHCase1), round(emulatedKmHCase2));
        commonSpeeds = emulatedKmHCase1(speedIndCase1);
        
        if isempty(speedIndCase1)
            warning('Files %s and %s cannot be compared: Not any speeds in common',...
                    fileData1.name, fileData2.name);
            continue;
        end
        
        % relative ber error ####
        
        % Number of bits transmitted without errors per iteration
        % here the field totalBitsNumberPerXNo is not correctly named, should be
        % totalBitsNumberPerSpeed
        totalBitsPerIterCase1 = dataCase1.bitsPerIter;
        totalBitsPerIterCase2 = dataCase2.bitsPerIter;
        
        correctBitsMatCase1 = totalBitsPerIterCase1-dataCase1.results.errorBitsMat(:,speedIndCase1);
        correctBitsMatCase2 = totalBitsPerIterCase2-dataCase2.results.errorBitsMat(:,speedIndCase2);
               
        % One-dimensional bootstrapping:
%         EberMat = 100*(correctBitsMatCase1-correctBitsMatCase2)./correctBitsMatCase2;
%         EberMean = bcaEstimator(EberMat);
%         confidenceInterval = bootci(bcaNIters,{bcaEstimator,EberMat},'alpha',bcaAlpha);
        %
        
        % Two-dimensional bootstrapping:
        EberMean = bcaRelativeErrorEstimator(correctBitsMatCase1,correctBitsMatCase2);
        confidenceInterval = bootci(bcaNIters,{bcaRelativeErrorEstimator,correctBitsMatCase1,...
            correctBitsMatCase2},'alpha',bcaAlpha);
        %
    
        % plot relative ber error
        figure(h1);
        set(h1, 'Visible', 'off');
        subplot(numel(systemcases),1,i);
        errorbar(commonSpeeds, EberMean,...
                 EberMean-confidenceInterval(1,:),...
                 EberMean-confidenceInterval(2,:),...
                 styles{mod(j-1,numel(styles))+1}, 'LineWidth', linewidth);
        grid on; 
        hold on;
        title(sprintf('Relative BER error for %s, %s = %d', systemcases{i},...
                      datavec(1).FBMC_results.noiseData.noiseValueType,...
                      datavec(1).FBMC_results.noiseData.noiseDB));
        xlabel('Emulated Speed [km/h]');
        ylabel('Error [%]');
        
        
        % relative evm error ###

        EevmCase1 = 10.^(dataCase1.results.EVMdBMat(:,speedIndCase1)/10);
        EevmCase2 = 10.^(dataCase2.results.EVMdBMat(:,speedIndCase2)/10);
        
        % One-dimensional bootstrapping
%         EevmMat = 100*(EevmCase1-EevmCase2)./EevmCase2;
%         EevmMean = bcaEstimator(EevmMat);
%         confidenceInterval = bootci(bcaNIters,{bcaEstimator,EevmMat},'alpha',bcaAlpha);
        %
        
        % Two-dimensional bootstrapping
        EevmMean = bcaRelativeErrorEstimator(EevmCase1,EevmCase2);
        confidenceInterval = bootci(bcaNIters,{bcaRelativeErrorEstimator,EevmCase1,...
            EevmCase2},'alpha',bcaAlpha);
        %

        % plot relative evm error
        figure(h2);
        set(h2, 'Visible', 'off');
        subplot(numel(systemcases),1,i);
        errorbar(commonSpeeds, EevmMean,...
             EevmMean-confidenceInterval(1,:),...
             EevmMean-confidenceInterval(2,:),...
             styles{mod(j-1,numel(styles))+1}, 'LineWidth', linewidth); 
        grid on; 
        hold on;
        title(sprintf('Relative EVM error for %s, %s = %d', systemcases{i},...
                      datavec(1).FBMC_results.noiseData.noiseValueType,...
                      datavec(1).FBMC_results.noiseData.noiseDB));
        xlabel('Emulated Speed [km/h]');
        ylabel('Error [%]');
        
        legendData{j} = sprintf('(I=%d)-(I=%d)', interpCmpCases(j,1), interpCmpCases(j,2));
    end
    
    hold off;
end

figure(h1)
subplot(numel(systemcases),1,1);
legend(legendData{1:plotInd});
figure(h2)
subplot(numel(systemcases),1,1);
legend(legendData{1:plotInd});
