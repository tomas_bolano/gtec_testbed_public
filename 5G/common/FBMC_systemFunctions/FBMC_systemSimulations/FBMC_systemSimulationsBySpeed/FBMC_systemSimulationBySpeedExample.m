%% Initializations

clear;
%close all;
format long g
%format short
%clc

%% set random number generator seed
rng(12521)


%% Basic configuration for the simulation
%speedVec = [50 200/3 100 400/3 150 200 300 400]/3.6;  % speed in m/s
speedVec = [50:50:400]/3.6;  % speed in m/s
snrType = 'SNR'; % 'EbNo' or 'SNR'
snr_dB = 25;
noiseAddingType = 'fixNo'; % 'fixNo' or 'varNo'
niter = 50; % maximum number of iterations per EbNo
saveResults = false; % Save results in a .mat file



%% Parameter initialization

FBMC_parameters = FBMC_systemExampleOFDMParametersDefinition();
%FBMC_parameters = FBMC_systemExampleHermiteParametersDefinition();


md = FBMC_getParametersMetadata(FBMC_parameters);


% Set single datablock configuration

% LTE CQIs
dataBlockModOrders = [4 4 4 4 4 4 16 16 16 64 64 64 64 64 64];
dataBlockCodeRates = [78 120 193 308 449 602 378 490 616,...
                      466 567 666 772 873 948]/1024;
                  
CQIind = 1;

FBMC_parameters.dataBlock = struct();
if strcmp(md.systemType, 'SMT')
    FBMC_parameters.dataBlock.modulation             = 'pam';
    FBMC_parameters.dataBlock.modulationOrder        = dataBlockModOrders(CQIind)/2;
else
    FBMC_parameters.dataBlock.modulation             = 'qam';
    FBMC_parameters.dataBlock.modulationOrder        = dataBlockModOrders(CQIind);
end
FBMC_parameters.dataBlock.modulationNormalize    = true;
FBMC_parameters.dataBlock.modulationEnergyFactor = 1;
FBMC_parameters.dataBlock.uncodedBitsNumber      = dataBlockCodeRates(CQIind);
FBMC_parameters.dataBlock.FECCoderFunction       = {@FBMC_nullFECCoder};
FBMC_parameters.dataBlock.FECDecoderFunction     = {@FBMC_nullFECDecoder};
FBMC_parameters.dataBlock.mappingFunction        = ...
    {@FBMC_dataBlockSquareMapper, 'min', 'max', 'min', 'max'};
    
    
FBMC_parameters.symbolsInterleaving = struct();
FBMC_parameters.symbolsInterleaving.dataBlocks = 1;
FBMC_parameters.symbolsInterleaving.interleaver = 'rnd';
FBMC_parameters.symbolsInterleaving.interleaverData = {0};


FBMC_parameters.pilots.pilotsEnergyFactor = 1;
%FBMC_parameters.pilots.pilotsGenerationFunction = {@FBMC_nullPilotIndexer};


FBMC_parameters.channelModel.channelGenerationFunctions = {@FBMC_idealChannelModel};
%FBMC_parameters.channelEstimation.channelEstimationFunction = {@FBMC_nullChannelEstimator};
%FBMC_parameters.channelEqualization.channelEqualizationFunction = {@FBMC_nullChannelEqualizator};

FBMC_parameters.channelModel.channelGenerationFunctions = {@FBMC_stdchanChannelModel};
%FBMC_parameters.channelModel.stdchanChannelModel.Type = '3gppRAx';
FBMC_parameters.channelModel.stdchanChannelModel.Type = 'itur3GVAx';
%FBMC_parameters.channelModel.relativeSpeed = 0.01/3.6;


%% Simulation

% Set snr type and snr value
FBMC_parameters = FBMC_setNoiseSNRType(FBMC_parameters, snrType, noiseAddingType);

tic();
FBMC_results = FBMC_systemSimulationBySpeed(FBMC_parameters, speedVec, snr_dB, snrType, niter, true);
toc();


if saveResults
    channelName = FBMC_channel2str(FBMC_parameters);
    simulationData.legend = sprintf('%s %s', simulationData.legend, channelName);
    
    systemMetadata = FBMC_getParametersMetadata(FBMC_parameters);
    fileName = sprintf('FBMC_results_by_speed_%s_%s_I%d%s', ...
                       systemMetadata.systemId, channelName,...
                       FBMC_parameters.highSpeedEmulation.interpolationFactor,...
                       datestr(now, '[YYYY-mm-dd_HH-MM-SS]'));
    save(fileName, 'simulationData');
end


%% plot results


FBMC_plotResultsBySpeed(FBMC_results,...
                        'linealScalePlot', false,...
                        'codedBERPlot', [1, 2],...
                        'uncodedStyles', {'-s'},...
                        'evmStyles', {'-s'},...
                        'linealScalePlot', true);
                    
