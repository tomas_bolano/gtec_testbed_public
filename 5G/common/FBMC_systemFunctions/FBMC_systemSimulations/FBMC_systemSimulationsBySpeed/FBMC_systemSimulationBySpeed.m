function FBMC_simulationResults = FBMC_systemSimulationBySpeed(FBMC_parameters,...
    speedVec,  snr_dB, snrType, niter, printReport)
%FBMC_SYSTEMSIMULATIONBYSPEED Execute a simulation iterating through different speeds.
%
% TODO documentation


if nargin < 4
    printReport = 0;
end

rng(0);
addingMethod = 'fixNo';
FBMC_constants = FBMC_pregenerateConstants(FBMC_parameters);
FBMC_parameters = FBMC_setNoiseSNRType(FBMC_parameters, snrType, addingMethod);
FBMC_parameters = FBMC_setNoiseSNRValue(FBMC_parameters, snr_dB);

FBMC_txData = FBMC_transmitter(FBMC_parameters, FBMC_constants);

assert(numel(FBMC_parameters.dataBlock) == 1,...
       'For simulations only 1 datablock is supported');


snrType = FBMC_parameters.channelModel.noise.snrType;
snr_dB = FBMC_parameters.channelModel.noise.snr_dB;
referenceBlock = FBMC_parameters.channelModel.noise.referenceBlock;

FBMC_noiseData.snrType = snrType;
FBMC_noiseData.referenceBlock = referenceBlock;
FBMC_noiseData.noiseValues = struct();

if printReport == 1
    FBMC_printSimulationInformation(FBMC_parameters, FBMC_constants);
    FBMC_printSignalInformation(FBMC_parameters, FBMC_constants, FBMC_txData);
    FBMC_printChannelInformation(FBMC_parameters, FBMC_constants);
    FBMC_printDataBlocksInformation(FBMC_parameters, FBMC_constants);
end

% Vector of detected frames per noise value
detectedVec = zeros(1, length(speedVec));

% Initialize matrix of struct of the results
resultsMat = repmat(struct(), niter, length(speedVec));

% Initialize fields of the structs
resultsMat(1).uncodedErrorBits = [];
resultsMat(1).codedErrorBits = [];
resultsMat(1).EVMdB = [];


FBMC_channelData = [];  %  always empty at the moment
FBMC_progressReportData = [];

noisevalues = [];

resultsCell = cell(1,length(speedVec));

parfor speedInd = 1:length(speedVec)
    FBMC_channelDataPar = [];
    FBMC_signalDataPar = [];
    FBMC_parametersPar = FBMC_parameters;
    
    rng(0, 'twister');
    FBMC_constantsPar = FBMC_pregenerateConstants(FBMC_parametersPar);
    
    resultsCell{speedInd} = repmat(struct('uncodedErrorBits', [], 'codedErrorBits', [], 'EVMdB', []), niter, 1);

    [FBMC_parametersPar, FBMC_constantsPar] = FBMC_setChannelModelRelativeSpeed(...
        FBMC_parametersPar, FBMC_constantsPar, speedVec(speedInd));
    
    if printReport == 2
        fprintf('%s %4.1f | started  [%s]\n', snrType, noiseVec(speedInd), datestr(now, 'HH:MM:SS.FFF'));
    end

    for i = 1:niter
        % print iteration progress
%         if printReport == 1
%             FBMC_progressReportString = sprintf('%4.1f km/h', speedVec(speedInd)*3.6);
%             FBMC_progressReportData = FBMC_progressReport(length(speedVec), niter,...
%                 speedInd, i, FBMC_progressReportData, FBMC_progressReportString, 1, 1);
%         end
        
        % Pass signal through channel and add noise
        [FBMC_channelSystemData, FBMC_channelDataPar, FBMC_signalDataPar] = ...
            FBMC_channelAndNoise(FBMC_parametersPar, FBMC_constantsPar,...
                FBMC_txData, FBMC_channelDataPar, FBMC_signalDataPar);

        % execute receiver
        FBMC_rxData = FBMC_receiver(FBMC_parametersPar, FBMC_constantsPar,...
                                    FBMC_channelSystemData.signal,...
                                    FBMC_txData.dataBlockCodedBitsNumber,...
                                    FBMC_channelSystemData.No);

        % I fthe signal was not detected try again
        if ~FBMC_rxData.detected
            continue;
        else
            detectedVec(speedInd) = 1 + detectedVec(speedInd);
        end

        % obtain results
        FBMC_results = FBMC_systemIterationResults(FBMC_parametersPar,...
            FBMC_constantsPar, FBMC_txData, FBMC_rxData);
        
        % check if an error ocurred decoding
        decodeErrorFlags = arrayfun(@(x) x.decodedData.criticalError, FBMC_rxData.dataBlock);
        if any(decodeErrorFlags)
            decodeErrorInd = find(decodeErrorFlags);
            error('Error decoding block %d: %s', decodeErrorInd,...
                FBMC_rxData.dataBlock(decodeErrorInd).decodedData.criticalErrorMsg);
        end
        
        % save results
        resultsCell{speedInd}(detectedVec(speedInd)) = FBMC_results;
        %resultsMat(detectedVec(speedInd), speedInd) = FBMC_results;
    end
    
    noiseValues(speedInd).symbNo = FBMC_signalDataPar.txNoiseSymbPw;
    noiseValues(speedInd).symbPw = FBMC_signalDataPar.txSignalSymbPw;
    noiseValues(speedInd).symbEb = FBMC_signalDataPar.txSignalSymbEb;
    noiseValues(speedInd).symbUncEb = FBMC_signalDataPar.txSignalSymbUncodedEb;
    
    noiseValues(speedInd).snr_dB = FBMC_channelSystemData.snr_dB;
    noiseValues(speedInd).EbNo_dB = FBMC_channelSystemData.EbNo_dB;
    noiseValues(speedInd).UncodedEbNo_dB = FBMC_channelSystemData.UncodedEbNo_dB;
    
    if printReport == 2
        fprintf('%s %4.1f | finished [%s]\n', snrType, noiseVec(speedInd), datestr(now, 'HH:MM:SS.FFF'));
    end
end


% Assign output values

resultsMat = cell2mat(resultsCell);

% Basic values
FBMC_simulationResults.parameters = FBMC_parameters;
FBMC_simulationResults.constants = FBMC_constants;
FBMC_simulationResults.nIter = niter;

% noise data
FBMC_noiseData.noiseValues = noiseValues(1);
FBMC_simulationResults.noiseData = FBMC_noiseData;

% X Axis data and data Type
FBMC_simulationResults.XAxisDataType = 'speed';

FBMC_simulationResults.XAxisData.interpolationFactor = ...
    FBMC_parameters.highSpeedEmulation.interpolationFactor;
FBMC_simulationResults.XAxisData.speedValues = speedVec;

% Y Axis data
FBMC_simulationResults.YAxisData.detectedVec = detectedVec;
FBMC_simulationResults.YAxisData.resultsMat = resultsMat;


end

