% FBMC_SYSTEMSIMULATIONS
%
% This folder contains several functions and sripts to perform simulations
% and plot the results.
%
%
%
% The functions return and struct with the results of the simulation,
% the fields of the structs are the following:
%   
%   parameters
%       Parameters used to execute the simulation.
%   constants
%       Constants obtained from the parameters.
%   nIter
%       Number of iterations for each value of the X axis data.
%   XAxisDataType
%       Type of the X axis values.
%   XAxisData
%       Data for the Xaxis values. The contents of this field depends on
%       the tipye of simulation and the XAxisDataTpe field.
%   noiseData
%       Noise values for the simulation. When simulating iterating through
%       noise values (XAxisDataType will be 'noise') this field will not
%       be present, instead the noise values will be stored in the
%       XAxisData field.
%   YAxisData
%       Struct with the data of the results obtained.
%
%
