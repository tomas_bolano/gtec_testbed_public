function FBMC_rxData = FBMC_receiver(FBMC_parameters,...
    FBMC_constants, FBMC_rxSignal, FBMC_dataBlockCodedBitsNumber, no)
%FBMC_RECEIVER Recover the transmitted message from the received signal.
%
% Given the input FBMC_parameters, FBMC_pregeneratedConstants, and the received
% signal FBMC_rxSignal, this function tries to recover the transmitted symbols.
% The Output value will be an struct with several of the intermediate data used
% in the reception process.
%
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_constants
%       Pregenerated constants from the parameters.
%
%   FBMC_rxSignal
%       Received Signal.
%
%   FBMC_dataBlockCodedBitsNumber (optional)
%       FBMC_dataBlockCodedBitsNumber array as returned by the FBMC_transmitter
%       function. If empty or not specified it will be initialized to the
%       dataBlockBitsNumber field of FBMC_constants.
%
%   no (optional)
%       Estimation of the spectral density of the noise
%
%
% Output parameters:
%   FBMC_rxData
%       Structure with the following fields:
%           *decimatedSignal
%               Received signal after apllying the decimation for high
%               speed emulation.
%           * preprocessedSignal
%               Received signal after applying the Rx pre-processing functions.
%           * syncOutput
%               Output structure of the synchronization function.
%           * detected
%               Boolean flag of signal detection. If this flag is zero then
%               the receiver did not detect any FBMC signal in the input
%               signal FBMC_rxSignal and the following output fields will
%               be empty.
%           * dataGrid
%               Time-Frequency data grid returned by the demodulator.
%           * channelEstimation
%               Time-Frequency grid of the estimated channel.
%           * actualDataGrid
%               Time-Frequency data grid after removing the preamble and before
%               decoding the auziliary pilots.
%           * equalizedDataGrid
%               Time-Frequency equalized data grid.
%           * decodedDataGrid
%               Time-Frequency data grid after decoding the auxiliary pilots.
%           * dataBlock
%               Array of structs with the data transmitted in each block.
%               Each struct element has the following fields:
%               - softBits
%                   Full sequence received of soft bits (coded bits plus padding).
%               - codedSoftBits
%                   Coded sequence of soft bits (padding removed).
%               - decodedData
%                   Struct of the FEC decoded data as returned by the 
%                   FBMC_fecDecoder function.



if nargin < 4 || isempty(FBMC_dataBlockCodedBitsNumber)
    FBMC_dataBlockCodedBitsNumber = FBMC_constants.dataBlockBitsNumber;
end

if nargin < 5
    no = [];
end


%% Initialize output data

FBMC_rxData.preprocessedSignal = [];
FBMC_rxData.decimatedSignal = [];
FBMC_rxData.syncOutput = [];
FBMC_rxData.detected = 0;
FBMC_rxData.dataGrid = [];
FBMC_rxData.channelEstimation = [];
FBMC_rxData.actualDataGrid = [];
FBMC_rxData.equalizedDataGrid = [];
FBMC_rxData.decodedDataGrid = [];
FBMC_rxData.dataBlock = [];


%% High speed emulation - decimation
FBMC_decimatedSignal = FBMC_highSpeedEmulation_decimation(FBMC_parameters, FBMC_rxSignal);


%% Rx signal pre-processing
FBMC_rxSignal = FBMC_rxSignalPreProcessing(FBMC_parameters,...
                        FBMC_constants, FBMC_decimatedSignal);


%% Multi-modulation receiver
% Synchronization
FBMC_syncOutput = FBMC_synchronizator(FBMC_parameters, FBMC_constants,...
                                      FBMC_rxSignal);

if ~FBMC_syncOutput.detected
    return;
end

FBMC_rxDataGrid = FBMC_demodulator(FBMC_parameters, FBMC_constants,...
                                   FBMC_syncOutput.synchronizedSignal);

FBMC_rxDataGrid = ifftshift(FBMC_rxDataGrid,1);


%% Channel estimation

% Note that if we are using a preamble and zeros after the preamble for our
% signal, the data grid that we are passing to the channel estimator function
% will include that preamble and zeros.
FBMC_channelEstimation = FBMC_channelEstimator(FBMC_parameters,...
                            FBMC_constants, FBMC_rxDataGrid, no);


%% Channel equalization

%remove preamble
[~, FBMC_rxActualDataGrid] = FBMC_separatePreamble(FBMC_parameters,...
    FBMC_constants, FBMC_rxDataGrid);

% equalize
FBMC_equalizedDataGrid = FBMC_channelEqualizator(FBMC_parameters,...
    FBMC_constants, FBMC_channelEstimation, FBMC_rxActualDataGrid, no);


%% Auxiliary pilot decoding (Only for Coded Auxiliary Pilots)

FBMC_decodedDataGrid = FBMC_dataGridAuxPilotDecoder(FBMC_parameters,...
    FBMC_constants, FBMC_equalizedDataGrid);


%% Recover data from data blocks
FBMC_decodedDataGrid(isnan(FBMC_decodedDataGrid)) = 0;
FBMC_decodedDataGrid(isinf(FBMC_decodedDataGrid)) = 0;

FBMC_dataBlock = struct();

FBMC_rxSymbolCell = FBMC_extrackDataBlockSymbols(FBMC_parameters,...
        FBMC_constants, FBMC_decodedDataGrid);
    
FBMC_rxSymbolCell = FBMC_dataBlockDeinterleaver(FBMC_parameters, FBMC_rxSymbolCell);

for i = 1:numel(FBMC_parameters.dataBlock)
    % Get the coded bits for the data block
    FBMC_rxSoftBits = FBMC_extractAndDetectSymbols(FBMC_parameters,...
        FBMC_constants, FBMC_rxSymbolCell{i}, i, FBMC_channelEstimation);
    
    % Remove padding
    FBMC_rxCodedSoftBits = FBMC_rxSoftBits(1:FBMC_dataBlockCodedBitsNumber(i));
    
    % Decode bits
    FBMC_rxDecodedData = FBMC_FECDecoder(FBMC_parameters, FBMC_constants,...
                                         i, FBMC_rxCodedSoftBits);
                                     
    FBMC_dataBlock(i).softBits = FBMC_rxSoftBits;
    FBMC_dataBlock(i).codedSoftBits = FBMC_rxCodedSoftBits;
    FBMC_dataBlock(i).decodedData = FBMC_rxDecodedData;
end


%% Assign output values

FBMC_rxData.decimatedSignal = FBMC_decimatedSignal;
FBMC_rxData.preprocessedSignal = FBMC_rxSignal;
FBMC_rxData.syncOutput = FBMC_syncOutput;
FBMC_rxData.detected = 1;
FBMC_rxData.dataGrid = FBMC_rxDataGrid;
FBMC_rxData.channelEstimation = FBMC_channelEstimation;
FBMC_rxData.actualDataGrid = FBMC_rxActualDataGrid;
FBMC_rxData.equalizedDataGrid = FBMC_equalizedDataGrid;
FBMC_rxData.decodedDataGrid = FBMC_decodedDataGrid;
FBMC_rxData.dataBlock = FBMC_dataBlock;

end

