function FBMC_simulationResults = FBMC_multiuserSystemSimulationByNoise(...
    FBMC_parameters, noiseLevelDBMatrix, receiversIndexes, ...
    noiseLevelType, niter, printReport)
%FBMC_MULTIUSERSYSTEMSIMULATIONBYNOISE Execute a multiuser simulation.
%
% TODO documentation

% FIX
% Use the code FBMC_MultiuserExampleGeneralExample as a reference.


if nargin < 6
    printReport = 0;
end

noiseMat = noiseLevelDBMatrix;
numParameters = length(FBMC_parameters);

assert(size(noiseMat,1) == numParameters,...
       ['noiseLevelDBMatrix must have the same number of rows as the number',...
        ' of elements in FBMC_parametersCell']);

assert(min(receiversIndexes) >= 1 && max(receiversIndexes) <= numParameters,...
       'receiverIndexes must be a vector of indexes from 1 to %d', numParameters);

assert(numel(unique(cellfun(@(x) x.signalGeneration.dt, FBMC_parameters))) == 1,...
       'all the parameters must have the same value for signalGeneration.dt');


FBMC_pregeneratedConstants = cell(1, numParameters);
FBMC_txData = cell(1, numParameters);
FBMC_noiseValues = cell(1, numParameters);

for i = 1:numParameters
    FBMC_pregeneratedConstants{i} = FBMC_pregenerateConstants(FBMC_parameters{i});
    FBMC_parameters{i} = FBMC_setNoiseCalculationType(FBMC_parameters{i}, noiseLevelType);
    FBMC_txData{i} = FBMC_transmitter(FBMC_parameters{i}, FBMC_pregeneratedConstants{i});
    FBMC_noiseValues{i} = FBMC_noise2NatEbNo(FBMC_txData{i},...
        FBMC_parameters{i}.basicParameters.modulationOrder, noiseLevelType, noiseMat(i,:));
end

if printReport
    fprintf('Executing multiuser simulation');
    for i = 1:numParameters
        fprintf('User %d system\n', i)
        FBMC_printSimulationInformation(FBMC_parameters{i}, FBMC_pregeneratedConstants{i});
        FBMC_printSignalInformation(FBMC_parameters{i}, FBMC_pregeneratedConstants{i}, FBMC_txData{i});
        FBMC_printChannelInformation(FBMC_parameters{i}, FBMC_pregeneratedConstants{i});
        fprintf('\n');
    end
end

detectvec = repmat({zeros(1, length(noiseMat))}, 1, numParameters);
errorBitsMat = repmat({zeros(niter, length(noiseMat))}, numParameters);
EVMdBMat = repmat({zeros(niter, length(noiseMat))}, numParameters);

FBMC_channelData = cell(1,numParameters);  %  always empty at the moment
FBMC_channelOutput = cell(1,numParameters);
FBMC_channelSignalOutput = cell(1,numParameters);
FBMC_progressReportData = [];

% FIXME

for noiseInd = 1:length(noiseMat)
    % Update parameters
    for i = 1:numParameters
        FBMC_parameters{i} = FBMC_setNoiseValue(FBMC_parameters{i}, noiseMat(noiseInd));
    end
    
    for i = 1:niter
        if printReport
            FBMC_progressReportString = sprintf('%s %4.1f', noiseLevelType, noiseMat(noiseInd));
            FBMC_progressReportData = FBMC_progressReport(length(noiseMat), niter,...
                noiseInd, i, FBMC_progressReportData, FBMC_progressReportString, 1, 1);
        end
        
        for j = 1:numParameters
            % Apply channel for each individual signal
            [FBMC_channelOutput{j}, FBMC_channelData{j}] = FBMC_channelAndNoise(FBMC_parameters{j},...
                FBMC_pregeneratedConstants{j}, FBMC_txData{j}, FBMC_channelData{j});
            FBMC_channelSignalOutput{j} = FBMC_channelOutput{j}.signal;
        end
            
        % Combine signals
        FBMC_combinedSignal = FBMC_multiuserSumSignals(FBMC_channelSignalOutput{:});
        
        for j = 1:numParameters
            % Execute each individual receiver using the combined signal
            FBMC_rxData = FBMC_receiver(FBMC_parameters{j}, FBMC_pregeneratedConstants{j},...
                                        FBMC_combinedSignal);

            if ~FBMC_rxData.detected
                continue;
            else
                detectvec{j}(noiseInd) = 1 + detectvec{j}(noiseInd);
            end

            FBMC_results = FBMC_systemIterationResults(FBMC_parameters{j},...
                FBMC_pregeneratedConstants{j}, FBMC_txData{j}, FBMC_rxData);
        
            errorBitsMat{j}(i,noiseInd) = FBMC_results.errorBits;
            EVMdBMat{j}(i,noiseInd) = FBMC_results.EVMdB;
        end
    end
end

FBMC_simulationResults = cell(1,numParameters);

% TODO
return
% Assign output values
FBMC_simulationResults.type = 'byNoise';
FBMC_simulationResults.version = 1;
FBMC_simulationResults.parameters = FBMC_parameters;
FBMC_simulationResults.metadata = FBMC_pregeneratedConstants.metadata;
FBMC_simulationResults.noiseData = FBMC_noiseValues;
FBMC_simulationResults.nIterPerParam = niter;
FBMC_simulationResults.bitsPerIter = numel(FBMC_txData.message)*...
                    log2(FBMC_parameters.basicParameters.modulationOrder);
FBMC_simulationResults.results.detectedVec = detectvec;
FBMC_simulationResults.results.errorBitsMat = errorBitsMat;
FBMC_simulationResults.results.EVMdBMat = EVMdBMat;


end

