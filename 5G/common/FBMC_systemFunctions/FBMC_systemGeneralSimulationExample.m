%% Initializations

%clear;
close all;
format long g
%format short
clc


%% set random number generator seed
%rng(12395)


%% Basic configuration for the simulation
snr_dB = 25; % EbNo or SNR in dB
snrType = 'SNR'; % 'SNR', 'EbNo'
noiseAddingType = 'varNo';
%noiseAddingType = 'fixNo';
noiseReferenceBlock = 1;


%% Plotting results configuration

plot_frame         = 0;
plot_time_txSignal = 0;
plot_freq_txSignal = 1;
plot_freq_filter_compare = 0;
plot_time_rxSignal = 0;
plot_freq_rxSignal = 1;
plot_est_channel   = 1;
plot_est_channel_first_symbol = 1;


% configuration for the pwelch functions
window = 1024;
noverlap = 512;
nfft = 1024;


%% Parameter initialization

FBMC_parameters = FBMC_systemExampleOFDMParametersDefinition;
%FBMC_parameters = FBMC_systemExamplePHYDYASParametersDefinition;
%FBMC_parameters = FBMC_systemExampleHermiteParametersDefinition();
%FBMC_parameters = FBMC_systemExampleFBMCSRRCParametersDefinition();
%FBMC_parameters = FBMC_FilteredOFDMExampleParametersDefinition();


% Set number of time symbols in the frame
FBMC_parameters.basicParameters.timeSymbolsNumber = 10000;


% Set single datablock configuration

% LTE CQIs
dataBlockModOrders = [4 4 4 4 4 4 16 16 16 64 64 64 64 64 64];
dataBlockCodeRates = [78 120 193 308 449 602 378 490 616,...
                      466 567 666 772 873 948]/1024;
                  
CQIind = 5;
md = FBMC_getParametersMetadata(FBMC_parameters);

FBMC_parameters.dataBlock = struct();
if strcmp(md.systemType, 'SMT')
    FBMC_parameters.dataBlock.modulation             = 'pam';
    FBMC_parameters.dataBlock.modulationOrder        = dataBlockModOrders(CQIind)/2;
else
    FBMC_parameters.dataBlock.modulation             = 'qam';
    FBMC_parameters.dataBlock.modulationOrder        = dataBlockModOrders(CQIind);
end
FBMC_parameters.dataBlock.modulationNormalize    = true;
FBMC_parameters.dataBlock.modulationEnergyFactor = 1;
FBMC_parameters.dataBlock.uncodedBitsNumber      = dataBlockCodeRates(CQIind);
FBMC_parameters.dataBlock.FECCoderFunction       = {@FBMC_MATLABLTEFECCoder};
FBMC_parameters.dataBlock.FECDecoderFunction     = {@FBMC_MATLABLTEFECDecoder};
FBMC_parameters.dataBlock.mappingFunction        = ...
    {@FBMC_dataBlockSquareMapper, 'min', 'max', 'min', 'max'};
    
    
FBMC_parameters.symbolsInterleaving = struct();
FBMC_parameters.symbolsInterleaving.dataBlocks = 1;
FBMC_parameters.symbolsInterleaving.interleaver = 'rnd';
FBMC_parameters.symbolsInterleaving.interleaverData = {0};


% Pilots
FBMC_parameters.pilots.modulationEnergyFactor = 2;
%FBMC_parameters.pilots.pilotsGenerationFunction =  {@FBMC_nullPilotIndexer};
%FBMC_parameters.pilots.numberAuxiliaryPilots = 0;

% Channel for simulation
%FBMC_parameters.channelModel.channelGenerationFunctions = {@FBMC_idealChannelModel};
FBMC_parameters.channelModel.channelGenerationFunctions = {@FBMC_stdchanChannelModel};
FBMC_parameters.channelModel.stdchanChannelModel.Type = '3gppRAx';
%FBMC_parameters.channelModel.stdchanChannelModel.Type = '3gppTUx';
%FBMC_parameters.channelModel.stdchanChannelModel.Type = 'itur3GIAx';
FBMC_parameters.channelModel.relativeSpeed = 0.1/3.6;

% Channel estimation and equalization
%FBMC_parameters.channelEstimation.channelEstimationFunction = {@FBMC_interpChannelEstimator};
FBMC_parameters.channelEstimation.channelEstimationFunction = {@FBMC_MMSEChannelEstimator};

FBMC_parameters.channelEqualization.channelEqualizationFunction = {@FBMC_MMSEChannelEqualizator};
%FBMC_parameters.channelEqualization.channelEqualizationFunction = {@FBMC_ZFChannelEqualizator};
%FBMC_parameters.channelEqualization.channelEqualizationFunction = {@FBMC_nullChannelEqualizator};

% Synchronization
% Currently only for OFDM a robust synchronization algorithm is
% implemented, for FBMC the implemented algorithms are not so good
% The following options assume OFDM, change for FBMC.
FBMC_parameters.synchronization.preambleGenerationFunction = {@FBMC_nullPreambleGenerator};
FBMC_parameters.synchronization.synchronizationFunction = {@FBMC_ofdmBlindSynchronizator};

% Add time and freq displacement to the generated transmit signal
% Note that the algorithm FBMC_ofdmBlindSynchronizator currently is
% configured to detect up to 5 or 6 subcarriers of frequency offset, this
% limit can be increased, just look into the function code.
% Just to test the synchronization
%FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions = ...
%    {{@(c,p,s) FBMC_addZerosSignalProcessing(c, p, s, 1e6, 1e6)}, ...
%     {@(c,p,s) FBMC_FreqDisplaceSignalProcessing(c, p, s, 2.5/1024)}};
% FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions = ...
%     {{@FBMC_nullSignalProcessing}};

% Configuration to add zeros and freq displacement
zeros_start = 1e6; 
zeros_end = 1e6;
freq_off = 2.5/1024;

% High speed emulation
FBMC_parameters.highSpeedEmulation.interpolationFactor = 1;
FBMC_parameters.highSpeedEmulation.displacementFreq = 3e6;



%% Simulation

% Calculate the neccesary constants
FBMC_constants = FBMC_pregenerateConstants(FBMC_parameters);

% Execute the transmitter
FBMC_txData = FBMC_transmitter(FBMC_parameters, FBMC_constants);

% Set the noise in the parameters to the desired value
FBMC_parameters = FBMC_setNoiseSNRType(FBMC_parameters, snrType, noiseAddingType, noiseReferenceBlock);
FBMC_parameters = FBMC_setNoiseSNRValue(FBMC_parameters, snr_dB);

% Obtain the noise value in natural and EbNo (dB and Nat) units
FBMC_noiseValues = FBMC_calculateNoiseValues(FBMC_txData, FBMC_parameters, FBMC_constants, snrType, snr_dB);

% print simulation information
FBMC_printSimulationInformation(FBMC_parameters, FBMC_constants);
FBMC_printSignalInformation(FBMC_parameters, FBMC_constants, FBMC_txData);
FBMC_printChannelInformation(FBMC_parameters, FBMC_constants);

% Pass the transmitted signal through the channel
FBMC_channelSystemData = FBMC_channelAndNoise(FBMC_parameters,...
    FBMC_constants, FBMC_txData, [], [], zeros_start, zeros_end, freq_off);

%signal = [zeros(10000, 1); FBMC_channelSystemData.signal; zeros(10000, 1)];
signal = FBMC_channelSystemData.signal;
%plot(real(signal));

% Execute the receiver
FBMC_rxData = FBMC_receiver(FBMC_parameters, FBMC_constants,...
                            signal,...
                            FBMC_txData.dataBlockCodedBitsNumber,...
                            FBMC_channelSystemData.No);

if ~FBMC_rxData.detected
    fprintf('Frame not detected');
    return;
end

FBMC_results = FBMC_systemIterationResults(FBMC_parameters,...
    FBMC_constants, FBMC_txData, FBMC_rxData);


% FBMC_noiseValues.snr_dB(1)
% FBMC_results.EVMdB(1)

%return


%% plot results

% print block information
FBMC_printDataBlocksInformation(FBMC_parameters, FBMC_constants);

% print information about the results
FBMC_printResultsInformation(FBMC_parameters, FBMC_constants, FBMC_txData, FBMC_results);

% Print information about the noise values
FBMC_printNoiseInformation(FBMC_parameters, FBMC_noiseValues);

% fprintf('Total data symbols: %d\n', numel(FBMC_txData.message));
% fprintf('Erroneous symbols received: %d\n', nnz(FBMC_txData.message ~= FBMC_rxData.message));
% fprintf('Symbol error rate: %g\n', ...
%     nnz(FBMC_txData.message ~= FBMC_rxData.message)/numel(FBMC_txData.message));
% 
% FBMC_printSeparatorLine(80);
% 
% if ~FBMC_rxData.decodedData.decoded
%     fprintf('Coding/Decoding not applied!\n');
% end
% 
% modOrder = FBMC_parameters.basicParameters.modulationOrder;
% fprintf('Total bits:              %d\n', numel(FBMC_txData.codedPaddedData.bits));
% fprintf('Padding bits:            %d\n', FBMC_txData.codedPaddedData.paddingLen);
% fprintf('Total coded data bits:   %d\n', numel(FBMC_txData.codedData.bits));
% fprintf('Total uncoded data bits: %d\n', numel(FBMC_txData.uncodedBits));
% 
% fprintf('Uncoded error bits:      %d\n', FBMC_results.uncodedErrorBits);
% fprintf('Coded error bits:        %d\n', FBMC_results.codedErrorBits);
% fprintf('Uncoded BER:             %g\n', ...
%     FBMC_results.uncodedErrorBits/length(FBMC_txData.codedPaddedData.bits));
% fprintf('Coded BER:               %g\n', ...
%     FBMC_results.codedErrorBits/length(FBMC_txData.uncodedBits));
% fprintf('Analytical uncoded BER (AWGN):     %g\n', ...
%     berawgn(FBMC_noiseValues.EbNoDB, FBMC_constants.metadata.modulation, modOrder));
% fprintf('Analytical uncoded BER (Rayleigh): %g\n', ...
%     berfading(FBMC_noiseValues.EbNoDB, FBMC_constants.metadata.modulation, modOrder,1));


%return

FBMC_printSeparatorLine(80);

if plot_frame
   FBMC_showResourceMapping(FBMC_parameters); 
end


if plot_time_txSignal
    figure();
    subplot(2,1,1);
    plot(real(FBMC_txData.interpolatedSignal));
    xlabel('time');
    ylabel('Amplitude');
    title('TX signal (real part)');
    grid on;
    
    subplot(2,1,2);
    plot(imag(FBMC_txData.interpolatedSignal));
    xlabel('time');
    ylabel('Amplitude');
    title('TX signal (imaginary part)');
    grid on;
end


if plot_freq_txSignal
    %figure();
    [pxx,f] = pwelch(FBMC_txData.interpolatedSignal, window, noverlap, nfft,...
                1/FBMC_parameters.signalGeneration.dt, 'centered');
    
    semilogy(f, abs(pxx)/max(abs(pxx)), '-');
    grid on;
    title('power spectral density estimate of post processed TX signal');
    xlabel('frequency [Hz]');
    ylabel('power/frequency [dB/Hz/sample]');
end


if plot_freq_filter_compare
    % get filter object index
    postProcFunc = FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions;
    filterMask = cellfun(@(x) isequal(x{1}, @FBMC_MatlabFilterSignalProcessing) ||...
                              isequal(x{1}, @FBMC_MatlabFilterSignalProcessing),...
                         postProcFunc);
    filterInd = find(filterMask);
    
    if numel(filterInd) == 1
        filterObject = postProcFunc{filterInd}{2};
        
        hf = figure('Visible', 'off');

        % plot post processed signal
        [pxx,f] = pwelch(FBMC_txData.modulatedSignal, window, noverlap, nfft,...
                         1/FBMC_parameters.signalGeneration.dt, 'centered');
        hp = plot(f, abs(pxx)/max(abs(pxx)));
        set(get(hp,'Parent'), 'YScale', 'log');
        hold on;

        % plot original modulated signal
        [pxx,f] = pwelch(FBMC_txData.signal, window, noverlap, nfft,...
                         1/FBMC_parameters.signalGeneration.dt, 'centered');
        plot(f, abs(pxx)/max(abs(pxx)), 'r--');
        ax1 = axis();

        % plot filter frequency response
        h = filterObject.freqz((f/max(f))*pi);
        hp = plot(f, abs(h)/(max(abs(h))), 'm', 'LineWidth', 2);
        axis(ax1);

        set(hf, 'Visible', 'on');
        grid on;
        title('Frequency responses normalized');
        xlabel('frequency [hz]');
        ylabel('Amplitude');
        legend('TX signal original',...
               'TX signal after filtering',...
               'Filter', 'Location', 'SouthEast');
    end
end


if plot_time_rxSignal
    figure();
    subplot(2,1,1);
    plot(real(FBMC_channelSystemData.signal));
    xlabel('time');
    ylabel('Amplitude');
    title('RX signal (real part)');
    grid on;
    
    subplot(2,1,2);
    plot(imag(FBMC_channelSystemData.signal));
    xlabel('time');
    ylabel('Amplitude');
    title('RX signal (imaginary part)');
    grid on;
end


if plot_freq_rxSignal
    figure();
    %[pxx,f] = 
    pwelch(FBMC_channelSystemData.signal, window, noverlap, nfft,...
           1/FBMC_parameters.signalGeneration.dt, 'centered');
    %semilogy(f, abs(pxx));
    grid on;
    title('power spectral density estimate of post processed RX signal');
    xlabel('frequency [Hz]');
    ylabel('power/frequency [dB/Hz/sample]');
end


if plot_est_channel && ~isempty(FBMC_rxData.channelEstimation)
    figure();
    FBMC_plotEstimatedChannel(FBMC_parameters, FBMC_constants, ...
        FBMC_txData, FBMC_rxData, 'XAxisUnits', 'time',...
        'YAxisUnits', 'freq')
end

if plot_est_channel_first_symbol
    figure();
    scShift = -(FBMC_parameters.basicParameters.subcarriersNumber/2+1);

    xind = (1:size(FBMC_rxData.channelEstimation,1));
    estChan1 = FBMC_rxData.channelEstimation(:,1);
    plot(xind+scShift, abs(estChan1));
    
    hold on;
    pilotInd = find((any(FBMC_constants.pilots.ActualPilotMask.')));
    plot(pilotInd+scShift, abs(estChan1(pilotInd)), 'ro');
    
    title('Estimated channel for the first symbol');
    xlabel('Subcarrier number');
    ylabel('Amplitude');
    legend('Estimated channel', 'Superimposed pilot points');
    
    figure();
    plot(xind+scShift, real(estChan1));
    hold on;
    plot(pilotInd+scShift, real(estChan1(pilotInd)), 'ro');
    
    title('Estimated channel for the first symbol (real part)');
    legend('Estimated channel (real part)', 'Superimposed pilot points');
end

%%

%return

% OFDM constellation
blocki = 1;
consellation = FBMC_rxData.decodedDataGrid(FBMC_constants.dataBlockMask == blocki);
figure();
scatter(real(consellation), imag(consellation), '.');
title('signal constellation');
axis square;
grid on;


%%
return;

h = figure();

for i = 1:124
    mask = FBMC_constants.dataMask(:,i);
    symb = FBMC_rxData.decodedDataGrid(:,i);
    plotSymb = symb(mask == 1);
    figure(h)
    plot(real(plotSymb), imag(plotSymb), '.');
    axis([-2 2 -2 2]);
    drawnow
    title(sprintf('%d', i));
    pause(0.2);
end

%close all
