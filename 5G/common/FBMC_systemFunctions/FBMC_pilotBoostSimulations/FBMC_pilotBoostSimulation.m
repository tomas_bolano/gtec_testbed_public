%close all;
clear;
clc

%% Basic configuration for the simulation
pilotEgFactor = 1:10:150; % Energy factor for the pilots
noiseDB = 5; % EbNo or SNR in dB
noiseType = 'EbNo'; % 'EbNo' or 'SNR'
niter = 10; % maximum number of iterations per EbNo

% parameters used for the comparison
FBMC_parameters_cell = {FBMC_PIMRC2016_Measurements_ParametersDefinition_OFDM,...
                        FBMC_PIMRC2016_Measurements_ParametersDefinition_Phydyas,...
                        FBMC_PIMRC2016_Measurements_ParametersDefinition_Hermite};
                    
% duplicate the pilot energy used for FBMC
fbmc_doubleEnergy = true;

% modulation configuration
ofdm_modorder = 64;
fbmc_modorder = sqrt(ofdm_modorder);

FBMC_parameters_cell{1}.basicParameters.modulationOrder = ofdm_modorder;
FBMC_parameters_cell{2}.basicParameters.modulationOrder = fbmc_modorder;
FBMC_parameters_cell{3}.basicParameters.modulationOrder = fbmc_modorder;

                    
% in-place modification to the parameters
for i = 1:length(FBMC_parameters_cell)
    % do not use synchronization
    FBMC_parameters_cell{i}.synchronization.synchronizationFunction = @FBMC_nullSynchronizator;
    FBMC_parameters_cell{i}.synchronization.preambleGenerationFunction = @FBMC_nullPreambleGenerator;
    
    % channel setup
    FBMC_parameters_cell{i}.channelModel.channelGenerationFunctions = {@FBMC_idealChannelModel};
    %FBMC_parameters_cell{i}.channelModel.channelGenerationFunctions = {@FBMC_stdchanChannelModel};
    %FBMC_parameters_cell{i}.channelModel.stdchanChannelModel.Type = '3gppRAx';
    %FBMC_parameters_cell{i}.channelModel.relativeSpeed = 100/3.6;
end


%% Simulation

assert(isvector(noiseDB));

FBMC_ber_cell = repmat({zeros(niter, length(pilotEgFactor))},...
                        size(FBMC_parameters_cell));
FBMC_detected_cell = repmat({zeros(1,length(pilotEgFactor))},...
                            size(FBMC_parameters_cell));
FBMC_system_metadata = cell(size(FBMC_parameters_cell));

tic();
parfor paramInd = 1:length(FBMC_parameters_cell)
    fprintf('Executing simulation for parameter %d\n', paramInd);
    FBMC_system_metadata{paramInd} = FBMC_getParametersMetadata(FBMC_parameters_cell{paramInd});
    for i = 1:length(pilotEgFactor)
        fprintf('-> Eg factor %f\n', pilotEgFactor(i));
        % Set pilot energy factor in the parameters
        if paramInd == 1 || ~fbmc_doubleEnergy
            FBMC_parameters_cell{paramInd}.pilots.pilotsEnergyFactor = pilotEgFactor(i);
        else
            FBMC_parameters_cell{paramInd}.pilots.pilotsEnergyFactor = 2*pilotEgFactor(i);
        end
        
        % run the simulation
        FBMC_results = FBMC_systemSimulationByNoise(FBMC_parameters_cell{paramInd},...
                       noiseDB, noiseType, niter, 0);
        
        % save the results
        FBMC_ber_cell{paramInd}(:,i) = FBMC_results.results.errorBitsMat/...
                                       FBMC_results.bitsPerIter;
        FBMC_detected_cell{paramInd}(i) = FBMC_results.results.detectedVec;
    end
end
toc();


%% Calculare confidence intervals

bcaEstimator = @(x)mean(x);
bcaAlpha = 0.05; % Confidence level
bcaNIters = 400; % Numer of bootstrap iterations

procResults = cell(size(FBMC_parameters_cell));

for i = 1:length(FBMC_parameters_cell)
    procResults{i}.bervec = zeros(1,length(pilotEgFactor));
    procResults{i}.confidenceInterval = zeros(2,length(pilotEgFactor));
    for j = 1:length(pilotEgFactor)
        iter_bervec = FBMC_ber_cell{i}(1:FBMC_detected_cell{i}(j),j);
        procResults{i}.bervec(j) = bcaEstimator(iter_bervec);
         procResults{i}.confidenceInterval(:,j) = ...
             bootci(bcaNIters, {bcaEstimator,iter_bervec},'alpha',bcaAlpha);
    end
end


%% Plot results

plotStyles = {'or-', 'sb--', '^m-'};

% Indexes of pilotEgFactor to plot
egInd = 1:length(pilotEgFactor);

% plot processed data
figure();
for i = 1:length(FBMC_parameters_cell)
    bervec = procResults{i}.bervec(1,egInd);
    confidenceInterval = procResults{i}.confidenceInterval(:,egInd);
    
    h = errorbar(pilotEgFactor(egInd), bervec,...
                bervec-confidenceInterval(1,:),...
                bervec-confidenceInterval(2,:),...
                plotStyles{mod(i-1,numel(plotStyles))+1});
    
    %set(get(h,'Parent'), 'YScale', 'log')
    hold on
end
ax1 = axis;
title(sprintf('Pilot boosting results for %s = %d dB', noiseType, noiseDB));
ylabel('BER');
xlabel('Pilot boosting factor');

axis(ax1);
legend(arrayfun(@(i) FBMC_system_metadata{i}.systemDesc,...
                1:length(FBMC_system_metadata), 'UniformOutput', false));
grid on;
hold off;
