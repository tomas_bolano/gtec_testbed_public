function [FBMC_channelSystemData, FBMC_channelData, FBMC_signalData] = FBMC_channelAndNoise(...
    FBMC_parameters, FBMC_constants, FBMC_txData, FBMC_channelData, FBMC_signalData,...
    zeros_start, zeros_end, freq_off)
%FBMC_CHANNEL Apply a channel to the transmitted signal
%
% TODO documentation

if nargin  < 5
    FBMC_signalData = [];
end

if nargin  < 6
    zeros_start = 0;
end

if nargin  < 7
    zeros_end = 0;
end

if nargin  < 8
    freq_off = 0;
end

    
%% Parameters
snrType = FBMC_parameters.channelModel.noise.snrType;
addingMethod = FBMC_parameters.channelModel.noise.addingMethod;

% if true the CP of OFDM will be taken into account for the energy
% calculation
considerOFDMCP = FBMC_parameters.channelModel.noise.considerOFDMCP;

snr_dB = FBMC_parameters.channelModel.noise.snr_dB;


%% Channel model

assert(ismember(addingMethod, {'fixNo', 'varNo'}),...
       'Invalid value of addingMethod.');


% Interpolation factor used
interpFactor = FBMC_parameters.highSpeedEmulation.interpolationFactor;

% Calculate energy and power of the tx signal
txSignal = FBMC_txData.interpolatedSignal;

No = 1;

if isempty(FBMC_signalData)
    % To calculate the power we consider the transmitter used
    % and calculate the powers of the received symbols.
    % Energy is calculated in time domain
    FBMC_parameters_aux = FBMC_parameters;
    FBMC_parameters_aux.synchronization.synchronizationFunction = {@FBMC_nullSynchronizator};
    FBMC_parameters_aux.channelEstimation.channelEstimationFunction = {@FBMC_nullChannelEstimator};
    FBMC_parameters_aux.channelEqualization.channelEqualizationFunction = {@FBMC_nullChannelEqualizator};
    for ii = 1:numel(FBMC_parameters_aux.dataBlock)
        FBMC_parameters_aux.dataBlock(ii).FECDecoderFunction = {@FBMC_nullFECDecoder};
    end

    sample_noise = sqrt(No/2)*(randn(size(txSignal)) + 1j*randn(size(txSignal)));
    FBMC_rxNoise = FBMC_receiver(FBMC_parameters_aux, FBMC_constants, sample_noise);
    FBMC_rxData = FBMC_receiver(FBMC_parameters_aux, FBMC_constants, txSignal);
    
    symbmask = FBMC_constants.dataMask;
    %if isfield(FBMC_constants, 'auxpilots')
    %    symbmask = symbmask & ~FBMC_constants.auxpilots.auxPilotFullCodedMask;
    %end
    
    txSignalSymbPw = var(FBMC_rxData.dataGrid(symbmask));
    txNoiseSymbPw = var(FBMC_rxNoise.dataGrid(:));
    
    nbits = FBMC_constants.dataBlockTotalBitsNumber;
    nUncodedBits = FBMC_constants.dataBlockTotalUncodedBitsNumber;
    if strcmp(FBMC_constants.metadata.systemType, 'SMT')
        % In FBMC systems, we only consider the real part to calculate the
        % energy, since the imaginary part is interference from adjacent
        % symbols
        txNoiseSymbEb = sum(real(FBMC_rxData.dataGrid(symbmask)).^2)/nbits;
    else
        txNoiseSymbEb = sum(abs(FBMC_rxData.dataGrid(symbmask)).^2)/nbits;
    end
    txNoiseSymbUncodedEb = sum(abs(FBMC_rxData.dataGrid(symbmask)).^2)/nUncodedBits;
      
    FBMC_signalData.txSignalSymbPw = txSignalSymbPw;
    FBMC_signalData.txNoiseSymbPw = txNoiseSymbPw;
    FBMC_signalData.txSignalSymbEb = txNoiseSymbEb;
    FBMC_signalData.txSignalSymbUncodedEb = txNoiseSymbUncodedEb;
end

if strcmp(addingMethod, 'fixNo')
    No = 1;
    
    switch snrType
        case 'SNR'
            powerFactor = 10^(snr_dB/10)*FBMC_signalData.txNoiseSymbPw/No;
        case 'EbNo'
            powerFactor = 10^(snr_dB/10)*FBMC_signalData.txNoiseSymbPw/No*...
                              1/FBMC_signalData.txSignalSymbEb;
            if strcmp(FBMC_constants.metadata.systemId, 'OFDM') && ~considerOFDMCP
                nsubc = FBMC_parameters.basicParameters.subcarriersNumber;
                cplen = FBMC_parameters.pulseShapping.cyclicPrefixLength;
                % we do not take into account the CP
                powerFactor = powerFactor*(nsubc+cplen)/nsubc;
            end
        case 'codedEbNo'
            error('TODO');
        otherwise
            error('Unsupported noise metric');
    end

    txSignal = sqrt(powerFactor)*txSignal;
end
    

% Pass signal through channel
[FBMC_rxSignal, FBMC_channelData] = ...
    FBMC_channelFunction(FBMC_parameters, FBMC_constants,...
                         txSignal, FBMC_channelData);
                     

% Energy and power of the rx signal
% Calculate energy and power of the rx signal
% FBMC_rxSignalEg = FBMC_rxSignal'*FBMC_rxSignal;
% FBMC_rxSignalPw = FBMC_rxSignalEg/(length(FBMC_rxSignal)/interpFactor);

if strcmp(addingMethod, 'varNo')
    powerFactor = 1;
    switch snrType
        case 'SNR'
            No = FBMC_signalData.txSignalSymbPw/10^(snr_dB/10);
        case 'EbNo'
            No = FBMC_signalData.txSignalSymbEb/10^(snr_dB/10);
        case 'codedEbNo'
            error('TODO');
        otherwise
            error('Unsupported noise metric');
    end
end

%% Add zeros and frequency offset
FBMC_rxSignal = [zeros(zeros_start,1);...
                 FBMC_rxSignal.*exp(1j*2*pi*(0:length(FBMC_rxSignal)-1).'*freq_off);...
                 zeros(zeros_end,1)];


%% Add AWGN noise
FBMC_rxSignal = FBMC_rxSignal + sqrt(No/2)*randn(size(FBMC_rxSignal))+...
                             1j*sqrt(No/2)*randn(size(FBMC_rxSignal));

%% Assign output values

FBMC_channelSystemData.signal = FBMC_rxSignal;

FBMC_channelSystemData.No = No;

FBMC_channelSystemData.snr_dB = 10*log10(FBMC_signalData.txSignalSymbPw*powerFactor/No);
if strcmp(FBMC_constants.metadata.systemId, 'OFDM') && ~considerOFDMCP
    nsubc = FBMC_parameters.basicParameters.subcarriersNumber;
    cplen = FBMC_parameters.pulseShapping.cyclicPrefixLength;
    FBMC_channelSystemData.EbNo_dB = 10*log10(FBMC_signalData.txSignalSymbEb*...
                                              powerFactor*nsubc/(nsubc+cplen)/No);
else
    FBMC_channelSystemData.EbNo_dB = 10*log10(FBMC_signalData.txSignalSymbEb*powerFactor/No);
end
FBMC_channelSystemData.UncodedEbNo_dB = [];

end

