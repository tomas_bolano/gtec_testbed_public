function FBMC_noiseValues = FBMC_calculateNoiseValues(FBMC_txData, ...
    FBMC_parameters, FBMC_constants, snrType, snr_dB, snrReferenceBlock)
%FBMC_CALCULATENOISEVALUES Converts the snr dB value
%
% Converts the snr value snr_dB of the snr metric indicated in snrType
% to the equivalent SNR EbNo and coded EbNo metrics for all the data blocks.
%
% snrType must be a string that indicates the snr metric used in the parameter
% snr_dB. It can take same values as the channelModel.noise.snrType field
% of FBMC_parameters ('SNR', 'EbNo' or 'codedEbNo').
%
% snr_dB must be a vector with values from the snr metric indicated in
% snrType.
%
% The output value FBMC_noiseValues will be a struct with the following
% fields:
%   snr_dB
%       2D matrix of SNR values in dB units where the columns represent the
%       blocks and the rows the different input snr values.
%   EbNo_dB
%       2D matrix of EbNo values in dB units where the columns represent the
%       blocks and the rows the different input snr values. 
%   codedEbNo_dB
%       2D matrix of EbNo values (considering only uncoded bits) in dB units
%       where the columns represent the blocks and the rows the different
%       input snr values.

assert(isvector(snr_dB), 'snr_dB must be a vector');

% assert modulations are normalized
if numel(FBMC_parameters.dataBlock) > 1
    for i = 1:numel(FBMC_parameters.dataBlock)
        assert(FBMC_parameters.dataBlock(i).modulationNormalize == 1,...
               'modulation for block %d is not normalized', i);
        assert(FBMC_parameters.dataBlock(i).modulationEnergyFactor == 1,...
               'modulations must have an energy factor of 1');
    end
end

% SNR from the input
snrInVec = snr_dB(:); %make a row vector
snrInNatVec = 10.^(snrInVec/10);

if nargin < 6
    snrReferenceBlock = FBMC_parameters.channelModel.noise.referenceBlock;
end

snrNatMat = zeros(numel(snrInNatVec), numel(FBMC_parameters.dataBlock));
EbNoNatMat = zeros(size(snrNatMat));
codedEbNoNatMat = zeros(size(snrNatMat));

signalEg = FBMC_txData.signalEnergy;
signalLen = numel(FBMC_txData.signal);

for i = 1:numel(FBMC_parameters.dataBlock)
    FBMC_noiseConverted = FBMC_convertNoiseValues(FBMC_parameters,...
        FBMC_constants, signalEg, signalLen, i, snrReferenceBlock, snrType, snr_dB);
    
    
    snrNatMat(:,i) = FBMC_noiseConverted.SNR_nat;
    EbNoNatMat(:,i) = FBMC_noiseConverted.EbNo_nat;
    codedEbNoNatMat(:,i) = FBMC_noiseConverted.codedEbNo_nat;
end


% FBMC_noiseValues.snrType = snrType;
% FBMC_noiseValues.referenceBlock = snrReferenceBlock;

% FBMC_noiseValues.snr_nat = snrNatMat;
% FBMC_noiseValues.EbNo_nat = EbNoNatMat;
% FBMC_noiseValues.codedEbNo_nat = codedEbNoNatMat;

FBMC_noiseValues.snr_dB = 10*log10(snrNatMat);
FBMC_noiseValues.EbNo_dB = 10*log10(EbNoNatMat);
FBMC_noiseValues.codedEbNo_dB = 10*log10(codedEbNoNatMat);

end

