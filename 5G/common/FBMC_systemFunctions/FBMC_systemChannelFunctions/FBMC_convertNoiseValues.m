function FBMC_noiseValues = FBMC_convertNoiseValues(...
    FBMC_parameters, FBMC_constants, signalEg, signalLen,...
    numBlock, referenceBlock, snrType, snr_dB, No)
%FBMC_CONVERTNOISEVALUES Converts the snr dB value
%
% Converts the snr values of the matrix snr_dB, with snr values of the snr
% metric indicated in snrType to the equivalent SNR, EbNo and coded EbNo,
% for the data block indicated in numBlock, using the reference block
% indicated in referenceBlock. The function also returns the values of No
% and signal energy required for achieving each snr value.
%
% snrType must be a string that indicates the snr metric used in the 
% snr_dB matrix. It can take same values as the channelModel.noise.snrType
% field of FBMC_parameters ('SNR', 'EbNo' or 'codedEbNo').
%
% snr_dB must be a vector with values from the snr metric indicated in
% snrType.
%
% No is the power spectral density. If ommited or is empty then the
% function assumes that the energy and power of the signal in FBMC_txData
% are fixed and changes the No accordingly, else the function takes the No
% value and returns the adequate values of energy and power for the signal.
%
% The output value FBMC_noiseValues will be a struct with the following
% fields:
%   signalEg
%       Matrix of required energies of the signal to met the values of snr_db. 
%   signalPw
%       Matrix of required powers of the signal to met the values of snr_db. 
%   signalLen
%       Lenght of the signal (without interpolation). The following
%       equality holds: signalPw = signalEg/signalLen.
%   No
%       Matrix of required power sprectral densities to met the values of snr_db.
%   SNR_dB
%       Matrix of SNR values (in dB).
%   EbNo_dB
%       Matrix of EbNo values (in dB).
%   codedEbNo_dB
%       Matrix of coded EbNo values (in dB).
%   SNR_nat
%       Matrix of SNR values (in natural units).
%   EbNo_nat
%       Matrix of EbNo values (in natural units).
%   codedEbNo_nat
%       Matrix of coded EbNo values (in natural units).


if nargin < 9
    No = [];
end

assert(mod(signalLen,1) == 0, 'The signal length has to be integer');

% assert modulations are normalized
if numel(FBMC_parameters.dataBlock) > 1
    for i = 1:numel(FBMC_parameters.dataBlock)
        assert(FBMC_parameters.dataBlock(i).modulationNormalize == 1,...
               'modulation for block %d is not normalized', i);
        assert(FBMC_parameters.dataBlock(i).modulationEnergyFactor == 1,...
               'modulations must have an energy factor of 1');
    end
end

% SNR from the input
snrIn = snr_dB;
snrInNat = 10.^(snrIn/10);

% Information about the number of bits and symbols for the reference block
uncodedNumBitsBlock = FBMC_constants.dataBlockInputUncodedBitsNumber(referenceBlock);
numBitsBlock = FBMC_constants.dataBlockBitsNumber(referenceBlock);
numSymbolsBlock = FBMC_constants.dataBlockSymbolsNumber(referenceBlock);

% Total number of symbols
numTotalSymbols = FBMC_constants.dataBlockTotalSymbolsNumber;

% number of subcarriers and buard band used
subcNum = FBMC_parameters.basicParameters.subcarriersNumber;
guardNum = nnz(FBMC_constants.usedSubcarriers.guardMask);

% Bandwith used by the multicarrier signal (from 0 to 1)
bw = 1 - guardNum/subcNum;

if isempty(No)
    % Signal power
    signalPw = signalEg/signalLen;

    % Calculate No
    switch snrType
        case 'SNR'
            No = signalPw./(snrInNat*bw);
        case 'EbNo'
            No = (signalEg*numSymbolsBlock)/...
                 (snrInNat*numBitsBlock*numTotalSymbols);
        case 'codedEbNo'
            No = (signalEg*numSymbolsBlock)/...
                 (snrInNat*uncodedNumBitsBlock*numTotalSymbols);
        otherwise
            error('Invalid SNR metric.');
   end
else
    % Calculate signalEg and signalPw
    switch snrType
        case 'SNR'
            signalEg = snrInNat*No*bw*signalLen;
        case 'EbNo'
            signalEg = snrInNat*No*numBitsBlock*numTotalSymbols/...
                       numSymbolsBlock;
        case 'codedEbNo'
            signalEg = snrInNat*No*uncodedNumBitsBlock*numTotalSymbols/...
                       numSymbolsBlock;
        otherwise
            error('Invalid SNR metric.');
    end
   
    signalPw = signalEg/signalLen;
end


% Information about the number of bits and symbols for the requested block
uncodedNumBitsBlock = FBMC_constants.dataBlockInputUncodedBitsNumber(numBlock);
numBitsBlock = FBMC_constants.dataBlockBitsNumber(numBlock);
numSymbolsBlock = FBMC_constants.dataBlockSymbolsNumber(numBlock);


% Calculate SNR, EbNo and uncodedEbNo for the block numBlock
SNR_nat = signalPw./(No*bw);
EbNo_nat = (signalEg*numSymbolsBlock)./...
           (numBitsBlock*numTotalSymbols*No);
codedEbNo_nat = (signalEg*numSymbolsBlock)./...
                (uncodedNumBitsBlock*numTotalSymbols*No);

SNR_dB = 10*log10(SNR_nat);
EbNo_dB = 10*log10(EbNo_nat);
codedEbNo_dB = 10*log10(codedEbNo_nat);

FBMC_noiseValues.signalEg = signalEg;
FBMC_noiseValues.signalPw = signalPw;
FBMC_noiseValues.signalLen = signalLen;

FBMC_noiseValues.No = No;

FBMC_noiseValues.SNR_nat = SNR_nat;
FBMC_noiseValues.EbNo_nat = EbNo_nat;
FBMC_noiseValues.codedEbNo_nat = codedEbNo_nat;

FBMC_noiseValues.SNR_dB = SNR_dB;
FBMC_noiseValues.EbNo_dB = EbNo_dB;
FBMC_noiseValues.codedEbNo_dB = codedEbNo_dB;

end

