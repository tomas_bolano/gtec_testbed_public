function FBMC_parameters = FBMC_setNoiseSNRType(FBMC_parameters, snrType,...
    addingMethod, noiseReferenceBlock)
%FBMC_SETNOISESNRTYPE Sets the method used to calculate the Noise value.
%
% Set the metric of the Noise to the value of snrType.
% Also set the adding method to the value of addingType (Default: 'fixNo')


if nargin < 4
    noiseReferenceBlock = 1;
end

FBMC_parameters.channelModel.noise.snrType = snrType;
FBMC_parameters.channelModel.noise.referenceBlock = noiseReferenceBlock;
FBMC_parameters.channelModel.noise.addingMethod = addingMethod;

end
