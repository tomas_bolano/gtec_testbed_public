function [FBMC_parameters, FBMC_pregeneratedConstants] = ...
    FBMC_setChannelModelRelativeSpeed(FBMC_parameters,...
        FBMC_pregeneratedConstants, relativeSpeed)
%FBMC_SETCHANNELMODELRELATIVESPEED Sets the channel model relative speed in m/s.
%
% This function will set a new speed for the channel model that will affect the
% maximum dopler shift. This function modifies the data related to the channel
% in the pregenerated constant parameter FBMC_pregeneratedConstants.


FBMC_parameters.channelModel.relativeSpeed = relativeSpeed;
FBMC_pregeneratedConstants.channelModel = FBMC_generateChannelConstants(FBMC_parameters);

end

