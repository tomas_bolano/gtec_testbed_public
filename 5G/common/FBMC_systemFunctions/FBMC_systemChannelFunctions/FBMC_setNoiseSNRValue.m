function FBMC_parameters = FBMC_setNoiseSNRValue(FBMC_parameters, snrValue)
%FBMC_SETNOISESNRVALUE Sets The noise value in dBs.

FBMC_parameters.channelModel.noise.snr_dB = snrValue;

end

