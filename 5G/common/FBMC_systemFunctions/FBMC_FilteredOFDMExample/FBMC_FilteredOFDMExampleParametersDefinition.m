function [FBMC_parameters] = FBMC_FilteredOFDMExampleParametersDefinition()
%FBMC_parametersDefinition 

% load OFDM base parameters
FBMC_parameters = FBMC_systemExampleOFDMParametersDefinition();


%% Post-processing of the transmitted signal

filter = FBMC_FilteredOFDMFIRFilterGenerationExample();
delay = floor(filter.order/2);

% TX postprocessing functions to apply in order
% with format
% {{function1, param1, param2, ...};...
%  {function2, param1, param2, ...};...}
FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions = ...
    {{@FBMC_addZerosSignalProcessing, 0, delay};
     {@FBMC_MatlabFilterSignalProcessing, filter};
     {@FBMC_truncateSignalProcessing, delay, 0}};


%% Pre-processing of the received signal

% RX preprocessing functions to apply in order
% with format
% {{function1, param1, param2, ...};...
%  {function2, param1, param2, ...};...}
FBMC_parameters.rxSignalPreProcessing.rxPreProcessingFunctions = ...
    {{@FBMC_nullSignalProcessing}};
