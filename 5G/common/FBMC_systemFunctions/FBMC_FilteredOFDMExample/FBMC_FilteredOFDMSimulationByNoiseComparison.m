%% Initializations

clear;
%close all;
format long g
%format short
%clc

%% set random number generator seed
rng(12321)


%% Basic configuration for the simulation
XNoVec = 0:1:20; % EbNo or SNR in dB
simType = 'EbNo'; % 'EbNo', 'SNR' or 'PwNo'
niter = 20; % maximum number of iterations per EbNo


%% Parameter initialization

FBMC_parameters1 = FBMC_systemExampleOFDMParametersDefinition();
FBMC_parameters2 = FBMC_FilteredOFDMExampleParametersDefinition();


%% Simulation

fprintf('Executing simulation 1\n');

tic();
FBMC_results1 = FBMC_systemSimulationByNoise(FBMC_parameters1, XNoVec, simType, niter, true);
toc();

fprintf('\n');
fprintf('Executing simulation 2\n');

tic();
FBMC_results2 = FBMC_systemSimulationByNoise(FBMC_parameters2, XNoVec, simType, niter, true);
toc();

% channelName = FBMC_channel2str(FBMC_parameters);
% simulationData.legend = sprintf('%s %s', simulationData.legend, channelName);
% 
% systemMetadata = FBMC_getParametersMetadata(FBMC_parameters);
% fileName = sprintf('FBMC_WSA2016_%s_Results_%s_I%d_S%d_%s%s', simType, channelName,...
%                    FBMC_parameters.highSpeedEmulation.interpolationFactor,...
%                    round(FBMC_parameters.channelModel.relativeSpeed),...
%                    systemMetadata.systemId,...
%                    datestr(now, '[YYYY-mm-dd_HH-MM-SS]'));
% 
% save(fileName, 'simulationData');


%% plot results

bcaEstimator = @(x)mean(x);
bcaAlpha = 0.05; % Confidence level
bcaNIters = 400; % Numer of bootstrap iterations

bermat1 = FBMC_results1.results.errorBitsMat/FBMC_results1.bitsPerIter;
bervec1 = bcaEstimator(bermat1);
confidenceInterval1 = bootci(bcaNIters,{bcaEstimator,bermat1},'alpha',bcaAlpha);

bermat2 = FBMC_results2.results.errorBitsMat/FBMC_results2.bitsPerIter;
bervec2 = bcaEstimator(bermat2);
confidenceInterval2 = bootci(bcaNIters,{bcaEstimator,bermat2},'alpha',bcaAlpha);

figure();
h = errorbar(FBMC_results1.noiseData.noiseDB, bervec1,...
             bervec1-confidenceInterval1(1,:),...
             bervec1-confidenceInterval1(2,:),...
             'ro--');
set(get(h,'Parent'), 'YScale', 'log');
hold on

h = errorbar(FBMC_results2.noiseData.noiseDB, bervec2,...
             bervec2-confidenceInterval2(1,:),...
             bervec2-confidenceInterval2(2,:),...
             'mo--');
ax1 = axis();

% plot fading channel alytical BER curve
semilogy(FBMC_results1.noiseData.noiseDB,...
         berfading(FBMC_results1.noiseData.EbNoDB, 'qam', 4',1), 'bs-');
     
% plot AWGN channel analytical BER curve
semilogy(FBMC_results1.noiseData.noiseDB,...
         berawgn(FBMC_results1.noiseData.EbNoDB, 'qam', 4'), 'bo-');

     
legendSystem1 = sprintf('1 - %s %s - int %d - speed %4.2f m/s',...
                        FBMC_results1.metadata.systemDesc,...
                        FBMC_results1.metadata.channel,...
                        FBMC_results1.parameters.highSpeedEmulation.interpolationFactor,...
                        FBMC_results1.parameters.channelModel.relativeSpeed);

legendSystem2 = sprintf('2 - %s %s - int %d - speed %4.2f m/s',...
                        FBMC_results2.metadata.systemDesc,...
                        FBMC_results2.metadata.channel,...
                        FBMC_results2.parameters.highSpeedEmulation.interpolationFactor,...
                        FBMC_results2.parameters.channelModel.relativeSpeed);

legendDataAnalytical1 = [FBMC_results1.metadata.systemDesc ' Rayleigh analytical'];
legendDataAnalytical2 = [FBMC_results1.metadata.systemDesc ' AWGN analytical'];

grid on;
hold off;
axis(ax1);
legend(legendSystem1, legendSystem2, legendDataAnalytical1, legendDataAnalytical2);
title('Bit Error Rate');
xlabel('E_b/N_0 [dB]');
ylabel('BER'); 


