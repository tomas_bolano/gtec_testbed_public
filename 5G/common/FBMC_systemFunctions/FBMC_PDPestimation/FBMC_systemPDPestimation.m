clear
%close all
clc

rng(11);

% Configuration -----------------------------------------------------------
FBMC_parameters = FBMC_systemExampleOFDMParametersDefinition();
noiseLevelType = 'EbNo'; % EbNo or SNR
noiseLeveldB = 50;
niter = 1;

FBMC_parameters.channelModel.stdchanChannelModel.Type = 'itur3GPAx';
FBMC_parameters.signalGeneration.dt = (1/15.36e6);
FBMC_parameters.highSpeedEmulation.interpolationFactor = 1;

% Code --------------------------------------------------------------------

FBMC_pregeneratedConstants = FBMC_pregenerateConstants(FBMC_parameters);

FBMC_parameters = FBMC_setNoiseCalculationType(FBMC_parameters, noiseLevelType);
FBMC_parameters = FBMC_setNoiseValue(FBMC_parameters, noiseLeveldB);
FBMC_txData = FBMC_transmitter(FBMC_parameters, FBMC_pregeneratedConstants);

FBMC_printSimulationInformation(FBMC_parameters, FBMC_pregeneratedConstants);
FBMC_printSignalInformation(FBMC_parameters, FBMC_pregeneratedConstants, FBMC_txData);
FBMC_printChannelInformation(FBMC_parameters, FBMC_pregeneratedConstants);

FBMC_channelData = [];  %  always empty at the moment
FBMC_progressReportData = [];


estimated_channels = complex(...
                     zeros(FBMC_parameters.basicParameters.subcarriersNumber,...
                           FBMC_parameters.basicParameters.timeSymbolsNumber,...
                           niter));
detected = 0;

for i = 1:niter
    fprintf('iteration %d\n', i);

    [FBMC_channelSystemData, FBMC_channelData] = FBMC_channelAndNoise(FBMC_parameters,...
        FBMC_pregeneratedConstants, FBMC_txData, FBMC_channelData);

    FBMC_rxData = FBMC_receiver(FBMC_parameters, FBMC_pregeneratedConstants,...
                                FBMC_channelSystemData.signal);

    if ~FBMC_rxData.detected
        warning('Frame not detected');
        continue;
    end
    
    detected = 1 + detected;
    estimated_channels(:,:,detected) = FBMC_rxData.channelEstimation;
    
    FBMC_results = FBMC_systemIterationResults(FBMC_parameters,...
                   FBMC_pregeneratedConstants, FBMC_txData, FBMC_rxData);
        
    fprintf('error bits: %d\n', FBMC_results.errorBits);
end
fprintf('finished\n');


[X, Y] = meshgrid(1:FBMC_parameters.basicParameters.timeSymbolsNumber,...
                 ((1:FBMC_parameters.basicParameters.subcarriersNumber)/1024)*1/FBMC_parameters.signalGeneration.dt - 1/2*1/FBMC_parameters.signalGeneration.dt);

figure;
surf(X,Y,abs(FBMC_rxData.channelEstimation));
title('Estimated channel');
xlabel('Time');
ylabel('Frequency');
shading flat;

L = 72;
pdp = zeros(L, detected);

for i = 1:detected
    estChannel = ifftshift(estimated_channels(:,:,i), 1);
    %estChannel(1,:) = mean(estChannel([2 end],:));
    timeResponses = sqrt(size(estChannel,1))*ifft(estChannel);
    timeResponses = timeResponses(1:L,:);
    % timeResponses = timeResponses-mean(timeResponses,2)*ones(1,140); !!
    pdp(:,i) = diag(abs(timeResponses*timeResponses'))/size(estChannel,2);
    pdp(:,i) = pdp(:,i)/max(pdp(:,i));
end

ntaps = 50;
pdp_mean = mean(pdp.',1);
pdp_mean = pdp_mean/max(pdp_mean);

[~,maxIndex] = max(pdp_mean);
taps_time = 1e6*(0:length(pdp_mean)-1)*FBMC_parameters.signalGeneration.dt;
taps_time = taps_time-taps_time(maxIndex);
figure;
stem(taps_time(maxIndex:ntaps),pdp_mean(maxIndex:ntaps))
title('normalized power delay profile')
xlabel('delay [\mus]'),ylabel('normalized magnitude')
grid on
hold on
plot(taps_time(maxIndex:ntaps),pdp_mean(maxIndex:ntaps),'r:');


figure();
obj = FBMC_pregeneratedConstants.channelModel.stdchanChannelObject;
stem(obj.PathDelays*1e6, 10.^(obj.AvgPathGaindB/10));
title('normalized power delay profile')
xlabel('delay [\mus]'),ylabel('normalized magnitude')
grid on
hold on
