function FBMC_plotEstimatedChannel(FBMC_parameters, ...
    FBMC_constants, FBMC_txData, FBMC_rxData, varargin)
%FBMC_PLOTESTIMATEDCHANNEL Plot the estimated channel
%
% This function plots the estimated channel. Aditional parameters can
% be specified using Name,Value pairs.
%
% Name-Value Pairs arguments:
%
% 'XAxisUnits'
%   Change the units used for the X axis. Available values:
%   * 'index0': Indexes starting at 0 (default).
%   * 'index1': Indexes starting at 1.
%   * 'time':   Time.
%
% 'YAxisUnits'
%   Change the units used for the Y axis. Available values:
%   * 'indexDC': Indexes relative to the DC carrier (0) (default).
%   * 'index0':  Indexes starting at 0.
%   * 'index1':  Indexes starting at 1.
%   * 'freq':    frequency.
%
% 'TitleStr'
%   String for the title of the figure. Default is 'Estimated channel'
%
% 'PilotPoits'
%   Superimpose pilot positions. Default true

% assign default values to non created variables
XAxisUnits = 'index0';
YAxisUnits = 'indexdc';
titlestr = 'Estimated channel';
pilotPoits = true;

% assign values specified in Name-Value pair parameters
i = 1;
while i <= numel(varargin)
    if i+1 > numel(varargin)
        error('Value missing from Name-Value pair');
    end
    
    value = varargin{i+1};
    switch lower(varargin{i})
        case 'xaxisunits'
            XAxisUnits = value;
        case 'yaxisunits'
            YAxisUnits = value;
        case 'titlestr'
            titlestr = value;
        case 'pilotpoints'
            pilotPoits = value;
        otherwise
            error('Parameter name %s not valid', varargin{i});
    end
    i = i+2;
end


% main code ---------------------------------------------------------------


if isempty(FBMC_rxData.channelEstimation)
   warning('There is no estimation of the channel');
   return;
end


% number of subcarriers
Snum = FBMC_parameters.basicParameters.subcarriersNumber;

% Create function xcoord and ycoord to convert between indexes and
% units selected.
switch lower(XAxisUnits)
    case 'index0'
        xcoord = @(x) x-1;
        xlabelstr = 'index';
    case 'index1'
        xcoord = @(x) x;
        xlabelstr = 'index';
    case 'time'
        xcoord = @(x) x*FBMC_constants.symbolPeriod*...
                    FBMC_parameters.signalGeneration.dt*1e3;
        xlabelstr = 'ms';
    otherwise
        error('Value of XAxisUnits is not valid');
end

switch lower(YAxisUnits)
    case 'indexdc'
        ycoord = @(y) y - Snum/2 - 1;
        ylabelstr = 'index';
    case 'index0'
        ycoord = @(y) y-1;
        ylabelstr = 'index';
    case 'index1'
        ycoord = @(y) y;
        ylabelstr = 'index';
    case 'freq'
        ycoord = @(y) (y - Snum/2 - 1)*...
            (1/(Snum*FBMC_parameters.signalGeneration.dt))*1e-6;
        ylabelstr = 'MHz';
    otherwise
        error('Value of YAxisUnits is not valid');
end


%figure();
%scShift = 0;% -(FBMC_parameters.basicParameters.subcarriersNumber/2+1);

% plot estimated channel coefficients
[fullXGrid, fullYGrid] = meshgrid(1:size(FBMC_rxData.channelEstimation,2),...
                                  1:size(FBMC_rxData.channelEstimation,1));
surf(xcoord(fullXGrid), ycoord(fullYGrid), abs(FBMC_rxData.channelEstimation));
hold on;
shading flat;

% plot superimposed pilot positions
if pilotPoits
    stem3(xcoord(FBMC_constants.pilots.ActualPilotXGrid),...
          ycoord(FBMC_constants.pilots.ActualPilotYGrid),...
          reshape(abs(FBMC_rxData.channelEstimation(...
                      FBMC_constants.pilots.ActualPilotMask)),...
                  size(FBMC_constants.pilots.ActualPilotXGrid)),...
          'm', 'filled', 'LineStyle', 'none', 'MarkerSize', 2);
end


% plot superimposed erroneous symbols recived
% modOrder = FBMC_parameters.basicParameters.modulationOrder;
% rxSymbolGrid = zeros(size(FBMC_rxData.decodedDataGrid));
% rxSymbolGrid(FBMC_pregeneratedConstants.dataMask) = ...
%     FBMC_parameters.basicParameters.modulationFunction(FBMC_rxData.message,modOrder);
% 
% txSymbolGrid = FBMC_txData.dataGrid;
% txSymbolGrid(~FBMC_pregeneratedConstants.dataMask) = 0;
% 
% errorSymbols = nan(size(txSymbolGrid));
% errorSymbols(rxSymbolGrid ~= txSymbolGrid) = ...
%     abs(FBMC_rxData.channelEstimation(rxSymbolGrid ~= txSymbolGrid));
% 
% [dataX, dataY] = meshgrid(1:size(txSymbolGrid,2), 1:size(txSymbolGrid,1));
% stem3(dataX, dataY+scShift, errorSymbols,...
%       'r', 'fill', 'LineStyle', 'none', 'MarkerSize', 2);

hold off;

% add labels and legend
ylabel(sprintf('Frequency [%s]', ylabelstr));
xlabel(sprintf('Time [%s]', xlabelstr));
zlabel('Amplitude');
title(titlestr);
legend('estimated channel coefficients',...
       'pilot points superimposed',...
       'location', 'NorthEast');
end

