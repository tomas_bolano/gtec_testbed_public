function FBMC_printRxPreProcInformation(FBMC_parameters, ~)
%FBMC_RXPREPROCINFORMATION Prints some information about the simulation.

FBMC_printSeparatorLine(80, 'Rx Pre-processing chain');
rxPreProc = FBMC_parameters.rxSignalPreProcessing.rxPreProcessingFunctions;
FBMC_printProcInformation(rxPreProc);
FBMC_printSeparatorLine(80);

end

