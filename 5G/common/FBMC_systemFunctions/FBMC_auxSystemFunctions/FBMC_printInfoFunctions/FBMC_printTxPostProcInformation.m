function FBMC_printTxPostProcInformation(FBMC_parameters, ~)
%FBMC_PRINTTXPOSTPROCINFORMATION Prints information about Tx post processing.

FBMC_printSeparatorLine(80, 'Tx Post-processing chain');

txPostProc = FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions;
FBMC_printProcInformation(txPostProc);
FBMC_printSeparatorLine(80);

end

