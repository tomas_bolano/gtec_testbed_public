function FBMC_printChannelInformation(FBMC_parameters, FBMC_pregeneratedConstants)
%FBMC_PRINTCHANNELINFORMATION Prints some basic information about the channel (if available).

try
    channelObject = FBMC_pregeneratedConstants.channelModel.stdchanChannelObject;
    
    FBMC_printSeparatorLine(80, 'Basic Channel Information');

    pathDelays = channelObject.pathDelays;
    AvgPathGaindB = channelObject.AvgPathGaindB;

    Pt = sum(10.^(AvgPathGaindB/10));
    t0 = 1/Pt*pathDelays*10.^(AvgPathGaindB.'/10);
    t_rms= sqrt(1/Pt*pathDelays.^2*10.^(AvgPathGaindB.'/10)-t0^2);
    
    fprintf('rms delay spread [s]:       %g\n',t_rms);
    fprintf('rms delay spread [samples]: %g\n',t_rms/FBMC_parameters.signalGeneration.dt);
    
    FBMC_printSeparatorLine(80);
catch
end


end

