function FBMC_printSeparatorLine(minWidth, title)
%FBMC_PRINTSEPARATORLINE Prints a separation line with an optional title

if nargin < 2
    title = '';
end

minWidth = max(minWidth, length(title)+4);

titleStartPosition = 4;
titleStartString = '{ ';
titleEndString = ' }';

if isempty(title)
    fprintf(repmat('-',1,minWidth));
else
    fprintf([repmat('-',1,titleStartPosition)...
             titleStartString title titleEndString...
             repmat('-',1,minWidth-length(title)-titleStartPosition-...
                          length(titleStartString)-length(titleEndString))]);
end
fprintf('\n');

end

