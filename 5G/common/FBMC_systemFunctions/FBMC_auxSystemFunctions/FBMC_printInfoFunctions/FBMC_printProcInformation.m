function FBMC_printProcInformation(processingParameter)
%FBMC_PRINTPROCINFORMATION General function for printing the processing information

for i = 1:length(processingParameter)
    fprintf('%d - %s(', i, func2str(processingParameter{i}{1}));
    for j = 2:length(processingParameter{i})
        param = processingParameter{i}{j};
        if isnumeric(param) && numel(param) == 1
            fprintf('%g', param);
        elseif ischar(param)
            fprintf('''%s''', param);    
        else
            fprintf('[%dx%d %s]', size(param,1), size(param,2), class(param));
        end
        
        if j ~= length(processingParameter{i})
            fprintf(', ');
        else
            fprintf(')');
        end
    end
    fprintf('\n');
end

end

