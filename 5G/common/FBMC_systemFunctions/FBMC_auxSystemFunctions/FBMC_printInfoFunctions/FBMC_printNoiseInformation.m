function FBMC_printNoiseInformation(FBMC_parameters, FBMC_noiseValues)
%FBMC_PRINTSIMULATIONINFORMATION Prints some information about the simulation.
%
% FBMC_noiseValues is a struct as returned by the function FBMC_calculateNoiseValues

FBMC_printSeparatorLine(80, 'Noise Information');

fprintf('Noise type added: %s\n', FBMC_parameters.channelModel.noise.snrType);
fprintf('Block used as reference: %d\n', FBMC_parameters.channelModel.noise.referenceBlock);


rowheaders = {'SNR (dB)',...
              'EbNo (dB)',...
              'Coded EbNo (dB)',...
              'Analitic BER (AWGN)',...
              'Analitic BER (Rayleigh)'};

colheaders = strcat('block', {' '}, cellfun(@(x) {num2str(x)},...
                    num2cell(1:numel(FBMC_parameters.dataBlock))));

data = cell(numel(rowheaders), numel(FBMC_parameters.dataBlock));

for i = 1:numel(FBMC_parameters.dataBlock)
    mod = FBMC_parameters.dataBlock(i).modulation;
    modOrder = FBMC_parameters.dataBlock(i).modulationOrder;
    
    data{1,i} = sprintf('%f', FBMC_noiseValues.snr_dB(i));
    data{2,i} = sprintf('%f', FBMC_noiseValues.EbNo_dB(i));
    data{3,i} = sprintf('%f', FBMC_noiseValues.codedEbNo_dB(i));
    data{4,i} = sprintf('%f', berawgn(FBMC_noiseValues.EbNo_dB(i), mod, modOrder));
    data{5,i} = sprintf('%f', berfading(FBMC_noiseValues.EbNo_dB(i), mod, modOrder, 1));
end


FBMC_displaytable(data, 'rowheaders', rowheaders,...
                        'colheaders', colheaders,...
                        'maxwidth', 120,...
                        'colwidth', 10,...
                        'rowheaderjustif', 'l',...
                        'colheadermargin', [0 1],...
                        'outerrowsep', '-');


end

