function FBMC_printSignalInformation(FBMC_parameters, FBMC_constants, FBMC_txData)
%FBMC_PRINTSIGNALINFORMATION Prints some information about the signal.


FBMC_printSeparatorLine(80, 'Basic Signal Information');

FBMC_signalLength = numel(FBMC_txData.signal);

fprintf('Signal length:          %d\n', FBMC_signalLength);
fprintf('Number of data blocks:  %d\n', numel(FBMC_parameters.dataBlock));
fprintf('Number of symbols:      %d\n', FBMC_constants.dataBlockTotalSymbolsNumber);
fprintf('Number of bits:         %d\n', FBMC_constants.dataBlockTotalBitsNumber);
fprintf('Number of uncoded bits: %d\n', FBMC_constants.dataBlockTotalUncodedBitsNumber);
fprintf('Pilot density:          %g\n', nnz(FBMC_constants.pilots.ActualPilotMask)/...
                                    nnz(FBMC_constants.dataMask));
fprintf('Sampling time:          %g s\n', FBMC_parameters.signalGeneration.dt);
fprintf('Bandwidth:              %g MHz\n', (1/FBMC_parameters.signalGeneration.dt)/1e6);
fprintf('Signal duration:        %g s\n', FBMC_signalLength*FBMC_parameters.signalGeneration.dt);

FBMC_printSeparatorLine(80);

end

