function FBMC_printSimulationInformation(FBMC_parameters, FBMC_pregeneratedConstants)
%FBMC_PRINTSIMULATIONINFORMATION Prints some information about the simulation.

FBMC_printSeparatorLine(80, 'Basic Parameters Information');

fprintf('System type:          %s\n', FBMC_pregeneratedConstants.metadata.systemDesc);
fprintf('Subcarriers:          %d\n', FBMC_parameters.basicParameters.subcarriersNumber);
fprintf('Time symbols:         %d\n', FBMC_parameters.basicParameters.timeSymbolsNumber);
fprintf('Channel:              %s\n', FBMC_channel2str(FBMC_parameters));
fprintf('Relative speed:       %.1f m/s (%.1f km/h)\n',...
    FBMC_parameters.channelModel.relativeSpeed,...
    FBMC_parameters.channelModel.relativeSpeed*3.6);
fprintf('snr value [dB]:       %f\n', FBMC_parameters.channelModel.noise.snr_dB);
fprintf('snr value metric:     %s\n', FBMC_parameters.channelModel.noise.snrType);
fprintf('Interpolation factor: %d\n', FBMC_parameters.highSpeedEmulation.interpolationFactor);

FBMC_printSeparatorLine(80);


%FBMC_printSeparatorLine(80, 'Basic Results Information');

fprintf('Saving uncoded BER results: %d\n', FBMC_parameters.resultsEvaluation.uncodedBER);
fprintf('Saving coded BER results:   %d\n', FBMC_parameters.resultsEvaluation.codedBER);
fprintf('Saving EVM results:         %d\n', FBMC_parameters.resultsEvaluation.EVM);

FBMC_printSeparatorLine(80);

end

