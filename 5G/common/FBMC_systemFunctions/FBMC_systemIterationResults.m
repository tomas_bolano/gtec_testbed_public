function FBMC_systemResults = FBMC_systemIterationResults(FBMC_parameters,...
    FBMC_constants, FBMC_txData, FBMC_rxData)
%FBMC_SYSTEMITERATIONRESULTS Compute error bits and the EVM.
%
% TODO documentation

%% Uncoded error bits

if FBMC_parameters.resultsEvaluation.uncodedBER
    FBMC_systemResults.uncodedErrorBits = zeros(1, numel(FBMC_parameters.dataBlock));
    
    for i = 1:numel(FBMC_systemResults.uncodedErrorBits)
        FBMC_systemResults.uncodedErrorBits(i) = ...
            nnz(FBMC_txData.dataBlock(i).codedPaddedData.bits ~= ...
            (FBMC_rxData.dataBlock(i).softBits >= 0));
    end
else
    FBMC_systemResults.uncodedErrorBits = [];
end


%% Coded error bits

if FBMC_parameters.resultsEvaluation.codedBER
    FBMC_systemResults.codedErrorBits = zeros(1, numel(FBMC_parameters.dataBlock));
    
    for i = 1:numel(FBMC_systemResults.codedErrorBits)
        FBMC_systemResults.codedErrorBits(i) = ...
            nnz(FBMC_txData.dataBlock(i).uncodedBits ~= ...
            FBMC_rxData.dataBlock(i).decodedData.bits);
    end
else
    FBMC_systemResults.codedErrorBits = [];
end


%% EVM

if FBMC_parameters.resultsEvaluation.EVM
    FBMC_systemResults.EVMdB = zeros(1, numel(FBMC_parameters.dataBlock));

    for i = 1:numel(FBMC_systemResults.EVMdB)
        dataBlockMask = (FBMC_constants.dataBlockMask == i);
        
        if strcmpi(FBMC_constants.metadata.systemType, 'SMT')
            symbolsEVM = real(FBMC_rxData.decodedDataGrid(dataBlockMask));
        else
            symbolsEVM = FBMC_rxData.decodedDataGrid(dataBlockMask);
        end

        FBMC_systemResults.EVMdB(i) = ...
            FBMC_computeEVM_3(symbolsEVM, FBMC_txData.dataGrid(dataBlockMask),...
                              FBMC_parameters.resultsEvaluation.EVMsaturationVec);
    end
else
    FBMC_systemResults.EVMdB = [];
end


end

