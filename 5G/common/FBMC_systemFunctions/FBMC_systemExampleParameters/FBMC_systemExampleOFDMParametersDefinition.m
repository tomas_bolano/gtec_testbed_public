function [FBMC_parameters] = FBMC_systemExampleOFDMParametersDefinition()
%FBMC_parametersDefinition 

% Load the base parameters
FBMC_parameters = FBMC_baseParameters();


%% Basic parameters

% Number of subcarriers
FBMC_parameters.basicParameters.subcarriersNumber = 1024;

% Number of time-symbols
FBMC_parameters.basicParameters.timeSymbolsNumber = 62;


%% Data blocks parameters

% Stablish all datablocks to use 4-QAM modulation
% for i = 1:numel(FBMC_parameters.dataBlock)
%     FBMC_parameters.dataBlock(i).modulation = 'qam';
%     FBMC_parameters.dataBlock(i).modulationOrder = 4;
% end


%% Pulse shapping parameters

% Cyclic Prefix lenght for OFDM
FBMC_parameters.pulseShapping.cyclicPrefixLength = 72;


%% Pilots

% Pilot generation function
% with format {function, param1, ... paramN}
FBMC_parameters.pilots.pilotsGenerationFunction = ...
    {@FBMC_squarePilotIndexer, [0,0,0,0]};

% Frequency spacing of the pilots
FBMC_parameters.pilots.freqSpacing = 8;

% Time spacing of the pilots
FBMC_parameters.pilots.timeSpacing = 5;

% Pilot modulation ('qam' or 'pam')
FBMC_parameters.pilots.modulation = 'qam';

% Pilot modulation order
FBMC_parameters.pilots.modulationOrder = 4;

% Pilots modulation normalized
FBMC_parameters.pilots.modulationNormalize = true;

% Pilots energy factor
FBMC_parameters.pilots.modulationEnergyFactor = 1;


% Number of auxiliary pilots to use for FBMC.
%   Possible values:
%       0 for no auxiliary pilots,
%       1 for plain auxiliary pilot method.
%       2, 4 or 8 for CAP (coded auxiliary pilot method).
FBMC_parameters.pilots.numberAuxiliaryPilots = 0;


%% Modulator

% Modulator function
% with format {function, param1, ... paramN}
FBMC_parameters.modulator.modulatorFunction = ...
    {@FBMC_ofdmcpModulator_2};


%% Demodulator

% Demodulator function
% with format {function, param1, ... paramN}
FBMC_parameters.demodulator.demodulatorFunction = ...
    {@FBMC_ofdmcpDemodulator_2};

