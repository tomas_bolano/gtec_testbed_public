function [FBMC_parameters] = FBMC_systemExampleHermiteParametersDefinition()
%FBMC_parametersDefinition

% Load the base parameters
FBMC_parameters = FBMC_baseParameters();

%% Basic parameters

% Number of subcarriers
FBMC_parameters.basicParameters.subcarriersNumber = 1024;

% Number of time-symbols
FBMC_parameters.basicParameters.timeSymbolsNumber = 134;


%% Data blocks parameters

% Stablish all datablocks to use 2-PAM modulation
for i = 1:numel(FBMC_parameters.dataBlock)
    FBMC_parameters.dataBlock(i).modulation = 'pam';
    FBMC_parameters.dataBlock(i).modulationOrder = 2;
end


%% Pulse shapping parameters

% Pulse overlapping Factor
% The overapping factor is definned as L/T,
% where L is the pulse lenght and T the pulse period.
FBMC_parameters.pulseShapping.overlappingFactor = 4; 

% Pulse generation function
% with format {function, param1, ... paramN}
FBMC_parameters.pulseShapping.pulseGenerationFunction =...
    {@FBMC_generateHermitePulse};


%% Pilots

% Pilot generation function
% with format {function, param1, ... paramN}
FBMC_parameters.pilots.pilotsGenerationFunction = ...
    {@FBMC_squarePilotIndexer, [1,1,1,1]};

% Frequency spacing of the pilots
FBMC_parameters.pilots.freqSpacing = 8;

% Time spacing of the pilots
FBMC_parameters.pilots.timeSpacing = 10;

% Pilot modulation ('qam' or 'pam')
FBMC_parameters.pilots.modulation = 'pam';

% Pilot modulation order
FBMC_parameters.pilots.modulationOrder = 2;

% Pilots modulation normalized
FBMC_parameters.pilots.modulationNormalize = true;

% Pilots energy factor
FBMC_parameters.pilots.modulationEnergyFactor = 2;

% Number of auxiliary pilots to use for FBMC.
%   Possible values:
%       0 for no auxiliary pilots,
%       1 for plain auxiliary pilot method.
%       2, 4 or 8 for CAP (coded auxiliary pilot method).
FBMC_parameters.pilots.numberAuxiliaryPilots = 8;

% % Number of rows/columns from the center to calculate the interference
% for FBMC. The size of the interference matrix will be
% (1+2*auxPilotMatrixFreqOffset)x(1+2*auxPilotMatrixTimeOffset)
FBMC_parameters.pilots.auxPilotMatrixFreqOffset = 5;
FBMC_parameters.pilots.auxPilotMatrixTimeOffset = 5;


%% Modulator

% Modulator function
% with format {function, param1, ... paramN}
FBMC_parameters.modulator.modulatorFunction = ...
    {@FBMC_smtModulator};


%% Demodulator

% Demodulator function
% with format {function, param1, ... paramN}
FBMC_parameters.demodulator.demodulatorFunction = ...
    {@FBMC_smtDemodulator};
