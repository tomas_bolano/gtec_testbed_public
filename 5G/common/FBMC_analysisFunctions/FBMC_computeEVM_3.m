function EVM = FBMC_computeEVM_3(Simbolos, SimbolosOrixinais, valorSaturacion)

% Simbolos=SimbolosUsuario,OrdeConstelacion=IndiceModulacion,PorcentaxeDescarte=0,VectorValoresSaturacion=vectorSaturacion

if(nargin > 3) 
    Simbolos(abs(real(Simbolos)) > valorSaturacion) = ...
        valorSaturacion*sign(real(Simbolos(abs(real(Simbolos)) > valorSaturacion)))+...
        1i*imag(Simbolos(abs(real(Simbolos)) > valorSaturacion));
    
    Simbolos(abs(imag(Simbolos)) > valorSaturacion) = ...
        real(Simbolos(abs(imag(Simbolos)) > valorSaturacion))+...
        1i*valorSaturacion*sign(imag(Simbolos(abs(imag(Simbolos)) > valorSaturacion)));
end


%% Calculo os niveis de potencia
Ruido = Simbolos-SimbolosOrixinais;
ValorCadraticoRuido = (Ruido'*Ruido)/length(Ruido);
ValorCadraticoSimbolosOrixinais = (SimbolosOrixinais'*SimbolosOrixinais)/...
                                  length(SimbolosOrixinais);


%% Calculo a EVM
EVM = 10*log10(ValorCadraticoRuido/ValorCadraticoSimbolosOrixinais);

