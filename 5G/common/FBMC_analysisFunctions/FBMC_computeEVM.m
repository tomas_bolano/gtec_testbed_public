function [EVM, PotenciaInicial, SimbolosDescartados, SimbolosRemanentesEscalados] = ...
    FBMC_computeEVM(Simbolos,OrdeConstelacion,PorcentaxeDescarte,VectorValoresSaturacion,SimbolosOrixinais)

% Simbolos=SimbolosUsuario,OrdeConstelacion=IndiceModulacion,PorcentaxeDescarte=0,VectorValoresSaturacion=vectorSaturacion

if((nargin>2)&&(PorcentaxeDescarte>0))
    SimbolosADescartar=min(max(0,round(PorcentaxeDescarte*length(Simbolos)/100)),length(Simbolos)-1);
    Simbolos=sort(Simbolos);
    Simbolos=Simbolos(1:(length(Simbolos)-SimbolosADescartar));
    SimbolosDescartados=SimbolosADescartar;
else
    SimbolosDescartados=0;
end

ValoresSaturacion=[];

if(nargin>3)
    if(~isempty(VectorValoresSaturacion>0))
        ValoresSaturacion=VectorValoresSaturacion;
        if(length(VectorValoresSaturacion)==1)
            ValoresSaturacion=[VectorValoresSaturacion VectorValoresSaturacion VectorValoresSaturacion].';
        end
        if (OrdeConstelacion <= 4)
            ValorSaturacion = ValoresSaturacion(1);
        elseif (OrdeConstelacion==16)
            ValorSaturacion = ValoresSaturacion(2);
        else
            ValorSaturacion = ValoresSaturacion(3);
        end
        %disp('CalculaEVM: Realizo o recorte dos s�mbolos de entrada...');
        Simbolos(abs(real(Simbolos))>ValorSaturacion)=ValorSaturacion*sign(real(Simbolos(abs(real(Simbolos))>ValorSaturacion)))+1i*imag(Simbolos(abs(real(Simbolos))>ValorSaturacion));
        Simbolos(abs(imag(Simbolos))>ValorSaturacion)=real(Simbolos(abs(imag(Simbolos))>ValorSaturacion))+1i*ValorSaturacion*sign(imag(Simbolos(abs(imag(Simbolos))>ValorSaturacion)));
    end
end

%% Creo os obxectos modulador e demodulador

hd = modem.qamdemod(OrdeConstelacion);
hm = modem.qammod(OrdeConstelacion);

%% Escalo o sinal de entrada dacordo aos obxectos creados previamente (XA NON)

PotenciaConstelacion = (hd.Constellation.')'*(hd.Constellation.')/length(hd.Constellation);
SimbolosEscalados = Simbolos;
% if(size(SimbolosEscalados,2)>size(SimbolosEscalados,1))
%     SimbolosEscalados=SimbolosEscalados.';
% end
% PotenciaInicial=SimbolosEscalados'*SimbolosEscalados/length(SimbolosEscalados);
% SimbolosEscalados=sqrt(PotenciaConstelacion)*SimbolosEscalados/sqrt(PotenciaInicial);
PotenciaInicial=[];

%% Proceso os s�mbolos orixinais, no caso en que os reciba

if((nargin>4)&&(length(SimbolosOrixinais)==length(Simbolos)))
    SimbolosOrixinaisEscalados = SimbolosOrixinais;
    if(size(SimbolosOrixinaisEscalados,2)>size(SimbolosOrixinaisEscalados,1))
        SimbolosOrixinaisEscalados=SimbolosOrixinaisEscalados.';
    end
    PotenciaInicialOrixinal=SimbolosOrixinaisEscalados'*SimbolosOrixinaisEscalados/length(SimbolosOrixinaisEscalados);
    SimbolosOrixinaisEscalados=sqrt(PotenciaConstelacion)*SimbolosOrixinaisEscalados/sqrt(PotenciaInicialOrixinal);
end


%% Fago unha decisi�n dura sobre os datos proporcionados

if((nargin>4)&&(length(SimbolosOrixinais)==length(Simbolos)))
    SimbolosDecididos = demodulate(hd, SimbolosOrixinaisEscalados);
    %disp('CalculaEVM: Emprego os s�mbolos orixinais...');
else
    SimbolosDecididos = demodulate(hd, SimbolosEscalados);
    %disp('CalculaEVM: Emprego os s�mbolos decididos...');
end

%% Calculo os niveis de potencia

Ruido = SimbolosEscalados-modulate(hm, SimbolosDecididos);
ValorCadraticoRuido = Ruido'*Ruido/length(Ruido);
ValorCadraticoSimbolosDecididos = modulate(hm, SimbolosDecididos)'*modulate(hm, SimbolosDecididos)/length(modulate(hm, SimbolosDecididos));

%% Calculo a EVM

EVM = 10*log10(ValorCadraticoRuido/ValorCadraticoSimbolosDecididos);

SimbolosRemanentesEscalados = SimbolosEscalados;

