function errorBits = FBMC_computeErrorBits(FBMC_txMessage, FBMC_rxMessage,...
    modulation, modOrder)
%FBMC_COMPUTEBER Compute the erroneous number of bits of a received message.

errorind = find(FBMC_txMessage ~= FBMC_rxMessage);
if numel(errorind) > 0
    errorTxSymb = FBMC_txMessage(errorind);
    errorRxSymb = FBMC_rxMessage(errorind);
    %errorTxGraySymb = gray2bin(errorTxSymb, modulation, modOrder);
    %errorRxGraySymb = gray2bin(errorRxSymb, modulation, modOrder);
    errorTxBits = de2bi(errorTxSymb, log2(modOrder));
    errorRxBits = de2bi(errorRxSymb, log2(modOrder));
    errorBits = nnz(errorTxBits ~= errorRxBits);
else
    errorBits = 0;
end

end

