function y = FBMC_smoothByMedian(x, wl)

windowLength = round(wl);
if (mod(windowLength,2)==0)
    windowLength = windowLength+1;
end

pointsEachSide = (windowLength-1)/2;

y = zeros(size(x));

for ii=1:length(x)
    si = max(1,ii-pointsEachSide);
    sf = min(length(x),ii+pointsEachSide);
    y(ii) = median(x(si:sf));
end