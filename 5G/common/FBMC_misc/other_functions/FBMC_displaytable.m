function FBMC_displaytable(data, varargin)
% Prints formatted matrix of numerical data with headings
% 
% Syntax
% 
% FBMC_displaytable(data, colheadings, rowheadings, Name, Value,)
%
%
% Input
%
% data
%   A matrix or cell containing the data of the table.
%
%
% Name-Value Pair Arguments 
% Specify optional comma-separated pairs of Name,Value arguments. Name is
% the argument name and Value is the corresponding value. Name must appear
% inside single quotes (' '). You can specify several name and value pair
% arguments in any order as Name1,Value1,...,NameN,ValueN.
%
% colheaders:
%   A cell array of the column headers.
%
% rowheaders:
%   A cell array of the row headers.
%
% fileid:
%   The file id to print to. Default is 1 (stdout).
%
% format:
%   Cell array or cell matrix with the format specifications to use for the
%   fields of the data. If not specified the default formats for each type
%   of data will be used. If vector, it can be a row vector or column vector.
%   If row vector it must be of the same lenght as the number of columns of
%   the data and each element will be the format for the columns. If column
%   vector it must be of the same lenght as the number of rows of the data
%   and each element will be the format for the rows. If matrix it must be
%   of the same size as the data and each element will be the format of the
%   corresponding element of the data.
%
% defaultintformat:
%   Default format for integer data. Default is '%d'.
%
% defaultfloatformat:
%   Default format for float data. Default is '%f'.
%
% defaultcharformat:
%   Default format for float data. Default is '%s'.
%
% justification:
%   Cell array or cell matrix with the justification specifications to use
%   for the fields of the data. The available values are 'c' for centered,
%   'r' for right and 'l'. for left. If not specified the default justifications
%   for each type of data will be used. If vector, it can be a row vector or
%   column vector. If row vector, it must be of the same lenght as the number
%   of columns of the data and each element will be the format for the
%   columns. If column vector, it must be of the same lenght as the number
%   of rows of the data and each element will be the format for the rows.
%   If matrix it must be of the same size as the data and each element will
%   be the format of the corresponding element of the data.
%
% defaultintjustif:
%   Default justification for integer data. Default is 'r'.
%
% defaultfloatjustif:
%   Default justification for float data. Default is 'r'.
%
% defaultcharjustif:
%   Default justification for float data. Default is 'r'.
%
% colheaderjustif:
%   Char or cell of chars specifying the justification of the column
%   headers. Default is 'r'.
%
% rowheaderjustif:
%   Char specifying the justification of the row headers. Default is 'r'.
%
% colwidth:
%   Scalar or vector of column widths to use for the table. If scalar,
%   every column will have the same width. If vector it must be of the same
%   lenght as the number of columns of the data.
%
% colsep:
%   String to use as the separator string for the columns. Default is  '|'.
%
% rowheadersep:
%   String that indicates the string to use as the separator string for
%   the rows headers and the columns. Default is  '|'.
%
% outercolsep:
%   String that indicates the outer columns separator. Default is none ('').
%
% outerrowsep:
%   Char that indicates the outer rows separator. If empty no
%   row separator will be used. Default is ''.
%
% intercolmargin:
%   2 element vector that specifies the left and right marging of the
%   intermediate columns. Default is [1 1].
%
% colheadermargin:
%   2 element vector that specifies the left and right marging of the
%   rows header initial column. Default is [1 1].
%
% rowsep:
%   Char to use as the separator string for the columns. If empty no
%   row separator will be used. Default is ''.
%
% colheadersep:
%   Char that indicates the string to use as the separator string for
%   the columns headers and the rows. If empty no row separator will be
%   used. Default is  ('').
%
% intersectsep:
%   Char to use as for the intersections of the rows and columns. Default
%   is the default column separator ('|').
%
% outerrowinterctsep:
%   Use the intersect separator in the outer row separator.
%   Default is false.
%
% outercolinterctsep:
%   Use the intersect separator in the outer column separator.
%   Default is false.
%
% maxwidth:
%   Maximum width of the table. If the maximum width is exceeded then the
%   table will be divided in several tables to reduce the width of it. If
%   0 the there is nomaximum width. Default is 0.
%
% suppresswarnings:
%   Do not print warnings. Default is false.


% TODO
% check that strings do not have newlines
% check that input variables have the correct format
% Add separator when the table is split on several tables

%% Initialize variables

% assign default values to non created variables
colheaders = {};
rowheaders = {};

fileid = 1;

format = {};
defaultintformat = '%d';
defaultfloatformat = '%f';
defaultcharformat = '%s';

justification = {};
defaultintjustif = 'r';
defaultfloatjustif = 'r';
defaultcharjustif = 'r';
colheaderjustif = 'r';
rowheaderjustif = 'r';

colwidth = [];
colsep = '|';
rowheadersep = '|';
outercolsep = '';
outerrowsep = '';

intercolmargin = [1 1];
colheadermargin = [1 1];

maxwidth = 0;

rowsep = '';
colheadersep = '';
intersectsep ='|';
outercolinterctsep = false;
outerrowinterctsep = false;
suppresswarnings = false;


% assign values specified in Name-Value pair parameters
i = 1;
while i <= numel(varargin)
    if i+1 > numel(varargin)
        error('Value missing from Name-Value pair');
    end
    value = varargin{i+1};
    switch lower(varargin{i})
        case 'colheaders'
            colheaders = value;
        case 'rowheaders'
            rowheaders = value;
        case 'fileid'
            fileid = value;
        case 'format'
            format = value;
        case 'defaultintformat'
            defaultintformat = value;
        case 'defaultfloatformat'
            defaultfloatformat = value;
        case 'defaultcharformat'
            defaultcharformat = value;
        case 'justification'
            justification = value;
        case 'defaultintjustif'
            defaultintjustif = value;
        case 'defaultfloatjustif'
            defaultfloatjustif = value;
        case 'defaultcharjustif'
            defaultcharjustif = value;
        case 'colheaderjustif'
            colheaderjustif = value;
        case 'rowheaderjustif'
            rowheaderjustif = value;
        case 'colwidth'
            colwidth = value;
        case 'colsep'
            colsep = value;
        case 'rowheadersep'
            rowheadersep = value;
        case 'outercolsep'
            outercolsep = value;
        case 'outerrowsep'
            outerrowsep = value;
        case 'intercolmargin'
            intercolmargin = value;
        case 'colheadermargin'
            colheadermargin = value;
        case 'endcolmargin'
            endcolmargin = value;
        case 'maxwidth'
            maxwidth = value;
        case 'rowsep'
            rowsep = value;
        case 'colheadersep'
            colheadersep = value;
        case 'intersectsep'
            intersectsep = value;
        case 'outercolinterctsep'
            outercolinterctsep = value;
        case 'outerrowinterctsep'
            outerrowinterctsep = value;
        case 'supresswarnings'
            suppresswarnings = value;
        otherwise
            error('Parameter name %s not valid', varargin{i});
    end
    i = i+2;
end




assert(numel(colheadersep) == 1 || numel(colheadersep) == 0,...
       'colheadersep must be a single char or empty');
assert(numel(rowsep) == 1 || numel(rowsep) == 0,...
       'rowsep must be a single char or empty');
assert(numel(outerrowsep) == 1 || numel(outerrowsep) == 0,...
       'outerrowsep must be a single char or empy');
assert(numel(intersectsep) == 1,...
       'intersectsep must be a single char');


%% Code

% Create format cell
if isrow(format)
    my_format = repmat(format, size(data,1), 1);
elseif iscolumn(format)
    my_format = repmat(format, 1, size(data,2));
else
    my_format = format;
end

% Create justification cell
if isrow(format)
    my_justif = repmat(justification, size(data,1), 1);
elseif iscolumn(format)
    my_justif = repmat(justification, 1, size(data,2));
else
    my_justif = justification;
end

% Create header justification cell
if ischar(colheaderjustif)
    colheaderjustif_cell = repmat({colheaderjustif}, 1, size(data,2));
end


% Create cell with the data to print
if iscell(data)
    data_cell = data;
elseif isnumeric(data)
    data_cell = num2cell(data);
else
    error('Invalid type of data.');
end


% Format data
assert(isempty(my_format) || all(size(data_cell) == size(data_cell)),...
       'Wrong size for format');
   
print_cell = cell(size(data_cell));

for i = 1:numel(data)
    element = data_cell{i};
    
    if isempty(my_format)
        if ischar(element)
            print_cell{i} = sprintf(defaultcharformat, element);
        elseif isinteger(element)
            print_cell{i} = sprintf(defaultintformat, element);
        elseif isfloat(element)
            print_cell{i} = sprintf(defaultfloatformat, element);
        else
            error('Invalid element type.');
        end
    else
        print_cell{i} = sprintf(my_format{i}, element);
    end
end


% Calculate columns widths
if ~isempty(colwidth)
    colwidthvec = repmat(colwidth, 1, size(data,2));
else
    if ~isempty(colheaders)
        colwidthvec = max(cellfun(@length, [colheaders; print_cell]));
    else
        colwidthvec = max(cellfun(@length, print_cell));
    end
end


% Fill and justify data
for i = 1:size(print_cell,1)
    for j = 1:size(print_cell,2)
        % Obtain the justification for the data element
        element = data_cell{i,j};
        if isempty(my_justif)
            if ischar(element)
                element_justif = defaultcharjustif;
            elseif isinteger(element)
                element_justif = defaultintjustif;
            elseif isfloat(element)
                element_justif = defaultfloatjustif;
            else
                error('Invalid element type.');
            end
        else
            element_justif = my_justif{i,j};
        end
        
        colmargin = intercolmargin;

        % fill and justify the data element
        [print_cell{i,j}, cut] = fillandjustify(print_cell{i,j},...
            colwidthvec(j), element_justif, colmargin);
    end
end


% Fill and justify columns headers
if ~isempty(colheaders)
    for i = 1:length(colheaders)
        % select column margins to use
        colmargin = intercolmargin;
        
        % fill and justify the header element
        [colheaders{i}, cut] = fillandjustify(colheaders{i},...
            colwidthvec(i), colheaderjustif_cell{i}, colmargin);
    end
end


% Fill and justify row headers
if ~isempty(rowheaders)
    % calculate headers lenghts
    rowheadermaxlength = max(cellfun(@length, rowheaders));

   for i = 1:length(rowheaders)
       colmargin = colheadermargin;
       
       % fill and justify the header element
        [rowheaders{i}, cut] = fillandjustify(rowheaders{i},...
            rowheadermaxlength, rowheaderjustif, colmargin);
   end
else
    rowheadermaxlength = 0;
end


% Add columns headers
if ~isempty(colheaders)
    print_cell = [colheaders; print_cell];
    rowheaders = [repmat(' ', 1, length(rowheaders{1})), rowheaders];
end


% Assemble the data, row headers and column separators on tables.
% Create several tables if width is exceeded
startcol = 1;
tables = {};
rowheadersep_cell = repmat({rowheadersep}, size(print_cell,1), 1);
colsep_cell = repmat({colsep}, size(print_cell,1), 1);
outercolsep_cell = repmat({outercolsep}, size(print_cell,1), 1);

while startcol <= size(data,2)
    if ~isempty(rowheaders)
        table1 = [outercolsep_cell, rowheaders(:), rowheadersep_cell, print_cell(:,startcol)];
    else
        table1 = [outercolsep_cell, print_cell{startcol}];
    end
    
    % If the start column is the last break
    if startcol == size(data,2)
        table1 = [table1, outercolsep_cell];
        tables{end+1} = table1;
        break;
    end
    
    % Else we join more columns
    currentcol = startcol + 1;
    maxwidth_reached = false;
    while currentcol <= size(data,2)
        if maxwidth > 0 
            tablelength = sum(cellfun(@length, table1(1,:)));

            if tablelength + length(colsep) + length(print_cell{1,currentcol}) +...
               length(outercolsep) < maxwidth
                table1 = [table1, colsep_cell, print_cell(:,currentcol)];
                currentcol = currentcol + 1;
            else
                maxwidth_reached = true;
            end
        else
            table1 = [table1, colsep_cell, print_cell(:,currentcol)];
            currentcol = currentcol + 1;
        end

        % If we reached the maximum width break
        if maxwidth_reached
            table1 = [table1, outercolsep_cell];
            break;
        end
    end
    
    tables{end+1} = table1; % save table
    
    % Set the new starting columns for the next iteration
    startcol = currentcol;
end


% Add row separators
for i = 1:length(tables)
    table1 = tables{i};
    
    outerrowsep_cell = cellfun(@(x) {repmat(outerrowsep, 1, length(x))}, table1(1,:));
    rowsep_cell = cellfun(@(x) {repmat(rowsep, 1, length(x))}, table1(1,:));
    colheadersep_cell = cellfun(@(x) {repmat(colheadersep, 1, length(x))}, table1(1,:));

    % assign intersection characters
    for j = 1:2:length(rowsep_cell)
        if ~isempty(outerrowsep) && outercolinterctsep && ...
                (j == 1 || j == length(rowsep_cell))
            continue;
        end
        
        if outerrowinterctsep
            outerrowsep_cell{j} = repmat(intersectsep, 1, length(outerrowsep_cell{j}));
        end
        
        rowsep_cell{j} = repmat(intersectsep, 1, length(rowsep_cell{j}));
        colheadersep_cell{j} = repmat(intersectsep, 1, length(colheadersep_cell{j}));
    end
    
    % assign row separators
    if ~isempty(rowsep)
        table_aux = {};
        for k = 1:size(table1,1)-1
            table_aux(end+1,:) = table1(k,:);
            table_aux(end+1,:) = rowsep_cell;
        end
        table_aux(end+1,:) = table1(end,:);
        table1 = table_aux;
    end
    
    % assign column headers separator
    if ~isempty(colheadersep) && ~isempty(rowheaders)
        if ~isempty(rowsep)
            table1(2,:) = colheadersep_cell;
        else
            table1 = [table1(1,:); colheadersep_cell; table1(2:end,:)];
        end
    end
    
    % assign outer row separators
    if ~isempty(outerrowsep)
        table1 = [outerrowsep_cell; table1; outerrowsep_cell];
    end
    
    % assign the table again to the tables cell
    tables{i} = table1;
end


% Print tables
for i = 1:length(tables)
    table1 = tables{i};
    
    for j = 1:size(table1,1)
        for k = 1:size(table1,2)
            fprintf(fileid, '%s', table1{j,k});
        end
        fprintf(fileid, '\n');
    end
end



end



function [out, cut] = fillandjustify(text, maxwidth, justification, margins)
    if length(text) < maxwidth
        N = maxwidth - length(text);
        
        switch justification
            case 'c'
                out = [repmat(' ', 1, ceil(N/2)) text repmat(' ', 1, floor(N/2))];
            case 'r'
                out = [repmat(' ', 1, N) text];
            case 'l'
                out = [text repmat(' ', 1, N)];
            otherwise
                error('Inavlid justification value');
        end
        
        cut = false;
    else
        out = text(1:maxwidth);
        cut = true;
    end
    
    out = [repmat(' ', 1, margins(1)) out repmat(' ', 1, margins(2))];
end

