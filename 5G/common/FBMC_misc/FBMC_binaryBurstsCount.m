function FBMC_binaryBurstsCount(in)
% FBMC_BINARYBURSTCOUNT counts the number and lenght of sequences of 0's and 1's

currentvec = zeros(1000,2);
current = in(1);
count = 1;

for i = 2:length(in)
    switch in(i)
        case current
            count = count + 1;
        case ~current
            if size(currentvec,1) < count
                currentvec(count,:) = 0;
            end
            currentvec(count, current + 1) = currentvec(count, current + 1) + 1;
            current = ~current;
            count= 1;
        otherwise
            error('vector is not binary');
    end
end


if size(currentvec,1) < count
    currentvec(count,:) = 0;
end
currentvec(count, current + 1) = currentvec(count, current + 1) + 1;


%% print results
fprintf('+-----------------------------------+\n');
fprintf('|            burst count            |\n');
fprintf('+-----------+-----------------------+\n');
fprintf('|           |         bits          |\n');
fprintf('+  length   +-----------------------+\n');
fprintf('|           |         0 |         1 |\n');
fprintf('+-----------+-----------+-----------+\n');

for i = 1:size(currentvec)
    if nnz(currentvec(i,:))
        fprintf('| %9d | %9d | %9d |\n', i, currentvec(i,1), currentvec(i,2));
    end
end

fprintf('+-----------------------------------+\n');

end