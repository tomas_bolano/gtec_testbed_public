function [execData, execPerSecond] = FBMC_executionsPerSecond(execData)
%FBMC_EXECUTIONSPERSECOND Calculate the number of times per second this function is called
%
%   INPUT ARGUMENTS:
%       execData - data structure with the required parameters to calculate
%           the result. It must be an empty matrix on the first call.
%
%   OUTPUT ARGUMENTS:
%       execData - the input execData structure updated, or initialized
%           on the first call of the function.


if isempty(execData)
    qmax = 10;
    execData = struct('timer', tic,...
                      'timeq', zeros(1,qmax),...
                      'qsize', 0,...     % number of elements in the queue
                      'qind', 1);        % index to insert an element
end

minelem = 3;    % minimum number of elements to do an estimation

% update execData elemets
qmax = length(execData.timeq);
qind = execData.qind;
currtime = toc(execData.timer);
execData.timeq(qind) = currtime;
execData.qsize = min(execData.qsize+1,qmax);
execData.qind = (qind<qmax)*qind+1;

% estimate time
if execData.qsize > minelem
    qsize = execData.qsize;
    tvec = diff([execData.timeq(execData.qind:end) execData.timeq(1:execData.qind-1)]);
    execPerSecond = (qsize-1)/sum(tvec);
else
    execPerSecond = -1;
end

end

