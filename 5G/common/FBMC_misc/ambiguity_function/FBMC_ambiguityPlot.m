% Plots an ambiguity function
close all

%% Set the parameters to use
%FBMC_parameters = FBMC_systemExampleHermiteParametersDefinition();
%FBMC_parameters = FBMC_systemExamplePHYDYASParametersDefinition();
FBMC_parameters = FBMC_systemExampleFBMCSRRCParametersDefinition();


%% Plot the ambiguity function
FBMC_pregeneratedConstants = FBMC_pregenerateConstants(FBMC_parameters);

pulse = FBMC_pregeneratedConstants.pulse.TxPulse;
T = FBMC_parameters.basicParameters.subcarriersNumber/2;
F = 2*T;
tau = -4*T:32:4*T;
mu = (-4:0.1:4)/F;

[taugrid, mugrid] = meshgrid(tau, mu);

tic();
amb = FBMC_ambiguityFunction2(pulse.', tau, mu);
toc();


fig = figure();
surf(taugrid/T, mugrid*F, (abs(amb)));
%pcolor(taugrid/T, mugrid*F, abs(amb));
%contour(taugrid/T, mugrid*F, abs(amb), (0:0.01:1).^2);
shading flat;
%shading interp;
title('Ambiguity');
xlabel('\tau/T');
ylabel('\nuF');


figure();
plot(mu, abs(amb(:, tau == 0)));
title('Ambiguity with \tau=0');
ylabel('Amplitude');
xlabel('\nuT');