function ambiguity = FBMC_ambiguityFunction2(pulse, tau, mu, center)
% FBMC_AMBIGUITYFUNCTION2 Calculates the ambiguity function
%
% Equivalent to the function FBMC_ambiguityFunction
%
% Input patameters:
%   pulse: Column vector with the pulse to calculate the ambiguity.
%   tau: vector of time displacements
%   mu: vector of frequence displacements
%   center: index of the position to use as the cero element of the pulse. If
%           not specified the value used will be half of the size of the pulse.

if nargin < 4
    center = floor(length(pulse)/2);
end

ambiguity = zeros(length(mu), length(tau));
pulse = [zeros(1,max(abs(tau))) pulse zeros(1,max(abs(tau)))];

t = 0:length(pulse)-1;
t = t-max(abs(tau))-center;

for tauind = 1:length(tau)
    tauval = tau(tauind);
    
    p1 = pulse;
    p2 = circshift(pulse, [1 -tauval]);
    
    for muind = 1:length(mu)
        muval = mu(muind);
        
        % expr = p1.*p2.*exp(-1j*2*pi*muval*t)*exp(-1j*pi*muval*tauval); % expression to integrate
        expr = p1.*p2.*exp(-1j*2*pi*muval*t); % expression to integrate
        %ambiguity(muind, tauind) = trapz(t, expr); % integral aproximation
        ambiguity(muind, tauind) = sum(expr.');
    end
end


end

