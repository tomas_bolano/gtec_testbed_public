function ambiguity = FBMC_ambiguityFunction(pulse, tau, mu, center)
% FBMC_AMBIGUITYFUNCTION Calculates the ambiguity function
%
% Input patameters:
%   pulse: Column vector with the pulse to calculate the ambiguity.
%   tau: vector of time displacements
%   mu: vector of frequence displacements
%   center: index of the position to use as the cero element of the pulse. If
%           not specified the value used will be half of the size of the pulse.

if nargin < 4
    center = floor(length(pulse)/2);
end

ambiguity = zeros(length(mu), length(tau));
pulse = [zeros(1,max(abs(tau))) pulse zeros(1,max(abs(tau)))];

t = 0:length(pulse)-1;
t = t-max(abs(tau))-center;

for tauind = 1:length(tau)
    tauval = tau(tauind);
    
    disp = round(tauval/2); % number of samples to displace the pulse
    p1 = circshift(pulse, [1 disp]);
    p2 = circshift(pulse, [1 -disp]);
    
    for muind = 1:length(mu)
        muval = mu(muind);
        
        expr = p1.*p2.*exp(-1j*2*pi*muval*t); % expression to integrate
        %ambiguity(muind, tauind) = trapz(t, expr); % integral aproximation
        ambiguity(muind, tauind) = sum(expr); % integral aproximation
    end
end


end

