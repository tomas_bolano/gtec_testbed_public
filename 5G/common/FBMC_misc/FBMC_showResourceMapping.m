function FBMC_showResourceMapping(FBMC_parameters, varargin)
%FBMC_SHOWRESOURCEMAPPING Plots an image showing the FBMC frame structure
%
% This function plots the FBMC frame structure. Aditional parameters can
% be specified using Name,Value pairs.
%
% Name-Value Pairs arguments:
%
% 'XAxisUnits'
%   Change the units used for the X axis. Available values:
%   * 'index0': Indexes starting at 0 (default).
%   * 'index1': Indexes starting at 1.
%   * 'time':   Time.
%
% 'YAxisUnits'
%   Change the units used for the Y axis. Available values:
%   * 'indexDC': Indexes relative to the DC carrier (0) (default).
%   * 'index0':  Indexes starting at 0.
%   * 'index1':  Indexes starting at 1.
%   * 'freq':    frequency.
%

%#ok<*AGROW>


% assign default values to non created variables
XAxisUnits = 'index0';
YAxisUnits = 'indexdc';

% assign values specified in Name-Value pair parameters
i = 1;
while i <= numel(varargin)
    if i+1 > numel(varargin)
        error('Value missing from Name-Value pair');
    end
    value = varargin{i+1};
    switch lower(varargin{i})
        case 'xaxisunits'
            XAxisUnits = value;
        case 'yaxisunits'
            YAxisUnits = value;
        otherwise
            error('Parameter name %s not valid', varargin{i});
    end
    i = i+2;
end


% main code ---------------------------------------------------------------


FBMC_constants = FBMC_pregenerateConstants(FBMC_parameters);

dataSize = size(FBMC_constants.dataMask);

rgb2color = @(x) hex2dec(reshape(x, 2, 3).').'/255;

% Color mapping
dataColor      = 0.9*rgb2color('7E2F8E');
guardBandColor = 0.9*rgb2color('00FFFF');
pilotColor     = 0.9*rgb2color('00FF00');
auxPilotColor  = 0.6*rgb2color('FF0000');
nullColor      = rgb2color('7F7F7F');

dataBlockColors = {rgb2color('0074BD'),...
                   rgb2color('D95319'),...
                   rgb2color('EDB120'),...
                   rgb2color('4DBEEE')
                  };

% data img
dataImg = {zeros(dataSize);  % red
           zeros(dataSize);  % green
           zeros(dataSize)}; % blue

% preamble 
preambleMask = FBMC_constants.preamble.preambleMask;
preambleSize = size(preambleMask);
preambleImg = {zeros(preambleSize);  % red
               zeros(preambleSize);  % green
               zeros(preambleSize)}; % blue

% zeros
zerosSize = [dataSize(1) FBMC_parameters.synchronization.zerosAfterPreamble];
zerosImg = {zeros(zerosSize);  % red
            zeros(zerosSize);  % green
            zeros(zerosSize)}; % blue

% Cell to store in each row the block labels specifying the
% [x y] coordinate and the block number.
blockLabelsCell = {};

% give values to the (data, preamble, zeros) images
for i = 1:3
    % data img
    dataImg{i}(:,:) = nullColor(i);
    dataImg{i}(FBMC_constants.usedSubcarriers.guardMask,:) = guardBandColor(i);
    
    dataImg{i}(FBMC_constants.pilots.NonZeroPilotMask | ...
               FBMC_constants.pilots.ActualPilotMask) = pilotColor(i);

    k = 1;
    for j = 1:numel(FBMC_parameters.dataBlock)
        dataImg{i}(FBMC_constants.dataBlockMask == j) = dataBlockColors{k}(i);
        k = mod(k, numel(dataBlockColors)) + 1;
        
        % The block asigned elements may be separated forming clusters
        % We find the clusters and store the center coordinates to add a
        % label later.
        clusters = bwlabel(FBMC_constants.dataBlockMask == j, 4);
        clusterind = unique(clusters(FBMC_constants.dataBlockMask == j));
        for l = 1:numel(clusterind)
            [clusterX, clusterY] = find(clusters == clusterind(l));
            blockLabelsCell(end+1, :) = {mean([clusterX clusterY], 1), j};
        end
    end
    
    dataImg{i}(FBMC_constants.dataBlockMask == -1) = dataColor(i);
    
    if (FBMC_parameters.pilots.numberAuxiliaryPilots ~= 0)
        dataImg{i}(FBMC_constants.auxpilots.auxPilotFullCodedMask) = auxPilotColor(i);
    end
    
    % preamble img
    if preambleSize(2) ~= 0
        preambleImg{i}(:,:) = nullColor(i);
        preambleImg{i}(FBMC_constants.usedSubcarriers.guardMask,:) = guardBandColor(i);
        preambleImg{i}(preambleMask) = pilotColor(i);
    end
    
    
    % zeros img
    if zerosSize(2) ~= 0
        zerosImg{i}(:,:) = nullColor(i);
        zerosImg{i}(FBMC_constants.usedSubcarriers.guardMask,:) = guardBandColor(i);
    end
end

dataImg = cat(3,dataImg{:});
preambleImg = cat(3,preambleImg{:});
zerosImg = cat(3,zerosImg{:});

gridImg = [preambleImg zerosImg dataImg];
gridSize = [size(gridImg,1) size(gridImg,2)];

% Create function xcoord and ycoord to convert between indexes and
% units selected.
switch lower(XAxisUnits)
    case 'index0'
        xcoord = @(x) x-1;
        xlabelstr = 'index';
    case 'index1'
        xcoord = @(x) x;
        xlabelstr = 'index';
    case 'time'
        xcoord = @(x) x*FBMC_constants.symbolPeriod*...
                    FBMC_parameters.signalGeneration.dt*1e3;
        xlabelstr = 'ms';
    otherwise
        error('Value of XAxisUnits is not valid');
end

switch lower(YAxisUnits)
    case 'indexdc'
        ycoord = @(y) y - gridSize(1)/2 - 1;
        ylabelstr = 'index';
    case 'index0'
        ycoord = @(y) y-1;
        ylabelstr = 'index';
    case 'index1'
        ycoord = @(y) y;
        ylabelstr = 'index';
    case 'freq'
        ycoord = @(y) (y - gridSize(1)/2 - 1)*...
            (1/(gridSize(1)*FBMC_parameters.signalGeneration.dt))*1e-6;
        ylabelstr = 'MHz';
    otherwise
        error('Value of YAxisUnits is not valid');
end


figure();
hold on;
%image(1:gridSize(2),1:gridSize(1),gridImg);

image(xcoord([1 gridSize(2)]), ycoord([1 gridSize(1)]), gridImg);


% Plot lines to separate symbols in time
for i = 1:gridSize(2)-1 
    plot(xcoord([i+0.5 i+0.5]), ycoord([1-0.5 gridSize(1)+0.5]), 'k');
end

% Plot block labels
if numel(FBMC_parameters.dataBlock) > 1        
    for i = 1:size(blockLabelsCell,1)
        h = text(xcoord(blockLabelsCell{i,1}(2) + ...
                        preambleSize(2) + zerosSize(2)),...
                 ycoord(blockLabelsCell{i,1}(1)),...
                 sprintf('%d', blockLabelsCell{i,2}),...
                 'FontSize', 10, 'FontWeight', 'bold',...
                 'HorizontalAlignment', 'Center') ;
         set (h, 'Clipping', 'on');
    end
end

% The legend will be specified using a colormap.
% In the following variables we store the colors and corresponding labels.
ColorMap = [];
ColorLabel = {};

if any(any(FBMC_constants.usedSubcarriers.guardMask))
    ColorMap(end+1,:) = guardBandColor;
    ColorLabel{end+1} = 'Guard';
end

if any(any(FBMC_constants.pilots.ActualPilotMask))
    ColorMap(end+1,:) = pilotColor;
    ColorLabel{end+1} = 'Pilot';    
end

if (FBMC_parameters.pilots.numberAuxiliaryPilots ~= 0)
    ColorMap(end+1,:) = auxPilotColor;
    ColorLabel{end+1} = 'Aux Pilot';      
end

ColorMap(end+1,:) = nullColor;
ColorLabel{end+1} = 'Null';

if any(any(FBMC_constants.dataBlockMask == -1))
    ColorMap(end+1,:) = dataColor;
    ColorLabel{end+1} = 'Not used';    
end

for i = 1:min(max(unique([blockLabelsCell{:,2}])), numel(dataBlockColors))
    ColorMap(end+1,:) = dataBlockColors{i};
    ColorLabel{end+1} = 'data block';
end


colormap(ColorMap);
ntiks = length(ColorMap);
colorbar('Ticks',      1/(2*ntiks):1/ntiks:1, ...
         'TickLabels', ColorLabel);
set(gca, 'XLim',       xcoord([0.5 gridSize(2)+0.45]),...
         'XTickMode',  'auto', ...
         'YLim',       ycoord([1-0.5 gridSize(1)+0.5]), ...
         'YTickMode',  'auto');
     

title('FBMC frame format');
xlabel(sprintf('Time [%s]', xlabelstr));
ylabel(sprintf('Subcarrier [%s]', ylabelstr));

% Print information about the data blocks
%FBMC_printDataBlocksInformation(FBMC_parameters, FBMC_constants);

end

