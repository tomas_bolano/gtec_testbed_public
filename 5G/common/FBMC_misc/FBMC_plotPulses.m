clear
close all

%% Set Parameters to use for plotting the pulses

FBMC_parameters = {FBMC_systemExampleOFDMParametersDefinition};
FBMC_parameters{1}.pulseShapping.pulseGenerationFunction = {@FBMC_generateSRRCPulse, 0.4};
FBMC_parameters{1}.pulseShapping.overlappingFactor = 8;


%FBMC_parameters = {FBMC_systemExampleHermiteParametersDefinition,...
%                   FBMC_systemExamplePHYDYASParametersDefinition};

% Note that for plotting the pulses the period (T) considered is the
% number of subcarriers of the system, so for OFDM with CP this code
% would not be correct.


%% Plot pulses
FBMC_constants = cellfun(@FBMC_pregenerateConstants, FBMC_parameters,...
                         'UniformOutput', false);

for i = 1:length(FBMC_constants)
    currparam = FBMC_parameters{i};
    currconst = FBMC_constants{i};
    
    % Get and normalize pulse
    pulse = currconst.pulse.TxPulse;
    pulse = pulse/max(abs(pulse));
    
    % Plot pulse (time)
    figure();
    x = floor(-length(pulse)/2):floor(length(pulse)/2)-1;
    x = x/currparam.basicParameters.subcarriersNumber;
    
    plot(x, pulse);
    xlim(round(x([1 end])));
    ax1 = axis();
    for j = round(x(1)):round(x(end))
        line([j j], [-1 1], 'Color', [1 1 1]*0.6, 'LineStyle', '--');
    end
    axis(ax1);
    ylabel('Amplitude')
    xlabel('t/T');
    title([currconst.metadata.systemDesc  ' pulse (time)']);
    grid on;
    
    
    % Plot pulse (freq)
    figure();
    N = 300;
    pulse_freq = abs(fft([pulse.' zeros(1,(N-1)*currparam.basicParameters.subcarriersNumber)]));
    pulse_freq = pulse_freq/max(pulse_freq);

    fft_center = ceil((length(pulse_freq)+1)/2);
    fft_size = length(pulse_freq);
    pulse_freq = fftshift(pulse_freq);
    pulse_freq = pulse_freq(fft_center+(-N*8+1:N*8));

    pulse_freq_x = floor(-length(pulse_freq)/2)+1:floor(length(pulse_freq)/2);
    %pulse_freq_x = 0:length(pulse_freq)-1;
    pulse_freq_x = pulse_freq_x/(fft_size/currparam.basicParameters.subcarriersNumber);

    plot(pulse_freq_x, 10*log10(pulse_freq));
    %plot(pulse_freq_x, (pulse_freq));
    xlim([-6 6]);
    ylim([-50 0]);
    ax1 = axis();
    line([0 0], [-100 2], 'Color', [1 1 1]*0.6, 'LineStyle', '--');
    for j = -5:5
        line([j j], [-100 2], 'Color', [1 1 1]*0.6, 'LineStyle', '--');
    end
    axis(ax1);
    ylabel('Amplitude [dB]')
    xlabel('f/F');
    title([currconst.metadata.systemDesc  ' pulse (frequency)']);
    grid on;
end


