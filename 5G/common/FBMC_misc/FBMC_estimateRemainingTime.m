function [esttimedata, time] = FBMC_estimateRemainingTime(esttimedata, progress)
%FBMC_ESTIMATEREMAININGTIME Estimates the remaining time needed for an algorithm to finish.
%
%   INPUT ARGUMENTS:
%       esttimedata - data structure with the required parameters to calculate
%           the time. It must be an empty matrix on the first call.
%       progress - the current progress of the algorithm, between 0 and 1.
%
%   OUTPUT ARGUMENTS:
%       esttimedata - the input esttimedata structure updated, or initialized
%           on the first call of the function.
%       time - the estimated time to finish.


assert(progress >= 0 && progress <= 1, 'progress must be between 0 and 1.');

% initialize data on first call
if isempty(esttimedata)
    qmax = 10;
    esttimedata = struct('timer', tic,...
                         'timeq', zeros(1,qmax),...
                         'progq', zeros(1,qmax),...
                         'qsize', 0,...     % number of elements in the queue
                         'qind', 1);        % index to insert an element
end

minelem = 3;    % minimum number of elements to do an estimation

% update esttimedata elemets
qmax = length(esttimedata.timeq);
qind = esttimedata.qind;
currtime = toc(esttimedata.timer);
esttimedata.timeq(qind) = currtime;
esttimedata.progq(qind) = progress;
esttimedata.qsize = min(esttimedata.qsize+1,qmax);
esttimedata.qind = (qind<qmax)*qind+1;

% estimate time
if esttimedata.qsize > minelem
    qsize = esttimedata.qsize;
    % linear regression considering a line that crossess (0,0)
    time = (esttimedata.timeq(1:qsize)*esttimedata.progq(1:qsize)')/...
           (esttimedata.progq(1:qsize)*esttimedata.progq(1:qsize)')-...
           currtime;

    % general linear regression
%     c = cov(esttimedata.timeq(1:qsize), esttimedata.progq(1:qsize));
%     b = c(1,end)/var(esttimedata.progq(1:qsize));
%     a = mean(esttimedata.timeq(1:qsize))-b*mean(esttimedata.progq(1:qsize));
%     time = a + b - currtime;
else
    time = -1;
end

end

