function [FBMC_progressReportData] = FBMC_progressReport(nParam, nIter,...
    currentParamInd, currentIterInd, FBMC_progressReportData,...
    datastr, estimateRemainingTime, estimateIterPerSec)
%FBMC_PROGRESSREPORT Print a progress report of a simulation

if nargin < 7
    estimateRemainingTime = 0;
end

if nargin < 8
    estimateIterPerSec = 0;
end


if isempty(FBMC_progressReportData)
    FBMC_progressReportData.clearString = '';
    FBMC_progressReportData.estimatedTimeData = [];
    FBMC_progressReportData.execPerSecData = [];
    lastwarn('');
    
    fprintf('Total iterations to execute: %d\n', nIter*nParam);
end

progress = ((currentParamInd-1)*nIter+(currentIterInd-1))/(nParam*nIter);
msg = sprintf([datastr ' - %5.2f %%'], progress*100);

if estimateRemainingTime
    [FBMC_progressReportData.estimatedTimeData, time] = ...
        FBMC_estimateRemainingTime(FBMC_progressReportData.estimatedTimeData, progress);
    if time > 0
        msg = [msg sprintf(' - estimated time remaining: %s',...
                           datestr(time/(60*60*24), 'HH:MM:SS'))];
    end
end

if estimateIterPerSec
    [FBMC_progressReportData.execPerSecData, execPerSec] = ...
        FBMC_executionsPerSecond(FBMC_progressReportData.execPerSecData);
    if execPerSec > 0
        msg = [msg sprintf(' - iterations per second: %3.2f', execPerSec)];
    end
end

if ~strcmp(lastwarn,'')
    fwrite(1, [msg sprintf('\n')]);
    lastwarn('');
else
    fwrite(1, [FBMC_progressReportData.clearString msg sprintf('\n')]);
end

FBMC_progressReportData.clearString = repmat(sprintf('\b'), 1, length(msg)+1);



end

