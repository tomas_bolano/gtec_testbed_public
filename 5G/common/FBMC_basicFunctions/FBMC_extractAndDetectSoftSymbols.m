function [FBMC_rxMessage, FBMC_rxBits, FBMC_rxSoftBits] = FBMC_extractAndDetectSoftSymbols(...
    FBMC_parameters, FBMC_pregeneratedConstants, FBMC_dataGrid, FBMC_channelEstimation)
%FBMC_EXTRACTDATA Extract and detect the user symbols from the data grid.
%

demodFun = FBMC_parameters.basicParameters.demodulationFunction;
modOrder = FBMC_parameters.basicParameters.modulationOrder;

% Case 1 - Hard bits
% FBMC_rxMessage = FBMC_dataGrid(FBMC_pregeneratedConstants.dataMask);
% FBMC_rxBits = reshape(de2bi(FBMC_rxMessage, log2(modOrder)).', [], 1);
% FBMC_rxSoftBits = FBMC_rxBits;

% Case 2 - Soft bits using normal decoding
% FBMC_rxMessage = FBMC_dataGrid(FBMC_pregeneratedConstants.dataMask);
% FBMC_rxDemodMessage = demodFun(FBMC_rxMessage, modOrder, 0, 'gray');
% FBMC_rxBits = de2bi(FBMC_rxDemodMessage, log2(modOrder));
% FBMC_rxSoftBits = FBMC_rxBits;
% FBMC_rxSoftBits(FBMC_rxSoftBits == 0) = -1;
% FBMC_rxAbsChannel = abs(FBMC_channelEstimation(FBMC_pregeneratedConstants.dataMask));
% FBMC_rxSoftBits = reshape(bsxfun(@times, FBMC_rxAbsChannel, FBMC_rxSoftBits).', [], 1);

% Case 2 - Soft bits using normal decoding, squared channel coefficients
% FBMC_rxMessage = FBMC_dataGrid(FBMC_pregeneratedConstants.dataMask);
% FBMC_rxDemodMessage = demodFun(FBMC_rxMessage, modOrder, 0, 'gray');
% FBMC_rxBits = de2bi(FBMC_rxDemodMessage, log2(modOrder));
% FBMC_rxSoftBits = FBMC_rxBits;
% FBMC_rxSoftBits(FBMC_rxSoftBits == 0) = -1;
% FBMC_rxAbsChannel = abs(FBMC_channelEstimation(FBMC_pregeneratedConstants.dataMask));
% FBMC_rxSoftBits = reshape(bsxfun(@times, FBMC_rxAbsChannel, FBMC_rxSoftBits).', [], 1);

% Case 3  - Soft bits using llr decoding
FBMC_rxMessage = FBMC_dataGrid(FBMC_pregeneratedConstants.dataMask);
FBMC_rxDemodMessage = demodFun(FBMC_rxMessage, modOrder, 0, 'gray');
FBMC_rxBits = de2bi(FBMC_rxDemodMessage, log2(modOrder));
FBMC_rxSoftBits = FBMC_rxBits;
FBMC_rxSoftBits(FBMC_rxSoftBits == 0) = -1;
FBMC_rxAbsChannel = abs(FBMC_channelEstimation(FBMC_pregeneratedConstants.dataMask));
FBMC_rxSoftBits = reshape(bsxfun(@times, FBMC_rxAbsChannel, FBMC_rxSoftBits).', [], 1);


end

