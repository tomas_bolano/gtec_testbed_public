function FBMC_decodedDataGrid = FBMC_dataGridAuxPilotDecoder(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_dataGrid)
%FBMC_DATAGRIDPILOTDECODER Summary of this function goes here
%   Detailed explanation goes here


FBMC_decodedDataGrid = FBMC_dataGrid;

if FBMC_parameters.pilots.numberAuxiliaryPilots < 2
    return;
end

auxPilotData = FBMC_pregeneratedConstants.auxpilots;

% decode symbols if using the CAP method
C = permute(auxPilotData.codingMatrix, [2 1 3]); % decoding matrix

for j = 1:size(auxPilotData.pilotCoord,1)
    dataRowInd = auxPilotData.pilotCoord(j,2)+(-1:1);
    dataColInd = auxPilotData.pilotCoord(j,1)+(-1:1);
        
    % data submatrix for the current pilot
    dataSubMat = FBMC_decodedDataGrid(dataRowInd, dataColInd);
        
    % decode data
    dataSubMat(auxPilotData.auxPilotUncodedMask) = ...
        C(:,:,auxPilotData.pilotTypeInd(j))*...
        real(dataSubMat(auxPilotData.auxPilotCodedMask));

    % place the decoded data submatrix on the data matrix
    FBMC_decodedDataGrid(dataRowInd, dataColInd) = dataSubMat;
end

end

