function FBMC_dataGrid = FBMC_dataGridGenerator(FBMC_parameters,...
    FBMC_constants, txSymbCell)
%FBMC_DATAGRIDGENERATOR Generates the FBMC data grid to transmit
%
% Inserts into the time-frequency grid the data symbols for each data block
% and the pilots. The symbols must be previously interleaved using the
% function FBMC_dataBlockInterleaver.
%
% TODO complete documentation
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_constants
%       FBMC pregenerated constants.
%   
%   txSymbCell
%       Cell of symbol vector for the different blocks.
%
% Output parameters:
%   FBMC_dataGrid
%       NxM matrix of symbols to transmit, including user data and pilots,
%       where N is the number of subcarriers and M the number of time symbols.


%% Parameters extraction

Snum = FBMC_parameters.basicParameters.subcarriersNumber;
Dnum = FBMC_parameters.basicParameters.timeSymbolsNumber;

pilotData = FBMC_constants.pilots;


%% Calculate and return the FBMC grid

% data grid
dataGrid = zeros(Snum, Dnum);

% insert pilots
dataGrid(pilotData.ActualPilotMask) = pilotData.ActualPilotValues;
dataGrid(pilotData.NonZeroPilotMask) = pilotData.NonZeroPilotValues;

% insert data
if numel(FBMC_parameters.dataBlock) ~= numel(txSymbCell)
    error('Not enough vectors of symbols.');
end

for i = 1:numel(FBMC_parameters.dataBlock);
    dataGrid(FBMC_constants.dataBlockMask == i) = txSymbCell{i};
end


%% assign return value
FBMC_dataGrid = dataGrid;

end

