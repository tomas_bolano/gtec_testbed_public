function [FBMC_preamble, FBMC_actualDataGrid] = FBMC_separatePreamble(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_dataGrid)
%FBMC_SEPARATEPREAMBLE Separate the preamble from the actual data of the data grid.
%

%% Parameters extraction

Npreamble = size(FBMC_pregeneratedConstants.preamble.preambleMask,2);
Nzeros = FBMC_parameters.synchronization.zerosAfterPreamble;


%% Separate preamble

FBMC_preamble = FBMC_dataGrid(:,1:Npreamble);
FBMC_actualDataGrid = FBMC_dataGrid(:,Npreamble+Nzeros+1:end);

end

