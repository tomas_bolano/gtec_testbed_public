function txSymbCell = FBMC_dataBlockInterleaver(FBMC_parameters, txSymbCell)
%FBMC_DATABLOCKINTERLEAVER Interleaves the data blocks
%
% TODO complete documentation
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%   
%   txSymbCell
%       Cell of symbol vector for the different blocks.
%
% Output parameters:
%   txSymbCell
%       Cell of symbol vector of the blocks with interleaved symbols.
%


for i = 1:numel(FBMC_parameters.symbolsInterleaving)
    intlvrParam = FBMC_parameters.symbolsInterleaving(i);
    
    if strcmpi(intlvrParam.interleaver, 'none')
        continue;
    end
    
    modulations = {FBMC_parameters.dataBlock(intlvrParam.dataBlocks).modulation};
    modulationsOrders = [FBMC_parameters.dataBlock(intlvrParam.dataBlocks).modulationOrder];
    
    if numel(unique(modulations)) > 1
        warning('Interpolating blocks with different modulations');
    end
    
    if numel(unique(modulationsOrders)) > 1
        warning('Interpolating blocks with different modulation orders');
    end

    % Group the symbols of the selected data blocks in a single vector
    blockSymbols = [txSymbCell{intlvrParam.dataBlocks}];
    
    % Apply interleaver
    switch lower(intlvrParam.interleaver)
        case 'rnd'
            blockSymbols = randintrlv(blockSymbols, intlvrParam.interleaverData{:});
        otherwise
            error('Invalid interleaver specified.');
    end
    
    % Divide the interleaved vector in chunks of the same size as the
    % data blocks and assign again the blocks to the cell of blocks.
    k = 1;
    for j = 1:numel(intlvrParam.dataBlocks)
        blocki = intlvrParam.dataBlocks(j);
        blockLen = numel(txSymbCell{blocki});
        txSymbCell{blocki} = blockSymbols(k:k+blockLen-1);
        k = k + blockLen;
    end
end


end
