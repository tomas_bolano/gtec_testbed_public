function rxSymbCell = FBMC_dataBlockDeinterleaver(FBMC_parameters, rxSymbCell)
%FBMC_DATABLOCKDEINTERLEAVER Deinterleaves the data blocks
%
% TODO complete documentation
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%   
%   rxSymbCell
%       Cell of symbol vector for the different blocks.
%
% Output parameters:
%   rxSymbCell
%       Cell of symbol vector of the blocks with deinterleaved symbols.
%


for i = 1:numel(FBMC_parameters.symbolsInterleaving)
    intlvrParam = FBMC_parameters.symbolsInterleaving(i);
    
    if strcmpi(intlvrParam.interleaver, 'none')
        continue;
    end

    % Group the symbols of the selected data blocks in a single vector
    blockSymbols = [rxSymbCell{intlvrParam.dataBlocks}];
    
    % Apply deinterleaver
    switch lower(intlvrParam.interleaver)
        case 'rnd'
            blockSymbols = randdeintrlv(blockSymbols, intlvrParam.interleaverData{:});
        otherwise
            error('Invalid interleaver specified.');
    end
    
    % Divide the interleaved vector in chunks of the same size as the
    % data blocks and assign again the blocks to the cell of blocks.
    k = 1;
    for j = 1:numel(intlvrParam.dataBlocks)
        blocki = intlvrParam.dataBlocks(j);
        blockLen = numel(rxSymbCell{blocki});
        rxSymbCell{blocki} = blockSymbols(k:k+blockLen-1);
        k = k + blockLen;
    end
end


end
