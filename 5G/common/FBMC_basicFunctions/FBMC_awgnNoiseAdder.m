function FBMC_outSignal = FBMC_awgnNoiseAdder(FBMC_inSignal, No)
%FBMC_AWGNNOISEADDER Add AWGN noise to a signal
%
% FBMC_outSignal = FBMC_awgnNoiseAdder(FBMC_inSignal, No)
%
% Generates the vector FBMC_outSignal adding a white gaussian noise with
% variance No to the input vector FBMC_inSignal
%

FBMC_outSignal = FBMC_inSignal + sqrt(No/2)*randn(size(FBMC_inSignal)) +...
                              1j*sqrt(No/2)*randn(size(FBMC_inSignal));

end

