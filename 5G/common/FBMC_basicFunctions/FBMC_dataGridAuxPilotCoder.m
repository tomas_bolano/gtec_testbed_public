function FBMC_auxPilotDataGrid = FBMC_dataGridAuxPilotCoder(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_dataGrid)
%FBMC_DATAGRIDGENERATOR Calculate and place the aux. pilots in the data grid.
%
% TODO complete documentation
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_pregeneratedConstants
%       FBMC pregenerated constants.
%   
%   FBMC_dataGrid
%       FBMC data grid to insert the auxiliary pilots.
%
% Output parameters:
%   FBMC_dataGrid
%       NxM matrix of symbols to transmit, including user data and pilots,
%       where N is the number of subcarriers and M the number of time symbols.


%% Parameters extraction
auxPilotNum = FBMC_parameters.pilots.numberAuxiliaryPilots;


%% Calculate and return the FBMC grid

% data grid
dataGrid = FBMC_dataGrid;

if auxPilotNum ~= 0
    % extract auxiliary pilot parameters
    auxPilotData = FBMC_pregeneratedConstants.auxpilots;

    % add auxiliary pilots
    for i = 1:size(auxPilotData.pilotCoord,1)
        % Data matrix limits for the current pilot
        dataRowInd = auxPilotData.dataLimits(i,3):auxPilotData.dataLimits(i,4);
        dataColInd = auxPilotData.dataLimits(i,1):auxPilotData.dataLimits(i,2);

        % Interference matrix limits for the current pilot
        interfRowInd = auxPilotData.interfLimits(i,3):auxPilotData.interfLimits(i,4);
        interfColInd = auxPilotData.interfLimits(i,1):auxPilotData.interfLimits(i,2);

        % data submatrix for the current pilot
        dataSubMat = dataGrid(dataRowInd, dataColInd);

        % Index for some interference data
        pilotTypeInd = auxPilotData.pilotTypeInd(i);

        % interference submatrix for the current pilot
        interfSubMat = auxPilotData.interfMatrixSigned(interfRowInd, interfColInd, pilotTypeInd);

        % interference mask submatrix for the current pilot
        auxPilotSubMask = auxPilotData.auxPilotInterfMask(interfRowInd, interfColInd);
        
        % calculate auxiliary pilot values
        if auxPilotNum == 1 % auxiliary pilot method
            interfVal = sum(dataSubMat(~auxPilotSubMask).*...
                            imag(interfSubMat(~auxPilotSubMask)));
            dataSubMat(auxPilotSubMask) = -interfVal/imag(interfSubMat(auxPilotSubMask));
        else % CAP method
            auxPilotCodedSubMask = auxPilotData.auxPilotInterfCodedMask(interfRowInd,interfColInd);
            auxPilotUncodedSubMask = auxPilotData.auxPilotInterfUncodedMask(interfRowInd,interfColInd);
            interfval = sum(dataSubMat(~auxPilotCodedSubMask).*imag(interfSubMat(~auxPilotCodedSubMask)));
            delta = -interfval/sum(abs(interfSubMat(auxPilotCodedSubMask)).^2)*...
                    imag(interfSubMat(auxPilotCodedSubMask));
            d = auxPilotData.codingMatrix(:,:,pilotTypeInd)*dataSubMat(auxPilotUncodedSubMask) + delta;
            dataSubMat(auxPilotCodedSubMask) = d;
        end

        % place the new data submatrix on the data matrix
        dataGrid(dataRowInd, dataColInd) = dataSubMat;
    end
end

% generate FBMC_auxPilotDataGrid
FBMC_auxPilotDataGrid = dataGrid;

end

