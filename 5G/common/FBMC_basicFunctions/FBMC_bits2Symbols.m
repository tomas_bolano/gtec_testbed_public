function [FBMC_txMessage, FBMC_txSymbols] = FBMC_bits2Symbols(...
    FBMC_parameters, ~, FBMC_inBits)
%FBMC_GENERATETXSYMBOLS Convert the bits to the symbols to transmit


%% Parameters extraction

modOrder = FBMC_parameters.basicParameters.modulationOrder;
modFun = FBMC_parameters.basicParameters.modulationFunction;


%% Generate random symbols


FBMC_inBits = reshape(FBMC_inBits, log2(modOrder), []).';
FBMC_txMessage = bi2de(FBMC_inBits, 'left-msb');
FBMC_txSymbols = modFun(FBMC_txMessage, modOrder);


end

