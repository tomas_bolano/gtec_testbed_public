function FBMC_txInterpolatedSignal = FBMC_highSpeedEmulation_interpolation(...
    FBMC_parameters, FBMC_txSignal)
%FBMC_HIGHSPEEDEMULATION_INTERPOLATION Interpolates a signal (for high speed emulation).
%
% The output signal FBMC_txInterpolatedSignal will have the same energy as the
% input signal FBMC_txSignal.


%% Parameters extraction
% interpolation parameters
interpolationFactor = FBMC_parameters.highSpeedEmulation.interpolationFactor;
filterOrder = FBMC_parameters.highSpeedEmulation.filterOrder;
displacementFreq = FBMC_parameters.highSpeedEmulation.displacementFreq;

%sampling time
dt = FBMC_parameters.signalGeneration.dt; 

%% Interpolation
if interpolationFactor > 1
    if isempty(filterOrder)
        FBMC_txInterpolatedSignal = resample(FBMC_txSignal,...
                interpolationFactor,1)/sqrt(interpolationFactor);
    else
        FBMC_txInterpolatedSignal = resample(FBMC_txSignal,...
                interpolationFactor,1,filterOrder)/sqrt(interpolationFactor);
    end
    
    if displacementFreq ~= 0
        FBMC_txInterpolatedSignal = FBMC_txInterpolatedSignal.*...
            exp(1j*2*pi*displacementFreq*dt*(0:length(FBMC_txInterpolatedSignal)-1)).';
    end
else
    FBMC_txInterpolatedSignal = FBMC_txSignal;
end


end

