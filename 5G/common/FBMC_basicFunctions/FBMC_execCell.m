function varargout = FBMC_execCell(cell_data, param_before, param_after)
%FBMC_EXECCELL Executes a function defined in a cell
%
% cell is a cell with the following format
%   {<function handler>, <parameter1>, <parameter2>,... <parameterN>}
%
% FBMC_execCell executes the function pointeb by the first element of the
% cell passing as parameters first the parameters defined in varargin, and
% then the rest of elements of the cell.

if nargin < 2
    param_before = {};
end

if nargin < 3
    param_after = {};
end

[varargout{1:nargout(cell_data{1})}] = ...
    cell_data{1}(param_before{:}, cell_data{2:end}, param_after{:});

end
