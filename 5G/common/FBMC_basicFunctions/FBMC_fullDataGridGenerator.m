function FBMC_dataGrid = FBMC_fullDataGridGenerator(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_tx_usrsymb)
%FBMC_DATAGRIDGENERATOR Generates the FBMC data grid to transmit
%
% TODO complete documentation
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_pregeneratedConstants
%       FBMC pregenerated constants.
%   
%   FBMC_tx_usrsymb
%       FBMC user symbols to transmit.
%
% Output parameters:
%   FBMC_dataGrid
%       NxM matrix of symbols to transmit, including user data and pilots,
%       where N is the number of subcarriers and M the number of time symbols.


FBMC_dataGrid = FBMC_dataGridGenerator(FBMC_parameters, FBMC_pregeneratedConstants,...
                                       FBMC_tx_usrsymb);

FBMC_dataGrid = FBMC_dataGridAuxPilotCoder(FBMC_parameters, FBMC_pregeneratedConstants,...
                                           FBMC_dataGrid);

FBMC_dataGrid = FBMC_addPreamble(FBMC_parameters, FBMC_pregeneratedConstants,...
                                 FBMC_dataGrid);

end

