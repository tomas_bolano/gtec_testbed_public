function paddedBitsData = FBMC_padTxBits(inBits, nBits)
%FBMC_PADTXBITS Adds a ramdom padding bits to the end of FBMC_inBits.
%
% Given inBits and the total number of bits available nBits, adds a padding
% of random bits to obtain a sequence of nBits.
%
% Returns a struct with the following fields:
%   * paddingLen
%       Number of padding bits added to the end of FBMC_inBits
%   * bits
%       Vector of FBMC_inBits with the padding bits at the end.


nPad = nBits - length(inBits);

if nPad < 0
    error('Data message too long (%d bits for %d bits available)',...
          length(inBits), nBits);
end

bits = [inBits; randi([0 1], nPad, 1)];

paddedBitsData.paddingLen = nPad;
paddedBitsData.bits = bits;

end
