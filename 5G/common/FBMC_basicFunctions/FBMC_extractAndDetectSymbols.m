function FBMC_rxSoftBits = FBMC_extractAndDetectSymbols(...
    FBMC_parameters, FBMC_constants, FBMC_rxSymbols, FBMC_dataBlockNumber,...
    FBMC_channelEstimation)
%FBMC_EXTRACTDATA Extract and detect the user symbols.
%
% This function extracts and detect the user symbols from the symbols
% FBMC_rxSymbols of the data block FBMC_dataBlockNumber. Deinterleaving
% will be applied to the data symbols if neccesary.
%

if nargin < 5
    FBMC_channelEstimation = [];
end

dataBlockParam = FBMC_parameters.dataBlock(FBMC_dataBlockNumber);
dataBlockMask = (FBMC_constants.dataBlockMask == FBMC_dataBlockNumber);
    
FBMC_rxSoftBits = FBMC_symbolDemodulator(FBMC_rxSymbols, dataBlockParam.modulation,...
    dataBlockParam.modulationOrder, dataBlockParam.modulationNormalize,...
    dataBlockParam.modulationEnergyFactor, 'llr').';

if ~isempty(FBMC_channelEstimation)
    FBMC_rxAbsChannel = abs(FBMC_channelEstimation(dataBlockMask)).^2;
    
    FBMC_rxAbsChannel = repelem(FBMC_rxAbsChannel, log2(dataBlockParam.modulationOrder));
    FBMC_rxSoftBits = FBMC_rxAbsChannel.*FBMC_rxSoftBits;
end

end
