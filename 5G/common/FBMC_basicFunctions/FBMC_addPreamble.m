function FBMC_preambleDataGrid = FBMC_addPreamble(FBMC_parameters,...
    FBMC_pregeneratedConstants, FBMC_dataGrid)
%FBMC_DATAGRIDGENERATOR Generates the FBMC data grid to transmit
%
% TODO complete documentation
% Input parameters:
%   FBMC_parameters
%       FBMC parameters.
%
%   FBMC_pregeneratedConstants
%       FBMC pregenerated constants.
%   
%   FBMC_dataGrid
%       FBMC data grid where the preamble will be added.
%
% Output parameters:
%   FBMC_dataGrid
%       NxM matrix of symbols to transmit, including user data and pilots,
%       where N is the number of subcarriers and M the number of time symbols.


%% Parameters extraction

Snum = FBMC_parameters.basicParameters.subcarriersNumber;

preambleData = FBMC_pregeneratedConstants.preamble;


%% Calculate and return the FBMC grid

% preamble grid
preambleGrid = zeros(size(preambleData.preambleMask));
preambleGrid(preambleData.preambleMask) = preambleData.preambleValues;

% zeros grid
zerosGrid = zeros(Snum, FBMC_parameters.synchronization.zerosAfterPreamble);

% generate FBMC_dataGrid
FBMC_preambleDataGrid = [preambleGrid zerosGrid FBMC_dataGrid];

end

