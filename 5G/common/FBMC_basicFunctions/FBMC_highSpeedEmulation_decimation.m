function FBMC_txDecimatedSignal = FBMC_highSpeedEmulation_decimation(...
    FBMC_parameters, FBMC_rxSignal)
%FBMC_HIGHSPEEDEMULATION_INTERPOLATION Decimates a signal (for high speed emulation).


%% Parameters extraction
interpolationFactor = FBMC_parameters.highSpeedEmulation.interpolationFactor;
filterOrder = FBMC_parameters.highSpeedEmulation.filterOrder;
displacementFreq = FBMC_parameters.highSpeedEmulation.displacementFreq;

%sampling time
dt = FBMC_parameters.signalGeneration.dt; 

%% Decimation
if interpolationFactor > 1 
    if displacementFreq ~= 0
        FBMC_rxSignal = FBMC_rxSignal.*...
            exp(-1j*2*pi*displacementFreq*dt*(0:length(FBMC_rxSignal)-1)).';
    end
    
    if isempty(filterOrder)
        FBMC_txDecimatedSignal = resample(FBMC_rxSignal,1,...
                interpolationFactor)*sqrt(interpolationFactor);
    else
        FBMC_txDecimatedSignal = resample(FBMC_rxSignal,1,...
                interpolationFactor,filterOrder)*sqrt(interpolationFactor);
    end
else
    FBMC_txDecimatedSignal = FBMC_rxSignal;
end


end

