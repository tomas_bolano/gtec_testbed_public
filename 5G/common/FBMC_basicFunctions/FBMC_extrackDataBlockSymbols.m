function FBMC_rxSymbolCell = FBMC_extrackDataBlockSymbols(...
    FBMC_parameters, FBMC_constants, FBMC_dataGrid)
%FBMC_EXTRACKDATABLOCKSYMBOLS Extract the data block symbols.
%
% This function extracts the data block symbols from the time-frequency
% grid FBMC_dataGrid and returns them in the cell array FBMC_rxSymbolCell.


FBMC_rxSymbolCell = cell(1, numel(FBMC_parameters.dataBlock));

for i = 1:numel(FBMC_rxSymbolCell)
    FBMC_rxSymbolCell{i} = FBMC_dataGrid(FBMC_constants.dataBlockMask == i);
end

end

