function FBMC_cyclicPrefixRemovalMatrix = FBMC_cyclicPrefixRemovalMatrixGeneration(FBMC_parameters)
%FBMC_cyclicPrefixRemovalMatrixGeneration Definition of the matrix for
%removing the CP
%
% See also FBMC_modulators


%% Parameters extraction

subcarriersNumber =  FBMC_parameters.basicParameters.subcarriersNumber;
cyclicPrefixLength = FBMC_parameters.pulseShapping.cyclicPrefixLength;

%% Matrix construction

FBMC_cyclicPrefixRemovalMatrix = [...
    zeros(subcarriersNumber,cyclicPrefixLength) ...
    eye(subcarriersNumber)...
    ];