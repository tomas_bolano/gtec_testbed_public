function FBMC_cyclicPrefixInsertionMatrix = FBMC_cyclicPrefixInsertionMatrixGeneration(FBMC_parameters)
%FBMC_cyclicPrefixInsertionMatrixGeneration Definition of the matrix for
%inserting the CP
%
% See also FBMC_modulators


%% Parameters extraction

subcarriersNumber =  FBMC_parameters.basicParameters.subcarriersNumber;
cyclicPrefixLength = FBMC_parameters.pulseShapping.cyclicPrefixLength;

%% Matrix construction

FBMC_cyclicPrefixInsertionMatrix = [...
    zeros(cyclicPrefixLength,subcarriersNumber-cyclicPrefixLength) eye(cyclicPrefixLength);
    eye(subcarriersNumber);...
    ];