function FBMC_cyclicPrefixInsertionMatrix = FBMC_auxCyclicPrefixInsertionMatrixGeneration(subcarriersNumber, cyclicPrefixLength)
%FBMC_cyclicPrefixInsertionMatrixGeneration Definition of the matrix for
%inserting the CP
%
% See also FBMC_modulators

%% Matrix construction

FBMC_cyclicPrefixInsertionMatrix = [...
    zeros(cyclicPrefixLength,subcarriersNumber-cyclicPrefixLength) eye(cyclicPrefixLength);
    eye(subcarriersNumber);...
    ];