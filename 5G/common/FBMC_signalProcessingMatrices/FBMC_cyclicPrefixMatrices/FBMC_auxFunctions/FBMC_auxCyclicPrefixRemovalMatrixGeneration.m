function FBMC_cyclicPrefixRemovalMatrix = FBMC_auxCyclicPrefixRemovalMatrixGeneration(subcarriersNumber, cyclicPrefixLength)
%FBMC_cyclicPrefixRemovalMatrixGeneration Definition of the matrix for
%removing the CP
%
% See also FBMC_modulators

%% Matrix construction

FBMC_cyclicPrefixRemovalMatrix = [...
    zeros(subcarriersNumber,cyclicPrefixLength) ...
    eye(subcarriersNumber)...
    ];