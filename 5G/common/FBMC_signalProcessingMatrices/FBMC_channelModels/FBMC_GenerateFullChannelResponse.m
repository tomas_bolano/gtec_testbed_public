function [h, H, FBMC_pregeneratedConstantsS] = FBMC_GenerateFullChannelResponse(FBMC_parameters,FBMC_pregeneratedConstants)

iscellmember = @(A,B) any(cellfun(@(x) isequal(x,A), B));

FBMC_pregeneratedConstantsS = FBMC_pregeneratedConstants;

if iscellmember(@FBMC_stdchanChannelModel,...
                FBMC_parameters.channelModel.channelGenerationFunctions)
            
	[h, H, FBMC_pregeneratedConstantsS.channelModel.stdchanChannelObject] = ...
        FBMC_auxGenerateFullChannelResponse(FBMC_pregeneratedConstants.channelModel.stdchanChannelObject,...
        FBMC_parameters.basicParameters.subcarriersNumber+FBMC_parameters.pulseShapping.cyclicPrefixLength,...
        FBMC_parameters.basicParameters.subcarriersNumber,...
        FBMC_parameters.highSpeedEmulation.interpolationFactor);
    
elseif iscellmember(@FBMC_rayleighChannelModel,...
                FBMC_parameters.channelModel.channelGenerationFunctions)
            
    [h, H, FBMC_pregeneratedConstantsS.channelModel.rayleighchanChannelObject] = ...
        FBMC_auxGenerateFullChannelResponse(FBMC_pregeneratedConstants.channelModel.rayleighchanChannelObject,...
        FBMC_parameters.basicParameters.subcarriersNumber+FBMC_parameters.pulseShapping.cyclicPrefixLength,...
        FBMC_parameters.basicParameters.subcarriersNumber,...
        FBMC_parameters.highSpeedEmulation.interpolationFactor);
	        
else
    
    h=[];
    H=[];
    
end