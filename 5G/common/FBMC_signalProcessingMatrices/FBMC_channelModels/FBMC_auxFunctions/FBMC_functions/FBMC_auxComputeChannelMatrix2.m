function H = FBMC_auxComputeChannelMatrix2(C, delays, fs, sparseMatrix, ntaps, ntaps_ac)
% Función que calcula la matriz H de la respuesta al impulso dada una
% representación de los taps en tiempo del canal.
% C: matriz NxL, donde N es el número de muestras de canal, y L el número
%    de taps.
% delays: array con los delays de los taps en segundos.
% fs: frecuencia de muestreo.
% sparseMatrix: flag booleano para obtener una matriz sparse (por defecto 1)
% ntaps: numero de taps que se usarán (por defecto max(90, 2*numel(delays))
% ntaps_ac: numero de taps anticausales que se usarán (por defecto 30)
%
% Ejemplo:
% >> computeH(randn(20, 2), [0, 2e-8], 1e8)

% TODO - CHECK
% Cuando se construye la matriz de canal usando los taps y delays
% proporcionados por stdchan la matriz H construida por esta función tiene
% una diagonal que no coincide con la del canal real, aunque en valor
% absoluto si que coincide, las partes real e imag no.


assert(size(C,2) == numel(delays),...
       'The number of columns of C must be equal to the number of delays');

if(nargin < 4)
    sparseMatrix = 1;
end

if(nargin < 5)
    ntaps = max(90, 2*numel(delays));
else
    assert(ntaps >= numel(delays), 'ntaps must be greater than numel(delays)');
end

if(nargin < 6)
    ntaps_ac = 30;
else
    assert(ntaps_ac >= 0, 'ntaps_ac must be greater or equal than 0');
end



delays_fs = delays*fs;

Hsize = size(C, 1);

if(sparseMatrix==0)
    H = zeros(Hsize);
else
    H = spalloc(Hsize, Hsize, Hsize*(ntaps+ntaps_ac));
end


% precalculate sinc values to use for each path delay
sinc_val = zeros(ntaps+ntaps_ac, numel(delays_fs));

for jj = 1:numel(delays_fs)
    for kk = 1:(ntaps+ntaps_ac)
        sinc_val(kk,jj) = sinc(delays_fs(jj) - (kk-ntaps_ac-1)).';
    end
end

sinc_val = flipud(sinc_val);
pause on
for ii = 1:length(H)
    v = zeros(1, ntaps+ntaps_ac);

    for jj = 1:numel(delays_fs)
        v = v + C(ii,jj)*sinc_val(:,jj).';
    end
    
    % correct energy of v
    %v = sqrt(sum(abs(C(1,:)).^2))*v/sqrt(sum(abs(v).^2));
    
    %H(ii, :) = circshift([v, zeros(1, Hsize-ntaps-ntaps_ac)],...
    %                     [0, -(ntaps-ii)]);

 	H(ii, max(ii-(ntaps-1), 1):min(length(H), ii+ntaps_ac)) = ...
         v(max(ntaps-ii+1,1):(ntaps+min(ntaps_ac, length(H)-ii)));
end

%H(1:(ntaps-1), (end-ntaps):end) = 0;
%H((end-ntaps):end, 1:(ntaps-1)) = 0;
