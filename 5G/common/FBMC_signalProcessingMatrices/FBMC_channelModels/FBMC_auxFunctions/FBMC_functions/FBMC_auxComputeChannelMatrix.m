function H = FBMC_auxComputeChannelMatrix(C, delays, fs, sparseMatrix)
% Función que calcula la matriz H de la respuesta al impulso dada una
% representación de los taps en tiempo del canal.
% C: matriz NxL, donde N es el número de muestras de canal, y L el número
%    de taps.
% delays: array con los delays de los taps en segundos.
% fs: frecuencia de muestreo.
%
% Ejemplo:
% >> computeH(randn(20, 2), [0, 2e-8], 1e8)

if(nargin<4)
    sparseMatrix=0;
end

delays = round(delays*fs);

Hsize = size(C, 1);
Lsize = max(delays)+1;

if(sparseMatrix==0)
    H = zeros(Hsize);
else
    H = spalloc(Hsize,Hsize,Hsize*length(delays));
end



for ii = 1:length(H)
    v = zeros(1, Lsize);
    v(delays+1) = C(ii, :);
    H(ii, :) = circshift([fliplr(v) zeros(1, Hsize-Lsize)], [0 -(Lsize-ii)]);
end

H(1:(Lsize-1), (end-Lsize):end) = 0;
