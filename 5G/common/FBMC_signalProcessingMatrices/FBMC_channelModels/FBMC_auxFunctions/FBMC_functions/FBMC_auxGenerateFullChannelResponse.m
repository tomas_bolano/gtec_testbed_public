function [h, H, canleS] = FBMC_auxGenerateFullChannelResponse(canle,Nt,N,I)

% Ensure that required channel properties are propperly set

canle.StoreHistory=1;
canle.ResetBeforeFiltering=0;
canle.NormalizePathGains = 1; % NOTA: engadido!!

%% Process

% Generate samples of the channel by filtering data

filter(canle,ones(1,Nt*I));

% Compute channel matrix in time-domain

h = FBMC_auxComputeChannelMatrix(canle.PathGains, canle.PathDelays, 1/canle.InputSamplePeriod,0);

%%%%%%%%%%%%%%%%%%%% MÉTODOS 1 E 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % Compute channel matrix in frequency-domain
% 
% F = dftmtx(N);
% G = FBMC_auxCyclicPrefixInsertionMatrixGeneration(N,Nt-N);
% GH = FBMC_auxCyclicPrefixRemovalMatrixGeneration(N,Nt-N);
% Mi = FBMC_interpolationMatrixGeneration(Nt,I);
% Md = FBMC_decimationMatrixGeneration(Nt,I);
% 
% % MÉTODO 1: Aquí emprego as matrices de subremostreo e submostreoF = FBMC_decimationDFTMatrixGeneration(N,I);
% FH = FBMC_interpolationDFTMatrixGeneration(N,I);
% 
% H=(1/(2*N))*F*GH*Md*h*Mi*G*(F');
% 
% % MÉTODO 2: Isto sería co resample
% % H=((1/(2*N))*(resample((F*GH).',I,1)).')*h*resample(G*(F'),I,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%% MÉTODO 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compute channel matrix in frequency-domain

F = FBMC_auxDecimationDFTMatrixGeneration(N,I);
FH = FBMC_auxInterpolationDFTMatrixGeneration(N,I);
G = FBMC_auxCyclicPrefixInsertionMatrixGeneration(I*N,I*(Nt-N));
GH = FBMC_auxCyclicPrefixRemovalMatrixGeneration(I*N,I*(Nt-N));

% MÉTODO 3: Aquí emprego as matrices de DFT con subremostreo e submostreo

H=(1/(I*N))*F*GH*h*G*FH;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Update channel object

canleS=canle;

%% Comprobacións (non borrar polo momento)
% 
% 
% 
% x=sign(2*(rand(N,1)-.5))+j*sign(2*(rand(N,1)-.5));
% 
% 
% s1 = G*(F')*x;
% 
% sr1=resample(s1,2,1);
% 
% M=G*(F');
% Mr=resample(M,2,1);
% 
% sr2=Mr*x;
% 
% % >> Sen interpolación
% 
% r = (1/N)*F*GH*G*(F')*x;
% 
% % >> Con interpolación
% 
% ri = (1/N)*F*GH*resample(resample(G*(F')*x,2,1),1,2);
% 
% % Sinal interpolada manualmente
% 
% s1 = G*(F')*x;
% s1r=resample(s1,2,1);
% 
% % Sinal interpolada mediante matriz
% 
% M=G*(F');
% Mr=resample(M,2,1);
% s2r=Mr*x;
% 
% % Sinal decimado manualmente
% 
% s3=resample(s1r,1,2);
% r3 = (1/N)*F*GH*s3;
% 
% % Sinal decimado mediante matrices
% 
% M2r=(1/(2*N))*(resample((F*GH).',2,1)).';
% r4=M2r*s1r;
% 
% % Con interpolación total mediante matrices
% 
% rt=((1/(2*N))*(resample((F*GH).',2,1)).')*resample(G*(F'),2,1)*x;
% 
% % Comprobación con interpolacións parciais
% 
% st1 = G*(F')*x;
% st1r = resample(st1,2,1);
% st1rr = resample(st1r,1,2);
% rtr = (1/N)*F*GH*st1rr;