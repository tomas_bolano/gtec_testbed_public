function [h, H, canle] = FBMC_exampleRayleighChannelResponse(dt,fd,tau,pdb,Nt,N,I)

%% Inicializations

% clear
% close all
% format long g
% clc

%% Parameters



% dt = 0.1e-6; % Sampling frequency 200MHz=>5ns
% T = 10e-6;         % "Timeduration" (debe ser da rexilla T/F)
% F = 1/T;           % Debe ser a F (da rexilla T/F)
% fd = 100;
% tau = [dt 2*dt 4*dt];
% pdb = [0 -3 -6];
% U = 1301; % N�mero de mostras de canle
% V=length(tau);

canleE = rayleighchan(dt,fd,tau,pdb);

[h, H, canle] = FBMC_auxGenerateFullChannelResponse(canleE,Nt,N,I);