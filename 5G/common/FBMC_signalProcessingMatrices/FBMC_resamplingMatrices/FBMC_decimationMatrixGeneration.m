function M = FBMC_decimationMatrixGeneration(FBMC_parameters)
%FBMC_DECIMATIONMATRIXGENERATION Generates a matrix to decimate the
%signal
%

M = FBMC_auxDecimationMatrixGeneration(FBMC_parameters.basicParameters.subcarriersNumber,...
    FBMC_parameters.highSpeedEmulation.interpolationFactor);

end
