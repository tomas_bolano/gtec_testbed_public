function M = FBMC_decimationDFTMatrixGeneration(FBMC_parameters)
%FBMC_DECIMATIONMATRIXGENERATION Generates a matrix to decimate the
%signal
%

M = FBMC_auxDecimationDFTMatrixGeneration(FBMC_parameters.basicParameters.subcarriersNumber,...
    FBMC_parameters.highSpeedEmulation.interpolationFactor);

end

