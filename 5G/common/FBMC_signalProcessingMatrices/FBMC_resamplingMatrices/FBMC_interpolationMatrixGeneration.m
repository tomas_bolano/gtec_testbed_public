function M = FBMC_interpolationMatrixGeneration(FBMC_parameters)
%FBMC_INTERPOLATIONMATRIXGENERATION Generates a matrix to interpolate the
%signal
%

M = FBMC_auxInterpolationMatrixGeneration(FBMC_parameters.basicParameters.subcarriersNumber,...
    FBMC_parameters.highSpeedEmulation.interpolationFactor);

end
