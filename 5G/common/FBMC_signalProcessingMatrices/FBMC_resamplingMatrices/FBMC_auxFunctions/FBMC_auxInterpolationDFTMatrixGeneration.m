function M = FBMC_auxInterpolationDFTMatrixGeneration(Ni,I)
%FBMC_INTERPOLATIONMATRIXGENERATION Generates a DFT matrix which
%interpolates the signal in the time domain
%
% Ni :: Number of samples of the input signal
% I  :: Interpolation factor
%

Mt = (dftmtx(I*Ni))';
M = Mt(:,1:Ni);

end
