function M = FBMC_auxDecimationMatrixGeneration(No,I)
%FBMC_DECIMATIONMATRIXGENERATION Generates a matrix to decimate the
%signal
%
% Ni :: Number of samples of the output signal
% I  :: Decimation factor
%

Mt = eye(I*No);

M = Mt(1:I:end,:);

end
