function M = FBMC_auxDecimationDFTMatrixGeneration(No,I)
%FBMC_DECIMATIONMATRIXGENERATION Generates a matrix to decimate the
%signal
%
% No :: Number of samples of the output signal
% I  :: Decimation factor
%

Mt = dftmtx(I*No);

M = Mt(1:No,:);

end
