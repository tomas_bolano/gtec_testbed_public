function M = FBMC_auxInterpolationMatrixGeneration(Ni,I)
%FBMC_INTERPOLATIONMATRIXGENERATION Generates a matrix to interpolate the
%signal
%
% Ni :: Number of samples of the input signal
% I  :: Interpolation factor
%

Mt = eye(Ni);

M = zeros(I*Ni,Ni);

M(1:I:end,:) = Mt;

end
