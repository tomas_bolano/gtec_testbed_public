function M = FBMC_interpolationDFTMatrixGeneration(FBMC_parameters)
%FBMC_INTERPOLATIONMATRIXGENERATION Generates a DFT matrix which
%interpolates the signal in the time domain
%

M = FBMC_auxInterpolationDFTMatrixGeneration(FBMC_parameters.basicParameters.subcarriersNumber,...
    FBMC_parameters.highSpeedEmulation.interpolationFactor);

end
