% GTEC 5G Simulator
%
% Files
%   FBMC_initPath - Path initialization
%   README        - README file
%
% Directories
%   FBMC_analysisFunctions      - Some functions to obtain results
%   FBMC_basicFunctions         - Some wrapper functions
%   FBMC_blockFunctions         - Channel equalization functions
%   FBMC_constantsGenerationFunctions   - Functions to generate the needed
%                                         constants
%   FBMC_misc                   - Some miscellaneous function
%   FBMC_signalProcessingMatrices       - Some functions to obtain matrices
%                                         of the system model
%   FBMC_systemFunctions        - High level functions to perform
%                                 simulations and other evaluations.

