#ifndef INCLUDED_GTIS_COMMANDS_HPP
#define INCLUDED_GTIS_COMMANDS_HPP

#include "netlink/socket.h"
#include <stdint.h>
 
typedef uint16_t gtis_command_id_t;
typedef uint16_t gtis_response_id_t;
typedef uint64_t gtis_buffer_length_t;
typedef double gtis_command_value_t;
typedef double gtis_response_value_t;

const gtis_command_id_t GTIS_COMMAND_EMPTY_VALUE = 0; 

/* TX-only commands */
const gtis_command_id_t GTIS_COMMAND_START_TX = 21;
const gtis_command_id_t GTIS_COMMAND_STOP_TX = 22;
const gtis_command_id_t GTIS_COMMAND_LOAD_INPUT_FILE_SIGNALS = 23;
const gtis_command_id_t GTIS_COMMAND_TX_GAIN = 24;
const gtis_command_id_t GTIS_COMMAND_TX_FREQ = 25;
const gtis_command_id_t GTIS_COMMAND_STREAM_INPUT_SIGNALS = 26;
const gtis_command_id_t GTIS_COMMAND_TX_BANDWIDTH = 27;
const gtis_command_id_t GTIS_COMMAND_TIMED_CONFIG_TX = 28;

/* RX-only commands */
const gtis_command_id_t GTIS_COMMAND_START_RX = 41;
const gtis_command_id_t GTIS_COMMAND_STOP_RX = 42;
const gtis_command_id_t GTIS_COMMAND_SET_RX_FILE = 43;
const gtis_command_id_t GTIS_COMMAND_RX_GAIN = 44;
const gtis_command_id_t GTIS_COMMAND_RX_FREQ = 45;
const gtis_command_id_t GTIS_COMMAND_RX_ANTENNA_MAPPING = 46;
const gtis_command_id_t GTIS_COMMAND_START_STREAMING_RX = 47;
const gtis_command_id_t GTIS_COMMAND_RX_BANDWIDTH = 48;
const gtis_command_id_t GTIS_COMMAND_TIMED_CONFIG_RX = 49;


/* TX and RX commands */
const gtis_command_id_t GTIS_COMMAND_READY = 100;
const gtis_command_id_t GTIS_COMMAND_FINISH = 110;
const gtis_command_id_t GTIS_COMMAND_GET_NODE_VERSION = 120;
const gtis_command_id_t GTIS_COMMAND_GET_NODE_TIME = 130;

/* control commands */
const gtis_command_id_t GTIS_COMMAND_RESPONSE_OK = 1;
const gtis_command_id_t GTIS_COMMAND_RESPONSE_KO = 0;

bool gtis_recv_command_id(gtis_command_id_t * command_id, 
	NL::Socket * client_connection, FILE * fd);
	
bool gtis_recv_command_value(gtis_response_value_t * response_value, 
	NL::Socket * client_connection, FILE * fd);	
	
bool gtis_send_response_id(gtis_response_id_t * response_id, 
	NL::Socket * client_connection, FILE * fd);
	
bool gtis_send_response_value(gtis_response_value_t * response_value, 
	NL::Socket * client_connection, FILE * fd);	

bool gtis_recv_buffer_length(gtis_buffer_length_t * buffer_length, 
	NL::Socket * client_connection, FILE * fd);

bool gtis_send_buffer_length(gtis_buffer_length_t * buffer_length,
        NL::Socket * client_connection, FILE * fd);

bool gtis_recv_buffer(char * buffer, size_t num_bytes_to_receive,
                size_t * num_bytes_received, NL::Socket * client_connection, FILE * fd);

bool gtis_send_buffer(unsigned char * buffer, size_t num_bytes_to_send,
                      size_t * num_bytes_sent, NL::Socket * client_connection, FILE * fd);

/* Utility functions */
bool gtis_recv_uint16(uint16_t * temp_buffer_size, 
	NL::Socket * client_connection, FILE * fd);
	
bool gtis_recv_uint64(uint64_t * value, 
	NL::Socket * client_connection, FILE * fd);
	
bool gtis_recv_double(double * value, 
	NL::Socket * client_connection, FILE * fd);

bool gtis_send_uint16(uint16_t * value,
    NL::Socket * client_connection, FILE * fd);

bool gtis_send_uint64(uint64_t * value,
    NL::Socket * client_connection, FILE * fd);

bool gtis_send_double(double * value,
    NL::Socket * client_connection, FILE * fd);

#endif
