#ifndef INCLUDED_GTIS_UHD_LIB_UTILS_HPP
#define INCLUDED_GTIS_UHD_LIB_UTILS_HPP

#include <uhd/utils/thread.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/usrp/multi_usrp.hpp>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <iostream>
#include <complex>
#include <ctime>
#include <cstdlib>

bool GTIS_wait_for_usrp_lo_locked(uhd::usrp::multi_usrp::sptr usrp,
        const bool verbose,
        const unsigned int total_number_of_trials,
        std::vector < size_t > channel_nums = {0});

bool GTIS_is_usrp_lo_locked(uhd::usrp::multi_usrp::sptr usrp, size_t channel = 0);

bool GTIS_usrp_has_gpsdo(uhd::usrp::multi_usrp::sptr usrp);

bool GTIS_is_usrp_gpsdo_locked(uhd::usrp::multi_usrp::sptr usrp);

bool GTIS_usrp_has_ref(uhd::usrp::multi_usrp::sptr usrp);

bool GTIS_is_usrp_ref_locked(uhd::usrp::multi_usrp::sptr usrp);

/* Waits in a loop for the PPS edge and returns the current value after the pps edge */
uhd::time_spec_t GTIS_wait_for_pps_edge(uhd::usrp::multi_usrp::sptr usrp);

/* Returns true if the GPSDO is locked, false otherwise (including if it is not found) */
bool GTIS_wait_for_usrp_gpsdo_locked(uhd::usrp::multi_usrp::sptr usrp,
    const bool verbose = false,
    const unsigned int total_number_of_trials = 10);

/* Returns true if the GPSDO is locked, false otherwise (including if it is not found) */
bool GTIS_wait_for_usrp_ref_locked(uhd::usrp::multi_usrp::sptr usrp,
    const bool verbose = false,
    const unsigned int total_number_of_trials = 10);

bool GTIS_are_gps_do_and_usrp_time_aligned(
        uhd::usrp::multi_usrp::sptr usrp,
        const bool verbose = false);

bool GTIS_align_usrp_time(uhd::usrp::multi_usrp::sptr usrp,
        const bool verbose = false);

void GTIS_print_all_usrp_sensor_names(uhd::usrp::multi_usrp::sptr usrp);

void GTIS_sleep_current_thread(long secs, bool verbose = false);

/***********************************************************************
 * GTIS_write_nmea_data_from_gpsdo_to_file: writes NMEA GPGGA GPRMC records
 * to a specified file and at a given rate.
 * Parameters:
 * usrp: the pointer to a initializated usrp
 * output_file_name: file name for the output record (text file).
 * seconds_between_measures: delay between two consecutive record queries and
 * specified in seconds. Minimum value is 1.
 * stop_nmea_writter_local: if true, then the recording process stops.
 * verbose: not supported yet
 **********************************************************************/
int GTIS_write_nmea_data_from_gpsdo_to_file (
        uhd::usrp::multi_usrp::sptr usrp,
        const std::string output_file_name,
        const unsigned int seconds_between_measures,
        volatile bool * stop_nmea_writter_local,
        const bool verbose);

#endif // GTIS_USRP_UHD_LIB_UTILS_HPP
