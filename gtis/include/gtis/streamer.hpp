#ifndef INCLUDED_GTIS_STREAMER_HPP
#define INCLUDED_GTIS_STREAMER_HPP

#include <netlink/socket.h>
#include <stdio.h>
#include "gtis/uhd_lib.hpp"
#include "gtis/commands.hpp"

/***********************************************************************
 * stream adquired data through a socket.
 * Parameters:
 * client_connection: NL::Socket ready to send the data to the client.
 * buffer_list: global shared variable with received data
 * num_complex_samples_requested_per_channel: total number of complex-valued
 * samples to stream. Must be > 0.
 * fd: file descriptor to print out log messages. NULL if no message is
 * required.
 **********************************************************************/
template<typename samp_type> int stream_from_usrp_multichannel_buffer(
        NL::Socket * client_connection,
        usrp_multichannel_buffer * buffer_list,
        unsigned long num_complex_samples_requested_per_channel,
        FILE * fd,
        bool print_to_fd) {

    /* boost::thread does not support fd as a NULL parameter. */
    if (not print_to_fd) {
        fd = NULL;
    }
    /* Set realtime Thread priority */
    bool priority_writer = uhd::set_thread_priority_safe(0,true);
    if (not priority_writer && (fd != NULL)){
      fprintf(fd, "Error setting priority to writer thread.\n");
    }

    if (fd != NULL) {
      fprintf(fd, "GLOBAL_STREAMER: number of complex-valued samples requested per channel = %lu\n",
              num_complex_samples_requested_per_channel);
    }

    /* Initialization of used variables */
    size_t num_useful_complex_samples_per_channel = 0;
    uint16_t num_channels = (uint16_t) buffer_list->number_of_channels();
    size_t num_bytes_to_send_per_channel = 0;
    uint16_t num_useful_complex_samples_per_channel_uint16 = 0;

    /* Structure as vector of pointers used to receive in rx_stream->recv(...) */
    std::vector < std::complex < short > * > buffer(num_channels);

    /* Send 2 bytes (unit16_t) with the number of channels */
    if (!gtis_send_uint16(& num_channels, client_connection, fd)) {
        if (fd != NULL) {
            fprintf(fd, "Error sending the number of channels.\n");
        }
      return ~0;
    }

    /* Receive data until maximum number of requested samples is reached */
    try {
      size_t num_bytes_sent = 0;
      unsigned long total_num_bytes_sent = 0;
      unsigned long total_num_complex_samps_sent_per_channel = 0;

      /* Main loop to stream the acquired data */
      while (num_complex_samples_requested_per_channel > total_num_complex_samps_sent_per_channel) {

        /* Request a position in the multichannel_buffer to consume the data */
        buffer_list->reference_buffer_for_consumer(&buffer, num_useful_complex_samples_per_channel);

        /* Number of bytes to send, depending the number of useful complex-valued samples
         * of the buffer being consumed */
        num_bytes_to_send_per_channel = sizeof(std::complex<samp_type>) * num_useful_complex_samples_per_channel;

        num_useful_complex_samples_per_channel_uint16 = (uint16_t) num_useful_complex_samples_per_channel;

        /* Send 2 bytes (unit16_t) with the number of complex-valued samples per channel of the consumed buffer */
        if (! gtis_send_uint16(& num_useful_complex_samples_per_channel_uint16, client_connection, fd)) {
            if (fd != NULL) {
                fprintf(fd, "Error sending the chunk header.");
                return ~0;
            }
        }

        /* Writing the consumed buffer (every channel) in serial mode on the destination file */
        for (size_t i = 0; i < num_channels; i++) {
          gtis_send_buffer((unsigned char *) &(buffer[i][0]), num_bytes_to_send_per_channel,
                  &num_bytes_sent, client_connection, NULL);
          total_num_bytes_sent += (unsigned long) num_bytes_sent;

          if (fd != NULL) {
            fprintf(fd, "GLOBAL_STREAMER: num_useful_complex_samples_per_channel = %lu, total_num_complex_samps_sent_per_channel = %lu\n",
                      num_useful_complex_samples_per_channel, total_num_complex_samps_sent_per_channel);
            fprintf(fd, "    GLOBAL_STREAMER: num_bytes_sent = %lu, total_num_bytes_sent = %lu\n",
                      num_bytes_sent, total_num_bytes_sent);
          }
        }        
        buffer_list->consume();

        /* Accumulating the total number of samples written in order to stop the loop */
        total_num_complex_samps_sent_per_channel += (unsigned long) num_useful_complex_samples_per_channel;

      } //while

      if (fd != NULL) {
        fprintf(fd, "GLOBAL_STREAMER: streaming finished. Sent %lu complex-valued sample(s) per channel.\n",
            total_num_bytes_sent / (num_channels * sizeof(std::complex<samp_type>)));
      }

      return EXIT_SUCCESS;

    } catch(NL::Exception e) {
        if (fd != NULL) {
            fprintf(fd, "NL error: %s\n", e.what());
        }
        return false;
    }
}


/***********************************************************************
 * read_from_stream_to_buffers: read to cache buffers the source stream to transmit
 * Parameters:
 * input_file_names: string with the path to source file
 * num_channels: number of channels. Must be >= 1
 * file_buffers: buffer to store the data that will be transmitted
 **********************************************************************/

int read_from_stream_to_buffers(
        NL::Socket * client_connection,
        size_t num_channels,
        size_t num_complex_samples_per_channel,
        std::vector < std::vector < std::complex < short > > > & file_buffers,
        FILE * fd);


#endif
