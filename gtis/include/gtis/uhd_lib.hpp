#ifndef INCLUDED_GTIS_UHD_LIB_HPP
#define INCLUDED_GTIS_UHD_LIB_HPP

#include <uhd/utils/thread.hpp>
#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/exception.hpp>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/lexical_cast.hpp>
#include <complex>
#include <csignal>
#include <math.h> 
#include <time.h>

#include "gtis/usrp_multichannel_buffer.hpp"
//
// Removed code for old multiple URSPB210 devices
//#include "gtis/recv_parameters.hpp"

#ifdef POSIX_FILE_API
  #include <sys/types.h>
  #include <sys/stat.h>
  #include <fcntl.h>
#else
  #include <stdio.h>
#endif

typedef boost::function<uhd::sensor_value_t (const std::string&)> get_sensor_fn_t;

/* Time delay between usrp timed commands */
const uhd::time_spec_t TIME_DELAY_BETWEEN_COMMANDS = 
	uhd::time_spec_t(0.05);
		
// Timestamp file extension
const std::string timestamp_file_extension = ".usrptimestamp";

/***********************************************************************
 * write adquired data to a single file. Needed to separate streams after the proccess
 * Parameters:
 * output_file_name: file to store received data
 * buffer_list: global shared variable with received data
 * num_requested_samples_per_channel: total number of samples to receive. If 0, receive continuously
 * stop_signal_called_local: signal called to stop the reception
 **********************************************************************/

template<typename samp_type> int write_to_file_from_usrp_multichannel_buffer(
        const std::string output_file_name,
        usrp_multichannel_buffer * buffer_list,
        unsigned long num_requested_samples_per_channel,
        volatile bool * stop_signal_called_local) {

  #ifdef POSIX_FILE_API
    std::cout << "GLOBAL_WRITTER: using POSIX file API" << std::endl;
  #else
    std::cout << "GLOBAL_WRITTER: using standard C file API" << std::endl;
  #endif

  // Set realtime Thread priority
  bool priority_writer = uhd::set_thread_priority_safe(0,true);

  // If not possible to assign priority, print on screen
  if (not priority_writer){
    std::cout << "Error setting priority to writer thread" << std::endl;
  } 

  // Initialization of used variables
  size_t number_of_useful_samples_per_channel = 0;
  uint16_t num_channels = (uint16_t) buffer_list->number_of_channels();
  size_t num_bytes_to_write = 0;
  uint16_t num_samples_payload = 0;

  // Structure as vector of pointers used to receive in rx_stream->recv(...)
  std::vector < std::complex < short > * > buffer(num_channels);

  // Maximum number of samples to receive per channel
  uint16_t max_payload_size_in_samples = (uint16_t) buffer_list->number_of_samples_per_channel();

  // Size of one sample
  uint16_t samp_type_size_in_bytes = (uint16_t) sizeof(std::complex<samp_type>);

  //open the file where received  data is stored
  #ifdef POSIX_FILE_API
    int file_descriptor = open(output_file_name.c_str(), O_CREAT | O_TRUNC | O_NOATIME | O_WRONLY, 0666);

    if (file_descriptor == (-1)) {
      std::cout << boost::format("Error opening the file with name \"%s\"")
        % output_file_name << std::endl;
      return ~0;
    }

    // Writes 2 bytes with the number of channels
    if (write(file_descriptor, & num_channels, sizeof(uint16_t)) 
        != sizeof(uint16_t)) {
      std::cout << "Error writting number of channels to file. " << std::endl;
      return ~0;
    }

    // Writes 2 bytes with the max payload size expressed in number of samples
    if (write(file_descriptor, & max_payload_size_in_samples, sizeof(uint16_t)) 
        != sizeof(uint16_t)) {
      std::cout << "Error writting the max payload size expressed in number of samples to file. " << std::endl;
      return ~0;
    }

    // Writes 2 bytes with the size of each sample in bytes
    if (write(file_descriptor, & samp_type_size_in_bytes, sizeof(uint16_t)) 
        != sizeof(uint16_t)) {
      std::cout << "Error writting the size of each sample in bytes to file. " << std::endl;
      return ~0;
    }

  #else
    FILE * file_descriptor = fopen(output_file_name.c_str(), "wb");


    if (file_descriptor ==  NULL) {
      std::cout << boost::format("Error opening the file with name \"%s\"")
        % output_file_name << std::endl;
      return ~0;
    }

    // Writes 2 bytes with the number of channels
    if (fwrite(& num_channels, sizeof(uint16_t), 1,file_descriptor) != 1) {
      std::cout << "Error writting number of channels to file. " << std::endl;
      return ~0;
    }

    // Writes 2 bytes with the max payload size expressed in number of samples
    if (fwrite(& max_payload_size_in_samples, sizeof(uint16_t), 1, file_descriptor) != 1) {
      std::cout << "Error writting the max payload size expressed in number of samples to file. " << std::endl;
      return ~0;
    }

    // Writes 2 bytes with the size of each sample in bytes
    if (fwrite(& samp_type_size_in_bytes, sizeof(uint16_t), 1, file_descriptor) != 1) {
      std::cout << "Error writting the size of each sample in bytes to file. " << std::endl;
      return ~0;
    }
  #endif

  // Receive data until stop_signal_called_local is activated or  maximum number of  requested samples is reached
  try {
    size_t num_bytes_written = 0;
    unsigned long total_num_bytes_written = 0;
    unsigned long total_num_samps_per_channel_written = 0;

    std::cout << boost::format("GLOBAL_WRITTER: number of requested samples per channel = %d") % num_requested_samples_per_channel << std::endl;

    // Main loop to receive data
    while ((not (*stop_signal_called_local)) and 
      (num_requested_samples_per_channel > total_num_samps_per_channel_written or num_requested_samples_per_channel == 0)) {
      
      // Request a position in the multichannel_buffer to consume the data
      buffer_list->reference_buffer_for_consumer( &buffer, number_of_useful_samples_per_channel);

      // Accumulating the total number of  samples written in order to stop the loop or just statistics
      total_num_samps_per_channel_written += (unsigned long) number_of_useful_samples_per_channel;

      // Number of bytes to write, depending the number of  samples of the buffer being consumed
      num_bytes_to_write = sizeof(std::complex<samp_type>) * number_of_useful_samples_per_channel;

      num_samples_payload = (uint16_t) number_of_useful_samples_per_channel;

      // Writing the header with the size in samples of the consumed buffer to destination file
      #ifdef POSIX_FILE_API
        if (write(file_descriptor, & num_samples_payload, sizeof(uint16_t)) 
          != sizeof(uint16_t)) {
          std::cout << "Error writting header to file. " << std::endl;
          return ~0;
        }
        #else
        if (fwrite(& num_samples_payload, sizeof(uint16_t), 1, file_descriptor) != 1) {
          std::cout << "Error writting header to file. " << std::endl;
          return ~0;
        }
      #endif

      // Writing the consumed buffer (every channel) in serial mode on the destination file
      for (size_t i = 0; i < num_channels; i++) {

      #ifdef POSIX_FILE_API
        num_bytes_written = write(file_descriptor, buffer[i], num_bytes_to_write);
      #else
        num_bytes_written = fwrite(buffer[i], sizeof(char), num_bytes_to_write, file_descriptor) * sizeof(char);
      #endif
        total_num_bytes_written += (unsigned long) num_bytes_written;

      }
      buffer_list->consume(); 
    }

    // Since stop signal called, writes the remaining buffers in buffer_list to destination file
    std::cout << "GLOBAL_WRITTER: Writing all the remaining buffers in memory to the file . . . " << std::endl;
    
    // Empty buffer_list
    while (not buffer_list->is_empty()) {

      // Request a remaining position in the multichannel_buffer to consume the data
      buffer_list->reference_buffer_for_consumer( &buffer, number_of_useful_samples_per_channel);
      
      // Accumulate the total number of samples written (same  number for both channels)
      total_num_samps_per_channel_written += (unsigned long) number_of_useful_samples_per_channel;
      
      // Calculate the number of bites to write = size of a complex * number of samples by channel
      num_bytes_to_write = sizeof(std::complex<samp_type>) * number_of_useful_samples_per_channel;

      num_samples_payload = (uint16_t) number_of_useful_samples_per_channel;

      std::cout << boost::format("GLOBAL_WRITTER: total number of samples written per channel = %d") % total_num_samps_per_channel_written << std::endl;

      // Writing the header with the size in samples of the consumed buffer to destination file
      #ifdef POSIX_FILE_API     
        if (write(file_descriptor, & num_samples_payload, sizeof(uint16_t)) 
          != sizeof(uint16_t)) {
          std::cout << "Error writting header to file. " << std::endl;
          return ~0;
        }
      #else
        if (fwrite(& num_samples_payload, sizeof(uint16_t), 1, file_descriptor) != 1) {
          std::cout << "Error writting header to file. " << std::endl;
          return ~0;
          }
      #endif

      // Writing the consumed buffer (every channel) in serial mode on the destination file
      for (size_t i = 0; i < num_channels; i++) {
        #ifdef POSIX_FILE_API
          num_bytes_written = write(file_descriptor, buffer[i], num_bytes_to_write);
        #else
          num_bytes_written = fwrite(buffer[i], sizeof(char), num_bytes_to_write, file_descriptor) * sizeof(char);
        #endif
          total_num_bytes_written += (unsigned long) num_bytes_written;
      }
      buffer_list->consume(); 
    }

    std::cout << boost::format("GLOBAL_WRITTER: Writting to disk finished. Written %d sample(s) per channel.")
      % (total_num_bytes_written / (num_channels * sizeof(std::complex<samp_type>))) << std::endl << std::endl;
  
    // Close files with the acquired samples after successful receipt
    #ifdef POSIX_FILE_API
        close(file_descriptor);
    #else
        fclose(file_descriptor);
    #endif

    // TIMESTAMP TEXT FILE (only standard C API is supported, no efficiency problems here)
    // open the file where the text timestamp is stored
    std::string timestamp_filename = output_file_name + timestamp_file_extension;
    FILE * timestamp_file_descriptor = fopen(timestamp_filename.c_str(), "w");
    if (timestamp_file_descriptor ==  NULL) {
        std::cout << boost::format("Error opening the timestamp file with name \"%s\"")
          % timestamp_filename << std::endl;
        return ~0;
    }

    // Writting the timestamp value as text in time_t and in human-readable format
    time_t utc_timestamp_first = buffer_list->get_utc_timestamp_first();
    time_t utc_timestamp_last = buffer_list->get_utc_timestamp_last();
    std::string utc_timestamp_first_human_readable = boost::posix_time::to_simple_string(
                boost::posix_time::from_time_t(utc_timestamp_first));
    std::string utc_timestamp_last_human_readable = boost::posix_time::to_simple_string(
                boost::posix_time::from_time_t(utc_timestamp_last));
    std::cout << boost::format("GLOBAL_WRITTER: Writing the UTC timestamp for\n");
    std::cout << boost::format("\t the first chunk of acquired samples: %s\n")
                 % utc_timestamp_first_human_readable;
    std::cout << boost::format("\t and the last chunk of acquired samples: %s\n")
                 % utc_timestamp_last_human_readable;
    std::cout << boost::format("\t in file name: \"%s\" . . .")
              % timestamp_filename << std::endl;

    fprintf(timestamp_file_descriptor, "# PERS UTC timestamp (human readable and Epoch) for\n");
    fprintf(timestamp_file_descriptor, "#\t the first chunk of acquired samples: %s\n",
            utc_timestamp_first_human_readable.c_str());
    fprintf(timestamp_file_descriptor, "#\t and the last chunk of acquired samples: %s\n",
            utc_timestamp_last_human_readable.c_str());
    fprintf(timestamp_file_descriptor, "#\t in the file name: \"%s\"\n", output_file_name.c_str());
    fprintf(timestamp_file_descriptor, "#\n");
    fprintf(timestamp_file_descriptor, "utc_time_stamp_first_human_readable=%s\n",
            utc_timestamp_first_human_readable.c_str());
    fprintf(timestamp_file_descriptor, "utc_time_stamp_first=%ld\n",
            utc_timestamp_first);
    fprintf(timestamp_file_descriptor, "utc_time_stamp_last_human_readable=%s\n",
            utc_timestamp_last_human_readable.c_str());
    fprintf(timestamp_file_descriptor, "utc_time_stamp_last=%ld\n",
            utc_timestamp_last);
    fprintf(timestamp_file_descriptor, "\n");
    fclose(timestamp_file_descriptor);

    return EXIT_SUCCESS;

  } catch (...) {

    // Close files after failed receipt    
    #ifdef POSIX_FILE_API
        close(file_descriptor);
    #else
        fclose(file_descriptor);
    #endif
        return EXIT_SUCCESS;
  }
}

/***********************************************************************
* recv_to_usrp_multichannel_buffer thread: read buffers from usrp. (Primary)
*   DEPRECATED FUNCTION
* Parameters:
* usrp: pointer to usrp object
* channel_nums: the numbers of the channels to use
* number_of_samples_per_channel: samples to acquire per channel
* num_requested_samples_per_channel: total number of samples to receive. If 0, receive continuously
* buffer_list: ad-hoc multichannel buffer dedicated to receive
* wait_to_receive_command: parameter used to wait to receive / receive continuously
* network_commands: network commands desactivated / activated, used to perform operations in remote mode
* stop_signal_called_local: signal called to stop the reception
*
* Modifications:
* Dec 7, 2014: added support for retrieving the timestamp of the first
* sample adquired.
**********************************************************************/

void recv_to_usrp_multichannel_buffer(
        uhd::usrp::multi_usrp::sptr usrp,
        const std::vector < size_t > &channel_nums,
        size_t number_of_samples_per_channel,
        unsigned long num_requested_samples_per_channel,
        usrp_multichannel_buffer * buffer_list,
        bool wait_for_key_pressed,
        volatile bool * stop_signal_called,
        bool network_commands)
__attribute__ ((deprecated("This function is deprecated. \nUse recv_from_rx_streamer_to_usrp_multichannel_buffer() instead.")));

/***********************************************************************
* recv_from_rx_streamer_to_usrp_multichannel_buffer thread: read buffers from usrp. (Primary)
* Parameters:
* usrp: pointer to usrp object
* rx_stream: pointer to a initialized rx_stream of the usrp
* num_requested_samples_per_channel: total number of samples to receive. If 0, receive continuously
* buffer_list: ad-hoc multichannel buffer dedicated to receive
* wait_to_receive_command: parameter used to wait to receive / receive continuously
* network_commands: network commands desactivated / activated, used to perform operations in remote mode
* stop_signal_called_local: signal called to stop the reception
*
* Modifications:
* Dec 7, 2014: added support for retrieving the timestamp of the first
* sample adquired.
**********************************************************************/

void recv_from_rx_streamer_to_usrp_multichannel_buffer(
        uhd::usrp::multi_usrp::sptr usrp,
        uhd::rx_streamer::sptr rx_stream,
        unsigned long num_requested_samples_per_channel,
        usrp_multichannel_buffer * buffer_list,
        uint64_t rx_time,
        bool wait_for_key_pressed,
        volatile bool * stop_signal_called,
        bool network_commands);

/***********************************************************************
 * file name splitter: separate a string with a list of files in a vector of strings with files ready to be used
 * Parameters:
 * input_file_list: string with the list of files
 * input_file_names: vector with the separated files
 * num_channels: total number of channels to be used
 **********************************************************************/

int file_name_splitter (
  const std::string input_file_list,
    std::vector < std::string > & input_file_names,
    size_t num_channels);

/***********************************************************************
 * split_multichannel_output_file: separate the serialized received data into
 * various files according to the channel specifications
 * Parameters:
 * input_file_name: string with the path to source file
 * output_file_names: vector of strings with destination channel files
 * stop_signal_called: when set to true it stops the splitting process.
 **********************************************************************/

int split_multichannel_output_file(
        std::string input_file_name,
        std::vector < std::string > output_file_names,
        volatile bool * stop_signal_called);

/***********************************************************************
 * read_from_file_to_buffers: read to cache buffers the source file to transmit
 * Parameters:
 * input_file_names: string with the path to source file
 * channel_nums: vector of strings with destination channel files
 * file_buffers: buffer to store the data that will be transmitted
 **********************************************************************/

int read_from_file_to_buffers(
  const std::vector < std::string > input_file_names,
  const std::vector < size_t > & channel_nums,
  std::vector < std::vector < std::complex < short > > > & file_buffers);

/***********************************************************************
 * send_from_buffers_to_usrp: send the buffered data through the USRP .
 *   DEPRECATED FUNCTION.
 * Parameters:
 * usrp: the pointer to a initializated usrp 
 * channel_nums: vector of strings with destination channel files
 * file_buffers: buffer to store the data that will be transmitted
 * stop_signal_called: flag to activate handler to stop transmission 
 **********************************************************************/

void send_from_buffers_to_usrp (
  uhd::usrp::multi_usrp::sptr usrp,
  const std::vector< size_t > & channel_nums,
  std::vector < std::vector < std::complex < short > > >  file_buffers,
  volatile bool * stop_signal_called)
  __attribute__ ((deprecated("This function is deprecated. \nUse send_from_buffers_to_tx_streamer() instead.")));

/***********************************************************************
 * send_from_buffers_to_tx_streamer: send the buffered data through the USRP
 *   reusing the tx_streamer object, which allows for keeping phase coherence
 *   between transmissions.
 *
 *   This is the recommended function when streaming signals over the network.
 *
 * Parameters:
 * usrp: pointer to a initializated usrp
 * tx_stream: pointer to a initialized tx_stream of the usrp
 * signal_buffers: buffers where the data to be transmitted is stored
 * stop_signal_called: flag to activate handler to stop transmission
 **********************************************************************/

void send_from_buffers_to_tx_streamer (
  uhd::usrp::multi_usrp::sptr usrp,
  uhd::tx_streamer::sptr tx_stream,
  std::vector < std::vector < std::complex < short > > >  signal_buffers,
  uint64_t tx_time,
  volatile bool * stop_signal_called);

#endif
