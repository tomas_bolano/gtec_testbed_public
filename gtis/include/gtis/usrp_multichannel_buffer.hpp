#ifndef INCLUDED_USRP_MULTICHANNEL_BUFFER_HPP
#define INCLUDED_USRP_MULTICHANNEL_BUFFER_HPP

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/thread.hpp>
#include <boost/progress.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/bind.hpp>
#include <ctime>

/* Modified on Dec 7, 2014 to add timestamp (time_t) support for the first buffer.
 */
class usrp_multichannel_buffer {
  
public:
  
  typedef std::vector < std::vector < std::vector < std::complex < short > > > > container_type;
  typedef typename container_type::size_type size_type;
  typedef typename container_type::value_type value_type;


  explicit usrp_multichannel_buffer(
    size_type c_number_of_buffers,
    size_type c_number_of_channels,
    size_type c_number_of_samples_per_channel) : 
      m_size(0),
      m_size_max(0),
      m_producer_index(0),
      m_consumer_index(0),
      m_capacity(c_number_of_buffers),
      m_number_of_channels(c_number_of_channels),
      m_number_of_samples_per_channel(c_number_of_samples_per_channel),
      m_container(m_capacity, 
        std::vector < std::vector < std::complex < short > > > (
          m_number_of_channels,
          std::vector < std::complex <short > > (
            m_number_of_samples_per_channel,
            std::complex<short>(0,0)))),
      m_container_number_valid_samples(new size_type[c_number_of_buffers]),
      m_utc_timestamp_first(0),
      m_utc_timestamp_last(0)
      {
        for (size_type ii = 0; ii < m_capacity; ++ii) {
          m_container_number_valid_samples[ii] = m_number_of_samples_per_channel;
        }
      }
  
  ~usrp_multichannel_buffer() {
    delete m_container_number_valid_samples;
  }
  size_type capacity_in_bytes() {
    boost::mutex::scoped_lock lock(m_mutex);
    return m_container.capacity() * m_container[0].capacity()   
      * m_container[0][0].capacity() * sizeof(std::complex<short>);
  }

  void reference_buffer_for_producer(std::vector < std::complex < short > * > * reference) {
    
    boost::mutex::scoped_lock lock(m_mutex);

    if(not usrp_multichannel_buffer::is_not_full()){
      std::cout << boost::format("usrp_buffer FULL.") << std::endl << std::endl;
    }
    m_not_full.wait(lock, boost::bind(& usrp_multichannel_buffer::is_not_full, this));

    try{
      // for (size_type ii = 0; ii < m_container[m_producer_index].size(); ++ii) {
      for (size_type ii = 0; ii < m_number_of_channels; ++ii) {
        // reference->at(ii) =  & (m_container[m_producer_index].at(ii).front());
        reference->at(ii) =  & (m_container[m_producer_index][ii][0]);
      }
    }
    catch (...){
      
    }

    lock.unlock();
  }


  void produce(size_type number_of_samples_per_channel_produced) {

    boost::mutex::scoped_lock lock(m_mutex);

    // if (number_of_samples_per_channel_produced < m_number_of_samples_per_channel) {
      // std::cout << boost::format("usrp_buffer: produce -> resizing to %d samples") % number_of_samples_per_channel_produced  << std::endl;
      // std::cout << boost::format("usrp_buffer.size() = %d") % m_size << std::endl << std::endl;
      // for (size_type ii = 0; ii < m_container[m_producer_index].size(); ++ii) {
      //   m_container[m_producer_index].at(ii).resize(number_of_samples_per_channel_produced);
      // }
      m_container_number_valid_samples[m_producer_index] = number_of_samples_per_channel_produced;
    // }
    
    // std::cout << boost::format("number_of_samples_per_channel_produced = %d") % number_of_samples_per_channel_produced 
    // << std::endl;
    ++m_size;
    // if (m_size > m_size_max) {
    //   m_size_max = m_size;
    // }

    ++m_producer_index;
    if (m_producer_index == m_capacity) {
      m_producer_index = 0;
    }
    
    lock.unlock();
    m_not_empty.notify_one();
  }


  void reference_buffer_for_consumer(
      std::vector < std::complex < short > * > * reference,
      size_type & number_of_useful_samples_per_channel) {
    
    boost::mutex::scoped_lock lock(m_mutex);

    m_not_empty.wait(lock, boost::bind(& usrp_multichannel_buffer::is_not_empty, this));
    
    // for (size_type ii = 0; ii < m_container[m_consumer_index].size(); ++ii) {
    for (size_type ii = 0; ii < m_number_of_channels; ++ii) {
      // reference->at(ii) = & (m_container[m_consumer_index].at(ii).front());
      reference->at(ii) = & (m_container[m_consumer_index][ii][0]);
    }

    // number_of_useful_samples_per_channel = m_container[m_consumer_index].at(0).size();
    number_of_useful_samples_per_channel = m_container_number_valid_samples[m_consumer_index];

    lock.unlock();
  }


  void consume() {

    boost::mutex::scoped_lock lock(m_mutex);

    // if (m_container[m_consumer_index].at(0).size() < m_number_of_samples_per_channel) {
    //   for (size_type ii = 0; ii < m_container[m_consumer_index].size(); ++ii) {
    //     m_container[m_consumer_index].at(ii).resize(m_number_of_samples_per_channel);
    //   }
    // }

    --m_size;
    ++m_consumer_index;
    if (m_consumer_index == m_capacity) {
      m_consumer_index = 0;
    }

    lock.unlock();
    m_not_full.notify_one();
  }


  size_type size() {
    boost::mutex::scoped_lock lock(m_mutex);
    return m_size;
  }

  
  size_type number_of_channels() {
    boost::mutex::scoped_lock lock(m_mutex);
    return m_number_of_channels;
  }


  size_type number_of_samples_per_channel() {
    boost::mutex::scoped_lock lock(m_mutex);
    return m_number_of_samples_per_channel;
  }

  // Added on Dec 7, 2014.
  time_t get_utc_timestamp_first() {
    boost::mutex::scoped_lock lock(m_mutex);
    return m_utc_timestamp_first;
  }

  // Added on Dec 7, 2014.
  void set_utc_timestamp_last(time_t utc_timestamp_last) {
    boost::mutex::scoped_lock lock(m_mutex);
    m_utc_timestamp_last = utc_timestamp_last;
  }

  // Added on Dec 7, 2014.
  time_t get_utc_timestamp_last() {
    boost::mutex::scoped_lock lock(m_mutex);
    return m_utc_timestamp_last;
  }

  // Added on Dec 7, 2014.
  void set_utc_timestamp_first(time_t utc_timestamp_first) {
    boost::mutex::scoped_lock lock(m_mutex);
    m_utc_timestamp_first = utc_timestamp_first;
  }

  bool is_empty() {
    boost::mutex::scoped_lock lock(m_mutex);
    return m_size == 0;
  }

  bool is_empty_ns() {   // Not thread safe!!
    return m_size == 0;
  }


  size_type size_max() {   // Not thread safe!!
    return m_size_max;
  }

  
  size_type capacity() {
    return m_capacity;
  }
 
private:
    
  bool is_not_empty() const { return m_size > 0; }
  bool is_not_full() const { return m_size < m_capacity; }
  
  size_type m_size;
  size_type m_size_max;

  size_type m_producer_index;
  size_type m_consumer_index; 

  size_type m_capacity;
  size_type m_number_of_channels;
  size_type m_number_of_samples_per_channel;
  container_type m_container;
  size_type * m_container_number_valid_samples;

  //Added on Dec 7, 2014.
  time_t m_utc_timestamp_first;
  time_t m_utc_timestamp_last;
  boost::mutex m_mutex;
  boost::condition m_not_empty;
  boost::condition m_not_full;
};

#endif
