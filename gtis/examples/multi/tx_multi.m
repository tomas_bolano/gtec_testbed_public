% 
% GTIS TX Multi USRP Example: OFDM TX for USRP B210
% 
% This example demonstrates the utilization of two time-synced USRP B210
% devices to transmit signals simultaneously.
%
% A third receive node will be added in near future.
%
% This example requires the so-called GTEC 5G simulatior which is usually
% bundled with GTIS in the same repository.
%
clear;
close all;
clc;
rng(737373);
rng('default');

fprintf('\t ==================================================\n');
fprintf('\t\t GTIS TX Multi USRP OFDM Example for USRP B210\n');
fprintf('\t ==================================================\n');

%% TX node A configuration (transmitter A)
txUsrpModelA = 'B210';
txNodeNameA = 'TX A';
txUserNameA = 'testbed';
% txIP = '127.0.0.1';
txIPA = '10.68.33.45';
txPortA = 5002;

%% TX node B configuration (transmitter B)
txUsrpModelB = 'B210';
txNodeNameB = 'TX B';
txUserNameB = 'testbed';
txIPB = '127.0.0.1';
txPortB = 5002;

%% RX node configuration (receiver)
rxUsrpModel = 'B210';
rxNodeName = 'RX';
rxUserName = 'testbed';
rxIP = '10.68.33.215';
rxPort = 5002;

%% GTIS Initialization
restoredefaultpath;
gtisMatlabPath = ...
    fullfile(fileparts(fileparts(fileparts(mfilename('fullpath')))), 'matlab');
addpath(gtisMatlabPath);
FBMC_path = genpath(fullfile(fileparts(fileparts(fileparts(pwd))), ... 
    '5G', 'common'));
addpath(FBMC_path);
GTIS_parameters = GTIS_init('printGtisVersion', true);

%**************************************************************************

%% Plotting results configuration
plot_frame         = 0;
plot_time_txSignal = 0;
plot_freq_txSignal = 0;
plot_time_rxSignal = 0;
plot_freq_rxSignal = 0;
plot_est_channel   = 1;
plot_est_channel_first_symbol = 0;

% configuration for the pwelch functions
window = 1024;
noverlap = 512;
nfft = 1024;

%% Parameter initialization
FBMC_parameters = FBMC_systemExampleOFDMParametersDefinition;

% Convolutional encoder with rates '1/2', '2/3', '3/4', and '5/6'
dataBlockModOrders = [4 4 4 4 16 16 16 16 64 64 64 64 256 256 256 256];
dataBlockCodeRates = {'1/2' '2/3' '3/4' '5/6',...
                      '1/2' '2/3' '3/4' '5/6',...
                      '1/2' '2/3' '3/4' '5/6',...
                      '1/2' '2/3' '3/4' '5/6'};

% Error correction
for k = 1:numel(FBMC_parameters.dataBlock)-1
    % Note that the option uncodedBitsNumber if less that one is taken
    % as the coding rate.
    
    % WIFI like convolutional code
    scrSeed = randi([0,1], 1, 7);
    FBMC_parameters.dataBlock(k).modulationOrder = dataBlockModOrders(k);
    FBMC_parameters.dataBlock(k).uncodedBitsNumber = str2num(dataBlockCodeRates{k})*0.99;
    FBMC_parameters.dataBlock(k).FECCoderFunction = ...
        {@FBMC_convFECCoder, dataBlockCodeRates{k}, scrSeed, 'rnd', {0}};
    FBMC_parameters.dataBlock(k).FECDecoderFunction = ...
        {@FBMC_convFECDecoder, dataBlockCodeRates{k}, scrSeed, 'rnd', {0}};
    % LTE turbocode using the MATLAB LTE toolbox
    %FBMC_parameters.dataBlock(k).FECCoderFunction = {@FBMC_MATLABLTEFECCoder};
    %FBMC_parameters.dataBlock(k).FECDecoderFunction = {@FBMC_MATLABLTEFECDecoder};
end

% Pilots
FBMC_parameters.pilots.modulationEnergyFactor = 2;
%FBMC_parameters.pilots.pilotsGenerationFunction =  {@FBMC_nullPilotIndexer};
%FBMC_parameters.pilots.numberAuxiliaryPilots = 0;

% Channel estimation and equalization
FBMC_parameters.channelEstimation.channelEstimationFunction = {@FBMC_interpChannelEstimator};
%FBMC_parameters.channelEstimation.channelEstimationFunction = {@FBMC_MMSEChannelEstimator};

%FBMC_parameters.channelEqualization.channelEqualizationFunction = {@FBMC_MMSEChannelEqualizator};
FBMC_parameters.channelEqualization.channelEqualizationFunction = {@FBMC_ZFChannelEqualizator};
%FBMC_parameters.channelEqualization.channelEqualizationFunction = {@FBMC_nullChannelEqualizator};

% Synchronization
% Currently only for OFDM a robust synchronization algorithm is
% implemented, for FBMC the implemented algorithms are not so good
% The following options assume OFDM, change for FBMC.
FBMC_parameters.synchronization.preambleGenerationFunction = {@FBMC_nullPreambleGenerator};
FBMC_parameters.synchronization.synchronizationFunction = {@FBMC_ofdmBlindSynchronizator};

% Add time and freq displacement to the generated transmit signal
% Note that the algorithm FBMC_ofdmBlindSynchronizator currently is
% configured to detect up to 5 or 6 subcarriers of frequency offset, this
% limit can be increased, just look into the function code.
% Just to test the synchronization
% FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions = ...
%     {{@(c,p,s) FBMC_addZerosSignalProcessing(c, p, s, 10000, 100000)}, ...
%      {@(c,p,s) FBMC_FreqDisplaceSignalProcessing(c, p, s, 2.5/1024)}};
% FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions = ...
%     {{@FBMC_nullSignalProcessing}};

% High speed emulation
FBMC_parameters.highSpeedEmulation.interpolationFactor = 1;
FBMC_parameters.highSpeedEmulation.displacementFreq = 3e6;

%**************************************************************************

% LTE sampling rate = 15.36 MHz
signalSamplingRate = 1/FBMC_parameters.signalGeneration.dt; 
rfCarrier = 400e6;

%% TXA node configuration
txUSRPA = GTIS_nodeDefaultParameters(...
    txUsrpModelA, txNodeNameA, txUserNameA, txIPA, txPortA, GTIS_parameters);

% Only for debugging. If true the console window of the node is not closed
% after node execution finishes
txUSRPA.doWaitBeforeExiting = true;

% Number of TX and RX antennas from 1 to 2.
txUSRPA.Ntx = 1;
txUSRPA.Nrx = 1;
                   
% USRP TX parameters
txUSRPA.referenceClockSource = 'external'; % Possible options: 'internal', 'external', and 'gpsdo'
txUSRPA.referenceTimeSource = 'external'; % Possible options: 'internal', 'external', and 'gpsdo'
txUSRPA.numberOfTrialsLOLocked = 1;
txUSRPA.numberOfTrialsGPSLocked = 0;
txUSRPA.numberOfTrialsREFLocked = 5;
txUSRPA.txRFCarrier = rfCarrier;
txUSRPA.signalSamplingRate = signalSamplingRate;
% USRP TX parameters depending on the USRP model
if (contains(txUSRPA.usrpModel, 'B2'))
    txUSRPA.masterClockRate = 2*signalSamplingRate;
elseif (contains(txUSRPA.usrpModel, 'X2'))
    warning(' This USRP model is NOT supported by this example!');
    txUSRPA.masterClockRate = 200e6; % USRP X200 requires 200 MHz
end

txUSRPA.txGain = 20; % 20; % Optimal value for external MiniCircuits TVA-11-422 power amplifier is 60
txUSRPA.txBandwidth = signalSamplingRate;

% Signal processing parameters
txUSRPA.nZeroSamples = 1024/4; % silence between frames
txUSRPA.clippingFactor = 0;

%**************************************************************************

%% TXB node configuration
txUSRPB = GTIS_nodeDefaultParameters(...
    txUsrpModelB, txNodeNameB, txUserNameB, txIPB, txPortB, GTIS_parameters);

% Only for debugging. If true the console window of the node is not closed
% after node execution finishes
txUSRPB.doWaitBeforeExiting = true;

% Number of TX and RX antennas from 1 to 2.
txUSRPB.Ntx = 1;
txUSRPB.Nrx = 1;
                   
% USRP TX parameters
txUSRPB.referenceClockSource = 'external'; % Possible options: 'internal', 'external', and 'gpsdo'
txUSRPB.referenceTimeSource = 'external'; % Possible options: 'internal', 'external', and 'gpsdo'
txUSRPB.numberOfTrialsLOLocked = 1;
txUSRPB.numberOfTrialsGPSLocked = 0;
txUSRPB.numberOfTrialsREFLocked = 5;
txUSRPB.txRFCarrier = rfCarrier;
txUSRPB.signalSamplingRate = signalSamplingRate;
% USRP TX parameters depending on the USRP model
if (contains(txUSRPB.usrpModel, 'B2'))
    txUSRPB.masterClockRate = 2*signalSamplingRate;
elseif (contains(txUSRPB.usrpModel, 'X2'))
    warning(' This USRP model is NOT supported by this example!');
    txUSRPB.masterClockRate = 200e6; % USRP X200 requires 200 MHz
end

txUSRPB.txGain = 20; % 20; % Optimal value for external MiniCircuits TVA-11-422 power amplifier is 60
txUSRPB.txBandwidth = signalSamplingRate;

% Signal processing parameters
txUSRPB.nZeroSamples = 1024/4; % silence between frames
txUSRPB.clippingFactor = 0;

% TBD: RX node
%% RX node configuration
rxUSRP = GTIS_nodeDefaultParameters(...
    rxUsrpModel, rxNodeName, rxUserName, rxIP, rxPort, GTIS_parameters);

% Only for debugging
rxUSRP.doWaitBeforeExiting = true;

% Number of TX and RX antennas from 1 to 2
rxUSRP.Ntx = 2;
rxUSRP.Nrx = 2;

% USRP RX parameters
rxUSRP.doAcquireWithBothAntennas = 0; % With Nrx = 1 it acquires using both RX antennas
rxUSRP.doGenerateNMEALog = 0;
rxUSRP.secondsBetweenNMEAMeasurements = 2;
rxUSRP.numberOfTrialsGPSLocked = 0;
rxUSRP.numberOfTrialsLOLocked = 1;
rxUSRP.numberOfTrialsREFLocked = 5;
rxUSRP.usrpWaitForKeyboard = false; % true => requires key pressed before acquisition starts

rxUSRP.referenceClockSource = 'external'; % Possible options: 'internal', 'external', and 'gpsdo'
rxUSRP.referenceTimeSource = 'external';  % Possible options: 'internal', 'external', and 'gpsdo'
rxUSRP.rxRFCarrier = rfCarrier;
rxUSRP.signalSamplingRate = signalSamplingRate;
% USRP RX parameters depending on the USRP model
if (contains(rxUSRP.usrpModel, 'B2'))
    % A decimation by a factor of 2 is recommended at the B2X0 RX
    rxUSRP.masterClockRate = 2*signalSamplingRate;  
elseif (contains(rxUSRP.usrpModel, 'X2'))
    rxUSRP.masterClockRate = 200e6;
end

rxUSRP.rxGain = 30; % 10;
rxUSRP.rxBandwidth = signalSamplingRate;
rxUSRP.rxAntennaMapping = 'RX2'; % Valid options: 'RX2' and 'TX/RX'
rxUSRP.nZeroSamples = txUSRPA.nZeroSamples;

%*********************************************************************

%% Create GTIS nodes
fprintf('RX: Starting remote GTIS node . . . ');
[success, rxUnixFullCommand] = GTIS_createRemoteRxNode(GTIS_parameters, rxUSRP);
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    error('Cannot start node.');
end

fprintf('TXA: Starting remote GTIS node . . . ');
[success, txAunixFullCommand] = GTIS_createRemoteTxNode(GTIS_parameters, txUSRPA);
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    error('Cannot start node.');
end

fprintf('TXB: Starting remote GTIS node . . . ');
[success, txBunixFullCommand] = GTIS_createRemoteTxNode(GTIS_parameters, txUSRPB);
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    error('Cannot start node.');
end


%**********************************************************************
%% Generation of TX signals
FBMC_constants = FBMC_pregenerateConstants(FBMC_parameters);
fprintf('Generation of the frames transmitted . . . ');
FBMC_txData = FBMC_transmitter(FBMC_parameters, FBMC_constants);
txSignal = FBMC_txData.interpolatedSignal.';
fprintf('OK\n');

% print simulation and signal information    
FBMC_printSimulationInformation(FBMC_parameters, FBMC_constants);
FBMC_printSignalInformation(FBMC_parameters, FBMC_constants, FBMC_txData);

fprintf('RX: Waiting for the RX USRP to be ready . . . ')
if (~GTIS_waitForNodeReady(rxUSRP))
    fprintf('FAIL\n');
    error(' RX node is not ready.');
end
fprintf('OK\n');
[~, rxVersionString] = GTIS_getGTISNodeVersion(rxUSRP);
fprintf('RX: GTIS node version: %s\n', rxVersionString);

fprintf('TXA: Waiting for the TXA USRP to be ready . . . ')
if (~GTIS_waitForNodeReady(txUSRPA))
    fprintf('FAIL\n');
    error(' TXA node is not ready.');
end
fprintf('OK\n');
[~, txAVersionString] = GTIS_getGTISNodeVersion(txUSRPA);
fprintf('TXA: GTIS node version: %s\n', txAVersionString);

fprintf('TXB: Waiting for the TXB USRP to be ready . . . ')
if (~GTIS_waitForNodeReady(txUSRPB))
    fprintf('FAIL\n');
    error(' TXB node is not ready.');
end
fprintf('OK\n');
[~, txBVersionString] = GTIS_getGTISNodeVersion(txUSRPB);
fprintf('TXB: GTIS node version: %s\n', txBVersionString);

%% Stream TX signals to the TX nodes
fprintf('TXA: Streaming transmit signals to the USRP . . . ')
[success, nTotalTXASamples, txASignalUpsNormClipped ] = ...
    GTIS_streamTxSignalsToRemoteNode(...
    txUSRPA, [zeros(1,0), txSignal], signalSamplingRate);
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    return;
end

fprintf('TXB: Streaming transmit signals to the USRP . . . ')
[success, nTotalTXBSamples, txBSignalUpsNormClipped ] = ...
    GTIS_streamTxSignalsToRemoteNode(...
    txUSRPB, txSignal, signalSamplingRate);
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    return;
end

%% Timed configuration
% This is the code to synchronize the two USRPs in time. It is assumed that
% the same 10 MHz and 1 PPS references are provided to both USRPs through,
% e.g., an OctoClock device.
% Note that the calls to GTIS_timedConfigTX are asynchronous, i.e., the
% function call returns inmediately after receiving an OK response from the
% node. Therefore, once all timed configurations are requested, the program
% must wait until all of them have completed the configuration. We use
% GTIS_waitForNodeReady() for this purpose.
%
% In the future we will provide a function that does all of this
% transparently for the user only providing the handlers of the USRPs to be
% synchronized.
%
[~, usrpTime] = GTIS_getNodeTimeLastPPS(txUSRPA, true);
waitingTimeInSeconds = 3;
usrpTime = usrpTime + waitingTimeInSeconds;
usrpTimeInHumanFormat = ...
    datetime(usrpTime, 'convertfrom', 'posixtime', 'timezone', 'Europe/Madrid');
fprintf('TXA: Starting configuration at %lu ticks (%s) . . . ', ...
    usrpTime, usrpTimeInHumanFormat);
success = GTIS_timedConfigTx(txUSRPA, usrpTime);
if (success == 1)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
end

fprintf('TXB: Starting configuration at %lu ticks (%s) . . . ', ...
    usrpTime, usrpTimeInHumanFormat);
success = GTIS_timedConfigTx(txUSRPB, usrpTime);
if (success == 1)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
end

fprintf('RX: Starting configuration at %lu ticks (%s) . . . ', ...
    usrpTime, usrpTimeInHumanFormat);
success = GTIS_timedConfigRx(rxUSRP, usrpTime);
if (success == 1)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
end


fprintf('TXA: Waiting for the TXA USRP to be ready . . . ')
if (~GTIS_waitForNodeReady(txUSRPA))
    fprintf('FAIL\n');
    error(' TXA node is not ready.');
end
fprintf('OK\n');

fprintf('TXB: Waiting for the TXB USRP to be ready . . . ')
if (~GTIS_waitForNodeReady(txUSRPB))
    fprintf('FAIL\n');
    error(' TXB node is not ready.');
end
fprintf('OK\n');

fprintf('RX: Waiting for the RX USRP to be ready . . . ')
if (~GTIS_waitForNodeReady(rxUSRP))
    fprintf('FAIL\n');
    error(' RX node is not ready.');
end
fprintf('OK\n');

% % % % WARNING! IF YOU CHANGE THE SETTINGS AFTER ISSUING A TIME CONFIG VIA
% % % % GTIS_timedConfigTx() YOU CAN DESTROY THE SYNCHRONIZATION BETWEEN
% % % % URPS. This will be fixed very soon.
% % % % Not really necessary since settings have not changed.
% % % fprintf('TXA: Writting TX settings to the USRPA . . . ')
% % % success = GTIS_writeTxSettingsToNode(txUSRPA);
% % % if (success)
% % %     fprintf('OK\n');
% % % else
% % %     fprintf('FAIL\n');
% % %     return;
% % % end
% % % 
% % % fprintf('TXB: Writting TX settings to the USRPB . . . ')
% % % success = GTIS_writeTxSettingsToNode(txUSRPB);
% % % if (success)
% % %     fprintf('OK\n');
% % % else
% % %     fprintf('FAIL\n');
% % %     return;
% % % end



%% Transmit with USRPA and USRPB aligned
for ii=1:1
    % Start transmission at a given time instant in seconds
    % Gets the current node time in POSIX time (seconds elapsed since
    % January 1st, 1970 at Coordinated Universal Time (UTC)).
    [~, usrpTime] = GTIS_getNodeTimeLastPPS(txUSRPB, true);

    % A delay of 3 seconds is stablished to ensure that all nodes
    % get the time instant on time. 
    % Transmission will take place at usrpTime (which caan be displayed in
    % human-readable format using datetime()
    waitingTimeInSeconds = 2;
    usrpTime = usrpTime + waitingTimeInSeconds;
    usrpTimeInHumanFormat = ...
        datetime(usrpTime, 'convertfrom', 'posixtime', 'timezone', 'Europe/Madrid');
    fprintf('TXA: Starting transmission at %lu ticks (%s) . . . ', ...
        usrpTime, usrpTimeInHumanFormat);
    success = GTIS_startCyclicTransmission(txUSRPA, usrpTime);
    if (success == 1)
        fprintf('OK\n');
    else
        fprintf('FAIL\n');
    end

    fprintf('TXB: Starting transmission at %lu ticks (%s) . . . ', ...
        usrpTime, usrpTimeInHumanFormat);
    success = GTIS_startCyclicTransmission(txUSRPB, usrpTime);
    if (success == 1)
        fprintf('OK\n');
    else
        fprintf('FAIL\n');
    end
    
    numRequestedSamplesToAcquirePerChannel = ...
        2*(length(txSignal) + rxUSRP.nZeroSamples);
    
    fprintf('RX: Streaming signals from the USRP at %lu ticks (%s) . . . ', ...
        usrpTime, usrpTimeInHumanFormat);
    [success, rxSignalsDecimated] = GTIS_startAcquisitionAndStreaming(...
        rxUSRP, signalSamplingRate, ...
        numRequestedSamplesToAcquirePerChannel, ...
        usrpTime); 
    if (success)
        fprintf('OK\n');
    else
        fprintf('FAIL\n');
        return;
    end
    
    pause(waitingTimeInSeconds + 1);
    GTIS_stopCyclicTransmission(txUSRPA);
    GTIS_stopCyclicTransmission(txUSRPB);
    
    plot(real(rxSignalsDecimated(1,:))); 
    hold on
    plot(real(rxSignalsDecimated(2,:)), 'r')
end
    
return

%%
GTIS_disposeNode(txUSRPA);
GTIS_disposeNode(txUSRPB);
GTIS_disposeNode(rxUSRP);

%%
% % % % Not really necessary since settings have not changed.
% % % fprintf('RX: Writting RX settings to the USRP . . . ')
% % % success = GTIS_writeRxSettingsToNode(rxUSRP);
% % % if (success)
% % %     fprintf('OK\n');
% % % else
% % %     fprintf('FAIL\n');
% % %     return;
% % % end

% % % %% Acquisition loop
% % % for iii = 1:1 % Set the number of acquisitions you want to perform
% % %     
% % %     numRequestedSamplesToAcquirePerChannel = ...
% % %         2*(length(txSignal) + rxUSRP.nZeroSamples);
% % %    
% % %     % Start transmission at a given time instant in seconds
% % %     % Gets the current node time in POSIX time (seconds elapsed since
% % %     % January 1st, 1970 at Coordinated Universal Time (UTC)).
% % %     [~, usrpTime] = GTIS_getGTISNodeTime(txUSRP);
% % %     
% % %     % A delay of 3 seconds is stablished to ensure that all nodes
% % %     % get the time instant on time. 
% % %     % Transmission will take place at usrpTime (which caan be displayed in
% % %     % human-readable format using datetime()
% % %     waitingTimeInSeconds = 3;
% % %     usrpTime = usrpTime + waitingTimeInSeconds;
% % %     usrpTimeInHumanFormat = ...
% % %         datetime(usrpTime, 'convertfrom', 'posixtime', 'timezone', 'Europe/Madrid');
% % %     fprintf('TX: Starting transmission at %lu ticks (%s) . . . ', ...
% % %         usrpTime, usrpTimeInHumanFormat);
% % %     success = GTIS_startCyclicTransmission(txUSRP, usrpTime);
% % %     if (success == 1)
% % %         fprintf('OK\n');
% % %     else
% % %         fprintf('FAIL\n');
% % %     end
% % %           
% % %     % Start acquisition at the same time instant defined by usrpTime
% % %     % Note that if the USRPs at different nodes are aligned with the
% % %     % computer clock, the level of time synchronization will depend on 
% % %     % the time synchronization between clocks. Even though computers are
% % %     % synchronized via NTP, it is not precise enough compared to the
% % %     % sampling time of the USRP, therefore an acquisition marging must be
% % %     % considered just increasing the number of samples to be acquired
% % %     % with respect to the number of transmitted samples.
% % %     if (doUseNetworkSignalStreaming)
% % %         fprintf('RX: Streaming signals from the USRP at %lu ticks (%s) . . . ', ...
% % %             usrpTime, usrpTimeInHumanFormat);
% % %         [success, rxSignalsDecimated] = GTIS_startAcquisitionAndStreaming(...
% % %             rxUSRP, signalSamplingRate, ...
% % %             numRequestedSamplesToAcquirePerChannel, ...
% % %             usrpTime);
% % %         if (success)
% % %             fprintf('OK\n');
% % %         else
% % %             fprintf('FAIL\n');
% % %             return;
% % %         end
% % %     else
% % %         fprintf('RX: Acquiring signals from the USRP . . . ')
% % %         success = GTIS_startAcquisition(...
% % %         rxUSRP, signalSamplingRate, numRequestedSamplesToAcquirePerChannel);
% % %         if (success)
% % %             fprintf('OK\n');
% % %         else
% % %             fprintf('FAIL\n');
% % %             return;
% % %         end
% % %         
% % %         % If signal streaming is not used, then if remote nodes are
% % %         % considered and the path is in a remote machine, files have to be
% % %         % first coppied to the local machine and then loaded into Matlab.
% % %         if (rxUSRP.remote.rxSignals)
% % %             fprintf('RX: Copying acquired signals from remote node . . . ');
% % %             success = GTIS_copyRxSignalsFromRemoteNode(GTIS_parameters, rxUSRP);
% % %             if (success)
% % %                 [success, rxSignalsDecimated] = ...
% % %                     GTIS_readRxSignalsFromLocalNode(rxUSRP, signalSamplingRate);
% % %                 fprintf('OK\n');
% % %             else
% % %                 fprintf('FAIL\n');
% % %                 return;
% % %             end
% % %         end        
% % %     end
% % %     
% % % 
% % %     % In case numRequestedSamplesToAcquirePerChannel == 0, then the acquisition
% % %     % process will not stop automatically. To stop it, use
% % %     if ((~doUseNetworkSignalStreaming) && ...
% % %             (~rxUSRP.usrpWaitForKeyboard) && ...
% % %             (numRequestedSamplesToAcquirePerChannel == 0))
% % %         % Configure the acquisition time
% % %         pause(1);
% % %         fprintf('RX: Stopping acquisition from the USRP . . . ')
% % %         success = GTIS_stopAcquisition(rxUSRP);
% % %         if (success)
% % %             fprintf('OK\n');
% % %         else
% % %             fprintf('FAIL\n');
% % %             return;
% % %         end
% % %     end
% % %     
% % %     % Stop transmission
% % %     GTIS_stopCyclicTransmission(txUSRP);
% % % 
% % %     % Execute the receiver
% % %     FBMC_channelSystemData.signal = rxSignalsDecimated.';
% % %     FBMC_rxData = FBMC_receiver(FBMC_parameters, FBMC_constants,...
% % %         FBMC_channelSystemData.signal,...
% % %         FBMC_txData.dataBlockCodedBitsNumber,...
% % %         []);
% % % 
% % %     if ~FBMC_rxData.detected
% % %         fprintf('Frame not detected');
% % %         continue;
% % %     end
% % % 
% % %     FBMC_results = FBMC_systemIterationResults(FBMC_parameters,...
% % %         FBMC_constants, FBMC_txData, FBMC_rxData);
% % % 
% % %     % print block information
% % %     FBMC_printDataBlocksInformation(FBMC_parameters, FBMC_constants);
% % % 
% % %     % print information about the results
% % %     FBMC_printResultsInformation(FBMC_parameters, FBMC_constants, ...
% % %         FBMC_txData, FBMC_results);
% % %     
% % %     
% % %     FBMC_printSeparatorLine(80);
% % % 
% % %     if plot_frame
% % %        FBMC_showResourceMapping(FBMC_parameters); 
% % %     end
% % % 
% % % 
% % %     if plot_time_txSignal
% % %         figure(10);
% % %         subplot(2,1,1);
% % %         plot(real(FBMC_txData.interpolatedSignal));
% % %         xlabel('time');
% % %         ylabel('Amplitude');
% % %         title('TX signal (real part)');
% % %         grid on;
% % % 
% % %         subplot(2,1,2);
% % %         plot(imag(FBMC_txData.interpolatedSignal));
% % %         xlabel('time');
% % %         ylabel('Amplitude');
% % %         title('TX signal (imaginary part)');
% % %         grid on;
% % %     end
% % % 
% % % 
% % %     if plot_freq_txSignal
% % %         figure(11);
% % %         [pxx,f] = pwelch(FBMC_txData.interpolatedSignal, window, noverlap, nfft,...
% % %                     1/FBMC_parameters.signalGeneration.dt, 'centered');
% % % 
% % %         semilogy(f, abs(pxx)/max(abs(pxx)), '-');
% % %         grid on;
% % %         title('power spectral density estimate of post processed TX signal');
% % %         xlabel('frequency [Hz]');
% % %         ylabel('power/frequency [dB/Hz/sample]');
% % %     end
% % %  
% % %     if plot_time_rxSignal
% % %         figure(12);
% % %         subplot(2,1,1);
% % %         plot(real(FBMC_channelSystemData.signal));
% % %         xlabel('time');
% % %         ylabel('Amplitude');
% % %         title('RX signal (real part)');
% % %         grid on;
% % % 
% % %         subplot(2,1,2);
% % %         plot(imag(FBMC_channelSystemData.signal));
% % %         xlabel('time');
% % %         ylabel('Amplitude');
% % %         title('RX signal (imaginary part)');
% % %         grid on;
% % %     end
% % % 
% % % 
% % %     if plot_freq_rxSignal
% % %         figure(13);
% % %         [pxx,f] = pwelch(FBMC_channelSystemData.signal, window, noverlap, nfft,...
% % %                     1/FBMC_parameters.signalGeneration.dt, 'centered');
% % %         semilogy(f, abs(pxx));
% % %         grid on;
% % %         title('power spectral density estimate of post processed RX signal');
% % %         xlabel('frequency [Hz]');
% % %         ylabel('power/frequency [dB/Hz/sample]');
% % %     end
% % % 
% % % 
% % %     if plot_est_channel && ~isempty(FBMC_rxData.channelEstimation)
% % %         figure(14);
% % %         FBMC_plotEstimatedChannel(FBMC_parameters, FBMC_constants, ...
% % %             FBMC_txData, FBMC_rxData, 'XAxisUnits', 'time',...
% % %             'YAxisUnits', 'freq')
% % %     end
% % % 
% % %     if plot_est_channel_first_symbol
% % %         figure(15);
% % %         scShift = -(FBMC_parameters.basicParameters.subcarriersNumber/2+1);
% % % 
% % %         xind = (1:size(FBMC_rxData.channelEstimation,1));
% % %         estChan1 = FBMC_rxData.channelEstimation(:,1);
% % %         plot(xind+scShift, abs(estChan1));
% % % 
% % %         hold on;
% % %         pilotInd = find((any(FBMC_constants.pilots.ActualPilotMask.')));
% % %         plot(pilotInd+scShift, abs(estChan1(pilotInd)), 'ro');
% % % 
% % %         title('Estimated channel for the first symbol');
% % %         xlabel('Subcarrier number');
% % %         ylabel('Amplitude');
% % %         legend('Estimated channel', 'Superimposed pilot points');
% % % 
% % %         figure();
% % %         plot(xind+scShift, real(estChan1));
% % %         hold on;
% % %         plot(pilotInd+scShift, real(estChan1(pilotInd)), 'ro');
% % % 
% % %         title('Estimated channel for the first symbol (real part)');
% % %         legend('Estimated channel (real part)', 'Superimposed pilot points');
% % %     end
% % % 
% % %     % OFDM constellation
% % %     blocki = 15; % Block number to be plotted
% % %     consellation = FBMC_rxData.decodedDataGrid(FBMC_constants.dataBlockMask == blocki);
% % %     figure(20);
% % %     scatter(real(consellation), imag(consellation), '.');
% % %     title('signal constellation');
% % %     axis square;
% % %     grid on;
% % %     
% % %     drawnow;
% % % end
% % % 
% % % return
% % % 
% % % %% Stop transmission and finalizes both GTIS nodes.
% % % % GTIS_disposeNode(rxUSRP);
% % % GTIS_stopCyclicTransmission(txUSRPA);
% % % GTIS_stopCyclicTransmission(txUSRPB);
% % % 
% % % %%
% % % GTIS_disposeNode(txUSRPA);
% % % GTIS_disposeNode(txUSRPB);
