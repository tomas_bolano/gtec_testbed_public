% 
% GTIS Example: WiMAX PHY RX for USRP B210/X310
% 
% This example requires a valid implementation of WiMAX PHY which may or 
% may not be bundled with GTIS.
%
clear;
close all;
clc;
rng(737373);
rng('default');

fprintf('\t ==================================================\n');
fprintf('\t\t GTIS USRP B210/X310 WiMAX TX&RX Example\n');
fprintf('\t\t Both TX and RX are executed remotely.\n');
fprintf('\t ==================================================\n');

% signal streaming through the network is now supported. Use it instead of
% file-based signal loading and acquisition. File-based signal exchange is
% still fully supported for the cases when network connections are not
% available (e.g. car measurements) or when the amount of data to acquire
% is too large or if the network streaming speed is not enough.
doUseNetworkSignalStreaming = true;

% GTIS nodes can be local (same machine) or remote. The differences between
% them are their IP addresses and that remote nodes are launched through an
% SSH session. Additionally, file-based signal exchange requires to copy
% the signals between local and remote. Specially when network signal
% streaming is used, local nodes do not provide any significant advantage
% and they are now declared as deprecated.
doUseRemoteTXNode = true;
doUseRemoteRXNode = true;

% GTIS Initialization
restoredefaultpath;
gtisMatlabPath = ...
    fullfile(fileparts(fileparts(fileparts(mfilename('fullpath')))), 'matlab');
addpath(gtisMatlabPath);
GTIS_parameters = GTIS_init('printGtisVersion', true);


% WiMAX_PHY default PATH
addpath(fullfile(fileparts(fileparts(gtisMatlabPath)), 'WiMAX_PHY'));

% WiMAX PHY TX & RX configuration
profile = 2;
wimaxSamplingRate = 10e6;
channelEstimationMethod = 'LinealInterp';

%**************************************************************************
% TX node configuration
txUsrpModel = 'B210';
txNodeName = 'TX';
txUserName = 'testbed';
txIP = '127.0.0.1';
txPort = 5002;
txUSRP = GTIS_nodeDefaultParameters(...
    txUsrpModel, txNodeName, txUserName, txIP, txPort, GTIS_parameters);

% Only for debugging. If true the console window of the node is not closed
% after node execution finishes
txUSRP.doWaitBeforeExiting = true;

% Number of TX and RX antennas from 1 to 2.
txUSRP.Ntx = 1;
txUSRP.Nrx = 1;
                   
% USRP TX parameters
txUSRP.referenceClockSource = 'internal'; % Possible options: 'internal', 'external', and 'gpsdo'
txUSRP.referenceTimeSource = 'internal'; % Possible options: 'internal', 'external', and 'gpsdo'
txUSRP.numberOfTrialsLOLocked = 1;
txUSRP.numberOfTrialsGPSLocked = 0;
txUSRP.numberOfTrialsREFLocked = 5;
txUSRP.txRFCarrier = 2.6e9;
txUSRP.signalSamplingRate = wimaxSamplingRate;
% USRP TX parameters depending on the USRP model
if (contains(txUSRP.usrpModel, 'B2'))
    txUSRP.masterClockRate = 2 * wimaxSamplingRate;
elseif (contains(txUSRP.usrpModel, 'X2'))
    txUSRP.masterClockRate = 200e6; % USRP X200 requires 200 MHz
end

txUSRP.txGain = 85; % 20; % Optimal value for external MiniCircuits TVA-11-422 power amplifier is 60
txUSRP.txBandwidth = wimaxSamplingRate;

% Signal processing parameters
txUSRP.nZeroSamples = 1024/4; % silence between frames
txUSRP.clippingFactor = 0;

% Remote node parameters
if (~doUseNetworkSignalStreaming)
    % If true remote TX node is considered
    txUSRP.remote.txSignals = true;
else
    txUSRP.remote.txSignals = false;
    if (txUSRP.Ntx == 1)
        txUSRP.remote.txSignalsInUSRPFormatFilenames = {''};
        txUSRP.txSignalsInUSRPFormatFilenames = {''};
    else
        txUSRP.remote.txSignalsInUSRPFormatFilenames = {'', ''};
        txUSRP.txSignalsInUSRPFormatFilenames = {'', ''};
    end
end
%**************************************************************************
% RX node (local) configuration
rxUsrpModel = 'B210';
rxNodeName = 'RX';
rxUserName = 'testbed';
rxIP = '127.0.0.1';
rxPort = 5002;
rxUSRP = GTIS_nodeDefaultParameters(...
    rxUsrpModel, rxNodeName, rxUserName, rxIP, rxPort, GTIS_parameters);

% Only for debugging
rxUSRP.doWaitBeforeExiting = true;

% Number of TX and RX antennas from 1 to 2
rxUSRP.Ntx = txUSRP.Ntx;
rxUSRP.Nrx = txUSRP.Ntx;

% USRP RX parameters
rxUSRP.doAcquireWithBothAntennas = 0; % With Nrx = 1 it acquires using both RX antennas
rxUSRP.doGenerateNMEALog = 0;
rxUSRP.secondsBetweenNMEAMeasurements = 2;
rxUSRP.numberOfTrialsGPSLocked = 0;
rxUSRP.numberOfTrialsLOLocked = 1;
rxUSRP.numberOfTrialsREFLocked = 5;
rxUSRP.usrpWaitForKeyboard = false; % true => requires key pressed before acquisition starts

rxUSRP.referenceClockSource = 'internal'; % Possible options: 'internal', 'external', and 'gpsdo'
rxUSRP.referenceTimeSource = 'internal';  % Possible options: 'internal', 'external', and 'gpsdo'
rxUSRP.rxRFCarrier = txUSRP.txRFCarrier;
rxUSRP.signalSamplingRate = wimaxSamplingRate;
% USRP RX parameters depending on the USRP model
if (contains(rxUSRP.usrpModel, 'B2'))
    % A decimation by a factor of 2 is recommended at the B2X0 RX
    rxUSRP.masterClockRate = 2 * wimaxSamplingRate;  
elseif (contains(rxUSRP.usrpModel, 'X2'))
    rxUSRP.masterClockRate = 200e6;
end

rxUSRP.rxGain = 35; % 10;
rxUSRP.rxBandwidth = wimaxSamplingRate;
rxUSRP.rxAntennaMapping = 'RX2'; % Valid options: 'RX2' and 'TX/RX'
rxUSRP.nZeroSamples = txUSRP.nZeroSamples;

if (~doUseNetworkSignalStreaming)
    rxUSRP.remote.rxSignals = true;
else
    rxUSRP.remote.rxSignals = false;
    rxUSRP.remote.rxSignalsInMultiplexedUSRPFormatFilename = '';
    rxUSRP.rxSignalsInMultiplexedUSRPFormatFilename = '';
end

%*********************************************************************
if (doUseRemoteRXNode)
    fprintf('RX: Starting remote GTIS node . . . ');
    [success, rxUnixFullCommand] = ...
        GTIS_createRemoteRxNode(GTIS_parameters, rxUSRP);
else
    fprintf('RX: Starting local GTIS node . . . ');
    [success, rxUnixFullCommand] = ...
        GTIS_createLocalRxNode(GTIS_parameters, rxUSRP);
end
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    error('Cannot start local node.');
end

%**********************************************************************
if (doUseRemoteTXNode)
    fprintf('TX: Starting remote GTIS node . . . ');
    [success, unixFullCommand] = GTIS_createRemoteTxNode(GTIS_parameters, txUSRP);
else
    fprintf('TX: Starting local GTIS node . . . ');
    [success, unixFullCommand] = GTIS_createLocalTxNode(GTIS_parameters, txUSRP);
end
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    error('Cannot start remote node.');
end

% Generation of WiMAX TX signals
fprintf('RX: Generation of the frames transmitted . . . ');
config = wimax_pusc_config(profile, txUSRP.Ntx, txUSRP.Nrx);
config.Seed = 1000;
bursts = burst_example(config);
[txSignal, bursts_tx, simb_freq] = PHY_tx(config, bursts);
fprintf('OK\n');

fprintf('RX: Waiting for the RX USRP to be ready . . . ')
if (~GTIS_waitForNodeReady(rxUSRP))
    fprintf('FAIL\n');
    error(' RX node is not ready.');
end
fprintf('OK\n');
[~, rxVersionString] = GTIS_getGTISNodeVersion(rxUSRP);
fprintf('RX: GTIS node version: %s\n', rxVersionString);

fprintf('TX: Waiting for the TX USRP to be ready . . . ')
if (~GTIS_waitForNodeReady(txUSRP))
    fprintf('FAIL\n');
    error(' TX node is not ready.');
end
fprintf('OK\n');
[~, txVersionString] = GTIS_getGTISNodeVersion(txUSRP);
fprintf('TX: GTIS node version: %s\n', txVersionString);


% Copy (through files or via streaming) TX signals to the TX node
if (doUseNetworkSignalStreaming)
    fprintf('TX: Streaming transmit signals to the USRP . . . ')
    [success, nTotalTXSamples, txSignalUpsNormClipped ] = ...
        GTIS_streamTxSignalsToRemoteNode(...
        txUSRP, txSignal, wimaxSamplingRate);
else
    fprintf('TX: Writting transmit signals to the USRP . . . ')
    [success, nTotalTXSamples, txSignalUpsNormClipped ] = ...
        GTIS_writeTxSignalsToRemoteNode(...
        GTIS_parameters, txUSRP, txSignal, wimaxSamplingRate);
end
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    return;
end

% Not really necessary since settings have not changed.
fprintf('TX: Writting TX settings to the USRP . . . ')
success = GTIS_writeTxSettingsToNode(txUSRP);
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    return;
end

fprintf('TX: Starting transmission . . . ');
success = GTIS_startCyclicTransmission(txUSRP);
if (success == 1)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
end

% Not really necessary since settings have not changed.
fprintf('RX: Writting RX settings to the USRP . . . ')
success = GTIS_writeRxSettingsToNode(rxUSRP);
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    return;
end

%%
for iii = 1:1 % Set the number of acquisitions you want to perform
    
    numRequestedSamplesToAcquirePerChannel = ...
        2*(length(txSignal) + rxUSRP.nZeroSamples);
    
    if (doUseNetworkSignalStreaming)
        fprintf('RX: Streaming signals from the USRP . . . ')
        [success, rxSignalsDecimated] = GTIS_startAcquisitionAndStreaming(...
            rxUSRP, wimaxSamplingRate, numRequestedSamplesToAcquirePerChannel);
        if (success)
            fprintf('OK\n');
        else
            fprintf('FAIL\n');
            return;
        end
    else
        fprintf('RX: Acquiring signals from the USRP . . . ')
        success = GTIS_startAcquisition(...
        rxUSRP, wimaxSamplingRate, numRequestedSamplesToAcquirePerChannel);
        if (success)
            fprintf('OK\n');
        else
            fprintf('FAIL\n');
            return;
        end
        
        % If signal streaming is not used, then if remote nodes are
        % considered and the path is in a remote machine, files have to be
        % first coppied to the local machine and then loaded into Matlab.
        if (rxUSRP.remote.rxSignals)
            fprintf('RX: Copying acquired signals from remote node . . . ');
            success = GTIS_copyRxSignalsFromRemoteNode(GTIS_parameters, rxUSRP);
            if (success)
                [success, rxSignalsDecimated] = ...
                    GTIS_readRxSignalsFromLocalNode(rxUSRP, wimaxSamplingRate);
                fprintf('OK\n');
            else
                fprintf('FAIL\n');
                return;
            end
        end        
    end
    

    % In case numRequestedSamplesToAcquirePerChannel == 0, then the acquisition
    % process will not stop automatically. To stop it, use
    if ((~doUseNetworkSignalStreaming) && ...
            (~rxUSRP.usrpWaitForKeyboard) && ...
            (numRequestedSamplesToAcquirePerChannel == 0))
        % Configure the acquisition time
        pause(1);
        fprintf('RX: Stopping acquisition from the USRP . . . ')
        success = GTIS_stopAcquisition(rxUSRP);
        if (success)
            fprintf('OK\n');
        else
            fprintf('FAIL\n');
            return;
        end
    end

    fprintf('RX: processing frames (PHY_rx) . . . ');
    load('bases_4MHz_ajakes.mat','Bdps');
    Q = 3;
    Bd = squeeze(Bdps(12, 1:Q, :));

    % El ultimo parametro de PHY_rx es una estructura con información para el
    % estimador de canal.
    channelEstimationParameters = {};
    channelEstimationParameters.Algorithm = channelEstimationMethod;
    channelEstimationParameters.IciCancellation = 0;
    channelEstimationParameters.BEM = Bd;
    channelEstimationParameters.SignalTx = simb_freq;

    % Process the received signal
    [bursts_rx, CSI, errorSync] = PHY_rx(...
        config, bursts_tx, rxSignalsDecimated, channelEstimationParameters);

    if (~errorSync)
        % Plot bursts
        for n = 1:length(bursts_rx)
            sprintf('bursts_rx(%d)',n)
            bursts_rx(n)
        end

        % Signal in time domain
        figure(10);
        plot(real(rxSignalsDecimated(1, CSI.TimeOffset:CSI.TimeOffset+(config.NumberOfSymbols+1)*config.Nfft*(1+config.GuardInterval)-1)));
        drawnow;
        
        % Signal in frequency domain
        figure(20);
        subplot(2, 2, 1);
        GTIS_spectrum(...
            rxSignalsDecimated(1, CSI.TimeOffset:CSI.TimeOffset+(config.NumberOfSymbols+1)*config.Nfft*(1+config.GuardInterval)-1), ...
            rxUSRP.signalSamplingRate);
        drawnow;

        % A constelation per constellation size is plotted
        figure(20);
        subplot(2, 2, 2);
        plot(bursts_rx(1).symbols, 'k.');
        axis([-1 1 -1 1]);
        axis square
        drawnow;
        
        figure(20);
        subplot(2, 2, 3);
        plot(bursts_rx(3).symbols, 'k.');
        axis([-1 1 -1 1]);
        axis square
        drawnow;
        
        figure(20);
        subplot(2, 2, 4);
        plot(bursts_rx(5).symbols, 'k.');
        axis([-1 1 -1 1]);
        axis square
        drawnow;
    else
        fprintf('\n FRAME SYNCHRONIZATION ERROR \n');
    end
end

return

%% Stop transmission and finalizes both GTIS nodes.
GTIS_disposeNode(rxUSRP);
GTIS_stopCyclicTransmission(txUSRP);
GTIS_disposeNode(txUSRP);
