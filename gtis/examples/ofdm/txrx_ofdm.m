% 
% GTIS OFDM Example: OFDM TX/RX for USRP B210/X310
% 
% This example requires the so-called GTEC 5G simulatior which is usually
% bundled with GTIS in the same repository.
%
clear;
close all;
clc;
rng(737373);
rng('default');

fprintf('\t ==================================================\n');
fprintf('\t\t GTIS TX/RX OFDM Example for USRP B210/X310\n');
fprintf('\t\t Both TX and RX are executed remotely.\n');
fprintf('\t ==================================================\n');

% signal streaming through the network is now supported. Use it instead of
% file-based signal loading and acquisition. File-based signal exchange is
% still fully supported for the cases when network connections are not
% available (e.g. car measurements) or when the amount of data to acquire
% is too large or if the network streaming speed is not enough.
doUseNetworkSignalStreaming = true;

% GTIS nodes can be local (same machine) or remote. The differences between
% them are their IP addresses and that remote nodes are launched through an
% SSH session. Additionally, file-based signal exchange requires to copy
% the signals between local and remote. Specially when network signal
% streaming is used, local nodes do not provide any significant advantage
% and they are now declared as deprecated.
doUseRemoteTXNode = true;
doUseRemoteRXNode = true;

%% TX node configuration
txUsrpModel = 'B210';
txNodeName = 'TX';
txUserName = 'testbed';
% txIP = '127.0.0.1';
txIP = '10.68.33.45';
txPort = 5002;

%% RX node configuration
rxUsrpModel = 'B210';
rxNodeName = 'RX';
rxUserName = 'testbed';
rxIP = '127.0.0.1';
rxPort = 5002;

%% GTIS Initialization
restoredefaultpath;
gtisMatlabPath = ...
    fullfile(fileparts(fileparts(fileparts(mfilename('fullpath')))), 'matlab');
addpath(gtisMatlabPath);
FBMC_path = genpath(fullfile(fileparts(fileparts(fileparts(pwd))), ... 
    '5G', 'common'));
addpath(FBMC_path);
GTIS_parameters = GTIS_init('printGtisVersion', true);

%**************************************************************************

%% Plotting results configuration
plot_frame         = 0;
plot_time_txSignal = 0;
plot_freq_txSignal = 0;
plot_time_rxSignal = 0;
plot_freq_rxSignal = 0;
plot_est_channel   = 1;
plot_est_channel_first_symbol = 0;

% configuration for the pwelch functions
window = 1024;
noverlap = 512;
nfft = 1024;

%% Parameter initialization
FBMC_parameters = FBMC_systemExampleOFDMParametersDefinition;

% Convolutional encoder with rates '1/2', '2/3', '3/4', and '5/6'
dataBlockModOrders = [4 4 4 4 16 16 16 16 64 64 64 64 256 256 256 256];
dataBlockCodeRates = {'1/2' '2/3' '3/4' '5/6',...
                      '1/2' '2/3' '3/4' '5/6',...
                      '1/2' '2/3' '3/4' '5/6',...
                      '1/2' '2/3' '3/4' '5/6'};

% Error correction
for k = 1:numel(FBMC_parameters.dataBlock)-1
    % Note that the option uncodedBitsNumber if less that one is taken
    % as the coding rate.
    
    % WIFI like convolutional code
    scrSeed = randi([0,1], 1, 7);
    FBMC_parameters.dataBlock(k).modulationOrder = dataBlockModOrders(k);
    FBMC_parameters.dataBlock(k).uncodedBitsNumber = str2num(dataBlockCodeRates{k})*0.99;
    FBMC_parameters.dataBlock(k).FECCoderFunction = ...
        {@FBMC_convFECCoder, dataBlockCodeRates{k}, scrSeed, 'rnd', {0}};
    FBMC_parameters.dataBlock(k).FECDecoderFunction = ...
        {@FBMC_convFECDecoder, dataBlockCodeRates{k}, scrSeed, 'rnd', {0}};
    % LTE turbocode using the MATLAB LTE toolbox
    %FBMC_parameters.dataBlock(k).FECCoderFunction = {@FBMC_MATLABLTEFECCoder};
    %FBMC_parameters.dataBlock(k).FECDecoderFunction = {@FBMC_MATLABLTEFECDecoder};
end

% Pilots
FBMC_parameters.pilots.modulationEnergyFactor = 2;
%FBMC_parameters.pilots.pilotsGenerationFunction =  {@FBMC_nullPilotIndexer};
%FBMC_parameters.pilots.numberAuxiliaryPilots = 0;

% Channel estimation and equalization
FBMC_parameters.channelEstimation.channelEstimationFunction = {@FBMC_interpChannelEstimator};
%FBMC_parameters.channelEstimation.channelEstimationFunction = {@FBMC_MMSEChannelEstimator};

%FBMC_parameters.channelEqualization.channelEqualizationFunction = {@FBMC_MMSEChannelEqualizator};
FBMC_parameters.channelEqualization.channelEqualizationFunction = {@FBMC_ZFChannelEqualizator};
%FBMC_parameters.channelEqualization.channelEqualizationFunction = {@FBMC_nullChannelEqualizator};

% Synchronization
% Currently only for OFDM a robust synchronization algorithm is
% implemented, for FBMC the implemented algorithms are not so good
% The following options assume OFDM, change for FBMC.
FBMC_parameters.synchronization.preambleGenerationFunction = {@FBMC_nullPreambleGenerator};
FBMC_parameters.synchronization.synchronizationFunction = {@FBMC_ofdmBlindSynchronizator};

% Add time and freq displacement to the generated transmit signal
% Note that the algorithm FBMC_ofdmBlindSynchronizator currently is
% configured to detect up to 5 or 6 subcarriers of frequency offset, this
% limit can be increased, just look into the function code.
% Just to test the synchronization
% FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions = ...
%     {{@(c,p,s) FBMC_addZerosSignalProcessing(c, p, s, 10000, 100000)}, ...
%      {@(c,p,s) FBMC_FreqDisplaceSignalProcessing(c, p, s, 2.5/1024)}};
% FBMC_parameters.txSignalPostProcessing.txPostProcessingFunctions = ...
%     {{@FBMC_nullSignalProcessing}};

% High speed emulation
FBMC_parameters.highSpeedEmulation.interpolationFactor = 1;
FBMC_parameters.highSpeedEmulation.displacementFreq = 3e6;

%**************************************************************************

% LTE sampling rate = 15.36 MHz
signalSamplingRate = 1/FBMC_parameters.signalGeneration.dt; 

%% TX node configuration
txUSRP = GTIS_nodeDefaultParameters(...
    txUsrpModel, txNodeName, txUserName, txIP, txPort, GTIS_parameters);

% Only for debugging. If true the console window of the node is not closed
% after node execution finishes
txUSRP.doWaitBeforeExiting = true;

% Number of TX and RX antennas from 1 to 2.
txUSRP.Ntx = 1;
txUSRP.Nrx = 1;
                   
% USRP TX parameters
txUSRP.referenceClockSource = 'internal'; % Possible options: 'internal', 'external', and 'gpsdo'
txUSRP.referenceTimeSource = 'internal'; % Possible options: 'internal', 'external', and 'gpsdo'
txUSRP.numberOfTrialsLOLocked = 1;
txUSRP.numberOfTrialsGPSLocked = 0;
txUSRP.numberOfTrialsREFLocked = 5;
txUSRP.txRFCarrier = 2.6e9;
txUSRP.signalSamplingRate = signalSamplingRate;
% USRP TX parameters depending on the USRP model
if (contains(txUSRP.usrpModel, 'B2'))
    txUSRP.masterClockRate = 2*signalSamplingRate;
elseif (contains(txUSRP.usrpModel, 'X2'))
    txUSRP.masterClockRate = 200e6; % USRP X200 requires 200 MHz
end

txUSRP.txGain = 75; % 20; % Optimal value for external MiniCircuits TVA-11-422 power amplifier is 60
txUSRP.txBandwidth = signalSamplingRate;

% Signal processing parameters
txUSRP.nZeroSamples = 1024/4; % silence between frames
txUSRP.clippingFactor = 0;

% Remote node parameters
if (~doUseNetworkSignalStreaming)
    % If true remote TX node is considered
    txUSRP.remote.txSignals = true;
    
    % Define the path of the file name(s) according to the remote node or
    % use the default ones. Do not use env variables since the shell will
    % not expand the variables
    % if (txUSRP.Ntx == 1)
    %     txUSRP.remote.txSignalsInUSRPFormatFilenames = {''};
    % else
    %     txUSRP.remote.txSignalsInUSRPFormatFilenames = {'', ''};
    % end
else
    txUSRP.remote.txSignals = false;
    if (txUSRP.Ntx == 1)
        txUSRP.remote.txSignalsInUSRPFormatFilenames = {''};
        txUSRP.txSignalsInUSRPFormatFilenames = {''};
    else
        txUSRP.remote.txSignalsInUSRPFormatFilenames = {'', ''};
        txUSRP.txSignalsInUSRPFormatFilenames = {'', ''};
    end
end
%**************************************************************************
%% RX node configuration
rxUSRP = GTIS_nodeDefaultParameters(...
    rxUsrpModel, rxNodeName, rxUserName, rxIP, rxPort, GTIS_parameters);

% Only for debugging
rxUSRP.doWaitBeforeExiting = true;

% Number of TX and RX antennas from 1 to 2
rxUSRP.Ntx = txUSRP.Ntx;
rxUSRP.Nrx = txUSRP.Ntx;

% USRP RX parameters
rxUSRP.doAcquireWithBothAntennas = 0; % With Nrx = 1 it acquires using both RX antennas
rxUSRP.doGenerateNMEALog = 0;
rxUSRP.secondsBetweenNMEAMeasurements = 2;
rxUSRP.numberOfTrialsGPSLocked = 0;
rxUSRP.numberOfTrialsLOLocked = 1;
rxUSRP.numberOfTrialsREFLocked = 5;
rxUSRP.usrpWaitForKeyboard = false; % true => requires key pressed before acquisition starts

rxUSRP.referenceClockSource = 'internal'; % Possible options: 'internal', 'external', and 'gpsdo'
rxUSRP.referenceTimeSource = 'internal';  % Possible options: 'internal', 'external', and 'gpsdo'
rxUSRP.rxRFCarrier = txUSRP.txRFCarrier;
rxUSRP.signalSamplingRate = signalSamplingRate;
% USRP RX parameters depending on the USRP model
if (contains(rxUSRP.usrpModel, 'B2'))
    % A decimation by a factor of 2 is recommended at the B2X0 RX
    rxUSRP.masterClockRate = 2*signalSamplingRate;  
elseif (contains(rxUSRP.usrpModel, 'X2'))
    rxUSRP.masterClockRate = 200e6;
end

rxUSRP.rxGain = 40; % 10;
rxUSRP.rxBandwidth = signalSamplingRate;
rxUSRP.rxAntennaMapping = 'RX2'; % Valid options: 'RX2' and 'TX/RX'
rxUSRP.nZeroSamples = txUSRP.nZeroSamples;

if (~doUseNetworkSignalStreaming)
    rxUSRP.remote.rxSignals = true;
    % Define the path of the file name(s) according to the remote node or
    % use the default ones. Do not use env variables since the shell will
    % not expand the variables
    % rxUSRP.remote.rxSignalsInMultiplexedUSRPFormatFilename = '';
else
    rxUSRP.remote.rxSignals = false;
    rxUSRP.remote.rxSignalsInMultiplexedUSRPFormatFilename = '';
    rxUSRP.rxSignalsInMultiplexedUSRPFormatFilename = '';
end

%*********************************************************************

%% Create GTIS nodes
if (doUseRemoteRXNode)
    fprintf('RX: Starting remote GTIS node . . . ');
    [success, rxUnixFullCommand] = ...
        GTIS_createRemoteRxNode(GTIS_parameters, rxUSRP);
else
    fprintf('RX: Starting local GTIS node . . . ');
    [success, rxUnixFullCommand] = ...
        GTIS_createLocalRxNode(GTIS_parameters, rxUSRP);
end
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    error('Cannot start node.');
end

%**********************************************************************

if (doUseRemoteTXNode)
    fprintf('TX: Starting remote GTIS node . . . ');
    [success, unixFullCommand] = GTIS_createRemoteTxNode(GTIS_parameters, txUSRP);
else
    fprintf('TX: Starting local GTIS node . . . ');
    [success, unixFullCommand] = GTIS_createLocalTxNode(GTIS_parameters, txUSRP);
end
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    error('Cannot start node.');
end


%**********************************************************************
%% Generation of TX signals
FBMC_constants = FBMC_pregenerateConstants(FBMC_parameters);
fprintf('RX: Generation of the frames transmitted . . . ');
FBMC_txData = FBMC_transmitter(FBMC_parameters, FBMC_constants);
txSignal = FBMC_txData.interpolatedSignal.';
fprintf('OK\n');

% print simulation and signal information    
FBMC_printSimulationInformation(FBMC_parameters, FBMC_constants);
FBMC_printSignalInformation(FBMC_parameters, FBMC_constants, FBMC_txData);

fprintf('RX: Waiting for the RX USRP to be ready . . . ')
if (~GTIS_waitForNodeReady(rxUSRP))
    fprintf('FAIL\n');
    error(' RX node is not ready.');
end
fprintf('OK\n');
[~, rxVersionString] = GTIS_getGTISNodeVersion(rxUSRP);
fprintf('RX: GTIS node version: %s\n', rxVersionString);

fprintf('TX: Waiting for the TX USRP to be ready . . . ')
if (~GTIS_waitForNodeReady(txUSRP))
    fprintf('FAIL\n');
    error(' TX node is not ready.');
end
fprintf('OK\n');
[~, txVersionString] = GTIS_getGTISNodeVersion(txUSRP);
fprintf('TX: GTIS node version: %s\n', txVersionString);

%% Copy (through files or via streaming) TX signals to the TX node
if (doUseNetworkSignalStreaming)
    fprintf('TX: Streaming transmit signals to the USRP . . . ')
    [success, nTotalTXSamples, txSignalUpsNormClipped ] = ...
        GTIS_streamTxSignalsToRemoteNode(...
        txUSRP, txSignal, signalSamplingRate);
else
    fprintf('TX: Writting transmit signals to the USRP . . . ')
    [success, nTotalTXSamples, txSignalUpsNormClipped ] = ...
        GTIS_writeTxSignalsToRemoteNode(...
        GTIS_parameters, txUSRP, txSignal, signalSamplingRate);
end
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    return;
end

% Not really necessary since settings have not changed.
fprintf('TX: Writting TX settings to the USRP . . . ')
success = GTIS_writeTxSettingsToNode(txUSRP);
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    return;
end

% Not really necessary since settings have not changed.
fprintf('RX: Writting RX settings to the USRP . . . ')
success = GTIS_writeRxSettingsToNode(rxUSRP);
if (success)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
    return;
end

fprintf('TX: Starting transmission . . . ');
success = GTIS_startCyclicTransmission(txUSRP);
if (success == 1)
    fprintf('OK\n');
else
    fprintf('FAIL\n');
end

%% Acquisition loop
for iii = 1:10 % Set the number of acquisitions you want to perform
    
    numRequestedSamplesToAcquirePerChannel = ...
        2*(length(txSignal) + rxUSRP.nZeroSamples);
   
% % %     % Start transmission at a given time instant in seconds
% % %     % Gets the current node time in POSIX time (seconds elapsed since
% % %     % January 1st, 1970 at Coordinated Universal Time (UTC)).
% % %     [~, usrpTime] = GTIS_getNodeTimeLastPPS(txUSRP);
% % %     
% % %     % A delay of 3 seconds is stablished to ensure that all nodes
% % %     % get the time instant on time. 
% % %     % Transmission will take place at usrpTime (which caan be displayed in
% % %     % human-readable format using datetime()
% % %     waitingTimeInSeconds = 3;
% % %     usrpTime = usrpTime + waitingTimeInSeconds;
% % %     usrpTimeInHumanFormat = ...
% % %         datetime(usrpTime, 'convertfrom', 'posixtime', 'timezone', 'Europe/Madrid');
% % %     fprintf('TX: Starting transmission at %lu ticks (%s) . . . ', ...
% % %         usrpTime, usrpTimeInHumanFormat);
% % % % % %     success = GTIS_startCyclicTransmission(txUSRP, usrpTime);
% % %     success = GTIS_startCyclicTransmission(txUSRP);
% % %     if (success == 1)
% % %         fprintf('OK\n');
% % %     else
% % %         fprintf('FAIL\n');
% % %     end
          
    % Start acquisition at the same time instant defined by usrpTime
    % Note that if the USRPs at different nodes are aligned with the
    % computer clock, the level of time synchronization will depend on 
    % the time synchronization between clocks. Even though computers are
    % synchronized via NTP, it is not precise enough compared to the
    % sampling time of the USRP, therefore an acquisition marging must be
    % considered just increasing the number of samples to be acquired
    % with respect to the number of transmitted samples.
    if (doUseNetworkSignalStreaming)
% % %         fprintf('RX: Streaming signals from the USRP at %lu ticks (%s) . . . ', ...
% % %             usrpTime, usrpTimeInHumanFormat);
% % %         [success, rxSignalsDecimated] = GTIS_startAcquisitionAndStreaming(...
% % %             rxUSRP, signalSamplingRate, ...
% % %             numRequestedSamplesToAcquirePerChannel, ...
% % %             usrpTime);
        fprintf('RX: Streaming signals from the USRP . . . ');
        [success, rxSignalsDecimated] = GTIS_startAcquisitionAndStreaming(...
            rxUSRP, signalSamplingRate, ...
            numRequestedSamplesToAcquirePerChannel);
        if (success)
            fprintf('OK\n');
        else
            fprintf('FAIL\n');
            return;
        end
    else
        fprintf('RX: Acquiring signals from the USRP . . . ')
        success = GTIS_startAcquisition(...
        rxUSRP, signalSamplingRate, numRequestedSamplesToAcquirePerChannel);
        if (success)
            fprintf('OK\n');
        else
            fprintf('FAIL\n');
            return;
        end
        
        % If signal streaming is not used, then if remote nodes are
        % considered and the path is in a remote machine, files have to be
        % first coppied to the local machine and then loaded into Matlab.
        if (rxUSRP.remote.rxSignals)
            fprintf('RX: Copying acquired signals from remote node . . . ');
            success = GTIS_copyRxSignalsFromRemoteNode(GTIS_parameters, rxUSRP);
            if (success)
                [success, rxSignalsDecimated] = ...
                    GTIS_readRxSignalsFromLocalNode(rxUSRP, signalSamplingRate);
                fprintf('OK\n');
            else
                fprintf('FAIL\n');
                return;
            end
        end        
    end
    

    % In case numRequestedSamplesToAcquirePerChannel == 0, then the acquisition
    % process will not stop automatically. To stop it, use
    if ((~doUseNetworkSignalStreaming) && ...
            (~rxUSRP.usrpWaitForKeyboard) && ...
            (numRequestedSamplesToAcquirePerChannel == 0))
        % Configure the acquisition time
        pause(1);
        fprintf('RX: Stopping acquisition from the USRP . . . ')
        success = GTIS_stopAcquisition(rxUSRP);
        if (success)
            fprintf('OK\n');
        else
            fprintf('FAIL\n');
            return;
        end
    end
    
    % Stop transmission
%     GTIS_stopCyclicTransmission(txUSRP);

    % Execute the receiver
    FBMC_channelSystemData.signal = rxSignalsDecimated.';
    FBMC_rxData = FBMC_receiver(FBMC_parameters, FBMC_constants,...
        FBMC_channelSystemData.signal,...
        FBMC_txData.dataBlockCodedBitsNumber,...
        []);

    if ~FBMC_rxData.detected
        fprintf('Frame not detected');
        continue;
    end

    FBMC_results = FBMC_systemIterationResults(FBMC_parameters,...
        FBMC_constants, FBMC_txData, FBMC_rxData);

    % print block information
    FBMC_printDataBlocksInformation(FBMC_parameters, FBMC_constants);

    % print information about the results
    FBMC_printResultsInformation(FBMC_parameters, FBMC_constants, ...
        FBMC_txData, FBMC_results);
    
    
    FBMC_printSeparatorLine(80);

    if plot_frame
       FBMC_showResourceMapping(FBMC_parameters); 
    end


    if plot_time_txSignal
        figure(10);
        subplot(2,1,1);
        plot(real(FBMC_txData.interpolatedSignal));
        xlabel('time');
        ylabel('Amplitude');
        title('TX signal (real part)');
        grid on;

        subplot(2,1,2);
        plot(imag(FBMC_txData.interpolatedSignal));
        xlabel('time');
        ylabel('Amplitude');
        title('TX signal (imaginary part)');
        grid on;
    end


    if plot_freq_txSignal
        figure(11);
        [pxx,f] = pwelch(FBMC_txData.interpolatedSignal, window, noverlap, nfft,...
                    1/FBMC_parameters.signalGeneration.dt, 'centered');

        semilogy(f, abs(pxx)/max(abs(pxx)), '-');
        grid on;
        title('power spectral density estimate of post processed TX signal');
        xlabel('frequency [Hz]');
        ylabel('power/frequency [dB/Hz/sample]');
    end
 
    if plot_time_rxSignal
        figure(12);
        subplot(2,1,1);
        plot(real(FBMC_channelSystemData.signal));
        xlabel('time');
        ylabel('Amplitude');
        title('RX signal (real part)');
        grid on;

        subplot(2,1,2);
        plot(imag(FBMC_channelSystemData.signal));
        xlabel('time');
        ylabel('Amplitude');
        title('RX signal (imaginary part)');
        grid on;
    end


    if plot_freq_rxSignal
        figure(13);
        [pxx,f] = pwelch(FBMC_channelSystemData.signal, window, noverlap, nfft,...
                    1/FBMC_parameters.signalGeneration.dt, 'centered');
        semilogy(f, abs(pxx));
        grid on;
        title('power spectral density estimate of post processed RX signal');
        xlabel('frequency [Hz]');
        ylabel('power/frequency [dB/Hz/sample]');
    end


    if plot_est_channel && ~isempty(FBMC_rxData.channelEstimation)
        figure(14);
        FBMC_plotEstimatedChannel(FBMC_parameters, FBMC_constants, ...
            FBMC_txData, FBMC_rxData, 'XAxisUnits', 'time',...
            'YAxisUnits', 'freq')
    end

    if plot_est_channel_first_symbol
        figure(15);
        scShift = -(FBMC_parameters.basicParameters.subcarriersNumber/2+1);

        xind = (1:size(FBMC_rxData.channelEstimation,1));
        estChan1 = FBMC_rxData.channelEstimation(:,1);
        plot(xind+scShift, abs(estChan1));

        hold on;
        pilotInd = find((any(FBMC_constants.pilots.ActualPilotMask.')));
        plot(pilotInd+scShift, abs(estChan1(pilotInd)), 'ro');

        title('Estimated channel for the first symbol');
        xlabel('Subcarrier number');
        ylabel('Amplitude');
        legend('Estimated channel', 'Superimposed pilot points');

        figure();
        plot(xind+scShift, real(estChan1));
        hold on;
        plot(pilotInd+scShift, real(estChan1(pilotInd)), 'ro');

        title('Estimated channel for the first symbol (real part)');
        legend('Estimated channel (real part)', 'Superimposed pilot points');
    end

    % OFDM constellation
    blocki = 15; % Block number to be plotted
    consellation = FBMC_rxData.decodedDataGrid(FBMC_constants.dataBlockMask == blocki);
    figure(20);
    scatter(real(consellation), imag(consellation), '.');
    title('signal constellation');
    axis square;
    grid on;
    
    drawnow;
end

return

%% Stop transmission and finalizes both GTIS nodes.
GTIS_disposeNode(rxUSRP);
GTIS_stopCyclicTransmission(txUSRP);
GTIS_disposeNode(txUSRP);
