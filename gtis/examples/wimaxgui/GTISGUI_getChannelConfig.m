function usrpParameters = GTISGUI_getChannelConfig(channel_menu, usrpParameters)

    contents = get(channel_menu,'String');
    channelConfig = contents{get(channel_menu,'Value')};
    if (strcmp(channelConfig,'1x1') == 1)
        usrpParameters.Ntx = 1;
        usrpParameters.Nrx = 1;
        fprintf('TX: SISO transmission mode.\n');
    else
        usrpParameters.Ntx = 2;
        usrpParameters.Nrx = 2;
        fprintf('TX: 2x2 MIMO transmission mode.\n');
    end
end
