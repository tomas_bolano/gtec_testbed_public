%***********************************************************
% GTIS TX WIMAX GUI
% DATE: OCT-2017
%
% AUTHORS: JOSE ANTONIO GARCÍA NAYA, ISMAEL ROZAS RAMALLAL and
% The GTEC Team!
%
% This software is part of GTIS.
%***********************************************************



%***********************************************************
function varargout = txwimaxgui(varargin)

% Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @txwimaxgui_OpeningFcn, ...
                       'gui_OutputFcn',  @txwimaxgui_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
% End initialization code - DO NOT EDIT
%***********************************************************

%***********************************************************
% --- Executes just before the GUI is made visible.
function txwimaxgui_OpeningFcn(hObject, eventdata, handles, varargin)

    rng(737373);
    rng('default');
    clc;
    
    fprintf('\n======================================================\n');
    fprintf('\t GTIS USRP B210 WiMAX GUI TX Example\n');
    fprintf('======================================================\n');

    % Global variables
    global GTIS_parameters;
    global txUSRP;
    global transmissionStarted;
    global gtisNodeStarted;
    global txSignal;
    global bursts_tx;
    global simb_freq;
    global wimaxSamplingRate;
    
    % GTIS Initialization
    restoredefaultpath;
    gtisMatlabPath = ...
        fullfile(fileparts(fileparts(fileparts(mfilename('fullpath')))), 'matlab');
    addpath(gtisMatlabPath);
    GTIS_parameters = GTIS_init('printGtisVersion', true);
 
    
    % WiMAX_PHY PATH
    addpath(fullfile(fileparts(fileparts(gtisMatlabPath)), 'WiMAX_PHY'));
    
    % Choose default command line output for txwimaxgui
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);
    
    % Read parameters from configuration file
    fid = fopen('parameters_tx.txt');

    if (fid == (-1))
        msgbox('Cannot read configuration parameters.', 'GTIS', 'error');
        return;
    end
    
    tline = fgetl(fid);
    while ischar(tline)
%         disp(tline);
        [paramId, paramValue] = strtok(tline, '=');
        paramId = strtrim(paramId);
        if strcmpi(paramId, 'txIP')
            paramValue = strtrim(strtok(paramValue,('=')));
            txIP = paramValue;
        elseif strcmpi(paramId, 'txPort')
            paramValue = strtrim(strtok(paramValue,('=')));
            txPort = str2double(paramValue);
        elseif strcmpi(paramId, 'txFreq')
            paramValue = strtrim(strtok(paramValue,('=')));
            txFreq = str2double(paramValue);
        elseif strcmpi(paramId, 'masterClockRate')
            paramValue = strtrim(strtok(paramValue,('=')));
            masterClockRate = str2double(paramValue);
        elseif strcmpi(paramId, 'signalSamplingRate')
            paramValue = strtrim(strtok(paramValue,('=')));
            signalSamplingRate = str2double(paramValue);
        elseif strcmpi(paramId, 'txGain')
            paramValue = strtrim(strtok(paramValue,('=')));
            txGain = str2double(paramValue); 
        elseif strcmpi(paramId, 'txBandwidth')
            paramValue = strtrim(strtok(paramValue,('=')));
            txBandwidth = str2double(paramValue); 
        else
            error('Wrong input parameter file');
        end
        tline = fgetl(fid);
    end

    fclose(fid);
    

    txUsrpModel = 'B210';
    txNodeName = 'TX';
    txUserName = 'testbed';

    txUSRP = GTIS_nodeDefaultParameters(...
        txUsrpModel, txNodeName, txUserName, txIP, txPort, GTIS_parameters);
    txUSRP.doWaitBeforeExiting = true;
    transmissionStarted = false;
    gtisNodeStarted = false;
    
    % Reads the transmission scheme for generating the WiMAX signals
    % accordingly
    txUSRP = GTISGUI_getChannelConfig(handles.channel_menu, txUSRP);

    % USRP TX parameters
    txUSRP.txRFCarrier = txFreq;
    txUSRP.signalSamplingRate = signalSamplingRate;
    txUSRP.masterClockRate = masterClockRate;
    txUSRP.txGain = txGain;
    txUSRP.txBandwidth = txBandwidth;
    
    % Signal processing parameters
    txUSRP.nZeroSamples = 1024/4;
    txUSRP.clippingFactor = 0;

    % set up logo
    imshow('gtec.png','Parent', handles.logo_axes);
    
    %preselect default values in popup menus
    set(handles.txGainEdit,'String', num2str(txUSRP.txGain));
    set(handles.freqEdit, 'String', num2str(txUSRP.txRFCarrier/1e6));
    
    % Generate WiMAX signals to be transmitted
    [txSignal, bursts_tx, simb_freq, wimaxSamplingRate] = ...
        GTISGUI_generateWimaxTxSignals(txUSRP.Ntx, txUSRP.Nrx);
    
    %initialize the usrp
    initialize_usrp(hObject, eventdata, handles);
%***********************************************************

%***********************************************************
% --- Outputs from this function are returned to the command line.
function varargout = txwimaxgui_OutputFcn(hObject, eventdata, handles) 

    % Get default command line output from handles structure
    varargout{1} = handles.output;
%***********************************************************

%***********************************************************
function initialize_usrp(hObject, eventdata, handles)

    global txUSRP;
    global GTIS_parameters;
    global gtisNodeStarted;
    
    dialogh = msgbox('Starting GTIS local node ...', 'GTIS');
        
    fprintf('Starting GTIS local node . . . ');
    success = GTIS_createRemoteTxNode(GTIS_parameters, txUSRP);
    if (success)
        fprintf('OK\n');
    else
        fprintf('FAIL\n');
        return;
    end
    
    fprintf('TX: Waiting for the USRP to be ready . . . ')
    GTIS_waitForNodeReady(txUSRP);
    fprintf('OK\n');
    close(dialogh);
    gtisNodeStarted = true;
%***********************************************************


%***********************************************************
% --- Executes on button press in startButton.
function startButton_Callback(hObject, eventdata, handles)
    global txUSRP;
    global txSignal;
    global wimaxSamplingRate;
    global transmissionStarted;
    global gtisNodeStarted;
    
    if (~gtisNodeStarted)
        initialize_usrp(hObject, eventdata, handles);
    end
    
    if (transmissionStarted)
        return;
    end
    
    fprintf('TX: Streaming transmit signals to the USRP . . . ')
    [success, ~, ~] = ...
        GTIS_streamTxSignalsToRemoteNode(...
        txUSRP, txSignal, wimaxSamplingRate);
    if (success)
        fprintf('OK\n');
    else
        fprintf('FAIL\n');
        return;
    end

    fprintf('TX: Writting settings to the USRP . . . ')
    success = GTIS_writeTxSettingsToNode(txUSRP);
    if (success)
        fprintf('OK\n');
    else
        fprintf('FAIL\n');
        return;
    end

    fprintf('TX: Starting transmission . . . ');
    success = GTIS_startCyclicTransmission(txUSRP);
    if (success == 1)
        fprintf('OK\n');
    else
        fprintf('FAIL\n');
    end

    transmissionStarted = true;
%***********************************************************



%***********************************************************
% --- Executes on button press in stopButton.
function stopButton_Callback(hObject, eventdata, handles)

    global txUSRP;
    global transmissionStarted;
    global gtisNodeStarted;
    
    if (gtisNodeStarted && transmissionStarted)
        fprintf('TX: Stopping the USRP . . . ')
        success = GTIS_stopCyclicTransmission(txUSRP);
        if (success)
            fprintf('OK\n');
        else
            fprintf('FAIL\n');
            return;
        end
        transmissionStarted = false;
    end
%***********************************************************

%***********************************************************
function freqEdit_Callback(hObject, eventdata, handles)

    global txUSRP;
    global gtisNodeStarted;
    
    txFreq = str2double(get(hObject,'String'));
    txUSRP.txRFCarrier = txFreq * 1e6;
    
    if (gtisNodeStarted)
        fprintf('TX: Writting settings to the USRP (TX RF carrier = %4.2f MHz) . . . ', ...
            txFreq)
        success = GTIS_writeTxSettingsToNode(txUSRP);
        if (success)
            fprintf('OK\n');
        else
            fprintf('FAIL\n');
            return;
        end
    end
%***********************************************************



%***********************************************************
% --- Executes during object creation, after setting all properties.
function freqEdit_CreateFcn(hObject, eventdata, handles)

    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
%***********************************************************


%***********************************************************
% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)

    global txUSRP;
    global transmissionStarted;
    global gtisNodeStarted;
    
    if (transmissionStarted)
        fprintf('TX: Stopping transmission . . . ');
        success = GTIS_stopCyclicTransmission(txUSRP);
        transmissionStarted = false;
        if (success)
            fprintf('OK\n');
        else
            fprintf('FAIL\n');
            return;
        end
    end
    if (gtisNodeStarted)
        fprintf('TX: Dispossing local GTIS node . . . ');        
        success = GTIS_disposeNode(txUSRP);
        gtisNodeStarted = false;
        if (success)
            fprintf('OK\n');
        else
            fprintf('FAIL\n');
            return;
        end
    end
    
    fprintf('\n Good Bye!\n');
%***********************************************************

    
%***********************************************************
% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)

% Hint: delete(hObject) closes the figure
delete(hObject);
%***********************************************************


%***********************************************************
% --- Executes on selection change in channel_menu.
function channel_menu_Callback(hObject, eventdata, handles)

    global txUSRP;
    global transmissionStarted;
    global gtisNodeStarted;
    global txSignal;
    global bursts_tx;
    global simb_freq;
    global wimaxSamplingRate;
    
    transmissionPreviouslyStarted = transmissionStarted;
    gtisNodePreviouslyStarted = gtisNodeStarted;
    
    if (transmissionStarted)
        GTIS_stopCyclicTransmission(txUSRP);
        transmissionStarted = false;
    end
    
    if (gtisNodeStarted)
        GTIS_disposeNode(txUSRP);
        gtisNodeStarted = false;
    end
    
    txUSRP = GTISGUI_getChannelConfig(handles.channel_menu, txUSRP);
    
    % Generate WiMAX signals to be transmitted
    [txSignal, bursts_tx, simb_freq, wimaxSamplingRate] = ...
        GTISGUI_generateWimaxTxSignals(txUSRP.Ntx, txUSRP.Nrx);

    if (gtisNodePreviouslyStarted)
        %initializate the usrp
        initialize_usrp(hObject, eventdata, handles);
    end
    
    if (transmissionPreviouslyStarted)
        %enter continuous mode in transmission
        startButton_Callback(hObject, eventdata, handles)
    end
    
%***********************************************************


% --- Executes during object creation, after setting all properties.
function channel_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to channel_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function Menu_help_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Menu_About_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_About (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global GTIS_parameters;

gtisGUIVersion = GTISGUI_version();
msgbox(sprintf(...
    'GTIS WiMAX GUI version %s\nGTIS WiMAX TX is part of GTIS.\nGTIS version %s\n\nThe GTEC Team, University of A Coruña', ...
    gtisGUIVersion, GTIS_parameters.version), ...
    'GTIS WiMAX TX');

function txGainEdit_Callback(hObject, eventdata, handles)
% hObject    handle to txGainEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txGainEdit as text
%        str2double(get(hObject,'String')) returns contents of txGainEdit as a double
    global txUSRP;
    global gtisNodeStarted;
    
    txGain =  str2double(get(hObject,'String'))
    txUSRP.txGain = txGain;
    
    if (gtisNodeStarted)
        fprintf('TX: Writting settings to the USRP (TX gain = %2.0f dB) . . . ', txGain)
        success = GTIS_writeTxSettingsToNode(txUSRP);
        if (success)
            fprintf('OK\n');
        else
            fprintf('FAIL\n');
            return;
        end
    end

% --- Executes during object creation, after setting all properties.
function txGainEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txGainEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function File_menu_Callback(hObject, eventdata, handles)
% hObject    handle to File_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Exit_menu_Callback(hObject, eventdata, handles)
% hObject    handle to Exit_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1);
