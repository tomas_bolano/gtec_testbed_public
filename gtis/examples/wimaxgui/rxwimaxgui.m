%***********************************************************
% GTIS RX WIMAX GUI
% DATE: NOV-2015
%
% AUTHORS: JOSE ANTONIO GARCÍA NAYA, ISMAEL ROZAS RAMALLAL and
% The GTEC Team!
%
% This software is part of GTIS.
%***********************************************************


%***********************************************************
function varargout = rxwimaxgui(varargin)
    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @rxwimaxgui_OpeningFcn, ...
                       'gui_OutputFcn',  @rxwimaxgui_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
% End initialization code - DO NOT EDIT
%***********************************************************

%***********************************************************
% --- Executes just before the GUI is made visible.
function rxwimaxgui_OpeningFcn(hObject, eventdata, handles, varargin)

    rng(737373);
    rng('default');
    clc;
    
    fprintf('\n======================================================\n');
    fprintf('\t GTIS USRP B210 WiMAX GUI RX Example\n');
    fprintf('======================================================\n');

    % Global variables
    global GTIS_parameters;
    global rxUSRP;
    global acquisitionStarted;
    global gtisNodeStarted;
    global ChannelEstimation;
    global bursts_tx;
    global txSignal;
    global simb_freq;
    global wimaxSamplingRate;
    global config;
    global rxUSRPHasChanged;
    
    % GTIS Initialization
    restoredefaultpath;
    gtisMatlabPath = ...
        fullfile(fileparts(fileparts(fileparts(mfilename('fullpath')))), 'matlab');
    addpath(gtisMatlabPath);
    GTIS_parameters = GTIS_init('printGtisVersion', true);
 
    
    % WiMAX_PHY PATH
    addpath(fullfile(fileparts(fileparts(gtisMatlabPath)), 'WiMAX_PHY'));

    % Choose default command line output for rxwimaxgui
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);
    
    % Read parameters from files
    fid = fopen('parameters_rx.txt');
    
    if (fid == (-1))
        msgbox('Cannot read configuration parameters.', 'GTIS', 'error');
        return;
    end
    
    tline = fgetl(fid);
    while ischar(tline)
%         disp(tline);
        [paramId, paramValue] = strtok(tline, '=');
        paramId = strtrim(paramId);
        if strcmpi(paramId, 'rxIP')
            paramValue = strtrim(strtok(paramValue,('=')));
            rxIP = paramValue;
        elseif strcmpi(paramId, 'rxPort')
            paramValue = strtrim(strtok(paramValue,('=')));
            rxPort = str2double(paramValue);
        elseif strcmpi(paramId, 'rxFreq')
            paramValue = strtrim(strtok(paramValue,('=')));
            rxFreq = str2double(paramValue);
        elseif strcmpi(paramId, 'masterClockRate')
            paramValue = strtrim(strtok(paramValue,('=')));
            masterClockRate = str2double(paramValue);
        elseif strcmpi(paramId, 'signalSamplingRate')
            paramValue = strtrim(strtok(paramValue,('=')));
            signalSamplingRate = str2double(paramValue);
        elseif strcmpi(paramId, 'rxGain')
            paramValue = strtrim(strtok(paramValue,('=')));
            rxGain = str2double(paramValue); 
        elseif strcmpi(paramId, 'rxBandwidth')
            paramValue = strtrim(strtok(paramValue,('=')));
            rxBandwidth = str2double(paramValue); 
        elseif strcmpi(paramId, 'rxAntennaMapping')
            paramValue = strtrim(strtok(paramValue,('=')));
            rxAntennaMapping = paramValue; 
        else
            error('Wrong input parameter file');
        end
        tline = fgetl(fid);
    end

    fclose(fid);
    
    rxUsrpModel = 'B210';
    rxNodeName = 'RX';
    rxUserName = 'testbed';
    rxUSRP = GTIS_nodeDefaultParameters(...
        rxUsrpModel, rxNodeName, rxUserName, rxIP, rxPort, GTIS_parameters);

    rxUSRP.remote.rxSignalsInMultiplexedUSRPFormatFilename = '';
    rxUSRP.rxSignalsInMultiplexedUSRPFormatFilename = '';
    
    rxUSRP.rxAntennaMapping = rxAntennaMapping;
    gtisNodeStarted = false;
    acquisitionStarted = false;
    rxUSRPHasChanged = false;
    
    % Reads the receiving scheme for generating the WiMAX signals
    % accordingly. The transmitted WiMAX signals are necessary to estimate
    % the EVM, and the BER at reception.
    rxUSRP = GTISGUI_getChannelConfig(handles.channel_menu, rxUSRP);
    
    % USRP RX parameters
    rxUSRP.doAcquireWithBothAntennas = 1; % With Nrx = 1 it acquires using both RX antennas
    rxUSRP.doGenerateNMEALog = 1;
    rxUSRP.secondsBetweenNMEAMeasurements = 2;
    rxUSRP.numberOfTrialsGPSLocked = 1;
    rxUSRP.numberOfTrialsREFLocked = 1;
    rxUSRP.usrpWaitForKeyboard = false; % true => requires key pressed before acquisition starts

    rxUSRP.referenceClockSource = 'internal';
    rxUSRP.referenceTimeSource = 'internal';
    rxUSRP.rxRFCarrier = rxFreq;
    rxUSRP.signalSamplingRate = signalSamplingRate;
    rxUSRP.masterClockRate = masterClockRate;
    rxUSRP.rxGain = rxGain;
    rxUSRP.rxRFBandwidth = rxBandwidth;

    rxUSRP.nZeroSamples = 1024/4; % Number of zero-samples, between each interpolation-factor TX signal.
    
    %set up logo
    imshow('gtec.png', 'Parent', handles.logo_axes)
    
    %preselect default values in popup menus
    set(handles.editGain,'String', num2str(rxUSRP.rxGain));
    set(handles.estMethod_menu,'Value', 1);
    set(handles.editRxBandwidth, 'String', num2str(rxUSRP.rxRFBandwidth/1e6));
    set(handles.editRxAntennaMapping, 'String', rxUSRP.rxAntennaMapping);
    ChannelEstimation = GTISGUI_getChannelEstimationMethod(handles.estMethod_menu);    
    set(handles.freqEdit, 'String', num2str(rxUSRP.rxRFCarrier/1e6));
    
    % Generate WiMAX signals to be transmitted
    [txSignal, bursts_tx, simb_freq, wimaxSamplingRate, config] = ...
        GTISGUI_generateWimaxTxSignals(rxUSRP.Ntx, rxUSRP.Nrx);
    
    initialize_usrp(hObject, eventdata, handles);      
%***********************************************************


%***********************************************************
% --- Outputs from this function are returned to the command line.
function varargout = rxwimaxgui_OutputFcn(hObject, eventdata, handles) 

    varargout{1} = handles.output;
%***********************************************************

%***********************************************************
function initialize_usrp(hObject, eventdata, handles)

    global rxUSRP;
    global GTIS_parameters;
    global gtisNodeStarted;
    global rxUSRPHasChanged;
    
    dialogh = msgbox('Starting GTIS local node ...', 'GTIS');
        
    fprintf('Starting remote GTIS node . . . ');
    [success, ~] = ...
        GTIS_createRemoteRxNode(GTIS_parameters, rxUSRP);
    if (success)
        fprintf('OK\n');
    else
        fprintf('FAIL\n');
        return;
    end
    
    fprintf('TX: Waiting for the USRP to be ready . . . ')
    GTIS_waitForNodeReady(rxUSRP);
    
    fprintf('OK\n');
    close(dialogh);
    gtisNodeStarted = true;
    rxUSRPHasChanged = false;
%***********************************************************   


%***********************************************************
% --- Executes on button press in startButton.
function startButton_Callback(hObject, eventdata, handles)

    %----------------- variable initialization --------------------
    global rxUSRP;
    global acquisitionStarted;
    global gtisNodeStarted;
    global ChannelEstimation;
    global bursts_tx;
    global txSignal;
    global simb_freq;
    global wimaxSamplingRate;
    global config;
    global rxUSRPHasChanged;
    
    if (~gtisNodeStarted)
        initialize_usrp(hObject, eventdata, handles);
    end
    
    load('bases_4MHz_ajakes.mat','Bdps');
    Q = 3;
    Bd = squeeze(Bdps(12, 1:Q, :));

    % El ultimo parametro de PHY_rx es una estructura con información para el
    % estimador de canal.
    ChannelEstimationParameters = {};
    ChannelEstimationParameters.Algorithm = ChannelEstimation;
    ChannelEstimationParameters.IciCancellation = 0;
    ChannelEstimationParameters.BEM = Bd;
    ChannelEstimationParameters.SignalTx = simb_freq;
    ts = 1/wimaxSamplingRate;
    
    if (acquisitionStarted)
        % There is already an acquisition running.
        return;
    end
    
    %set the maximum rate in each modulation and codification
    if ((rxUSRP.Ntx == 2) && (rxUSRP.Nrx == 2))
%         set(findobj('Tag','rate_4_0'), 'String', '6.59');
        set(handles.rate_4_0, 'String', '6.59');
        set(handles.rate_4_2, 'String', '9.88');
        set(handles.rate_16_0, 'String', '13.18');
        set(handles.rate_16_2, 'String', '19.77');
        set(handles.rate_64_0, 'String', '19.77');
        set(handles.rate_64_2, 'String', '29.66');
    else
        set(handles.rate_4_0, 'String', '3.29');
        set(handles.rate_4_2, 'String', '4.94');
        set(handles.rate_16_0, 'String', '6.59');
        set(handles.rate_16_2, 'String', '9.88');
        set(handles.rate_64_0, 'String', '9.88');
        set(handles.rate_64_2, 'String', '14.83');
    end

    constellation4QAM = sqrt(2)/2 .* [-1-1j, -1+1j, 1-1j, 1+1j];
    constellation16QAM = [...
        -0.9487-0.9487j, -0.3162-0.9487j, 0.3162-0.9487j, 0.9487-0.9487j, ...
        -0.9487-0.3162j, -0.3162-0.3162j, 0.3162-0.3162j, 0.9487-0.3162j, ...
        -0.9487+0.9487j, -0.3162+0.9487j, 0.3162+0.9487j, 0.9487+0.9487j, ...
        -0.9487+0.3162j, -0.3162+0.3162j, 0.3162+0.3162j, 0.9487+0.3162j, ...
        ];
    constellation64QAM = [...
        -1.0801-1.0801j, -0.7715-1.0801j, -0.4629-1.0801j, -0.1543-1.0801j, ...
        +0.1543-1.0801j, +0.4629-1.0801j, +0.7715-1.0801j, +1.0801-1.0801j, ...
        -1.0801+1.0801j, -0.7715+1.0801j, -0.4629+1.0801j, -0.1543+1.0801j, ...
        +0.1543+1.0801j, +0.4629+1.0801j, +0.7715+1.0801j, +1.0801+1.0801j, ...
        -1.0801-0.7715j, -0.7715-0.7715j, -0.4629-0.7715j, -0.1543-0.7715j, ...
        +0.1543-0.7715j, +0.4629-0.7715j, +0.7715-0.7715j, +1.0801-0.7715j, ...
        -1.0801+0.7715j, -0.7715+0.7715j, -0.4629+0.7715j, -0.1543+0.7715j, ...
        +0.1543+0.7715j, +0.4629+0.7715j, +0.7715+0.7715j, +1.0801+0.7715j, ...
        -1.0801-0.4629j, -0.7715-0.4629j, -0.4629-0.4629j, -0.1543-0.4629j, ...
        +0.1543-0.4629j, +0.4629-0.4629j, +0.7715-0.4629j, +1.0801-0.4629j, ...
        -1.0801+0.4629j, -0.7715+0.4629j, -0.4629+0.4629j, -0.1543+0.4629j, ...
        +0.1543+0.4629j, +0.4629+0.4629j, +0.7715+0.4629j, +1.0801+0.4629j, ...
        -1.0801-0.1543j, -0.7715-0.1543j, -0.4629-0.1543j, -0.1543-0.1543j, ...
        +0.1543-0.1543j, +0.4629-0.1543j, +0.7715-0.1543j, +1.0801-0.1543j, ...
        -1.0801+0.1543j, -0.7715+0.1543j, -0.4629+0.1543j, -0.1543+0.1543j, ...
        +0.1543+0.1543j, +0.4629+0.1543j, +0.7715+0.1543j, +1.0801+0.1543j, ...
        ];
    
     acquisitionStarted = true;
    while (acquisitionStarted)
        if (rxUSRPHasChanged)
            rxUSRPHasChanged = false;
            fprintf('RX: Writting settings to the USRP . . . ');
            success = GTIS_writeRxSettingsToNode(rxUSRP);
            if (success)
                fprintf('OK\n');
            else
                fprintf('FAIL\n');
                return;
            end
        end
        
        % initializate the struct to avoid errors when reception fails
        bursts_rx = struct([]);

        numRequestedSamplesToAcquirePerChannel = ...
            2*(length(txSignal) + rxUSRP.nZeroSamples);
        fprintf('RX: Streaming signals (%d samples) from the USRP . . . ', ...
            numRequestedSamplesToAcquirePerChannel);
        [success, rxSignalsDecimated] = GTIS_startAcquisitionAndStreaming(...
            rxUSRP, wimaxSamplingRate, numRequestedSamplesToAcquirePerChannel);
        if (success)
            fprintf('OK\n');
        else
            fprintf('FAIL\n');
            return;
        end
        
        % Process the received signal
        [bursts_rx, CSI, errorSync] = PHY_rx(...
            config, bursts_tx, rxSignalsDecimated, ChannelEstimationParameters);
        
        if (errorSync)
            fprintf('RX ERROR: Cannot detect frame \n');
            continue;
        end
        

        % RX signal in the time domain
        sampleTimesInMs = (1:length(rxSignalsDecimated)) .* ts .* 1e3;
        plot(handles.graph_timeSignal, sampleTimesInMs, real(rxSignalsDecimated), 'k');
        grid(handles.graph_timeSignal, 'on');
        xlabel(handles.graph_timeSignal, 'time [ms]');
        ylabel(handles.graph_timeSignal, 'amplitude []');
        
        % RX signal in the frequency domain
        Hs = spectrum.welch('Hamming', config.Nfft);
        Hpsd = Hs.psd(rxSignalsDecimated(1,1:min(5e3,length(rxSignalsDecimated))), ...
            'NFFT', config.Nfft, 'Fs', wimaxSamplingRate, 'CenterDC', 1, ...
            'ConfLevel', 0.95);
        XFrequencies = Hpsd.Frequencies ./ 1e6; % Convert frequencies from Hz to MHz
        normFactor = max(abs(Hpsd.Data));
        YPSD = 10*log10(Hpsd.Data ./ normFactor); % Normalize PSD
        plot(handles.graph_spectrum, XFrequencies, YPSD, 'k-', 'LineWidth', 2);
        axisLimits = axis(handles.graph_spectrum);
        set(handles.graph_spectrum, 'xtick', axisLimits(1):2:axisLimits(2));
        set(handles.graph_spectrum, 'ytick', axisLimits(3):10:axisLimits(4));
        set(handles.graph_spectrum, 'GridLineStyle', '-');
        set(handles.graph_spectrum, 'MinorGridLineStyle', '-');
        set(handles.graph_spectrum, 'LineWidth', 0.5);
        grid(handles.graph_spectrum, 'on');
        xlabel(handles.graph_spectrum, 'frequency [MHz]');
        ylabel(handles.graph_spectrum, 'PSD [dB]');

        % channel impulse response
        ratio = size(CSI.ChannelFR(:), 1)/config.Nfft;
        CSI.ChannelFR = downsample(CSI.ChannelFR, 8);
        axis_t = (0:config.NumberOfSymbols-1)*ts*config.Nfft*(1+config.GuardInterval)*1e3;
        axis_f = linspace(-1/ts/2, 1/ts/2, size(CSI.ChannelFR, 1))*1e-6*ratio;
        axis_tau = (0:(size(CSI.ChannelIR,1)-1))*ts*1e6;

        [mesh_t, mesh_tau] = meshgrid(axis_t, axis_tau);
        mesh(handles.graph_channel2, mesh_tau, mesh_t, 10*log10(abs(CSI.ChannelIR)));
        xlabel(handles.graph_channel2, 'delay [\mus]');
        ylabel(handles.graph_channel2, 'time [ms]');
        zlabel(handles.graph_channel2, 'magnitude [dB]');
    
        % channel time-frequency response
        [mesh_t, mesh_f] = meshgrid(axis_t, axis_f);
        mesh(handles.graph_channel1, mesh_f, mesh_t, 10*log10(abs(CSI.ChannelFR)));
        xlabel(handles.graph_channel1, 'frequency [MHz]');
        ylabel(handles.graph_channel1, 'time [ms]');
        zlabel(handles.graph_channel1, 'magnitude [dB]');
    
        % 4-QAM burst
        plot(handles.graph_4qam, bursts_rx(1).symbols, 'k.');
        hold(handles.graph_4qam, 'on');
        plot(handles.graph_4qam, constellation4QAM, 'ro','MarkerSize',5,'LineWidth',2);
        hold(handles.graph_4qam, 'off');
        set(handles.graph_4qam, 'ytick', []);
        set(handles.graph_4qam,'xtick',[]);
        axis(handles.graph_4qam, [-1.25 1.25 -1.25 1.25],'manual');
        set(handles.graph_4qam, 'GridLineStyle', '-');
        set(handles.graph_4qam, 'MinorGridLineStyle', '-');
        set(handles.graph_4qam, 'LineWidth', 0.5);
        grid(handles.graph_4qam, 'on');
        
        % 16-QAM burst
        plot(handles.graph_16qam, bursts_rx(3).symbols, 'k.');
        hold(handles.graph_16qam, 'on');
        plot(handles.graph_16qam, constellation16QAM, 'ro','MarkerSize',5,'LineWidth',2);
        hold(handles.graph_16qam, 'off');
        set(handles.graph_16qam, 'ytick', []);
        set(handles.graph_16qam,'xtick',[]);
        axis(handles.graph_16qam, [-1.25 1.25 -1.25 1.25],'manual');
        set(handles.graph_16qam, 'GridLineStyle', '-');
        set(handles.graph_16qam, 'MinorGridLineStyle', '-');
        set(handles.graph_16qam, 'LineWidth', 0.5);
        grid(handles.graph_16qam, 'on');
        
        % 64-QAM burst
        plot(handles.graph_64qam, bursts_rx(5).symbols, 'k.');
        hold(handles.graph_64qam, 'on');
        plot(handles.graph_64qam, constellation64QAM, 'ro','MarkerSize',5,'LineWidth',2);
        hold(handles.graph_64qam, 'off');
        set(handles.graph_64qam, 'ytick', []);
        set(handles.graph_64qam,'xtick',[]);
        axis(handles.graph_64qam, [-1.25 1.25 -1.25 1.25],'manual');
        set(handles.graph_64qam, 'GridLineStyle', '-');
        set(handles.graph_64qam, 'MinorGridLineStyle', '-');
        set(handles.graph_64qam, 'LineWidth', 0.5);
        grid(handles.graph_64qam, 'on');
        
        set(handles.evm_4_0, 'String', sprintf('%2.2f', bursts_rx(1).EVM));
        set(handles.evm_4_2, 'String', sprintf('%2.2f', bursts_rx(2).EVM));
        set(handles.evm_16_0, 'String', sprintf('%2.2f', bursts_rx(3).EVM));
        set(handles.evm_16_2, 'String', sprintf('%2.2f', bursts_rx(4).EVM));
        set(handles.evm_64_0, 'String', sprintf('%2.2f', bursts_rx(5).EVM));
        set(handles.evm_64_2, 'String', sprintf('%2.2f', bursts_rx(6).EVM));

        set(handles.ber_4_0, 'String', sprintf('%2.2f', bursts_rx(1).BER*100));
        set(handles.ber_4_2, 'String', sprintf('%2.2f', bursts_rx(2).BER*100));
        set(handles.ber_16_0, 'String', sprintf('%2.2f', bursts_rx(3).BER*100));
        set(handles.ber_16_2, 'String', sprintf('%2.2f', bursts_rx(4).BER*100));
        set(handles.ber_64_0, 'String', sprintf('%2.2f', bursts_rx(5).BER*100));
        set(handles.ber_64_2, 'String', sprintf('%2.2f', bursts_rx(6).BER*100));     
        
        drawnow
    end
%***********************************************************


%***********************************************************
% --- Executes on button press in stopButton.
function stopButton_Callback(hObject, eventdata, handles)
    global gtisNodeStarted;
    global acquisitionStarted;
    
    if (gtisNodeStarted && acquisitionStarted)
        acquisitionStarted = false;
    end
%***********************************************************

%*********************************************************** 
% --- Executes during object creation, after setting all properties.
function freqEdit_CreateFcn(hObject, eventdata, handles)

    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
%***********************************************************


%***********************************************************
function freqEdit_Callback(hObject, eventdata, handles)

    global rxUSRP;
    global gtisNodeStarted;
    global acquisitionStarted;
    global rxUSRPHasChanged;
    
    rxFreq = str2double(get(hObject,'String'));
    rxUSRP.rxRFCarrier = rxFreq*1e6;
    
    if (gtisNodeStarted)
        if (~acquisitionStarted)
            fprintf('RX: Writting settings to the USRP (RX RF carrier = %4.2f MHz) . . . ', ...
                rxUSRP.rxRFCarrier/1e6)
            success = GTIS_writeRxSettingsToNode(rxUSRP);
            if (success)
                fprintf('OK\n');
            else
                fprintf('FAIL\n');
                return;
            end
        else
            rxUSRPHasChanged = true;
            fprintf('\n RX: Queuing settings for the USRP => RX carrier frequency = %2.0f MHz\n', ...
                rxUSRP.rxRFCarrier/1e6); 
        end
    end
%***********************************************************    


%***********************************************************
% --- Executes on selection change in estMethod_menu.
function estMethod_menu_Callback(hObject, eventdata, handles)

    global ChannelEstimation;
    
    ChannelEstimation = GTISGUI_getChannelEstimationMethod(hObject);
%***********************************************************


%***********************************************************
% --- Executes during object creation, after setting all properties.
function estMethod_menu_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
%***********************************************************


%***********************************************************
% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
    global rxUSRP;
    global acquisitionStarted;
    global gtisNodeStarted;
    
    if (acquisitionStarted)
        acquisitionStarted = false;
        pause(2);
    end
    
    if (gtisNodeStarted)
        fprintf('RX: Stopping local GTIS node . . . ');        
        success = GTIS_disposeNode(rxUSRP);
        gtisNodeStarted = false;
        if (success)
            fprintf('OK\n');
        else
            fprintf('FAIL\n');
            return;
        end
    end
    
    fprintf('\n Good Bye!\n');
%***********************************************************


%***********************************************************
% --- Executes on selection change in channel_menu.
function channel_menu_Callback(hObject, eventdata, handles)
    global rxUSRP;
    global acquisitionStarted;
    global gtisNodeStarted;
    global txSignal;
    global bursts_tx;
    global simb_freq;
    global wimaxSamplingRate;
    global config;
    
    acquisitionPreviouslyStarted = acquisitionStarted;
    gtisNodePreviouslyStarted = gtisNodeStarted;
    
    if (acquisitionStarted)
        stopButton_Callback(hObject, eventdata, handles);
        % Waits for 2 seconds just in case a frame is being processed
        pause(2);
    end
    
    if (gtisNodeStarted)
        GTIS_disposeNode(rxUSRP);
        gtisNodeStarted = false;
    end
    
    rxUSRP = GTISGUI_getChannelConfig(handles.channel_menu, rxUSRP);
    
    % Generate WiMAX signals to be transmitted
    [txSignal, bursts_tx, simb_freq, wimaxSamplingRate, config] = ...
        GTISGUI_generateWimaxTxSignals(rxUSRP.Ntx, rxUSRP.Nrx);

    if (gtisNodePreviouslyStarted)
        %initializate the usrp
        initialize_usrp(hObject, eventdata, handles);
    end
    
    if (acquisitionPreviouslyStarted)
        %enter continuous mode in transmission
        startButton_Callback(hObject, eventdata, handles)
    end
%***********************************************************


% --- Executes during object creation, after setting all properties.
function channel_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to channel_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editGain_Callback(hObject, eventdata, handles)
% hObject    handle to editGain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editGain as text
%        str2double(get(hObject,'String')) returns contents of editGain as a double
    global rxUSRP;
    global gtisNodeStarted;
    global acquisitionStarted;    
    global rxUSRPHasChanged;
    
    %read gain value and change to it into receiver
    rxGain = str2double(get(hObject,'String'));
    rxUSRP.rxGain = rxGain;
    
    if (gtisNodeStarted)
        if (~acquisitionStarted)
            fprintf('RX: Writting settings to the USRP (RX gain = %2.0f dB) . . . ', ...
                rxUSRP.rxGain);
            success = GTIS_writeRxSettingsToNode(rxUSRP);
            if (success)
                fprintf('OK\n');
            else
                fprintf('FAIL\n');
                return;
            end
        else
            fprintf('\n RX: Queuing settings for the USRP => RX gain = %2.0f dB\n', ...
                rxUSRP.rxGain);
            rxUSRPHasChanged = true;
        end
    end
    

% --- Executes during object creation, after setting all properties.
function editGain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editGain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function Menu_help_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Menu_about_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_about (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global GTIS_parameters;

gtisGUIVersion = GTISGUI_version();
msgbox(sprintf(...
    'GTIS WiMAX GUI version %s\nGTIS WiMAX RX is part of GTIS.\nGTIS version %s\n\nThe GTEC Team, University of A Coruña', ...
    gtisGUIVersion, GTIS_parameters.version), ...
    'GTIS WiMAX RX');
% --------------------------------------------------------------------
function File_menu_Callback(hObject, eventdata, handles)
% hObject    handle to File_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Exit_menu_Callback(hObject, eventdata, handles)
% hObject    handle to Exit_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1);



function editRxBandwidth_Callback(hObject, eventdata, handles)
% hObject    handle to editRxBandwidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editRxBandwidth as text
%        str2double(get(hObject,'String')) returns contents of editRxBandwidth as a double


% --- Executes during object creation, after setting all properties.
function editRxBandwidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editRxBandwidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editRxAntennaMapping_Callback(hObject, eventdata, handles)
% hObject    handle to editRxAntennaMapping (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editRxAntennaMapping as text
%        str2double(get(hObject,'String')) returns contents of editRxAntennaMapping as a double


% --- Executes during object creation, after setting all properties.
function editRxAntennaMapping_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editRxAntennaMapping (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
