function [txSignal, bursts_tx, simb_freq, wimaxSamplingRate, config] = ...
    GTISGUI_generateWimaxTxSignals(Ntx, Nrx)
    
    % WiMAX TX signals
    fprintf('Generation of the frames to be transmitted . . . ');
    dialogh = msgbox('Generating WiMAX transmit signals ...', 'GTIS');
    perfil = 2;
    wimaxSamplingRate = 8e6;
    
    % Generates the WiMAX frames to be transmitted
    config = wimax_pusc_config(perfil, Ntx, Nrx);
    config.Seed = 1000;
    bursts = burst_example(config);
    [txSignal, bursts_tx, simb_freq] = PHY_tx(config, bursts);
    fprintf('OK\n');
    close(dialogh);

end