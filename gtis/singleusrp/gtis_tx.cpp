
#define SLEEP_TIME_IN_SECS_FOR_CONFIGURATION ((double) 1.0)

#include <uhd/utils/thread.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/utils/static.hpp>
#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/exception.hpp>
#include <boost/program_options.hpp>
#include <boost/math/special_functions/round.hpp>
#include <boost/foreach.hpp>
#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <complex>
#include <csignal>
#include <cmath>
#include <limits>
#include <math.h> 
#include <time.h>
#include <netlink/socket.h>

#include <boost/date_time/posix_time/posix_time.hpp>

#include <stdio.h>

#include "gtis/uhd_lib.hpp"
#include "gtis/uhd_lib_utils.hpp"
#include "gtis/config.hpp"
#include "gtis/commands.hpp"
#include "gtis/streamer.hpp"
#define GTIS_COMMAND_NAME "gtis_tx"

namespace po = boost::program_options;

/***********************************************************************
 * SIG_INT handler: needed to stop acquisition 
 **********************************************************************/
static volatile bool stop_signal_called = false;
void sig_int_handler(int) {
	stop_signal_called = true;
}

/***********************************************************************
 * Main function
 **********************************************************************/
int UHD_SAFE_MAIN(int argc, char *argv[]) {	
    /* where to print out messages */
    FILE * fd = stdout;
	
    /* Setting priority to main thread. Not critical in transmission. */
    if (not uhd::set_thread_priority_safe()) {
        fprintf(stdout, "Failure in main priority.\n");
    }

    /* Variables needed by socket commands */
    NL::Protocol protocol = NL::TCP;
    NL::IPVer ipver = NL::ANY;
    NL::Socket * server = NULL;
    NL::Socket * client_connection = NULL;
    gtis_command_id_t command_id = 0;
    gtis_response_id_t response_id = 0;

    /* variables to be set by po */
    std::string args, 
        tx_antenna_mapping,
        subdevice_specification,
        reference_clock_source, 
        reference_time_source,
        channels_mapping,
        input_file_list;
    double sampling_rate,
        tx_rf_frequency,
        tx_gain,
        tx_bandwidth;
    unsigned int socket_port_number;

    /* Threads */
    boost::thread transmission_thread;

    /* Command variables */
    bool transmitting_state = false;
    bool network_commands = true;
    bool transmit_data_loaded = false;
	
    /* Timed commands */;
    // uhd::time_spec_t current_time;

    /* GPSDO, REF and LO */
    bool usrp_has_gpsdo_available = false;
    bool usrp_has_ref_available = false;
    unsigned int number_of_trials_gps_locked;
    unsigned int number_of_trials_ref_locked;
    unsigned int number_of_trials_lo_locked;

    /* Buffers for TX signals */
    std::vector < std::vector < std::complex < short > > > file_buffers;

    /* usrp transmit streamer object */
    uhd::tx_streamer::sptr tx_stream = NULL;

    /* Setup the program options */
    po::options_description desc("Allowed options");
    desc.add_options()
        ("version", "prints out the version")
        ("help", "help message")
        ("args",
            po::value < std::string > (&args)->default_value(""),
            "passed to uhd::usrp::multi_usrp::make(args)")
        ("sampling_rate",
            po::value < double >(&sampling_rate),
            "sampling rate of outgoing samples")
        ("tx_rf_frequency", po::value < double >(&tx_rf_frequency),
            "TX RF center frequency in Hz")
        ("tx_gain", po::value < double >(&tx_gain),
            "TX gain in dB")
        ("tx_antenna_mapping",
            po::value < std::string > (&tx_antenna_mapping)->default_value("TX/RX"),
            "TX antenna mapping. The default value is \"TX/RX\"")
        ("subdevice_specification",
            po::value < std::string > (&subdevice_specification),
            "daughterboard subdevice specification. See Ettus UHD user manual")
        ("tx_bandwidth",  po::value < double >(&tx_bandwidth),
            "TX filter bandwidth in Hz")
        ("reference_clock_source",
            po::value<std::string>(&reference_clock_source)->default_value("internal"),
            "reference clock source. The default value is \"internal\"")
        ("reference_time_source",
            po::value<std::string>(&reference_time_source)->default_value("internal"),
            "reference time source. The default value is \"internal\"")
        ("channels_mapping",
            po::value < std::string > (&channels_mapping)->default_value("0"),
            "specifies what channels are used (specify \"0\", \"1\", \"0,1\", etc)")
        ("socket_port_number",
            po::value < unsigned int > (&socket_port_number)->default_value(5000),
            "socket port number for network commands. The default value is 5000")
        ("net_commands", po::value <bool> (&network_commands)->default_value(true),
            "Listen to network commands (true by default) or execute in local (false)")
        ("number_of_trials_gps_locked",
            po::value <unsigned int> (&number_of_trials_gps_locked)->default_value(10),
            "number of trials to get the GPS locked. The default value is 10")
        ("number_of_trials_ref_locked",
            po::value <unsigned int> (&number_of_trials_ref_locked)->default_value(10),
            "number of trials to get the external REF (10 MHz) locked. The default value is 10")
        ("number_of_trials_lo_locked",
            po::value <unsigned int> (&number_of_trials_lo_locked)->default_value(3),
            "number of trials to get the LO locked. The default value is 3")
        ("input-files",
            po::value < std::string > (&input_file_list),
            "an input filename per channel (e.g. \"filename_channel_0, \"filename_channel_1\"")
        ;
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (argc == 1) {
        GTIS_PRINT_VERSION((char *)GTIS_COMMAND_NAME);
        std::cout << desc << std::endl;
        return EXIT_SUCCESS;
    }

    /* print the version */
    if (vm.count("version")) {
        GTIS_PRINT_VERSION((char *)GTIS_COMMAND_NAME);
        return EXIT_SUCCESS;
    }

    /* print the help message */
    if (vm.count("help")) {
        GTIS_PRINT_VERSION((char *)GTIS_COMMAND_NAME);
        std::cout << desc << std::endl;
        return EXIT_SUCCESS;
    }

    /* checks the center frequency parameter */
    if (not vm.count("tx_rf_frequency")) {
        std::cerr << "Please specify the center frequency with --tx_rf_frequency" << std::endl;
        return EXIT_FAILURE;
    }

    /* checks the sampling rate parameter */
    if (not vm.count("sampling_rate")) {
        std::cerr << "Please specify the sampling rate with --sampling_rate" << std::endl;
        return EXIT_FAILURE;
    }

    GTIS_PRINT_VERSION((char *) GTIS_COMMAND_NAME);

    /* create a usrp device */
    std::cout << boost::format("Creating the usrp device with: %s...") %
        args << std::endl;
    uhd::usrp::multi_usrp::sptr usrp = uhd::usrp::multi_usrp::make(args);

    /* always select the subdevice first, the channel mapping affects
     * the other settings */
    if (vm.count("subdevice_specification")) {
        usrp->set_tx_subdev_spec(subdevice_specification);
    }
    std::cout << boost::format("\n Using device: %s") % usrp->get_pp_string() << std::endl;
    std::cout << boost::format(" TX subdevice: %s") % usrp->get_tx_subdev_spec().to_pp_string() << std::endl;

    //TODO: Fixme!!
    boost::this_thread::sleep(boost::posix_time::milliseconds(1000));

    /* detect the channels to be used */
    std::vector < std::string > channel_strings;
    std::vector < size_t > channel_nums;
    boost::split(channel_strings, channels_mapping,
        boost::is_any_of("\"',"), boost::token_compress_on);
    for (size_t ch = 0; ch < channel_strings.size(); ch++) {
        size_t chan = boost::lexical_cast < int >(channel_strings[ch]);
        if (chan < usrp->get_tx_num_channels()) {
            channel_nums.push_back(boost::lexical_cast < int >(channel_strings[ch]));
        } else {
            throw std::runtime_error("Invalid channel(s) specified.");
        }
    }

    // detect the filenames to use
    std::vector < std::string > input_file_names;
    if (vm.count("input-files")) {
        std::cout << boost::format("Input file(s): \"%s\"") % input_file_list << std::endl;
        if (file_name_splitter(input_file_list, input_file_names, channel_nums.size()) != EXIT_SUCCESS) {
            std::cerr << "Please provide an input file name per channel " << std::endl;
            return EXIT_FAILURE;
        }
    }

    /* Set time and clock source */
    std::cout << boost::format("Setting reference clock source to \"%s\"")
         % reference_clock_source << std::endl;
    usrp->set_clock_source(reference_clock_source);
    std::cout << boost::format("Setting reference time source to \"%s\"")
          % reference_time_source << std::endl;
    usrp->set_time_source(reference_time_source);

    /* Waits for GPSDO in case there is one */
    usrp_has_gpsdo_available = GTIS_usrp_has_gpsdo(usrp);
    if ((reference_clock_source == "gpsdo") || 
		(reference_time_source == "gpsdo")) {
        if (usrp_has_gpsdo_available) {
            GTIS_wait_for_usrp_gpsdo_locked(usrp, true, 
				number_of_trials_gps_locked);
        } else {
            std::cout << boost::format("WARNING! gspdo requested but not available.") << std::endl;
        }
    }

    /* Waits for the external reference in case it was selected */
    usrp_has_ref_available = GTIS_usrp_has_ref(usrp);
    if (usrp_has_ref_available &&
            ((reference_clock_source == "external") ||
            (reference_time_source == "external"))) {
        GTIS_wait_for_usrp_ref_locked(usrp, true, 
        number_of_trials_ref_locked);
    }
    GTIS_wait_for_usrp_lo_locked(
        usrp, true, number_of_trials_lo_locked, channel_nums);

    /* aligns USRP time with GPSDO (if available) or with the host.
     * this is important to get precise timestamp as well as NMEA 
     * records. */
    GTIS_align_usrp_time(usrp, true);

    /* Configuration of the USRP with the received command-line parameters. */

    /* Set sampling rate */
    /* Warning! USRP X310 doesn't support this as a timed command*/
    std::cout << boost::format("Setting TX rate to \"%3.3f\" Msample/s") %
        (sampling_rate / 1.0e6) << std::endl;
    usrp->set_tx_rate(sampling_rate);

    usrp->set_command_time(usrp->get_time_now() + TIME_DELAY_BETWEEN_COMMANDS);
    for (size_t ch = 0; ch < channel_nums.size(); ch++) {
        /* Code from tx_bursts.cpp */
        //uhd::tune_request_t tune_request(tx_rf_frequency);
        //usrp->set_tx_freq(tune_request, channel_nums[ch]);
        /* Code from tx_bursts.cpp */
        usrp->set_tx_freq(tx_rf_frequency, channel_nums[ch]);
        if (vm.count("tx_gain")) {
           usrp->set_tx_gain(tx_gain, channel_nums[ch]);
        }

        if (vm.count("tx_bandwidth")) {
            usrp->set_tx_bandwidth(tx_bandwidth, channel_nums[ch]);
        }

        if (vm.count("tx_antenna_mapping")) {
            usrp->set_tx_antenna(tx_antenna_mapping, channel_nums[ch]);
        }
    }
    usrp->clear_command_time();

    GTIS_wait_for_usrp_lo_locked(usrp, true,
        number_of_trials_lo_locked, channel_nums);

    std::cout << boost::format("Actual reference clock source: \"%s\"") %
        usrp->get_clock_source(0) << std::endl;
    std::cout << boost::format("Actual reference time source: \"%s\"") %
        usrp->get_time_source(0) << std::endl;
    std::cout << boost::format("Actual master clock rate: %3.2f Msample/s") %
        (usrp->get_master_clock_rate() / 1e6) << std::endl;
    std::cout << boost::format("Actual TX rate: %3.2f Msample/s") %
        (usrp->get_tx_rate() / 1e6) << std::endl;
    std::cout << boost::format("Actual TX frequency: %4.2f MHz") %
        (usrp->get_tx_freq(channel_nums[0]) / 1e6) << std::endl;
    std::cout << boost::format("Actual TX gain: %2.1f dB") %
        usrp->get_tx_gain(channel_nums[0]) << std::endl;
    std::cout << boost::format("Actual TX bandwidth: %2.1f MHz") %
        (1e-6 * usrp->get_tx_bandwidth(channel_nums[0])) << std::endl;
    std::cout << boost::format("Actual TX antenna mapping: \"%s\"") %
        usrp->get_tx_antenna(channel_nums[0]) << std::endl;

    /* create transmit streamer
     * channels are mapped as follows: index0 = channel0, index1 = channel1, ...
     */
    uhd::stream_args_t stream_args("sc16", "sc16");
    stream_args.channels = channel_nums;
    usrp->set_command_time(usrp->get_time_now() + TIME_DELAY_BETWEEN_COMMANDS);
    tx_stream = usrp->get_tx_stream(stream_args);
    usrp->clear_command_time();

    /* Two execution modes: online with network commands or offline */
    /* Read signals from file(s) only if network commands are disabled. */
    if (not network_commands) {
            // Buffering samples from files
            if (read_from_file_to_buffers(input_file_names, channel_nums, file_buffers) != EXIT_SUCCESS) {
                    std::cout << "Error loading files. " << std::endl;
                    return EXIT_FAILURE;
            }
            transmit_data_loaded = true;
    }

    if (network_commands){
        /* Initialize NetLink Sockets library */
        NL::init();

        /* Waiting for commands */
        bool all_commands_received = false;
        try {
            /* Open server socket to receive the requests from clients */
            server = new NL::Socket(socket_port_number, protocol, ipver, "*");
            server->blocking(true);

            /* Disable manual stopping by means of CTRL+C */
            //std::signal(SIGINT, SIG_IGN);

            /* Loop for receiving commands until finish command is get */
            while (not all_commands_received) {
                fprintf(fd, "\n\n%s: Listening for connection on port number %d ... ",
                        GTIS_COMMAND_NAME, socket_port_number);
                fflush(fd);

                /* Wait for a client connection */
                client_connection = server->accept();
                client_connection->blocking(true); /* Needed ???? */
                fprintf(fd, "CONNECTED!\n");
                fflush(fd);

                /* Wait for a command_id */
                fprintf(fd, "Listening for command id on port number %d ... ",
                       socket_port_number);
                fflush(fd);

                if (not gtis_recv_command_id(&command_id, client_connection, NULL)) {
                    fprintf(fd, "FAIL\n");
                    if (client_connection != NULL) {
                        response_id = GTIS_COMMAND_RESPONSE_KO;
                        gtis_send_response_id(&response_id, client_connection, NULL);
                        fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                        fflush(fd);
                        client_connection->disconnect();
                        continue;
                    }
                } else {
                    fprintf(fd, "OK\n");
                    fprintf(fd, "Received command id = \"%d\"\n", command_id);
                    fflush(fd);
                }

                /* Processes the received command */
                if (command_id == GTIS_COMMAND_READY) {
                    /* GTIS_COMMAND_READY: used for knowing when the node is ready */
                    response_id = GTIS_COMMAND_RESPONSE_OK;
                    gtis_send_response_id(&response_id, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                    fflush(fd);
                }  else if (command_id == GTIS_COMMAND_FINISH) {
                    /* GTIS_COMMAND_FINISH: finishes the node */
                    all_commands_received = true;
                    if (transmitting_state) {
                        stop_signal_called = true;
                        transmission_thread.join();
                        transmitting_state = false;
                        stop_signal_called = false;
                    }
                    response_id = GTIS_COMMAND_RESPONSE_OK;
                    gtis_send_response_id(&response_id, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                    fflush(fd);
                } else if (command_id == GTIS_COMMAND_GET_NODE_VERSION) {
                    /* GTIS_COMMAND_GET_NODE_VERSION: used to get the GTIS version of the node */
                    response_id = GTIS_COMMAND_RESPONSE_OK;
                    gtis_send_response_id(&response_id, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                    fflush(fd);
                    size_t str_max_length = 255;
                    char str[str_max_length+1];
                    GTIS_get_version_string(str, str_max_length);
                    str[str_max_length] = '\0';
                    size_t str_length = strlen(str);
                    size_t num_bytes_sent = 0;
                    gtis_send_buffer_length((gtis_buffer_length_t *) &str_length,
                        client_connection, NULL);
                    gtis_send_buffer((unsigned char *) & str, strlen(str),
                        &num_bytes_sent, client_connection, NULL);
                    fprintf(fd, "Sent gtis version string = \"%s\"", str);
                    fflush(fd);
                } else if (command_id == GTIS_COMMAND_TX_GAIN) {
                    /* GTIS_COMMAND_TX_GAIN: set transmission gain. */
                    /* if successful, response includes the actual gain */
                    gtis_response_value_t response_value;
                    if (gtis_recv_command_value((gtis_response_value_t *) & tx_gain,
                        client_connection, NULL)) {
                        fprintf(fd, " Received tx gain = \"%f\" dB\n", tx_gain);

                        usrp->set_command_time(usrp->get_time_now() +
                           TIME_DELAY_BETWEEN_COMMANDS);
                        for (std::vector < size_t >::size_type ch = 0;
                            ch < channel_nums.size(); ch++) {
                            usrp->set_tx_gain(tx_gain, channel_nums[ch]);
                        }
                        usrp->clear_command_time();
                        //boost::this_thread::sleep(boost::posix_time::seconds(1.0));
                        fprintf(fd, " Wait until LO is locked . . . ");
                        GTIS_wait_for_usrp_lo_locked(
                            usrp, true, number_of_trials_lo_locked, channel_nums);
                        for (std::vector < size_t >::size_type ch = 0;
                            ch < channel_nums.size(); ch++) {
                            tx_gain = usrp->get_tx_gain(channel_nums[ch]);
                            fprintf(fd, " Actual TX gain for channel %d: %f dB\n",
                                (int) ch, tx_gain);
                        }
                        response_id = GTIS_COMMAND_RESPONSE_OK;
                        response_value = tx_gain;
                    } else {
                        response_id = GTIS_COMMAND_RESPONSE_KO;
                        response_value = 0;
                    }
                    gtis_send_response_id(& response_id, client_connection, NULL);
                    gtis_send_response_value(& response_value, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\", value = \"%f\"\n",
                        response_id, response_value);
                    fflush(fd);
                } else if (command_id == GTIS_COMMAND_TX_FREQ) {
                    /* GTIS_COMMAND_TX_FREQ: set carrier frequency in transmission */
                    /* if successful, response includes the actual RF frequency */
                    gtis_response_value_t response_value;
                    if (gtis_recv_command_value((gtis_response_value_t *) & tx_rf_frequency,
                        client_connection, NULL)) {
                        fprintf(fd, " Received TX RF frequency = \"%f\" MHz\n",
                            tx_rf_frequency / 1e6);

                        usrp->set_command_time(usrp->get_time_now() + TIME_DELAY_BETWEEN_COMMANDS);
                        for (std::vector < size_t >::size_type ch = 0;
                            ch < channel_nums.size(); ch++) {
                            usrp->set_tx_freq(tx_rf_frequency, channel_nums[ch]);
                        }
                        usrp->clear_command_time();

                        fprintf(fd, " Wait until LO is locked . . . ");
                        GTIS_wait_for_usrp_lo_locked(
                            usrp, true, number_of_trials_lo_locked, channel_nums);

                        for (std::vector < size_t >::size_type ch = 0; ch < channel_nums.size(); ch++) {
                            tx_rf_frequency = usrp->get_tx_freq(channel_nums[ch]);
                            fprintf(fd, " Actual TX frequency for channel %d: %f MHz\n",
                                (int) ch, (tx_rf_frequency / 1.0e6));
                        }
                        response_id = GTIS_COMMAND_RESPONSE_OK;
                        response_value = tx_rf_frequency;
                    } else {
                        response_id = GTIS_COMMAND_RESPONSE_KO;
                        response_value = 0;
                    }
                    gtis_send_response_id(& response_id, client_connection, NULL);
                    gtis_send_response_value(& response_value, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\", value = \"%f\"\n",
                        response_id, response_value);
                    fflush(fd);
                } else if (command_id == GTIS_COMMAND_TX_BANDWIDTH) {
                    /* GTIS_COMMAND_TX_BANDWIDTH: set transmitter bandwidh. */
                    /* if successful, response includes the actual bandwidth */
                    gtis_response_value_t response_value;
                    if (gtis_recv_command_value((gtis_response_value_t *) & tx_bandwidth,
                        client_connection, NULL)) {
                        fprintf(fd, " Received tx bandwidth = \"%f\" dB\n", tx_bandwidth);

                        usrp->set_command_time(usrp->get_time_now() +
                            TIME_DELAY_BETWEEN_COMMANDS);
                        for (std::vector < size_t >::size_type ch = 0;
                            ch < channel_nums.size(); ch++) {
                            usrp->set_tx_bandwidth(tx_bandwidth, channel_nums[ch]);
                        }
                        usrp->clear_command_time();
                        //boost::this_thread::sleep(boost::posix_time::seconds(1.0));
                        fprintf(fd, " Wait until LO is locked . . . ");
                        GTIS_wait_for_usrp_lo_locked(
                            usrp, true, number_of_trials_lo_locked, channel_nums);
                        for (std::vector < size_t >::size_type ch = 0;
                            ch < channel_nums.size(); ch++) {
                            tx_bandwidth = usrp->get_tx_bandwidth(channel_nums[ch]);
                            fprintf(fd, " Actual TX bandwidth for channel %d: %f MHz\n",
                                (int) ch, tx_bandwidth);
                        }
                        response_id = GTIS_COMMAND_RESPONSE_OK;
                        response_value = tx_bandwidth;
                    } else {
                        response_id = GTIS_COMMAND_RESPONSE_KO;
                        response_value = 0;
                    }
                    gtis_send_response_id(& response_id, client_connection, NULL);
                    gtis_send_response_value(& response_value, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\", value = \"%f\"\n",
                        response_id, response_value);
                    fflush(fd);
                } else if (command_id == GTIS_COMMAND_START_TX) {
                    /* GTIS_COMMAND_START_TX: starts transmission */
                    /* Response can be OK or KO if node is already transmitting */
                    uint64_t tx_time = 0;
                    if (not gtis_recv_uint64(& tx_time,
                        client_connection, NULL)) {
                        response_id = GTIS_COMMAND_RESPONSE_KO;
                    } else {
                        fprintf(fd, "Received tx_time = %lu tics\n", tx_time);
                        bool started_new_transmission = false;
                        if (transmit_data_loaded and (not transmitting_state)) {
                            transmitting_state = true;
                            stop_signal_called = false;
                            transmission_thread = boost::thread(
                                send_from_buffers_to_tx_streamer,
                                usrp,
                                tx_stream,
                                file_buffers,
                                tx_time,
                                & stop_signal_called);
                            started_new_transmission = true;
                        }

                        response_id = started_new_transmission ?
                            GTIS_COMMAND_RESPONSE_OK : GTIS_COMMAND_RESPONSE_KO;
                        gtis_send_response_id(&response_id, client_connection, NULL);
                        fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                        fflush(fd);
                        if (started_new_transmission) {
                            fprintf(fd, "\n>>> TRANSMITTING . . .\n\n");
                        }
                        fflush(fd);
                    }
                } else if (command_id == GTIS_COMMAND_STOP_TX) {
                    /* GTIS_COMMAND_STOP_TX: stops transmission */
                    /* Response can be OK or KO if node is already stopped */
                    if (transmitting_state) {
                        stop_signal_called = true;
                        transmission_thread.join();
                        transmitting_state = false;
                        stop_signal_called = false;
                        response_id = GTIS_COMMAND_RESPONSE_OK;
                    } else {
                        response_id = GTIS_COMMAND_RESPONSE_KO;
                    }
                    gtis_send_response_id(&response_id, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                    fflush(fd);
                } else if (command_id == GTIS_COMMAND_LOAD_INPUT_FILE_SIGNALS) {
                    /* GTIS_COMMAND_LOAD_INPUT_FILE_SIGNALS: load input signals
                     * from file(s). Filename list is received from the client.*/
                    response_id = transmitting_state ? GTIS_COMMAND_RESPONSE_KO : GTIS_COMMAND_RESPONSE_OK;
                    if (not transmitting_state) {
                        gtis_buffer_length_t temp_buffer_size = 0;
                        bool response_ok = true;
                        if (gtis_recv_buffer_length(& temp_buffer_size,
                            client_connection, NULL)) {
                            fprintf(fd, "Received file name(s) length = \"%lu\"\n",
                                temp_buffer_size);
                            fflush(fd);
                            char temp_buffer[temp_buffer_size + 1];
                            size_t num_bytes_received = 0;
                            fprintf(fd, "Reading the file name list ...\n");
                            if (not gtis_recv_buffer((char *) &temp_buffer, (size_t) temp_buffer_size,
                                &num_bytes_received, client_connection, NULL)) {
                                fprintf(fd, "Error reading the filename buffer\n");
                                fflush(fd);
                                response_ok = false;
                            } else {
                                fprintf(fd, "num_bytes_received = %ld\n", num_bytes_received);
                            }
                            temp_buffer[temp_buffer_size] = '\0';
                            input_file_list = std::string(temp_buffer);
                            fprintf(fd, "Received file list = \"%s\"\n", input_file_list.c_str());
                            fprintf(fd, "Loading files . . . \n");;
                            if (response_ok && (file_name_splitter(input_file_list, input_file_names, channel_nums.size())
                                != EXIT_SUCCESS)) {
                                fprintf(fd, "Please provide an input file name per channel.\n");
                                response_ok = false;
                            }

                            if (response_ok && (read_from_file_to_buffers(input_file_names, channel_nums, file_buffers)
                                != EXIT_SUCCESS)) {
                                fprintf(fd, "Error loading files.\n");
                                response_ok = false;
                            }
                            response_id = response_ok ? GTIS_COMMAND_RESPONSE_OK : GTIS_COMMAND_RESPONSE_KO;
                            transmit_data_loaded = response_ok;
                        }
                    }
                    gtis_send_response_id(&response_id, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                    fflush(fd);
                } else if (command_id == GTIS_COMMAND_STREAM_INPUT_SIGNALS) {
                    /* GTIS_COMMAND_STREAM_INPUT_SIGNALS: load input signals
                     * from network streaming. */
                    response_id = transmitting_state ? GTIS_COMMAND_RESPONSE_KO : GTIS_COMMAND_RESPONSE_OK;
                    if (not transmitting_state) {
                        size_t num_complex_samples_per_channel = 0;
                        if (gtis_recv_buffer_length((gtis_buffer_length_t *) & num_complex_samples_per_channel,
                            client_connection, NULL)) {
                            fprintf(fd, " num_complex_samples_per_channel = \"%lu\"\n",
                                num_complex_samples_per_channel);
                            fflush(fd);

                            gtis_send_response_id(&response_id, client_connection, NULL);
                            fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                            fflush(fd);

                            read_from_stream_to_buffers(
                                client_connection,
                                channel_nums.size(),
                                num_complex_samples_per_channel,
                                file_buffers,
                                    NULL);
                            transmit_data_loaded = true;
                        } else {
                            response_id = GTIS_COMMAND_RESPONSE_KO;
                            gtis_send_response_id(&response_id, client_connection, NULL);
                            fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                            fflush(fd);
                        }
                    } else { /* if (not transmitting_state) */
                        gtis_send_response_id(&response_id, client_connection, NULL);
                        fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                        fflush(fd);
                    }
                } else if (command_id == GTIS_COMMAND_GET_NODE_TIME) {
                    /* GTIS_COMMAND_GET_NODE_TIME: get the USRP last pps time */
                    uint64_t waitForPPSEdge = 0;
                    if (not gtis_recv_uint64(& waitForPPSEdge,
                        client_connection, NULL)) {
                        response_id = GTIS_COMMAND_RESPONSE_KO;
                    } else {
                        response_id = GTIS_COMMAND_RESPONSE_OK;
                    }
                    gtis_send_response_id(&response_id, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                    fflush(fd);
                    if (waitForPPSEdge >= 1) {
                        GTIS_wait_for_pps_edge(usrp);
                    }
                    uhd::time_spec_t time_last_pps = usrp->get_time_last_pps();
                    uint64_t usrp_time = (uint64_t) time_last_pps.to_ticks(1.0);
                    gtis_send_uint64(&usrp_time, client_connection, NULL);
                    fprintf(fd, "Sent USRP time last PPS: %lu s\n", usrp_time);
                    fflush(fd);
                } else if (command_id == GTIS_COMMAND_TIMED_CONFIG_TX) {
                    /* GTIS_COMMAND_TIMED_CONFIG_TX: configure USRP at given time instant */
                    /* Response can be OK or KO if node is already transmitting */
                    uint64_t config_time = 0;
                    if (not gtis_recv_uint64(& config_time,
                        client_connection, NULL)) {
                        response_id = GTIS_COMMAND_RESPONSE_KO;
                        gtis_send_response_id(&response_id, client_connection, NULL);
                        fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                        fprintf(fd, "Missing configuration time!\n");
                        fflush(fd);
                    } else {
                        fprintf(fd, "Received config_time = %lu tics\n", config_time);
                        if (transmitting_state) {
                            response_id = GTIS_COMMAND_RESPONSE_KO;
                            gtis_send_response_id(&response_id, client_connection, NULL);
                            fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                            fprintf(fd, "Cannot configure device while transmitting.\n");
                            fflush(fd);
                        } else {
                            /* Reply inmediately to the client */
                            response_id = GTIS_COMMAND_RESPONSE_OK;
                            gtis_send_response_id(&response_id, client_connection, NULL);
                            fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                            fflush(fd);
                        }

                        fprintf(fd, "Timed configuration will start at %lu, current time is %lu ...\n",
                                config_time, (uint64_t) usrp->get_time_now().to_ticks(1.0));

                        uint64_t time_now = 0;
                        while (time_now > (config_time - 1)) {
                            time_now = (uint64_t) GTIS_wait_for_pps_edge(usrp).to_ticks(1.0);
                        }

                        fprintf(fd, " Aligning USRP time ... ");
                        fflush(fd);
                        if (GTIS_align_usrp_time(usrp, true)) {
                            fprintf(fd, "OK\n");
                        } else {
                            fprintf(fd, "FAIL\n");
                        }

                        uint64_t secs_between_commands = 1;
                        config_time = secs_between_commands + usrp->get_time_last_pps().to_ticks(1.0);

                        for (size_t ch = 0; ch < channel_nums.size(); ch++) {
                            fprintf(fd, "Setting TX RF frequency for channel %lu to %4.2f MHz at time %lu ...\n",
                                    channel_nums[ch], tx_rf_frequency, config_time);
                            fprintf(fd, "Setting TX gain for channel %lu to %2.1f dB at time %lu ...\n",
                                    channel_nums[ch], tx_gain, config_time);
                            fprintf(fd, "Setting TX bandwidth for channel %lu to %3.2f MHz at time %lu ...\n",
                                    channel_nums[ch], tx_bandwidth, config_time);
                            fprintf(fd, "Setting TX antenna mapping for channel %lu to \"%s\" at time %lu ...\n",
                                    channel_nums[ch], tx_antenna_mapping.c_str(), config_time);
                        }
                        fflush(fd);

                        for (size_t ch = 0; ch < channel_nums.size(); ch++) {
                            /* Code from tx_bursts.cpp */
                            //uhd::tune_request_t tune_request(tx_rf_frequency);
                            //usrp->set_tx_freq(tune_request, channel_nums[ch]);
                            /* Code from tx_bursts.cpp */
                            
                            if (vm.count("tx_rf_frequency")) {
                                usrp->set_tx_freq(tx_rf_frequency, channel_nums[ch]);
                            }
                            if (vm.count("tx_gain")) {
                                usrp->set_tx_gain(tx_gain, channel_nums[ch]);
                            }
                            if (vm.count("tx_bandwidth")) {
                                usrp->set_tx_bandwidth(tx_bandwidth, channel_nums[ch]);
                            }
                            if (vm.count("tx_antenna_mapping")) {
                                usrp->set_tx_antenna(tx_antenna_mapping, channel_nums[ch]);
                            }
                        }
                        usrp->clear_command_time();

                        for (size_t ch = 0; ch < channel_nums.size(); ch++) {
                            fprintf(fd, "Actual TX RF frequency for channel %lu: %4.2f MHz ...\n",
                                    channel_nums[ch], usrp->get_tx_freq(channel_nums[ch]));
                            fprintf(fd, "Actual TX gain for channel %lu: %2.1f dB\n",
                                    channel_nums[ch], usrp->get_tx_gain(channel_nums[ch]));
                            fprintf(fd, "Actual TX bandwidth for channel %lu: %3.2f MHz\n",
                                    channel_nums[ch], usrp->get_tx_bandwidth(channel_nums[ch]));
                            fprintf(fd, "Actual TX antenna mapping for channel %lu: \"%s\"\n",
                                    channel_nums[ch], usrp->get_tx_antenna(channel_nums[ch]).c_str());
                        }
                        fflush(fd);


                        /* Set sampling rate */
                        /* Warning! USRP X310 doesn't support this as a timed command*/
                        GTIS_wait_for_pps_edge(usrp);
                        config_time = secs_between_commands + usrp->get_time_last_pps().to_ticks(1.0);
                        fprintf(fd, "Setting TX signal sampling rate to %3.3f Msample/s at time %lu ...\n",
                            (sampling_rate / 1.0e6), config_time);
                        fflush(fd);
                        usrp->set_command_time(uhd::time_spec_t::from_ticks((long long) config_time, 1.0));
                        usrp->set_tx_rate(sampling_rate);
                        usrp->clear_command_time();
                        fprintf(fd, "Actual TX signal sampling rate: %3.3f Msample/s\n",
                            (usrp->get_tx_rate() / 1e6));
                        fflush(fd);

                        GTIS_wait_for_usrp_lo_locked(usrp, true,
                            number_of_trials_lo_locked, channel_nums);

                        /* creates transmit streamer */

                        GTIS_wait_for_pps_edge(usrp);
                        config_time = secs_between_commands + usrp->get_time_last_pps().to_ticks(1.0);
                        fprintf(fd, "Getting TX streamer at time %lu ... ", config_time);
                        fflush(fd);
                        uhd::stream_args_t stream_args("sc16", "sc16");
                        stream_args.channels = channel_nums;
                        usrp->set_command_time(uhd::time_spec_t::from_ticks((long long) config_time, 1.0));
                        tx_stream = usrp->get_tx_stream(stream_args);
                        usrp->clear_command_time();
                        fprintf(fd, "Finished GTIS_COMMAND_TIMED_CONFIG_TX\n");
                        fflush(fd);
                    }
                } else {
                    fprintf(fd, "Invalid command!\n");
                    response_id = GTIS_COMMAND_RESPONSE_KO;
                    gtis_send_response_id(&response_id, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                    fflush(fd);
                }

                /* closes connection */
                if (client_connection != NULL) {
                    client_connection->disconnect();
                    delete client_connection;
                }
            } /* while */
                /* closes server socket */
                if (server != NULL) {
                    server->disconnect();
                }
        } catch(NL::Exception e) {
            std::cout << "***ERROR*** " << e.what();
            if (client_connection != NULL) {
                client_connection->disconnect();
                delete client_connection;
            }
            if (server != NULL) {
                server->disconnect();
            }
            return EXIT_FAILURE;
        }
    } else { /* if network_commands*/
        /* Offline mode: Start transmission after running the program.
         * Press CTRL+C to stop it. */
        fprintf(fd, "\n\n%s: Running in local ... ", GTIS_COMMAND_NAME);
        fflush(fd);

        //set the handler to interruptions
        std::signal(SIGINT, &sig_int_handler);
        stop_signal_called = false;

        //start transmission thread until stop signal called
        uint64_t tx_time = 0;
        transmission_thread = boost::thread(
            send_from_buffers_to_tx_streamer,
            usrp,
            tx_stream,
            file_buffers,
            tx_time,
            & stop_signal_called);
        std::cout << " Transmitting (press Ctrl + C to stop streaming) . . . " << std::endl;
        transmission_thread.join();
    }

    //finished
    std::cout << std::endl << "Done!" << std::endl;
    return EXIT_SUCCESS;
}
