#include <uhd/utils/thread.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/exception.hpp>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <complex>
#include <csignal>
#include <time.h>
#include <netlink/socket.h>

#include "gtis/uhd_lib.hpp"
#include "gtis/uhd_lib_utils.hpp"
#include "gtis/config.hpp"
#include "gtis/commands.hpp"
#include "gtis/streamer.hpp"

#define GTIS_COMMAND_NAME "gtis_rx"

namespace po = boost::program_options;
   
/***********************************************************************
 * SIG_INT handler: needed to stop acquisition 
 **********************************************************************/ 
static volatile bool stop_signal_called = false;
void sig_int_handler(int) {
    stop_signal_called = true;
}  

/*
 * stop_nmea_writter stops the thread that writes NMEA data to disk
 */
static volatile bool stop_nmea_writter = false;
  
/***********************************************************************
 * Main function
 **********************************************************************/ 
int UHD_SAFE_MAIN(int argc, char *argv[]) {
     
	/* where to print out messages */
	FILE * fd = stdout;
	
	/* Set NON realtime thread priority */
	if (not uhd::set_thread_priority_safe(-1, false)) {
        fprintf(stdout, "Failure in main priority.\n");
    }
    
	/* Variables needed by socket commands */
	NL::Protocol protocol = NL::TCP;
	NL::IPVer ipver = NL::ANY;
	NL::Socket * server = NULL;
	NL::Socket * client_connection = NULL;
	gtis_command_id_t command_id = 0;
	gtis_response_id_t response_id = 0;

	/* variables to be set by po */
    std::string args, 
		rx_antenna_mapping,
        subdevice_specification, 
		reference_clock_source,
        reference_time_source, 
		channels_mapping,
        output_file_name;    
    unsigned long num_requested_samples_per_channel;
    double sampling_rate,
        rx_rf_frequency,
        rx_gain,
        rx_bandwidth;
    size_t number_of_antenna_buffers;
    bool wait_for_key_pressed;
    bool generate_nmea_log;
    unsigned int seconds_between_nmea_measurements;
    unsigned int number_of_trials_gps_locked;
    unsigned int number_of_trials_ref_locked;
    unsigned int number_of_trials_lo_locked;
    unsigned int socket_port_number;
	
	/* Threads */
	boost::thread reception_th;
    boost::thread write_th;
    boost::thread nmea_writter_th;

	/* Command variables */
	bool acquiring_state = false;
    bool network_commands = true;
	
	/* Timed commands */
    // uhd::time_spec_t command_time;
	
	/* GPSDO, REF and LO */
    bool usrp_has_gpsdo_available = false;
    bool usrp_has_ref_available = false;

    /* USRP receive streamer object */
    uhd::rx_streamer::sptr rx_stream = NULL;

	/* Setup the program options */
	//TODO: Update descriptions
	po::options_description desc("Allowed options");
    desc.add_options() 
        ("version", "prints out the version")
        ("help", "help message")
        ("args",
            po::value < std::string > (&args)->default_value(""),
            "passed to uhd::usrp::multi_usrp::make(args)")
        ("sampling_rate",
            po::value < double >(&sampling_rate)->default_value(1e6),
            "sampling_rate of incoming samples")
        ("rx_rf_frequency", po::value <double>(&rx_rf_frequency),
            "RX RF center frequency in Hz")
        ("rx_gain", po::value < double >(&rx_gain),
            "RX gain in dB")
        ("rx_antenna_mapping",
            po::value < std::string > (&rx_antenna_mapping)-> default_value("RX2"),
            "RX antenna mapping. The default value is \"RX2\".")
        ("subdevice_specification",
            po::value < std::string > (&subdevice_specification),
            "daughterboard subdevice specification. See Ettus UHD user manual")
        ("rx_bandwidth", po::value <double>(&rx_bandwidth),
            "RX filter bandwidth in Hz")
        ("reference_clock_source",
            po::value < std::string >(&reference_clock_source)->default_value("internal"),
            "reference clock source. The default value is \"internal\"")
        ("reference_time_source",
            po::value < std::string >(&reference_time_source)->default_value("internal"),
           "reference time source. The default value is \"internal\"")
        ("channels_mapping",
            po::value < std::string > (&channels_mapping)->default_value("0"),
           "specifies what channels are used (specify \"0\", \"1\", \"0,1\", etc)")
        ("socket_port_number",
            po::value <unsigned int>(&socket_port_number)->default_value(5000),
            "socket port number for network commands. The default value is 5000")
        ("net_commands", po::value < bool > (&network_commands)->default_value(true),
            "Listen to network commands (true by default) or execute in local (false)")
        ("number_of_trials_gps_locked",
            po::value <unsigned int >(&number_of_trials_gps_locked)->default_value(10),
            "number of trials to get the GPS locked. The default value is 10")
        ("number_of_trials_ref_locked",
            po::value <unsigned int >(&number_of_trials_ref_locked)->default_value(10),
            "number of trials to get the external REF (10 MHz) locked. The default value is 10")
        ("number_of_trials_lo_locked",
            po::value <unsigned int >(&number_of_trials_lo_locked)->default_value(3),
            "number of trials to get the LO locked. The default value is 3")
        ("output_file_name", po::value < std::string > (&output_file_name),
            "a single output filename for one or the two channels")
        ("num_requested_samples_per_channel",
            po::value <unsigned long>(&num_requested_samples_per_channel)->default_value(0),
            "number of samples to acquire per channel or zero to acquire continously. The default value is 0")
        ("number_of_antenna_buffers",
            po::value <size_t>(&number_of_antenna_buffers)-> default_value(1024),
            "number of temporary buffers. The default value is 1024. Each buffer stores uhd::rx_streamer::get_max_num_samps() samples per channel.")
        ("wait_for_key_pressed",
            po::value < bool > (&wait_for_key_pressed)->default_value(false),
            "acquisition does not start until a key is presed. The default value is false")
        ("generate_nmea_log",
            "when present it generates a log of NMEA records from the GPSDO")
        ("seconds_between_nmea_measurements",
            po::value <unsigned int >(&seconds_between_nmea_measurements)->default_value(1),
           "seconds between consecutive NMEA records from the GPSDO")
         ;
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
	
    if (argc == 1) {
        GTIS_PRINT_VERSION((char *) GTIS_COMMAND_NAME);
        std::cout << desc << std::endl;
        return EXIT_SUCCESS;
    }
     
	/* print the version */
	if (vm.count("version")) {
        GTIS_PRINT_VERSION((char *) GTIS_COMMAND_NAME);
        return EXIT_SUCCESS;
    }
     
	/* print the help message */
	if (vm.count("help")) {
        GTIS_PRINT_VERSION((char *) GTIS_COMMAND_NAME);
        std::cout << desc << std::endl;
        return EXIT_SUCCESS;
    }
     
	/* checks the center frequency parameter */
	if (not vm.count("rx_rf_frequency")) {
        std::cerr <<
            "Please specify the center frequency with --rx_rf_frequency" <<
	    std::endl;
        return EXIT_FAILURE;
    }
     
	/* checks the sampling rate parameter */
	if (not vm.count("sampling_rate")) {
        std::cerr <<
            "Please specify the sampling rate with --sampling_rate" <<
            std::endl;
        return EXIT_FAILURE;
    }
	
    if ((not vm.count("output_file_name")) and (not network_commands)) {
        std::cerr <<
            "At least one of the options --output_file_name and net_commands must be specified" <<
            std::endl;
        return EXIT_FAILURE;
    }

    generate_nmea_log = vm.count("generate_nmea_log");
    
	GTIS_PRINT_VERSION((char *) GTIS_COMMAND_NAME);
     
	/* create a usrp device */
    std::cout << boost::format("Creating the usrp device with: %s...") %
	args << std::endl;
    uhd::usrp::multi_usrp::sptr usrp = uhd::usrp::multi_usrp::make(args);
    
	/* always select the subdevice first, the channel mapping affects 
	 * the other settings */
    if (vm.count("subdevice_specification")) {
		usrp->set_rx_subdev_spec(subdevice_specification);
	}
    std::cout << boost::format("\n Using device: %s") % usrp->get_pp_string() << std::endl;
    std::cout << boost::format(" RX subdevice: %s") % usrp->get_rx_subdev_spec().to_pp_string() << std::endl;
	 
    /* detect the channels to be used */
	std::vector < std::string > channel_strings;
    std::vector < size_t > channel_nums;
    boost::split(channel_strings, channels_mapping,
		boost::is_any_of("\"',"), boost::token_compress_on);
    for (size_t ch = 0; ch < channel_strings.size(); ch++) {
        size_t chan = boost::lexical_cast <int>(channel_strings[ch]);
        if (chan < usrp->get_rx_num_channels()) {
            channel_nums.push_back(boost::lexical_cast<int>(channel_strings[ch]));
        } else {
            throw std::runtime_error("Invalid channel(s) specified.");
        }
    }
	if (vm.count("output_file_name")) {
		std::cout << boost::format("Output file: \"%s\"") % output_file_name << std::endl;
	}
     
    /* Set time and clock source */
    std::cout << boost::format("Setting reference clock source to \"%s\"") %
                 reference_clock_source << std::endl;
    usrp->set_clock_source(reference_clock_source);
    std::cout << boost::format("Setting reference time source to \"%s\"") %
                 reference_time_source << std::endl;
    usrp->set_time_source(reference_time_source);

	/* Waits for GPSDO in case there is one */
	usrp_has_gpsdo_available = GTIS_usrp_has_gpsdo(usrp);
    if ((reference_clock_source == "gpsdo") || 
		(reference_time_source == "gpsdo")) {
		if (usrp_has_gpsdo_available) {
			GTIS_wait_for_usrp_gpsdo_locked(usrp, true,
							 number_of_trials_gps_locked);
		} else {
			std::cout << boost::format("WARNING! gspdo requested but not available.") << std::endl;
		}
    }
     
	/* Waits for the external reference in case it was selected */
	usrp_has_ref_available = GTIS_usrp_has_ref(usrp);
    if (usrp_has_ref_available && 
		((reference_clock_source == "external") || 
		(reference_time_source == "external"))) {
		GTIS_wait_for_usrp_ref_locked(usrp, true,
			number_of_trials_ref_locked);
    }
    GTIS_wait_for_usrp_lo_locked(usrp, true,
		number_of_trials_lo_locked, channel_nums);
     
	/* aligns USRP time with GPSDO (if available) or with the host.
	 * this is important to get precise timestamp as well as NMEA
	 * records. */
	GTIS_align_usrp_time(usrp, true);

    /* Configuration of the USRP with the received command-line parameters. */

    /* Set sampling rate */
    /* Warning! USRP X310 doesn't support this as a timed command*/
        std::cout << boost::format("Setting RX rate to \"%3.3f\" Msample/s") %
                 (sampling_rate / 1.0e6) << std::endl;
    usrp->set_rx_rate(sampling_rate);

    usrp->set_command_time(usrp->get_time_now() + TIME_DELAY_BETWEEN_COMMANDS);
    for (size_t ch = 0; ch < channel_nums.size(); ch++) {

        usrp->set_rx_freq(rx_rf_frequency, channel_nums[ch]);
        if (vm.count("rx_gain")) {
            usrp->set_rx_gain(rx_gain, channel_nums[ch]);
        }
        if (vm.count("rx_bandwidth")) {
            usrp->set_rx_bandwidth(rx_bandwidth, channel_nums[ch]);
        }
        if (vm.count("rx_antenna_mapping")) {
            usrp->set_rx_antenna(rx_antenna_mapping, channel_nums[ch]);
        }
        // usrp->set_rx_agc(false, channel_nums[ch]); //AGC disabled
        // by default
    }
    usrp->clear_command_time();

    GTIS_wait_for_usrp_lo_locked(usrp, true,
        number_of_trials_lo_locked, channel_nums);

    std::cout << boost::format("Actual reference clock source: \"%s\"") %
        usrp->get_clock_source(0) << std::endl;
    std::cout << boost::format("Actual reference time source: \"%s\"") %
        usrp->get_time_source(0) << std::endl;
    std::cout << boost::format("Actual master clock rate: %3.2f Msample/s") %
        (usrp->get_master_clock_rate() / 1e6) << std::endl;
    std::cout << boost::format("Actual RX rate: %3.2f Msample/s") %
        (usrp->get_rx_rate() / 1e6) << std::endl;
    std::cout << boost::format("Actual RX frequency: %4.2f MHz") %
        (usrp->get_rx_freq(channel_nums[0]) / 1e6) << std::endl;
    std::cout << boost::format("Actual RX gain: %2.1f dB") %
        usrp->get_rx_gain(channel_nums[0]) << std::endl;
    std::cout << boost::format("Actual RX bandwidth: %2.1f MHz") %
        (1e-6 * usrp->get_rx_bandwidth(channel_nums[0])) << std::endl;
    std::cout << boost::format("Actual RX antenna mapping: \"%s\"") %
        usrp->get_rx_antenna(channel_nums[0]) << std::endl;
     if (num_requested_samples_per_channel > 0) {
        std::cout << boost::format("Total number of samples to receive: %d")
            %num_requested_samples_per_channel << std::endl;
    }

    /* creates the multichannel buffer */
    /* Detect the maximum number of samples per channel (antenna)
     * the streamer can produce.
     * This is the value we use for the usrp_multichannel_buffer.
     * WARNING! Do not call rx_stream->get_max_num_samps() before configuring
     * all USRP parameters or a warning reporting odd decimation in the filters
     * can be displayed, and such warning is just confusing because it does not
     * correspond to the actual USRP configuration.
     * Therefore, first configure the USRP correctly, then query for the
     * max number of samples per chunk and per channel, finally create the
     * multichannel buffer based on such a value.
     */
    std::string cpu_format = "sc16";
    std::string wire_format = "sc16";
    uhd::stream_args_t stream_args(cpu_format, wire_format);
    stream_args.channels = channel_nums;
    rx_stream = usrp->get_rx_stream(stream_args);

    usrp_multichannel_buffer * buffer_list =
        new usrp_multichannel_buffer(
            number_of_antenna_buffers,
            channel_nums.size(),
            rx_stream->get_max_num_samps());
    std::cout << boost::format("usrp_multichannel_buffer: ") << std::endl;
    std::cout << boost::format("\t number of buffers: %d")  %
                 buffer_list->capacity() << std::endl;
    std::cout << boost::format("\t number of channels per buffer: %d")  %
                 channel_nums.size() << std::endl;
    std::cout << boost::format("\t number of samples per channel: %d") %
                 rx_stream->get_max_num_samps() << std::endl;
    std::cout << boost::format("\t total memory occupied: %f MBytes") %
                 ((buffer_list->capacity_in_bytes()) / (1024.0 * 1024.0))
              << std::endl << std::endl;

	/* Two execution modes: online with network commands or offline */
	if (network_commands) {
		/* Initialize NetLink Sockets library */
		NL::init();
		
	    /* Waiting for commands */
	    bool all_commands_received = false;
		try {
			/* Open server socket to receive the requests from clients */
			server = new NL::Socket(socket_port_number, protocol, ipver, "*");
			server->blocking(true);
			
			/* Loop for receiving commands until finish command is get */
			while (not all_commands_received) {
				fprintf(fd, "\n\n%s: Listening for connection on port number %d ... ",
					GTIS_COMMAND_NAME, socket_port_number);
				fflush(fd);
				
				/* Wait for a client connection */
				client_connection = server->accept();
				client_connection->blocking(true); /* Needed ???? */
				fprintf(fd, "CONNECTED!\n");
				fflush(fd);
			
				/* Wait for a command_id */
				fprintf(fd, "Listening for command id on port number %d ... ", 
					socket_port_number);
				fflush(fd);
				
				if (not gtis_recv_command_id(&command_id, client_connection, NULL)) {
					fprintf(fd, "FAIL\n");
					if (client_connection != NULL) {
						response_id = GTIS_COMMAND_RESPONSE_KO;
						gtis_send_response_id(&response_id, client_connection, NULL);
						fprintf(fd, "Sent response id = \"%d\"\n", response_id);
						fflush(fd);
						client_connection->disconnect();
						continue;
					}
				} else {
					fprintf(fd, "OK\n");
					fprintf(fd, "Received command id = \"%d\"\n", command_id);
					fflush(fd);
				}
				
				/* Processes the received command */
				if (command_id == GTIS_COMMAND_READY) {
					/* GTIS_COMMAND_READY: used for knowing when the node is ready */
					response_id = GTIS_COMMAND_RESPONSE_OK;
					gtis_send_response_id(&response_id, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                    fflush(fd);
				}  else if (command_id == GTIS_COMMAND_FINISH) {
					/* GTIS_COMMAND_FINISH: finishes the node */
					all_commands_received = true;
					if (acquiring_state) {
						stop_signal_called = true;
						reception_th.join();
						write_th.join();
						if (generate_nmea_log && usrp_has_gpsdo_available) {
							nmea_writter_th.join();
						}
						acquiring_state = false;
						stop_signal_called = false;
					}
					response_id = GTIS_COMMAND_RESPONSE_OK;
					gtis_send_response_id(&response_id, client_connection, NULL);
					fprintf(fd, "Sent response id = \"%d\"\n", response_id);
					fflush(fd);
                } else if (command_id == GTIS_COMMAND_GET_NODE_VERSION) {
                    /* GTIS_COMMAND_GET_NODE_VERSION: used to get the GTIS version of the node */
                    response_id = GTIS_COMMAND_RESPONSE_OK;
                    gtis_send_response_id(&response_id, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                    fflush(fd);
                    size_t str_max_length = 255;
                    char str[str_max_length+1];
                    GTIS_get_version_string(str, str_max_length);
                    str[str_max_length] = '\0';
                    size_t str_length = strlen(str);
                    size_t num_bytes_sent = 0;
                    gtis_send_buffer_length((gtis_buffer_length_t *) &str_length,
                                            client_connection, NULL);
                    gtis_send_buffer((unsigned char *) & str, strlen(str),
                                     &num_bytes_sent, client_connection, NULL);
                    fprintf(fd, "Sent gtis version string = \"%s\"", str);
                    fflush(fd);
				} else if (command_id == GTIS_COMMAND_RX_GAIN) {
                    /* GTIS_COMMAND_RX_GAIN: set receiver gain. */
					/* if successful, response includes the actual gain */	
					gtis_response_value_t response_value;
					if (gtis_recv_command_value((gtis_response_value_t *) & rx_gain, 
						client_connection, NULL)) {
						fprintf(fd, " Received rx gain = \"%f\" dB\n", rx_gain);
						
						usrp->set_command_time(usrp->get_time_now() + 
							TIME_DELAY_BETWEEN_COMMANDS);
						for (std::vector < size_t >::size_type ch = 0; 
							ch < channel_nums.size(); ch++) {
							usrp->set_rx_gain(rx_gain, channel_nums[ch]);
						}
						usrp->clear_command_time();
						//boost::this_thread::sleep(boost::posix_time::seconds(1.0));
						fprintf(fd, " Wait until LO is locked . . . ");
						GTIS_wait_for_usrp_lo_locked(
							usrp, true, number_of_trials_lo_locked, channel_nums);
						for (std::vector < size_t >::size_type ch = 0; 
							ch < channel_nums.size(); ch++) {
							rx_gain = usrp->get_rx_gain(channel_nums[ch]);
							fprintf(fd, " Actual RX gain for channel %d: %f dB\n", 
								(int) ch, rx_gain);
						}
						response_id = GTIS_COMMAND_RESPONSE_OK;
						response_value = rx_gain;			
					} else {
						response_id = GTIS_COMMAND_RESPONSE_KO;
						response_value = 0;
					}
					gtis_send_response_id(& response_id, client_connection, NULL);
					gtis_send_response_value(& response_value, client_connection, NULL);	
					fprintf(fd, "Sent response id = \"%d\", value = \"%f\"\n", 
						response_id, response_value);
					fflush(fd);	
				} else if (command_id == GTIS_COMMAND_RX_FREQ) {
                    /* GTIS_COMMAND_RX_FREQ: set receiver carrier frequency*/
					/* if successful, response includes the actual RF frequency */
					gtis_response_value_t response_value;
					if (gtis_recv_command_value((gtis_response_value_t *) & rx_rf_frequency, 
						client_connection, NULL)) {
						fprintf(fd, " Received RX RF frequency = \"%f\" MHz\n", 
							rx_rf_frequency / 1e6);
						
						usrp->set_command_time(usrp->get_time_now() + TIME_DELAY_BETWEEN_COMMANDS);
						for (std::vector < size_t >::size_type ch = 0; 
							ch < channel_nums.size(); ch++) {
							usrp->set_rx_freq(rx_rf_frequency, channel_nums[ch]);
						}
						usrp->clear_command_time();

						fprintf(fd, " Wait until LO is locked . . . ");
						GTIS_wait_for_usrp_lo_locked(
							usrp, true, number_of_trials_lo_locked, channel_nums);
								
						for (std::vector < size_t >::size_type ch = 0; 
							ch < channel_nums.size(); ch++) {
							rx_rf_frequency = usrp->get_rx_freq(channel_nums[ch]);
							fprintf(fd, " Actual RX frequency for channel %d: %f MHz\n", 
								(int) ch, (rx_rf_frequency / 1.0e6));
						}
						response_id = GTIS_COMMAND_RESPONSE_OK;
						response_value = rx_rf_frequency;
					} else {
						response_id = GTIS_COMMAND_RESPONSE_KO;
						response_value = 0;
					}
					gtis_send_response_id(& response_id, client_connection, NULL);
					gtis_send_response_value(& response_value, client_connection, NULL);	
					fprintf(fd, "Sent response id = \"%d\", value = \"%f\"\n", 
						response_id, response_value);
					fflush(fd);	
                } else if (command_id == GTIS_COMMAND_RX_BANDWIDTH) {
                    /* GTIS_COMMAND_RX_BANDWIDTH: set receiver bandwidh. */
                    /* if successful, response includes the actual bandwidth */
                    gtis_response_value_t response_value;
                    if (gtis_recv_command_value((gtis_response_value_t *) & rx_bandwidth,
                        client_connection, NULL)) {
                        fprintf(fd, " Received rx bandwidth = \"%f\" dB\n", rx_bandwidth);

                        usrp->set_command_time(usrp->get_time_now() +
                            TIME_DELAY_BETWEEN_COMMANDS);
                        for (std::vector < size_t >::size_type ch = 0;
                            ch < channel_nums.size(); ch++) {
                            usrp->set_rx_bandwidth(rx_bandwidth, channel_nums[ch]);
                        }
                        usrp->clear_command_time();
                        //boost::this_thread::sleep(boost::posix_time::seconds(1.0));
                        fprintf(fd, " Wait until LO is locked . . . ");
                        GTIS_wait_for_usrp_lo_locked(
                            usrp, true, number_of_trials_lo_locked, channel_nums);
                        for (std::vector < size_t >::size_type ch = 0;
                            ch < channel_nums.size(); ch++) {
                            rx_bandwidth = usrp->get_rx_bandwidth(channel_nums[ch]);
                            fprintf(fd, " Actual RX bandwidth for channel %d: %f MHz\n",
                                (int) ch, rx_bandwidth);
                        }
                        response_id = GTIS_COMMAND_RESPONSE_OK;
                        response_value = rx_bandwidth;
                    } else {
                        response_id = GTIS_COMMAND_RESPONSE_KO;
                        response_value = 0;
                    }
                    gtis_send_response_id(& response_id, client_connection, NULL);
                    gtis_send_response_value(& response_value, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\", value = \"%f\"\n",
                        response_id, response_value);
                    fflush(fd);
				} else if (command_id == GTIS_COMMAND_START_RX) {
					/* GTIS_COMMAND_START_RX: starts acquisition */
					/* Response can be OK or KO if node is already acquiring.
					 * If a fixed number of samples is requested to be acquired,
					 * then the client will be waiting for the OK response until
					 * all samples are written on disk. */
					bool start_new_acquisition = false;
		     
					/* if USRP is not acquiring, set start_new_acquisition flag to true */
					if (not acquiring_state) {
						acquiring_state = true;
						stop_signal_called = false;
						start_new_acquisition = true;
						response_id = GTIS_COMMAND_RESPONSE_OK;
					} else {
						response_id = GTIS_COMMAND_RESPONSE_KO;
					}
					/* if new start_new_acqusition flag, create the reception
					 * and writing threads to perform data acquisition */
					if (start_new_acquisition) {
						if (gtis_recv_buffer_length(
							(gtis_buffer_length_t *) & num_requested_samples_per_channel, 
							client_connection, NULL)) {
							fprintf(fd, "Requested the acquisition of \"%lu\" samples\n", 
								num_requested_samples_per_channel);
							fflush(fd);
							
                            // !!!!!!!!!!!TODO: CHECK THIS!!!!!!!!!!!!
                            if (num_requested_samples_per_channel == 0) {
								/* Disable manual stopping by means of CTRL+C */
								std::signal(SIGINT, SIG_IGN);
								
								/* set the handler for interruptions */
								std::signal(SIGINT, &sig_int_handler);
								stop_signal_called = false;
							}
							
							/* Start receiver (usrp -> host) thread */
							fprintf(fd, "Starting receiver (usrp -> host) thread . . .\n");
                            uint64_t rx_time = 0;
							reception_th = boost::thread(
                                recv_from_rx_streamer_to_usrp_multichannel_buffer,
                                usrp,
                                rx_stream,
								num_requested_samples_per_channel,
                                buffer_list,
                                rx_time,
								wait_for_key_pressed,
								&stop_signal_called,
								network_commands);
							/* Start writter (host -> disk) thread */
							fprintf(fd, "Starting writer (host -> disk) thread . . .\n");
							write_th = boost::thread(
								write_to_file_from_usrp_multichannel_buffer < short >, 
								output_file_name, 
								buffer_list, 
								num_requested_samples_per_channel, 
								&stop_signal_called);
							/* Start NMEA writter if requested */
							if (generate_nmea_log && usrp_has_gpsdo_available) {
								stop_nmea_writter = false;
								fprintf(fd, "Starting NMEA writer (GPSDO -> host -> disk) thread . . .\n");
								nmea_writter_th = boost::thread(
									GTIS_write_nmea_data_from_gpsdo_to_file,
									usrp, output_file_name + ".usrpnmea",
									seconds_between_nmea_measurements,
									&stop_nmea_writter, true);
							}
							fflush(fd);
							/* if we have a limited number of samples to
							 * receive, join threads to complete command */
							if (num_requested_samples_per_channel > 0) {
								reception_th.join();
								acquiring_state = false;
								if (generate_nmea_log && usrp_has_gpsdo_available) {
									stop_nmea_writter = true;
									fprintf(fd, "Stopping NMEA writer thread . . .\n");
									fflush(fd);
									nmea_writter_th.join();
								}
								write_th.join();
							}
						} else {
							response_id = GTIS_COMMAND_RESPONSE_KO;	
						}
					} /* if (start_new_acquisition) */
					gtis_send_response_id(&response_id, client_connection, NULL);
					fprintf(fd, "Sent response id = \"%d\"\n", response_id);
					fflush(fd);
					/*TODO: modify usrp_multichannel_buffer to store the total
					 number of samples produced so that such a value can be
					 sent to the client. Alternatively, it can be obtained from
                     recv_from_rx_streamer_to_usrp_multichannel_buffer, but the maximum number of
					 parameters for boost::thread were already reached.
					 */
                } else if (command_id == GTIS_COMMAND_START_STREAMING_RX) {
                    /* GTIS_COMMAND_START_STREAMING_RX: starts acquisition via network streaming */
                    /* Response can be OK or KO if node is already acquiring.
                     * Only a fixed number of samples can be requested to be acquired */
                    bool start_new_acquisition = false;

                    /* if USRP is not acquiring, set start_new_acquisition flag to true */
                    if (not acquiring_state) {
                        acquiring_state = true;
                        stop_signal_called = false; // NOT SUPPORTED!!!
                        start_new_acquisition = true;
                        response_id = GTIS_COMMAND_RESPONSE_OK;
                    } else {
                        response_id = GTIS_COMMAND_RESPONSE_KO;
                    }

                    /* if new start_new_acqusition flag, create the reception
                     * and writing threads to perform data acquisition */
                    if (start_new_acquisition) {
                        uint64_t rx_time = 0;
                        if (gtis_recv_buffer_length(
                            (gtis_buffer_length_t *) & num_requested_samples_per_channel,
                            client_connection, NULL) &&
                            gtis_recv_uint64(& rx_time, client_connection, NULL)) {

                            fprintf(fd, "Requested the acquisition of \"%lu\" samples\n",
                                num_requested_samples_per_channel);
                            fprintf(fd, "Received rx_time = %lu tics\n", rx_time);
                            fflush(fd);

                            /* Answers to the client inmediately so that the client can
                             * start listening to the streaming of samples. */
                            gtis_send_response_id(&response_id, client_connection, NULL);
                            fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                            fflush(fd);

                            /* Start receiver (usrp -> host) thread */
                            fprintf(fd, "Starting receiver (usrp -> host) thread . . .\n");
                            reception_th = boost::thread(
                                recv_from_rx_streamer_to_usrp_multichannel_buffer,
                                usrp,
                                rx_stream,
                                num_requested_samples_per_channel,
                                buffer_list,
                                rx_time,
                                wait_for_key_pressed,
                                &stop_signal_called,
                                network_commands);
                            /* Start writter (host -> disk) thread */
                            fprintf(fd, "Starting writer (host -> disk) thread . . .\n");
                            write_th = boost::thread(
                                stream_from_usrp_multichannel_buffer < short >,
                                client_connection,
                                buffer_list,
                                num_requested_samples_per_channel,
                                fd,
                                false);

                            /* Start NMEA writter if requested */
                            /* Not Supported Yet*/
                            /* if (generate_nmea_log && usrp_has_gpsdo_available) {
                                stop_nmea_writter = false;
                                fprintf(fd, "Starting NMEA writer (GPSDO -> host -> disk) thread . . .\n");
                                nmea_writter_th = boost::thread(
                                    GTIS_write_nmea_data_from_gpsdo_to_file,
                                    usrp, output_file_name + ".usrpnmea",
                                    seconds_between_nmea_measurements,
                                    &stop_nmea_writter, true);
                            } */
                            fflush(fd);
                            /* if we have a limited number of samples to
                             * receive, join threads to complete command */
                            if (num_requested_samples_per_channel > 0) {
                                reception_th.join();
                                acquiring_state = false;
                                /*
                                if (generate_nmea_log && usrp_has_gpsdo_available) {
                                    stop_nmea_writter = true;
                                    fprintf(fd, "Stopping NMEA writer thread . . .\n");
                                    fflush(fd);
                                    nmea_writter_th.join();
                                } */
                                write_th.join();
                            }
                        } else {
                            fprintf(fd, "ERROR receiving the number of requested samples per channel.\n");
                            response_id = GTIS_COMMAND_RESPONSE_KO;
                            /* Answers to the client */
                            gtis_send_response_id(&response_id, client_connection, NULL);
                            fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                            fflush(fd);

                        }
                    } else {/* if (start_new_acquisition) */
                        /* Answers to the client */
                        gtis_send_response_id(&response_id, client_connection, NULL);
                        fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                        fflush(fd);
                    }

                    /*TODO: modify usrp_multichannel_buffer to store the total
                     number of samples produced so that such a value can be
                     sent to the client. Alternatively, it can be obtained from
                     recv_from_rx_streamer_to_usrp_multichannel_buffer, but the maximum number of
                     parameters for boost::thread were already reached.
                     */
				} else if (command_id == GTIS_COMMAND_STOP_RX) {
					/* GTIS_COMMAND_STOP_RX: stop acquisition and
					 * allows for the acquisition to be restarted */
					/* Response can be OK or KO if node is already stopped */
					if (acquiring_state) {
						stop_signal_called = true;
						reception_th.join();
						write_th.join();
						if (generate_nmea_log && usrp_has_gpsdo_available) {
							stop_nmea_writter = true;
							fprintf(fd, "Stopping NMEA writer thread . . .\n");
							fflush(fd);
							nmea_writter_th.join();
						}
						acquiring_state = false;
						stop_signal_called = false;
						response_id = GTIS_COMMAND_RESPONSE_OK;
					} else {
						response_id = GTIS_COMMAND_RESPONSE_KO;
					}
					gtis_send_response_id(&response_id, client_connection, NULL);
					fprintf(fd, "Sent response id = \"%d\"\n", response_id);
					fflush(fd);	
				} else if (command_id == GTIS_COMMAND_SET_RX_FILE) {
					/* GTIS_COMMAND_SET_RX_FILE: set filename for RX signals
					 * Filename is received from the client.*/
					gtis_buffer_length_t temp_buffer_size = 0;
					bool response_ok = true;
					if (gtis_recv_buffer_length(& temp_buffer_size, 
						client_connection, NULL)) {
						fprintf(fd, "Received file name length = \"%lu\"\n", 
							temp_buffer_size);
						fflush(fd);
						char temp_buffer[temp_buffer_size + 1];
						size_t num_bytes_received = 0;
						fprintf(fd, "Reading the file name ...\n");
						if (not gtis_recv_buffer((char *) &temp_buffer, (size_t) temp_buffer_size,
							&num_bytes_received, client_connection, NULL)) {
							fprintf(fd, "Error reading the buffer\n");
							fflush(fd);
							response_ok = false;
						} else {
							fprintf(fd, "num_bytes_received = %ld\n", num_bytes_received);
						}
						temp_buffer[temp_buffer_size] = '\0';
						output_file_name = std::string(temp_buffer);
						fprintf(fd, "Received file name = \"%s\"\n", output_file_name.c_str());					
						response_id = response_ok ? GTIS_COMMAND_RESPONSE_OK : GTIS_COMMAND_RESPONSE_KO;
					} else {
						response_id = GTIS_COMMAND_RESPONSE_KO;	
					}
					gtis_send_response_id(&response_id, client_connection, NULL);
					fprintf(fd, "Sent response id = \"%d\"\n", response_id);
					fflush(fd);	
				} else if (command_id == GTIS_COMMAND_RX_ANTENNA_MAPPING) {
					/* GTIS_COMMAND_RX_ANTENNA_MAPPING: specify antenna mapping
					 * at the receiver side. */
					gtis_buffer_length_t temp_buffer_size = 0;
					bool response_ok = true;
					if (gtis_recv_buffer_length(& temp_buffer_size, 
						client_connection, NULL)) {
						fprintf(fd, "Received RX antenna mapping length = \"%lu\"\n", 
							temp_buffer_size);
						fflush(fd);
						char temp_buffer[temp_buffer_size + 1];
						size_t num_bytes_received = 0;
						fprintf(fd, "Reading the RX antenna mapping ...\n");
						if (not gtis_recv_buffer((char *) &temp_buffer, (size_t) temp_buffer_size,
							&num_bytes_received, client_connection, NULL)) {
							fprintf(fd, "Error reading the buffer\n");
							fflush(fd);
							response_ok = false;
                        }
						temp_buffer[temp_buffer_size] = '\0';
						rx_antenna_mapping = std::string(temp_buffer);
						fprintf(fd, "Received RX antenna mapping = \"%s\"\n", 
							rx_antenna_mapping.c_str());
                        fprintf(fd, "Setting RX antenna mapping for all channels = \"%s\" ...\n",
							rx_antenna_mapping.c_str());
						usrp->set_command_time(usrp->get_time_now() + 
							TIME_DELAY_BETWEEN_COMMANDS);
						for (std::vector < size_t >::size_type ch = 0;
							ch < channel_nums.size(); ch++) {
							usrp->set_rx_antenna(rx_antenna_mapping, channel_nums[ch]);
						}
						usrp->clear_command_time();
		     
                        /* Needed by USRP B210 (not documented by Ettus) */
						GTIS_sleep_current_thread(2, false);
                        fprintf(fd, " Wait until LO is locked . . . ");
						GTIS_wait_for_usrp_lo_locked(usrp, true,
						    number_of_trials_lo_locked, channel_nums);
						for (std::vector < size_t >::size_type ch = 0;
							ch < channel_nums.size(); ch++) {
							rx_antenna_mapping =
								usrp->get_rx_antenna(channel_nums[ch]);
                            fprintf(fd, "Actual RX antenna mapping for channel %lu = \"%s\" ...\n",
								ch, rx_antenna_mapping.c_str());
						}
						response_id = response_ok ? GTIS_COMMAND_RESPONSE_OK : GTIS_COMMAND_RESPONSE_KO;
					} else {
						response_id = GTIS_COMMAND_RESPONSE_KO;	
					}
					gtis_send_response_id(&response_id, client_connection, NULL);
					fprintf(fd, "Sent response id = \"%d\"\n", response_id);
					fflush(fd);			    
                } else if (command_id == GTIS_COMMAND_GET_NODE_TIME) {
                    /* GTIS_COMMAND_GET_NODE_TIME: get the USRP last pps time */
                    uint64_t waitForPPSEdge = 0;
                    if (not gtis_recv_uint64(& waitForPPSEdge,
                        client_connection, NULL)) {
                        response_id = GTIS_COMMAND_RESPONSE_KO;
                    } else {
                        response_id = GTIS_COMMAND_RESPONSE_OK;
                    }
                    gtis_send_response_id(&response_id, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                    fflush(fd);
                    if (waitForPPSEdge >= 1) {
                        GTIS_wait_for_pps_edge(usrp);
                    }
                    uhd::time_spec_t time_last_pps = usrp->get_time_last_pps();
                    uint64_t usrp_time = (uint64_t) time_last_pps.to_ticks(1.0);
                    gtis_send_uint64(&usrp_time, client_connection, NULL);
                    fprintf(fd, "Sent USRP time last PPS: %lu s\n", usrp_time);
                    fflush(fd);
                } else if (command_id == GTIS_COMMAND_TIMED_CONFIG_RX) {
                    /* GTIS_COMMAND_TIMED_CONFIG_RX: configure USRP at given time instant */
                    /* Response can be OK or KO if node is already receiving */
                    uint64_t config_time = 0;
                    if (not gtis_recv_uint64(& config_time,
                        client_connection, NULL)) {
                        response_id = GTIS_COMMAND_RESPONSE_KO;
                        gtis_send_response_id(&response_id, client_connection, NULL);
                        fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                        fprintf(fd, "Missing configuration time!\n");
                        fflush(fd);
                    } else {
                        fprintf(fd, "Received config_time = %lu tics\n", config_time);
                        if (acquiring_state) {
                            response_id = GTIS_COMMAND_RESPONSE_KO;
                            gtis_send_response_id(&response_id, client_connection, NULL);
                            fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                            fprintf(fd, "Cannot configure device while acquiring.\n");
                            fflush(fd);
                        } else {
                            /* Reply inmediately to the client */
                            response_id = GTIS_COMMAND_RESPONSE_OK;
                            gtis_send_response_id(&response_id, client_connection, NULL);
                            fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                            fflush(fd);
                        }

                        fprintf(fd, "Timed configuration will start at %lu, current time is %lu ...\n",
                                config_time, (uint64_t) usrp->get_time_now().to_ticks(1.0));

                        uint64_t time_now = 0;
                        while (time_now > (config_time - 1)) {
                            time_now = (uint64_t) GTIS_wait_for_pps_edge(usrp).to_ticks(1.0);
                        }

                        fprintf(fd, " Aligning USRP time ... ");
                        fflush(fd);
                        if (GTIS_align_usrp_time(usrp, true)) {
                            fprintf(fd, "OK\n");
                        } else {
                            fprintf(fd, "FAIL\n");
                        }

                        uint64_t secs_between_commands = 1;
                        config_time = secs_between_commands + usrp->get_time_last_pps().to_ticks(1.0);

                        for (size_t ch = 0; ch < channel_nums.size(); ch++) {
                            fprintf(fd, "Setting RX RF frequency for channel %lu to %4.2f MHz at time %lu ...\n",
                                    channel_nums[ch], rx_rf_frequency, config_time);
                            fprintf(fd, "Setting RX gain for channel %lu to %2.1f dB at time %lu ...\n",
                                    channel_nums[ch], rx_gain, config_time);
                            fprintf(fd, "Setting RX bandwidth for channel %lu to %3.2f MHz at time %lu ...\n",
                                    channel_nums[ch], rx_bandwidth, config_time);
                            fprintf(fd, "Setting RX antenna mapping for channel %lu to \"%s\" at time %lu ...\n",
                                    channel_nums[ch], rx_antenna_mapping.c_str(), config_time);
                        }
                        fflush(fd);

                        for (size_t ch = 0; ch < channel_nums.size(); ch++) {
                            /* Code from tx_bursts.cpp */
                            //uhd::tune_request_t tune_request(tx_rf_frequency);
                            //usrp->set_tx_freq(tune_request, channel_nums[ch]);
                            /* Code from tx_bursts.cpp */

                            if (vm.count("rx_rf_frequency")) {
                                usrp->set_rx_freq(rx_rf_frequency, channel_nums[ch]);
                            }
                            if (vm.count("rx_gain")) {
                                usrp->set_rx_gain(rx_gain, channel_nums[ch]);
                            }
                            if (vm.count("rx_bandwidth")) {
                                usrp->set_rx_bandwidth(rx_bandwidth, channel_nums[ch]);
                            }
                            if (vm.count("rx_antenna_mapping")) {
                                usrp->set_rx_antenna(rx_antenna_mapping, channel_nums[ch]);
                            }
                        }
                        usrp->clear_command_time();

                        for (size_t ch = 0; ch < channel_nums.size(); ch++) {
                            fprintf(fd, "Actual RX RF frequency for channel %lu: %4.2f MHz ...\n",
                                    channel_nums[ch], usrp->get_rx_freq(channel_nums[ch]));
                            fprintf(fd, "Actual RX gain for channel %lu: %2.1f dB\n",
                                    channel_nums[ch], usrp->get_rx_gain(channel_nums[ch]));
                            fprintf(fd, "Actual RX bandwidth for channel %lu: %3.2f MHz\n",
                                    channel_nums[ch], usrp->get_rx_bandwidth(channel_nums[ch]));
                            fprintf(fd, "Actual RX antenna mapping for channel %lu: \"%s\"\n",
                                    channel_nums[ch], usrp->get_rx_antenna(channel_nums[ch]).c_str());
                        }
                        fflush(fd);


                        /* Set sampling rate */
                        /* Warning! USRP X310 doesn't support this as a timed command*/
                        GTIS_wait_for_pps_edge(usrp);
                        config_time = secs_between_commands + usrp->get_time_last_pps().to_ticks(1.0);
                        fprintf(fd, "Setting RX signal sampling rate to %3.3f Msample/s at time %lu ...\n",
                            (sampling_rate / 1.0e6), config_time);
                        fflush(fd);
                        usrp->set_command_time(uhd::time_spec_t::from_ticks((long long) config_time, 1.0));
                        usrp->set_rx_rate(sampling_rate);
                        usrp->clear_command_time();
                        fprintf(fd, "Actual RX signal sampling rate: %3.3f Msample/s\n",
                            (usrp->get_rx_rate() / 1e6));
                        fflush(fd);

                        GTIS_wait_for_usrp_lo_locked(usrp, true,
                            number_of_trials_lo_locked, channel_nums);

                        /* creates receive streamer */

                        GTIS_wait_for_pps_edge(usrp);
                        config_time = secs_between_commands + usrp->get_time_last_pps().to_ticks(1.0);
                        fprintf(fd, "Getting RX streamer at time %lu ... ", config_time);
                        fflush(fd);
                        uhd::stream_args_t stream_args(cpu_format, wire_format);
                        stream_args.channels = channel_nums;
                        usrp->set_command_time(uhd::time_spec_t::from_ticks((long long) config_time, 1.0));
                        rx_stream = usrp->get_rx_stream(stream_args);
                        usrp->clear_command_time();
                        fprintf(fd, "Finished GTIS_COMMAND_TIMED_CONFIG_RX\n");
                        fflush(fd);
                    }
                } else {
                    fprintf(fd, "Invalid command!\n");
                    response_id = GTIS_COMMAND_RESPONSE_KO;
                    gtis_send_response_id(&response_id, client_connection, NULL);
                    fprintf(fd, "Sent response id = \"%d\"\n", response_id);
                    fflush(fd);
                }
				
				/* closes connection */
				if (client_connection != NULL) {
					client_connection->disconnect();
					delete client_connection;
				}
			} /* while */
			/* closes server socket */
			if (server != NULL) {
				server->disconnect();
			}
		} catch(NL::Exception e) {
			std::cout << "***ERROR*** " << e.what();
			if (client_connection != NULL) {
				client_connection->disconnect();
				delete client_connection;			
			}
			if (server != NULL) {
				server->disconnect();
			}
            return EXIT_FAILURE;
		}
	}
     
	// Offline mode: Start transmission after running the program.
	// Press CRTL+C to stop it.
	else {			/* if network commands */

	 std::cout << "Executing RX in local" << std::endl;
	 /* Disable manual stopping by means of CTRL+C */
	std::signal(SIGINT, SIG_IGN);
	 std::signal(SIGINT, &sig_int_handler);
	 stop_signal_called = false;
	 write_th = boost::thread(write_to_file_from_usrp_multichannel_buffer < short >, output_file_name, buffer_list, num_requested_samples_per_channel, &stop_signal_called);	// Writer 
																							// thread 
																							// initialization
	if (num_requested_samples_per_channel > 0) {
	    std::
		cout <<
		boost::format
		("MAIN: num_requested_samples_per_channel = %d") 
		%num_requested_samples_per_channel << std::endl;
	}
	
	    // Writes to file NMEA records if GPSDO is available
	    if (generate_nmea_log && usrp_has_gpsdo_available) {
	    stop_nmea_writter = false;
	    std::
		cout <<
		boost::format
		("@@@@@@@@@@@@@ Starting NMEA_WRITTER thread . . .\n");
	    nmea_writter_th =
		boost::thread(GTIS_write_nmea_data_from_gpsdo_to_file,
			      usrp, output_file_name + ".usrpnmea",
			      seconds_between_nmea_measurements,
			      &stop_nmea_writter, true);
	}
    uint64_t rx_time = 0;
	reception_th =
        boost::thread(recv_from_rx_streamer_to_usrp_multichannel_buffer, usrp,
              rx_stream,
			  num_requested_samples_per_channel,
              buffer_list,
              rx_time,
			  wait_for_key_pressed, &stop_signal_called,
			  network_commands);
	 reception_th.join();
	if (generate_nmea_log && usrp_has_gpsdo_available) {
	    stop_nmea_writter = true;
	    std::
		cout <<
		boost::format
		("@@@@@@@@@@@@@ Stopping NMEA_WRITTER thread . . .\n");
	    nmea_writter_th.join();
	}
	write_th.join();
    }
      
	// finished
	delete buffer_list;
    std::cout << std::endl << "Done!" << std::endl << std::endl;
     return EXIT_SUCCESS;
}


