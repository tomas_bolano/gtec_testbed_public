#include "gtis/commands.hpp"

bool is_little_endian() {
	int num = 1;
	return ((*(char *)&num == 1));
}


bool gtis_recv_buffer(char * buffer, size_t num_bytes_to_receive, 
		size_t * num_bytes_received, NL::Socket * client_connection, FILE * fd) {	
	if (fd != NULL) {
		fprintf(fd, "num_bytes_to_receive = %ld\n", num_bytes_to_receive);
	}
	*num_bytes_received = 0;
	size_t buffer_index = 0;
	try {
		/* reads on a byte basis assuming big endian network order */
		while (*num_bytes_received < num_bytes_to_receive) {
			if (client_connection->read((void *) &buffer[buffer_index], 1) != 1) {
				// ERROR***
				if (fd != NULL) {
					fprintf(fd, "ERROR reading the command id\n");
				}
				return false;
			} else {
				(*num_bytes_received)++;
				buffer_index++;
			}
		}
					
		if (fd != NULL) {
			fprintf(fd, "Readed %ld byte(s) in total\n",  *num_bytes_received);
			fflush(fd);
		}		
		return true;
	} catch(NL::Exception e) {
		fprintf(fd, "NL error: %s\n", e.what());
		return false;
	}
}

bool gtis_send_buffer(unsigned char * buffer, size_t num_bytes_to_send,
                      size_t * num_bytes_sent, NL::Socket * client_connection, FILE * fd) {
    if (fd != NULL) {
        fprintf(fd, "gtis_send_buffer(): num_bytes_to_send = %lu\n", num_bytes_to_send);
    }
    *num_bytes_sent = 0;
    int buffer_index = 0;
    try {
        while (*num_bytes_sent < num_bytes_to_send) {
            client_connection->send((void *) &buffer[buffer_index], 1);
            (*num_bytes_sent)++;
            if (fd != NULL) {
                fprintf(fd, "gtis_send_buffer(): buffer[%d] = %d\n", buffer_index, buffer[buffer_index]);
                fprintf(fd, "gtis_send_buffer(): sent %lu byte(s) in total\n", *num_bytes_sent);
                fflush(fd);
            }
            buffer_index++;
        }
        return true;
    } catch(NL::Exception e) {
        fprintf(fd, "NL error: %s\n", e.what());
        return false;
    }
}

bool gtis_recv_uint16(uint16_t * value, 
	NL::Socket * client_connection, FILE * fd) {
	
	size_t num_bytes_received = 0;
	if (not gtis_recv_buffer((char *) value, sizeof(*value), 
		&num_bytes_received, client_connection, fd)) {						
		if (fd != NULL) {
			fprintf(fd, "Error reading length of the filename buffer\n");
		}
		*value = 0;
		return false;
	} else {
		if (fd != NULL) {		
			fprintf(fd, "num_bytes_received = %ld\n", num_bytes_received);
		}
		*value = ntohs(*value);
		return true;
	}
}

bool gtis_recv_command_id(gtis_command_id_t * command_id, 
	NL::Socket * client_connection, FILE * fd) {
	return gtis_recv_uint16((uint16_t *) command_id, client_connection, fd);
}

bool gtis_send_response_id(gtis_response_id_t * response_id, 
	NL::Socket * client_connection, FILE * fd) {
    return gtis_send_uint16((uint16_t *) response_id, client_connection, fd);
}

bool gtis_send_response_value(gtis_response_value_t * response_value, NL::Socket * client_connection, FILE * fd) {
    return gtis_send_double((double *) response_value, client_connection, fd);
}

bool gtis_recv_double(double * value, 
	NL::Socket * client_connection, FILE * fd) {
	unsigned int num_received_bytes = 0;
	unsigned int num_bytes_to_receive = (unsigned int) sizeof(*value);
	if (fd != NULL) {
		fprintf(fd, "num_bytes_to_receive = %d\n", num_bytes_to_receive);
		fflush(fd);
	}
	unsigned char buffer[num_bytes_to_receive];				
	int buffer_index = is_little_endian() ? num_bytes_to_receive - 1 : 0;
	try {
		while (num_received_bytes < num_bytes_to_receive) {
			if (client_connection->read((void *) &buffer[buffer_index], 1) != 1) {
				// ERROR***
				if (fd != NULL) {
					fprintf(fd, "ERROR reading the double value\n");
				}
				return false;
			} else {
				if (fd != NULL) {
					fprintf(fd, "buffer[%d] = %d\n", buffer_index, buffer[buffer_index]);
					fflush(fd);
				}
				num_received_bytes++;
				is_little_endian() ? buffer_index-- : buffer_index++;
			}
			if (fd != NULL) {
				fprintf(fd, "Readed %d byte(s) in total\n", num_received_bytes);
				fflush(fd);
			}
		}
		memcpy(value, buffer, sizeof(*value));
		if (fd != NULL) {
			fprintf(fd, "double value = %f\n", *value);
			fflush(fd);
		}
		return true;
	} catch(NL::Exception e) {
		fprintf(fd, "NL error: %s\n", e.what());
		return false;
	}		
}

bool gtis_send_double(double * value, NL::Socket * client_connection, FILE * fd) {
    unsigned int num_sent_bytes = 0;
    unsigned int num_bytes_to_send = (unsigned int) sizeof(*value);
    if (fd != NULL) {
        fprintf(fd, "num_bytes_to_send = %d\n", num_bytes_to_send);
        fflush(fd);
    }
    unsigned char buffer[num_bytes_to_send];
    memcpy(buffer, value, sizeof(*value));
    int buffer_index = is_little_endian() ? num_bytes_to_send - 1 : 0;
    try {
        while (num_sent_bytes < num_bytes_to_send) {
            client_connection->send((void *) &buffer[buffer_index], 1);
            num_sent_bytes++;
            if (fd != NULL) {
                fprintf(fd, "buffer[%d] = %d\n", buffer_index, buffer[buffer_index]);
                fprintf(fd, "Sent %d byte(s) in total\n", num_sent_bytes);
                fflush(fd);
            }
            is_little_endian() ? buffer_index-- : buffer_index++;
        }
        return true;
    } catch(NL::Exception e) {
        fprintf(fd, "NL error: %s\n", e.what());
        return false;
    }
}

bool gtis_recv_command_value(gtis_response_value_t * command_value, 
	NL::Socket * client_connection, FILE * fd) {
	return gtis_recv_double((double *) command_value, client_connection, fd);	
}

bool gtis_recv_uint64(uint64_t * value, 
	NL::Socket * client_connection, FILE * fd) {
	
	size_t num_bytes_received = 0;
	if (not gtis_recv_buffer((char *) value, sizeof(*value), 
		&num_bytes_received, client_connection, fd)) {						
		if (fd != NULL) {
            fprintf(fd, "Error reading uint64\n");
		}
		*value = 0;
		return false;
	} else {
		if (fd != NULL) {		
			fprintf(fd, "num_bytes_received = %ld\n", num_bytes_received);
		}
		*value = be64toh(*value);
		return true;
	}
}

bool gtis_recv_buffer_length(gtis_buffer_length_t * buffer_length, 
	NL::Socket * client_connection, FILE * fd) {
	return gtis_recv_uint64((uint64_t *) buffer_length, 
		client_connection, fd);
}

bool gtis_send_uint64(uint64_t * value,
    NL::Socket * client_connection, FILE * fd) {

    uint64_t value_in_network_order = htobe64(*value);
    size_t num_sent_bytes = 0;
    bool result = gtis_send_buffer(
                (unsigned char *) & value_in_network_order,
                sizeof(value_in_network_order),
                &num_sent_bytes,
                client_connection,
                fd);
    if (fd != NULL) {
        fprintf(fd, "num_sent_bytes = %lu\n", num_sent_bytes);
    }
    return result;
}

bool gtis_send_buffer_length(gtis_buffer_length_t * buffer_length,
    NL::Socket * client_connection, FILE * fd) {
    return gtis_send_uint64((uint64_t *) buffer_length,
        client_connection, fd);
}

bool gtis_send_uint16(uint16_t * value,
    NL::Socket * client_connection, FILE * fd) {

    uint16_t value_in_network_order = htons(*value);
    size_t num_sent_bytes = 0;
    bool result = gtis_send_buffer(
                (unsigned char *) & value_in_network_order,
                sizeof(value_in_network_order),
                &num_sent_bytes,
                client_connection,
                fd);
    if (fd != NULL) {
        fprintf(fd, "gtis_send_buffer_uint16(): num_sent_bytes = %lu\n", num_sent_bytes);
    }
    return result;
}

