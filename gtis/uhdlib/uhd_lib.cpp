#include "gtis/uhd_lib.hpp"
#include "gtis/uhd_lib_utils.hpp"

void recv_from_rx_streamer_to_usrp_multichannel_buffer(
        uhd::usrp::multi_usrp::sptr usrp,
        uhd::rx_streamer::sptr rx_stream,
        unsigned long num_requested_samples_per_channel,
        usrp_multichannel_buffer * buffer_list,
        uint64_t rx_time,
        bool wait_for_key_pressed,
        volatile bool * stop_signal_called,
        bool network_commands) {

  // Set realtime Thread priority
  bool priority_recv = uhd::set_thread_priority_safe(1,true);

  // If not possible to stablish priority, notice the error on screen
  if (not priority_recv){
    std::cout << "failure in recv priority" << std::endl;
  }

  try {

    size_t rx_stream_max_num_samples = rx_stream->get_max_num_samps();
    unsigned long num_acquired_samples_per_channel = 0;
    
    // Declaration and initialization of usrp buffer, used to receive data through recv command.
    std::vector < std::complex < short > * > usrp_buffer(buffer_list->number_of_channels());
    

    uhd::rx_metadata_t md;
    
    // First, acquire the first packet to start reception ...
    //EXPERIMENTAL!!!!
    //uhd::stream_cmd_t stream_cmd(uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS);
    uhd::stream_cmd_t stream_cmd((num_requested_samples_per_channel == 0)?
        uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS:
        uhd::stream_cmd_t::STREAM_MODE_NUM_SAMPS_AND_DONE
    );

     
    //stream_cmd.num_samps = 0;
    //stream_cmd.stream_now = false;
    

    stream_cmd.num_samps = size_t(num_requested_samples_per_channel);
    stream_cmd.stream_now = (num_requested_samples_per_channel == 0);

    //std::cout << boost::format("recv_to_usrp_multichannel_buffer: num_requested_samples_per_channel = %d") % num_requested_samples_per_channel << std::endl;

    // Demand a reference to produce data (write a position with the information received)
    buffer_list->reference_buffer_for_producer( &usrp_buffer);

    // If wait command, wait to enter command to start acquiring. This parameter is set in command options
    if(wait_for_key_pressed){
      std::cout << boost::format("Press ENTER to start acquiring ...") << std::endl;
      std::cin.get();
    }

    /* Code from rx_samples_to_file.cpp */
    //stream_cmd.time_spec = uhd::time_spec_t();
    //rx_stream->issue_stream_cmd(stream_cmd);
    /* End of code from rx_samples_to_file.cpp */

    
    // // stream_cmd.time_spec = usrp->get_time_now() + uhd::time_spec_t(0.0, 0.5);
    //stream_cmd.time_spec = usrp->get_time_now() + uhd::time_spec_t(0.05);

    if (rx_time == 0) {
        stream_cmd.time_spec = usrp->get_time_now() + uhd::time_spec_t(0.0, 0.001);
    } else {
        fprintf(stdout, "\nrx_time = %lu\n", rx_time);
        fflush(stdout);
        stream_cmd.time_spec =
                uhd::time_spec_t::from_ticks((long long) rx_time, 1.0);
    }

    rx_stream->issue_stream_cmd(stream_cmd);
    

    if ((not network_commands) and (num_requested_samples_per_channel == 0)) {
      std::cout << " Acquiring samples (press Ctrl + C to stop) . . ." << std::endl;
    } /*else {
      if (num_requested_samples_per_channel > 0) {
        std::cout << std::endl << boost::format(" Acquiring %d samples . . . ") %
          num_requested_samples_per_channel << std::endl << std::endl;
      } else {
        std::cout << std::endl << " Acquiring samples . . . " << std::endl << std::endl;
      }
    }*/

    size_t num_rx_samps = rx_stream->recv(usrp_buffer, rx_stream_max_num_samples, md, 3.0);
    if (num_rx_samps > 0) {
      buffer_list->produce(num_rx_samps);
      num_acquired_samples_per_channel += num_rx_samps;

      // Stores the timestamp of the first chunk of samples in the buffer_list
      buffer_list->set_utc_timestamp_first(md.time_spec.get_full_secs());
    }

    
    //std::cout << boost::format("** num_rx_samps = %d, num_acquired_samples_per_channel = %d, num_requested_samples_per_channel = %d ") %
    //      num_rx_samps % num_acquired_samples_per_channel % num_requested_samples_per_channel << std::endl;


    // ... and then capture data until stop signal called or maximum number of samples reached
    while ((not (*stop_signal_called)) and
           (num_requested_samples_per_channel > num_acquired_samples_per_channel
            or num_requested_samples_per_channel == 0)) {
      buffer_list->reference_buffer_for_producer( &usrp_buffer);
      num_rx_samps = rx_stream->recv(usrp_buffer, rx_stream_max_num_samples, md, 1.0);

      if (num_rx_samps > 0) {
        buffer_list->produce(num_rx_samps);
        num_acquired_samples_per_channel += num_rx_samps;
      }

      //std::cout << boost::format("   num_rx_samps = %d, num_acquired_samples_per_channel = %d, num_requested_samples_per_channel = %d ") %
      //    num_rx_samps % num_acquired_samples_per_channel % num_requested_samples_per_channel << std::endl;
    }

    // Stores the timestamp of the last chunk of samples in the buffer_list
    buffer_list->set_utc_timestamp_last(md.time_spec.get_full_secs());

    // Last packet to stop acquisition
    std::cout << boost::format("Acquired %d samples per channel.")
      % num_acquired_samples_per_channel << std::endl << std::endl;
    stream_cmd = uhd::stream_cmd_t(uhd::stream_cmd_t::STREAM_MODE_STOP_CONTINUOUS);
    stream_cmd.num_samps = 0;
    stream_cmd.stream_now = false;
    stream_cmd.time_spec = usrp->get_time_now() + uhd::time_spec_t(0.0, 0.1);
    rx_stream->issue_stream_cmd(stream_cmd);

  } catch (boost::thread_interrupted e) { // boost::thread_interrupted //TODO: get better
    std::cout << " @@@ reception thread interrupted " << std::endl;
    return;
    // return ~0;
  }

}

void recv_to_usrp_multichannel_buffer(
        uhd::usrp::multi_usrp::sptr usrp,
        const std::vector < size_t > &channel_nums,
        size_t number_of_samples_per_channel,
        unsigned long num_requested_samples_per_channel,
        usrp_multichannel_buffer * buffer_list,
        bool wait_for_key_pressed,
        volatile bool * stop_signal_called,
        bool network_commands) {

  // Set realtime Thread priority
  bool priority_recv = uhd::set_thread_priority_safe(1,true);

  // If not possible to stablish priority, notice the error on screen
  if (not priority_recv){
    std::cout << "failure in recv priority" << std::endl;
  }

  try {

    std::string cpu_format = "sc16";
    std::string wire_format = "sc16";

    unsigned long num_acquired_samples_per_channel = 0;

    // Declaration and initialization of usrp buffer, used to receive data through recv command.
    std::vector < std::complex < short > * > usrp_buffer(buffer_list->number_of_channels());

    //create a receive streamer
    uhd::stream_args_t stream_args(cpu_format, wire_format);

    // Stablish the number of channels in rx_streamer arguments
    stream_args.channels = channel_nums;

    // Obtain the rx_stream
    uhd::rx_streamer::sptr rx_stream = usrp->get_rx_stream(stream_args);

    //std::cout << boost::format("rx_stream->get_max_num_samps() = %d") % rx_stream->get_max_num_samps() << std::endl;

    // TODO: Try without this
    /*std::cout << "Sleeping 1 second for configuration . . . ";
    boost::this_thread::sleep(boost::posix_time::seconds(1.0));
    std::cout << "OK" << std::endl;*/

    uhd::rx_metadata_t md;

    // First, acquire the first packet to start reception ...
    //EXPERIMENTAL!!!!
    //uhd::stream_cmd_t stream_cmd(uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS);
    uhd::stream_cmd_t stream_cmd((num_requested_samples_per_channel == 0)?
        uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS:
        uhd::stream_cmd_t::STREAM_MODE_NUM_SAMPS_AND_DONE
    );


    //stream_cmd.num_samps = 0;
    //stream_cmd.stream_now = false;


    stream_cmd.num_samps = size_t(num_requested_samples_per_channel);
    stream_cmd.stream_now = (num_requested_samples_per_channel == 0);

    //std::cout << boost::format("recv_to_usrp_multichannel_buffer: num_requested_samples_per_channel = %d") % num_requested_samples_per_channel << std::endl;

    // Demand a reference to produce data (write a position with the information received)
    buffer_list->reference_buffer_for_producer( &usrp_buffer);

    // If wait command, wait to enter command to start acquiring. This parameter is set in command options
    if(wait_for_key_pressed){
      std::cout << boost::format("Press ENTER to start acquiring ...") << std::endl;
      std::cin.get();
    }

    /* Code from rx_samples_to_file.cpp */
    //stream_cmd.time_spec = uhd::time_spec_t();
    //rx_stream->issue_stream_cmd(stream_cmd);
    /* End of code from rx_samples_to_file.cpp */


    //stream_cmd.time_spec = usrp->get_time_now() + uhd::time_spec_t(0.0, 0.5);
    stream_cmd.time_spec = usrp->get_time_now() + uhd::time_spec_t(0.05);
    rx_stream->issue_stream_cmd(stream_cmd);


    if ((not network_commands) and (num_requested_samples_per_channel == 0)) {
      std::cout << " Acquiring samples (press Ctrl + C to stop) . . ." << std::endl;
    } /*else {
      if (num_requested_samples_per_channel > 0) {
        std::cout << std::endl << boost::format(" Acquiring %d samples . . . ") %
          num_requested_samples_per_channel << std::endl << std::endl;
      } else {
        std::cout << std::endl << " Acquiring samples . . . " << std::endl << std::endl;
      }
    }*/

    size_t num_rx_samps = rx_stream->recv(usrp_buffer, number_of_samples_per_channel, md, 3.0);
    if (num_rx_samps > 0) {
      buffer_list->produce(num_rx_samps);
      num_acquired_samples_per_channel += num_rx_samps;

      // Stores the timestamp of the first chunk of samples in the buffer_list
      buffer_list->set_utc_timestamp_first(md.time_spec.get_full_secs());
    }


    //std::cout << boost::format("** num_rx_samps = %d, num_acquired_samples_per_channel = %d, num_requested_samples_per_channel = %d ") %
    //      num_rx_samps % num_acquired_samples_per_channel % num_requested_samples_per_channel << std::endl;


    // ... and then capture data until stop signal called or maximum number of samples reached
    while ((not (*stop_signal_called)) and
           (num_requested_samples_per_channel > num_acquired_samples_per_channel
            or num_requested_samples_per_channel == 0)) {
      buffer_list->reference_buffer_for_producer( &usrp_buffer);
      num_rx_samps = rx_stream->recv(usrp_buffer, number_of_samples_per_channel, md, 1.0);

      if (num_rx_samps > 0) {
        buffer_list->produce(num_rx_samps);
        num_acquired_samples_per_channel += num_rx_samps;
      }

      //std::cout << boost::format("   num_rx_samps = %d, num_acquired_samples_per_channel = %d, num_requested_samples_per_channel = %d ") %
      //    num_rx_samps % num_acquired_samples_per_channel % num_requested_samples_per_channel << std::endl;
    }

    // Stores the timestamp of the last chunk of samples in the buffer_list
    buffer_list->set_utc_timestamp_last(md.time_spec.get_full_secs());

    // Last packet to stop acquisition
    std::cout << boost::format("Acquired %d samples per channel.")
      % num_acquired_samples_per_channel << std::endl << std::endl;
    stream_cmd = uhd::stream_cmd_t(uhd::stream_cmd_t::STREAM_MODE_STOP_CONTINUOUS);
    stream_cmd.num_samps = 0;
    stream_cmd.stream_now = false;
    stream_cmd.time_spec = usrp->get_time_now() + uhd::time_spec_t(0.0, 0.1);
    rx_stream->issue_stream_cmd(stream_cmd);

  } catch (boost::thread_interrupted e) { // boost::thread_interrupted //TODO: get better
    std::cout << " @@@ reception thread interrupted " << std::endl;
    return;
    // return ~0;
  }

}


int file_name_splitter (
  const std::string input_file_list,
    std::vector < std::string > & input_file_names,
    size_t num_channels) {

  // split the input_file_names string into a vector of strings according to split characters (in this case """, "'" and ",")
  boost::split(input_file_names, input_file_list, boost::is_any_of("\"',"));

  // remove whitespaces at the beginning and at the end of each filename
  for (std::vector< std::string>::iterator it = input_file_names.begin();
       it != input_file_names.end();
       ++it) {
    boost::trim(*it);
  }

  std::cout << boost::format("size of channel names string: %d") % (input_file_names.size()) << std::endl;
  std::cout << boost::format("size of channels: %d") % (num_channels) << std::endl;

  //the number of files must be the same as the number of channels
  if (input_file_names.size() != num_channels) {
    std::cout << "Error: Wrong list of input files: more/less than channels" << std::endl;
    return ~0;
  }

  //print the extracted filenames to screen, just to check
  for (std::vector< std::string >::size_type ii = 0; ii < input_file_names.size(); ii++) {
    std::cout << boost::format(" Filename for channel %d = \"%s\"") % ii % input_file_names[ii] << std::endl;
  }
  return EXIT_SUCCESS;
}

/***********************************************************************
 * split_multichannel_output_file: separate the serialized received data into
 * various files according to the channel specifications
 * Parameters:
 * input_file_name: string with the path to source file
 * output_file_names: vector of strings with destination channel files
 * stop_signal_called: when set to true it stops the splitting process.
 **********************************************************************/

int split_multichannel_output_file(
        std::string input_file_name,
        std::vector < std::string > output_file_names,
        volatile bool * stop_signal_called) {

  // -------------------------------------------------------------
  //first of all, open the file with source data. Depending on the system, there are two avaliable ways to do:
  //  -with  UNIX commands (only for linux users).
  //  -with C commands.
  #ifdef POSIX_FILE_API
    int file_descriptor = open(input_file_name.c_str(), O_RDONLY);

    if (file_descriptor == (-1)) {
      std::cerr << boost::format("Error opening the file with name \"%s\"")
        % input_file_name << std::endl;
      return ~0;
    } else {
      std::cout << boost::format(" Processing filename = \"%s\"") % input_file_name << std::endl;
    }
  #else
    FILE * file_descriptor = fopen(input_file_name.c_str(), "rb");

    if (file_descriptor == NULL) {
      std::cerr << boost::format("Error opening the file with name \"%s\"")
        % input_file_name << std::endl;
      return ~0;
    } else {
      std::cout << boost::format(" Processing filename = \"%s\"") % input_file_name << std::endl;
    }
  #endif
  // -------------------------------------------------------------

  //inizialization of var "num_channels", in order to read the number of channels from the opened source file
  uint16_t num_channels = 0;


  // -------------------------------------------------------------
  //read the number of channels from source file (2 bytes).
  #ifdef POSIX_FILE_API
    // Reads 2 bytes containing the number of channels
    if (read(file_descriptor, & num_channels, sizeof(uint16_t)) == sizeof(uint16_t)) {
      std::cout << boost::format(" Number of channels = %d") % num_channels << std::endl;
    } else {
      std::cerr << "Error reading number of channels from file" << std::endl;
      return ~0;
    }

  #else

    if (fread(& num_channels, sizeof(uint16_t), 1, file_descriptor) == 1) {
      std::cout << boost::format(" Number of channels = %d") % num_channels << std::endl;
    } else {
      std::cerr << "Error reading number of channels from file" << std::endl;
      return ~0;
    }

  #endif
  // -------------------------------------------------------------

  uint16_t max_payload_size_in_samples;
  uint16_t sample_size_in_bytes;

  // -------------------------------------------------------------
  //read the maximum number of samples from source file (2 bytes).
  #ifdef POSIX_FILE_API
    // Reading 2 bytes containing the maximum number of samples in the payloads

    if (read(file_descriptor, & max_payload_size_in_samples, sizeof(uint16_t)) == sizeof(uint16_t)) {
      std::cout << boost::format(" Max payload size in samples = %d") % max_payload_size_in_samples << std::endl;
    } else {
      std::cerr << "Error reading the max payload size in samples from file"  << std::endl;
      return ~0;
    }

    // Reading 2 bytes containing the size of each sample in bytes
    if (read(file_descriptor, & sample_size_in_bytes, sizeof(uint16_t)) == sizeof(uint16_t)) {
      std::cout << boost::format(" Sample size in bytes = %d") % sample_size_in_bytes << std::endl;
    } else {
      std::cerr << "Error reading the sample size in bytes from file"  << std::endl;
      return ~0;
    }

  #else
    // Reading 2 bytes containing the maximum number of samples in the payloads
    if (fread(& max_payload_size_in_samples, sizeof(uint16_t), 1, file_descriptor) == 1) {
      std::cout << boost::format(" Max payload size in samples = %d") % max_payload_size_in_samples << std::endl;
    } else {
      std::cerr << "Error reading the max payload size in samples from file"  << std::endl;
      return ~0;
    }

    // Reading 2 bytes containing the size of each sample in bytes
    if (fread(& sample_size_in_bytes, sizeof(uint16_t), 1, file_descriptor) == 1) {
      std::cout << boost::format(" Sample size in bytes = %d") % sample_size_in_bytes << std::endl;
    } else {
      std::cerr << "Error reading the sample size in bytes from file"  << std::endl;
      return ~0;
    }

  #endif
  // -------------------------------------------------------------


  // -------------------------------------------------------------
  // Define the array with  file descriptors based on I/O mode
  #ifdef POSIX_FILE_API
    int output_file_descriptors[num_channels];
  #else
    FILE * output_file_descriptors[num_channels];
  #endif
  // -------------------------------------------------------------



  // -------------------------------------------------------------
  // open the files which will store the different channel data
  for (size_t ii = 0; ii < num_channels; ++ii) {

    // open the files for write in
    #ifdef POSIX_FILE_API
        output_file_descriptors[ii] = open(output_file_names[ii].c_str(), O_CREAT | O_TRUNC | O_NOATIME | O_WRONLY, 0666);
    #else
        output_file_descriptors[ii] = fopen(output_file_names[ii].c_str(), "wb");
    #endif

    // if error, close the file descriptors opened until this point
    #ifdef POSIX_FILE_API
        if (output_file_descriptors[ii] == -1) {
          std::cerr << boost::format("Error opening the file with name \"%s\"")
            % output_file_names[ii] << std::endl;
    #else
        if (output_file_descriptors[ii] == NULL) {
          std::cerr << boost::format("Error opening the file with name \"%s\"")
            % output_file_names[ii] << std::endl;
    #endif

          // close file opened just before the error
          #ifdef POSIX_FILE_API
                close(file_descriptor);
                for (size_t iii = 0; iii < ii; ++iii) {
                  close(output_file_descriptors[iii]);
                }
          #else
                fclose(file_descriptor);
                for (size_t iii = 0; iii < ii; ++iii) {
                  fclose(output_file_descriptors[iii]);
                }
          #endif
        return ~0;
      }
    } //end for !
    // -------------------------------------------------------------


  // buffer inicialization to store provisional data until write it in the correct channel file
  char buffer[max_payload_size_in_samples * sample_size_in_bytes];

  //auxiliar variables initialization
  uint16_t payload_size;
  ssize_t num_bytes_to_process;
  unsigned long total_num_samps_processed = 0;
  unsigned long total_num_bytes_processed = 0;
  //size_t index = 1;

  // -------------------------------------------------------------
  // Read the entire file separating into the different channel files
  std::cout << boost::format(" Press CTRL+C to stop the splitting process . . .\n") << std::endl;
  #ifdef POSIX_FILE_API
    while ((not (*stop_signal_called)) &&
           (read(file_descriptor, & payload_size, sizeof(uint16_t)) > 0)) {
      // Reads 2 bytes of the header containing the size of the payload
      //std::cout << boost::format(" BLOCK %d ") % (index++) << std::endl;
      //std::cout << boost::format(" payload_size = %d samples.") % payload_size << std::endl;
      if (payload_size > max_payload_size_in_samples) {
        std::cout << boost::format(" payload_size = %d samples > max_payload_size_in_samples!!!") % payload_size << std::endl;
      }
      // Calculate the number of bytes to read from source file corresponding to a single channel transmission
      num_bytes_to_process = payload_size * sample_size_in_bytes;

      // Read this ammount for each channel
      for (size_t i = 0; i < num_channels; ++i) {
        if (read(file_descriptor, & buffer, num_bytes_to_process) == num_bytes_to_process) {
          if (write(output_file_descriptors[i], & buffer, num_bytes_to_process) != num_bytes_to_process) {
            std::cerr << boost::format("Error writting samples to file: \"%s\"") % (output_file_names[i]) << std::endl;
            return ~0;
          } else {
            total_num_bytes_processed += num_bytes_to_process;
            total_num_samps_processed += num_bytes_to_process / sample_size_in_bytes;
          }
        } else {
          std::cerr << boost::format("Error reading the payload with size = %d bytes") % num_bytes_to_process << std::endl;
          return ~0;
        }
        //std::cout << boost::format(" total_num_samps_processed = %d") % total_num_samps_processed << std::endl;
      } // end for

      //std::cout << std::endl;
    }
  #else
    while ((not (*stop_signal_called)) &&
           (fread(& payload_size, sizeof(uint16_t), 1, file_descriptor) > 0)) {
      // Reads 2 bytes of the header containing the size of the payload
      //std::cout << boost::format(" BLOCK %d ") % (index++) << std::endl;
      //std::cout << boost::format(" payload_size = %d samples.") % payload_size << std::endl;
      if (payload_size > max_payload_size_in_samples) {
        std::cout << boost::format(" payload_size = %d samples > max_payload_size_in_samples!!!") % payload_size << std::endl;
      }

      // Calculate the number of bytes to read from source file corresponding to a single channel transmission
      num_bytes_to_process = payload_size * sample_size_in_bytes;

      // Read this ammount for each channel
      for (size_t i = 0; i < num_channels; ++i) {
        if (fread(& buffer, sample_size_in_bytes, payload_size, file_descriptor) == payload_size) {
          if (fwrite(& buffer, sample_size_in_bytes, payload_size, output_file_descriptors[i]) != payload_size) {
            std::cerr << boost::format("Error writting samples to file: \"%s\"") % (output_file_names[i]) << std::endl;
            return ~0;
          } else {
            total_num_bytes_processed += num_bytes_to_process;
            total_num_samps_processed += num_bytes_to_process / sample_size_in_bytes;
          }
        } else {
          std::cerr << boost::format("Error reading the payload with size = %d bytes") % num_bytes_to_process << std::endl;
          return ~0;
        }
        //std::cout << boost::format(" total_num_samps_processed = %d") % total_num_samps_processed << std::endl;
      } //end for

      //std::cout << std::endl;
    }
  #endif
  // -------------------------------------------------------------

  if (*stop_signal_called) {
    std::cout << boost::format(" Splitting process stopped by the user\n") << std::endl;
  }
  // -------------------------------------------------------------
  // close each used file
  #ifdef POSIX_FILE_API
    close(file_descriptor);
    for (size_t ii = 0; ii < num_channels; ++ii) {
      close(output_file_descriptors[ii]);
    }
  #else
    fclose(file_descriptor);
    for (size_t ii = 0; ii < num_channels; ++ii) {
      fclose(output_file_descriptors[ii]);
    }
  #endif
  // -------------------------------------------------------------

  std::cout << boost::format(" Processed %d samples per channel.")
    % (total_num_bytes_processed / (num_channels * sample_size_in_bytes)) << std::endl << std::endl;

  return EXIT_SUCCESS;
}

int read_from_file_to_buffers(
  const std::vector < std::string > input_file_names,
  const std::vector < size_t > & channel_nums,
  std::vector < std::vector < std::complex < short > > > & file_buffers) {

  // open the transmitted files in order to store it in different buffers
  size_t file_length = 0;
  FILE * file_descriptors[channel_nums.size()];

  // check if number of files equal to number of channels
  if ( not (channel_nums.size() == input_file_names.size()) ){
    std::cout << "Error: wrong list of input files: length different from number of channels" << std::endl;
    return ~0;
   }

  //for each file (one per channel), store the file descriptor in an array
  for (size_t ch = 0; ch < channel_nums.size(); ch++) {
    file_descriptors[ch] = fopen(input_file_names[ch].c_str(), "rb");

    if (file_descriptors[ch] == NULL) {
      std::cerr << boost::format("Error opening the file with name \"%s\"")
        % input_file_names[ch] << std::endl;

      // in case of error, close all possibly opened files
      for (size_t chaux = 0; chaux < ch; chaux++) {
        fclose(file_descriptors[chaux]);
      }
      return ~0;
    }

    // read the file size. In case of one channel, only  compute it. In case of various channels,
    // test if all the channels sizes are the same.
    rewind(file_descriptors[ch]);

    if (ch==0){
      fseek(file_descriptors[ch], SEEK_SET, SEEK_END);
      file_length = (size_t) ftell(file_descriptors[ch]);
    } else {
      fseek(file_descriptors[ch], SEEK_SET, SEEK_END);
      if (file_length != (size_t) ftell(file_descriptors[ch])) {
        std::cerr << boost::format("Error opening the file with name \"%s\"")
        % input_file_names[ch] << std::endl;

        // in case of error, close all possibly opened files
        for (size_t chaux = 0; chaux <= ch; chaux++) {
          fclose(file_descriptors[chaux]);
        }
        return ~0;
      }
    }

    rewind(file_descriptors[ch]);
  } //end outer for

  // compute the number of samples per channel, based on file length and the sample size
  size_t num_samples_per_channel = file_length / sizeof(std::complex < short >);

  // inicializate the structure to hold the source files. 2 dimensions:
  // - first: one position per channel
  // - second: set of samples transmitted at the same time (maximum supported by the channel).
  //           The sum of all rows in second dimension is equivalent to entire channel source file.
  std::vector < std::vector < std::complex < short > > > file_buffers_reference(
    channel_nums.size(), std::vector<std::complex<short > >(num_samples_per_channel));
  file_buffers = file_buffers_reference;

  std::cout << boost::format("Buffering %d files with %d samples each")
    % channel_nums.size() % num_samples_per_channel << std::endl;

  // num_bytes_to_read is forced to be multiple of the size of a complex<short> in bytes.
  size_t num_bytes_to_read =
    (file_length / sizeof(std::complex < short >)) * sizeof(std::complex < short >);

  // for each channel, read the source file to respective buffer
  for (size_t ch=0; ch < channel_nums.size(); ch ++) {
    if (fread(&file_buffers[ch][0], sizeof(char), num_bytes_to_read, file_descriptors[ch])
      != num_bytes_to_read) {
      std::cerr << boost::format("Error reading the file") << std::endl;

      // in case of error, close all possibly opened files
      for (size_t chaux = 0; chaux <= ch; chaux++) {
        fclose(file_descriptors[chaux]);
      }
      return ~0;
    }
    fclose(file_descriptors[ch]);
  }
  return EXIT_SUCCESS;
}

void send_from_buffers_to_usrp (
  uhd::usrp::multi_usrp::sptr usrp,
  const std::vector< size_t > & channel_nums,
  std::vector < std::vector < std::complex < short > > >  file_buffers,
  volatile bool * stop_signal_called) {

  // Set realtime Thread priority
  uhd::set_thread_priority_safe(1, true);

  try {
    const uhd::time_spec_t TIME_DELAY_BETWEEN_COMMANDS =
      uhd::time_spec_t(0.0, 0.2);

    size_t number_of_channels = file_buffers.size();
    size_t total_num_samps_per_channel = file_buffers[0].size();

    // create a transmit streamer
    // channels are mapped as follows: index0 = channel0, index1 = channel1, ...
    uhd::stream_args_t stream_args("sc16", "sc16");
    stream_args.channels = channel_nums;
    usrp->set_command_time(usrp->get_time_now() + TIME_DELAY_BETWEEN_COMMANDS);
    uhd::tx_streamer::sptr tx_stream = usrp->get_tx_stream(stream_args);
    usrp->clear_command_time();

    //calculate the number of iterations we need to send the entire file and the number of samples in the last iter
    size_t usrp_buffer_num_samples_per_channel = tx_stream->get_max_num_samps();
    size_t number_iters_per_file = (total_num_samps_per_channel / usrp_buffer_num_samples_per_channel);
    size_t number_samples_last_iter = (total_num_samps_per_channel % usrp_buffer_num_samples_per_channel);
    if (number_samples_last_iter > 0) {
      number_iters_per_file++;
    }
    // Special case in which the total number of samples to be transmitted is less
    // than the usrp buffer size.
    if (usrp_buffer_num_samples_per_channel > total_num_samps_per_channel) {
      usrp_buffer_num_samples_per_channel = total_num_samps_per_channel;
      number_iters_per_file = 1;
      number_samples_last_iter = 0;
    }

    //std::cout << boost::format(" number of samples per channel for each transmission: %f samples")
    //  % usrp_buffer_num_samples_per_channel << std::endl;
    //std::cout << boost::format(" maximum number of samples per channel allowed: %f samples (%d channel(s))") %
    //  (tx_stream->get_max_num_samps()) % number_of_channels << std::endl;

    // Vector of pointers for the transmission (Structure required by tx_stream)
    std::vector < std::vector < std::complex < short > * > > tx_stream_buffers(
      number_iters_per_file,
      std::vector < std::complex < short > * > (file_buffers.size()));

    // store all relative positions in file to be pointed in each transmissions. By this way, we assign
    // only one time the pointers for all transmissions, instead of assign them one time per iteration.
    // To store this relative positions we use a helper array with 2 dimensions:
    //   -first: one position per channel
    //   -second: one position per relative index pointed to transmit, separated by the maximum number of samples
    //            per channel. Read the maximum number of samples in each position on this dimension is equivalent
    //            to read the entire channel file
    size_t current_file_pos = 0;
    for (size_t iter_index = 0; iter_index < number_iters_per_file; ++iter_index) {
        for (size_t channel_index = 0; channel_index < number_of_channels; ++channel_index) {
            tx_stream_buffers[iter_index][channel_index] = & file_buffers[channel_index][current_file_pos];
        }

        current_file_pos += usrp_buffer_num_samples_per_channel;
    }

    //send the first packet -> Setup the metadata flags
    uhd::tx_metadata_t md;
    md.start_of_burst = true;
    md.end_of_burst = false;
    md.has_time_spec = true;
    md.time_spec = usrp->get_time_now() + TIME_DELAY_BETWEEN_COMMANDS;

    size_t iter_index_start_value = 0;

    tx_stream->send(tx_stream_buffers[iter_index_start_value], usrp_buffer_num_samples_per_channel, md);

    // metadata for all sample blocks but the first one
    md.start_of_burst = false;
    md.has_time_spec = false;

    // start the transmission of different channel files. The last transmission must be separated to adapt it
    // to the ammount of remaining data in the channel file (probably not the rx_stream_max_num_samples).
    // only unnecessary when file_size multiple of rx_stream_max_num_samples)
    iter_index_start_value++;

    // case: file_size multiple of rx_stream_max_num_samples
    if (number_samples_last_iter == 0) {
      while (not (*stop_signal_called)) {
        for (size_t iter_index = iter_index_start_value; iter_index < number_iters_per_file; ++iter_index) {
          tx_stream->send(tx_stream_buffers[iter_index], usrp_buffer_num_samples_per_channel, md);
        }
        iter_index_start_value = 0;
      }
    // case: file_size not multiple of rx_stream_max_num_samples (last iteration separated)
    } else {
      while (not (*stop_signal_called)) {
        for (size_t iter_index = iter_index_start_value; iter_index < number_iters_per_file - 1; ++iter_index) {
          tx_stream->send(tx_stream_buffers[iter_index], usrp_buffer_num_samples_per_channel, md);
        }
        tx_stream->send(tx_stream_buffers[number_iters_per_file - 1], number_samples_last_iter, md);
        iter_index_start_value = 0;
      }
    } //end if

    //send a mini EOB packet
    md.end_of_burst = true;
    tx_stream->send("", 0, md, 1);
    std::cout <<std::endl << " @@@ transmission thread stopped." << std::endl << std::endl;

  } catch (boost::thread_interrupted e) { // boost::thread_interrupted //TODO: get better
    std::cout << " @@@ transmission thread interrupted " << std::endl;
    return;
  }
}

void send_from_buffers_to_tx_streamer (
  uhd::usrp::multi_usrp::sptr usrp,
  uhd::tx_streamer::sptr tx_stream,
  std::vector < std::vector < std::complex < short > > >  signal_buffers,
  uint64_t tx_time,
  volatile bool * stop_signal_called) {

  // Set realtime Thread priority
  uhd::set_thread_priority_safe(1, true);

  try {
    size_t number_of_channels = signal_buffers.size();
    size_t total_num_samps_per_channel = signal_buffers[0].size();

    //calculate the number of iterations we need to send the entire file and the number of samples in the last iter
    size_t usrp_buffer_num_samples_per_channel = tx_stream->get_max_num_samps();
    size_t number_iters_per_file = (total_num_samps_per_channel / usrp_buffer_num_samples_per_channel);
    size_t number_samples_last_iter = (total_num_samps_per_channel % usrp_buffer_num_samples_per_channel);
    if (number_samples_last_iter > 0) {
      number_iters_per_file++;
    }

    // Special case in which the total number of samples to be transmitted is less
    // than the usrp buffer size.
    if (usrp_buffer_num_samples_per_channel > total_num_samps_per_channel) {
      usrp_buffer_num_samples_per_channel = total_num_samps_per_channel;
      number_iters_per_file = 1;
      number_samples_last_iter = 0;
    }

    //std::cout << boost::format(" number of samples per channel for each transmission: %f samples")
    //  % usrp_buffer_num_samples_per_channel << std::endl;
    //std::cout << boost::format(" maximum number of samples per channel allowed: %f samples (%d channel(s))") %
    //  (tx_stream->get_max_num_samps()) % number_of_channels << std::endl;

    // Vector of pointers for the transmission (structure required by tx_stream)
    std::vector < std::vector < std::complex < short > * > > tx_stream_buffers(
      number_iters_per_file,
      std::vector < std::complex < short > * > (signal_buffers.size()));

    // store all relative positions in file to be pointed in each transmissions. By this way, we assign
    // only one time the pointers for all transmissions, instead of assign them one time per iteration.
    // To store this relative positions we use a helper array with 2 dimensions:
    //   -first: one position per channel
    //   -second: one position per relative index pointed to transmit, separated by the maximum number of samples
    //            per channel. Read the maximum number of samples in each position on this dimension is equivalent
    //            to read the entire channel file
    size_t current_file_pos = 0;
    for (size_t iter_index = 0; iter_index < number_iters_per_file; ++iter_index) {
        for (size_t channel_index = 0; channel_index < number_of_channels; ++channel_index) {
            tx_stream_buffers[iter_index][channel_index] = & signal_buffers[channel_index][current_file_pos];
        }

        current_file_pos += usrp_buffer_num_samples_per_channel;
    }

    //send the first packet -> Setup the metadata flags
    uhd::tx_metadata_t md;
    md.start_of_burst = true;
    md.end_of_burst = false;
    md.has_time_spec = true;
    /* Minimum time to start transmission */
    if (tx_time == 0) {
        md.time_spec = usrp->get_time_now() + uhd::time_spec_t(0.0, 0.001);
    } else {
        fprintf(stdout, "\ntx_time = %lu\n", tx_time);
        fflush(stdout);
        md.time_spec = uhd::time_spec_t::from_ticks((long long) tx_time, 1.0);
    }

    //GTIS_wait_for_pps_edge(usrp);
    uhd::time_spec_t current_last_pps_time = usrp->get_time_last_pps();
    std::cout << boost::format("Transmission will start at: %f %f ...")
                 % (md.time_spec.get_real_secs())
                 % (md.time_spec.get_frac_secs()) << std::endl;
    std::cout << boost::format("Time last pps: %f %f ...")
                 % (current_last_pps_time.get_real_secs())
                 % (current_last_pps_time.get_frac_secs()) << std::endl << std::endl;


    size_t iter_index_start_value = 0;

    tx_stream->send(tx_stream_buffers[iter_index_start_value], usrp_buffer_num_samples_per_channel, md);

    // metadata for all sample blocks but the first one
    md.start_of_burst = false;
    md.has_time_spec = false;

    // start the transmission of different channel files. The last transmission must be separated to adapt it
    // to the ammount of remaining data in the channel file (probably not the rx_stream_max_num_samples).
    // only unnecessary when file_size multiple of rx_stream_max_num_samples)
    iter_index_start_value++;

    // case: file_size multiple of rx_stream_max_num_samples
    if (number_samples_last_iter == 0) {
      while (not (*stop_signal_called)) {
        for (size_t iter_index = iter_index_start_value; iter_index < number_iters_per_file; ++iter_index) {
          tx_stream->send(tx_stream_buffers[iter_index], usrp_buffer_num_samples_per_channel, md);
        }
        iter_index_start_value = 0;
      }
    // case: file_size not multiple of number_of_samples_per_channel (last iteration separated)
    } else {
      while (not (*stop_signal_called)) {
        for (size_t iter_index = iter_index_start_value; iter_index < number_iters_per_file - 1; ++iter_index) {
          tx_stream->send(tx_stream_buffers[iter_index], usrp_buffer_num_samples_per_channel, md);
        }
        tx_stream->send(tx_stream_buffers[number_iters_per_file - 1], number_samples_last_iter, md);
        iter_index_start_value = 0;
      }
    } //end if

    //send a mini EOB packet
    md.end_of_burst = true;
    tx_stream->send("", 0, md, 1);
    std::cout <<std::endl << " @@@ transmission thread stopped." << std::endl << std::endl;

  } catch (boost::thread_interrupted e) { // boost::thread_interrupted //TODO: get better
    std::cout << " @@@ transmission thread interrupted " << std::endl;
    return;
  }
}

/***********************************************************************
 *
 * OLD MULTI-USRPB210 CODE TO BE UPDATED
 *
 **********************************************************************/

/*
 * OLD HEADER FILE
 */
//#ifndef INCLUDED_GTIS_RECV_PARAMETERS_HPP
//#define INCLUDED_GTIS_RECV_PARAMETERS_HPP

//#include <boost/thread/mutex.hpp>
//#include <boost/thread/condition.hpp>
//#include <boost/thread/thread.hpp>
//#include <boost/progress.hpp>
//#include <boost/algorithm/string.hpp>
//#include <boost/bind.hpp>

//class recv_parameters {

//public:

//	typedef size_t size_type;

//	explicit recv_parameters(
//		bool c_network_commands,
//		size_type c_rate,
//		size_type c_number_of_samples_per_channel,
//		size_type c_num_requested_samples_per_channel):
//			network_commands(c_network_commands),
//			rate(c_rate),
//			number_of_samples_per_channel(c_number_of_samples_per_channel),
//			num_requested_samples_per_channel(c_num_requested_samples_per_channel){

//	}

//	bool get_network_commands() {
//    	return network_commands;
//  	}

//  	size_type get_rate(){
//  		return rate;
//  	}

//  	size_type get_number_of_samples_per_channel(){
//  		return number_of_samples_per_channel;
//  	}

//  	size_type get_num_requested_samples_per_channel(){
//  		return num_requested_samples_per_channel;
//  	}

//private:

//	bool network_commands;
//	size_type rate;
//	size_type number_of_samples_per_channel;
//    size_type num_requested_samples_per_channel;

//};

//#endif

//***********************************************************************
// * check_synchronism: check if two USRPs are synchronized in time
// * Parameters:
// * usrp0: first USRP
// * usrp1: second USRP
// **********************************************************************/
//bool check_synchronism(uhd::usrp::multi_usrp::sptr usrp0, uhd::usrp::multi_usrp::sptr usrp1){

//    uhd::time_spec_t usrp0_time_last_pps = usrp0->get_time_last_pps();
//    uhd::time_spec_t usrp1_time_last_pps = usrp1->get_time_last_pps();

//    time_t usrp0_full_secs = usrp0_time_last_pps.get_full_secs();
//    time_t usrp1_full_secs = usrp1_time_last_pps.get_full_secs();

//    time_t usrp0_frac_secs = usrp0_time_last_pps.get_frac_secs();
//    time_t usrp1_frac_secs = usrp1_time_last_pps.get_frac_secs();

//    return (usrp0_full_secs == usrp1_full_secs) and (usrp0_frac_secs == usrp1_frac_secs);
//}


//***********************************************************************
//* receive_in_one_usrp: receive function for one usrp
//* Parameters:
//* usrp: pointer to usrp object
//* channel_nums: the numbers of the channels to use
//* recv_params: auxiliar class with the number of samples per channel, the requested samples per channel, the rate and boolean if network commands
//* buffer_list: ad-hoc multichannel buffer dedicated to receive
//* wait_to_receive_command: parameter used to wait to receive / receive continuously
//* stop_signal_called_local: signal called to stop the reception
// **********************************************************************/

//void recv_in_one_usrp(uhd::time_spec_t stream_starting_time,
//      uhd::usrp::multi_usrp::sptr usrp,
//      uhd::rx_streamer::sptr rx_stream,
//      const std::vector < size_t > &channel_nums,
//      recv_parameters * recv_params,
//      usrp_multichannel_buffer * buffer_list,
//      bool wait_for_key_pressed,
//      volatile bool * stop_signal_called) {

//  // Set realtime Thread priority
//  bool priority_recv = uhd::set_thread_priority_safe(1,true);

//  // If not possible to stablish priority, notice the error on screen
//  if (not priority_recv){
//    std::cout << "failure in recv priority" << std::endl;
//  }

//  try{
//    unsigned long num_acquired_samples_per_channel = 0;

//    // usrp buffer to store received data
//    std::vector < std::complex < short > * > usrp_buffer(buffer_list->number_of_channels());

//    // First, acquire the first packet to start reception ...
//    uhd::rx_metadata_t md;
//    uhd::stream_cmd_t stream_cmd(uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS);
//    stream_cmd.num_samps = 0;
//    stream_cmd.stream_now = false;


//    // Demand a reference to produce data (write a position with the information received)
//    buffer_list->reference_buffer_for_producer( &usrp_buffer);

//    stream_cmd.time_spec = stream_starting_time;
//    rx_stream->issue_stream_cmd(stream_cmd);

//    size_t num_rx_samps = rx_stream->recv(usrp_buffer, recv_params->get_number_of_samples_per_channel(), md, 3.0);
    
//    if (num_rx_samps > 0) {
//      buffer_list->produce(num_rx_samps);
//      num_acquired_samples_per_channel += num_rx_samps;
//    }

//    // ... and then capture data until stop signal called or maximum number of samples reached
//    while ((not (*stop_signal_called)) and ((recv_params->get_num_requested_samples_per_channel()) > num_acquired_samples_per_channel or (recv_params->get_num_requested_samples_per_channel()) == 0)) {
//      buffer_list->reference_buffer_for_producer( &usrp_buffer);
//      num_rx_samps = rx_stream->recv(usrp_buffer, recv_params->get_number_of_samples_per_channel(), md, 1.0);

//      if (num_rx_samps > 0) {
//        buffer_list->produce(num_rx_samps);
//        num_acquired_samples_per_channel += (unsigned long) num_rx_samps;
//      }

//    }

//    //uhd::time_spec_t usrp0_time_last_pps = usrp->get_time_last_pps();
//    //std::cout << boost::format("get_time_last_pps: %d : %f") % usrp0_time_last_pps.get_full_secs() % usrp0_time_last_pps.get_frac_secs() << std::endl;

//    uhd::time_spec_t usrp0_time_now = usrp->get_time_now();
//    std::cout << boost::format("get_time_now: %d : %f") % usrp0_time_now.get_full_secs() % usrp0_time_now.get_frac_secs() << std::endl;

//    // Last packet to stop acquisition
//    std::cout << boost::format(" Acquisition stopped. Acquired %d samples per channel.")
//      % num_acquired_samples_per_channel << std::endl << std::endl;
//    stream_cmd = uhd::stream_cmd_t(uhd::stream_cmd_t::STREAM_MODE_STOP_CONTINUOUS);
//    stream_cmd.num_samps = 0;
//    stream_cmd.stream_now = false;
//    const uhd::time_spec_t stream_finishing_time = usrp->get_time_now() + uhd::time_spec_t(0.0, 0.5);
//    stream_cmd.time_spec = stream_finishing_time;
//    rx_stream->issue_stream_cmd(stream_cmd);

//  } catch (boost::thread_interrupted e) { // boost::thread_interrupted //TODO: get better
//    std::cout << " @@@ reception thread interrupted " << std::endl;
//    return;
//    // return ~0;
//  }

//}

//***********************************************************************
//* recv_to_multiple_usrp_multichannel_buffer thread: read buffers from multiple usrp. (Primary)
//* Parameters:
//* usrp: pointer to usrp object 0
//* usrp1: pointer to usrp object 1
//* channel_nums: the numbers of the channels to use
//* recv_params: auxiliar class with the number of samples per channel, the requested samples per channel, the rate and boolean if network commands
//* buffer_list: ad-hoc multichannel buffer dedicated to receive
//* wait_to_receive_command: parameter used to wait to receive / receive continuously
//* stop_signal_called_local: signal called to stop the reception
//**********************************************************************/

// void recv_to_multiple_usrp_multichannel_buffer(
//      uhd::usrp::multi_usrp::sptr usrp,
//      uhd::usrp::multi_usrp::sptr usrp1,
//      const std::vector < size_t > &channel_nums,
//      recv_parameters * recv_params,
//      usrp_multichannel_buffer * buffer_list,
//      usrp_multichannel_buffer * buffer_list1,
//      bool wait_for_key_pressed,
//      volatile bool * stop_signal_called) {


//  // Set realtime Thread priority
//  //bool priority_recv = uhd::set_thread_priority_safe(1,true);

//  // If not possible to stablish priority, notice the error on screen
//  // if (not priority_recv){
//  //   std::cout << "failure in recv priority" << std::endl;
//  // }


//  try {

//    std::string cpu_format = "sc16";
//    std::string wire_format = "sc16";


//    //create a receive streamer
//    uhd::stream_args_t stream_args(cpu_format, wire_format);

//    // Stablish the number of channels in rx_streamer arguments
//    stream_args.channels = channel_nums;


//    std::cout << boost::format("Getting RX streams for both USRPs . . . ");
//    const uhd::time_spec_t cmd_time = usrp->get_time_last_pps() + uhd::time_spec_t(2.0, 0.0);
//    usrp->set_command_time(cmd_time);
//    //usrp1->set_command_time(cmd_time);
//    // Obtain the rx_stream
//    uhd::rx_streamer::sptr rx_stream = usrp->get_rx_stream(stream_args);
//    //uhd::rx_streamer::sptr rx_stream1 = usrp1->get_rx_stream(stream_args);
//    usrp->clear_command_time();
//    //usrp1->clear_command_time();
//    std::cout << boost::format("OK ") << std::endl;

//    std::cout << boost::format("Sleeping 2 seconds for configuration . . . ") << std::endl;
//    boost::this_thread::sleep(boost::posix_time::seconds(2));

//    std::cout << boost::format(" Aligning time between USRPs . . . ");
//    uhd::time_spec_t time_now = usrp->get_time_last_pps();
//    uhd::time_spec_t time_to_reset = time_now + uhd::time_spec_t(2.0, 0.0);
//    usrp->set_time_next_pps(time_to_reset);
//    //usrp1->set_time_next_pps(time_to_reset);
//    std::cout << boost::format(" OK ") << std::endl;
//    std::cout << boost::format(" time now: %f . %f ") % time_now.get_full_secs() % time_now.get_frac_secs() << std::endl;
//    std::cout << boost::format(" time to reset: %f . %f ") % time_to_reset.get_full_secs() % time_to_reset.get_frac_secs() << std::endl;

//    std::cout << boost::format("Sleeping 5 seconds for time reset") << std::endl << std::endl;
//    boost::this_thread::sleep(boost::posix_time::seconds(5));

//    std::cout << boost::format("rx_stream->get_max_num_samps() = %d") % rx_stream->get_max_num_samps() << std::endl;

//    std::cout << boost::format("recv_to_usrp_multichannel_buffer: num_requested_samples_per_channel = %d") % (recv_params->get_num_requested_samples_per_channel()) << std::endl;

//    // If wait command, wait to enter command to start acquiring. This parameter is set in command options
//    if(wait_for_key_pressed){
//      std::cout << boost::format("Press ENTER to start acquiring ...") << std::endl;
//      std::cin.get();
//    }

//    if ((not (recv_params->get_network_commands())) and (recv_params->get_num_requested_samples_per_channel() == 0)) {
//      std::cout << " Acquiring samples (press Ctrl + C to stop) . . ." << std::endl;
//    } else {
//      if (recv_params->get_num_requested_samples_per_channel() > 0) {
//        std::cout << std::endl << boost::format(" Acquiring %d samples . . . ") %
//          recv_params->get_num_requested_samples_per_channel() << std::endl << std::endl;
//      } else {
//        std::cout << std::endl << " Acquiring samples . . . " << std::endl << std::endl;
//      }
//    }

//    uhd::time_spec_t stream_starting_time = usrp->get_time_now() + uhd::time_spec_t(1.0, 0.5);

//    // boost::thread reception_th1;
//    // reception_th1 = boost::thread(recv_in_one_usrp,
//    //                    stream_starting_time,
//    //                    usrp1,
//    //                    rx_stream1,
//    //                    channel_nums,
//    //                    recv_params,
//    //                    buffer_list1,
//    //                    wait_to_receive_command,
//    //                    stop_signal_called);

//    // receive by one usrp on this thread
//    //recv_in_one_usrp(stream_starting_time, usrp, rx_stream, channel_nums, recv_params, buffer_list, wait_to_receive_command, stop_signal_called);
//    // stream_starting_time = usrp->get_time_now() + uhd::time_spec_t(0.0, 0.5);
//    // recv_in_one_usrp(stream_starting_time, usrp1, rx_stream1, channel_nums, recv_params, buffer_list1, wait_to_receive_command, stop_signal_called);
    
//    boost::thread reception_th;
//    reception_th = boost::thread(recv_in_one_usrp,
//                       stream_starting_time,
//                       usrp,
//                       rx_stream,
//                       channel_nums,
//                       recv_params,
//                       buffer_list,
//                       wait_for_key_pressed,
//                       stop_signal_called);

//    //reception_th1.join();
//    reception_th.join();

//    //bool check_sync = check_synchronism(usrp, usrp1);

//    // if ( not check_sync){
//    //   std::cout << boost::format("ERROR: USRP unsynchronized.") << std::endl;
//    // } else {
//    //   std::cout << boost::format("SUCCESS: USRP synchronized") << std::endl;
//    // }


//  } catch (boost::thread_interrupted e) { // boost::thread_interrupted //TODO: get better
//    std::cout << " @@@ reception thread interrupted " << std::endl;
//    return;
//    // return ~0;
//  }

//}

//***********************************************************************
// * Check_locked_sensor_function: chek if "sensor_names" sensor/s is/are locked
// * Parameters:
// * sensor_names: List with the enabled sensors in the USRP
// * sensor_name: Sensor to check
// * get_sensor_fn: Function  to check if sensor exists
// * setup_time: time needed to setup a sensor
// **********************************************************************/

//bool check_locked_sensor(std::vector<std::string> sensor_names, const char* sensor_name, get_sensor_fn_t get_sensor_fn, double setup_time){
//  if (std::find(sensor_names.begin(), sensor_names.end(), sensor_name) == sensor_names.end())
//    return false;

//  boost::system_time start = boost::get_system_time();
//  boost::system_time first_lock_time;

//  std::cout << boost::format("Waiting for \"%s\": ") % sensor_name;
//  std::cout.flush();

//  while (true){
//    if ((not first_lock_time.is_not_a_date_time()) and
//        (boost::get_system_time() > (first_lock_time + boost::posix_time::seconds(setup_time))))
//    {
//      std::cout << " locked." << std::endl;
//      break;
//    }

//    if (get_sensor_fn(sensor_name).to_bool()){
//      if (first_lock_time.is_not_a_date_time())
//        first_lock_time = boost::get_system_time();
//      std::cout << "+";
//      std::cout.flush();
//    }
//    else{
//      first_lock_time = boost::system_time(); //reset to 'not a date time'

//      if (boost::get_system_time() > (start + boost::posix_time::seconds(setup_time))){
//        std::cout << std::endl;
//        throw std::runtime_error(str(boost::format("timed out waiting for consecutive locks on sensor \"%s\"") % sensor_name));
//      }

//      std::cout << "_";
//      std::cout.flush();
//    }

//    boost::this_thread::sleep(boost::posix_time::milliseconds(100));
//  }

//  std::cout << std::endl;

//  return true;
//}


//***********************************************************************
// * check_refs_USRPs: check if two USRPs are synchronized in time
// * Parameters:
// * usrp0: first USRP to check external reference
// * usrp1: second USRP to check external reference
// * ref: string with reference to be taken
// * setup_time: seconds of setup time
// **********************************************************************/
//bool check_refs_USRPs(uhd::usrp::multi_usrp::sptr usrp0, uhd::usrp::multi_usrp::sptr usrp1, std::string ref, double setup_time){

//  bool usrp0_checking = false;
//  bool usrp1_checking = false;

//  if (ref == "external") {
//    usrp0_checking = check_locked_sensor(usrp0->get_mboard_sensor_names(0), "ref_locked", boost::bind(&uhd::usrp::multi_usrp::get_mboard_sensor, usrp0, _1, 0), setup_time);
//    usrp1_checking = check_locked_sensor(usrp1->get_mboard_sensor_names(0), "ref_locked", boost::bind(&uhd::usrp::multi_usrp::get_mboard_sensor, usrp1, _1, 0), setup_time);
//  }

//  return (usrp0_checking) and (usrp1_checking);
//}

//***********************************************************************
// * send_from_buffers_to_multiple_usrp: send the buffered data through multiple USRPs
// * Parameters:
// * usrp: the pointer to a initializated usrp
// * usrp1: the pointer to another initialized usrp
// * channel_nums: vector of strings with destination channel files
// * file_buffers: buffer to store the data that will be transmitted
// * stop_signal_called: flag to activate handler to stop transmission
// **********************************************************************/

//void send_from_buffers_to_multiple_usrp (
//  uhd::usrp::multi_usrp::sptr usrp,
//  uhd::usrp::multi_usrp::sptr usrp1,
//  const std::vector< size_t > & channel_nums,
//  std::vector < std::vector < std::complex < short > > >  file_buffers,
//  volatile bool * stop_signal_called) {

//  // Set realtime Thread priority
//  uhd::set_thread_priority_safe(1, true);

//    std::cout << boost::format("Entering function multiple usrp . . . ");

//  try {

//    if (file_buffers.size() == 0){
//      std::cout << "ERROR: empty file buffers. Load files before transmission" << std::endl;
//      return;
//    }

//    size_t number_of_channels = file_buffers.size();
//    size_t total_num_samps_per_channel = file_buffers[0].size();
//    //size_t number_of_channels = 0;
//    //size_t total_num_samps_per_channel = 0;

//    // // create a transmit streamer
//    // // channels are mapped as follows: index0 = channel0, index1 = channel1, ...
//    uhd::stream_args_t stream_args("sc16", "sc16");
//    stream_args.channels = channel_nums;

//    uhd::time_spec_t usrp0_time_last_pps;
//    uhd::time_spec_t usrp1_time_last_pps;



//    std::cout << boost::format("Getting TX streams for both USRPs . . . ");
//    const uhd::time_spec_t cmd_time = usrp->get_time_last_pps() + uhd::time_spec_t(2.0, 0.0);
//    usrp->set_command_time(cmd_time);
//    usrp1->set_command_time(cmd_time);
//    uhd::tx_streamer::sptr tx_stream = usrp->get_tx_stream(stream_args);
//    uhd::tx_streamer::sptr tx_stream1 = usrp1->get_tx_stream(stream_args);
//    usrp->clear_command_time();
//    usrp1->clear_command_time();
//    std::cout << boost::format("OK ") << std::endl;

//    std::cout << boost::format("Sleeping 2 seconds for configuration . . . ") << std::endl;
//    boost::this_thread::sleep(boost::posix_time::seconds(2));

//    std::cout << boost::format(" Aligning time between USRPs . . . ");
//    uhd::time_spec_t time_now = usrp->get_time_last_pps();
//    uhd::time_spec_t time_to_reset = time_now + uhd::time_spec_t(2.0, 0.0);
//    usrp->set_time_next_pps(time_to_reset);
//    usrp1->set_time_next_pps(time_to_reset);
//    std::cout << boost::format(" OK ") << std::endl;
//    std::cout << boost::format(" time now: %f . %f ") % time_now.get_full_secs() % time_now.get_frac_secs() << std::endl;
//    std::cout << boost::format(" time to reset: %f . %f ") % time_to_reset.get_full_secs() % time_to_reset.get_frac_secs() << std::endl;

//    std::cout << boost::format("Sleeping 5 seconds for time reset") << std::endl << std::endl;
//    boost::this_thread::sleep(boost::posix_time::seconds(5));

//    //calculate the number of iterations we need to send the entire file and the number of samples in the last iter
//    size_t usrp_buffer_num_samples_per_channel = tx_stream->get_max_num_samps();
//    size_t number_iters_per_file = (total_num_samps_per_channel / usrp_buffer_num_samples_per_channel);
//    size_t number_samples_last_iter = (total_num_samps_per_channel % usrp_buffer_num_samples_per_channel);
//    if (number_samples_last_iter > 0) {
//      number_iters_per_file++;
//    }
//    // Special case in which the total number of samples to be transmitted is less
//    // than the usrp buffer size.
//    if (usrp_buffer_num_samples_per_channel > total_num_samps_per_channel) {
//      usrp_buffer_num_samples_per_channel = total_num_samps_per_channel;
//      number_iters_per_file = 1;
//      number_samples_last_iter = 0;
//    }

//    std::cout << boost::format(" number of samples per channel for each transmission: %f samples")
//      % usrp_buffer_num_samples_per_channel << std::endl;
//    std::cout << boost::format(" maximum number of samples per channel allowed: %f samples (%d channel(s))") %
//      (tx_stream->get_max_num_samps()) % number_of_channels << std::endl;

//    // Vector of pointers for the transmission (Structure required by tx_stream)
//    std::vector < std::vector < std::complex < short > * > > tx_stream_buffers(
//      number_iters_per_file,
//      std::vector < std::complex < short > * > (file_buffers.size()));

//    // store all relative positions in file to be pointed in each transmissions. By this way, we assign
//    // only one time the pointers for all transmissions, instead of assign them one time per iteration.
//    // To store this relative positions we use a helper array with 2 dimensions:
//    //   -first: one position per channel
//    //   -second: one position per relative index pointed to transmit, separated by the maximum number of samples
//    //            per channel. Read the maximum number of samples in each position on this dimension is equivalent
//    //            to read the entire channel file
//    size_t current_file_pos = 0;
//    for (size_t iter_index = 0; iter_index < number_iters_per_file; ++iter_index) {
//        for (size_t channel_index = 0; channel_index < number_of_channels; ++channel_index) {
//            tx_stream_buffers[iter_index][channel_index] = & file_buffers[channel_index][current_file_pos];
//        }

//        current_file_pos += usrp_buffer_num_samples_per_channel;
//    }

//    std::cout << boost::format("\n\n Checking if both USRPs are synchronous inside the thread after getting streams: ") << std::endl;
//    bool check_sync;
//    check_sync = check_synchronism(usrp, usrp1);
//    if ( not check_sync){
//      std::cout << boost::format("ERROR: USRP unsynchronized.");
//    }

//    //check if same absolute time
//    uhd::time_spec_t usrp0_time_now = usrp->get_time_now();
//    uhd::time_spec_t usrp1_time_now = usrp1->get_time_now();

//    std::cout << boost::format("time now usrp0: %d : %f ") % usrp0_time_now.get_full_secs() % usrp0_time_now.get_frac_secs() << std::endl;
//    std::cout << boost::format("time now usrp1: %d : %f ") % usrp1_time_now.get_full_secs() % usrp1_time_now.get_frac_secs() << std::endl;

//    //send the first packet -> Setup the metadata flags
//    const uhd::time_spec_t stream_starting_time = usrp->get_time_now() + uhd::time_spec_t(0.0, 0.5);
//    uhd::tx_metadata_t md;
//    md.start_of_burst = true;
//    md.end_of_burst = false;
//    md.has_time_spec = true;
//    md.time_spec = stream_starting_time;

//    uhd::tx_metadata_t md1;
//    md1.start_of_burst = true;
//    md1.end_of_burst = false;
//    md1.has_time_spec = true;
//    md1.time_spec = stream_starting_time;
    
//    size_t iter_index_start_value = 0;

//    tx_stream->send(tx_stream_buffers[iter_index_start_value], usrp_buffer_num_samples_per_channel, md);
//    tx_stream1->send(tx_stream_buffers[iter_index_start_value], usrp_buffer_num_samples_per_channel, md);
    
//    // metadata for all sample blocks but the first one
//    md.start_of_burst = false;
//    md.has_time_spec = false;

//    md1.start_of_burst = false;
//    md1.has_time_spec = false;
    
//    // start the transmission of different channel files. The last transmission must be separated to adapt it
//    // to the ammount of remaining data in the channel file (probably not the maximum_number_of_samples_per_channel).
//    // only unnecessary when file_size multiple of maximum_number_of_samples_per_channel)
//    iter_index_start_value++;

//    // case: file_size multiple of maximum_number_of_samples_per_channel
//    if (number_samples_last_iter == 0) {
//      while (not (*stop_signal_called)) {
//        for (size_t iter_index = iter_index_start_value; iter_index < number_iters_per_file; ++iter_index) {
//          tx_stream->send(tx_stream_buffers[iter_index], usrp_buffer_num_samples_per_channel, md);
//          tx_stream1->send(tx_stream_buffers[iter_index], usrp_buffer_num_samples_per_channel, md);
//        }
//        iter_index_start_value = 0;
//      }
//    // case: file_size not multiple of maximum_number_of_samples_per_channel (last iteration separated)
//    } else {
//      std::cout << boost::format("Transmitting ... ") << std::endl;
//      while (not (*stop_signal_called)) {
//      //while (iter < 100){
//        for (size_t iter_index = iter_index_start_value; iter_index < number_iters_per_file - 1; ++iter_index) {
//          tx_stream->send(tx_stream_buffers[iter_index], usrp_buffer_num_samples_per_channel, md);
//          tx_stream1->send(tx_stream_buffers[iter_index], usrp_buffer_num_samples_per_channel, md);
//          //iter = iter +1;
//        }
//        tx_stream->send(tx_stream_buffers[number_iters_per_file - 1], number_samples_last_iter, md);
//        tx_stream1->send(tx_stream_buffers[number_iters_per_file - 1], number_samples_last_iter, md);
//        iter_index_start_value = 0;
        

//      }
//    } //end if

//    //send a mini EOB packet
//    md.end_of_burst = true;
//    md1.end_of_burst = true;
//    tx_stream->send("", 0, md);
//    tx_stream1->send("", 0, md1);
//    std::cout <<std::endl << " @@@ transmission thread stopped." << std::endl << std::endl;

//    std::cout << "checking if usrp synchronized just after continuous transmission" << std::endl;

//    check_sync = check_synchronism(usrp, usrp1);
//    if ( not check_sync){
//      std::cout << boost::format("ERROR: USRP unsynchronized.");
//    }

//  } catch (boost::thread_interrupted e) { // boost::thread_interrupted //TODO: get better
//    std::cout << " @@@ transmission thread interrupted " << std::endl;
//    return;
//  }
//}
