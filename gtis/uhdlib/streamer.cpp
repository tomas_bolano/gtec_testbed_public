#include "gtis/streamer.hpp"

int read_from_stream_to_buffers(
        NL::Socket * client_connection,
        size_t num_channels,
        size_t num_complex_samples_per_channel,
        std::vector < std::vector < std::complex < short > > > & file_buffers,
        FILE * fd) {

    /* inicializate the structure to hold the source files. 2 dimensions:
     * - first: one position per channel
     * - second: set of samples transmitted at the same time (maximum supported by the channel).
     *           The sum of all rows in second dimension is equivalent to entire channel source file.
     */
    std::vector < std::vector < std::complex < short > > > file_buffers_reference(
        num_channels, std::vector<std::complex<short > >(num_complex_samples_per_channel));
    file_buffers = file_buffers_reference;

    if (fd != NULL) {
        fprintf(fd, "Buffering %lu channels with %lu complex-valued samples each.\n",
            num_channels, num_complex_samples_per_channel);
    }

    /* num_bytes_to_read is forced to be multiple of the size of a complex<short> in bytes. */
    size_t num_bytes_to_read = num_complex_samples_per_channel * sizeof(std::complex < short >);
//        (num_complex_samples_per_channel / sizeof(std::complex < short >)) * sizeof(std::complex < short >);

    if (fd != NULL) {
        fprintf(fd, "num_bytes_to_read = %lu\n", num_bytes_to_read);
    }

    // for each channel, read the source file to respective buffer
    size_t num_bytes_received = 0;
    for (size_t ch=0; ch < num_channels; ch ++) {
        if (! gtis_recv_buffer(
                    (char *) & file_buffers[ch][0],
                    num_bytes_to_read,
                    & num_bytes_received,
                    client_connection,
                    fd)) {
            fprintf(fd, "Error reading stream.\n");
            return ~0;
        }
    }
    return EXIT_SUCCESS;
}
