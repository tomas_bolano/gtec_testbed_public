#include "gtis/uhd_lib_utils.hpp"
#include <ctime>
bool GTIS_wait_for_usrp_lo_locked(uhd::usrp::multi_usrp::sptr usrp,
        const bool verbose,
        const unsigned int total_number_of_trials,
        std::vector<size_t> channel_nums) {

    if (verbose) {
        std::cout << boost::format("LO status: ");
    }

    for (unsigned int number_of_trials = 0; number_of_trials < total_number_of_trials;
         number_of_trials++) {
        bool all_los_locked = true;
        for (unsigned int channel_number = 0; channel_number < channel_nums.size();
             channel_number++) {
            all_los_locked = all_los_locked && usrp->get_rx_sensor("lo_locked").to_bool();
        }
        if (all_los_locked) {
            if (verbose) {
                std::cout << boost::format("LOCKED!") << std::endl;
            }
            return true;
        } else {
            GTIS_sleep_current_thread(1, false);
        }
        if (verbose) {
            std::cout << ". ";
            std::cout.flush();
        }
    }
    if (verbose) {
        std::cout << boost::format("UNLOCKED!") << std::endl;
    }
    return false;
}

bool GTIS_is_usrp_lo_locked(uhd::usrp::multi_usrp::sptr usrp, size_t channel) {
    return usrp->get_rx_sensor("lo_locked", channel).to_bool();
}

bool GTIS_usrp_has_gpsdo(uhd::usrp::multi_usrp::sptr usrp) {
    std::vector<std::string> sensor_names = usrp->get_mboard_sensor_names(0);
    return std::find(sensor_names.begin(), sensor_names.end(), "gps_locked") != sensor_names.end();
}

bool GTIS_is_usrp_gpsdo_locked(uhd::usrp::multi_usrp::sptr usrp) {
    if (GTIS_usrp_has_gpsdo(usrp)) {
        return (usrp->get_mboard_sensor("gps_locked")).to_bool();
    }
    return false;
}

bool GTIS_usrp_has_ref(uhd::usrp::multi_usrp::sptr usrp) {
    std::vector<std::string> sensor_names = usrp->get_mboard_sensor_names(0);
    return std::find(sensor_names.begin(), sensor_names.end(), "ref_locked") != sensor_names.end();
}

bool GTIS_is_usrp_ref_locked(uhd::usrp::multi_usrp::sptr usrp) {
    if (GTIS_usrp_has_ref(usrp)) {
        return (usrp->get_mboard_sensor("ref_locked")).to_bool();
    }
    return false;
}

// waits (in a loop) for the PPS edge
uhd::time_spec_t GTIS_wait_for_pps_edge(uhd::usrp::multi_usrp::sptr usrp) {
    uhd::time_spec_t initial_time_last_pps = usrp->get_time_last_pps();
    uhd::time_spec_t time_last_pss = usrp->get_time_last_pps();
    while (initial_time_last_pps == time_last_pss) {
        time_last_pss = usrp->get_time_last_pps();
    }
    return time_last_pss;
}

/* Returns true if the GPSDO is locked, false otherwise (including if it is not found) */
bool GTIS_wait_for_usrp_gpsdo_locked(uhd::usrp::multi_usrp::sptr usrp,
    const bool verbose,
    const unsigned int total_number_of_trials) {

    unsigned int number_of_trials = 0;
    if (GTIS_usrp_has_gpsdo(usrp)) {
        if (verbose) {
            std::cout << boost::format("GPS status: ");
            std::cout.flush();
        }
        while ((! GTIS_is_usrp_gpsdo_locked(usrp))
               && (number_of_trials < total_number_of_trials)) {
            if (verbose) {
                std::cout << ". ";
                std::cout.flush();
            }
            boost::this_thread::sleep(boost::posix_time::seconds(1));
            number_of_trials++;
        }
        bool is_locked = number_of_trials != total_number_of_trials;
        if (verbose) {
            std::cout << (is_locked ? "LOCKED!" : "UNLOCKED!") << std::endl;
        }
        return is_locked;
    } else {
        if (verbose) {
            std::cout << "REF not found (something is wrong here)" << std::endl;
        }
    }
    return false;
}

/* Returns true if the GPSDO is locked, false otherwise (including if it is not found) */
bool GTIS_wait_for_usrp_ref_locked(uhd::usrp::multi_usrp::sptr usrp,
    const bool verbose,
    const unsigned int total_number_of_trials) {

    unsigned int number_of_trials = 0;
    if (GTIS_usrp_has_ref(usrp)) {
        if (verbose) {
            std::cout << boost::format("REF status: ");
            std::cout.flush();
        }
        while ((! GTIS_is_usrp_ref_locked(usrp))
               && (number_of_trials < total_number_of_trials)) {
            if (verbose) {
                std::cout << ". ";
                std::cout.flush();
            }
            boost::this_thread::sleep(boost::posix_time::seconds(1));
            number_of_trials++;
        }
        bool is_locked = number_of_trials != total_number_of_trials;
        if (verbose) {
            std::cout << (is_locked ? "LOCKED!" : "UNLOCKED!") << std::endl;
        }
        return is_locked;
    } else {
        if (verbose) {
            std::cout << "GPS not found" << std::endl;
        }
    }
    return false;
}

bool GTIS_are_gps_do_and_usrp_time_aligned(
        uhd::usrp::multi_usrp::sptr usrp,
        const bool verbose) {

    GTIS_wait_for_pps_edge(usrp);
    long long current_usrp_time_ticks = (usrp->get_time_last_pps()).to_ticks(1.0);
    uhd::sensor_value_t current_gps_time = usrp->get_mboard_sensor("gps_time");
    int current_gps_time_ticks = current_gps_time.to_int();
    if (current_usrp_time_ticks == current_gps_time_ticks) {
        if (verbose) {
            std::cout << boost::format("OK\n>> GPS and USRP times are aligned to %ld tics (UTC: %s)\n")
                     % current_usrp_time_ticks
                     % boost::posix_time::to_simple_string(
                             boost::posix_time::from_time_t((time_t) current_usrp_time_ticks));
        }
        return true;
    } else {
        if (verbose) {
            std::cout << boost::format("FAIL\n>> GPS and USRP times are NOT aligned (USRP: %ld vs GPS: %ld)\n")
                % current_usrp_time_ticks
                % current_gps_time_ticks << std::endl;
        }
        return false;
    }
}

#define MICROSECONDS_PER_SECOND 1000000
#define MICROSECONDS_IN_HALF_A_SECOND 500000
/* Private function from the TJU MIMO sounder respository by DJPinei */
uhd::time_spec_t GTIS_rounded_time_for_next_pps(boost::posix_time::time_duration posix_universal_time_previous_pps){

//    std::cout << boost::format("\n [ROUNDED_TIME_FOR_NEXT_PPS] HOST time just after last PPS was %d:%d:%d,%d\n")
//               % posix_universal_time_previous_pps.hours()
//               % posix_universal_time_previous_pps.minutes()
//               % posix_universal_time_previous_pps.seconds()
//               % posix_universal_time_previous_pps.fractional_seconds();

    long fract_microseconds = posix_universal_time_previous_pps.total_microseconds()
          - ((unsigned long)posix_universal_time_previous_pps.total_seconds())*MICROSECONDS_PER_SECOND;

//    std::cout << boost::format("\n [ROUNDED_TIME_FOR_NEXT_PPS] Fract. microseconds for HOST time at PPS are %d (%d - %d [%ds])\n")
//               % fract_microseconds
//               % posix_universal_time_previous_pps.total_microseconds()
//               % (((unsigned long)posix_universal_time_previous_pps.total_seconds())*MICROSECONDS_PER_SECOND)
//               % posix_universal_time_previous_pps.total_seconds();

    long seconds_increment = long(2);
    if(fract_microseconds < MICROSECONDS_IN_HALF_A_SECOND){
        seconds_increment = long(1);
    }

    uhd::time_spec_t next_pps_time = uhd::time_spec_t(
              time_t(posix_universal_time_previous_pps.total_seconds() + seconds_increment),
              long(0),
              double(boost::posix_time::time_duration::ticks_per_second()));

//    time_t new_time(time_t(posix_universal_time_previous_pps.total_seconds() + seconds_increment));
//    struct tm * time_to_print = gmtime(&new_time);
//    std::cout << boost::format("\n [ROUNDED_TIME_FOR_NEXT_PPS] The time for the next PPS (+%ds) will be %d:%d:%d\n") % seconds_increment % time_to_print->tm_hour % time_to_print->tm_min % time_to_print->tm_sec;

    return(next_pps_time);

}

bool GTIS_align_usrp_time(uhd::usrp::multi_usrp::sptr usrp,
        const bool verbose) {

    if (GTIS_is_usrp_gpsdo_locked(usrp)) {
        if (verbose) {
            std::cout << boost::format(" Setting current UHD time to GPS time . . . ");
            std::cout.flush();
        }
        // waits for the PPS edge
        GTIS_wait_for_pps_edge(usrp);

        // sets current gps time to the usrp
        uhd::sensor_value_t gps_time_for_next_pps = usrp->get_mboard_sensor("gps_time");
        uhd::time_spec_t next_pps_time =
            uhd::time_spec_t::from_ticks((long long) gps_time_for_next_pps.to_int() + 1, 1.0);
        usrp->set_time_next_pps(next_pps_time);

        boost::this_thread::sleep(boost::posix_time::seconds(2.0));
        return GTIS_are_gps_do_and_usrp_time_aligned(usrp, verbose);
    } else {
        if (verbose) {
            std::cout << boost::format(" Setting current UHD time to HOST time . . . ");
            std::cout.flush();
        }
        // waits for the PPS edge
        GTIS_wait_for_pps_edge(usrp);

        // sets current host time to the usrp
        boost::posix_time::time_duration posix_universal_time =
                boost::posix_time::microsec_clock::universal_time()
                - boost::posix_time::from_time_t(0);
//        uhd::time_spec_t next_pps_time = uhd::time_spec_t(
//                time_t(posix_universal_time.total_seconds() + 1),
//                long(0),
//                double(boost::posix_time::time_duration::ticks_per_second()));
        uhd::time_spec_t next_pps_time = GTIS_rounded_time_for_next_pps(posix_universal_time);
        usrp->set_time_next_pps(next_pps_time);

        boost::this_thread::sleep(boost::posix_time::seconds(2.0));
        GTIS_wait_for_pps_edge(usrp);
        long long current_usrp_time_ticks = (usrp->get_time_last_pps()).to_ticks(1.0);
        posix_universal_time =
                boost::posix_time::microsec_clock::universal_time()
                - boost::posix_time::from_time_t(0);
        int current_host_time_ticks = uhd::time_spec_t(
                time_t(posix_universal_time.total_seconds()),
                long(posix_universal_time.fractional_seconds()),
                double(boost::posix_time::time_duration::ticks_per_second())).to_ticks(1.0);
        if (current_usrp_time_ticks == current_host_time_ticks) {
            if (verbose) {
                std::cout << boost::format("OK\n>> HOST and USRP times are aligned to %ld tics (UTC: %s)\n")
                         % current_usrp_time_ticks
                         % boost::posix_time::to_simple_string(
                                 boost::posix_time::from_time_t(
                                    (time_t) current_usrp_time_ticks));
            }
            return true;
        } else {
            if (verbose) {
                std::cout << boost::format("FAIL\n>> HOST and USRP times are NOT aligned USRP ticks: %ld vs PC ticks: %ld. \n\n")
                    % current_usrp_time_ticks
                    % current_host_time_ticks << std::endl;
            }
            return false;
        }
    }
}

void GTIS_print_all_usrp_sensor_names(uhd::usrp::multi_usrp::sptr usrp) {
    std::vector<std::string> sensor_names = usrp->get_mboard_sensor_names(0);
    std::cout << boost::format("Sensor names: \"%s\"\n")
        % boost::algorithm::join(sensor_names, ", ");
    //prints all sensor values
    try {
        for (std::vector<std::string>::iterator it = sensor_names.begin();
            it != sensor_names.end();
            ++it) {

            std::cout << boost::format("%s: \"%s\"\n")
                % *it
                % (usrp->get_mboard_sensor(*it)).to_pp_string();
        }
    } catch (std::exception &e) {
        std::cout << "Problem found accessing sensor value." << std::endl;
    }
}

void GTIS_sleep_current_thread(long secs, bool verbose) {
    if (verbose) {
        std::cout << boost::format("Sleeping %d seconds for configuration . . . ")
                     % secs << std::endl;
    }
    boost::this_thread::sleep(boost::posix_time::seconds(secs));
}


int GTIS_write_nmea_data_from_gpsdo_to_file (
        uhd::usrp::multi_usrp::sptr usrp,
        const std::string output_file_name,
        const unsigned int seconds_between_measures,
        volatile bool * stop_nmea_writter_local,
        const bool verbose) {

    // Set realtime thread priority
    bool priority_writer = uhd::set_thread_priority_safe(0,true);

    // If not possible to assign priority, print on screen
    if (not priority_writer && verbose){
      std::cout << "PERS NMEA_WRITTER: Error setting priority to writer thread" << std::endl;
    }

    // Checks for the GPSDO, GPGGA, and GPRMC
    std::vector<std::string> sensor_names = usrp->get_mboard_sensor_names(0);

    if (std::find(sensor_names.begin(), sensor_names.end(), "gps_locked") == sensor_names.end()) {
        std::cout << boost::format("PERS NMEA_WRITTER: Error! GPSDO is not available.\n");
        return ~0;
    }

    if (std::find(sensor_names.begin(), sensor_names.end(), "gps_gpgga") == sensor_names.end()) {
        std::cout << boost::format("PERS NMEA_WRITTER: Error! GPS GPGGA is not available.\n");
        return ~0;
    }

    if (std::find(sensor_names.begin(), sensor_names.end(), "gps_gprmc") == sensor_names.end()) {
        std::cout << boost::format("PERS NMEA_WRITTER: Error! GPS GPRMC is not available.\n");
        return ~0;
    }

    if (not GTIS_is_usrp_gpsdo_locked(usrp)) {
        std::cout << boost::format("PERS NMEA_WRITTER: Warning! GPSDO is not locked.\n");
    }

    FILE * file_descriptor = fopen(output_file_name.c_str(), "w");

    if (file_descriptor ==  NULL) {
        if (verbose) {
            std::cout << boost::format("Error opening the file with name \"%s\"")
                % output_file_name << std::endl;
        }
      return ~0;
    }

    // Writes an informative header with a timestamp
    long long current_usrp_time_ticks = (usrp->get_time_now()).to_ticks(1.0);
    std::string timestamp =
            boost::posix_time::to_simple_string(
                boost::posix_time::from_time_t((time_t) current_usrp_time_ticks));
    fprintf(file_descriptor,
            "# PERS NMEA_WRITTER \n# File created at UTC time: %s\n#\n", timestamp.c_str());

    while (not (*stop_nmea_writter_local)) {
        std::string gpgga_string = (usrp->get_mboard_sensor("gps_gpgga")).value;
        std::string gprmc_string = (usrp->get_mboard_sensor("gps_gprmc")).value;
        fprintf(file_descriptor, "%s\n%s\n", gpgga_string.c_str(), gprmc_string.c_str());
        GTIS_sleep_current_thread(seconds_between_measures, false);
    }

    fclose(file_descriptor);

    if (verbose) {
        std::cout << boost::format("NMEA_WRITTER: Finished!.\n");
    }
    return EXIT_SUCCESS;
}
