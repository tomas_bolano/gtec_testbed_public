#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <complex>
#ifdef POSIX_FILE_API
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <fcntl.h>
#else
	#include <stdio.h>
#endif

#include "gtis/uhd_lib.hpp"
#include "gtis/config.hpp"

#define GTIS_COMMAND_NAME "convert_gtis_file_to_usrp_format"

namespace po = boost::program_options;

/***********************************************************************
 * SIG_INT handler: needed to stop acquisition
 **********************************************************************/

static volatile bool stop_signal_called = false;
void sig_int_handler(int){
    stop_signal_called = true;
}

/***********************************************************************
 * Main function
 **********************************************************************/


int main(int argc, char *argv[]){

	//variables to be set by po
	std::string input_file_name;
	std::string output_file_names_list;
	std::string channels;

	std::vector < std::string > output_file_names;

	//setup the program options
	po::options_description desc("Allowed options");
	desc.add_options()
		("version", "prints out the version")
		("help", "help message")
		("input-file-name", po::value < std::string > (&input_file_name)->default_value("/home/tecrail/RXsignals/ReceivedFrames.usrp"), "an input filename for all channels")
		("output-file-names", po::value < std::string > (&output_file_names_list), "a list for the output filenames for all channels")
		("channels", po::value < std::string > (&channels)->default_value("0,1"), "the list of used channel numbers")
		;
	
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (argc == 1) {
		GTIS_PRINT_VERSION((char *)GTIS_COMMAND_NAME);
		std::cout << desc << std::endl;
        return EXIT_SUCCESS;
	}

	//print the version
	if (vm.count("version")) {
		GTIS_PRINT_VERSION((char *)GTIS_COMMAND_NAME);
        return EXIT_SUCCESS;
	}
	
	//print the help message
    if (vm.count("help")) {
    	GTIS_PRINT_VERSION((char *)GTIS_COMMAND_NAME);
		std::cout << boost::format("PERS convert multichannel USRP file to multiple files %s") % desc << std::endl;
        return EXIT_SUCCESS;
	}
	
	// detect which channels to use
	std::vector < std::string > channel_strings;
	std::vector < size_t > channel_nums;
	boost::split(channel_strings, channels, boost::is_any_of("\"',"), boost::token_compress_on);
	for (size_t ch = 0; ch < channel_strings.size(); ch++) {
			channel_nums.push_back(boost::lexical_cast < int >(channel_strings[ch]));
	}

	std::cout << boost::format("size of channel names string: %d") % (output_file_names.size()) << std::endl;
	std::cout << boost::format("size of channel nums string: %d") % (channel_nums.size()) << std::endl;

	if (file_name_splitter(output_file_names_list, output_file_names, channel_nums.size()) 
	!= EXIT_SUCCESS) {
		std::cerr << "Error splitting channel files " << std::endl;
	}	

    std::signal(SIGINT, &sig_int_handler);
    stop_signal_called = false;

    split_multichannel_output_file(
                input_file_name, output_file_names, & stop_signal_called);
	std::cout << std::endl << "Done!" << std::endl << std::endl;
    return EXIT_SUCCESS;
}
