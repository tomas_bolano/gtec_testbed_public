# Specifies the version of the GTIS release
set(GTIS_VERSION_MAJOR 0)
set(GTIS_VERSION_MINOR 6)
set(GTIS_VERSION_PATCH 3)
set(GTIS_VERSION_MAINT 0)
set(GTIS_VERSION_DESC "bidueiro (2018.07)")
