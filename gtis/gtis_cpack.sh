#!/bin/bash

# Builds binary distribution
cpack --config build/CPackConfig.cmake

# Builds source distribution
# Disabled by default
# cpack --config build/CPackSourceConfig.cmake

