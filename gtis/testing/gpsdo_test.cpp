#include <uhd/utils/thread.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/usrp/multi_usrp.hpp>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <iostream>
#include <complex>
#include <ctime>
#include <cstdlib>
#include <csignal>


#include "gtis/uhd_lib_utils.hpp"
#include "gtis/config.hpp"
#define GTIS_COMMAND_NAME "gpsdo_test"

static volatile bool stop_nmea_writter = false;
static volatile bool stop_signal_called = false;

void sig_int_handler(int){
    stop_signal_called = true;
}

namespace po = boost::program_options;

int UHD_SAFE_MAIN(int argc, char *argv[]){

    std::cout << boost::format("GPSDO_TEST: %s, %s") % __DATE__ % __TIME__ << std::endl;

    uhd::set_thread_priority_safe();

    //Threads
    boost::thread nmea_writter_th;

    //variables to be set by po
    std::string args;
    std::string wire;
    double seconds_in_future;
    size_t total_num_samps;
    double rate, master_clock_rate;
    std::string channel_list;

    //setup the program options
    po::options_description desc("Allowed options");
    desc.add_options()
		("version", "prints out the version")
        ("help", "help message")
        ("args", po::value<std::string>(&args)->default_value(""), "single uhd device address args")
        ("wire", po::value<std::string>(&wire)->default_value(""), "the over the wire type, sc16, sc8, etc")
        ("secs", po::value<double>(&seconds_in_future)->default_value(4), "number of seconds in the future to receive")
        ("nsamps", po::value<size_t>(&total_num_samps)->default_value(10000), "total number of samples to receive")
        ("rate", po::value<double>(&rate)->default_value(15e6), "signal sampling rate")
        ("master_clock_rate", po::value<double>(&master_clock_rate)->default_value(30e6), "master clock rate")
        ("dilv", "specify to disable inner-loop verbose")
        ("channels", po::value<std::string>(&channel_list)->default_value("0"), "which channel(s) to use (specify \"0\", \"1\", \"0,1\", etc)")
    ;
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (argc == 1) {
		GTIS_PRINT_VERSION((char *)GTIS_COMMAND_NAME);
		std::cout << desc << std::endl;
        return EXIT_SUCCESS;
	}

	//print the version
	if (vm.count("version")) {
		GTIS_PRINT_VERSION((char *)GTIS_COMMAND_NAME);
        return EXIT_SUCCESS;
	}
	
	//print the help message
    if (vm.count("help")) {
    	GTIS_PRINT_VERSION((char *)GTIS_COMMAND_NAME);
		std::cout << boost::format("PERS GPSDO_TEST: %s") % desc << std::endl;
        return EXIT_SUCCESS;
	}

    bool verbose = vm.count("dilv") == 0;

    //create a usrp device
    std::cout << std::endl;
    std::cout << boost::format("Creating the usrp device with: %s...") % args << std::endl;
    uhd::usrp::multi_usrp::sptr usrp = uhd::usrp::multi_usrp::make(args);
    //std::cout << boost::format("Using Device: %s") % usrp->get_pp_string() << std::endl;

    // set master clock rate
    std::cout << boost::format("Setting master clock rate: %f Msample/s\n") % (master_clock_rate/1e6);
    usrp->set_master_clock_rate(master_clock_rate);
    // set the rx signal sampling rate
    std::cout << boost::format("Setting RX signal sampling rate: %f Msample/s\n") % (rate/1e6);
    usrp->set_rx_rate(rate);

    // Required setup time as Ettus recomends
    GTIS_sleep_current_thread(2);
    std::cout << boost::format("Actual master clock rate: %f Msample/s\n")
                 % (usrp->get_master_clock_rate()/1e6);
    std::cout << boost::format("Actual RX signal sampling rate: %f Msample/s\n")
                 % (usrp->get_rx_rate()/1e6);


    GTIS_wait_for_usrp_gpsdo_locked(usrp, true, 10);
    GTIS_wait_for_usrp_ref_locked(usrp, true, 10);

    GTIS_align_usrp_time(usrp, true);

    std::cout << boost::format("\n ==============================================\n");
    std::cout << boost::format("\t USRP parameters \n");
    std::cout << boost::format(" ==============================================\n\n");

    std::cout << boost::format("Board name: %s\n")
        % usrp->get_mboard_name(0);
    std::cout << boost::format("Master clock rate: %2.4f MHz\n")
        % ((usrp->get_master_clock_rate(0))/1e6);
    std::cout << boost::format("RX bandwidth: %f MHz\n")
        % ((usrp->get_rx_bandwidth(0))/1e6);
    std::cout << boost::format("RX gain names: \"%s\"\n")
        % boost::algorithm::join(usrp->get_rx_gain_names(0), ", ");
    std::cout << boost::format("RX gain: %2.2f dB\n")
        % ((usrp->get_rx_gain(0))/1e6);

    std::cout << boost::format("\n");
    std::cout << boost::format("Time synchronized: %s")
        % (usrp->get_time_synchronized() ? "yes" : "no") << std::endl;
    std::cout << boost::format("Time sources: \"%s\"\n")
        % boost::algorithm::join(usrp->get_time_sources(0), ", ");
    std::cout << boost::format("Time source: %s\n")
        % usrp->get_time_source(0);
    std::cout << boost::format("Clock sources: \"%s\"\n")
        % boost::algorithm::join(usrp->get_clock_sources(0), ", ");
    std::cout << boost::format("Clock source: %s\n")
        % usrp->get_clock_source(0);

    std::cout << boost::format("\n");
    GTIS_print_all_usrp_sensor_names(usrp);

    stop_nmea_writter = false;
    std::cout << boost::format("Starting NMEA_WRITTER thread . . .\n");
    long seconts_between_measures = 1;
    nmea_writter_th = boost::thread(
                GTIS_write_nmea_data_from_gpsdo_to_file,
                usrp,
                "./nmea_log.txt",
                seconts_between_measures,
                &stop_nmea_writter,
                true);


    //detect which channels to use
    std::vector<std::string> channel_strings;
    std::vector<size_t> channel_nums;
    boost::split(channel_strings, channel_list, boost::is_any_of("\"',"));
    for(size_t ch = 0; ch < channel_strings.size(); ch++){
        size_t chan = boost::lexical_cast<int>(channel_strings[ch]);
        if(chan >= usrp->get_tx_num_channels() or chan >= usrp->get_rx_num_channels()){
            throw std::runtime_error("Invalid channel(s) specified.");
        }
        else channel_nums.push_back(boost::lexical_cast<int>(channel_strings[ch]));
    }

    std::cout << boost::format("USRP time: %.0f seconds\n") % ((usrp->get_time_last_pps()).get_full_secs());

    //std::cout << boost::format("Setting device timestamp to 0...") << std::endl;
    //usrp->set_time_now(uhd::time_spec_t(0.0));

    //create a receive streamer
    uhd::stream_args_t stream_args("fc32", wire); //complex floats
    stream_args.channels = channel_nums;
    uhd::rx_streamer::sptr rx_stream = usrp->get_rx_stream(stream_args);

    //setup streaming
    std::cout << std::endl;
    uhd::stream_cmd_t stream_cmd(uhd::stream_cmd_t::STREAM_MODE_NUM_SAMPS_AND_DONE);
    stream_cmd.num_samps = total_num_samps;
    stream_cmd.stream_now = false;
    std::cout << boost::format("USRP time: %.0f seconds\n") % ((usrp->get_time_last_pps()).get_full_secs());
    std::cout.flush();
    stream_cmd.time_spec = uhd::time_spec_t(seconds_in_future) + usrp->get_time_last_pps();
    std::cout << boost::format(
        "Begin streaming %d samples at time %.0f ..."
    ) % total_num_samps % stream_cmd.time_spec.get_full_secs() << std::endl;
    rx_stream->issue_stream_cmd(stream_cmd);

    //meta-data will be filled in by recv()
    uhd::rx_metadata_t md;

    //allocate buffer to receive with samples
    std::vector<std::complex<float> > buff(rx_stream->get_max_num_samps());
    std::vector<void *> buffs;
    for (size_t ch = 0; ch < rx_stream->get_num_channels(); ch++)
        buffs.push_back(&buff.front()); //same buffer for each channel

    //the first call to recv() will block this many seconds before receiving
    double timeout = seconds_in_future + 0.1; //timeout (delay before receive + padding)

    size_t num_acc_samps = 0; //number of accumulated samples
    while(num_acc_samps < total_num_samps){
        //receive a single packet
        size_t num_rx_samps = rx_stream->recv(
            buffs, buff.size(), md, timeout, true
        );

        //use a small timeout for subsequent packets
        timeout = 0.1;

        //handle the error code
        if (md.error_code == uhd::rx_metadata_t::ERROR_CODE_TIMEOUT) break;
        if (md.error_code != uhd::rx_metadata_t::ERROR_CODE_NONE){
            throw std::runtime_error(str(boost::format(
                "Receiver error %s"
            ) % md.strerror()));
        }

        if(verbose) std::cout << boost::format(
            "Received packet: %u samples, %.0f full secs, %f frac secs"
        ) % num_rx_samps % md.time_spec.get_full_secs() % md.time_spec.get_frac_secs() << std::endl;

        num_acc_samps += num_rx_samps;
    }

    if (num_acc_samps < total_num_samps) std::cerr << "Receive timeout before all samples received..." << std::endl;

    std::signal(SIGINT, &sig_int_handler);
    std::cout << " Writting NMEA records (press Ctrl + C to stop) . . ." << std::endl;
    while(not stop_signal_called) {
        GTIS_sleep_current_thread(1.0, false);
    }

    stop_nmea_writter = true;
    nmea_writter_th.join();

    //finished
    std::cout << std::endl << "Done!" << std::endl << std::endl;

    return EXIT_SUCCESS;
}
