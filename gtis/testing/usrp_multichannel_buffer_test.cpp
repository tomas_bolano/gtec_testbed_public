#include <boost/date_time/posix_time/posix_time.hpp>

#include <uhd/utils/thread.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/utils/static.hpp>
#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/exception.hpp>
#include <boost/program_options.hpp>
#include <boost/math/special_functions/round.hpp>
#include <boost/foreach.hpp>
#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <complex>
#include <csignal>
#include <cmath>
#include <limits>
#include <math.h> 
#include <time.h>
#include <fstream>

#include "gtis/usrp_multichannel_buffer.hpp"
#include "gtis/uhd_lib_utils.hpp"
#include "gtis/config.hpp"
#define GTIS_COMMAND_NAME "usrp_multichannel_buffer_test"

namespace po = boost::program_options;


/***********************************************************************
 * WRITE FUNCTION
 **********************************************************************/
 size_t write( usrp_multichannel_buffer * usrp_buffer, size_t number_of_samples_per_channel){

 	try{
 		//std::cout << "entering write" << std::endl;
		std::vector < std::complex < short > * > buffer(usrp_buffer->number_of_channels());

		//std::cout << "entering reference for buffer" << std::endl;
		usrp_buffer->reference_buffer_for_producer( &buffer);

		//std::cout << "exiting reference for buffer" << std::endl;
		//MOCK: write memory with USRP data. To simulate processing, sleep 0.1 secs
		boost::this_thread::sleep(boost::posix_time::milliseconds(100));

		//std::cout << boost::format("Producer thread: writting buffer %d") %index << std::endl;	
		usrp_buffer->produce(number_of_samples_per_channel); 	

 	} catch (const std::exception & e){
 		std::cout << "fail" << std::endl;
		std::cerr << "Got exception: " << e.what() << std::endl;
		return ~0;
 	}

 	return EXIT_SUCCESS;

 }



 /***********************************************************************
 * READ FUNCTION
 **********************************************************************/
 size_t read(usrp_multichannel_buffer * usrp_buffer){

 	//number of samples to read
 	size_t number_of_useful_samples_per_channel;

 	try{
		std::vector < std::complex < short > * > buffer(usrp_buffer->number_of_channels());
		usrp_buffer->reference_buffer_for_consumer( &buffer, number_of_useful_samples_per_channel);

		//MOCK: read memory to file. To simulate processing, sleep 80 ms
		//ATTENTION: number_of_useful_samples_per_channel: Used by read function.
		boost::this_thread::sleep(boost::posix_time::milliseconds(80));

		usrp_buffer->consume(); 		
 	} catch (const std::exception & e){
 		std::cout << "fail" << std::endl;
		std::cerr << "Got exception: " << e.what() << std::endl;
		return ~0;
 	}

 	return EXIT_SUCCESS;

 }


/***********************************************************************
 * CONSUMER THREAD
 **********************************************************************/
void consumer_thread(
        usrp_multichannel_buffer * usrp_buffer) {

	// Set realtime Thread priority
	uhd::set_thread_priority_safe();

	//sleep 2 seconds
	GTIS_sleep_current_thread(2);

	//read  10 times
	try{
		for (size_t i =0; i < 30; i++) {
			read(usrp_buffer);
		}
	}catch(...){
		std::cout << "Exception in consumer_thread." << std::endl;
	}	

}




/***********************************************************************
 * PRODUCER THREAD
 **********************************************************************/
void producer_thread(
 		usrp_multichannel_buffer * usrp_buffer,
 		size_t number_of_samples_per_channel){

	// Set realtime Thread priority
	uhd::set_thread_priority_safe();

	//sleep 2 seconds
	//boost::this_thread::sleep(boost::posix_time::seconds(5));
	GTIS_sleep_current_thread(2);
	
	//write  10 times
	try{
		for (size_t i =0; i < 11; i++) {
			write(usrp_buffer, number_of_samples_per_channel);
		}
	}catch(...){
		std::cout << "Exception in producer_thread." << std::endl;
	}

	//wait until consumer finish reading all positions
	GTIS_sleep_current_thread(2);


	try{
		for (size_t i =0; i < 20; i++) {
			write(usrp_buffer, number_of_samples_per_channel);
		}
	}catch(...){
		std::cout << "Exception in producer_thread." << std::endl;
	}

}



/***********************************************************************
 * MAIN
 **********************************************************************/
int UHD_SAFE_MAIN(int argc, char *argv[]){
	
    po::options_description desc("Allowed options");
    desc.add_options()
        ("version", "prints out the version");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    /* print the version */
    if (vm.count("version")) {
        GTIS_PRINT_VERSION((char *) GTIS_COMMAND_NAME);
        return EXIT_SUCCESS;
    }

	GTIS_PRINT_VERSION((char *)GTIS_COMMAND_NAME);
	std::cout << std::endl;
		
	uhd::set_thread_priority_safe();

	size_t number_of_buffers = 10;
	size_t number_of_channels = 2;
	size_t number_of_samples_per_channel = 100;

	usrp_multichannel_buffer usrp_buffer(number_of_buffers, number_of_channels, number_of_samples_per_channel);

	std::cout << "Launching consumer thread" << std::endl;
    boost::thread consumer(consumer_thread, &usrp_buffer); // Processing thread initialization

	std::cout << "Launching producer thread" << std::endl;
	boost::thread producer(producer_thread, &usrp_buffer, number_of_samples_per_channel); // Processing thread initialization

	producer.join();
	consumer.join();

	std::cout << "Done!" << std::endl;

	return EXIT_SUCCESS;
}
