function [success, versionString] = GTIS_getGTISNodeVersion(nodeParameters)
%GTIS_getGTISNodeVersion Summary of this function goes here
%   Detailed explanation goes here

[success, versionString] = ...
    GTIS_CommandGetGTISNodeVersion(nodeParameters.ip, nodeParameters.port, ...
    nodeParameters.socketTimeoutInMs);
end

