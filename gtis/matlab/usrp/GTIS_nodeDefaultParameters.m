function [ usrpNode ] = GTIS_nodeDefaultParameters(...
    usrpModel, nodeName, userName, ip, port, gtisParameters)
%
% usrpNode = GTIS_nodeDefaultParameters(usrpModel, nodeName, userName, 
%               ip, port, gtisParameters)
%   Provides the usrpNode structure with the default values for an USRP
%   node. It is the handler of the node. The following parameters are required:
%       usrpModel: the model of the USRP to be used, e.g., B210
%       nodeName: a reference name.
%       ip: IP address of the node.
%       port: the port of the node.
%       gtisParameters: structure provided by GTIS_init()
%

usrpAllowedModels = ...
    {'B200', 'B210', 'N200', 'N210', 'X300', 'X310'};
usrpModelFound = false;
for ii=1:length(usrpAllowedModels)
    usrpModelFound = strcmpi(usrpAllowedModels{ii}, usrpModel);
    if (usrpModelFound)
        break;
    end
end
if (~usrpModelFound)
    error(' Unsupported USRP model specified or wrong syntax.');
end

usrpNode.serialNumber = '';
usrpNode.Ntx = 1;
usrpNode.Nrx = 1;
usrpNode.fullDuplex = 0;
usrpNode.txRFCarrier = 2.6e9;
usrpNode.rxRFCarrier = 2.6e9;
usrpNode.signalSamplingRate = 16e6;
usrpNode.masterClockRate = 16e6;
usrpNode.referenceTimeSource = 'internal';
usrpNode.referenceClockSource = 'internal';
usrpNode.txGain = 80;
usrpNode.txAntennaMapping = 'TX/RX';
usrpNode.rxGain = 30;
usrpNode.txBandwidth = 32e6;
usrpNode.rxBandwidth = 32e6;
usrpNode.rxAntennaMapping = 'RX2'; % Default value = RX2, alternative value = TX/RX
usrpNode.ip = ip;
usrpNode.port = port;
usrpNode.socketTimeoutInMs = 2000;
usrpNode.numRequestedSamplesPerChannel = 0;

usrpNode.userName = userName;
usrpNode.name = nodeName;
usrpNode.usrpModel = usrpModel;

usrpNode.doAdjustTxPowerWithNtx = 1;
usrpNode.clippingFactor = 1.0;
usrpNode.nZeroSamples = 0;
usrpNode.rxSignalOffset = 0;

usrpNode.doAcquireWithBothAntennas = 0;
usrpNode.doGenerateNMEALog = 1;
usrpNode.secondsBetweenNMEAMeasurements = 2;
usrpNode.numberOfTrialsGPSLocked = 10;
usrpNode.numberOfTrialsREFLocked = 10;
usrpNode.numberOfTrialsLOLocked = 3;

usrpNode.usrpWaitForKeyboard = false; 
usrpNode.useNetworkCommands = true;
usrpNode.doWaitBeforeExiting = false;
usrpNode.doIgnoreLdLibraryPath = true;

% RX-only parameters for the usrp_multichannel_buffer and for the
% usrp args
usrpNode.RX_numberOfAntennaBuffers = 20000;
usrpNode.RX_recv_frame_size = 16384;
usrpNode.RX_num_recv_frames = 200;

% TX-only parameters for the usrp args
usrpNode.TX_send_frame_size = 32768;
usrpNode.TX_num_send_frames = 200;

% Paths can be relative or not and can be local or remote
usrpNode.TXExecutable = gtisParameters.TXExecutable;
usrpNode.RXExecutable = gtisParameters.RXExecutable;

% Paths for both local and remote nodes.
usrpNode.txSignalsInUSRPFormatFilenames = {...
    fullfile(gtisParameters.txSignalsTempDir, 'txframech0.usrp'), ...
    fullfile(gtisParameters.txSignalsTempDir, 'txframech1.usrp')};
usrpNode.rxSignalsInMultiplexedUSRPFormatFilename = ...
    fullfile(gtisParameters.rxSignalsTempDir, 'rxframech0ch1.usrp');

% If true, remote paths are used instead of local ones.
usrpNode.remote.txSignals = false;
usrpNode.remote.rxSignals = false;

% Remote paths by default.
usrpNode.remote.txSignalsInUSRPFormatFilenames = {...
    fullfile(gtisParameters.txSignalsTempDir, 'txframech0.remote.usrp'), ...
    fullfile(gtisParameters.txSignalsTempDir, 'txframech1.remote.usrp')};
usrpNode.remote.rxSignalsInMultiplexedUSRPFormatFilename = ...
    fullfile(gtisParameters.rxSignalsTempDir, 'rxframech0ch1.remote.usrp');

end
