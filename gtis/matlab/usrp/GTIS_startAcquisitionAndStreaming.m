function [success, rxSignalsMatlabFormatDecimated] = ...
    GTIS_startAcquisitionAndStreaming(...
    usrpNode, userSignalSamplingRate, numRequestedSamplesPerChannel, ...
    varargin)

rxSignalsMatlabFormatDecimated = [];

if (isempty(varargin))
    rxTime = 0;
else
    rxTime = uint64(varargin{1});
end

internalInterpolationFactor = ...
    round(usrpNode.signalSamplingRate/userSignalSamplingRate);
nTotalRequestedSamplesPerChannel = ...
    (usrpNode.rxSignalOffset + ...
    numRequestedSamplesPerChannel) .* internalInterpolationFactor;

[success, rxSignalsMatlabFormat] = GTIS_CommandStartStreamingRX(...
    usrpNode.ip, ...
    usrpNode.port, ...
    nTotalRequestedSamplesPerChannel, ...
    rxTime, ...
    usrpNode.socketTimeoutInMs);
if (~success)
    return;
end

% Low-pass filtering and decimation (resampling)
internalDecimationFactor = ...
    round(usrpNode.signalSamplingRate/userSignalSamplingRate);
if (internalDecimationFactor > 1)
    rxSignalsMatlabFormatDecimated = zeros(...
        size(rxSignalsMatlabFormat, 1), ...
        length(rxSignalsMatlabFormat(1, 1:internalDecimationFactor:end)));
    for streamIndex = 1:size(rxSignalsMatlabFormat, 1)
        rxSignalsMatlabFormatDecimated(streamIndex, :) = ...
            resample(rxSignalsMatlabFormat(streamIndex, :), 1, internalDecimationFactor);
    end
else
    rxSignalsMatlabFormatDecimated = rxSignalsMatlabFormat;
end