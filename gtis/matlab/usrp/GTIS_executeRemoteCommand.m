function [ success, unixFullCommand ] = GTIS_executeRemoteCommand(...
    gtisParameters, nodeParameters, gtisRemoteCommandStr, varargin)
%GTIS_executeRemoteCommand
%   Detailed explanation goes here
if (~isempty(varargin))
    terminalTitle = varargin{1};
else
    terminalTitle = '';
end

if ((~isempty(terminalTitle)) && (~isempty(gtisParameters.terminal.TitleOption)))
    terminalTitleOption = [...
        gtisParameters.terminal.TitleOption, ' "', terminalTitle, '"'];
else
    terminalTitleOption = '';
end

unixCommandForSSH = strrep(gtisRemoteCommandStr, '"', '\"');
if (isunix())
    unixCommandForSSH = strrep(unixCommandForSSH, '$', '\$');
end
terminalExecuteOptionSeparator = '"';

launchTerminalWithSSH =  [...
    gtisParameters.terminal.CommandName, ' ', ...
    terminalTitleOption, ' ', ...
    gtisParameters.terminal.ExecuteOption, ' ', ...
    gtisParameters.SecureShellCommandName, ' ', ...
    nodeParameters.userName, '@', nodeParameters.ip, ' ', ...
    terminalExecuteOptionSeparator, ' bash -c hostname; '];

if (nodeParameters.doWaitBeforeExiting)
    runAndWait = ...
        '; echo \"\"; read -p \" \" ';
else
    runAndWait = '; ';
end
unixFullCommand = [...
    launchTerminalWithSSH, ' ', ...
    unixCommandForSSH, ' ', ...
    runAndWait, ' ', terminalExecuteOptionSeparator, ' &'];
if (nodeParameters.doIgnoreLdLibraryPath)
    envBackup = getenv('LD_LIBRARY_PATH');
    setenv('LD_LIBRARY_PATH','');
end
[status, ~] = unix(unixFullCommand);
if (status == 0)
    success = 1;
else
    success = 0;
end
if (nodeParameters.doIgnoreLdLibraryPath)
    setenv('LD_LIBRARY_PATH', envBackup);
end
end

