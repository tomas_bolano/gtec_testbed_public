function [ cstr ] = GTIS_rxNodeStartCommand(usrpNode)
%GTIS_rxNodeStartCommand
%   Detailed explanation goes here

cstr = [usrpNode.RXExecutable, ' '];
cstr = [cstr, '--args='''];
if (~isempty(usrpNode.serialNumber))
    cstr = [cstr, 'serial=', usrpNode.serialNumber, ', '];
end
cstr = [cstr, 'master_clock_rate=', num2str(usrpNode.masterClockRate), ', '];
cstr = [cstr, 'recv_frame_size=', num2str(usrpNode.RX_recv_frame_size),', '];
cstr = [cstr, 'num_recv_frames=', num2str(usrpNode.RX_num_recv_frames), ''' '];
cstr = [cstr, '--rx_rf_frequency ', num2str(usrpNode.rxRFCarrier), ' '];
cstr = [cstr, '--sampling_rate ', num2str(usrpNode.signalSamplingRate), ' '];
cstr = [cstr, '--reference_clock_source ', usrpNode.referenceClockSource, ' '];
cstr = [cstr, '--reference_time_source ', usrpNode.referenceTimeSource, ' '];
cstr = [cstr, '--rx_bandwidth ', num2str(usrpNode.rxBandwidth), ' '];
cstr = [cstr, '--rx_gain ', num2str(usrpNode.rxGain), ' '];
cstr = [cstr, '--number_of_antenna_buffers ', num2str(usrpNode.RX_numberOfAntennaBuffers), ' '];
cstr = [cstr, '--num_requested_samples_per_channel ', ...
    num2str(usrpNode.numRequestedSamplesPerChannel), ' '];
cstr = [cstr, '--wait_for_key_pressed '];
if (usrpNode.usrpWaitForKeyboard)
    cstr = [cstr, 'true', ' '];    
else
    cstr = [cstr, 'false', ' '];
end
if ((usrpNode.Nrx == 2) || usrpNode.doAcquireWithBothAntennas)
    cstr = [cstr, '--channels_mapping "0,1" '];
else
    cstr = [cstr, '--channels_mapping "0" '];
end
cstr = [cstr, '--rx_antenna_mapping ', usrpNode.rxAntennaMapping, ' '];

if (usrpNode.doGenerateNMEALog)
    cstr = [cstr, '--generate_nmea_log '];
end
cstr = [cstr, '--seconds_between_nmea_measurements ', ...
    num2str(usrpNode.secondsBetweenNMEAMeasurements), ' '];
cstr = [cstr, '--number_of_trials_gps_locked ', ...
    num2str(usrpNode.numberOfTrialsGPSLocked), ' '];
cstr = [cstr, '--number_of_trials_ref_locked ', ...
    num2str(usrpNode.numberOfTrialsREFLocked), ' '];
cstr = [cstr, '--number_of_trials_lo_locked ', ...
    num2str(usrpNode.numberOfTrialsLOLocked), ' '];
if (usrpNode.remote.rxSignals)
    cstr = [cstr, '--output_file_name "', usrpNode.remote.rxSignalsInMultiplexedUSRPFormatFilename, '" '];
else
    cstr = [cstr, '--output_file_name "', usrpNode.rxSignalsInMultiplexedUSRPFormatFilename, '" '];
end
cstr = [cstr, '--net_commands '];
if (usrpNode.useNetworkCommands)
    cstr = [cstr, 'true'];
else
    cstr = [cstr, 'false'];
end
cstr = [cstr, ' --socket_port_number ', num2str(usrpNode.port), ' '];
end