function [success, usrpTimeLastPPS] = GTIS_getNodeTimeLastPPS(nodeParameters, varargin)
%GTIS_getGTISNodeTime Summary of this function goes here
%   Detailed explanation goes here

if (isempty(varargin))
    waitForPPSEdge = false;
else
    waitForPPSEdge = varargin{1} >= 1;
end
[success, usrpTimeLastPPS] = ...
    GTIS_CommandGetNodeTimeLastPPS(nodeParameters.ip, nodeParameters.port, ...
    waitForPPSEdge, nodeParameters.socketTimeoutInMs);
end

