function [success] = GTIS_waitForNodeReady(nodeParameters, varargin)
%GTIS_waitForNodeReady
%   Detailed explanation goes here

if (isempty(varargin))
    numberOfRetries = 5;
else
    numberOfRetries = varargin{1};
end
success = 0;
for ii = 1:numberOfRetries
    success = GTIS_CommandReady(...
        nodeParameters.ip, nodeParameters.port, ...
        nodeParameters.socketTimeoutInMs);
    if (~success)
        fprintf(' . ');
        pause(2);
    else
        success = 1;
        return
    end
end
end

