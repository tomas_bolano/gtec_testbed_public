function [success] = GTIS_startCyclicTransmission(nodeParameters, varargin)
%GTIS_startCyclicTransmission
%   Detailed explanation goes here
% success = 0 means already transmitting!
if (isempty(varargin))
    txTime = 0;
else
    txTime = uint64(varargin{1});
end
success = GTIS_CommandStartTX(...
    nodeParameters.ip, nodeParameters.port, ...
    txTime, ...
    nodeParameters.socketTimeoutInMs);
end

