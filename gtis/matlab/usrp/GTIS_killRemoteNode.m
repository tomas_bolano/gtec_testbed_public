function [ success ] = GTIS_killRemoteNode(GTIS_parameters, GTIS_USRPNodeParameters)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

gtisRemoteCommandStr = ['killall ', GTIS_parameters.TXExecutableFullPath];
success = GTIS_executeRemoteCommand(...
    GTIS_parameters, GTIS_USRPNodeParameters, gtisRemoteCommandStr, 'KILL REMOTE NODE', true);
end

