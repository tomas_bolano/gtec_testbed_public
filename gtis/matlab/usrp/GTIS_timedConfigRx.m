function [success] = GTIS_timedConfigRx(nodeParameters, varargin)
%GTIS_timedConfigTx
%   Detailed explanation goes here
% success = 0 means already transmitting!
if (isempty(varargin))
    configTime = 0;
else
    configTime = uint64(varargin{1});
end
success = GTIS_CommandTimedConfigRX(...
    nodeParameters.ip, nodeParameters.port, ...
    configTime, ...
    nodeParameters.socketTimeoutInMs);
end

