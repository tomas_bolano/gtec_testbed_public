function [ cstr ] = GTIS_txNodeStartCommand(usrpNode)
%GTIS_getGTISTxCommand
%   Detailed explanation goes here

cstr = [usrpNode.TXExecutable, ' '];
cstr = [cstr, '--args='''];
if (~isempty(usrpNode.serialNumber))
    cstr = [cstr, 'serial=', usrpNode.serialNumber, ', '];
end
cstr = [cstr, 'send_frame_size=', num2str(usrpNode.TX_send_frame_size),', '];
cstr = [cstr, 'num_send_frames=', num2str(usrpNode.TX_num_send_frames), ', '];
cstr = [cstr, 'master_clock_rate=', ...
    num2str(usrpNode.masterClockRate), ''' '];
cstr = [cstr, '--tx_rf_frequency ', ...
    num2str(usrpNode.txRFCarrier), ' '];
cstr = [cstr, '--sampling_rate ', ...
    num2str(usrpNode.signalSamplingRate), ' '];
cstr = [cstr, '--reference_clock_source ', ...
    usrpNode.referenceClockSource, ' '];
cstr = [cstr, '--reference_time_source ', ...
    usrpNode.referenceTimeSource, ' '];
cstr = [cstr, '--number_of_trials_gps_locked ', ...
    num2str(usrpNode.numberOfTrialsGPSLocked), ' '];
cstr = [cstr, '--number_of_trials_ref_locked ', ...
    num2str(usrpNode.numberOfTrialsREFLocked), ' '];
cstr = [cstr, '--number_of_trials_lo_locked ', ...
    num2str(usrpNode.numberOfTrialsLOLocked), ' '];
cstr = [cstr, '--tx_gain ', ...
    num2str(usrpNode.txGain), ' '];
cstr = [cstr, '--tx_bandwidth ', ...
    num2str(usrpNode.txBandwidth), ' '];
cstr = [cstr, '--socket_port_number ', ...
    num2str(usrpNode.port), ' '];
cstr = [cstr, '--net_commands '];
if (usrpNode.useNetworkCommands)
    cstr = [cstr, 'true'];
else
    cstr = [cstr, 'false'];
end
if (usrpNode.Ntx == 2)
    cstr = [cstr, ' --channels "0,1" '];
    if (~usrpNode.useNetworkCommands)
        cstr = [cstr, ' --input-files "'];
        if (usrpNode.remote.txSignals)
            cstr = [cstr, usrpNode.remote.txSignalsInUSRPFormatFilenames{1}, ','];
            cstr = [cstr, usrpNode.remote.txSignalsInUSRPFormatFilenames{2}, '" '];
        else
            cstr = [cstr, usrpNode.txSignalsInUSRPFormatFilenames{1}, ','];
            cstr = [cstr, usrpNode.txSignalsInUSRPFormatFilenames{2}, '" '];
        end
    end
elseif (usrpNode.Ntx == 1)
    cstr = [cstr, ' --channels "0" '];
    if (~usrpNode.useNetworkCommands)
        cstr = [cstr, ' --input-files "'];
        if (usrpNode.remote.txSignals)
            cstr = [cstr, usrpNode.remote.txSignalsInUSRPFormatFilenames{1}, '" '];
        else
            cstr = [cstr, usrpNode.txSignalsInUSRPFormatFilenames{1}, '" '];
        end
    end
end
cstr = [cstr, '--tx_antenna_mapping ', usrpNode.txAntennaMapping, ' '];
end

