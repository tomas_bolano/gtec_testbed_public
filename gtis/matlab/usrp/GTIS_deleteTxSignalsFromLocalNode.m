function GTIS_deleteTxSignalsFromLocalNode(txUSRP)
%GTIS_deleteTxSignalsFromLocalNode Summary of this function goes here
%   Detailed explanation goes here

for ii = 1:length(txUSRP.txSignalsInUSRPFormatFilenames)
    if (exist(txUSRP.txSignalsInUSRPFormatFilenames{ii}, 'file'))
        delete(txUSRP.txSignalsInUSRPFormatFilenames{ii});
    end
end
