function [success] = GTIS_copyRxSignalsFromRemoteNode(...
        gtisParameters, usrpNodeParameters)
%GTIS_copyRxSignalsFromRemoteNode
%   Detailed explanation goes here

success = 0;

if (usrpNodeParameters.remote.rxSignals)
    % Copy signals from the remote node
    % A single (multiplexed) file regardless 1 or 2 acquiring antennas
    scpCommand = [gtisParameters.SecureCopyCommandName, ' ', ...
        usrpNodeParameters.userName, '@', usrpNodeParameters.ip, ':' ...
        usrpNodeParameters.remote.rxSignalsInMultiplexedUSRPFormatFilename, ' '...
        usrpNodeParameters.rxSignalsInMultiplexedUSRPFormatFilename];
        
    success = GTIS_executeLocalCommand(scpCommand);
    if (success == 0)
        warning('Cannot copy the signals via SCP');
        return;
    else
        pause(1);
    end
    
    % Copy the .usrptimestamp file which is always generated
    scpCommand = [gtisParameters.SecureCopyCommandName, ' ', ...
        usrpNodeParameters.userName, '@', usrpNodeParameters.ip, ':' ...
        usrpNodeParameters.remote.rxSignalsInMultiplexedUSRPFormatFilename, ...
        '.usrptimestamp ', ...
        usrpNodeParameters.rxSignalsInMultiplexedUSRPFormatFilename, ...
        '.usrptimestamp '];
        
    success = GTIS_executeLocalCommand(scpCommand);
    if (success == 0)
        warning('Cannot copy the signals via SCP');
        return;
    end
    
    % Copy the .usrpnmea file in case it is generated
    if (usrpNodeParameters.doGenerateNMEALog)
        scpCommand = [gtisParameters.SecureCopyCommandName, ' ', ...
            usrpNodeParameters.userName, '@', usrpNodeParameters.ip, ':' ...
            usrpNodeParameters.remote.rxSignalsInMultiplexedUSRPFormatFilename, ...
            '.usrpnmea ', ...
            usrpNodeParameters.rxSignalsInMultiplexedUSRPFormatFilename, ...
            '.usrpnmea '];

        success = GTIS_executeLocalCommand(...
        '', '', '', '', scpCommand, ...
        false, false, false);
        if (success == 0)
            warning('Cannot copy the signals via SCP');
            return;
        end
    end
end
   
end

