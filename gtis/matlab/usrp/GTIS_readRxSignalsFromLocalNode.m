function [success, rxSignalsMatlabFormatDecimated, ...
    rxSignalsMatlabFormat] = ...
    GTIS_readRxSignalsFromLocalNode(rxUSRP, userSignalSamplingRate)
%GTIS_writeSignalsToLocalGTISNode
%   Detailed explanation goes here


success = 0;
rxSignalsMatlabFormatDecimated = [];
rxSignalsMatlabFormat = [];


[rxSignalsMatlabFormat, success] = GTIS_readUSRPMultichannelSignalsFromFile(...
    rxUSRP.rxSignalsInMultiplexedUSRPFormatFilename, ...
    Inf, ...
    rxUSRP.rxSignalOffset);
if (~success)
    return;
end
% Low-pass filtering and decimation (resampling)
internalDecimationFactor = ...
    round(rxUSRP.signalSamplingRate/userSignalSamplingRate);
if (internalDecimationFactor > 1)
    rxSignalsMatlabFormatDecimated = zeros(...
        size(rxSignalsMatlabFormat, 1), ...
        length(rxSignalsMatlabFormat(1, 1:internalDecimationFactor:end)));
    for streamIndex = 1:size(rxSignalsMatlabFormat, 1),
        rxSignalsMatlabFormatDecimated(streamIndex, :) = ...
            resample(rxSignalsMatlabFormat(streamIndex, :), 1, internalDecimationFactor);
    end
else
    rxSignalsMatlabFormatDecimated = rxSignalsMatlabFormat;
end
success = 1;

