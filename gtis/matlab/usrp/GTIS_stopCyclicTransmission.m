function [success] = GTIS_stopCyclicTransmission(nodeParameters)
%GTIS_waitForNodeReady
%   Detailed explanation goes here
success = GTIS_CommandStopTX(...
    nodeParameters.ip, nodeParameters.port, nodeParameters.socketTimeoutInMs);
end

