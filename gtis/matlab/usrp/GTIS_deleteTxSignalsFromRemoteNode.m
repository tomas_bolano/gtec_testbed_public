function [success, unixFullCommand] = ...
    GTIS_deleteTxSignalsFromRemoteNode(GTIS_parameters, txUSRP)
%GTIS_deleteTxSignalsFromRemoteNode Summary of this function goes here
%   Detailed explanation goes here

if (txUSRP.remote.txSignals)
    for ii = 1:txUSRP.Ntx
        rmCommand = ['rm ', ...
            txUSRP.remote.txSignalsInUSRPFormatFilenames{ii}];
        
        [success, unixFullCommand] = GTIS_executeRemoteCommand(...
            GTIS_parameters, txUSRP, rmCommand, '', false);
        if (success == 0)
            warning('Cannot delete the signals via SSH');
            return;
        end
    end
end
end