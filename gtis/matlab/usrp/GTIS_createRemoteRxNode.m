function [ success, unixFullCommand ] = GTIS_createRemoteRxNode(...
    GTIS_parameters, nodeParameters, varargin)
%GTIS_createRemoteRxNode Summary of this function goes here
%   Detailed explanation goes here

if (nargin >= 3)
    gtisRemoteCommandStr = varargin{1};
else
    gtisRemoteCommandStr = GTIS_rxNodeStartCommand(nodeParameters);
end

[success , unixFullCommand] = GTIS_executeRemoteCommand(...
    GTIS_parameters, nodeParameters, gtisRemoteCommandStr, ...
     ['GTIS remote node - ', nodeParameters.name, ...
    ' (', nodeParameters.userName, '@', ...
    nodeParameters.ip, ')']);

