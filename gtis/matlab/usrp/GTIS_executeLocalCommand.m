function [ success, unixFullCommand ] = GTIS_executeLocalCommand(...
    localCommandStr, varargin)
% GTIS_executeLocalCommand
%   Executes a command in the local computer by calling unix() function
%
%   GTIS_executeLocalCommand(localCommandStr) executes the command
%       defined in localCommandStr in the environment by default.
%
%    GTIS_executeLocalCommand(localCommandStr, terminalCommandName, 
%       terminalTitleOption, terminalTitle, terminalExecuteOption, 
%       doWaitBeforeExiting, doUseBashShell, doIgnoreLdLibraryPath)
%       Allows us to specify the command to start a new terminal with
%       its corresponding title.
%         

if (isempty(varargin))
    terminalCommandName = '';
    terminalTitleOption = '';
    terminalTitle = '';
    terminalExecuteOption = ''; 
    doWaitBeforeExiting = false;
    doUseBashShell = false;
    doIgnoreLdLibraryPath = false;
elseif (length(varargin) == 7)
    terminalCommandName = varargin{1};
    terminalTitleOption = varargin{2};
    terminalTitle = varargin{3};
    terminalExecuteOption = varargin{4}; 
    doWaitBeforeExiting = varargin{5};
    doUseBashShell = varargin{6};
    doIgnoreLdLibraryPath = varargin{7};
else
    error(' Wrong input parameters.');
end

if ((~isempty(terminalTitle)) && (~isempty(terminalTitleOption)))
    terminalTitleOption = [...
        terminalTitleOption, ' "', terminalTitle, '"'];
else
    terminalTitleOption = '';
end

if (doUseBashShell)
    unixCommand = strrep(localCommandStr, '"', '\"');
    if (isunix())
        unixCommand = strrep(unixCommand, '$', '\$');
    end
else
    unixCommand = localCommandStr;
end

if (isempty(terminalCommandName))
    launchTerminal = '';
else
    launchTerminal =  [...
        terminalCommandName, ' ', ...
        terminalTitleOption, ' ', ...
        terminalExecuteOption, ' '];
    if (doUseBashShell)
        launchTerminal = [launchTerminal, '  bash -c " '];
    end
end

if (doWaitBeforeExiting)
    runAndWait = ...
        '; echo \"\"; read -p \" \" ';
else
    if (isempty(launchTerminal))
        runAndWait = ' ';
    else
        if (doUseBashShell)
            runAndWait = '; ';
        else
            runAndWait = ' ';
        end
    end
end

unixFullCommand = [...
    launchTerminal, ' ', ...
    unixCommand, ' ', ...
    runAndWait];

if ((~isempty(launchTerminal)) && doUseBashShell)
    unixFullCommand = [unixFullCommand, ' " &'];
end

if (doIgnoreLdLibraryPath)
    envBackup = getenv('LD_LIBRARY_PATH');
    setenv('LD_LIBRARY_PATH','');
end
[status, ~] = unix(unixFullCommand);
if (status == 0)
    success = 1;
else
    success = 0;
end
if (doIgnoreLdLibraryPath)
    setenv('LD_LIBRARY_PATH', envBackup);
end
end

