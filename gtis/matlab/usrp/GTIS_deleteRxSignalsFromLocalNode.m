function GTIS_deleteRxSignalsFromLocalNode(rxUSRP)
%GTIS_deleteRxSignalsFromLocalNode Summary of this function goes here
%   Detailed explanation goes here

if (exist(rxUSRP.rxSignalsInMultiplexedUSRPFormatFilename, 'file'))
    delete(rxUSRP.rxSignalsInMultiplexedUSRPFormatFilename)
end

timestampFilename = ...
    [rxUSRP.rxSignalsInMultiplexedUSRPFormatFilename, '.usrptimestamp'];
if (exist(timestampFilename, 'file'))
    delete(timestampFilename);
end

nmeaFilename = ...
    [rxUSRP.rxSignalsInMultiplexedUSRPFormatFilename, '.usrpnmea'];
if (exist(nmeaFilename, 'file'))
    delete(nmeaFilename);
end
