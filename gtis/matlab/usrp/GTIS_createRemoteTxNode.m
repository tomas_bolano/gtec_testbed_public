function [ success, unixFullCommand ] = GTIS_createRemoteTxNode(...
    gtisParameters, nodeParameters, varargin)
%GTIS_createRemoteTxNode Summary of this function goes here
%   Detailed explanation goes here

if (nargin >= 3)
    gtisRemoteCommandStr = varargin{1};
else
    gtisRemoteCommandStr = GTIS_txNodeStartCommand(nodeParameters);
end

[success , unixFullCommand] = GTIS_executeRemoteCommand(...
    gtisParameters, nodeParameters, gtisRemoteCommandStr, ...
     ['GTIS remote node - ', nodeParameters.name, ...
    ' (', nodeParameters.userName, '@', ...
    nodeParameters.ip, ')']);

