function [success, unixFullCommand] = ...
    GTIS_deleteRxSignalsFromRemoteNode(GTIS_parameters, rxUSRP)
%GTIS_deleteRxSignalsFromRemoteNode Summary of this function goes here
%   Detailed explanation goes here

if (rxUSRP.remote.rxSignals)
    rmCommand = ['rm ', ...
        rxUSRP.remote.rxSignalsInMultiplexedUSRPFormatFilename];

    [success, unixFullCommand] = GTIS_executeRemoteCommand(...
        GTIS_parameters, rxUSRP, rmCommand, '', false);
    if (success == 0)
        warning('Cannot delete the signals via SSH');
        return;
    end
    
    rmCommand = ['rm ', ...
        rxUSRP.remote.rxSignalsInMultiplexedUSRPFormatFilename, ...
        '.usrptimestamp'];

    [success, unixFullCommand] = GTIS_executeRemoteCommand(...
        GTIS_parameters, rxUSRP, rmCommand, '', false);
    if (success == 0)
        warning('Cannot delete the signals via SSH');
        return;
    end
    
    rmCommand = ['rm ', ...
        rxUSRP.remote.rxSignalsInMultiplexedUSRPFormatFilename, ...
        '.usrpnmea'];

    [success, unixFullCommand] = GTIS_executeRemoteCommand(...
        GTIS_parameters, rxUSRP, rmCommand, '', false);
    if (success == 0)
        warning('Cannot delete the signals via SSH');
        return;
    end
end
end