function [success, actualGain, serverConnection] = ...
    GTIS_CommandRxBandwidth(ip, port, gain, varargin)
% GTIS_CommandTxGain 
% TODO

gtisCommandId = uint16(48);
[success, actualGain, serverConnection] = ...
    GTIS_ValueCommandResponseTemplate(...
    gtisCommandId, gain, ip, port, [], varargin{:});
end
