function [success, actualGain, serverConnection] = ...
    GTIS_CommandTxBandwidth(ip, port, gain, varargin)
% GTIS_CommandTxGain 
% TODO

gtisCommandId = uint16(27);
[success, actualGain, serverConnection] = ...
    GTIS_ValueCommandResponseTemplate(...
    gtisCommandId, gain, ip, port, [], varargin{:});
end
