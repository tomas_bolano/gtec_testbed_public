function [success, actualFrequency, serverConnection] = ...
    GTIS_CommandRxFrequency(ip, port, frequency, varargin)
% GTIS_CommandTxFrequency 
% TODO

gtisCommandId = uint16(45);
[success, actualFrequency, serverConnection] = ...
    GTIS_ValueCommandResponseTemplate(...
    gtisCommandId, frequency, ip, port, [], varargin{:});
end
