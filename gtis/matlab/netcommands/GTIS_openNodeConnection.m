function [success, serverConnection] = ...
    GTIS_openNodeConnection(ip, port, varargin)
%GTIS_openNodeConnection Summary of this function goes here
%   Detailed explanation goes here

success = [];
serverConnection = [];

import java.net.Socket
import java.net.InetSocketAddress
import java.io.*

if (isempty(varargin))
    timeout = 2000; % timeout in milliseconds
else
    timeout = varargin{1};
end

try
    serverConnection = Socket();
    serverConnection.connect(InetSocketAddress(ip, port), timeout);
    success = 1;
catch
    if (~isempty(serverConnection))
        serverConnection.close();
    end
    success = 0;
end
end