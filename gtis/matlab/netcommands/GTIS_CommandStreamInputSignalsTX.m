function [success, serverConnection] = GTIS_CommandStreamInputSignalsTX(...
    ip, port, txSignalInUSRPFormat, varargin)
% GTIS_CommandStartTX 
% TODO

success = 0;

if (~isa(txSignalInUSRPFormat, 'int16'))
    error(' txSignalInUSRPFormat must be of type ''int16''');
end

gtisCommandId = uint16(26);

if (isempty(varargin))
    timeout = 2000; % timeout in milliseconds
else
    timeout = varargin{1};
end

if (length(varargin) >= 2)
    keepServerConnection = varargin{2};
else
    keepServerConnection = 0;
end

% Open connection to the node
[success, serverConnection] = ...
    GTIS_openNodeConnection(ip, port, timeout);
if (~success)
    return;
end

% Send gtis command id
[success, serverConnection] = ...
    GTIS_sendNumericValueToNode(serverConnection, gtisCommandId);
if (~success)
    return;
end

numComplexSamplesPerChannel = floor(length(txSignalInUSRPFormat)/2);
if ((numComplexSamplesPerChannel <= 0) || ...
    (numComplexSamplesPerChannel > intmax('uint64')))
    error(' Wrong number of samples per channel to be sent.');    
end

% Send number of complex-valued samples per channel to be sent
[success, serverConnection] = GTIS_sendNumericValueToNode(...
    serverConnection, ...
    uint64(numComplexSamplesPerChannel));
if (~success)
    return;
end

% Receive gtis response id
[success, gtisResponseId, serverConnection] = ...
    GTIS_recvNumericValueFromNode(serverConnection, 'uint16');
success = success && gtisResponseId;
if (~success)
    return;
end

numChannels = size(txSignalInUSRPFormat, 1);

import java.net.Socket
import java.net.InetSocketAddress
import java.io.*


try  
    serverOutputStream = serverConnection.getOutputStream();
    serverDataOutputStream = DataOutputStream(serverOutputStream);
    
    for ch = 1:numChannels
        buffer = typecast(txSignalInUSRPFormat(ch, :), 'int8');
        serverDataOutputStream.write(buffer, 0, length(buffer));
    end    
    success = 1;
catch
    success = 0;
end

if (~success)
    return;
end

if (~keepServerConnection)
    [success, serverConnection] = ...
        GTIS_closeNodeConnection(serverConnection);
end

end



