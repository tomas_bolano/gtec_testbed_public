function [success, serverConnection] = ...
    GTIS_sendBufferToNode(serverConnection, buffer)
%GTIS_sendBufferToNode Summary of this function goes here
%   Detailed explanation goes here

success = 0;

import java.net.Socket
import java.io.*

if (~isa(buffer, 'char'))
    error(' Wrong value type. Must be ''char'' ');
end

if (isempty(serverConnection))
    error(' Empty server connection detected ');
end

try 
    serverOutputStream = serverConnection.getOutputStream();
    serverDataOutputStream = DataOutputStream(serverOutputStream);
    serverDataOutputStream.writeBytes(buffer);
    success = 1;
catch
    success = 0;
    return;
end
end