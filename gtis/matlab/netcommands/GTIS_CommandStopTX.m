function [ success ] = GTIS_CommandStopTX(ip, port, varargin)
% GTIS_CommandStopTX 
% TODO

gtisCommandId = uint16(22);
[success, ~] = ...
    GTIS_SimpleCommandResponseTemplate(...
    gtisCommandId, ip, port, [], varargin{:});
end
