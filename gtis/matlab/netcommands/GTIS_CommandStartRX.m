function [success, serverConnection] = GTIS_CommandStartRX(...
    ip, port, numRequestedSamplesPerChannel, varargin)
% GTIS_CommandStartTX 
% TODO

success = 0;

gtisCommandId = uint16(41);

if (isempty(varargin))
    timeout = 2000; % timeout in milliseconds
else
    timeout = varargin{1};
end

if (length(varargin) >= 2)
    keepServerConnection = varargin{2};
else
    keepServerConnection = 0;
end

% Open connection to the node
[success, serverConnection] = ...
    GTIS_openNodeConnection(ip, port, timeout);
if (~success)
    return;
end

% Send gtis command id
[success, serverConnection] = ...
    GTIS_sendNumericValueToNode(serverConnection, gtisCommandId);
if (~success)
    return;
end

if ((numRequestedSamplesPerChannel < 0) || ...
    (numRequestedSamplesPerChannel > intmax('uint64')))
    error(' number of requested samples per channel is too long ');    
end

% Send gtis command value
[success, serverConnection] = GTIS_sendNumericValueToNode(...
    serverConnection, ...
    uint64(numRequestedSamplesPerChannel));
if (~success)
    return;
end

% Receive gtis response id
[success, gtisResponseId, serverConnection] = ...
    GTIS_recvNumericValueFromNode(serverConnection, 'uint16');
if (~success)
    return;
end
if (~keepServerConnection)
    [success, serverConnection] = ...
        GTIS_closeNodeConnection(serverConnection);
end
success = success && gtisResponseId;
end



