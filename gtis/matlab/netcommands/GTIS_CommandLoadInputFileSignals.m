function [success, serverConnection] = GTIS_CommandLoadInputFileSignals(...
    ip, port, fileList, varargin)
% GTIS_CommandLoadInputFileSignals 
% TODO

success = 0;

gtisCommandId = uint16(23);

if (isempty(varargin))
    timeout = 2000; % timeout in milliseconds
else
    timeout = varargin{1};
end

if (length(varargin) >= 2)
    keepServerConnection = varargin{2};
else
    keepServerConnection = 0;
end

% Open connection to the node
[success, serverConnection] = ...
    GTIS_openNodeConnection(ip, port, timeout);
if (~success)
    return;
end

% Send gtis command id
[success, serverConnection] = ...
    GTIS_sendNumericValueToNode(serverConnection, gtisCommandId);
if (~success)
    return;
end

fileListLength = length(fileList);
if ((fileListLength < 0) || (fileListLength > intmax('uint64')))
    error(' fileList is too long ');
end
% Send gtis command value
[success, serverConnection] = ...
    GTIS_sendNumericValueToNode(serverConnection, uint64(fileListLength));
if (~success)
    return;
end

% Send file list to node
[success, serverConnection] = ...
    GTIS_sendBufferToNode(serverConnection, char(fileList));
if (~success)
    return;
end

% Receive gtis response id
[success, gtisResponseId, serverConnection] = ...
    GTIS_recvNumericValueFromNode(serverConnection, 'uint16');
if (~success)
    return;
end

if (~keepServerConnection)
    [success, serverConnection] = ...
        GTIS_closeNodeConnection(serverConnection);
end

success = success && gtisResponseId;
end

