function [success, usrpTimeLastPPS] = ...
    GTIS_CommandGetNodeTimeLastPPS(ip, port, waitForPPSEdge, varargin)
% GTIS_CommandGetGTISNodeTime 
% TODO

success = 0;
usrpTimeLastPPS = [];
gtisCommandId = uint16(130);

if (isempty(varargin))
    timeout = 2000; % timeout in milliseconds
else
    timeout = varargin{1};
end

% Open connection to the node
[success, serverConnection] = ...
    GTIS_openNodeConnection(ip, port, timeout);
if (~success)
    return;
end

% Send gtisCommandId
[success, serverConnection] = ...
    GTIS_sendNumericValueToNode(serverConnection, gtisCommandId);
if (~success)
    return;
end

% Send waitForPPSEdge
if (waitForPPSEdge)
    waitForPPSEdgeValue = 1;
else
    waitForPPSEdgeValue = 0;
end

[success, serverConnection] = ...
    GTIS_sendNumericValueToNode(serverConnection, waitForPPSEdgeValue);
if (~success)
    return;
end
    

% Receive gtisResponseId
[success, gtisResponseId, serverConnection] = ...
    GTIS_recvNumericValueFromNode(serverConnection, 'uint16');
success = success && gtisResponseId;
if (~success)
    return;
end

% Receive USRP time last PPS in full seconds
[success, usrpTimeLastPPS, serverConnection] = ...
    GTIS_recvNumericValueFromNode(serverConnection, 'uint64');
if (~success)
    return;
end

[success] = GTIS_closeNodeConnection(serverConnection);

end
