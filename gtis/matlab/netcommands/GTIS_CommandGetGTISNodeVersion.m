function [success, buffer] = ...
    GTIS_CommandGetGTISNodeVersion(ip, port, varargin)
% GTIS_CommandGetGTISNodeVersion 
% TODO

success = 0;
buffer = [];
gtisCommandId = uint16(120);

if (isempty(varargin))
    timeout = 2000; % timeout in milliseconds
else
    timeout = varargin{1};
end

% Open connection to the node
[success, serverConnection] = ...
    GTIS_openNodeConnection(ip, port, timeout);
if (~success)
    return;
end

% Send gtisCommandId
[success, serverConnection] = ...
    GTIS_sendNumericValueToNode(serverConnection, gtisCommandId);
if (~success)
    return;
end

% Receive gtisResponseId
[success, gtisResponseId, serverConnection] = ...
    GTIS_recvNumericValueFromNode(serverConnection, 'uint16');
success = success && gtisResponseId;
if (~success)
    return;
end

% Receive buffer length
[success, bufferLength, serverConnection] = ...
    GTIS_recvNumericValueFromNode(serverConnection, 'uint64');
if (~success)
    return;
end

% Receive buffer
[success, buffer, serverConnection] = ...
    GTIS_recvBufferFromNode(serverConnection, bufferLength);

if (~success)
    return;
end
[success] = GTIS_closeNodeConnection(serverConnection);

end
