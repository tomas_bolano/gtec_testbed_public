function [success, value, serverConnection] = ...
    GTIS_recvNumericValueFromNode(serverConnection, valueType)
%GTIS_sendCommand Summary of this function goes here
%   Detailed explanation goes here

success = 0;
value = [];

import java.net.Socket
import java.net.InetSocketAddress
import java.io.*

if ((~strcmpi(valueType, 'uint16')) && ...
    (~strcmpi(valueType, 'uint32')) && ...
    (~strcmpi(valueType, 'uint64')) && ...
    (~strcmpi(valueType, 'double')))
	error(' Wrong value type. Must be ''uint16'', ''uint64'', or ''double'' ');
end

if (isempty(serverConnection))
    error(' Empty server connection detected ');
end

try  
    % Receive the response
    serverInputStream = serverConnection.getInputStream();
    serverDataInputStream = DataInputStream(serverInputStream);
    
    if (strcmpi(valueType, 'uint16'))
        value = uint16(serverDataInputStream.readShort());
    elseif (strcmpi(valueType, 'uint32'))
        value = uint32(serverDataInputStream.readInt());
    elseif (strcmpi(valueType, 'uint64'))
        value = uint64(serverDataInputStream.readLong());
    elseif (strcmpi(valueType, 'double'))        
        % A double has 8 bytes
        % WARNING!
        % Bytes must be treated as int8 (and not uint8!!!) when readed 
        % using readByte() from the DataInputStream Java object.
        % According to the documentation of the method readByte() of the 
        % java.io.DataInput Java class, such a method:
        % "Reads and returns one input byte. The byte is treated as a 
        % SIGNED VALUE in the range -128 through 127, inclusive.
        %
        valueBytes = zeros(1, 8, 'int8'); 
        [~, ~, endian] = computer();
        if (endian == 'L')
            responseValueIndices = 8:-1:1;
        else
            responseValueIndices = 1:8;
        end
        numBytesReceived = 0;
        for ii = responseValueIndices
            valueBytes(ii) = int8(serverDataInputStream.readByte());
            numBytesReceived = numBytesReceived + 1;
        end
        if (numBytesReceived == 8)
            value = typecast(valueBytes, 'double');
        end
    else
        success = 0;
        return;
    end
    success = 1;
catch
    success = 0;
end
end