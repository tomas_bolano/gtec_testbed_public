function [success, serverConnection] = ...
    GTIS_closeNodeConnection(serverConnection)
%GTIS_closeNodeConnection Summary of this function goes here
%   Detailed explanation goes here

success = [];

import java.net.Socket
import java.net.InetSocketAddress
import java.io.*


try
    if (~isempty(serverConnection))
        serverConnection.close();
        success = 1;
        return;
    end
    success = 0;
catch
    success = 0;
end
end