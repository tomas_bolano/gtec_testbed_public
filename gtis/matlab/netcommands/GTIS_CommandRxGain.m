function [success, actualGain, serverConnection] = ...
    GTIS_CommandRxGain(ip, port, gain, varargin)
% GTIS_CommandTxGain 
% TODO

gtisCommandId = uint16(44);
[success, actualGain, serverConnection] = ...
    GTIS_ValueCommandResponseTemplate(...
    gtisCommandId, gain, ip, port, [], varargin{:});
end
