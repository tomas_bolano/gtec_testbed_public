function [ success ] = GTIS_CommandFinish(ip, port, varargin)
% GTIS_CommandFinish 
% TODO

gtisCommandId = uint16(110);
[success, ~] = ...
    GTIS_SimpleCommandResponseTemplate(...
    gtisCommandId, ip, port, [], varargin{:});
end