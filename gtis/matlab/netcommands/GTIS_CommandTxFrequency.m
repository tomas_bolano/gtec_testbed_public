function [success, actualFrequency, serverConnection] = ...
    GTIS_CommandTxFrequency(ip, port, frequency, varargin)
% GTIS_CommandTxFrequency 
% TODO

gtisCommandId = uint16(25);
[success, actualFrequency, serverConnection] = ...
    GTIS_ValueCommandResponseTemplate(...
    gtisCommandId, frequency, ip, port, [], varargin{:});
end