function [success, readSignalComplex, serverConnection] = GTIS_CommandStartStreamingRX(...
    ip, port, numRequestedSamplesPerChannel, rxTime, varargin)
% GTIS_CommandStartTX 
% TODO

success = 0;
readSignalComplex = [];

gtisCommandId = uint16(47);

if (isempty(varargin))
    timeout = 2000; % timeout in milliseconds
else
    timeout = varargin{1};
end

if (length(varargin) >= 2)
    keepServerConnection = varargin{2};
else
    keepServerConnection = 0;
end

% Open connection to the node
[success, serverConnection] = ...
    GTIS_openNodeConnection(ip, port, timeout);
if (~success)
    return;
end

% Send gtis command id
[success, serverConnection] = ...
    GTIS_sendNumericValueToNode(serverConnection, gtisCommandId);
if (~success)
    return;
end

if ((numRequestedSamplesPerChannel <= 0) || ...
    (numRequestedSamplesPerChannel > intmax('uint64')))
    error(' Wrong number of requested samples per channel.');    
end

% Send number of requested complex-valued samples per channel
[success, serverConnection] = GTIS_sendNumericValueToNode(...
    serverConnection, ...
    uint64(numRequestedSamplesPerChannel));
if (~success)
    return;
end

% Send rxTime
[success, serverConnection] = ...
    GTIS_sendNumericValueToNode(serverConnection, uint64(rxTime));
if (~success)
    return;
end

% Receive gtis response id
[success, gtisResponseId, serverConnection] = ...
    GTIS_recvNumericValueFromNode(serverConnection, 'uint16');
success = success && gtisResponseId;
if (~success)
    return;
end

% Get the number of channels
[success, numChannels_int16, serverConnection] = ...
    GTIS_recvNumericValueFromNode(serverConnection, 'uint16');
if (~success)
    return;
end
numChannels = double(numChannels_int16);

import java.net.Socket
import java.net.InetSocketAddress
import java.io.*

% Each sample consists of a pair of 16-bit samples (the I sample and the Q
% sample), therefore, two 16-bit integers are required to store the number
% of requested samples per channel.
bufferSamps_int16 = zeros(numChannels, 2 * numRequestedSamplesPerChannel, 'int16');
try  
    serverInputStream = serverConnection.getInputStream();
    serverDataInputStream = DataInputStream(serverInputStream);
       
    totalNumSamplesPerChannelReceived = 0;
    sampIndex = 1;
    while (numRequestedSamplesPerChannel > totalNumSamplesPerChannelReceived)        
        % Get the number of samples per channel in the chunk
        % Each chunk may contain up to 65535 samples.
        numUsefulComplexSamplesPerChannel_int16 = javaMethod('readShort', serverDataInputStream);
        numUsefulComplexSamplesPerChannel = double(numUsefulComplexSamplesPerChannel_int16);
        % 16-bit samples means 2 bytes per sample
        numIQSampsToReadPerChannel = 2 * numUsefulComplexSamplesPerChannel; 
        % A complex sample consists of 4 bytes, 2 bytes for the I and
        % another two bytes for the Q
        numBytesToReadPerChannel = 4 * numUsefulComplexSamplesPerChannel;
        for ch = 1:numChannels
            tempBuffer = javaMethod('recvByteArray', 'GtisBufferReader', ...
                serverDataInputStream, numBytesToReadPerChannel).';
            bufferSamps_int16(ch, ...
                sampIndex:sampIndex + numIQSampsToReadPerChannel - 1) = ...
                typecast(tempBuffer, 'int16');
        end

% % % %         Alternative approach. Doesn't save significant time.
% % %         tempBuffer = javaMethod('recvChunk', 'GtisBufferReader', ...
% % %             serverDataInputStream, numChannels_int16);
% % %         numUsefulComplexSamplesPerChannel = size(tempBuffer, 2)/4;
% % %         numIQSampsToReadPerChannel = 2 * numUsefulComplexSamplesPerChannel;
% % %         for ch = 1:numChannels
% % %             bufferSamps_int16(ch, ...
% % %                 sampIndex:sampIndex + numIQSampsToReadPerChannel - 1) = ...
% % %                 typecast(tempBuffer(ch, :), 'int16');
% % %         end
        
        sampIndex = sampIndex + numIQSampsToReadPerChannel;
        totalNumSamplesPerChannelReceived = totalNumSamplesPerChannelReceived + ...
            numUsefulComplexSamplesPerChannel;
    end
    
    % 16-bit samples are converted to complex doubles
    readSignalComplex = zeros(numChannels, numRequestedSamplesPerChannel);    
    for ch = 1:numChannels
        readSignalComplex(ch, :) = ...
            double(bufferSamps_int16(ch, 1:2:end)) + ...
            1j .* double(bufferSamps_int16(ch, 2:2:end));
    end
    
    success = 1;
catch
    success = 0;
end

if (~success)
    return;
end

if (~keepServerConnection)
    [success, serverConnection] = ...
        GTIS_closeNodeConnection(serverConnection);
end

end



