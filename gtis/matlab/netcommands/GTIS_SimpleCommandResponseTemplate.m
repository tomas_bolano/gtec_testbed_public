function [success, serverConnection] = ...
    GTIS_SimpleCommandResponseTemplate(...
    gtisCommandId, ip, port, serverConnection, varargin)
% GTIS_SimpleCommandResponseTemplate 
% TODO
    
success = 0;

if (isempty(varargin))
    timeout = 2000; % timeout in milliseconds
else
    timeout = varargin{1};
end

if (length(varargin) >= 2)
    keepServerConnection = varargin{2};
else
    keepServerConnection = 0;
end

% Open connection to the node
if (isempty(serverConnection))
    [success, serverConnection] = ...
        GTIS_openNodeConnection(ip, port, timeout);
    if (~success)
        return;
    end
end

% Send gtisCommandId
[success, serverConnection] = ...
    GTIS_sendNumericValueToNode(serverConnection, gtisCommandId);
if (~success)
    return;
end

% Receive gtisResponseId
[success, gtisResponseId, serverConnection] = ...
    GTIS_recvNumericValueFromNode(serverConnection, 'uint16');
if (~success)
    return;
end

if (~keepServerConnection)
    [success, serverConnection] = ...
        GTIS_closeNodeConnection(serverConnection);
end

success = success && gtisResponseId;
end
