function [success, serverConnection] = ...
    GTIS_sendNumericValueToNode(serverConnection, value)
%GTIS_sendNumericValueToNode Summary of this function goes here
%   Detailed explanation goes here

success = 0;

import java.net.Socket
import java.io.*

if ((~isa(value, 'uint16')) && ...
    (~isa(value, 'uint32')) && ...
    (~isa(value, 'uint64')) && ...
    (~isa(value, 'double')))
    error(' Wrong value type. Must be ''uint16'', ''uint32'', ''uint64'', or ''double'' ');
end

if (isempty(serverConnection))
    error(' Empty server connection detected ');
end

try
    serverOutputStream = serverConnection.getOutputStream();
    serverDataOutputStream = DataOutputStream(serverOutputStream);

    if (isa(value, 'uint16'))
        serverDataOutputStream.writeShort(value);
    elseif (isa(value, 'uint32'))
        serverDataOutputStream.writeInt(value);        
    elseif (isa(value, 'uint64'))
        serverDataOutputStream.writeLong(value);
    elseif (isa(value, 'double'))        
        % Convert double to a Java byte[] array. Note that Java output
        % streams consider a byte as a signed integer, therefore int8
        % is used instead of uint8.
        valueBytes = (typecast(value, 'int8'));

        % Sends always in big endian (network) order. If the client is
        % little endian, then the reverse order is taken.
        [~, ~, endian] = computer();
        if (endian == 'L')
            valueIndices = length(valueBytes):-1:1;
        else
            valueIndices = 1:length(valueBytes);
        end
        for ii = valueIndices
            serverDataOutputStream.writeByte(valueBytes(ii));
        end
    else
        success = 0;
        return;
    end
    success = 1;
catch
    success = 0;
end
end