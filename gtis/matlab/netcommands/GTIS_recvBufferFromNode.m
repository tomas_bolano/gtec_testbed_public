function [success, buffer, serverConnection] = ...
    GTIS_recvBufferFromNode(serverConnection, bufferLength)
%GTIS_recvBufferFromNode Summary of this function goes here
%   Detailed explanation goes here

success = 0;
value = [];

import java.net.Socket
import java.net.InetSocketAddress
import java.io.*

if (isempty(serverConnection))
    error(' Empty server connection detected ');
end

try  
    serverInputStream = serverConnection.getInputStream();
    serverDataInputStream = DataInputStream(serverInputStream);

    buffer = zeros(1, bufferLength, 'int8');
    for ii = 1:bufferLength
        buffer(ii) = serverDataInputStream.readByte();
    end
    buffer = char(buffer);
    success = 1;
catch
    success = 0;
end
end