import java.io.*;

class GtisBufferReader {

    public static byte[] recvByteArray(DataInput m_data_input, int numBytesToRecv) {
        byte[] buffer = new byte[numBytesToRecv];
        //System.out.println("Reading " + numBytesToRecv + " bytes");
        try {
            m_data_input.readFully(buffer, 0, numBytesToRecv);
        } catch (Exception e) {
            buffer = new byte[0];
            e.printStackTrace();
        }
        return buffer;
    }
    
    public static byte[][] recvChunk(DataInput dataInput, short numChannels) {
        byte[][] buffer = null;
        short numUsefulSampsPerChannel = 0;
        int numBytesToRecvPerChannel = 0;
        try {
            numUsefulSampsPerChannel = dataInput.readShort();
            numBytesToRecvPerChannel = 4 * numUsefulSampsPerChannel;
            buffer = new byte[numChannels][numBytesToRecvPerChannel];
            for (short ch = 0; ch < numChannels; ch++) {
                dataInput.readFully(buffer[ch], 0, numBytesToRecvPerChannel);
            }
        } catch (Exception e) {
            buffer = new byte[0][0];
            e.printStackTrace();
        }
        return buffer;
    }
}
