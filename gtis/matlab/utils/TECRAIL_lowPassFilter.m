function Hd = TECRAIL_lowPassFilter
%TECRAIL_LOWPASSFILTER Returns a discrete-time filter object.

%
% MATLAB Code
% Generated by MATLAB(R) 7.12 and the DSP System Toolbox 8.0.
%
% Generated on: 14-Feb-2014 00:17:11
%

% Elliptic Lowpass filter designed using FDESIGN.LOWPASS.

% All frequency values are normalized to 1.

Fpass = 0.15;    % Passband Frequency
Fstop = 0.2;     % Stopband Frequency
Apass = 0.05;    % Passband Ripple (dB)
Astop = 100;     % Stopband Attenuation (dB)
match = 'both';  % Band to match exactly

% Construct an FDESIGN object and call its ELLIP method.
h  = fdesign.lowpass(Fpass, Fstop, Apass, Astop);
Hd = design(h, 'ellip', 'MatchExactly', match);

% [EOF]
