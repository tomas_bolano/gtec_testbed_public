function signalMatrix = GTIS_pulseShapeFilterIQMod(...
    symbolMatrix, Fd, Ts, Delay, pulse, fc, fs, dacResolution, varargin)

% 
% signalMatrix = GTIS_pulseShapeFilterIQMod(...
%     symbolMatrix, Fd, Ts, Delay, pulse, fc, fs, dacResolution, varargin)
% 
% symbolMatrix is expected to have a mean power equal to nt, the number of
% TX antennas.
% The mean power of the output signals is close to dacResolution.^2 per
% antenna when the input TX power per antenna is equal to 1.
% 

nt = size(symbolMatrix, 1); % Number of TX antennas
nSamples = (size(symbolMatrix, 2) + 2*Delay)*Ts;
% nSamples = size(symbolMatrix, 2) * Ts;
signalMatrix = zeros(nt, nSamples);

filterTolerance = 0.05;

sign = -1;
if (~isempty(varargin))
    sign = varargin{1};
end

% For the I/Q modulation.
t = 2*pi*fc*(0:(1/fs):((nSamples - 1)/fs));
icos = cos(t);
qsin = sin(t);

for ii = 1:nt,
% %     signalMatrix(ii, :) = ...
% %         rcosflt(real(symbolMatrix(ii, :)), Fd, Ts, 'fir/sqrt/filter', pulse, [], filterTolerance).' .* icos + ...
% %         sign .* rcosflt(imag(symbolMatrix(ii, :)), Fd, Ts, 'fir/sqrt/filter', pulse, [], filterTolerance).' .* qsin;
% %     
    signalMatrix(ii, :) = ...
        conv(pulse.Numerator, upsample(real(symbolMatrix(ii, :)), Ts)) .* icos + ...
        sign .* conv(pulse.Numerator, upsample(imag(symbolMatrix(ii, :)), Ts)) .* qsin;
end

% Signal scaling is proportinal to all TX antennas, otherwise if the signal
% to be transmitted by different antennas has a different mean power, such
% diferences would be canceled, which is not desired in i.e. precoding.
normFactor = max(abs(signalMatrix(:)));
if (normFactor == 0)
    normFactor = 1;
end
if (dacResolution > 0)
    signalMatrix = signalMatrix .* (dacResolution ./ normFactor);
end