function [symbolMatrix] = GTIS_matchedFilterIQDemod(...
    signalMatrix, Fd, Ts, Delay, pulse, fc, fs, varargin)
%
% [symbolMatrix] = GTIS_matchedFilterIQDemod(...
%     signalMatrix, Fd, Ts, Delay, pulse, fc, fs, varargin)
%
% TODO
%

[nr, nSamples] = size(signalMatrix);
nSymbols = (nSamples / Ts) - 2*Delay;
% nSymbols = (nSamples / Ts);
symbolMatrix = zeros(nr, nSymbols);

if (length(fc) == 1)
    fc = fc.*ones(1, nr);
end

sign = -1;
if(~isempty(varargin))
    sign = varargin{1};
end

t = 2*pi*(0:(1/fs):(nSamples - 1)/fs);
% % icos = cos(t);
% % qsin = sin(t);

% pulselr = fliplr(pulse);

for nrIndex = 1:nr,
% %     tmpSig1 = (rcosflt(signalMatrix(nrIndex, :) .* cos(fc(nrIndex)*t), ...
% %             Fd, Ts, 'Fs/filter', pulselr) + ...
% %         sign .* 1j.* rcosflt(signalMatrix(nrIndex, :) .* sin(fc(nrIndex)*t), ...
% %             Fd, Ts, 'Fs/filter', pulselr)).';
% %     
    tmpSig1 = conv(fliplr(pulse.Numerator), signalMatrix(nrIndex, :) .* cos(fc(nrIndex)*t)) + ...
        sign .* 1j .* conv(fliplr(pulse.Numerator), signalMatrix(nrIndex, :) .* sin(fc(nrIndex)*t));
    tmpSig2 = tmpSig1(1 + 2*Delay*Ts : end - 2*Delay*Ts);
    symbolMatrix(nrIndex, :) = tmpSig2(1:Ts:end);
end