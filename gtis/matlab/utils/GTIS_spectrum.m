function GTIS_spectrum(simb_tx, Fs)
%
% GTIS_spectrum(signal, Fs)
%
% TODO: update deprecated functions
%

Nfft = 1024;
% Power Spectral Density estimation using Welch estimator
% figure;
Hs = spectrum.welch('Hamming', Nfft);
Hpsd = Hs.psd(simb_tx, 'NFFT', Nfft, 'Fs', Fs, 'CenterDC', 1, ...
    'ConfLevel', 0.95);
%plot(Hpsd); % Plots spectrum with frequencies in MHz and not normalized PSD
XFrequencies = Hpsd.Frequencies ./ 1e6; % Convert frequencies from Hz to MHz
normFactor = max(abs(Hpsd.Data));
YPSD = 10*log10(Hpsd.Data ./ normFactor); % Normalize PSD
plot(XFrequencies, YPSD, 'k-', 'LineWidth', 3);
axisLimits = axis;
set(gca, 'xtick', axisLimits(1):2:axisLimits(2));
set(gca, 'ytick', axisLimits(3):5:axisLimits(4));
set(gca, 'GridLineStyle', '-');
set(gca, 'MinorGridLineStyle', '-');
set(gca, 'LineWidth', 0.5);
grid on;
xlabel('frequency [MHz]');
ylabel('PSD [dB]');
% title(' Power Spectral Density (PSD) in dB/Hz vs MHz');
