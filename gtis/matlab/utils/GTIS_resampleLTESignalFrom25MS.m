%
% lteSignal = GTIS_resampleLTESignalFrom25MS(usrpSignal, doDoubleDecimationFactor)
% 
% LTE signals are defined at a sample rate of 30.72 Msample/s, which is not
% compatible with the sampling rate of some USRPs fixed at 100 Msample/s.
% Usually, a common transfer rate between host and USRP is 25 Msample/s
% (so the USRP will interpolate/decimate the signal by a factor of four).
% This function allows for resampling a USRP signal defined at 25
% Msample/s (complex-valued samples) to a 30.72 Msample/s signal as defined
% by LTE.
%
% If doDoubleDecimationFactor == true, then the source signal is at
% 15.36 Msample/s
%
function resampledSignal = GTIS_resampleLTESignalFrom25MS(usrpSignal,doDoubleDecimationFactor)

if (doDoubleDecimationFactor)
    decimationFactor2 = 2;
else
    decimationFactor2 = 1;
end


% Parameters
interpolationFactor = 768; % (2^8)*3
decimationFactor = 625 * decimationFactor2; % 5^4
filterLength = 100;
%

resampledSignal = resample(usrpSignal, interpolationFactor, decimationFactor, filterLength);