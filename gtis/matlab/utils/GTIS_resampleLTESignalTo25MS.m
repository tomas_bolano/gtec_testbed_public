%
% usrpSignal = GTIS_resampleLTESignalTo25MS(lteSignal, doDoubleInterpolationFactor)
% 
% LTE signals are defined at a sample rate of 30.72 Msample/s, which is not
% compatible with the sampling rate of some USRPs fixed at 100 Msample/s.
% Usually, a common transfer rate between host and USRP is 25 Msample/s
% (so the USRP will interpolate/decimate the signal by a factor of four).
% This function allows for resampling an LTE signal defined at 30.72
% Msample/s (complex-valued samples, i.e. pairs of doubles) to a 
% 25 Msample/s signal valid for being passed to the USRP.
%
% If doDoubleInterpolationFactor == true, then the source signal is at
% 15.36 Msample/s
%
function usrpSignal = GTIS_resampleLTESignalTo25MS(lteSignal, doDoubleInterpolationFactor)

if (doDoubleInterpolationFactor)
    interpolationFactor2 = 2;
else
    interpolationFactor2 = 1;
end

% Parameters
interpolationFactor = 625 * interpolationFactor2; % 5^4
decimationFactor = 768; % (2^8)*3
filterLength = 100;
%

usrpSignal = resample(lteSignal, interpolationFactor, decimationFactor, filterLength);