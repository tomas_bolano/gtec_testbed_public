function [ GTIS_parameters ] = GTIS_init(varargin)
%
% GTIS_parameters = GTIS_init()
%   First function to be called before using GTIS.
%   With no input parameter tries to guess the path for GTIS Matlab files.
%   Requires that GTIS Matlab files are installed in a valid GTIS source
%   tree.
%
%   Returns an struct with all relevant GTIS parameters.
%
% GTIS_parameters = GTIS_init(paramName, paramValue)
%   Allows for specifying input values for certain parameters described
%   below:
%       'gtisMatlabPath': path to the gtis/matlab directory in the GTIS
%       source tree.
%       'txSignalsTempDir': directory for TX signals, $HOME/gtis/txsignals
%       by default.
%       'rxSignalstempDir': directory for RX signals, $HOME/gtis/rxsignals
%       by default.
%       'printGtisVersion': if false, then the GTIS version is not printed
%       out at the end. True by default.
%       'sshPuttyPrivateKeyPath': Windows OS only. Full path for the 
%       private PuTTY key (.ppk) when PuTTY is used as the SSH client.
%       The default value is obtained as 
%           fullfile(getenv('HOMEPATH'), '.ssh', 'id_rsa.ppk')
%       'conEmuAbsolutePath': Windows OS only. Full path for the ConEmu 
%       terminal (Windows OS). The default value is the usual install path
%       obtained as
%           fullfile(getenv('ProgramFiles'), 'ConEmu', 'ConEmu64.exe')
%
%       Note that defining 'sshPuttyPrivateKeyPath' and 'conEmuAbsolutePath' 
%       is recommended in Windows OS unless corresponding system paths are
%       redefined. GTIS_init() does not raise any error nor warning if
%       these parameters are not defined in Windows OS and tries to use the
%       default values specified above.
%

GTIS_parameters = [];

minArgs = 0;
maxArgs = 10;
narginchk(minArgs, maxArgs);
inparams = struct(...
    'gtisMatlabPath', [], ...
    'txSignalsTempDir', [], ...
    'rxSignalsTempDir', [], ...
    'printGtisVersion', [], ...
    'sshPuttyPrivateKeyPath', [], ...
    'conEmuAbsolutePath', []);

% Parses all input parameters
if (~isempty(varargin))
    if (mod(nargin, 2) == 1)
        error(' Provide name-value pairs of parameters.');
    end
    
    ii = 1;
    while ((ii + 1) <= nargin)
        paramName = varargin{ii};
        paramValue = varargin{ii + 1};
        ii = ii + 2;
        
        switch paramName
            case 'gtisMatlabPath'
                inparams.gtisMatlabPath = paramValue;
            case 'txSignalsTempDir'
                inparams.txSignalsTempDir = paramValue;
            case 'rxSignalsTempDir'
                inparams.rxSignalsTempDir = paramValue;
            case 'printGtisVersion'
                inparams.printGtisVersion = paramValue;
            case 'sshPuttyPrivateKeyPath'
                inparams.sshPuttyPrivateKeyPath = paramValue;
            case 'conEmuAbsolutePath'
                inparams.conEmuAbsolutePath = paramValue;
            otherwise
                error('Unknown input param name');
        end        
    end
end

% If no path is provided, then use this function's path
if (isempty(inparams.gtisMatlabPath))
    gtisMatlabPath = fileparts(mfilename('fullpath'));
else    
    gtisMatlabPath = inparams.gtisMatlabPath;
end

GTIS_parameters.MatlabPath = genpath(gtisMatlabPath);
addpath(GTIS_parameters.MatlabPath);

gtisJavaClassPath = fullfile(gtisMatlabPath, 'netcommands');
javaaddpath(gtisJavaClassPath);

GTIS_parameters.name = 'GTIS';

% TODO: let cmake provide a simple function with the GTIS version
gtisVersion = parseGtisVersion(gtisMatlabPath);
if (isempty(gtisVersion))
    error(' Cannot find GTIS file version. ');
end
GTIS_parameters.version = gtisVersion; 

% Default names for GTIS executables
GTIS_parameters.TXExecutable = 'gtis_tx';
GTIS_parameters.RXExecutable = 'gtis_rx';

% Temporary folders for storing signals
if (isempty(inparams.txSignalsTempDir))
    if (ispc())
        homeDir = getenv('HOMEPATH');
    else
        homeDir = getenv('HOME');
    end
    txSignalsTempDir = ...
        fullfile(homeDir, 'gtis', 'txsignals');
else
    txSignalsTempDir = inparams.txSignalsTempDir;
end
if (isempty(inparams.rxSignalsTempDir))
    if (ispc())
        homeDir = getenv('HOMEPATH');
    else
        homeDir = getenv('HOME');
    end
    rxSignalsTempDir = ...
        fullfile(homeDir, 'gtis', 'rxsignals');
else
    rxSignalsTempDir = inparams.rxSignalsTempDir;
end
if (~exist(txSignalsTempDir, 'dir'))
    mkdir(txSignalsTempDir);
end
if (~exist(rxSignalsTempDir, 'dir'))
    mkdir(rxSignalsTempDir);
end
GTIS_parameters.txSignalsTempDir = txSignalsTempDir;
GTIS_parameters.rxSignalsTempDir = rxSignalsTempDir;

% Specific terminal parametrization for non-Windows OS and Windows OS
if (~ispc())
    GTIS_parameters.terminal.CommandName = ...
        'xterm -fa ''Monospace'' -fs 11 -fg white -bg black -sl 8192';
    GTIS_parameters.terminal.TitleOption = '-T';
    GTIS_parameters.terminal.ExecuteOption = '-e';

    GTIS_parameters.SecureCopyCommandName = 'scp';
    GTIS_parameters.SecureShellCommandName = 'ssh';
else
    if (isempty(inparams.conEmuAbsolutePath))
        % Attemp to find ConEmu
        conEmuDefaultPath = ...
            fullfile(getenv('ProgramFiles'), 'ConEmu', 'ConEmu64.exe');
        if (exist(conEmuDefaultPath, 'file'))
            terminalCommandName = conEmuDefaultPath;
        else
            terminalCommandName = '';
        end
    else
        terminalCommandName = inparams.conEmuAbsolutePath;
    end
    if (~isempty(terminalCommandName))
        GTIS_parameters.terminal.CommandName = ...
            [' "', terminalCommandName, ...
            '" -WndW 50% -WndH 50%  -NoUpdate -NoMulti '];
        GTIS_parameters.terminal.TitleOption = ' -Title ';
        GTIS_parameters.terminal.ExecuteOption = ' -run ';
    else
        GTIS_parameters.terminal.CommandName = '';
        GTIS_parameters.terminal.TitleOption = '';
        GTIS_parameters.terminal.ExecuteOption = '';
    end
    if (isempty(inparams.sshPuttyPrivateKeyPath))
        sshPuttyPrivateKeyPath = ...
            fullfile(getenv('HOMEPATH'), '.ssh', 'id_rsa.ppk');
    else
        sshPuttyPrivateKeyPath = inparams.sshPuttyPrivateKeyPath;
    end

    GTIS_parameters.SecureShellCommandName = ...
        ['plink -ssh -i ', sshPuttyPrivateKeyPath,' '];

    GTIS_parameters.SecureCopyCommandName = ...
        ['pscp -i ', sshPuttyPrivateKeyPath, ' '];
end

printGtisVersion = true;
if (~isempty(inparams.printGtisVersion))
    printGtisVersion = inparams.printGtisVersion;
end
if (printGtisVersion)
    fprintf('\n %s version %s\n\n', ...
        GTIS_parameters.name, GTIS_parameters.version);
end


    function [gtisVersion] = parseGtisVersion(gtisMatlabPath)
        gtisVersion = [];
        % Parses the file with the GTIS version
        gtisVersionFilename = 'version.cmake';
        fd = fopen(...
            fullfile(fileparts(gtisMatlabPath), gtisVersionFilename), 'r');
        if (fd ==(-1))
            return;
        end
        str = fscanf(fd, '%s\n', Inf); % No whitespaces
        fclose(fd);
        versionLabels = {...
            'GTIS_VERSION_MAJOR', 'GTIS_VERSION_MINOR', ...
            'GTIS_VERSION_PATCH', 'GTIS_VERSION_MAINT', ...
            'GTIS_VERSION_DESC"'};
        version = cell(size(versionLabels));
        for jj=1:length(versionLabels)
            posIni = strfind(str, versionLabels{jj});
            if (isempty(posIni))
                return;
            end
            posIni = posIni + length(versionLabels{jj});
            relativePosEnd = strfind(str(posIni:end), ')');
            posEnd = posIni + relativePosEnd(1) - 2;
            version{jj} = str(posIni:posEnd);
            str = str(posEnd + 1:end);
        end
        gtisVersion = ...
            strcat([version{1}, '.', version{2}, '.', version{3}, '.', ...
            version{4}], '-', version{5}, ')');
    end
end