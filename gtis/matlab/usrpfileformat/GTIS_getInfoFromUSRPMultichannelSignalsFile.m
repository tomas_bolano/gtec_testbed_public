function [multichannelFileInfo] = ...
    GTIS_getInfoFromUSRPMultichannelSignalsFile(filename)
% 
% GTIS_getInfoFromUSRPMultichannelSignalsFile
% 
% This function returns a structure containing basic info about a
% multichannel file containing samples acquired from a USRP.
% Fields are: 
%   * numberOfChannels: total number of multiplexed channels
%   * numberOfSamplesPerChannel: total number of samples available for each
%     channel. All channels should have the same number of samples.
%   * version: version of the file format.
%

% Internal parameters
complexSampleSizeInBytes = 4; 
complexSampleSizeInInt16 = 2;

multichannelFileInfo = [];

fid = fopen(filename, 'r', 'ieee-le');
if (fid == (-1))
    return;
end

% Reads 2 bytes containing the number of channels 
numChannels = fread(fid, 1, 'uint16=>double');

% Reads 2 bytes containing the maximum number of complex samples per
% each channel in the payload
maxPayloadSizeInComplexSamplesPerChannel = fread(fid, 1, 'uint16=>double');

% Reads 2 bytes containing the size of each sample in bytes
sampleSizeInBytes = fread(fid, 1, 'uint16=>double');

if (sampleSizeInBytes ~= complexSampleSizeInBytes)
    fprintf(' Cannot deal with complex samples different than 2 bytes ');
    fprintf(' for I and 2 bytes for Q (4 bytes in total)\n');
    fclose(fid);
    return;
end

fileProcessed = false;
numComplexSamplesPerChannel = 0;
while (~fileProcessed)
    % Reads 2 bytes of the header containing the size of the payload in
    % complex samples
    [payloadSizeInComplexSamplesPerChannel, count] = ...
        fread(fid, 1, 'uint16=>double');
    if (payloadSizeInComplexSamplesPerChannel > maxPayloadSizeInComplexSamplesPerChannel)
        fclose(fid);
        return;
    end
    if (count == 0)
        % End-of-file reached
        fclose(fid);
        fileProcessed = true;
    else
        numComplexSamplesPerChannel = numComplexSamplesPerChannel + ...
            payloadSizeInComplexSamplesPerChannel;
        numBytesToSkip = payloadSizeInComplexSamplesPerChannel * ...
            complexSampleSizeInBytes;
        status = fseek(fid, numChannels * numBytesToSkip, 'cof');
        if (status == (-1))
            fclose(fid);
            fprintf(' Cannot skip enough data.\n');
            return;
        end
    end
end

if (fileProcessed)
    multichannelFileInfo.version = 1;
    multichannelFileInfo.numChannels = numChannels;
    multichannelFileInfo.numComplexSamplesPerChannel = ...
        numComplexSamplesPerChannel;
    multichannelFileInfo.complexSampleSizeInBytes = ...
        complexSampleSizeInBytes; 
    multichannelFileInfo.complexSampleSizeInInt16 = ...
        complexSampleSizeInInt16;
end