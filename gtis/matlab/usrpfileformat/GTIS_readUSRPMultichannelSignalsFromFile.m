function [ readSignalComplex, success, ...
    totalNumSamplessProcessedPerChannel, ...
    totalNumBytesProcessedPerChannel, ...
    numChannels] = GTIS_readUSRPMultichannelSignalsFromFile(...
    filename, ...
    numComplexSamplesPerChannelToBeRead, ...
    numComplexSamplesOffsetPerChannel)
% 
% GTIS_readUSRPMultichannelSignalsFromFile
% 
% This function allows for efficiently reading a GTIS USRP file containing
% a single or two multiplexed I/Q data streams. Each data stream consists of
% complex samples (real part for the I, imaginary part for the Q),
% each being an int16 (2-byte integer), resulting in 4
% bytes per sample (2 bytes for the I and another 2 for the Q).
%

% Internal parameters
complexSampleSizeInBytes = 4; 
complexSampleSizeInInt16 = 2;

totalNumSamplessProcessedPerChannel = 0;
totalNumBytesProcessedPerChannel = 0;
readSignalComplex = [];
success = 0;

fid = fopen(filename, 'r', 'ieee-le');
if (fid == (-1))
    return;
end

% Reads 2 bytes containing the number of channels 
numChannels = fread(fid, 1, 'uint16=>double');

% Reads 2 bytes containing the maximum number of complex samples per
% each channel in the payload
maxPayloadSizeInComplexSamplesPerChannel = fread(fid, 1, 'uint16=>double');

chunkInUSRPFormat = zeros(numChannels, ...
    maxPayloadSizeInComplexSamplesPerChannel * complexSampleSizeInInt16);

% Reads 2 bytes containing the size of each sample in bytes
sampleSizeInBytes = fread(fid, 1, 'uint16=>double');

if (sampleSizeInBytes ~= complexSampleSizeInBytes)
    fprintf(' Cannot deal with complex samples different than 2 bytes ');
    fprintf(' for I and 2 bytes for Q (4 bytes in total)\n');
    fclose(fid);
    return;
end

if (isinf(numComplexSamplesPerChannelToBeRead))
    readSignalComplex = [];
else
    readSignalComplex = zeros(numChannels, numComplexSamplesPerChannelToBeRead);
end

jj = 1; % index for readSignalComplex
if (numComplexSamplesOffsetPerChannel > 0)
    totalNumSamplesSkippedPerChannel = 0;
    numComplexSamplesPerChannelRemainInChunk = 0;
    while (totalNumSamplesSkippedPerChannel < numComplexSamplesOffsetPerChannel)
        % Reads 2 bytes of the header containing the size of the payload in
        % complex samples
        [payloadSizeInComplexSamplesPerChannel, count] = fread(fid, 1, 'uint16=>double');
        if (payloadSizeInComplexSamplesPerChannel > maxPayloadSizeInComplexSamplesPerChannel)
            success = 0;
            fclose(fid);
            return;
        end
        if (count == 0)
            % End-of-file reached
            fclose(fid);
            success = 1;
            return;
        end
      
        if ((totalNumSamplesSkippedPerChannel + payloadSizeInComplexSamplesPerChannel) ...
                > numComplexSamplesOffsetPerChannel)
            % This is the last chunk to be skipped but not the whole chunk 
            % has to be skipped, part of it is needed, therefore the whole
            % chunk must be read and stored for later use
            numComplexSamplesPerChannelToSkipFromChunk = numComplexSamplesOffsetPerChannel ...
                - totalNumSamplesSkippedPerChannel;
            numComplexSamplesPerChannelRemainInChunk = ...
                payloadSizeInComplexSamplesPerChannel - ...
                numComplexSamplesPerChannelToSkipFromChunk;
            numInt16SamplesToBeRead = ...
                payloadSizeInComplexSamplesPerChannel * complexSampleSizeInInt16;
            for ii=1:numChannels,
                [chunkInUSRPFormat(ii, 1:numInt16SamplesToBeRead), count] = fread(...
                    fid, numInt16SamplesToBeRead, 'int16=>double');
                if (count ~= numInt16SamplesToBeRead)
                    success = 0;
                    fclose(fid);
                    fprintf(' Not enough data while reading the last chunk to be skipped.\n');
                    return;
                end
            end
        else
            % If the whole chunk is not needed, then it can be skeeped by
            % seeking the file pointer adequately.
            numComplexSamplesPerChannelToSkipFromChunk = ...
                payloadSizeInComplexSamplesPerChannel;
            numBytesToSkip = numChannels ...
                * numComplexSamplesPerChannelToSkipFromChunk ...
                * complexSampleSizeInBytes;
                status = fseek(fid, numBytesToSkip, 'cof');
            if (status == (-1))
                fclose(fid);
                success = 0;
                fprintf(' Cannot skip enough data.\n');
                return;
            end
        end
        
        totalNumSamplesSkippedPerChannel = ...
            totalNumSamplesSkippedPerChannel + numComplexSamplesPerChannelToSkipFromChunk;
    end    
    % If samples have been previously skipped and a chunk has been
    % partially skipped, then the whole chunk has been actually read (a
    % chunk cannot be partially skipped because it is multiplexed by the
    % channel) and now only the remaining part has to be stored in the
    % output signal vector.
    % In case the whole chunk has been skipped, then the remaining of the
    % file can be processed as if no skipping would have been taken.
    
    if (numComplexSamplesPerChannelRemainInChunk > 0)        
        
        if (numComplexSamplesPerChannelRemainInChunk > numComplexSamplesPerChannelToBeRead)
            % The whole remaining of the partially skipped chunk is not needed
            numComplexSamplesPerChannelToStoreFromChunk = numComplexSamplesPerChannelToBeRead;
        else
            numComplexSamplesPerChannelToStoreFromChunk = numComplexSamplesPerChannelRemainInChunk;
        end
        chunkIndexI = (numComplexSamplesPerChannelToSkipFromChunk ...
            * complexSampleSizeInInt16 + 1):2:...
            ((numComplexSamplesPerChannelToSkipFromChunk + ...
            numComplexSamplesPerChannelToStoreFromChunk) * complexSampleSizeInInt16);
        chunkIndexQ = (numComplexSamplesPerChannelToSkipFromChunk ...
            * complexSampleSizeInInt16 + 2):2:...
            ((numComplexSamplesPerChannelToSkipFromChunk + ...
            numComplexSamplesPerChannelToStoreFromChunk) * complexSampleSizeInInt16);
        readSignalComplex(:, jj:jj + numComplexSamplesPerChannelToStoreFromChunk - 1) = ...
            chunkInUSRPFormat(:, chunkIndexI) + ...
            1j .* chunkInUSRPFormat(:, chunkIndexQ);

        jj = jj + numComplexSamplesPerChannelToStoreFromChunk;

        totalNumSamplessProcessedPerChannel = ...
            totalNumSamplessProcessedPerChannel ...
            + numComplexSamplesPerChannelToStoreFromChunk;
        totalNumBytesProcessedPerChannel = ...
            totalNumBytesProcessedPerChannel ...
            + sampleSizeInBytes * numComplexSamplesPerChannelToStoreFromChunk;
    end
end

success = 1;
while (totalNumSamplessProcessedPerChannel < numComplexSamplesPerChannelToBeRead)
    % Reads 2 bytes of the header containing the size of the payload in
    % complex samples
    [payloadSizeInComplexSamplesPerChannel, count] = fread(fid, 1, 'uint16=>double');
    if (payloadSizeInComplexSamplesPerChannel > maxPayloadSizeInComplexSamplesPerChannel)
        success = 0;
        break;
    end
    if (count == 0)
        % End-of-file reached
        break;
    end
    
    % TBD: This step should be carried out after the calculation below and
    % the number of int16 samples to be read should be adjusted depending
    % on numSamplesFromChunk to read just the samples needed, and not more.
    numInt16SamplesToBeRead = ...
        payloadSizeInComplexSamplesPerChannel * complexSampleSizeInInt16;
    for ii=1:numChannels,
        [chunkInUSRPFormat(ii, 1:numInt16SamplesToBeRead), count] = fread(...
            fid, numInt16SamplesToBeRead, 'int16=>double');
        if (count ~= numInt16SamplesToBeRead)
            success = 0;
            break;
        end
    end
    
    if ((totalNumSamplessProcessedPerChannel + payloadSizeInComplexSamplesPerChannel) ...
            > numComplexSamplesPerChannelToBeRead)
        % This is the last chunk to be read but not the whole chunk is
        % needed, the rest is discarded.
        numComplexSamplesPerChannelToStoreFromChunk = numComplexSamplesPerChannelToBeRead ...
            - totalNumSamplessProcessedPerChannel;
    else
        numComplexSamplesPerChannelToStoreFromChunk = payloadSizeInComplexSamplesPerChannel;
    end
    readSignalComplex(:, jj:jj + numComplexSamplesPerChannelToStoreFromChunk - 1) = ...
        chunkInUSRPFormat(:, ...
        1:2:numComplexSamplesPerChannelToStoreFromChunk*complexSampleSizeInInt16) + ...
        1j .* chunkInUSRPFormat(:, ...
        2:2:numComplexSamplesPerChannelToStoreFromChunk*complexSampleSizeInInt16);
    
    jj = jj + numComplexSamplesPerChannelToStoreFromChunk;
    
    totalNumSamplessProcessedPerChannel = ...
        totalNumSamplessProcessedPerChannel ...
        + numComplexSamplesPerChannelToStoreFromChunk;
    totalNumBytesProcessedPerChannel = ...
        totalNumBytesProcessedPerChannel ...
        + sampleSizeInBytes * numComplexSamplesPerChannelToStoreFromChunk;
end

fclose(fid);
