%
% success = GTIS_writeUSRPSignalsToFile(usrpSignal, filename, overwriteExistingFile)
%
% Writes a (single-stream, or single-channel) signal in the USRP format 
% (interleaved int16 samples) in a file.
% 
% Note that at the transmit side, GTIS does not use multiple-stream (or
% multiple-channel) signal files since there is not as much pressure 
% Input parameters:
%   usrpSignal: the signal to be written.
%   filename: name of the file to be written.
%   overwriteExistingFile: if true, the file is always written.
%
function success = GTIS_writeUSRPSignalsToFile(usrpSignal, filename, overwriteExistingFile)

% Internal parameters
complexSampleSizeInInt16 = 2;
%

if (~overwriteExistingFile)
    fid = fopen(filename, 'r');
    if (fid ~= (-1))
        fclose(fid);
        success = 0;
        return;
    end
end

fid = fopen(filename,'w','ieee-le');
if (fid ~= (-1))
    count = fwrite(fid, usrpSignal, 'int16');
    fclose(fid);
    success = count / complexSampleSizeInInt16;
else
    success = 0;
end