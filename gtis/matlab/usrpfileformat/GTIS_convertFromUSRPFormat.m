%
% function [complexSignal] = GTIS_convertFromUSRPFormat(usrpSignalInt16)
%
% Converts a signal in the USRP format (interleaved pairs of int16 values)
% to a usual complex vector in Matlab.
% 

function complexSignal = GTIS_convertFromUSRPFormat(usrpSignalInt16)

% Internal parameters
% nBitsOfADCResolution = 16; % Fixed parameter defined in the UHD
% % maxAmplitudeValue = 2^(nBitsOfADCResolution - 1) - 1;
%
% % idata = double(usrpSignalInt16(1:2:end));
% % idata = idata - mean(idata);
% % qdata = double(usrpSignalInt16(2:2:end));
% % qdata = qdata - mean(qdata);
% % complexSignal = idata ./ maxAmplitudeValue ...
% %     + 1j .* qdata ./ maxAmplitudeValue;

complexSignal = double(usrpSignalInt16(1:2:end)) ...
    + 1j .* double(usrpSignalInt16(2:2:end));