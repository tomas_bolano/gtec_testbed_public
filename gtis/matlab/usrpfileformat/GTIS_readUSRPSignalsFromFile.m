%
% function [readSignalInt16, success] = GTIS_readUSRPSignalsFromFile(...
%    filename, complexSamplesToBeRead, complexSamplesOffsetRead)
%
% Reads a signal stored in a file in the USRP format (interleaved int16
% samples) and returns the values in int16 format to be further processed.
% Notice that this function is only valid for single-channel
% (single-stream) files. For two-stream (two-channel) files use 
% GTIS_readUSRPMultichannelSignalsFromFile()
% 
% Input parameters:
%   filename: name of the file to be read.
%   complexSamplesToBeRead: number of int16 pairs to be read or Inf for
%   reading the whole file.
%   complexSamplesOffsetRead: number of int16 pairs (complex samples) to be
%   discarded from the beginning of the file.
%
% Returns:
%   readSignalInt16: the signal or [].
%   success == 0 => cannot read file, == -1 => cannot read requested
%   success >= 1 => successful operation with the number of complex samples
%   read.

function [readSignalInt16, success] = GTIS_readUSRPSignalsFromFile(...
    filename, complexSamplesToBeRead, complexSamplesOffsetRead)

% Internal parameters
complexSampleSizeInBytes = 4;
complexSampleSizeInInt16 = 2;
%

fid = fopen(filename, 'r', 'ieee-le');
if (fid == (-1))
    readSignalInt16 = [];
    success = 0;
    return;
end

if (complexSamplesOffsetRead > 0)
    status = fseek(fid, complexSamplesOffsetRead * complexSampleSizeInBytes, 'bof');
    if (status == (-1))
        fclose(fid);
        readSignalInt16 = [];
        success = 0;
        return;
    end
end

readSignalInt16 = fread(fid, [1, complexSamplesToBeRead * complexSampleSizeInInt16], 'int16');
fclose(fid);
numComplexSamplesRead = length(readSignalInt16) / complexSampleSizeInInt16;
if (isinf(complexSamplesToBeRead) || (numComplexSamplesRead == complexSamplesToBeRead))
    success = numComplexSamplesRead;
else
    success = -1;
end