%
% [convertedSignal, normalizedClippedSignal, idataInt16, qdataInt16] = 
%    GTIS_convertToUSRPFormat(...
%       complexMatlabSignal, clippingFactor, doNormalizeSignal)
%
% TODO: Document the MIMO version
%
% Converts a vector of complex samples in Matlab to a vector of pairs of
% interleaved int16 values, which is the format defined in the UHD driver
% for the USRPs. Additional parameters ca be use for tuning the process:
%     
%   clippingFactor: maximum absolute value for the real and imaginary
%   sample values. Those values exceeding the clippingFactor are set equal
%   to such a clippingFactor. A value <= 0 is specified for avoiding
%   clipping.
%   doNormalizeSignal: true if the signal is to be converted to values
%   in the [-1, 1] interval for the real and imaginary parts before being
%   converted to the USRP format.
%
% Return values:
%   convertedSignal: the complex samples as pairs of int16 values.
%   normalizedClippedSignal: the signal normalized and clipped just in
%       Matlab format.
%

function [convertedSignal, normalizedClippedSignal, idataInt16, qdataInt16] = ...
    GTIS_convertToUSRPFormat(...
    complexMatlabSignal, clippingFactor, doNormalizeSignal)

% Internal parameters
nBitsOfDACResolution = 16; % Fixed parameter defined in the UHD
maxAmplitudeValue = 2^(nBitsOfDACResolution - 1) - 1;
%

idata = real(complexMatlabSignal);
qdata = imag(complexMatlabSignal);

if (clippingFactor > 0)
    for ii = 1:size(complexMatlabSignal, 1),
        idata(ii, idata(ii, :) > clippingFactor) = clippingFactor;
        idata(ii, idata(ii, :) < -clippingFactor) = -clippingFactor;
        qdata(ii, qdata(ii, :) > clippingFactor) = clippingFactor;
        qdata(ii, qdata(ii, :) < -clippingFactor) = -clippingFactor;
    end
end

if (doNormalizeSignal)
    normalizationValue = max(max(abs(idata(:))),max(abs(qdata(:))));
    idata(:) = idata(:) ./ normalizationValue;
    qdata(:) = qdata(:) ./ normalizationValue;
end

normalizedClippedSignal = idata + 1j .* qdata;

idata(:) = round(idata(:) * maxAmplitudeValue);
qdata(:) = round(qdata(:) * maxAmplitudeValue);

idataInt16 = int16(idata);
qdataInt16 = int16(qdata);

convertedSignal = int16(zeros(size(complexMatlabSignal, 1), 2*length(idataInt16)));
for ii = 1:size(complexMatlabSignal, 1),
    convertedSignal(ii, 1:2:end) = idataInt16(ii, :);
    convertedSignal(ii, 2:2:end) = qdataInt16(ii, :);
end


