% Calculate and plot doppler spectrum using the correlation of the channel
close all;

%% configuration

ndata = 600;  % number of used subcarriers
nsubc = 1024; % number of total subcarriers
nsymb = 6*140; % number of time symbols per iter
cplen = 72; % CP length
M = 4; % QAM constellation order

fc = 2.6e9; % carrier frequency
ts = 1/15.36e6; % sampling time
v = 200; % speed in km/h

fd = (v/3.6)*fc/3e8; % maximum doppler frequency
ch = stdchan(ts, fd, '3gppRAx');
%ch = stdchan(ts, fd, '3gppTUx');
%ch = rayleighchan(ts, fd, 0, 0);
ch.ResetBeforeFiltering = 0;

niter = 20;

%% code
assert(mod(ndata,2) == 0, 'ndata must be even');
assert(mod(log2(nsubc),1) == 0, 'nsubc must be a power of 2');

% subcarrier mask
subcmask = logical([0 ones(1,ndata/2) zeros(1,nsubc-ndata-1) ones(1,ndata/2)]);

% subcarrier mask including the DC subcarrier
subcmask0 = subcmask;
subcmask0(1) = true;

corrmaxtaps = 70; % number of taps to correlate
corrmat = zeros(nsymb*2-1, corrmaxtaps, niter);

for kk = 1:niter
    % transmit data
    data = qammod(randi([0 M-1], ndata, nsymb), M);

    % transmit signal
    tx_f = zeros(nsubc,nsymb);
    tx_f(subcmask,:) = data;
    tx_t1 = 1/sqrt(1024)*ifft(tx_f);
    tx_t = [tx_t1(end-cplen+1:end,:); tx_t1]; % add cp
    tx_t = tx_t(:);

    % apply channel to obtain received signal
    rx_t = filter(ch, tx_t);

    % obtain received symbols in frequency domain
    %rx_t = rx_t.*exp(1j*2*pi*120*(0:numel(rx_t)-1)*ts).';
    rx_t = reshape(rx_t, nsubc+cplen, nsymb);
    rx_t = rx_t(cplen+1:end,:); % remove cp
    rx_f = sqrt(1024)*fft(rx_t);

    % channel estimation
    H = rx_f./tx_f;
    H(~subcmask,:) = 0; % assign 0 to non-used subcarriers
    %H(1,:) = (H(2,:)+H(end,:))/2;
    %H = H(subcmask0,:);
    
    cir = ifft(H);

    for jj = 1:corrmaxtaps
        corrmat(:,jj,kk) = xcorr(cir(jj,:));
    end
    
    kk
end

%% calculate doppler spectral power (dsp)
%L = 1024*2;
dsp = zeros(nsymb*2-1, niter);
%dsp = zeros(L, niter);
for kk = 1:niter
    for jj = 1:corrmaxtaps
        dsp(:,kk) = dsp(:,kk) + fftshift(fft(corrmat(:,jj,kk)));
    end
end

%% calculate frequency x axis values
fmax = 1/(2*ts*(1024+72));
f = linspace(-fmax, fmax, size(dsp,1));

%% Plot estimated spectrum for 1st iteration
figure();
plot(f,(abs(dsp(:,1))), '.-');
ax = axis();
grid on;
hold on;
plot([fd fd], [ax(3) ax(4)], '--', 'color', [1 1 1]*0.5);
plot(-[fd fd], [ax(3) ax(4)], '--', 'color', [1 1 1]*0.5);
legend('doppler spectrum', 'max/min doppler');
title('estimated doppler for 1st iteration');


%% Plot estimated spectrum image
if niter > 1
    figure();
    surf(1:niter, f, 10*log10(abs(dsp)));
    hold on;
    grid on;
    title('estimated doppler');
    %shading flat;
    shading interp;
    ylim([-fd fd]*3);
    zlim([-10, 50]);
    view([0 90]);
end


%% Plot mean estimated spectrum
figure();
meandsp = abs(mean(dsp,2));
plot(f, meandsp/max(meandsp), '.-');
ax = axis();
grid on;
hold on;
plot([fd fd], [ax(3) ax(4)], '--', 'color', [1 1 1]*0.5);
plot(-[fd fd], [ax(3) ax(4)], '--', 'color', [1 1 1]*0.5);
fjakes = linspace(-fd, fd, 100);
sjakes = 1./(pi*fd)*1./sqrt(1-(fjakes./fd).^2);
plot(fjakes, (sjakes/sjakes(2))+0.2);
legend('doppler spectrum', 'max/min doppler');
title('mean estimated doppler');


