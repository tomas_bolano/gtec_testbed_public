function [e, s] = sageofdm1rx(varargin)
%SAGEOFDM1RX SAGE algorithm for OFDM with 1 receiver antenna
%
% See reference "Channel Parameter Estimation in Mobile Radio
% Environments Using the SAGE Algorithm" for more details
%
% Input parameters are specified as a list of name, value pairs.
% The available input properties are the following:
%   'numwavesest'
%       Number of waves to estimate.
%       Default: 150.
%   'maxiter'
%       Maximum number of iterations.
%       Default: 10.
%   'maxtau'
%       Maximum delay (tau) to estimate, specified as an integer number
%       of samples.
%       Default: 70
%   'mintau'
%       Minimum delay (tau) to estimate, specified as an integer number of
%       samples.
%       Default: -10
%   'maxv'
%       Maximum doppler freq (v) to estimate.
%   'minv'
%       Minimum doppler freq (v) to estimate.
%   'txsamp'
%       Matrix MxN of transmitted samples of the OFDM symbols (without cp),
%       where M is the number of samples per symbol (number of subcarriers)
%       and N is the number of symbols.
%   'rxsamp'
%       Matrix MxN of received samples of the OFDM symbols (without cp),
%       where M is the number of samples per symbol (number of subcarriers)
%       and N is the number of symbols. Note that the size of rxsamp must
%       be the same as txsamp.
%   'tsymb'
%       Vector 1xN of time instants of the transmitted OFDM symbols
%       specified in txsamp. The element n of tsymb must be the time
%       instant (in seconds) when the OFDM symbol n was transmitted.
%   'ts'
%       sampling time.
%   'tau_discrete'
%       If true returns the delays (taus) as discrete integers, else it
%       returns delays with fractionary part. Default: 0.
%   'toltau'
%       Tolerance of tau estimated parameters. The algorithm will not stop
%       until the tolerance is reached or the maximum number of iterations
%       is reached. Default: 0.1;
%   'toltau_type'
%       Method used to calculate tolerance. Available values are:
%       - 'Absolute': the tolerance will be calculated as 
%         mean(abs(tau_new - tau_old)).
%       - 'Relative': the tolerance will be calculated as 
%         mean(abs(tau_new - tau_old)./abs(tau_new))).
%       Default: 'Relative'.
%   'tolv'
%       Tolerance of v (doppler frequency) estimated parameters. The
%       algorithm will not stop until the tolerance is reached or the
%       maximum number of iterations is reached. Default: 0.1;
%   'tolv_type'
%       Method used to calculate tolerance. Available values are:
%       - 'Absolute': the tolerance will be calculated as 
%         mean(abs(v_new - v_old)).
%       - 'Relative': the tolerance will be calculated as 
%         mean(abs(v_new - v_old)./abs(v_new))).
%       Default: 'Relative'.
%   'tola'
%       Tolerance of a (complex amplitude) estimated parameters.The
%       algorithm will not stop until the tolerance is reached or the
%       maximum number of iterations is reached. Default: 0.1;
%   'tola_type'
%       Method used to calculate tolerance. Available values are:
%       - 'Absolute': the tolerance will be calculated as 
%         mean(abs(a_new - a_old)).
%       - 'Relative': the tolerance will be calculated as 
%         mean(abs(a_new - a_old)./abs(a_new))).
%       Default: 'Relative'.
%   'display'
%       Detail level of iterative display. Possible values:
%       - 'final' only print final report
%       - 'iter'  print information for each iteration
%       - 'off'   disable printing information (default)


%% Parameter initializations ----------------------------------------------

% Create cell matrix of input name parameters
% Format of this cell matrix is (per columns):
% parameter name, default value, required (i.e. not optional), updated
parameters = ...
{'numwavesest',  150,   0,  0
 'maxiter',     10,     0,  0
 'maxtau',      70,     0,  0
 'mintau',     -10,     0,  0
 'maxv',        [],     1,  0
 'minv',        [],     1,  0
 'txsamp',      [],     1,  0
 'rxsamp',      [],     1,  0
 'tsymb',       [],     1,  0
 'ts',          [],     1,  0
 'tau_discrete', 0,     0,  0
 'toltau',      0.1,    0,  0
 'toltau_type', 'Relative', 0, 0
 'tolv',        0.1,      0,  0
 'tolv_type',   'Relative', 0, 0
 'tola',        0.1,   0,  0
 'tola_type',   'Relative', 0, 0
 'display',     'off',  0,  0};

% read parameters
if mod(length(varargin),2) ~= 0
    error('missing parameter value');
end

for ii = 1:2:length(varargin)
    str_ind = find(strcmpi(parameters(:,1), varargin{ii}), 1);
    
    if isempty(str_ind)
        error('invalid parameter %s', varargin{ii});
    end
    
    parameters{str_ind, 4} = 1;
    parameters{str_ind, 2} = varargin{ii+1};
end

% check that all required parameters are specified
a = cell2mat(parameters(:,[3 4]));
assert(all(all(~a(:,1) | a(:,2))),...
       'some required parameters are missing');

% add parameters to p struct
p = struct();
for ii = 1:size(parameters,1)
    p.(parameters{ii,1}) = parameters{ii,2};
end

% display options
disp_final = 0;
disp_iter = 1;
disp_off = 2;

switch p.display
    case 'final', disp_val = disp_final;
    case 'iter',  disp_val = disp_iter;
    case 'off',   disp_val = disp_off;
    otherwise
        error('invalid display value');
end


%% SAGE variables initialization ------------------------------------------

% check tau and v min and max values
assert(p.mintau < p.maxtau, 'mintau must be less than maxtau');
assert(p.minv < p.maxv, 'minv must be less than maxv');

% maximum freq. resolution
sage_maxfreqres = min(1./(2*diff(p.tsymb)));

% FIXME
assert(sage_maxfreqres > max(abs(p.maxv), abs(p.minv)),...
       'not enough freq resolution, max resolution is %f', sage_maxfreqres);

% check samples variables size
assert(all(size(p.txsamp) == size(p.rxsamp)),...
       'txsamp and rxsamp must have the same size');

assert(isvector(p.tsymb) && size(p.txsamp,2) == length(p.tsymb),...
       'tsymb must be a vector with the same number of elements as columns of txsamp');
   
% power of tx signal
tx_pw = var(p.txsamp(:));
   
% create matrix of time instants for each sample (t)
sage_t = repmat(p.ts*(0:size(p.txsamp,1)-1).', 1, size(p.txsamp,2));
sage_t = bsxfun(@plus, sage_t, p.tsymb);
   
% waves estimates (s)
sage_s = zeros(size(p.txsamp,1), size(p.txsamp,2), p.numwavesest);

% waves parameteres (phi)
% struct array with the following fields (initialized to 0):
% t: tau; v: nu; a: alpha
sage_phi = repmat(struct('t', 0, 'v', 0, 'a', 0), p.numwavesest, 1);
sage_phi_old = sage_phi;

% To perform the the maximization calculations we first define some
% test points, the maximization function will be executed on these test
% points and then on the maximum the function fminbnd will be used.
% >>> test points for tau
tau_test = p.mintau:p.maxtau;

% >>> test points for v
% in this case we precalculate the complex exponential values needed
% and some other variables

% frequency of the oscillations in the doppler maximization function
% not sure why but it seems to work
freq_oscillation = 1/(diff(p.tsymb([1 end])));

v_num_test_points = ceil(2*(p.maxv-p.minv)/freq_oscillation);
v_test = linspace(p.minv, p.maxv, v_num_test_points);
if numel(v_test) > 1
    v_test_diff = v_test(2) - v_test(1);
else
    v_test_diff = (p.maxv-p.minv)/2;
end
v_test_expmat = zeros(size(sage_t,1), size(sage_t,2), v_num_test_points);
for kk = 1:length(v_test)
    v_test_expmat(:,:,kk) = exp(-1j*2*pi*v_test(kk)*sage_t);
end

% toleranche checking functions
switch lower(p.toltau_type)
    case 'absolute', toltau_func = @(p1,p2) mean(abs([p1.t] - [p2.t]));
    case 'relative', toltau_func = @(p1,p2) mean(abs([p1.t] - [p2.t])./abs([p1.t]));
    otherwise, error('invalid toltau_type value');
end

switch lower(p.tolv_type)
    case 'absolute', tolv_func = @(p1,p2) mean(abs([p1.v] - [p2.v]));
    case 'relative', tolv_func = @(p1,p2) mean(abs([p1.v] - [p2.v])./abs([p1.v]));
    otherwise, error('invalid toltau_type value');
end

switch lower(p.tola_type)
    case 'absolute', tola_func = @(p1,p2) mean(abs([p1.a] - [p2.a]));
    case 'relative', tola_func = @(p1,p2) mean(abs([p1.a] - [p2.a])./abs([p1.a]));
    otherwise, error('invalid toltau_type value');
end


%% SAGE main loop ---------------------------------------------------------

gt = tic; % global timer
ii = 0;

while true
    ii = ii + 1;
    for jj = 1:p.numwavesest
        it = tic; % iteration step timer
        
        % Expectation step ------------------------------------------------
        % obtain estimate of the wave jj (x_jj)
        %x_jj = sage_s(:,:,jj) + p.rxsamp - sum(sage_s, 3);
        if jj == 1
            x_jj = sage_s(:,:,jj) + p.rxsamp - sum(sage_s, 3);
        else
            x_jj = sage_s(:,:,jj) + x_jj - sage_s(:,:,jj-1);
        end
        
        % Maximization step -----------------------------------------------
        % max tau
        xmat = x_jj.*exp(-1j*2*pi*(sage_phi(jj).v)*sage_t);
        tauf_test = @(t) -abs(sum(sum(conj(circshift(p.txsamp,t)).*xmat)));
        tauf = @(t) -abs(sum(sum(conj(fshift(p.txsamp,t)).*xmat)));
        
        [~, mini] = min(arrayfun(tauf_test, tau_test));
        if p.tau_discrete
            tau = tau_test(mini);
        else
            tau = fminbnd(tauf, max(tau_test(mini)-0.5, p.mintau), ...
                                min(tau_test(mini)+0.5, p.maxtau));
        end

        % max v
        sage_tx_tau = fshift(p.txsamp, tau);
        xmat = conj(sage_tx_tau).*x_jj;
        vf_test = @(ii) -abs(sum(sum(xmat.*v_test_expmat(:,:,ii))));
        vf = @(vv) -abs(sum(sum(xmat.*exp(-1j*2*pi*vv*sage_t))));
        
        [~, mini] = min(arrayfun(vf_test, 1:v_num_test_points));
        vv = fminbnd(vf, max(v_test(mini)-v_test_diff, p.minv),...
                         min(v_test(mini)+v_test_diff, p.maxv));

        % complex amplitude
        a = 1/(numel(sage_tx_tau)*tx_pw)*sum(...
            sum(xmat.*exp(-1j*2*pi*vv*sage_t)));
        
        % Iteration ending steps ------------------------------------------
        
        % save estimated parameters
        sage_phi(jj).t = tau;
        sage_phi(jj).v = vv;
        sage_phi(jj).a = a;

        % calculate wave estimate for this iteration
        sage_s(:,:,jj) = a*sage_tx_tau.*exp(1j*2*pi*vv*sage_t);
        
        time = toc(it);
        if disp_val == disp_iter
            fprintf('SAGE OFDM - Iteration %d [%3d] (%5.3f s)\n', ii, jj, time);
        end
    end
    
    % check stop criteria
    if toltau_func(sage_phi, sage_phi_old) < p.toltau && ...
       tola_func(sage_phi, sage_phi_old) < p.tola && ...
       tolv_func(sage_phi, sage_phi_old) < p.tolv
        if disp_val ~= disp_off
            fprintf('Algorithm finished - minimum tolerance reached\n');
        end
        break;
    elseif ii == p.maxiter
        if disp_val ~= disp_off
            fprintf('Algorithm finished - maximum number of iterations reached\n');
        end
        break;
    end
    
    % save this iteration parameters for the next one
    sage_phi_old = sage_phi;
end

time = toc(gt);
if disp_val ~= disp_off
    fprintf('Elapsed time is %f seconds.\n', time);
end

e = sage_phi;
s = sage_s;

end

