function [x, ap, phi] = genchan(N,m,fd,d)
%GENCHAN Generate rayleigh channel path realization with mean variance 1
%
% Inputs:
% N - number of sums of sinusoids to use
% m - number of samples to generate
% fd - max doppler frequency
% d - doppler type: 'jakes' (default) or 'flat'
%
% Outputs:
% x - vector of samples of channel realization
% ap - vector of the N doppler factors used for each exponential
% phi - vector of the N phases used for each exponential

if nargin < 4
    d = 'jakes';
end
 
switch d
    case 'jakes'
        ap = cos(rand(1,N)*2*pi);
    case 'flat'
        ap = rand(1,N)*2-1;
    otherwise
        error('invalid doppler type');
end
    
phi = rand(1,N)*2*pi; % random phase

if N == 1
    x = randn()*exp(1j*(2*pi*fd*(0:m-1)*ap + phi));
elseif N <= 100
    auxvec = repmat(2*pi*fd*(0:m-1),N,1);
    auxvec = bsxfun(@times, auxvec, ap.');
    auxvec = bsxfun(@plus, auxvec, phi.');
    %C = randn(1,N);
    %auxvec = bsxfun(@plus, auxvec, C.');
    x = sqrt(1/N)*sum(exp(1j*auxvec));
else
    % slower but less memory consuming
    x = zeros(1,m);
    for ii = 1:N
        x = x + exp(1j*(2*pi*fd*(0:m-1)*ap(ii) + phi(ii)));
    end
    x = sqrt(1/N)*x;
end

end
