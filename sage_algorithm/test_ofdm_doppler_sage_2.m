close all;
clear;
rng(4);

%% OFDM configuration

ndata = 600;  % number of used subcarriers
nsubc = 1024; % number of total subcarriers
nsymb = 140; % number of time symbols
cplen = 72; % CP length
M = 4; % QAM constellation order

fc = 2.6e9; % carrier frequency
ts = 1/15.36e6; % sampling time
v = 100; % speed (in km/h)

snrdb = 80; % SNR

% maximum doppler frequency
fd = (v/3.6)*fc/3e8; 

% matlab channel configuration
%h = stdchan(ts, fd, '3gppRAx');
%h = stdchan(ts, fd, '3gppTUx');
h = stdchan(ts, fd, 'itur3GVAx');


%% code -------------------------------------------------------------------
assert(mod(ndata,2) == 0, 'ndata must be even');
assert(mod(log2(nsubc),1) == 0, 'nsubc must be a power of 2');

% subcarrier mask
subcmask = logical([0 ones(1,ndata/2) zeros(1,nsubc-ndata-1) ones(1,ndata/2)]);

% transmit data
data = qammod(randi([0 M-1], ndata, nsymb), M);

% transmit signal
tx_f = zeros(nsubc,nsymb);
tx_f(subcmask,:) = data;
tx_t1 = sqrt(1024)*ifft(tx_f);
tx_t = [tx_t1(end-cplen+1:end,:); tx_t1]; % add cp
tx_t = tx_t(:);

% apply channel
rx_t = filter(h, tx_t);

% add noise to the signal
rx_t_eg = var(rx_t(:));
noise_eg = rx_t_eg./(10^(snrdb/10))*nsubc/nnz(subcmask);
noise_t = sqrt(noise_eg/2)*(randn(size(rx_t)) +1j*randn(size(rx_t))); 
rx_t_n = rx_t + noise_t;

% obtain received symbols in frequency domain
%rx_t = rx_t.*exp(1j*2*pi*420*(0:numel(rx_t)-1)*ts).';
rx_t_n = reshape(rx_t_n, nsubc+cplen, nsymb);
rx_t_n = rx_t_n(cplen+1:end,:); % remove cp
rx_f = 1/sqrt(1024)*fft(rx_t_n);

% channel estimation
H = rx_f./tx_f;
H(~subcmask,:) = 0; % assign 0 to non-used subcarriers

% calculate channel impulse response
cir = ifft(H);
  

%% Plot OFDM estimated channel
fmaxsig = (ndata/2)/(ts*nsubc);
fsig = linspace(-fmaxsig, fmaxsig, ndata+1);
tsig = ts*(nsubc+cplen)*(0:nsymb-1);

H_used = H([1:ndata/2+1 end-ndata/2+1:end],:);
H_used(1,:) = 0.5*(H_used(2,:) + H_used(end,:));
H_used = fftshift(H_used,1);

figure();
surf(tsig*1e3, fsig*1e-6, abs(H_used));
shading flat;
xlabel('time [ms]');
ylabel('frequency [MHz]');
title('OFDM estimated channel');
view([0 90]);
c = caxis();

%% SAGE algorithm ---------------------------------------------------------

K = 10; % number of symbols to pick
symb_ind = floor(linspace(1, nsymb, K));


L = 100; % number of waves to estimate
I = 10; % maximum number of iterations

% stop criteria - parameters tolerances
toltau = 0.05;
toltau_type = 'Relative';
tolv = 0.05;
tolv_type = 'Relative';
tola = 0.05;
tola_type = 'Relative';

% maximum and minimum time delays
sage_max_t = 60; % maximum delay
sage_min_t = -10; % minimum delay

% maximum and minimum doppler shifts
sage_max_v = fd*1.05; % maximum doppler shift
sage_min_v = -fd*1.05; % minimum doppler shift

% transmitted symbols in frequency
sage_tx_f = tx_f;

% transmitted signal (u, without cp)
sage_u = tx_t1(:,symb_ind);
sage_u_var = var(sage_u(:));

% matrix of time instants of each sample
sage_u_t = reshape(0:nsymb*(nsubc+cplen)-1, [], nsymb);
sage_u_t = sage_u_t(cplen+1:end, symb_ind)*ts;

% received signal (y, without cp)
sage_y = rx_t_n(:,symb_ind);

% waves estimates (s)
sage_s = zeros(size(sage_u,1), size(sage_u,2), L);

% call SAGE algorithm
[e, ~] = ...
sageofdm1rx('numwavesest', L, 'maxiter', I,...
            'maxtau', sage_max_t, 'mintau', sage_min_t,...
            'maxv', sage_max_v, 'minv', sage_min_v,...
            'txsamp', sage_u, 'rxsamp', sage_y,...
            'tsymb', sage_u_t(1,:), 'ts', ts,...
            'tau_discrete', 1,...
            'toltau', toltau, 'toltau_type', toltau_type,...
            'tolv', tolv, 'tolv_type', tolv_type,...
            'tola', tola, 'tola_type', tola_type,...
            'display', 'iter');


%% Plot SAGE estimated channel
fmaxsig = (ndata/2)/(ts*nsubc);
f1 = linspace(-fmaxsig, fmaxsig, ndata+1);
%f1 = [f1(1:300) f1(302:end)];
%t1 = linspace(0, 0.01, 140);
t1 = ts*(nsubc+cplen)*(0:nsymb-1);
                
tic;
a_vec = [e.a];
v_vec = [e.v];
tau_vec = [e.t]*ts;

t_mat = repmat(t1, numel(f1), 1, numel(e));
f_mat = repmat(f1.', 1, numel(t1), numel(e));
v_mat = repmat(reshape(v_vec,1,1,[]), numel(f1), numel(t1), 1);
tau_mat = repmat(reshape(tau_vec,1,1,[]), numel(f1), numel(t1), 1);
a_mat = repmat(reshape(a_vec,1,1,[]), numel(f1), numel(t1), 1);

H_sage = sum(a_mat.*exp(1j*2*pi*v_mat.*(t_mat)).*...
             exp(-1j*2*pi*f_mat.*tau_mat), 3);
toc;

figure();
surf(t1*1e3, f1*1e-6, abs(H_sage));
shading flat;
xlabel('time [ms]');
ylabel('frequency [MHz]');
title('SAGE estimated channel');
view([0 90]);
caxis(c);


%% Plot channels differences
figure();
surf(t1*1e3, f1*1e-6, real(H_sage - H_used));
shading flat;
xlabel('time [ms]');
ylabel('frequency [MHz]');
title('channel diff SAGE - OFDM');
view([0 90]);