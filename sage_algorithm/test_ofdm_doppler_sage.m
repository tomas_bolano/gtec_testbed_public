close all;
clear;
rng(5);

%% OFDM configuration

ndata = 600;  % number of used subcarriers
nsubc = 1024; % number of total subcarriers
nsymb = 140; % number of time symbols
cplen = 72; % CP length
M = 4; % QAM constellation order

fc = 2.6e9; % carrier frequency
ts = 1/15.36e6; % sampling time
v = 20; % speed (in km/h)

snrdb = 20; % SNR

% maximum doppler frequency
fd = (v/3.6)*fc/3e8; 

% channel implementation to use, possible values are
% 'genchan' - the genchan function will be used
% 'matlab' - matlab channel functions will be used
channelImpl = 'matlab';

% genchan channel configuration -------------------------------------------
chnsum = 15; % number of sinusoidal sums for channel generation
doptype = 'jakes'; % doppler spectrum ('jakes' or 'flat')

apg = [0 -10 -20]; % average path gain of channel
pd = ([0 1.3 3.2])*ts; % path delays (in seconds)

apg = [0 -10 -20]; % average path gain of channel
pd = ([0.6 1.4 5.3])*ts; % path delays (in seconds)

% matlab channel configuration --------------------------------------------
h = stdchan(ts, fd, '3gppRAx');
%h = stdchan(ts, fd, '3gppTUx');


%% code -------------------------------------------------------------------
assert(mod(ndata,2) == 0, 'ndata must be even');
assert(mod(log2(nsubc),1) == 0, 'nsubc must be a power of 2');

% subcarrier mask
subcmask = logical([0 ones(1,ndata/2) zeros(1,nsubc-ndata-1) ones(1,ndata/2)]);

% transmit data
data = qammod(randi([0 M-1], ndata, nsymb), M);

% transmit signal
tx_f = zeros(nsubc,nsymb);
tx_f(subcmask,:) = data;
tx_t1 = sqrt(1024)*ifft(tx_f);
tx_t = [tx_t1(end-cplen+1:end,:); tx_t1]; % add cp
tx_t = tx_t(:);

if strcmpi(channelImpl, 'genchan')
    % generate time variyng channel coefficients
    h = zeros(length(tx_t), length(apg));
    hap = zeros(chnsum, length(apg)); % doppler frequencies
    hphi = zeros(chnsum, length(apg)); % doppler phases
    for ii = 1:length(apg)
        [x, ap, phi] = genchan(chnsum, length(tx_t), fd*ts, doptype);
        hap(:,ii) = ap;
        hphi(:,ii) = phi;
        h(:,ii) = sqrt(10^(apg(ii)/10))*x;
    end

    % generate resampled channel
    taps_ac = 2; % number of anticausal resampled taps
    taps_c = ceil(2*max(pd)/ts+2); % number of causal resampled taps
    h_resamp = zeros(size(h,1), taps_ac+taps_c);
    for ii = 1:size(h,2)
        h_resamp = h_resamp + h(:,ii)*sinc((-taps_ac:taps_c-1)-pd(ii)/ts);
    end

    % apply  channel to obtain received signal
    rx_t = zeros(size(tx_t));
    for ii = 1:taps_ac
        % apply anticausal taps
        rx_t(1:end-ii) = rx_t(1:end-ii) + ...
                         tx_t(ii+1:end).*h_resamp(ii+1:end,ii);
    end

    for ii = 1:taps_c;
        % apply causal taps
        rx_t(ii:end) = rx_t(ii:end) + ...
                       tx_t(1:end-ii+1).*h_resamp(1:end-ii+1,taps_ac+ii);
    end
elseif strcmpi(channelImpl, 'matlab')
    rx_t = filter(h, tx_t);
else
    error('invalid channel implementation specified');
end

% add noise to the signal
rx_t_eg = var(rx_t(:));
noise_eg = rx_t_eg./(10^(snrdb/10))*nsubc/nnz(subcmask);
noise_t = sqrt(noise_eg/2)*(randn(size(rx_t)) +1j*randn(size(rx_t))); 
rx_t_n = rx_t + noise_t;

% obtain received symbols in frequency domain
%rx_t = rx_t.*exp(1j*2*pi*420*(0:numel(rx_t)-1)*ts).';
rx_t_n = reshape(rx_t_n, nsubc+cplen, nsymb);
rx_t_n = rx_t_n(cplen+1:end,:); % remove cp
rx_f = 1/sqrt(1024)*fft(rx_t_n);

% channel estimation
H = rx_f./tx_f;
H(~subcmask,:) = 0; % assign 0 to non-used subcarriers

% calculate channel impulse response
cir = ifft(H);


%% plot resampled channel taps
if 0 && strcmpi(channelImpl, 'genchan')
    figure();
    for ii = 1:size(h_resamp,2)
        plot3(1e3*ts*(0:size(h_resamp,1)-1),...
              ts*1e6*(ii-taps_ac-1)*ones(1,size(h_resamp,1)), ...
              real(h_resamp(:,ii)));
        grid on;
        hold on;
    end
    xlabel('t [ms]');
    ylabel('\tau [\mus]');
    zlabel('amplitude (real part)');
    title('resampled channel taps');
end
    

%% Plot estimated channel
fmaxsig = 1/(2*ts);
fsig = linspace(-fmaxsig, fmaxsig, nsubc);
tsig = ts*(nsubc+cplen)*(0:nsymb-1);

figure();
surf(tsig*1e3, fsig*1e-6, fftshift(abs(H),1));
shading flat;
xlabel('time [ms]');
ylabel('frequency [MHz]');
title('estimated channel');


%% SAGE algorithm ---------------------------------------------------------

K = 15; % symbol separation between oservations
Na = (nsubc+cplen)*K; % samples separation between observations

L = 200; % number of waves to estimate
I = 4; % maximum number of iterations

% stop criteria - parameters tolerances
toltau = 0.01;
tolv = 0.5;
tola = 1e-3;

% maximum and minimum time delays
sage_max_t = 50; % maximum delay
sage_min_t = -10; % minimum delay

% transmitted symbols in frequency
sage_tx_f = tx_f;

% transmitted signal (u, without cp)
sage_u = tx_t1(:,1:K:nsymb);
sage_u_var = var(sage_u(:));

% matrix of time instants of each sample
sage_u_t = reshape(0:nsymb*(nsubc+cplen)-1, [], nsymb);
sage_u_t = sage_u_t(cplen+1:end, 1:K:nsymb)*ts;

% maximum and minimum doppler frequencies
sage_max_fres = min(1./(2*diff(sage_u_t(1,:)))); % maximum freq. resolution
assert(sage_max_fres >= fd, 'not enough freq. resolution');

sage_max_fd = sage_max_fres; % maximum doppler frequency
sage_min_fd = -sage_max_fres; % minimum doppler frequency

% received signal (y, without cp)
sage_y = rx_t_n(:,1:K:nsymb);

% waves estimates (s)
sage_s = zeros(size(sage_u,1), size(sage_u,2), L);

% waves parameteres (phi)
% struct array with the following fields:
% t: tau; v: nu; a: alpha
sage_phi = repmat(struct('t', 0, 'v', 0, 'a', 0), L, 1);
sage_phi_old = sage_phi;

% SAGE main loop

% GlobalSearch object used to search the tau global minimum
%gs_tau = GlobalSearch('MaxTime', 1, ...
%                      'NumStageOnePoints', 80,...
%                      'NumTrialPoints', 100,...
%                      'Display', 'off');

% GlobalSearch object used to search the v global minimum
%gs_v = GlobalSearch('MaxTime', 1, ...
%                    'NumStageOnePoints', 30,...
%                    'NumTrialPoints', 40,...
%                    'Display', 'off');

et = tic;
h = figure();

ii = 0;
while true
    ii = ii + 1;
    for jj = 1:L
        tic;
        % Expectation step (13) -------------------------------------------
        % obtain estimate of the wave jj (x_jj)
        x_jj2 = sage_s(:,:,jj) + sage_y - sum(sage_s, 3);
        if jj == 1
            x_jj = sage_s(:,:,jj) + sage_y - sum(sage_s, 3);
        else
            x_jj = sage_s(:,:,jj) + x_jj - sage_s(:,:,jj-1);
        end
        assert(all(all(abs(x_jj2 - x_jj) < 1e-14)));
        
        %tic
        % Maximization step (14) ------------------------------------------
        % max tau
        xmat = x_jj.*exp(-1j*2*pi*(sage_phi(jj).v)*sage_u_t);
        taufint = @(t) -abs(sum(sum(conj(circshift(sage_u,t)).*xmat)));
        tauf = @(t) -abs(sum(sum(conj(fshift(sage_u,t)).*xmat)));
        %problem = createOptimProblem('fmincon', 'objective', tauf,...
        %              'x0', 0, 'lb', sage_min_t, 'ub', sage_max_t);
        %tau = run(gs_tau, problem);
        
        % TEST
        tau_test = sage_min_t:sage_max_t;
        [~, mini] = min(arrayfun(taufint, tau_test));
        tau = fminbnd(tauf, max(tau_test(mini)-0.5,sage_min_t), ...
                            min(tau_test(mini)+0.5,sage_max_t));
                    
        % max v
        sage_u_tau = fshift(sage_u, tau);
        xmat = conj(sage_u_tau).*x_jj;
        vf = @(vv) -abs(sum(sum(xmat.*exp(-1j*2*pi*vv*sage_u_t))));
        %problem = createOptimProblem('fmincon', 'objective', vf,...
        %          'x0', 0, 'lb', -fd, 'ub', fd);
        %v = run(gs_v, problem)
        
        % TEST
        %[~, mini] = min(arrayfun(vf, v_test));
        
        if ii == 1 && jj == 1
            % we calculate the frequency of the oscillations in the
            % doppler maximization function and precalculate some
            % complex exponentials
            freq_oscillation = 1/(1024*ts*round(nsymb/2));
            v_test_npoints = ceil(4*(sage_max_fd-sage_min_fd)/freq_oscillation);
            v_test = linspace(sage_min_fd, sage_max_fd, v_test_npoints);
            v_test_diff = v_test(2) - v_test(1);
            v_test_expmat = zeros(size(sage_u_t,1), size(sage_u_t,2), v_test_npoints);
            for kk = 1:length(v_test)
                v_test_expmat(:,:,kk) = exp(-1j*2*pi*v_test(kk)*sage_u_t);
            end
        end
        vf_test = @(ii) -abs(sum(sum(xmat.*v_test_expmat(:,:,ii))));
        [~, mini] = min(arrayfun(vf_test, 1:v_test_npoints));
        vv = fminbnd(vf, max(v_test(mini)-v_test_diff, sage_min_fd),...
                         min(v_test(mini)+v_test_diff, sage_max_fd));
        
        % complex amplitude
        a = 1/(numel(sage_u_tau)*sage_u_var)*sum(...
            sum(xmat.*exp(-1j*2*pi*vv*sage_u_t)));

        % save parameters
        sage_phi(jj).t = tau;
        sage_phi(jj).v = vv;
        sage_phi(jj).a = a;

        % calculate wave estimate for this iteration
        sage_s(:,:,jj) = a*sage_u_tau.*exp(1j*2*pi*vv*sage_u_t);
        
        time = toc;
        fprintf('SAGE OFDM - Iteration %d [%3d] (%5.3f s)\n', ii, jj, time);

        %%
        if 0
            %x = -10:0.25:80;
            %plot(x, -arrayfun(tauf, x));
            x = sage_min_fd:10:sage_max_fd;
            plot(x, -arrayfun(vf, x));
            hold on;
            grid on;
            ax = axis();
            plot([vv vv], [ax(3) ax(4)], '--');
            hold off;
            title(sprintf('max v - Iteration %d [%d]', ii, jj));
            %if ii > 1
            %    pause();
            %end
            %pause(0.5);
            drawnow();
        end
        
        if 0
            x = sage_min_t:0.25:sage_max_t;
            plot(x, -arrayfun(tauf, x));
            hold on;
            grid on;
            ax = axis();
            plot([tau tau], [ax(3) ax(4)], '--');
            hold off;
            title(sprintf('max tau - Iteration %d [%d]', ii, jj));
            drawnow();
            %pause(1);
        end
        
        if 0
            %figure();
            plot(sage_u_t(:,1), real(sage_y(:,1)));
            hold on;
            grid on;
            plot(sage_u_t(:,1), real(x_jj(:,1)), '.--');
            plot(sage_u_t(:,1), real(sage_s(:,1,jj)), '--');
            plot(sage_u_t(:,1), real(sum(sage_s(:,1,:),3)), '--');
            plot(sage_u_t(:,1), real(sage_y(:,1) - sum(sage_s(:,1,:),3)), '.--');
            title(sprintf('Iteration %d [%d]', ii, jj));
            legend('y', 'x_j', 's_j', 'sum(s)', 'y - sum(s)');
            xlim([72 200]*ts);
            hold off;
            pause(0.00001);
            drawnow();

            %if jj == 2
            %    return
            %end
            %pause();
            %return
        end
    end
    
    % check stop criteria
    if mean(abs([sage_phi.t] - [sage_phi_old.t])) < toltau && ...
       mean(abs([sage_phi.a] - [sage_phi_old.a])) < tola && ...
       mean(abs([sage_phi.v] - [sage_phi_old.v])) < tolv
        fprintf('Algorithm finished - minimum tolerance reached\n');
        break;
    elseif ii == I
        fprintf('Algorithm finished - maximum number of iterations reached\n');
        break;
    end
    
    % save this iteration parameters for the next one
    sage_phi_old = sage_phi;
end
toc(et);


%%
figure();
plot(sage_u_t(:,1), real(sage_y(:,1)));
hold on;
grid on;
plot(sage_u_t(:,1), real(rx_t(cplen+(1:nsubc))), '--');
plot(sage_u_t(:,1), real(sum(sage_s(:,1,:),3)), '--');
plot(sage_u_t(:,1), real(rx_t(cplen+(1:nsubc)) - sum(sage_s(:,1,:),3)), '.--');
legend('rx', 'rx (no noise)', 'sage estimate', 'rx (no noise) - sage est.');

signalpw_est = var(sage_y(:,1));
noisepw_est = var(sage_y(:,1) - sum(sage_s(:,1,:),3));
fprintf('Actual noise pw (dB): %f\n', 10*log10(var(noise_t)));
fprintf('Estimated noise pw (dB): %f\n', 10*log10(noisepw_est));
fprintf('SNR estimated (dB): %f\n',...
        10*log10((signalpw_est-noisepw_est)./noisepw_est));


%%
% estimated received symbols in frequency domain from sage
rx_s_t = sum(sage_s,3);
rx_s_f = 1/sqrt(1024)*fft(rx_s_t);

% channel estimation first symbol
Hs = rx_s_f(:,1)./tx_f(:,1);
Hs(~subcmask,:) = 0; % assign 0 to non-used subcarriers

% plot channel
fmaxsig = 1/(2*ts);
fsig = linspace(-fmaxsig, fmaxsig, nsubc);
tsig = ts*(nsubc+cplen)*(0:nsymb-1);

figure();
plot(fsig*1e-6, fftshift(abs(Hs),1));
hold on;
grid on;
plot(fsig*1e-6, fftshift(abs(H(:,1)),1), '--');
xlabel('time [ms]');
ylabel('frequency [MHz]');
title('estimated channel from sage');
legend('sage estimation', 'original estimation');

%%
figure();
v_vec = [sage_phi.v];
a_vec = [sage_phi.a];
stem(v_vec, (abs(a_vec).^2), '.');
grid on;
hold on;
ax = axis();
plot([fd fd], [ax(3) ax(4)], 'r--');
plot([-fd -fd], [ax(3) ax(4)], 'r--');

return
%%
e = ...
sageofdm1rx('numwavesest', L, 'maxiter', I,...
            'maxtau', sage_max_t, 'mintau', sage_min_t,...
            'maxv', 778, 'minv', -778,...
            'txsamp', sage_u, 'rxsamp', sage_y,...
            'tsymb', sage_u_t(1,:), 'ts', ts,...
            'toltau', toltau, 'tolv', tolv, 'tola', tola,...
            'display', 'iter');



