close all;

subcmask = logical([0 ones(1,300) zeros(1,1024-601) ones(1,300)]);
ndata = nnz(subcmask);
nsubc = length(subcmask);
M = 4;
cp = 72;

apg = [0 -10 -10 -20 -50]; % average path gain


%% code
h = sqrt(10.^(apg/10)/2).*(randn(1,length(apg)) + 1j*randn(1,length(apg)));
%h = [1 0.1 0.01] + 1j*[-2 0.2 -0.03];

% transmit data
data = qammod(randi([0 M-1], 1, ndata), M);

% transmit signal
tx_f = zeros(1,nsubc);
tx_f(subcmask) = data;
tx_t1 = 1/sqrt(1024)*ifft(tx_f);
tx_t = [tx_t1(end-cp+1:end) tx_t1];

% receive signal
rx_t = conv(h,tx_t);
rx_t = rx_t(cp:nsubc+cp-1);
rx_f = sqrt(1024)*fft(rx_t);
H = rx_f./tx_f;

%%
figure();
plot(fftshift(abs(H)));
hold on;
grid on;
plot(fftshift(abs(fft(h,nsubc))), '--');

%%
figure()
H0 = H;
H0(~subcmask) = 0;
vf = @(x) abs(x);
stem(vf(h));
grid on;
hold on;
stem(vf(ifft(H0)));
%stem(vf(ifft(H0([1:256 end-255:end]))));
xcorr_val = xcorr(tx_t1,rx_t);
stem(vf(1024*xcorr_val(1023:end)), '-x');
xlim([1 20]);
legend('actual', 'ifft', 'xcorr');





