close all;
clear;
%rng(67);

%% Channel configuration

fc = 2.6e9; % carrier frequency
ts = 1/15.36e6; % sampling time
v = 100; % speed (in km/h)

snrdb = 15; % SNR

% maximum doppler frequency
fd = (v/3.6)*fc/3e8; 

% channel implementation to use, possible values are
% 'genchan' - the genchan function will be used
% 'matlab' - matlab channel functions will be used
%channelImpl = 'matlab';
channelImpl = 'genchan';

% genchan channel configuration -------------------------------------------
chnsum = 5; % number of sinusoidal sums for channel generation
doptype = 'jakes'; % doppler spectrum ('jakes' or 'flat')

%apg = [0 -10 -20]; % average path gain of channel
%pd = ([0 1.3 3.2])*ts; % path delays (in seconds)

%apg = [0 -10 -20]; % average path gain of channel
%pd = ([0.6 1.4 5.3])*ts; % path delays (in seconds)

apg = [0 -5 -10]; % average path gain of channel
pd = ([0 2 4])*ts; % path delays (in seconds)

% matlab channel configuration --------------------------------------------
%h = stdchan(ts, fd, '3gppRAx');
%h = stdchan(ts, fd, 'itur3GVAx');
h = stdchan(ts, fd, 'itur3GPAx');

%h = stdchan(ts, fd, '3gppTUx');


%% LTE signal generation

% Generate the configuration for R.2
rmc = lteRMCDL('R.2');
% Generate a random signal to transmit
Data = randi([0 1], 1, sum(rmc.PDSCH.TrBlkSizes));
% Generate the standard-compliant data
[waveform, txgrid, enb] = lteRMCDLTool(rmc, Data);



%% Apply channel --------------------------------------------------

% transmit signal
tx_t = waveform;

if strcmpi(channelImpl, 'genchan')
    % generate time variyng channel coefficients
    h = zeros(length(tx_t), length(apg));
    hap = zeros(chnsum, length(apg)); % doppler frequencies
    hphi = zeros(chnsum, length(apg)); % doppler phases
    for ii = 1:length(apg)
        [x, ap, phi] = genchan(chnsum, length(tx_t), fd*ts, doptype);
        hap(:,ii) = ap;
        hphi(:,ii) = phi;
        h(:,ii) = sqrt(10^(apg(ii)/10))*x;
    end

    % generate resampled channel
    taps_ac = 2; % number of anticausal resampled taps
    taps_c = ceil(2*max(pd)/ts+2); % number of causal resampled taps
    h_resamp = zeros(size(h,1), taps_ac+taps_c);
    for ii = 1:size(h,2)
        h_resamp = h_resamp + h(:,ii)*sinc((-taps_ac:taps_c-1)-pd(ii)/ts);
    end

    % apply  channel to obtain received signal
    rx_t = zeros(size(tx_t));
    for ii = 1:taps_ac
        % apply anticausal taps
        rx_t(1:end-ii) = rx_t(1:end-ii) + ...
                         tx_t(ii+1:end).*h_resamp(ii+1:end,ii);
    end

    for ii = 1:taps_c;
        % apply causal taps
        rx_t(ii:end) = rx_t(ii:end) + ...
                       tx_t(1:end-ii+1).*h_resamp(1:end-ii+1,taps_ac+ii);
    end
elseif strcmpi(channelImpl, 'matlab')
    rx_t = filter(h, tx_t);
else
    error('invalid channel implementation specified');
end

% add noise to the signal
rx_t_eg = var(rx_t(:));
noise_eg = rx_t_eg./(10^(snrdb/10))*double(enb.Nfft)/size(txgrid,1);
noise_t = sqrt(noise_eg/2)*(randn(size(rx_t)) +1j*randn(size(rx_t))); 
rx_t_n = rx_t + noise_t;


%% OFDM demodulation and channel estimation -----------------------
rxgrid = lteOFDMDemodulate(enb, rx_t_n);

% Estimate the channel on the middle 6 RBs
cec = struct('FreqWindow',7,'TimeWindow',1,'InterpType','cubic', ...
             'PilotAverage','UserDefined','InterpWindow','Centered',...
             'InterpWinSize', 3);
[ChannelEst, noiseEst] = lteDLChannelEstimate(enb,cec,rxgrid);


%% plot resampled channel taps
if 0 && strcmpi(channelImpl, 'genchan')
    figure();
    for ii = 1:size(h_resamp,2)
        plot3(1e3*ts*(0:size(h_resamp,1)-1),...
              ts*1e6*(ii-taps_ac-1)*ones(1,size(h_resamp,1)), ...
              real(h_resamp(:,ii)));
        grid on;
        hold on;
    end
    xlabel('t [ms]');
    ylabel('\tau [\mus]');
    zlabel('amplitude (real part)');
    title('resampled channel taps');
end
    

%% Plot estimated channel
fmaxsig = 1/(2*ts)*(600/1024);
fsig = linspace(-fmaxsig, fmaxsig, 600);
tsig = linspace(0, 0.01, 140);

figure();
surf(tsig*1e3, fsig*1e-6, abs(ChannelEst));
shading flat;
xlabel('time [ms]');
ylabel('frequency [MHz]');
title('estimated channel');
view([0 90]);
c = caxis;


%% Plot estimated channel (first symbol)
% plot((real(H(:,1))));
% hold on;
% %plot((real(exp(1j*2*pi*((0:1024-1))*2.5/1024))));
% plot((real(exp(-1j*2*pi*([0:512, -511:1:1])*2.5/1024))));
% grid on;
% hold on;
% title('estimated channel');


%% SAGE algorithm in freq domain ----------------------------------

L = 25; % number of waves to estimate
I = 10; % maximum number of iterations

% stop criteria - parameters tolerances
toltau = 0.01;
tolv = 0.5;
tola = 1e-3;

% maximum and minimum time delays
sage_max_t = 50; % maximum delay
sage_min_t = -10; % minimum delay

% LTE reference signal indexes and values
lte_rs = [];
lte_rs_ind = [];
enb_aux = enb;

nsubframes = 10;
symbols_subframe = 14;
for ii = 1:nsubframes
    enb_aux.NSubframe = ii-1;
    lte_rs = [lte_rs; lteCellRS(enb_aux)]; %#ok<AGROW>
    lte_rs_ind = [lte_rs_ind; lteCellRSIndices(enb_aux) + ...
                              size(rxgrid,1)*symbols_subframe*(ii-1)]; %#ok<AGROW>
end
assert(all(lte_rs == txgrid(lte_rs_ind)))


[lte_rs_ind_f, lte_rs_ind_t] = ind2sub(size(txgrid), lte_rs_ind);


% time instants for each symbol
ofdminfo = lteOFDMInfo(enb);
nfft = double(ofdminfo.Nfft);
cpsamples = repmat(ofdminfo.CyclicPrefixLengths, 1, nsubframes);
timesamp = double(cumsum(cpsamples+nfft) - nfft/2);

sage_X_t = timesamp(lte_rs_ind_t).'*ts;

% freq index for each symbol
% NOTE - In order to work with fractional time offsets indexes must
% be chosen from negative freq to positive
freqvals = [-300:-1 1:300].'/nfft;
sage_X_f = freqvals(lte_rs_ind_f);

% transmitted symbols
sage_X = lte_rs;
sage_X_var = var(sage_X);

% received symbols
sage_Y = rxgrid(lte_rs_ind);

% maximum and minimum doppler frequencies
sage_max_fres = min(1./(2*diff(sage_X_t))); % maximum freq. resolution
assert(sage_max_fres >= fd, 'not enough freq. resolution');

sage_max_fd = fd*1.2; % maximum doppler frequency
sage_min_fd = -fd*1.2; % minimum doppler frequency

%% SAGE algorithm
e = ...
sagefreq1rx('numwavesest', L, 'maxiter', I,...
            'maxtau', sage_max_t, 'mintau', sage_min_t,...
            'maxv', sage_max_fd, 'minv', sage_min_fd,...
            'X', sage_X, 'Y', sage_Y,...
            'X_t', sage_X_t, 'X_f', sage_X_f,...
            'toltau', toltau, 'tolv', tolv, 'tola', tola,...
            'display', 'iter');


%% Plot SAGE estimated channel

fmaxsig = (size(txgrid,1)/2)/(ts*nfft);
f1 = linspace(-fmaxsig, fmaxsig, size(txgrid,1)+1);

%f1 = [f1(1:300) f1(302:end)];
%t1 = linspace(0, 0.01, 140);
t1 = timesamp*ts;

tic;
a_vec = [e.a];
v_vec = [e.v];
tau_vec = [e.t]*ts;
t_off = 0;

% Fourier transform from the estimated taps
t_mat = repmat(t1, numel(f1), 1, numel(e));
f_mat = repmat(f1.', 1, numel(t1), numel(e));
v_mat = repmat(reshape(v_vec,1,1,[]), numel(f1), numel(t1), 1);
tau_mat = repmat(reshape(tau_vec,1,1,[]), numel(f1), numel(t1), 1);
a_mat = repmat(reshape(a_vec,1,1,[]), numel(f1), numel(t1), 1);

H_sage = sum(a_mat.*...
             exp(-1j*2*pi*f_mat.*(-t_off*ts)).*...
             exp(1j*2*pi*v_mat.*t_mat).*...
             exp(-1j*2*pi*f_mat.*tau_mat), 3);
toc;

figure();
surf(t1*1e3, f1*1e-6, abs(H_sage));
shading flat;
xlabel('time [ms]');
ylabel('frequency [MHz]');
title('SAGE estimated channel');
view([0 90]);
caxis(c);

