% Calculate and plot doppler spectrum using the correlation of the channel
% and the SAGE algorithm

close all;

%% configuration

ndata = 600;  % number of used subcarriers
nsubc = 1024; % number of total subcarriers
nsymb = 6*140; % number of time symbols per iter
cplen = 72; % CP length
M = 4; % QAM constellation order

fc = 2.6e9; % carrier frequency
ts = 1/15.36e6; % sampling time
v = 200; % speed in km/h

fd = (v/3.6)*fc/3e8; % maximum doppler frequency
ch = stdchan(ts, fd, '3gppRAx');
%ch = stdchan(ts, fd, '3gppTUx');
%ch = rayleighchan(ts, fd, 0, 0);
ch.ResetBeforeFiltering = 0;

% iterations of the script
niter = 40;

% sage options
nwaves = 30;
maxiter = 3;
sage_max_t = 60;
sage_min_t = -20;
toltau = 0.1;
tolv = 0.01;
tola = 1e-3;



%% main code
assert(mod(ndata,2) == 0, 'ndata must be even');
assert(mod(log2(nsubc),1) == 0, 'nsubc must be a power of 2');

% subcarrier mask
subcmask = logical([0 ones(1,ndata/2) zeros(1,nsubc-ndata-1) ones(1,ndata/2)]);

% subcarrier mask including the DC subcarrier
subcmask0 = subcmask;
subcmask0(1) = true;

corrmaxtaps = 70; % number of taps to correlate
corrmat = zeros(nsymb*2-1, corrmaxtaps, niter); % correlation matrix

txcell = cell(1, niter*nsymb/140);
rxcell = cell(1, niter*nsymb/140);
sage_est = cell(1, niter*nsymb/140);

for kk = 1:niter
    % transmit data
    data = qammod(randi([0 M-1], ndata, nsymb), M);

    % transmit signal
    tx_f = zeros(nsubc,nsymb);
    tx_f(subcmask,:) = data;
    tx_t1 = 1/sqrt(1024)*ifft(tx_f);
    tx_t = [tx_t1(end-cplen+1:end,:); tx_t1]; % add cp
    tx_t = tx_t(:);

    % apply channel to obtain received signal
    rx_t = filter(ch, tx_t);

    % obtain received symbols in frequency domain
    %rx_t = rx_t.*exp(1j*2*pi*120*(0:numel(rx_t)-1)*ts).';
    rx_t = reshape(rx_t, nsubc+cplen, nsymb);
    rx_t = rx_t(cplen+1:end,:); % remove cp
    rx_f = sqrt(1024)*fft(rx_t);

    % channel estimation
    H = rx_f./tx_f;
    H(~subcmask,:) = 0; % assign 0 to non-used subcarriers
    %H(1,:) = (H(2,:)+H(end,:))/2;
    %H = H(subcmask0,:);
    
    % calculate channel autocorrelation
    cir = ifft(H);
    for jj = 1:corrmaxtaps
        corrmat(:,jj,kk) = xcorr(cir(jj,:));
    end
    
    % save tx and rx observations in time domain for the SAGE algorithm
    for jj = 1:nsymb/140
        K = 9; % separation between OFDM symbols
        txcell{(kk-1)*(nsymb/140)+jj} = tx_t1(:,140*(jj-1)+1:K:140*jj);
        rxcell{(kk-1)*(nsymb/140)+jj} = rx_t(:,140*(jj-1)+1:K:140*jj);
    end
    
    kk
end
    
%% calculate SAGE estimates
tsymb = reshape(0:140*(nsubc+cplen)-1, [], 140);
tsymb = tsymb(cplen+1, 1:K:140)*ts;

parfor jj = 1:length(sage_est)
    txsamp = txcell{jj};
    rxsamp = rxcell{jj};
    
    timer = tic();
    sage_est{jj} = ...
        sageofdm1rx('numwavesest', nwaves, 'maxiter', 10,...
                    'maxtau', sage_max_t, 'mintau', sage_min_t,...
                    'maxv', 778, 'minv', -778,...
                    'txsamp', txsamp, 'rxsamp', rxsamp,...
                    'tsymb', tsymb, 'ts', ts,...
                    'toltau', toltau, 'tolv', tolv, 'tola', tola,...
                    'display', 'off');
    t = toc(timer);
    fprintf('[%3d/%3d] SAGE algorithm finished (%f s)\n',...
            jj, length(sage_est), t);
end


%% calculate doppler spectral power (dsp)
%L = 1024*2;
dsp = zeros(nsymb*2-1, niter);
%dsp = zeros(L, niter);
for kk = 1:niter
    for jj = 1:corrmaxtaps
        dsp(:,kk) = dsp(:,kk) + fftshift(fft(corrmat(:,jj,kk)));
    end
end

%% calculate frequency x axis values
fmax = 1/(2*ts*(1024+72));
f = linspace(-fmax, fmax, size(dsp,1));

%% Plot estimated spectrum for 1st iteration
figure();
plot(f,(abs(dsp(:,1))), '.-');
ax = axis();
grid on;
hold on;
plot([fd fd], [ax(3) ax(4)], '--', 'color', [1 1 1]*0.5);
plot(-[fd fd], [ax(3) ax(4)], '--', 'color', [1 1 1]*0.5);
legend('doppler spectrum', 'max/min doppler');
title('estimated doppler for 1st iteration');


%% Plot estimated spectrum image
if niter > 1
    figure();
    surf(1:niter, f, 10*log10(abs(dsp)));
    hold on;
    grid on;
    title('estimated doppler');
    %shading flat;
    shading interp;
    ylim([-fd fd]*3);
    zlim([-10, 50]);
    view([0 90]);
end

%% Plot mean estimated spectrum
if 0
    figure();
    meandsp = abs(mean(dsp,2));
    plot(f, meandsp/max(meandsp), '.-');
    ax = axis();
    grid on;
    hold on;
    plot([fd fd], [ax(3) ax(4)], '--', 'color', [1 1 1]*0.5);
    plot(-[fd fd], [ax(3) ax(4)], '--', 'color', [1 1 1]*0.5);
    fjakes = linspace(-fd, fd, 100);
    sjakes = 1./(pi*fd)*1./sqrt(1-(fjakes./fd).^2);
    plot(fjakes, (sjakes/sjakes(2))+0.2);
    legend('doppler spectrum', 'max/min doppler');
    title('mean estimated doppler');
end

%% Plot sage points
figure();
numinterp = 300;
freq_ind = linspace(-778, 778, numinterp);
freq_mat = zeros(length(freq_ind), length(sage_est));

for ii = 1:length(sage_est)
    for jj = 1:nwaves
        freq_mat(:,ii) = freq_mat(:,ii) +...
            abs(abs(sage_est{ii}(jj).a)).^2*...
            rcos(freq_ind-sage_est{ii}(jj).v, 0.8, 2*778/numinterp).^2.';
    end
end
%plot(X(:), Y(:), '.');
surf(1:nsymb/140*niter, freq_ind, 10*log10(abs(freq_mat)));
colorbar();
view([0 90]);
shading interp;
caxis([-55 -10])
title('SAGE doppler estimations');



%%
figure()
for ii = 1:2
    stem([sage_est{ii}.v], abs([sage_est{ii}.a].^2), '.');
    hold on
    %plot(freq_ind, freq_mat(:,ii));
end

