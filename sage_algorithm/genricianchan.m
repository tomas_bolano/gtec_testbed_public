function [x, x_mp, x_LOS] = ...
    genricianchan(S,m,ts,fd,pdb,k,fdLOS,ds,normalize)
%GENCHAN Generate rician channel taps
%
% Inputs:
% S   - Number of sums of sinusoids to use
% m   - Number of samples to generate
% ts  - sampling time
% fd  - Max doppler frequency in Hz
% pdb - Vector of average path gains in dB. The number of taps generated
%       will be the same as the number of elements of this vector.
% k   - Vector of k factors. The size of k must be less or equal than the
%       size of pdb. Each element of k indicates the K-Factor value of the
%       correnponding tap generated. If the size of k is less than the size
%       of pdb then for the rest of the taps a K-Factor = 0 (rayleigh
%       fading) will be used. Default 0 for all taps.
% fdLOS - Vector of the same size of k indicating the doppler frequency in
%         Hz of the LoS component for each tap. Default 0 for all the taps.
% ds  - Cell of doppler spectrum to use for each tap. Available values are:
%       'jakes', and 'flat'. Default is 'jakes' for all taps.
% normalize - Normalize channel power. Default 1.
%
% Outputs:
% x     - matrix of samples of channel taps.
% x_mp  - Multipath component of x 
% x_LOS - LoS component of x


N = numel(pdb); % number of taps to generate

%% Check parameters
if nargin < 6 || isempty(k); k = zeros(1,N); end
if nargin < 7 || isempty(fdLOS); fdLOS = zeros(1,N); end
if nargin < 8 || isempty(ds); ds = {'jakes'};
if nargin < 9 || isempty(normalize); normalize = 1; end

if length(ds) == 1;
    ds = repmat(ds, 1, N); end
end

assert(numel(k) <= N,...
       'Lenght of k must be less or equal than the lenght of pdb');
assert(numel(fdLOS) <= N,...
       'Lenght of fdLOS must be less or equal than the lenght of pdb');
assert(numel(ds) <= N,...
       'Lenght of ds must be less or equal than the lenght of pdb');

if numel(k) < N
    k = [reshape(k,1,[]), zeros(1,numel(k)-N)];
end
assert(all(k >= 0), 'all elements of k must be positive');
k(isinf(k)) = 1e16; % replace infinites by a finite but big enough value.

if numel(fdLOS) < N
    fdLOS = [reshape(fdLOS,1,[]), zeros(1,numel(k)-N)];
end
assert(all(fdLOS <= fd) && all(fdLOS >= -fd),...
       'fdLOS elements must be in [-fd,fd]');
        
if numel(ds) < N
    ds = {reshape(ds,1,[]), repmat('jakes', 1, numel(ds)-N)};
end


%% Generate channel taps

x = zeros(m,N);
x_mp = zeros(m,N);
x_LOS = zeros(m,N);

pw = 10.^(pdb./10); % taps powers in natural units
n = 0:m-1; % discrete time indices

if normalize
    pw = pw./sum(pw);
end

for ii = 1:N
    % ------------------------------
    % multipath component (rayleigh)
    % ------------------------------
    switch ds{ii}
        case 'jakes'
            ap = cos(rand(1,S)*2*pi);
        case 'flat'
            ap = rand(1,S)*2-1;
        otherwise
            error('invalid doppler type');
    end
    
    phi = rand(1,S)*2*pi; % random phase
    
    h = zeros(1,m);
    for jj = 1:S
        h = h + exp(1j*(2*pi*fd*ts*ap(jj)*n + phi(jj)));
    end
    h = sqrt(1/S)*h;
    x_mp(:,ii) = sqrt(pw(ii))*sqrt(1/((1+k(ii))))*h;
    
    % --------------
    % LoS component
    % --------------
    phi_LOS = rand()*2*pi;
    h_LOS = exp(1j*(2*pi*fdLOS(ii)*ts*n + phi_LOS));
    x_LOS(:,ii) = sqrt(pw(ii))*sqrt(k(ii)/(k(ii)+1))*h_LOS;
    
    % -----------------
    % Complete channel
    % -----------------
    %hc = sqrt(pw(ii))*(sqrt(k(ii)/(k(ii)+1))*h_LOS +...
    %                   sqrt(1/((1+k(ii))))*h);               
    %x(:,ii) = hc;
end

% complete channel
x = x_mp + x_LOS;

end
