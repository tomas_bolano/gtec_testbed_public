close all;

%% configuration

ndata = 600;  % number of used subcarriers
nsubc = 1024; % number of total subcarriers
nsymb = 3*140; % number of time symbols
cplen = 72; % CP length
M = 4; % QAM constellation order

fc = 2.6e9; % carrier frequency
ts = 1/15.36e6; % sampling time
v = 200; % speed in km/h

chnsum = 25; % number of sinusoidal sums for channel generation
doptype = 'jakes'; % doppler spectrum ('jakes' or 'flat')

apg = [0 -10]; % average path gain of channel
pd = 0:1; % path delays (only integers, no resampling is made)


%% code
assert(mod(ndata,2) == 0, 'ndata must be even');
assert(mod(log2(nsubc),1) == 0, 'nsubc must be a power of 2');

% subcarrier mask
subcmask = logical([0 ones(1,ndata/2) zeros(1,nsubc-ndata-1) ones(1,ndata/2)]);

% maximum doppler frequency
fd = (v/3.6)*fc/3e8; 

% transmit data
data = qammod(randi([0 M-1], ndata, nsymb), M);

% transmit signal
tx_f = zeros(nsubc,nsymb);
tx_f(subcmask,:) = data;
tx_t1 = 1/sqrt(1024)*ifft(tx_f);
tx_t = [tx_t1(end-cplen+1:end,:); tx_t1]; % add cp
tx_t = tx_t(:);
 
% generate time variyng channel coefficients
h = zeros(length(tx_t), length(apg));
hap = zeros(chnsum, length(apg)); % doppler frequencies
hphi = zeros(chnsum, length(apg)); % doppler phases
for ii = 1:length(apg)
    [x, ap, phi] = genchan(chnsum, length(tx_t), fd*ts, doptype);
    hap(:,ii) = ap;
    hphi(:,ii) = phi;
    h(:,ii) = sqrt(10^(apg(ii)/10))*x;
end

% apply channel to obtain received signal
rx_t = zeros(size(tx_t));
for ii = 1:length(apg)
    rx_t(1+pd(ii):end) = rx_t(1+pd(ii):end) + tx_t(1:end-pd(ii)).*h(1:end-pd(ii),ii);
end

% obtain received symbols in frequency domain
%rx_t = rx_t.*exp(1j*2*pi*420*(0:numel(rx_t)-1)*ts).';
rx_t = reshape(rx_t, nsubc+cplen, nsymb);
rx_t = rx_t(cplen+1:end,:); % remove cp
rx_f = sqrt(1024)*fft(rx_t);

% channel estimation
H = rx_f./tx_f;
H(~subcmask,:) = 0; % assign 0 to non-used subcarriers

% calculate channel impulse response
cir = ifft(H);

%% calculate doppler frequency x axis values
fmax = 1/(2*ts*(1024+72));
f = linspace(-fmax, fmax, nsymb*2-1);

%% calculate signal frequency and time indexes
fmaxsig = 1/(2*ts);
fsig = linspace(-fmaxsig, fmaxsig, nsubc);
tsig = ts*(nsubc+cplen)*(0:nsymb-1);


%% plot estimated channel
figure();
surf(tsig*1e3, fsig*1e-6, fftshift(abs(H),1));
shading flat;
xlabel('time [ms]');
ylabel('frequency [MHz]');
title('estimated channel');


%% plot CIR
maxcir = 4;
% figure();
% surf(abs(cir(1:maxcir,:)));
% shading flat;
% title('CIR');

figure();
figlegend = cell(1,maxcir);
for ii = 1:maxcir
    plot(tsig*1e3, real(cir(ii,:)));
    grid on;
    hold on;
    figlegend{ii} = sprintf('tap %d', ii);
end
xlabel('time [ms]');
ylabel('amplitude');
title('CIR (real part)');
legend(figlegend);


%% plot correlation 
chcorr = xcorr((cir(1,:)));

figure();
plot(real(chcorr));
title('channel correlation tap 1');

%% estimate and plot doppler spectrum
userealchan = 0;
if userealchan
    % use the real generated channel impulse response
    dsp = zeros(size(h,1)*2-1,1);
    for ii = 1:size(h,2)
        dsp = dsp + fftshift(fft(xcorr(h(:,ii))));
    end
    dsp = dsp((-nsymb+1:nsymb-1)+round(length(dsp)/2));
else
    % use the estimated channel impulse response
    corrmaxtaps_c = 10;
    corrmaxtaps_ac = 10;
    dsp = zeros(1,nsymb*2-1);
    for ii = [1:corrmaxtaps_c size(cir,1)-corrmaxtaps_ac+1:size(cir,1)]
        dsp = dsp + fftshift(fft(xcorr(cir(ii,:))));
    end
end

% doppler path frequencies and phases
dopfreq = fd*hap;
dopphase = exp(1j*hphi);
% escale doppler paths by the path average power
dopphase = bsxfun(@times, 10.^(apg/10), dopphase);
dopfreq = dopfreq(:);
dopphase = dopphase(:);

figure();
plot(f, abs(dsp), '.-');
grid on;
hold on;

% create extended vector of doppler frequencies and phases
dopfreq_ext = [dopfreq; f.'];
dopphase_ext =  [dopphase; zeros(length(f),1)];
% sort vectors
[dopfreq_ext,I] = sort(dopfreq_ext);
dopphase_ext = dopphase_ext(I);

% create vectors of 3d points and directions to plot
dopfreq3d = [dopfreq_ext, zeros(size(dopfreq_ext,1), 2)];
dopphase3d = [zeros(size(dopphase_ext,1),1), real(dopphase_ext), imag(dopphase_ext)];
dopphase3d = 2e4*dopphase3d;

% plot 3d vectors
quiver3(dopfreq3d(:,1), dopfreq3d(:,2), dopfreq3d(:,3),...
        dopphase3d(:,1),dopphase3d(:,2),dopphase3d(:,3),...
        0, 'MaxHeadSize', 0.01);
    
% plot 2d doppler smoothed amplitude
%dopphase_s = abs(smooth(3e4*dopphase_ext, 3));
%plot(dopfreq_ext, dopphase_s, '.-');

% plot 3d with sinc resampling
%sincf = linspace(-2*fd, 2*fd, 100000);
%sincp = 2e4*real(dopphase(1))*sinc((sincf-dopfreq(1))/(f(2)-f(1)));
%plot(sincf, sincp, '--');

sincf = f;
sincp = zeros(1,length(f));
for jj = 1:length(apg)
    for ii = 1:chnsum
        sincp = sincp + 10^(apg(jj)/10)*...
                exp(1j*hphi(ii,jj))*sinc((sincf-fd*hap(ii,jj))/(2*(f(2)-f(1)))).^2;
    end
end
sincp = max(dsp)*abs(sincp)./max(abs(sincp));
plot(sincf, abs(sincp), '--');

title('doppler spectrum');
legend('estimated', 'phases and amplitudes', 'smoothed amplitude');
ylabel('amplitude');
xlabel('frequency [Hz]');

return
%%
figure();
plot(fftshift(abs(H)));
hold on;
grid on;
plot(fftshift(abs(fft(h,nsubc))), '--');

%%
figure()
H0 = H;
H0(~subcmask) = 0;
vf = @(x) abs(x);
stem(vf(h));
grid on;
hold on;
stem(vf(ifft(H0)));
%stem(vf(ifft(H0([1:256 end-255:end]))));
xcorr_val = xcorr(tx_t1,rx_t);
stem(vf(1024*xcorr_val(1023:end)), '-x');
xlim([1 20]);
legend('actual', 'ifft', 'xcorr');
