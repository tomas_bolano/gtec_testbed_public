function [e, s] = sagefreq1rx(varargin)
%SAGEFREQ1RX SAGE algorithm in freq. domain with 1 receiver antenna
%
% See reference "Channel Parameter Estimation in Mobile Radio
% Environments Using the SAGE Algorithm" for more details
%
% Input parameters are specified as a list of name, value pairs.
% The available input properties are the following:
%   'numwavesest'
%       Number of waves to estimate.
%       Default: 150.
%   'maxiter'
%       Maximum number of iterations.
%       Default: 10.
%   'maxtau'
%       Maximum delay (tau) to estimate, specified as an integer number
%       of samples.
%       Default: 70
%   'mintau'
%       Minimum delay (tau) to estimate, specified as an integer number of
%       samples.
%       Default: -10
%   'maxv'
%       Maximum doppler freq (v) to estimate.
%   'minv'
%       Minimum doppler freq (v) to estimate.
%   'X'
%       Vector of transmitted symbols.
%   'Y'
%       Vector of received symbols.
%   'X_f'
%       Frequencies for each symbol. Normalized from -0.5 to 0.5.
%   'X_t'
%       Time instants for each symbol (middle sample).
%   'intitial_state'
%       Vector of numwavesest parameters to use as initial state for each
%       tap. If empty the parameters used for the intial state will be set
%       to zero. Default: [].       
%   'toltau'
%       Tolerance of tau estimated parameters. The algorithm will not stop
%       until the tolerance is reached or the maximum number of iterations
%       is reached. Default: 0.1;
%   'toltau_type'
%       Method used to calculate tolerance. Available values are:
%       - 'Absolute': the tolerance will be calculated as 
%         mean(abs(tau_new - tau_old)).
%       - 'Relative': the tolerance will be calculated as 
%         mean(abs(tau_new - tau_old)./abs(tau_new))).
%       Default: 'Relative'.
%   'tolv'
%       Tolerance of v (doppler frequency) estimated parameters. The
%       algorithm will not stop until the tolerance is reached or the
%       maximum number of iterations is reached. Default: 0.1;
%   'tolv_type'
%       Method used to calculate tolerance. Available values are:
%       - 'Absolute': the tolerance will be calculated as 
%         mean(abs(v_new - v_old)).
%       - 'Relative': the tolerance will be calculated as 
%         mean(abs(v_new - v_old)./abs(v_new))).
%       Default: 'Relative'.
%   'tola'
%       Tolerance of a (complex amplitude) estimated parameters.The
%       algorithm will not stop until the tolerance is reached or the
%       maximum number of iterations is reached. Default: 0.1;
%   'tola_type'
%       Method used to calculate tolerance. Available values are:
%       - 'Absolute': the tolerance will be calculated as 
%         mean(abs(a_new - a_old)).
%       - 'Relative': the tolerance will be calculated as 
%         mean(abs(a_new - a_old)./abs(a_new))).
%       Default: 'Relative'.
%   'display'
%       Detail level of iterative display. Possible values:
%       - 'final' only print final report
%       - 'iter'  print information for each iteration
%       - 'off'   disable printing information (default)


%% Parameter initializations ----------------------------------------------

% Create cell matrix of input name parameters
% Format of this cell matrix is (per columns):
% parameter name, default value, required (i.e. not optional), updated
parameters = ...
{'numwavesest',  150,   0,  0
 'maxiter',     10,     0,  0
 'maxtau',      70,     0,  0
 'mintau',     -10,     0,  0
 'maxv',        [],     1,  0
 'minv',        [],     1,  0
 'X',           [],     1,  0
 'Y',           [],     1,  0
 'X_f',         [],     1,  0
 'X_t',         [],     1,  0
 'tau_discrete', 0,     0,  0
 'initial_state', [],  0,  0
 'toltau',      0.1,    0,  0
 'toltau_type', 'Relative', 0, 0
 'tolv',        0.1,      0,  0
 'tolv_type',   'Relative', 0, 0
 'tola',        0.1,   0,  0
 'tola_type',   'Relative', 0, 0
 'display',     'off',  0,  0};

% read parameters
if mod(length(varargin),2) ~= 0
    error('missing parameter value');
end

for ii = 1:2:length(varargin)
    str_ind = find(strcmpi(parameters(:,1), varargin{ii}), 1);
    
    if isempty(str_ind)
        error('invalid parameter %s', varargin{ii});
    end
    
    parameters{str_ind, 4} = 1;
    parameters{str_ind, 2} = varargin{ii+1};
end

% check that all required parameters are specified
a = cell2mat(parameters(:,[3 4]));
assert(all(all(~a(:,1) | a(:,2))),...
       'some required parameters are missing');

% add parameters to p struct
p = struct();
for ii = 1:size(parameters,1)
    p.(parameters{ii,1}) = parameters{ii,2};
end

% display options
disp_final = 0;
disp_iter = 1;
disp_off = 2;

switch p.display
    case 'final', disp_val = disp_final;
    case 'iter',  disp_val = disp_iter;
    case 'off',   disp_val = disp_off;
    otherwise
        error('invalid display value');
end


%% SAGE variables initialization ------------------------------------------

% check tau and v min and max values
assert(p.mintau < p.maxtau, 'mintau must be less than maxtau');
assert(p.minv < p.maxv, 'minv must be less than maxv');

% maximum freq. resolution
sage_maxfreqres = min(1./(2*diff(p.X_t)));

assert(sage_maxfreqres > max(abs(p.maxv), abs(p.minv)),...
       'not enough freq resolution, max resolution is %f', sage_maxfreqres);

% check samples variables size
assert(all(size(p.X) == size(p.Y)) &&...
       all(size(p.X) == size(p.X_f)) &&...
       all(size(p.X) == size(p.X_t)),...
       'X, Y, X_f, and X_t must have the same size');

assert(isvector(p.X),...
       'X must be a vector');
   
% power of tx signal
X_pw = var(p.X);
      
% waves estimates (s)
sage_s = zeros(size(p.Y,1), p.numwavesest);

% waves parameteres (phi)
% struct array with the following fields (initialized to 0):
% t: tau; v: nu; a: alpha
if ~isempty(p.initial_state)
    assert(numel(p.initial_state) == p.numwavesest);
    sage_phi = p.initial_state;
else
    sage_phi = repmat(struct('t', 0, 'v', 0, 'a', 0), p.numwavesest, 1);
end
sage_phi_old = sage_phi;

% To perform the the maximization calculations we first define some
% test points, the maximization function will be executed on these test
% points and then on the maximum the function fminbnd will be used.
% >>> test points for tau
tau_test = p.mintau:p.maxtau;

% >>> test points for v
% in this case we precalculate the complex exponential values needed
% and some other variables

% frequency of the oscillations in the doppler maximization function
% not sure why but it seems to work
freq_oscillation = 1/(max(p.X_t) - min(p.X_t));

v_num_test_points = ceil(4*(p.maxv-p.minv)/freq_oscillation);
v_test = linspace(p.minv, p.maxv, v_num_test_points);
if numel(v_test) > 1
    v_test_diff = v_test(2) - v_test(1);
else
    v_test_diff = (p.maxv-p.minv)/2;
end
v_test_expmat = zeros(numel(p.X), v_num_test_points);
for kk = 1:length(v_test)
    v_test_expmat(:,kk) = exp(-1j*2*pi*v_test(kk)*p.X_t);
end

% toleranche checking functions
switch lower(p.toltau_type)
    case 'absolute', toltau_func = @(p1,p2) mean(abs([p1.t] - [p2.t]));
    case 'relative', toltau_func = @(p1,p2) mean(abs([p1.t] - [p2.t])./abs([p1.t]));
    otherwise, error('invalid toltau_type value');
end

switch lower(p.tolv_type)
    case 'absolute', tolv_func = @(p1,p2) mean(abs([p1.v] - [p2.v]));
    case 'relative', tolv_func = @(p1,p2) mean(abs([p1.v] - [p2.v])./abs([p1.v]));
    otherwise, error('invalid toltau_type value');
end

switch lower(p.tola_type)
    case 'absolute', tola_func = @(p1,p2) mean(abs([p1.a] - [p2.a]));
    case 'relative', tola_func = @(p1,p2) mean(abs([p1.a] - [p2.a])./abs([p1.a]));
    otherwise, error('invalid toltau_type value');
end

% Create sparse matrix to sum same frequency components
X_f_uniq = unique(p.X_f);
sumfmat = zeros(length(p.X_f), length(X_f_uniq));
for ii = 1:length(X_f_uniq)
    sumfmat(:,ii) = p.X_f == X_f_uniq(ii);
end
sumfmat = sparse(sumfmat).';

% Create sparse matrix to sum same time components
X_t_uniq = unique(p.X_t);
sumtmat = zeros(length(p.X_t), length(X_t_uniq));
for ii = 1:length(X_t_uniq)
    sumtmat(:,ii) = p.X_t == X_t_uniq(ii);
end
sumtmat = sparse(sumtmat).';
options = optimset('Display', 'off', 'MaxIter', 7);

%% SAGE main loop ---------------------------------------------------------

gt = tic; % global timer
ii = 0;

while true
    ii = ii + 1;
    for jj = 1:p.numwavesest
        it = tic; % iteration step timer
        
        % Expectation step ------------------------------------------------
        % obtain estimate of the wave jj (x_jj)
        %x_jj = sage_s(:,jj) + p.Y - sum(sage_s, 2);
        if jj == 1
            x_jj = sage_s(:,jj) + p.Y - sum(sage_s, 2);
        else
            x_jj = sage_s(:,jj) + x_jj - sage_s(:,jj-1);
        end
        
        % Maximization step -----------------------------------------------
        % max tau
        xvec = x_jj.*exp(-1j*2*pi*(sage_phi(jj).v)*p.X_t);
        xveccorr = sumfmat*(xvec.*conj(p.X));
        %tauf = @(t) -abs(sum(xvec.*conj(p.X).*exp(1j*2*pi*p.X_f*t)));
        tauf = @(t) -abs(sum(xveccorr.*exp(1j*2*pi*X_f_uniq*t)));

        [~, mini] = min(arrayfun(tauf, tau_test));
        tau = fminbnd(tauf, max(tau_test(mini)-0.5, p.mintau), ...
                            min(tau_test(mini)+0.5, p.maxtau), options);

        % max v
        sage_X_tau = p.X.*exp(-1j*2*pi*p.X_f*tau);
        %xvec = x_jj.*conj(sage_X_tau);
        %vf = @(vv) -abs(sum(xvec.*exp(-1j*2*pi*vv*p.X_t)));
        %vf_test = @(ii) -abs(sum(xvec.*v_test_expmat(:,ii)));
        
        xvec = sumtmat*(x_jj.*conj(sage_X_tau));
        vf = @(vv) -abs(sum(xvec.*exp(-1j*2*pi*vv*X_t_uniq)));
        
        %[~, mini] = min(arrayfun(vf_test, 1:v_num_test_points));
        [~, mini] = min(arrayfun(vf, v_test));
        vv = fminbnd(vf, max(v_test(mini)-v_test_diff, p.minv),...
                         min(v_test(mini)+v_test_diff, p.maxv));

        % complex amplitude
        %a = 1/(numel(sage_X_tau)*X_pw)*sum(...
        %    sum(xvec.*exp(-1j*2*pi*vv*p.X_t)));
        a = 1/(numel(sage_X_tau)*X_pw)*sum(...
            sum(xvec.*exp(-1j*2*pi*vv*X_t_uniq)));
        
        % Iteration ending steps ------------------------------------------
        
        % save estimated parameters
        sage_phi(jj).t = tau;
        sage_phi(jj).v = vv;
        sage_phi(jj).a = a;

        % calculate wave estimate for this iteration
        sage_s(:,jj) = a*sage_X_tau.*exp(1j*2*pi*vv*p.X_t);
        
        time = toc(it);
        if disp_val == disp_iter
            fprintf('SAGE - Iteration %d [%3d] (%5.3f s)\n', ii, jj, time);
        end
    end
    
    % check stop criteria
    if toltau_func(sage_phi, sage_phi_old) < p.toltau && ...
       tola_func(sage_phi, sage_phi_old) < p.tola && ...
       tolv_func(sage_phi, sage_phi_old) < p.tolv
        if disp_val ~= disp_off
            fprintf('Algorithm finished - minimum tolerance reached\n');
        end
        break;
    elseif ii == p.maxiter
        if disp_val ~= disp_off
            fprintf('Algorithm finished - maximum number of iterations reached\n');
        end
        break;
    end
    
    % save this iteration parameters for the next one
    sage_phi_old = sage_phi;
end

time = toc(gt);
if disp_val ~= disp_off
    fprintf('Elapsed time is %f seconds.\n', time);
end

e = sage_phi;
s = sage_s;

end

