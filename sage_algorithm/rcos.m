function y = rcos(t,b,T)
% RCOS raised cosine filter
%
% Returns the impulse response of a raised cosine filter with beta
% parameter b (default 0) and period T (default 1).

if nargin < 2
    b = 0;
end

if nargin < 3
    T = 1;
end

y = sinc(t/T).*cos(pi*b*t/T)./(1-(2*b*t/T).^2);
y(abs(t) == T/(2*b)) = pi/4*sinc(1/(2*b));

end