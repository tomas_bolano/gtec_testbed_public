function pilotos_interp = genie_channel_mimo_est(config, simb_freq_rx, simb_freq)
% Estima e invierte el canal suponiendo que todas las portadoras son
% conocidas.

% load('simb_freq.mat', 'simb_freq');
% for ii = 1:n_symb
%     simb_freq_rx(ii, :) = simb_freq_rx(ii, :)/std(simb_freq_rx(ii, :));
% end
% config = wimax_pusc_config(Nfft, Ntx);
Nfft = config.Nfft;
Ntx = config.Ntx;
L = 60;
n_symb = size(simb_freq_rx, 2);
Nused = sum(squeeze(config.symbol_structure(1, 1, :)) ~= 0);
G = 1/8;
Noff = Nfft;
Nrx = 1;

%%
% LS con la matriz de FFT
% config = wimax_pusc_config(Nfft);

% a = 0:Nfft-1;
% F = 1/sqrt(Nfft)*exp(-1j*2*pi*(a'*a)/Nfft);
% H_freq = simb_freq_rx ./ simb_freq;
% 
% Fp = F(config.ROM_portadoras_par ~= 0, 1:L);
% Fl = F(:, 1:L);
% 
% pilotos_interp = Fl*((Fp'*Fp)\Fp')*H_freq(:, config.ROM_portadoras_par ~= 0).';
% 
% simb_correg = simb_freq_rx ./ pilotos_interp.';

%%
% Inversion en tiempo

rx_t = ifft(fftshift(squeeze(simb_freq_rx(1, :, :)).', 1))*Nfft/sqrt(Nfft);
tx_t = zeros(Ntx, Nfft, n_symb);
for ii = 1:Ntx
    tx_t(ii, :, :) = ifft(fftshift(squeeze(simb_freq(ii,:,:)).', 1))*Nfft/sqrt(Nfft);
end
% tx_t = ifft(fftshift(squeeze(simb_freq(1,:,:)).', 1))*Nfft/sqrt(Nfft);

h_est_ls = zeros(Ntx*L, n_symb);
pilotos_interp = zeros(Ntx, Nrx, n_symb, Nfft);

ii = 1;

if Ntx == 1
    tx_t = squeeze(tx_t(1, :, :));
    while ii <= n_symb
        X = toeplitz(tx_t(:, ii), [tx_t(1, ii); tx_t(end:-1:end-L+2, ii)]);
        rx = rx_t(:, ii);
        h_est_ls(:, ii) = (X'*X)\X'*rx;
        ii = ii+1;
    end
    pilotos_interp(1, 1, :, :) = fftshift(fft([h_est_ls; zeros(1024 - L, n_symb)]), 1).';
elseif Ntx == 2
    % MIMO 2x1: se utilizan dos símbolos consecutivos para estimar
    if Nrx == 1
        tx_t1 = squeeze(tx_t(1, :, :));
        tx_t2 = squeeze(tx_t(2, :, :));
        while ii <= n_symb
            X1 = toeplitz(tx_t1(:, ii), [tx_t1(1, ii); tx_t1(end:-1:end-L+2, ii)]);
            X2 = toeplitz(tx_t2(:, ii), [tx_t2(1, ii); tx_t2(end:-1:end-L+2, ii)]);
            X3 = toeplitz(tx_t1(:, ii+1), [tx_t1(1, ii+1); tx_t1(end:-1:end-L+2, ii+1)]);
            X4 = toeplitz(tx_t2(:, ii+1), [tx_t2(1, ii+1); tx_t2(end:-1:end-L+2, ii+1)]);
            X = [X1 X2; X3 X4];
            rx = [rx_t(:, ii); rx_t(:, ii+1)];
            h_est_ls(:, ii) = (X'*X)\X'*rx;
            h_est_ls(:, ii+1) = h_est_ls(:, ii);

            h_est_ls1 = h_est_ls(1:end/2, ii);
            h_est_ls2 = h_est_ls(end/2+1:end, ii);
            pilotos_interp(1, ii, :) = fftshift(fft([h_est_ls1; zeros(1024 - L, 1)]), 1).';
            pilotos_interp(2, ii, :) = fftshift(fft([h_est_ls2; zeros(1024 - L, 1)]), 1).';
            pilotos_interp(1, ii+1, :) = conj(pilotos_interp(2, ii, :));
            pilotos_interp(2, ii+1, :) = -1*conj(pilotos_interp(1, ii, :));
            ii = ii+2;
        end
    elseif Nrx == 2
    % MIMO 2x2
    end
end

end
