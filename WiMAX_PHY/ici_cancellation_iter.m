
function [simb_correg,pilotos_interp] = ici_cancellation_iter(config, channel_estimator, simb_freq_rx, BEM)

%%
% LS con la matriz de FFT
% config = wimax_pusc_config(Nfft);
Ntx = config.Ntx;
% L = 60;
n_symb = size(simb_freq_rx, 2);
Nused = sum(config.symbol_structure(1, 1, :) ~= 0);
Ndata = sum(config.symbol_structure(1, 1, :) == 2);

simb_freq = pusc_tx(config, zeros(Ntx, n_symb, Ndata));

pilotos_interp = channel_estimator(config, simb_freq_rx);

[simb_freq_rx2, decided_rx] = cancel_ici_MIMO_frame(config, simb_freq_rx, simb_freq, pilotos_interp, BEM,...
    Nused);


pilotos_interp = genie_channel_mimo_est(config, simb_freq_rx2, decided_rx);

simb_correg = ls_channel_mimo_inv(config, simb_freq_rx2, pilotos_interp);

% pilotos_interp = ls_channel_mimo_est(config, simb_freq_rx2);

% simb_freq_rx2 = cancel_ici_MIMO_frame(config, simb_freq_rx, simb_freq, pilotos_interp, BEM,...
%     Nused, G);
 
% simb_correg = ls_channel_mimo_inv(config, simb_freq_rx2, pilotos_interp);

end
