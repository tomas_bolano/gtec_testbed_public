function write_frame(simb_conf,filename)

idata = real(simb_conf);
qdata = imag(simb_conf);

% First, the data is converted to Fix_16_13
if max([abs(idata) abs(qdata)]) >= 4
    disp('Warning!! The written data will be clipped')
end

idata = round(idata * 2^13);
qdata = round(qdata * 2^13);

idata = int16(idata);
qdata = int16(qdata);

data = [idata;qdata];
data = data(:);



% fid = fopen(filename);
% if fid ~= -1
%     disp('Error: the file already exists');
%     fclose(fid);
%     return;
% end

fid = fopen(filename,'w','ieee-le');
count = fwrite(fid, data,'int16');
sprintf('%d IQ symbols were written',count/2)
fclose(fid);
