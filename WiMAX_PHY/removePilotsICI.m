function a = removePilotsICI(H, simb_freq_rx_used, simb_freq_used)

[xx yy] = meshgrid(1:size(H,1), 1:size(H,2));
H_ici = H;
H_ici(xx == yy) = 0;

a = simb_freq_rx_used-H_ici*simb_freq_used;

end
