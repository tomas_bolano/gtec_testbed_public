
function [datos_rx, pil_inv,evm_subcarrier] = pusc_rx_nodeintr(simb_freq_rx, Nfft, ch_estimation_algorithm)
%%
% Estimates channel, equalizes, and extracts the data subcarriers from a
% WiMAX PUSC zone.
% Input arguments:
%   simb_freq_rx: A Nsymb x Nfft matrix with the received subcarriers in
%                 the frequency domain.
%   Nfft: The FFT window size of the profile.
%   ch_estimation_algorithm: Channel estimation algorithm to be used.
%                 Possible values of this parameter can be:
%                   0 -> LS estimation on the pilot positions with linear
%                 interpolation on the data subcarriers.
%                   1 -> Perfect LS estimation assuming the transmitted
%                 signal is known, including data subcarriers. The
%                 transmitted signal is read from the 'simb_freq.mat' file.
%                   2 -> Perfect LS and ICI estimation assuming the
%                 trasmitted signal is known, including data subcarriers.
%                 The transmitted signal is read from the 'simb_freq.mat'
%                 file.
%                 If this parameter is not present, it takes by default 
%                 value 0.
% Output arguments:
%   datos_rx: A Nsymb x Nfft matrix with the equalized data subcarriers.
%   pil_inv: Optional output argument where the channel frequency response
%            will be stored if it is present, in a Nsymb x Nfft matrix.
%   evm_subcarrier: Optional output argument where an estimation of the EVM
%            per subcarrier will be stored if it is present, in a Nused x
%            Nfft matrix.

% narginchk(2,3);
% nargoutchk(1,3);

if nargin < 3
    ch_estimation_algorithm = 0;
end

config = wimax_pusc_config(Nfft);
num_simb_trama = size(simb_freq_rx, 1);

if ch_estimation_algorithm == 1
    [simb_correg, pil_inv] = genie_channel_inv(simb_freq_rx, Nfft);

    
    set(config.randomizer, 'NumBitsOut', Nfft);
    R = (ones(num_simb_trama, 1)*(1-2*generate(config.randomizer)'));
    simb_correg = simb_correg .* R;
elseif ch_estimation_algorithm == 2
    [simb_correg, pil_inv] = genie_channel_ici_inv(simb_freq_rx, Nfft);

    set(config.randomizer, 'NumBitsOut', Nfft);
    R = (ones(num_simb_trama, 1)*(1-2*generate(config.randomizer)'));
    simb_correg = simb_correg .* R;
elseif ch_estimation_algorithm == 3
    set(config.randomizer, 'NumBitsOut', Nfft);
    simb_freq_rx = simb_freq_rx .* (ones(num_simb_trama, 1)*(1-2*generate(config.randomizer)'));

    [simb_correg, pil_inv] = pusc_channel_inv(simb_freq_rx, Nfft);
elseif ch_estimation_algorithm == 4
    [simb_correg, pil_inv] = ls_channel_inv(simb_freq_rx, Nfft);

    set(config.randomizer, 'NumBitsOut', Nfft);
    R = (ones(num_simb_trama, 1)*(1-2*generate(config.randomizer)'));
    simb_correg = simb_correg .* R;
elseif ch_estimation_algorithm == 5
    [simb_correg, pil_inv] = ls_channel_ici_inv(simb_freq_rx, Nfft);

    set(config.randomizer, 'NumBitsOut', Nfft);
    R = (ones(num_simb_trama, 1)*(1-2*generate(config.randomizer)'));
    simb_correg = simb_correg .* R;
end

% Se devuelven los coeficientes de canal estimados, solo para las
% posiciones de portadoras útiles (pilotos y datos).
if nargout > 1
    pil_inv = pil_inv(:,(1+config.guard_i):(Nfft-config.guard_d));
end

datos_rx = zeros(num_simb_trama, config.num_clusters*12);
datos_rx(1:2:end,:) = simb_correg(1:2:end,config.ROM_portadoras_par==2);
datos_rx(2:2:end,:) = simb_correg(2:2:end,config.ROM_portadoras_impar==2);


% Cáculo de la EVM por subportadora. Este cálculo se hace suponiendo que en
% la trama hay 6 ráfagas de 4 símbolos cada una, 2 con QPSK, 2 con
% 16-QAM y 2 con 64-QAM.
if nargout > 2
    evm_subcarrier = zeros(3, size(datos_rx, 2));
    for ii = 1:size(datos_rx, 2)
        evm_subcarrier(1, ii) = evm(datos_rx(1:8, ii), 4);
        evm_subcarrier(2, ii) = evm(datos_rx(9:16, ii), 16);
        evm_subcarrier(3, ii) = evm(datos_rx(17:24, ii), 64);
    end
    evm_subcarrier = 10*log10(mean(10.^(evm_subcarrier./10)));
end

% datos_rx = deintrlv(datos_rx.', config.ROM_entrelazado_portadoras);
% datos_rx = datos_rx.';

end
