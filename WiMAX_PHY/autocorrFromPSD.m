function autocorr = autocorrFromPSD(PSDname, fd, tau, FreqMinMaxJakes)
% autocorr = autocorrFromPSD(PSDname, fd, tau, FreqMinMaxJakes)
% PSDname: 'jakesR' or 'jakesA' for restricted Jakes or asymmetric Jakes PSD.
% fd: Maximum doppler shift in Hertzs.
% tau: vector with the time instants to sample the autocorrelation. Example
%   of use: tau = (0:fs*taumax-1)/fs. Samples from zero to taumax-1/fs
%   seconds.
% FreqMinMaxJakes: The same parameter as in doppler.ajakes and
%   doppler.rjakes, in 'jakesA' and 'jakesR' cases.
%   Example: FreqMinMaxJakes = [0.1 0.5]. 

autocorr = zeros(size(tau));

if strcmp(PSDname,'jakesR')
    fmin = FreqMinMaxJakes(1)*fd;
    fmax = FreqMinMaxJakes(2)*fd;
    
    Arj = (pi/2)/(asin(fmax/fd)-asin(fmin/fd));

    for n=1:length(tau)
        autocorr(n) = quad(@(phi)cosJakes(phi,tau(n),fd),asin(fmin/fd),asin(fmax/fd));
    end
    autocorr = autocorr*Arj*2/pi;
elseif strcmp(PSDname,'jakesA')
    fmin = FreqMinMaxJakes(1)*fd;
    fmax = FreqMinMaxJakes(2)*fd;
    
    Aaj = pi/(asin(fmax/fd)-asin(fmin/fd));

    for n=1:length(tau)
        autocorr(n) = quad(@(phi)cosJakes(phi,tau(n),fd),asin(fmin/fd),asin(fmax/fd)) - 1j*quad(@(phi)sinJakes(phi,tau(n),fd),acos(fmin/fd),acos(fmax/fd));
    end
    autocorr = autocorr*Aaj/pi;
elseif strcmp(PSDname, 'flat')
    autocorr = sin(2*pi*fd*tau)./(2*pi*fd*tau);
    autocorr(isnan(autocorr)) = 1;
end


