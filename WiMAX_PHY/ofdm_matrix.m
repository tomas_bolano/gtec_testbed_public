function [F1, F2] = ofdm_matrix(N, Nused, G)
%     Construye matrices de IFFT y FFT para la generación de símbolos OFDM.
%     La matriz F1 tendrá dimensiones NusedxN*(1+G), y la matriz F2 tendrá
%     dimensiones N*(1+G)xNused
%     N: Tamaño de la FFT
%     Nused: Número de portadoras útiles dentro del símbolo OFDM, considerando
%     pilotos y datos. En las matrices generadas se situarán estas portadoras a
%     ambos lados de una portadora de guarda en la frecuencia cero.
%     G: Tamaño del prefijo cíclico como una fracción del tamaño de FFT.

% Cálculo de número de portadoras de guarda a la derecha y a la izquierda
% de las portadoras útiles.
guard_i = ceil((N-Nused-1)/2);
guard_d = floor((N-Nused-1)/2);

% Matriz para insertar las portadoras de guarda.
F_guard = zeros(N, Nused);
% F_guard((guard_i+1):(N/2), 1:Nused/2) = eye(Nused/2);
% F_guard((N/2+2):(guard_i+Nused+1), (Nused/2+1):Nused) = eye(Nused/2);
F_guard(2:Nused/2+1, (Nused/2+1):Nused) = eye(Nused/2);
F_guard(1+guard_i+guard_d+((Nused/2+1):Nused), 1:Nused/2) = eye(Nused/2);

% Matriz para insertar el prefijo cíclico.
F_cp = zeros(N*(1+G), N);
F_cp(1:N*G, end-N*G+1:end) = eye(N*G);
F_cp(N*G+1:end, :) = eye(N);
% Matriz para eliminar el prefijo cíclico.
F_cprem = zeros(N, N*(1+G));
F_cprem(:, (N*G+1):end) = eye(N);
% Matríz para eliminar las portadoras de guarda.
% F_guardrem = zeros(Nused, N);
% F_guardrem(1:Nused/2, 1+guard_i+guard_d+((Nused/2+1):Nused)) = eye(Nused/2);
% F_guardrem((Nused/2+1):Nused, 2:Nused/2+1) = eye(Nused/2);

F_guardrem2 = zeros(N, N);
F_guardrem2(1:N/2, ((N/2+1):N)) = eye(N/2);
F_guardrem2((N/2+1):N, 1:N/2) = eye(N/2);
% Matriz de la FFT
a = 0:N-1;
F = 1/sqrt(N)*exp(-1j*2*pi*(a'*a)/N);
% F1 = F_guardrem*F*F_cprem;
F1 = F_guardrem2*F*F_cprem;
F2 = F_cp*F'*F_guard;
end