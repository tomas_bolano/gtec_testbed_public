clear;
close all;


N = 1024;
fs = 8e6;
up_factor = 8;
M = 24;

pilots_norm = 2*randi(2, N, M)-3;
% pilots_norm = reshape(1:N*M, N, M);

pilots_trick = upsample(pilots_norm, up_factor);

for ii = 1:length(pilots_norm)
    pilots_shifted = circshift(pilots_norm, up_factor/2 - ii + 1);
    pilots_window = pilots_shifted(1:up_factor+1, :);
    if ii == 1
        indices = mod((up_factor*(ii-3/2):up_factor*(ii-1/2)), N*up_factor)+1;
    else
        indices = 1+(up_factor*(ii-3/2):up_factor*(ii-1/2));
    end
    pilots_trick(indices, :) = pilots_window;
end

ts = 1/fs;
fd = 5;
chan_slow = stdchan(ts, fd, 'itur3GPBx');
fd = 5*up_factor;
chan_fast = stdchan(ts, fd, 'itur3GPBx');

chan_fast.StorePathGains = 1;
chan_slow.StorePathGains = 1;


pilots_trick_ofdm = simple_ofdm_tx(pilots_trick);
pilots_norm_ofdm = simple_ofdm_tx(pilots_norm);

pilots_trick_rx = filter(chan_slow, pilots_trick_ofdm(:));
pilots_norm_rx = filter(chan_fast, pilots_norm_ofdm(:));

pilots_trick_rx = simple_ofdm_rx(reshape(pilots_trick_rx, N*up_factor*(9/8), M));
pilots_norm_rx = simple_ofdm_rx(reshape(pilots_norm_rx, N*(9/8), M));

pilots_detrick_rx = downsample(pilots_trick_rx, 8);

figure, plot(real(pilots_detrick_rx./pilots_norm));

figure, plot(real(pilots_norm_rx./pilots_norm));

% a_test=ones(1, 100000);
% pilots_trick_rx = filter(chan_slow, a_test);
% pilots_norm_rx = filter(chan_fast, a_test);

