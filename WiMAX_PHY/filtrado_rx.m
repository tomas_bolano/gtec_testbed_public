function simb_diez = filtrado_rx(simb_conf,perfil)

%% Diezmado


%fvtool(fir8/8,1,firpm(128,[0 2*420/8192 2*604/8192 1],[1 1 0 0]),1)
%fir8 = firpm(128,[0 2*430/8192 2*600/8192 1],[1 1 0 0]);

%fir7 = rcosine(1, 7, 'sqrt/fir', 0.3,6)/sqrt(7)*7;
%fir51d = rcosine(1, 5, 'sqrt/fir', 0.1,8)/sqrt(5);
%fir52d = rcosine(1, 6, 'sqrt/fir', 0.2,7)/sqrt(6);
%fir4d = rcosine(1, 4, 'sqrt/fir', 0.1,8)/sqrt(4);
%fir2d = rcosine(1, 4, 'sqrt/fir', 0.5,6)/sqrt(4);

fir7 = firnyquist(50,7,0.3)*7;
fir51d = firnyquist(80,5,0.15);
%fir51d = rcosine(1, 5, 'sqrt/fir', 0.15,16)/sqrt(5);
fir52d = firnyquist(36,5,0.4);

fir4d = firnyquist(68,4,0.15);
fir2d = firnyquist(32,2,0.2);  % antes eran 20 coefs

% d = fdesign.halfband('Type','Lowpass',.1, 40);
% Hfir = design(d,'equiripple');
% fir_halfd = Hfir.Numerator;




simb_diez = simb_conf*sqrt(2);
switch perfil
    case 0
%         simb_diez = downsample(conv(fir4d,simb_diez),4);
         simb_diez = downsample(conv(fir51d,simb_diez),5);
          %simb_diez = downsample(conv(fir2d,simb_diez),2);
    case 1
        simb_diez = downsample(conv(fir52d,simb_diez),5);
        simb_diez = conv(fir7,upsample(simb_diez,7));
        simb_diez = downsample(conv(fir4d,simb_diez),4);
        simb_diez = downsample(conv(fir51d,simb_diez),5);
    case 2
        simb_diez = downsample(conv(fir2d,simb_diez),2);
        simb_diez = downsample(conv(fir51d,simb_diez),5);
    case 3
        simb_diez = downsample(conv(fir2d,simb_diez),2);
        simb_diez = downsample(conv(fir4d,simb_diez),4);
%         simb_diez = downsample(conv(fir_halfd,simb_diez),2);
%         simb_diez = downsample(conv(fir_halfd,simb_diez),2);
%         simb_diez = downsample(conv(fir_halfd,simb_diez),2);
%         
        
    case 4
%        simb_diez = src_farrow(simb_diez, 7/5, 3);
        simb_diez = downsample(conv(fir52d,simb_diez),5);
        simb_diez = conv(fir7,upsample(simb_diez,7));
        %simb_diez = upsample(simb_diez,7);
        %simb_diez = repmat(simb_diez,7,1);
        %simb_diez = simb_diez(:).';
        simb_diez = downsample(conv(fir2d,simb_diez),2);
        simb_diez = downsample(conv(fir51d,simb_diez),5);
end
