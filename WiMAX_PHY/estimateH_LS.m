function [D] = estimateH_LS(H_est, B, N, F1, F2, Noff, symbol_indices, totalG)
%     Construye la matriz del canal a partir de una BEM.
%     Utiliza las estimaciones de Nsymb símbolos OFDM con K portadoras útiles
%     cada uno. Se hace uso de una BEM de orden Q.
% 
%     H_est: Matriz KxNsymb con las estimaciones de simbolos individuales.
%     B: Matriz QxNsymb*K con los vectores de la BEM.
%     N: Tamaño total de los símbolos OFDM, considerando portadoras útiles,
%     guardas, y prefijo cíclico.
%     F1: Matriz de FFT, eliminación de prefijo cíclico y eliminación de
%     guardas.
%     F2: Matriz de IFFT, inserción de prefijo cíclico e inserción de guardas.
%     Noff: Número de diagonales no principales que se considerarán para
%     generar la matriz del canal.

% m = N/2+(0:N:2*N).';
% M = [m.^0 m.^1 m.^2];

Nsymb = size(B, 2)/N;
M = B(:, N/2+(0:N:(Nsymb-1)*N)).';
H_ici = pinv(M)*H_est.';
% t = N*(G+1)+(1:N*(G+1));

if nargin < 7
    symbol_indices = 1:Nsymb;
end

D = zeros(Nsymb, size(F1, 1), size(F1, 1));
[xx yy] = meshgrid(1:size(F1, 1), 1:size(F1, 1));

if nargin < 8
    if exist('ici_estimator_data.mat', 'file')
        load('ici_estimator_data.mat', 'totalG');
    else
        totalG = zeros(Nsymb, size(B, 1), size(F1, 1), size(F1, 1));
        for ii = 1:Nsymb
            Bsymb = B(:, (N*(ii-1)+1):N*ii);
            for jj = 1:size(B, 1)
                totalG(ii, jj, :, :) = F1*diag(Bsymb(jj,:))*F2;
            end
        end
        save('ici_estimator_data.mat', 'totalG');
    end
end

for ii = symbol_indices
%     Bsymb = B(:, (N*(ii-1)+1):N*ii);
    for jj = 1:size(B, 1)
%         G = F1*diag(Bsymb(jj,:))*F2;
        G = squeeze(totalG(ii, jj, :, :));
        G(abs(xx-yy)>Noff) = 0;
        D(ii,:,:) = squeeze(D(ii,:,:)) + diag(H_ici(jj,:))*G;
    end
end
% 
% D = diag(H_ici(1,:))*F1*diag(t.^0)*F2
%    +diag(H_ici(2,:))*F1*diag(t.^1)*F2
%    +diag(H_ici(3,:))*F1*diag(t.^2)*F2;

