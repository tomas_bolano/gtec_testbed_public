function [mapped_antennas] = slot_mapping(subcarriers, num_simb_rafaga, Ntx, Nrx)

if nargin < 3
    Ntx = 1;
end

if nargin < 4
    Nrx = 1;
end

num_slots_total = length(subcarriers) / 48;
num_slots_symbol = (num_slots_total / (num_simb_rafaga/2))/Nrx;

mapped_antennas = zeros(Ntx, num_simb_rafaga, num_slots_symbol*24);

if Ntx == 1
    idx = 1;
    for ii = 1:2:num_simb_rafaga
        for jj = 1:num_slots_symbol
            a_slot = subcarriers(idx:idx+47);
            mapped_antennas(1, ii:ii+1, (jj-1)*24+1:(jj-1)*24+24) = reshape(a_slot, 2, 24);
            idx = idx+48;
        end
    end
elseif Ntx == 2
    if Nrx == 1
        idx = 1;
        for ii = 1:2:num_simb_rafaga
            for jj = 1:num_slots_symbol
                a_slot = subcarriers(idx:idx+47);
                a_slot_reshaped = reshape(a_slot.', 24, 2);
                mapped_antennas(1, ii, (jj-1)*24+1:(jj-1)*24+24) = a_slot_reshaped(:, 1);
                mapped_antennas(2, ii, (jj-1)*24+1:(jj-1)*24+24) = a_slot_reshaped(:, 2);
                mapped_antennas(1, ii+1, (jj-1)*24+1:(jj-1)*24+24) = -1*conj(a_slot_reshaped(:, 2));
                mapped_antennas(2, ii+1, (jj-1)*24+1:(jj-1)*24+24) = conj(a_slot_reshaped(:, 1));
                idx = idx+48;
            end
        end
%         mapped_antennas = mapped_antennas / sqrt(2);
    elseif Nrx == 2
        idx = 1;
        for ii = 1:2:num_simb_rafaga
            for jj = 1:num_slots_symbol
                a_slot = subcarriers(idx:idx+47);
                a_slot_reshaped = reshape(a_slot, 2, 24);
                mapped_antennas(1, ii, (jj-1)*24+1:(jj-1)*24+24) = a_slot_reshaped(1, :);
                mapped_antennas(2, ii, (jj-1)*24+1:(jj-1)*24+24) = a_slot_reshaped(2, :);
                a_slot = subcarriers(idx+48:idx+95);
                a_slot_reshaped = reshape(a_slot, 2, 24);
                mapped_antennas(1, ii+1, (jj-1)*24+1:(jj-1)*24+24) = a_slot_reshaped(1, :);
                mapped_antennas(2, ii+1, (jj-1)*24+1:(jj-1)*24+24) = a_slot_reshaped(2, :);
                idx = idx+96;
            end
        end
    end
end

end
