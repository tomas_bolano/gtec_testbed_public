function rx = simple_ofdm_rx(tx)

Nt = size(tx, 1);
G = 1/8;
Ng = Nt*(G/(1+G));

rx = tx(Ng+1:end, :);

rx = fft(rx);

end
