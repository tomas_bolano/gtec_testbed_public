function [symb_rx] = slot_demapping(mapped_antennas, num_simb_rafaga, Ntx, Nrx)

if nargin < 3
    Ntx = 1;
end

if nargin < 4
    Nrx = 1;
end

symb_rx = zeros(length(mapped_antennas(:)), 1);
num_slots_symbol = size(mapped_antennas, 3)/24;

if Ntx == 1
    idx = 1;
    for ii = 1:2:num_simb_rafaga
        for jj = 1:num_slots_symbol
            a_slot = mapped_antennas(1, ii:ii+1, (jj-1)*24+1:(jj-1)*24+24);
            symb_rx(idx:idx+47) = a_slot(:);
            idx = idx+48;
        end
    end
elseif Ntx == 2
    if Nrx == 1
        idx = 1;
        for ii = 1:2:num_simb_rafaga
            for jj = 1:num_slots_symbol
                a_slot = squeeze(mapped_antennas(1, ii:ii+1, (jj-1)*24+1:(jj-1)*24+24)).';
                symb_rx(idx:idx+47) = a_slot(:);
                idx = idx+48;
            end
        end
    elseif Nrx == 2
        idx = 1;
        for ii = 1:2:num_simb_rafaga
            for jj = 1:num_slots_symbol
                a_slot = [mapped_antennas(1, ii, (jj-1)*24+1:(jj-1)*24+24);...
                    mapped_antennas(2, ii, (jj-1)*24+1:(jj-1)*24+24)];
                symb_rx(idx:idx+47) = a_slot(:);
                a_slot = [mapped_antennas(1, ii+1, (jj-1)*24+1:(jj-1)*24+24);...
                    mapped_antennas(2, ii+1, (jj-1)*24+1:(jj-1)*24+24)];
                symb_rx(idx+48:idx+95) = a_slot(:);
                idx = idx+96;
            end
        end
    end
end

end
