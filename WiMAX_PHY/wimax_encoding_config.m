function coding_configuration = wimax_encoding_config(M, code_rate)
% returns the configuration of the convolutional coder in the IEEE 802.16
% standard. It receives two parameters indicating the desired modulation
% level and code rate. The expected values of M are 4, 16 and 64, and the
% values for code_rate are 0 for 1/2, 1 for 2/3 and 2 for 3/4.
%
% The fields of the returned struct are:
%
%   constraint_length: Constraint length of the encoder.
%   punct_pattern: Puncturing pattern to apply depending on the code rate.
%   trellis: Configuration for the convolutional encoder.
%   code_rate_value: Fractional representation of the code rate. For
%       code_rate 0 this field contains 0.5 and so on.
%   max_num_slots: Maximum number of slots allowed in each codified block
%       for the selected modulation level an code rate.
%   scale_factor: Factor to scale the constellations after modulation and
%       before decision.
%   randomizer: Object of the PN class of Matlab to generate the randomizer
%       sequences.
%   modulator: Object of the QAMMOD class of Matlab to map the bits to
%       symbols according to the selected modulation level.

coding_configuration = {};

coding_configuration.M = M;
coding_configuration.code_rate = code_rate;

coding_configuration.constraint_length = 7;

if (code_rate == 0)
    coding_configuration.punct_pattern = [1 1];
    coding_configuration.code_rate_value = 1/2;
elseif (code_rate == 1)
    coding_configuration.punct_pattern = [1 1 0 1];
    coding_configuration.code_rate_value = 2/3;
elseif (code_rate == 2)
    coding_configuration.punct_pattern = [1 1 0 1 1 0];
    coding_configuration.code_rate_value = 3/4;
end

switch M
    case 4
        if code_rate == 0
            coding_configuration.max_num_slots = 6;
        elseif code_rate==2
            coding_configuration.max_num_slots = 4;
        end
        coding_configuration.scale_factor = 1/sqrt(2);
    case 16
        if code_rate == 0
            coding_configuration.max_num_slots = 3;
        elseif code_rate==2
           coding_configuration. max_num_slots = 2;
        end
        coding_configuration.scale_factor = 1/sqrt(10);
    case 64
        if code_rate == 0
            coding_configuration.max_num_slots = 2;
        elseif code_rate==1
            coding_configuration.max_num_slots = 1;
        elseif code_rate==2
            coding_configuration.max_num_slots = 1;
        end
        coding_configuration.scale_factor = 1/sqrt(42);
end

coding_configuration.trellis = poly2trellis(coding_configuration.constraint_length, [171 133]);

% coding_configuration.randomizer = seqgen.pn('GenPoly', [1 zeros(1,13) 1 1],...
%     'InitialStates', [0 1 1 0 1 1 1 0 0 0 1 0 1 0 1]);

% coding_configuration.randomizer = comm.PNSequence('Polynomial', [1 zeros(1,13) 1 1],...
%                          'InitialConditions', [0 1 1 0 1 1 1 0 0 0 1 0 1 0 1]);

coding_configuration.randomizer = commsrc.pn('GenPoly', [1 zeros(1,13) 1 1],...
    'InitialStates', [0 1 1 0 1 1 1 0 0 0 1 0 1 0 1]);

coding_configuration.modulator = modem.qammod('M', M,...
    'SymbolOrder', 'gray',...
    'InputType', 'bit');
