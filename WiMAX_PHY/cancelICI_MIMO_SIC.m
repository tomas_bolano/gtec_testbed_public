function [x_noici, decided_x] = cancelICI_MIMO_SIC(simb_freq_rx_used, simb_freq_used, H, Bf, m_demod, scale)

N = size(H, 3);
Nfft = size(Bf, 2);
Ntx = size(H, 1);
Nrx = size(H, 2);
Q = size(H, 4);

s = simb_freq_used.';
s = s(:);
x = simb_freq_rx_used.';
x = x(:);


% Hf = zeros(Q, Nrx*N, Ntx*N);
Hf = zeros(Q, Nrx*Nfft, Ntx*N);
for ii = 1:Ntx
    for jj = 1:Nrx
        for kk = 1:Q
            Bq = squeeze(Bf(kk, :, :));
            Hf(kk, (jj-1)*Nfft+1:jj*Nfft, (ii-1)*N+1:ii*N) = ...
                bsxfun(@times, Bq, squeeze(H(ii, jj, :, kk)).');
%                 Bq*diag(squeeze(H(ii, jj, :, kk)));

        end
    end
end
Hf = squeeze(sum(Hf, 1));


% [xx yy] = meshgrid(1:size(Hf,2), 1:size(Hf,1));
H_ici = Hf;
diagonals = zeros(Nfft, N);
guards = (Nfft - N)/2;
diagonals(guards+(1:N/2), 1:N/2) = eye(N/2);
diagonals(guards+N/2+1+(1:N/2), N/2+1:end) = eye(N/2);
diagonals = repmat(diagonals == 1, Nrx, Ntx);
H_ici(diagonals) = 0;

% for ii = 1:Ntx
%     for jj = 1:Nrx
%         H_ici(xx+(ii-1)*N == yy+(jj-1)*N) = 0;
%     end
% end
y = x - H_ici*s;
% Hf(:, abs(s) ~= 0) = 0;

m_mod = modem.qammod(m_demod);
x_ls = zeros(size(x));
decided_x = s;
[idx_input, idx_output] = find(diagonals == 1);
num_indices = length(idx_input);
idx_input = reshape(idx_input, num_indices/Nrx, Nrx);
idx_output = reshape(idx_output, num_indices/Ntx, Ntx);

idx_cols = downsample(idx_output, Ntx).';
idx_rows = reshape(idx_input(:, 1), Nrx, N);

% Calcular indices de las submatrices a lo largo de la diagonal principal,
% para aprovechar la posible ganancia por diversidad que ofrece la ICI.
Noff = 0;
idx_cols_ext = zeros(Ntx*(2*Noff+1), N);
idx_rows_ext = zeros(Nrx*(2*Noff+1), N);

idx = 1:2*Noff+1;
for jj = 1:N
    idx_cols_ext(idx, jj) = idx_cols(:, jj)-Noff:idx_cols(:,jj)+Noff;
    if jj < Noff
        idx_cols_ext(1:(Noff-jj), jj) = 0;
    elseif jj > N/2 - Noff && jj <= N/2
        idx_cols_ext(2*Noff+1, jj) = 0;
    elseif jj > N/2 && jj <= N/2 + Noff
        idx_cols_ext(1, jj) = 0;
    elseif jj + Noff > N
        idx_cols_ext(Noff-(jj-N)+2:end, jj) = 0;
    end

    idx_rows_ext(idx, jj) = idx_rows(:, jj)-Noff:idx_rows(:,jj)+Noff;
end

% Extension de los indices para todas las antenas
for ii = 1:Ntx-1
    idx_rows_ext(ii*(2*Noff+1)+1:(ii+1)*(2*Noff+1),:) = ii*Nfft + idx_rows_ext(1:(2*Noff+1),:);
    tmp = ii*N + idx_cols_ext(1:(2*Noff+1),:);
    tmp(idx_cols_ext(1:(2*Noff+1),:) == 0) = 0;
    idx_cols_ext(ii*(2*Noff+1)+1:(ii+1)*(2*Noff+1),:) = tmp;
end

pilots_index = find(abs(sum(reshape(s, length(s)/Ntx, Ntx).')) ~= 0);
data_index = find(abs(s) == 0).';
antenna_cols = 0:N:N*(Ntx-1);
for ii = pilots_index
    Hf(H_ici(:, ii) ~= 0, ii+antenna_cols) = 0;
end
% idx_1 = repmat([zeros(guards, 1); 1; zeros(Nfft-guards-1, 1)], Ntx, 1) == 1;
% idx_2 = repmat([1; zeros(N-1, 1)], Ntx, 1) == 1;
for ii = data_index(1:length(data_index)/Nrx)
    row = idx_rows(:, ii);
    col = idx_cols(:, ii);
    row_ext = idx_rows_ext(:, ii);
    col_ext = idx_cols_ext(idx_cols_ext(:, ii) > 0, ii);
%         H_carrier = Hf(row, col);
%         y_carrier = y(row);
    H_carrier = Hf(row_ext, col_ext);
    y_carrier = y(row_ext);
    x_eq = H_carrier \ y_carrier;
    x_eq_resh = reshape(x_eq, length(x_eq)/Nrx, Nrx);
    if ii <= Noff
        x_ls(row) = x_eq_resh(ii, :);
    elseif ii > N/2 && ii <= N/2+Noff
        x_ls(row) = x_eq_resh(Noff, :);
    else
        x_ls(row) = x_eq_resh(Noff+1, :);
    end

    bits = demodulate(m_demod, x_ls(row) / scale);
    decided_x(col) = scale*modulate(m_mod, bits);

    Hcol = H_ici(:, col);
    y = y-(Hcol*decided_x(col));
    Hf(H_ici(:, col(1)) ~= 0, col) = 0;
end

decided_x = reshape(decided_x, N, Nrx).';
x_noici = reshape(y, Nfft, Nrx).';

end