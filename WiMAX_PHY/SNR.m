function snr_dB = SNR(simb_env,simb_rec)
% snr_dB = SNR(simb_env,simb_rec)
% Devuelve la SNR de la constelación recibida en dB comparándola con la
% enviada.

ruido = (simb_rec - simb_env);
valor_cuadratico_ruido = ruido'*ruido/length(ruido);
valor_cuadratico_orig = (simb_env)'*(simb_env)/length(simb_env);
snr_dB = 10 * log10(valor_cuadratico_orig/valor_cuadratico_ruido);



