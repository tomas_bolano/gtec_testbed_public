function bursts = burst_example(config)

s = rng;
rng(config.Seed);

if config.Ntx == 2 && config.Nrx == 2
    STC_rate = 2;
else
    STC_rate = 1;
end

num_clusters = 60;  % para 1024 portadoras
frame_burst_size = [4 4 4 4 4 4];
% frame_burst_size = [4];
frame_M = [4 4 16 16 64 64];
frame_code_rate = [0 2 0 2 0 2];
frame_code_rate_value = [1/2 3/4 1/2 3/4 1/2 3/4];
frame_symbol_offset = 1:4:24;
frame_subchannel_offset = ones(1, 6);
frame_n_symbols = frame_burst_size;
frame_n_subchannels = num_clusters/2*ones(1, 6);

% bursts = cell(1, length(frame_burst_size));
% Configuraci�n de las r�fagas
for n = 1:length(frame_burst_size)
    bursts(n).code_rate = frame_code_rate(n);
    bursts(n).M = frame_M(n);
    bursts(n).symbol_offset = frame_symbol_offset(n);
    bursts(n).subchannel_offset = frame_subchannel_offset(n);
    bursts(n).n_symbols = frame_n_symbols(n);
    bursts(n).n_subchannels = frame_n_subchannels(n);
    num_unc_bits = 48*bursts(n).n_subchannels*(bursts(n).n_symbols/2)*STC_rate*...
        log2(frame_M(n))*frame_code_rate_value(n); % bits para 2 símbolos
    bursts(n).bits = randi(2, 1, num_unc_bits)-1;
end

rng(s);