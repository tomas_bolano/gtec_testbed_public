% function bursts_rec = PHY_rx(perfil,G,num_simb_trama,bursts_sent, simb_conf,est_algorithm)
% Decodifica una trama OFDMA generada con la funci�n PHY_tx.
% Par�metros de entrada:
%   perfil, G, numb_simb_trama y bursts_sent: son los mismos par�metros que
%       los usados para llamar a PHY_tx.
%   simb_conf: El vector fila con los s�mbolos complejos recibidos.
%   est_algorithm: Algorithmo de estimación de canal a utilizar.
%
% La funci�n devuelve un array de estructuras con los mismos campos que
% bursts_sent pero en el campo bits se ponen los bits recibidos. Adem�s se
% a�aden los siguientes campos para cada r�faga:
%   EVM: Error Vector Magnitude de los s�mbolos recibidos (en dB) de la r�faga.
%   symbols: los s�mbolos complejos de la r�faga
%   BER: tasa de error de bit de la r�faga
function [bursts_rec, time_offset, u] = PHY_rx_nodec(config, bursts_sent, simb_conf,est_algorithm)

nargoutchk(1, 3);

bursts_rec = [];

Nfft = config.Nfft;
Ntx = config.Ntx;
Nrx = config.Nrx;
G = config.GuardInterval;
num_simb_trama = config.NumberOfSymbols;

desv_sincr = 0;


tam_pref = G*Nfft;

%% Filtrado Rx

% eliminamos el offset DC
% simb_conf = simb_conf - mean(simb_conf);
% simb_conf = simb_conf - filter(2^(-8),[1 -(1-2^(-8))],simb_conf);
% simb_diez = sig_in;

sig_in = simb_conf;
simb_diez = sig_in;

%% Sincronizaci�n
% Tener 1 de las 2 lineas de abajo (2 metodos distintos de sincr).
[time_offset, freq_offset] = synchronization(simb_diez(1, :),Nfft,G);
%simb_diez = simb_diez .*exp(1j*2*pi*[0:(length(simb_diez)-1)]*(freq_offset));


time_offset=time_offset+desv_sincr;   % Para modificar el pto. de sincronizaci�n
if (time_offset<1) || (time_offset+(num_simb_trama+1)*Nfft*(1+G)-1)>length(simb_diez)
    error_sinc = 1;
    snr_rx = -1;
    evm_rx = -1;
	return;
else
    error_sinc = 0; 
end

simb_sinc = simb_diez(:, time_offset:(time_offset+(num_simb_trama+1)*Nfft*(1+G)-1));
simb_sinc = freq_synchronization(simb_sinc,Nfft,G);
simb_sinc2 = zeros(Nrx, num_simb_trama+1, Nfft*(1+G));
for ii = 1:Nrx
    simb_sinc2(ii, :, :) = reshape(simb_sinc(ii, :),Nfft*(1+G),num_simb_trama+1).';
end
simb_sinc = simb_sinc2;
simb_sinc = simb_sinc(:, :, (tam_pref+1):end);

simb_freq_rx = fft(simb_sinc,[],3)/sqrt(Nfft);
simb_freq_rx = fftshift(simb_freq_rx,3);

%% Eliminar preambulo
simb_freq_rx = simb_freq_rx(:, 2:end,:);

%% Inversi�n del canal
%
% Llamada a la función de extracción de portadoras de datos para obtener la
% EVM por portadora.
%
% [datos_rx,~,evm_subcarrier] = pusc_rx(simb_freq_rx, Nfft);
% figure,
% plot(evm_subcarrier);

% datos_rx = pusc_rx_nodeintr(simb_freq_rx, Nfft, est_algorithm);
[datos_rx, pil_inv] = pusc_rx(config, simb_freq_rx, est_algorithm);

if Ntx == 2 && Nrx == 2
    asdf = squeeze(pil_inv(:, :, :, 300));
    for rr = 1:24
        u(:, rr) = svd(squeeze(asdf(:, :, rr)));
    end
    u = mean(u.');
else
    for rr = 1:2:24
        H1 = squeeze(pil_inv(1, 1, rr, 300));
        H2 = squeeze(pil_inv(2, 1, rr, 300));
        H3 = conj(squeeze(pil_inv(2, 1, rr+1, 300)));
        H4 = -1*conj(squeeze(pil_inv(1, 1, rr+1, 300)));
        H = [H1 H2; H3 H4];
        u(:, rr) = svd(H);
    end
    u = mean(u.');
end

bursts_rec = bursts_sent;

for nb = 1:length(bursts_sent)
    burst = bursts_sent(nb);
    
    num_simb_rafaga = burst.n_symbols;

    datos_rx_burst = datos_rx(:, (burst.symbol_offset-1)+(1:num_simb_rafaga), ...
        (burst.subchannel_offset-1)*24+(1:burst.n_subchannels*24));
    
    symb_rx = slot_demapping(datos_rx_burst, num_simb_rafaga, Ntx, Nrx);
    
    % bursts_rec(nb).EVM = evm(symb_rx,burst.M);
    bursts_rec(nb).EVM = evm_exact(symb_rx, bursts_sent(nb).symbols);
    bursts_rec(nb).symbols = symb_rx;
    bursts_rec(nb).bits = [];
    bursts_rec(nb).BER = 0.0;
end

end
