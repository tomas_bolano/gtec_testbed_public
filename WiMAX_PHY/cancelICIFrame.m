function [s_sin_ici] = cancelICIFrame(s_freq_awgn, pilots_sent, D, Noff, m)

Nsymb = size(s_freq_awgn, 2);
s_sin_ici= zeros(size(s_freq_awgn));

for ii = 1:Nsymb
%     s_sin_ici(:,ii) = cancelICI_LS_SIC(s_freq_awgn(:,ii), pilots_sent(:,ii), squeeze(D(ii,:,:)), Noff, m);
    s_sin_ici(:,ii) = cancelICI_LS(s_freq_awgn(:,ii), pilots_sent(:,ii), squeeze(D(ii,:,:)), Noff);
%     s_sin_ici(:,ii) = cancelICI_SIC(s_freq_awgn(:,ii), pilots_sent(:,ii), squeeze(D(ii,:,:)), m);
end
