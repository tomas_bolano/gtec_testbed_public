
function [datos_rx,pil_inv,evm_subcarrier] = pusc_rx(config, simb_freq_rx, ch_estimation)
%%
% Estimates channel, equalizes, and extracts the data subcarriers from a
% WiMAX PUSC zone.
% Input arguments:
%   config: object from wimax_pusc_config.
%   simb_freq_rx: A Nsymb x Nfft matrix with the received subcarriers in
%                 the frequency domain.
%   ch_estimation: Structure with the information about the channel
%       estimator and ICI cancellator to be used. It must contain the
%       following fields:
%          * Algorithm: It can take the following values:
%             'Genie' -> Assumes all subcarriers are known. This method
%             relies on the SignalTx field of this structure.
%             'LinealInterp' -> Uses LS at pilot positions, and applies a
%             linear interpolation at data subcarriers.
%             'LeastSquares' -> Uses a mismatched LMMSE estimator, which
%             only assumes to be known the power of the channel
%             coefficients, and not the full covariance matrix.
%             'LeastSquaresIter' -> Uses the 'LeastSquares' estimator with
%             ICI cancellation, and the decided data symbols are used as
%             feedback with the 'Genie' estimator to refine the estimation.
%          * IciCancellation: 1 or 0, if ICI cancellation is activated or
%          not, respectively. If set to 1, the field BEM must be added to
%          this structure.
%          * SignalTx: Transmitted signal, used by the Genie estimator.
%          * BEM: A structure with information for the ICI cancellation 
%          algorithm, with fields: Method, Basis, Matrix1, Eigenvalues,
%          Matrix2.
%
% Output arguments:
%   datos_rx: A Nsymb x Nfft matrix with the equalized data subcarriers.
%   pil_inv: Optional output argument where the channel frequency response
%            will be stored if it is present, in a Nsymb x Nfft matrix.
%   evm_subcarrier: Optional output argument where an estimation of the EVM
%            per subcarrier will be stored if it is present, in a Nused x
%            Nfft matrix.

% narginchk(2,3);
% nargoutchk(1,3);

% if nargin < 3
%     Ntx = 1;
% end

if nargin < 3
    ch_estimation = {};
    ch_estimation.Algorithm = 'LeastSquares';
    ch_estimation.ICICancellation = 0;
end

Nrx = size(simb_freq_rx, 1);

% config = wimax_pusc_config(Nfft, Ntx);
Nfft = config.Nfft;

num_simb_trama = size(simb_freq_rx, 2);


if strcmp(ch_estimation.Algorithm, 'Genie') == 1
    if ch_estimation.IciCancellation == 0
        pil_inv = genie_channel_mimo_est(config, simb_freq_rx, ch_estimation.SignalTx);
        simb_correg = ls_channel_mimo_inv(config, simb_freq_rx, pil_inv);
    else
        [simb_correg, pil_inv] = genie_channel_ici_inv(config, simb_freq_rx, ch_estimation.SignalTx, ch_estimation.BEM);
    end
elseif strcmp(ch_estimation.Algorithm, 'LinealInterp') == 1
    if ch_estimation.IciCancellation == 0
        pil_inv = linear_channel_mimo_est(config, simb_freq_rx);
        simb_correg = ls_channel_mimo_inv(config, simb_freq_rx, pil_inv);
    else
        [simb_correg, pil_inv] = ici_cancellation(config, @linear_channel_mimo_est, simb_freq_rx, ch_estimation.BEM);
    end
elseif strcmp(ch_estimation.Algorithm, 'LeastSquares') == 1
    if ch_estimation.IciCancellation == 0
        pil_inv = ls_channel_mimo_est(config, simb_freq_rx);
        simb_correg = ls_channel_mimo_inv(config, simb_freq_rx, pil_inv);
    else
        [simb_correg, pil_inv] = ici_cancellation(config, @ls_channel_mimo_est, simb_freq_rx, ch_estimation.BEM);
    end
elseif strcmp(ch_estimation.Algorithm, 'LeastSquaresIter') == 1
    if ch_estimation.IciCancellation == 0
        pil_inv = ls_channel_mimo_est(config, simb_freq_rx);
        simb_correg = ls_channel_mimo_inv(config, simb_freq_rx, pil_inv);
    else
        [simb_correg, pil_inv] = ici_cancellation_iter(config, @ls_channel_mimo_est, simb_freq_rx, ch_estimation.BEM);
    end
else
    error('Channel estimation algorithm unknown.')
end
% elseif ch_estimation == 3
%     set(config.randomizer, 'NumBitsOut', Nfft);
%     simb_freq_rx = simb_freq_rx .* (ones(num_simb_trama, 1)*(1-2*generate(config.randomizer)'));
% 
%     [simb_correg, pil_inv] = pusc_channel_inv(simb_freq_rx, Nfft);

reset(config.randomizer);
set(config.randomizer, 'NumBitsOut', Nfft);
R = (ones(num_simb_trama, 1)*(1-2*generate(config.randomizer)'));
for ii = 1:Nrx
    simb_correg(ii, :, :) = squeeze(simb_correg(ii, :, :)) .* R;
end

% Se devuelven los coeficientes de canal estimados, solo para las
% posiciones de portadoras útiles (pilotos y datos).
if nargout > 1
    pil_inv = pil_inv(:,:,:,(1+config.guard_i):(Nfft-config.guard_d));
end

symbol_patterns = size(config.symbol_structure, 2);
datos_rx = zeros(Nrx, num_simb_trama, config.num_clusters*12);
pattern_idx = 1;
for ii = 1:Nrx
    for jj = 1:num_simb_trama
        data = config.symbol_structure(1, pattern_idx, :) == 2;
        datos_rx(ii, jj, :) = simb_correg(ii, jj, data);
        pattern_idx = pattern_idx+1;
        if pattern_idx > symbol_patterns
            pattern_idx = 1;
        end
    end
end

% Cáculo de la EVM por subportadora. Este cálculo se hace suponiendo que en
% la trama hay 6 ráfagas de 4 símbolos cada una, 2 con QPSK, 2 con
% 16-QAM y 2 con 64-QAM.
if nargout > 2
    evm_subcarrier = zeros(3, size(datos_rx, 3));
    for ii = 1:size(datos_rx, 3)
        evm_subcarrier(1, ii) = evm(squeeze(datos_rx(1, 1:8, ii)).', 4);
        evm_subcarrier(2, ii) = evm(squeeze(datos_rx(1, 9:16, ii)).', 16);
        evm_subcarrier(3, ii) = evm(squeeze(datos_rx(1, 17:24, ii)).', 64);
    end
    evm_subcarrier = 10*log10(mean(10.^(evm_subcarrier./10)));
end

for ii = 1:Nrx
    datos_rx(ii, :, :) = deintrlv(squeeze(datos_rx(ii, :, :)).',...
        config.ROM_entrelazado_portadoras).';
end
% datos_rx = datos_rx.';

end
