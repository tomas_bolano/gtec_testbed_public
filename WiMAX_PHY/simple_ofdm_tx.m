
function rx = simple_ofdm_tx(tx)

N = size(tx, 1);
G = 1/8;
Ng = round(N*G);

rx = ifft(tx);
rx = [rx(end-Ng+1:end, :); rx];

end
