
function bits_rec = cc_tailbiting_dec(config, symb, randomize, repetition)

index = 1;
bits_rec = [];
traceback = 72;

% config = wimax_encoding_config(M, code_rate);
M = config.M;

demodulator = modem.qamdemod(config.modulator, 'DecisionType', 'llr',...
    'OutputType', 'bit');

symb = symb / config.scale_factor;

if M==4 && nargin == 4
    symb_avg = zeros(48, length(symb)/(48*repetition));
    symb = reshape(symb, 48*repetition, length(symb)/(48*repetition));
    for ii = 1:size(symb, 2)
        symb_avg(:, ii) = mean(reshape(symb(:, ii), 48, repetition), 2);
    end
    symb = symb_avg(:).';
end

total_num_slots_block = length(symb)/48;

while total_num_slots_block > 0
    if total_num_slots_block <= config.max_num_slots
        num_slots_block = total_num_slots_block;
    elseif total_num_slots_block < 2*config.max_num_slots
        num_slots_block = ceil(total_num_slots_block/2);
    else
        num_slots_block = config.max_num_slots;
    end
    total_num_slots_block = total_num_slots_block-num_slots_block;
    
    interleaved_bits = demodulate(demodulator, symb(index:index+num_slots_block*48-1));
    interleaved_bits = interleaved_bits(:)';
    interleaved_bits = 7-quantiz(interleaved_bits,[0.001,.1,.3,.5,.7,.9,.999]);
    index = index+num_slots_block*48;
    %% ROM Interleaving
    k=0:length(interleaved_bits)-1;
    s = log2(M)/2;
    d = 16;
    %Primera permutacion
    m = ((length(interleaved_bits)/d) * mod(k,d) + floor(k/d));
    %Segunda permutacion
    indicesInterleaving = (s*floor(m/s) + mod((m + length(interleaved_bits) - floor(d*m/length(interleaved_bits))), s))+1;
    deintrlvd_bits = deintrlv(interleaved_bits, indicesInterleaving);

    if traceback/config.code_rate_value > length(deintrlvd_bits)
        tail_biting_bits = repmat(deintrlvd_bits, 1, 3);
    else
        tail_biting_bits = [ deintrlvd_bits(end-traceback/config.code_rate_value+1:end) deintrlvd_bits deintrlvd_bits(1:traceback/config.code_rate_value)];
    end

    bits_decoded = vitdec(tail_biting_bits, config.trellis, traceback, 'trunc', 'soft', 3, config.punct_pattern);
    if traceback/config.code_rate_value > length(deintrlvd_bits)
        bits_decoded = bits_decoded(length(bits_decoded)/3+1:end-length(bits_decoded)/3);
    else
        bits_decoded = bits_decoded(traceback+1:end-traceback);
    end
    
    if nargin >=3 && randomize == 1
        reset(config.randomizer);
        set(config.randomizer, 'NumBitsOut', length(bits_decoded));
        bits_decoded = double(xor(bits_decoded, generate(config.randomizer)'));
    end
    bits_rec = [bits_rec bits_decoded];
end
