function simb_corrected = ici_cancel_iter(simb_freq_rx_used, simb_freq_used, pilotos_interp,...
                                         B, config, Nfft, G) 

Nused = sum(config.symbol_structure(1, 1, :) ~= 0);

simb_corrected = zeros(size(simb_freq_rx_used));
simb_corrected(:, 1, :) = simb_freq_rx_used(:, 1, :);
simb_corrected(:, 24, :) = simb_freq_rx_used(:, 24, :);

[F1, F2] = ofdm_matrix(Nfft, Nused, G);
% load('ici_estimator_data.mat', 'totalG');

kk = 2;
H_est = squeeze(pilotos_interp(1, 1:6, config.ROM_portadoras_impar ~= 0)).';
% H_total = estimateH_LS(H_est, B, Nfft*(1+G), F1, F2, Nfft, 1:4, totalG);
H_total = estimateH_LS(H_est, B, Nfft*(1+G), F1, F2, Nfft, 1:4);
for ii = 2:4
    H = squeeze(H_total(ii, :, :));
    simb_corrected(:, kk, :) = removePilotsICI(H, ...
        squeeze(simb_freq_rx_used(1, kk, :)), ...
        squeeze(simb_freq_used(1, kk, :)));
    kk = kk+1;
end
for ii = 2:24-5
    H_est = squeeze(pilotos_interp(1, ii:ii+5, config.ROM_portadoras_impar ~= 0)).';
%     H_total = estimateH_LS(H_est, B, Nfft*(1+G), F1, F2, Nfft, 4, totalG);
    H_total = estimateH_LS(H_est, B, Nfft*(1+G), F1, F2, Nfft, 4);
    H = squeeze(H_total(4, :, :));

    simb_corrected(:, kk, :) = removePilotsICI(H, ...
        squeeze(simb_freq_rx_used(1, kk, :)), ...
        squeeze(simb_freq_used(1, kk, :)));

    kk = kk+1;
end
% H_total = estimateH_LS(H_est, B, Nfft*(1+G), F1, F2, Nfft, 5, totalG);
H_total = estimateH_LS(H_est, B, Nfft*(1+G), F1, F2, Nfft, 5);
H = squeeze(H_total(5, :, :));
simb_corrected(:, kk, :) = removePilotsICI(H, ...
    squeeze(simb_freq_rx_used(1, kk, :)), ...
    squeeze(simb_freq_used(1, kk, :)));

end

