function BEM = load_block_BEM(U1, S1, V1, U2, S2, V2, Q, Rs)
% Loads a BEM to use with the MMSE ICI estimator.
% Receives as parameters the SVD decompositions of the matrices Cg,bar{g},
% and Cbar{g},bar{g}, the order of the BEM to be considered, and a spatial
% channel correlation matrix.
% It returns a structure with the fields:
% Basis: U1
% Matrix1: S1*V1*U2
% Eigenvalues: S2
% Matrix2: V2
% These matrices are created also considering the spatial correlation.

% Nt = size(S1, 1);
% Nsymb = size(S1, 2);
U = U1(:,1:Q).';
Nrxtx = size(Rs,1);

S_cross = diag(S1);
% S_cross((Q+1):end) = 0;
S_cross = diag(S_cross(1:Q));
% S_cross = [S_cross; zeros(Nt-Q, Nsymb)];
S_num = kron(Rs, S_cross)*kron(eye(Nrxtx), V1(:, 1:Q)')*kron(eye(Nrxtx), U2);
S_den = diag(S2);
% S_den((Q+1):end) = 0;
S_den = kron(Rs, diag(S_den));

V = kron(eye(Nrxtx), V2');

BEM = {};
BEM.Method = 'LinearMMSE';
BEM.Basis = U;
BEM.Matrix1 = S_num;
BEM.Eigenvalues = S_den;
BEM.Matrix2 = V;