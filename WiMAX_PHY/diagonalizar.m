clear;
close all;


% v = 350;
v = 50:25:350;
fs = 8e6;
fc = 5.2e9;
fds = (v*1000/3600)*fc/3e8; % Maximo desplazamiento doppler en Hz
G = 1/8;
Nfft = 1024;
N = Nfft*(G+1);
Nsymb = 6;
% N = 64;
% Nsymb = 6;
Q = 5;
fdt = Nfft/fs*fds;
tau = (0:N-1)/fs;


% [V1 s1] = eig(C);
% [V3 s3] = eig(S);

Brayleigh = zeros(length(fds), Q, N*Nsymb);
Bdps = zeros(length(fds), Q, N*Nsymb);
Bgauss = zeros(length(fds), Q, N*Nsymb);
Bbigauss = zeros(length(fds), Q, N*Nsymb);
Bajakes = zeros(length(fds), Q, N*Nsymb);
jj = 1;
for fd = fds
    % b = ((fd*Nsymb*2)/fs)*besselj(0, 2*pi*fd*Nsymb*tau);
    %% Espectro Jakes
    b = besselj(0, 2*pi*fd*Nsymb*tau);
    C = toeplitz(b);
    
    %% Espectror plano
    [xx yy] = meshgrid(0:N-1, 0:N-1);
    vdmax = Nsymb*fd/fs;
    % vdmax = 3.9e-3;
    S = sin(2*pi*(yy-xx)*vdmax)./(pi*(yy-xx));
    S(eye(N)==1) = 2*Nsymb*fd/fs;

    %% Espectro de Jakes asimetrico
%     aj = autocorrFromPSD('jakesA', fd, tau*Nsymb, [0.4 1]);
%     AJ = toeplitz(aj);
    %% Espectro gaussian
%     g = exp(-2*pi^2*(fd/5)^2*(Nsymb*tau).^2);
%     G = toeplitz(g);
    
%     'SigmaGaussian1', 0.05, ...
%     'SigmaGaussian2', 0.1, ...
%     'CenterFreqGaussian1', -0.8, ...
%     'CenterFreqGaussian2', 0.4, ...
%     'GainGaussian1', 1, ...
%     'GainGaussian2', 1/10
    %% Espectro bigaussian
%     Abg = 1/(1+0.1);
%     g_bi = Abg*1*exp(-2*pi^2*(fd*0.05)^2*(Nsymb*tau).^2).*exp(-2*pi*j*(-0.8*fd)*(Nsymb*tau)) + ...
%            Abg*0.1*exp(-2*pi^2*(fd*0.1)^2*(Nsymb*tau).^2).*exp(-2*pi*j*(0.4*fd)*(Nsymb*tau));
%     G_bi = toeplitz(g_bi);

%     c = rayleighchan(1/fs, fd, 0, 0);
%     c.DopplerSpectrum = doppler.ajakes([0.4 1]);
%     aj = c.RayleighFading.FiltGaussian.ImpulseResponse;
%     A = toeplitz(aj);
    
    [V1 s_jakes U1] = svd(C);
    [V3 s_dps U3] = svd(S);
%     [V4 s_gauss U4] = svd(G);
%     [V5 s_bigauss U5] = svd(G_bi);
%     [V6 s_ajakes U6] = svd(AJ);
    
    for ii = 1:Q
        Brayleigh(jj, ii,:) = interp1((0:N-1)*Nsymb, V1(:,ii), 0:Nsymb*N-1,'spline')/sqrt(Nsymb);
        Bdps(jj, ii, :) = interp1((0:N-1)*Nsymb, V3(:,ii), 0:Nsymb*N-1,'spline')/sqrt(Nsymb);
%         Bgauss(jj, ii, :) = interp1((0:N-1)*Nsymb, V4(:,ii), 0:Nsymb*N-1,'spline')/sqrt(Nsymb);
%         Bbigauss(jj, ii, :) = interp1((0:N-1)*Nsymb, V5(:,ii), 0:Nsymb*N-1,'spline')/sqrt(Nsymb);
%         Bajakes(jj, ii,:) = interp1((0:N-1)*Nsymb, V6(:,ii), 0:Nsymb*N-1,'spline')/sqrt(Nsymb);
    end
    jj = jj+1;
end

name = sprintf('bases_%d_%dMHz', N, round(fs/1e6));
save([name '.mat'], 'v', 'fdt', 'Brayleigh', 'Bdps'); % Bgauss Bbigauss Bajakes s_jakes s_dps s_gauss s_bigauss s_ajakes

% 
% figure,
% plot(Bdps.');
% grid on;
% legend('Orden 0', 'Orden 1', 'Orden 2');
% figure,
% plot(Brayleigh.');
% grid on;
% legend('Orden 0', 'Orden 1', 'Orden 2');
