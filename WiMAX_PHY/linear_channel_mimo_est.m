function [h_freq] = linear_channel_mimo_est(config, simb_freq_rx)

% config = wimax_pusc_config(Nfft);
Nfft = config.Nfft;
Ntx = config.Ntx;
Nrx = size(simb_freq_rx, 1);
n_symb = size(simb_freq_rx, 2);
Nused = sum(config.symbol_structure(1, 1, :) == 2);

% simb_freq = pusc_tx(zeros(Ntx, n_symb, Nused), Nfft);
simb_freq = pusc_tx_config(config, zeros(Ntx, n_symb, Nused));
h_freq = zeros(Ntx, Nrx, n_symb, Nfft);

symbol_patterns = size(config.symbol_structure, 2);
pattern_idx = 1;
for ii = 1:Ntx
    for jj = 1:Nrx
        for kk = 1:n_symb
            pilot_carriers = config.symbol_structure(ii, pattern_idx,:) == 1;
            
            H_freq = squeeze(simb_freq_rx(jj, kk, pilot_carriers) ./ ...
                simb_freq(ii, kk, pilot_carriers));
            h_freq(ii, jj, kk, :) = interp1(find(pilot_carriers), H_freq, ...
                1:Nfft, 'linear', 'extrap');

            pattern_idx = pattern_idx+1;
            if pattern_idx > symbol_patterns
                pattern_idx = 1;
            end
        end
    end
end

