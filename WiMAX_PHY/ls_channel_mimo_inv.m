function simb_correg = ls_channel_mimo_inv(config, simb_freq_rx, h_freq)

Nfft = config.Nfft;
Ntx = size(h_freq, 1);
Nrx = size(h_freq, 2);
n_symb = size(h_freq, 3);

%% Equalizacion LS
if Ntx == 1
    simb_correg(1, :, :) = squeeze(simb_freq_rx) ./ squeeze(h_freq(1, 1, :, :));
elseif Ntx == 2
    if Nrx == 1
        % Alamouti
        simb_correg = zeros(Nrx, n_symb, Nfft);
        ii = 1;
        while ii <= n_symb
            rx1 = squeeze(simb_freq_rx(1, ii, :));
            rx2 = conj(squeeze(simb_freq_rx(1, ii+1, :)));
            H1 = squeeze(h_freq(1, 1, ii, :));
            H2 = squeeze(h_freq(2, 1, ii, :));
            H3 = conj(squeeze(h_freq(2, 1, ii+1, :)));
            H4 = -1*conj(squeeze(h_freq(1, 1, ii+1, :)));
            for jj = 1:Nfft
                H = [H1(jj) H2(jj);...
                    H3(jj) H4(jj)];
                rx = [rx1(jj); rx2(jj)];
                correg = (H'*H)\H'*rx;
                simb_correg(1, ii, jj) = correg(1);
                simb_correg(1, ii+1, jj) = correg(2);
            end
            ii = ii+2;
        end
    elseif Nrx ==2
        simb_correg = zeros(Nrx, n_symb, Nfft);
        ii = 1;
        while ii <= n_symb
            rx1 = squeeze(simb_freq_rx(1, ii, :));
            rx2 = squeeze(simb_freq_rx(2, ii, :));
            H1 = squeeze(h_freq(1, 1, ii, :));
            H2 = squeeze(h_freq(2, 1, ii, :));
            H3 = squeeze(h_freq(1, 2, ii, :));
            H4 = squeeze(h_freq(2, 2, ii, :));
            for jj = 1:Nfft
                H = [H1(jj) H2(jj);...
                    H3(jj) H4(jj)];
                rx = [rx1(jj); rx2(jj)];
                correg = (H'*H)\H'*rx;
                simb_correg(1, ii, jj) = correg(1);
                simb_correg(2, ii, jj) = correg(2);
            end
            ii = ii+1;
        end
    end
end

end
