function [D] = estimateH_MIMO_LS(H_est, B, N)
%     Construye la matriz del canal MIMO a partir de una BEM.
%     Utiliza las estimaciones de Nsymb símbolos OFDM con K portadoras útiles
%     cada uno. Se hace uso de una BEM de orden Q.
% 
%     H_est: Matriz KxNsymb con las estimaciones de simbolos individuales.
%     B: Matriz QxNsymb*K con los vectores de la BEM.
%     N: Tamaño total de los símbolos OFDM, considerando portadoras útiles,
%     guardas, y prefijo cíclico.

% m = N/2+(0:N:2*N).';
% M = [m.^0 m.^1 m.^2];

Ntx = size(H_est, 1);
Nrx = size(H_est, 2);
Nsymb = size(H_est, 3);
Nused = size(H_est, 4);
Q = size(B, 1);
M = B(:, N/2+(0:N:(Nsymb-1)*N)).';

H_ici = zeros(Ntx, Nrx, Nused, Q);
for ii = 1:Ntx
    for jj = 1:Nrx
        H_siso = squeeze(H_est(ii,jj, :, :));
        H_ici(ii, jj, :, :) = (pinv(M)*H_siso).';
    end
end

D = H_ici;

