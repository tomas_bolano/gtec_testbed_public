clear;
close all;

% v = 350;
v = 50:25:350;
fs = 8e6;
fc = 5.2e9;
fds = (v*1000/3600)*fc/3e8; % Maximo desplazamiento doppler en Hz
G = 1/8;
N = 1024;
Ng = N*G;
Nt = N*(G+1);
Nsymb = 6;
% N = 64;
% Nsymb = 6;
Q = 5;
fdt = N/fs*fds;

tau = (-Nsymb*Nt:Nsymb*Nt-1)/fs;

% [V1 s1] = eig(C);
% [V3 s3] = eig(S);

U1_d = zeros(length(fds), Nsymb*Nt, Nsymb);
S1_d = zeros(length(fds), Nsymb, Nsymb);
V1_d = zeros(length(fds), Nsymb, Nsymb);
U2_d = zeros(length(fds), Nsymb, Nsymb);
S2_d = zeros(length(fds), Nsymb, Nsymb);
V2_d = zeros(length(fds), Nsymb, Nsymb);


U1_j = zeros(length(fds), Nsymb*Nt, Nsymb);
S1_j = zeros(length(fds), Nsymb, Nsymb);
V1_j = zeros(length(fds), Nsymb, Nsymb);
U2_j = zeros(length(fds), Nsymb, Nsymb);
S2_j = zeros(length(fds), Nsymb, Nsymb);
V2_j = zeros(length(fds), Nsymb, Nsymb);

U1_aj = zeros(length(fds), Nsymb*Nt, Nsymb);
S1_aj = zeros(length(fds), Nsymb, Nsymb);
V1_aj = zeros(length(fds), Nsymb, Nsymb);
U2_aj = zeros(length(fds), Nsymb, Nsymb);
S2_aj = zeros(length(fds), Nsymb, Nsymb);
V2_aj = zeros(length(fds), Nsymb, Nsymb);

U1_rj = zeros(length(fds), Nsymb*Nt, Nsymb);
S1_rj = zeros(length(fds), Nsymb, Nsymb);
V1_rj = zeros(length(fds), Nsymb, Nsymb);
U2_rj = zeros(length(fds), Nsymb, Nsymb);
S2_rj = zeros(length(fds), Nsymb, Nsymb);
V2_rj = zeros(length(fds), Nsymb, Nsymb);

% Brayleigh = zeros(length(fds), Q, N*Nsymb);
% Bdps = zeros(length(fds), Q, N*Nsymb);
% Bgauss = zeros(length(fds), Q, N*Nsymb);
% Bbigauss = zeros(length(fds), Q, N*Nsymb);
% Bajakes = zeros(length(fds), Q, N*Nsymb);
jj = 1;
for fd = fds
    % vdmax = 3.9e-3;
    S = sin(2*pi*fd*tau)./(pi*tau);
    S(length(S)/2+1) = 2*fd;
    [U1_d(jj, :, :) S1_d(jj, :, :) V1_d(jj, :, :) ...
     U2_d(jj, :, :) S2_d(jj, :, :) V2_d(jj, :, :)] = block_autocorr(S, N, Ng, Nsymb);
    
    b = real(besselj(0, 2*pi*fd*tau));
    [U1_j(jj, :, :) S1_j(jj, :, :) V1_j(jj, :, :) ...
     U2_j(jj, :, :) S2_j(jj, :, :) V2_j(jj, :, :)] = block_autocorr(b, N, Ng, Nsymb);

%     aj = autocorrFromPSD('jakesA', fd, tau, [0.4 1]);
%     [U1_aj(jj, :, :) S1_aj(jj, :, :) V1_aj(jj, :, :) ...
%      U2_aj(jj, :, :) S2_aj(jj, :, :) V2_aj(jj, :, :)] = block_autocorr(aj, N, Ng, Nsymb);
%  
%     rj = autocorrFromPSD('jakesR', fd, tau, [0.4 1]);
%     [U1_rj(jj, :, :) S1_rj(jj, :, :) V1_rj(jj, :, :) ...
%      U2_rj(jj, :, :) S2_rj(jj, :, :) V2_rj(jj, :, :)] = block_autocorr(rj, N, Ng, Nsymb);
    jj = jj+1;
end

name = sprintf('bases_%d_%dMHz_MMSE', N, round(fs/1e6));
save([name '.mat'], 'v', 'fdt', 'U1_d', 'S1_d', 'V1_d', 'U2_d', 'S2_d', 'V2_d', ...
                         'U1_j', 'S1_j', 'V1_j', 'U2_j', 'S2_j', 'V2_j');%, ...
%                          'U1_aj', 'S1_aj', 'V1_aj', 'U2_aj', 'S2_aj', 'V2_aj', ...
%                          'U1_rj', 'S1_rj', 'V1_rj', 'U2_rj', 'S2_rj', 'V2_rj');

