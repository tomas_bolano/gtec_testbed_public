function simb_conf = filtrado_tx(simb_tiempo,perfil)

%% Conformado

%fvtool(fir8/8,1,firpm(128,[0 2*420/8192 2*604/8192 1],[1 1 0 0]),1)
%fir8 = firpm(128,[0 2*430/8192 2*600/8192 1],[1 1 0 0])*8;

%fir7d = rcosine(1, 7, 'sqrt/fir', 0.3,6)/sqrt(7);
%fir51 = rcosine(1, 5, 'sqrt/fir', 0.1,8)/sqrt(5)*5;
%fir52 = rcosine(1, 6, 'sqrt/fir', 0.2,7)/sqrt(6)*5;
%fir4 = rcosine(1, 4, 'sqrt/fir', 0.1,8)/sqrt(4)*4;
%fir2 = rcosine(1, 4, 'sqrt/fir', 0.5,6)/sqrt(4)*2;

fir7d = firnyquist(50,7,0.3);
fir51 = firnyquist(80,5,0.15)*5;
%fir51 = rcosine(1, 5, 'sqrt/fir', 0.15,16)/sqrt(5)*5;
fir52 = firnyquist(36,5,0.4)*5;

fir4 = firnyquist(68,4,0.15)*4;
fir2 = firnyquist(32,2,0.2)*2;   % antes eran 20 coefs


%d = fdesign.halfband('Type','Lowpass',.1, 40);
%Hfir = design(d,'equiripple');
%fir_half = Hfir.Numerator*2;

simb_conf = simb_tiempo;
switch perfil
    case 0
        simb_conf = conv(fir51,upsample(simb_conf,5));
        %simb_conf = conv(fir4,upsample(simb_conf,4));
        %simb_conf = conv(fir2, upsample(simb_conf,2));
    case 1
        simb_conf = conv(fir51,upsample(simb_conf,5));
        simb_conf = conv(fir4,upsample(simb_conf,4));
        simb_conf = downsample(conv(fir7d,simb_conf),7);
        simb_conf = conv(fir52,upsample(simb_conf,5));
    case 2
        simb_conf = conv(fir51,upsample(simb_conf,5));
        simb_conf = conv(fir2,upsample(simb_conf,2));
    case 3
        simb_conf = conv(fir4,upsample(simb_conf,4));
        simb_conf = conv(fir2,upsample(simb_conf,2));
        
%         simb_conf = conv(fir_half,upsample(simb_conf,2));
%         simb_conf = conv(fir_half,upsample(simb_conf,2));
%         simb_conf = conv(fir_half,upsample(simb_conf,2));

    case 4
        simb_conf = conv(fir51,upsample(simb_conf,5));
        simb_conf = conv(fir2,upsample(simb_conf,2));
        simb_conf = downsample(conv(fir7d,simb_conf),7);
        %simb_conf = downsample(simb_conf,7);
        simb_conf = conv(fir52,upsample(simb_conf,5));     
%        simb_conf = src_farrow(simb_conf, 5/7, 3);
end
