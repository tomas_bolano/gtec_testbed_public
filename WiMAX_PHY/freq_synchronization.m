function [sout, freq_offset] = freq_synchronization(sin,Nfft,G)

symb_length = Nfft*(1+G);
tam_pref = Nfft*G;

Nrx = size(sin, 1);

nsymb = length(sin)/symb_length;




% sout = sin;

%% Estimación del offset en frecuencia fraccional en el preambulo
% Preambulo sin prefijo cíclico.
pr_noG = sin(1, tam_pref+1:symb_length);
% Primer tercio del preambulo.
fr1 = pr_noG(1:round(Nfft/3));
% Segundo tercio del preambulo.
fr2 = pr_noG(round(Nfft/3)+1:round(2*Nfft/3));
% Correlación cruzada entre los dos tercios.
fr_conv = xcorr(fr1, fr2);
% En el máximo de la correlacion se puede calcular un offset de frecuencia.
[~, i_max] = max(abs(fr_conv));
freq_offset_pr1 = angle(fr_conv(1, i_max))/(2*pi*round(Nfft/3));
sin = sin .* (ones(Nrx, 1)*exp(1j*2*pi*freq_offset_pr1*(0:length(sin)-1)));
sin_frame = reshape(squeeze(sin(1, :, :)), symb_length, nsymb);
freq_offset_pr2 = angle(sin_frame(end-tam_pref+1:end, 1)'*sin_frame(1:tam_pref,1))/(2*pi*Nfft);
sin = sin .* (ones(Nrx, 1)*exp(1j*2*pi*freq_offset_pr2*(0:length(sin)-1)));
freq_offset = freq_offset_pr1+freq_offset_pr2;
% sin = sin .* exp(1j*2*pi*freq_offset_pr*((0:length(sin)-1)-tam_pref));

%% Estimación del offset en frecuencia entero en el preambulo
% Preambulo recibido.
sin_frame = reshape(squeeze(sin(1, :, :)), symb_length, nsymb);
preamb_freq_rx = fftshift(fft(sin_frame(tam_pref+1:end,1)));
% Preambulo transmitido.
preamb_freq_tx = preambulos_OFDMA(1024, 0, 0);
% Se cogen solo las posiciones donde se encuentran los pilotos.
preamb_freq_pil_rx = preamb_freq_rx(abs(preamb_freq_tx) > 0);
preamb_freq_pil_tx = preamb_freq_tx(abs(preamb_freq_tx) > 0);
% Para reducir la influencia del canal, se calcula la secuencia diferencial
% de los pilotos, tanto en transmision como en recepcion.
preamb_diff_tx = preamb_freq_pil_tx(1:end-1) .* conj(preamb_freq_pil_tx(2:end));
preamb_diff_tx = preamb_diff_tx / max(preamb_diff_tx);
preamb_diff_rx = preamb_freq_pil_rx(1:end-1) .* conj(preamb_freq_pil_rx(2:end));

% Se correlan ambas secuencias diferenciales para calcular el offset
% entero.
xcorr_metric = abs(xcorr(preamb_diff_tx, preamb_diff_rx));
[vmax, imax] = max(xcorr_metric / (sum(abs(preamb_diff_rx))));
% Se evitan picos espureos.
if vmax > 0.3
    freq_offset_pr_int = (3*(imax - length(preamb_diff_tx)))/Nfft;

    freq_offset = freq_offset + freq_offset_pr_int;
    sin = sin .* (ones(Nrx, 1)*exp(1j*2*pi*freq_offset_pr_int*(0:length(sin)-1)));
end
% for it = 0:nsymb-1
%     exp_correct = exp( 1j*2*pi*freq_offset_pr*((0:(symb_length-1)) - tam_pref));
%     sin((symb_length*it+1):(symb_length*(it+1))) = sin((symb_length*it+1):(symb_length*(it+1))) .* exp_correct;    
% end
% sout = zeros(size(sin));
% for it = 0:nsymb-1
%     autocorr = sum(sin(symb_length*it + (1:tam_pref)) .* conj(sin(symb_length*it + Nfft + (1:tam_pref))));
%     freq_offset = angle(autocorr)/(2*pi*Nfft);
%     sout((symb_length*it+1):(symb_length*(it+1))) = sin((symb_length*it+1):(symb_length*(it+1))) .*exp(1j*2*pi*[0:(symb_length-1)]*(freq_offset));    
% end

sout = sin;
