function [y, x_noici] = cancelICI_SIC(x, s, H, m_demod)
%     Devuelve una versión de la señal de entrada sin la ICI, realizando la
%     cancelación mediante el algoritmo Successive Interference
%     Cancellation. Para ello utiliza lo símbolos decididos durante la
%     decidificación para eliminar sucesivamente interferencia a la señal
%     recibida.
%     En x_noici se devuelve una version de la señal de entrada tras haber
%     eliminado la componente de ICI generada por los símbolos decididos
%     estimados.
% Primero se cancela la ICI generada por los pilotos.
% interferencia = zeros(size(x));
% 
% for ii = 1:B
%     d_down = diag(H(ii+1:end, 1:end-ii));
%     d_up = diag(H(1:end-ii, ii+1:end));
%     interferencia = interferencia + [zeros(ii, 1); d_down.*s(1:end-ii)] + [d_up.*s(ii+1:end); zeros(ii,1)];
% end

[xx yy] = meshgrid(1:size(H,1), 1:size(H,2));
H_ici = H;
H_ici(xx == yy) = 0;

y = x-H_ici*s;
m_mod = modem.qammod(m_demod);

H_diag = diag(H);
[H_o orden] = sort(abs(H_diag), 1, 'descend');

x_ls = zeros(size(x));
for ii = orden.'
    if s(ii) == 0
        x_ls(ii) = y(ii)./H_diag(ii).';

        bits = demodulate(m_demod, x_ls(ii));
        decided_x = 1/sqrt(2)*modulate(m_mod, bits);

        Hcol = H(:, ii);
        Hcol(ii) = 0;
        y = y-(Hcol*decided_x);
    end
end
x_noici = y;
% y = y./H_diag;
y = x_ls;

end
