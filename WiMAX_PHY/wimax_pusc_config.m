function config = wimax_pusc_config(perfil, Ntx, Nrx)
% Returns a structure with the configuration of a PUSC zone for the WiMAX
% downlink. It includes some information about the whole frame, to allow an
% easy configuration of frames with the mandatory PUSC zone.
% Relevant fields:
%   Nfft: Number of subcarriers.
%   Ntx: Number of transmission antennas.
%   Nrx: Number of receive antennas.
%   Seed: A value used as seed to generate frames. This value allows to
%     generate predictable frames.
%   NumberOfSymbols: Number of OFDM symbols inside the PUSC zone.
%   GuardInteval: Fraction of the number of subcarriers which is used as
%     guard interval, with a cyclic prefix.
%   randomizer: LFSR used for subcarrier randomization.
%   guard_i, guard_d: Number of subcarriers at the left and right sides of
%     the spectrum used as guard bands.
%   num_clusters: Number of physical clusters contained in each OFDM
%     symbol. One cluster contains 14 subcarriers, 12 for data and 2 for
%     training.

if nargin < 2
    Ntx = 1;
end
if nargin < 3
    Nrx = 1;
end

config = {};

% config.Nfft = Nfft;
if perfil >= 2
    config.Nfft=1024;
else
    config.Nfft=512;
end
Nfft = config.Nfft;
config.Ntx = Ntx;
config.Nrx = Nrx;
config.Seed = sum(clock*100);
config.NumberOfSymbols = 24;
config.GuardInterval = 1/8;

% port_cluster=14;
port_datos_cluster=12;

if Nfft==512
    fis2log=[12, 13, 26, 9, 5, 15, 21, 6, 28, 4, 2, 7, 10, 18, 29, 17, 16, 3, 20, 24, 14, 8, 23, 1, 25, 27, 22, 19, 11, 0];
    num_clusters=30;
    PermutationBase5=[4,2,3,1,0];
    clusters_x_grupo=[10, 0, 10, 0, 10, 0];
    Nused=421;
elseif Nfft==1024
    fis2log=[6, 48, 37, 21, 31, 40, 42, 56, 32, 47, 30, 33, 54, 18, 10, 15, 50, 51, 58, 46, 23, 45, 16, 57, 39, 35, 7, 55, 25, 59, 53, 11, 22, 38, 28, 19, 17, 3, 27, 12, 29, 26, 5, 41, 49, 44, 9, 8, 1, 13, 36, 14, 43, 2, 20, 24, 52, 4, 34, 0];
    num_clusters=60;
    PermutationBase6=[3,2,0,4,5,1];
    PermutationBase4=[3,0,2,1];
    clusters_x_grupo=[12, 8, 12, 8, 12, 8];
    Nused=841;
end


%fis2log=fis2log+1;
% for cl=0:num_clusters-1
%     log2fis(cl+1)=find(fis2log==cl)-1;  % mapea los clusters l�gicos a f�sicos (0..num_clusters-1)
% end
[tmp, log2fis] = sort(fis2log);
log2fis = log2fis-1;

% num_subcanales=num_clusters/2;
% subcanales_x_grupo=clusters_x_grupo/2;

num_port_piloto=num_clusters*2;

num_port_datos = Nused - 1 - num_port_piloto;

cluster2grupo=-ones(1,num_clusters);
ini_grp=1;
fin_grp=clusters_x_grupo(1);
for grp=0:4
    cluster2grupo(ini_grp:fin_grp)=grp*ones(1,clusters_x_grupo(grp+1));
    ini_grp=fin_grp+1;
    fin_grp=fin_grp+clusters_x_grupo(grp+2);
end
cluster2grupo(ini_grp:fin_grp)=5*ones(1,clusters_x_grupo(6));


% pos_port_log2fis=-ones(1,num_port_datos);
port_log2fis=-ones(1,num_port_datos);
% grupos=[];
for ind=0:num_port_datos-1
    cluster=fix(ind/port_datos_cluster);      % cluster l�gico absoluto (de todo el s�mbolo)
    grupo=cluster2grupo(cluster+1);           % calculo el grupo en el que est� el cluster
    pos_dentro_cluster=ind-cluster*port_datos_cluster; % 0..11
    cluster_dentro_grupo=cluster-sum(cluster2grupo<grupo); % 0..clusters_x_grupo(grupo)-1
    subcanal_dentro_grupo=fix(cluster_dentro_grupo/2);
    pos_dentro_subcanal=pos_dentro_cluster + port_datos_cluster*mod(cluster_dentro_grupo,2);
    if (clusters_x_grupo(grupo+1)/2==5)
        Permutacion=PermutationBase5;
    elseif (clusters_x_grupo(grupo+1)/2==4)
        Permutacion=PermutationBase4;
    elseif (clusters_x_grupo(grupo+1)/2==6)
        Permutacion=PermutationBase6;
    end
    new_pos_dentro_grupo=subcarrier(pos_dentro_subcanal, subcanal_dentro_grupo, clusters_x_grupo(grupo+1)/2, Permutacion);
    if grupo==0
        pos_port_ini_grupo=0;
    else
        pos_port_ini_grupo=sum(clusters_x_grupo(1:grupo))*port_datos_cluster;
    end
    new_pos_port=pos_port_ini_grupo+new_pos_dentro_grupo;
    new_cluster=fix(new_pos_port/port_datos_cluster);
    new_pos_dentro_cluster=mod(new_pos_port,port_datos_cluster);
    new_cluster_fis=log2fis(new_cluster+1);
    port_log2fis(ind+1)=new_cluster_fis*port_datos_cluster + new_pos_dentro_cluster;
end

config.ROM_entrelazado_portadoras=port_log2fis+1;

% calculo el mapeo inverso

% ROM_desentrelazado_portadoras=zeros(1,length(ROM_entrelazado_portadoras));
% for x=0:length(ROM_entrelazado_portadoras)-1
%     ROM_desentrelazado_portadoras(x+1)=find(ROM_entrelazado_portadoras==x)-1;
% end
% [tmp, ROM_desentrelazado_portadoras] = sort(ROM_entrelazado_portadoras);
% ROM_desentrelazado_portadoras = ROM_desentrelazado_portadoras-1;


if Nfft==512
    config.guard_i=46;
    config.guard_d=45;
    config.num_clusters=30;
elseif Nfft==1024
    config.guard_i=92;
    config.guard_d=91;
    config.num_clusters=60;
end

% Creo las ROMs para insertar los pilotos y las guardas
% 2 -> enviar datos
% 1 -> enviar piloto
% 0 -> guarda
guardas_neg=zeros(1,config.guard_i);
guardas_pos=zeros(1,config.guard_d);
dc = 0;
if Ntx == 1
    cluster_par = [2 2 2 2 1 2 2 2 1 2 2 2 2 2];
    cluster_impar  =  [1 2 2 2 2 2 2 2 2 2 2 2 1 2];
%     cluster_par =     [2 2 2 2 1 2 2 2 2 2 2 1 2 2];
%     cluster_impar  =  [1 2 2 2 2 2 2 1 2 2 2 2 2 2];

    clusters_impar = [];
    clusters_par = [];
    for n=1:config.num_clusters/2
        clusters_impar = [clusters_impar cluster_impar];
        clusters_par = [clusters_par cluster_par];
    end

    config.ROM_portadoras_par = [ guardas_neg clusters_par dc clusters_par guardas_pos];

    config.ROM_portadoras_impar = [ guardas_neg clusters_impar dc clusters_impar guardas_pos];

    config.symbol_structure = zeros(Ntx, 2, Nfft);
    config.symbol_structure(1, :, :) = [config.ROM_portadoras_par; config.ROM_portadoras_impar];
elseif Ntx == 2
    cluster_antenna0_2tx = ...
        [2 2 2 2 3 2 2 2 1 2 2 2 2 2;...
         2 2 2 2 1 2 2 2 3 2 2 2 2 2;...
         3 2 2 2 2 2 2 2 2 2 2 2 1 2;...
         1 2 2 2 2 2 2 2 2 2 2 2 3 2];

    cluster_antenna1_2tx = ...
        [2 2 2 2 1 2 2 2 3 2 2 2 2 2;...
         2 2 2 2 3 2 2 2 1 2 2 2 2 2;...
         1 2 2 2 2 2 2 2 2 2 2 2 3 2;...
         3 2 2 2 2 2 2 2 2 2 2 2 1 2];
     clusters_antenna0_2tx = repmat(cluster_antenna0_2tx, ...
         1, config.num_clusters/2);
     clusters_antenna1_2tx = repmat(cluster_antenna1_2tx, ...
         1, config.num_clusters/2);
     clusters_antenna0_2tx = [...
         repmat(guardas_neg, 4, 1) ...
         clusters_antenna0_2tx ...
         repmat(dc, 4, 1) ...
         clusters_antenna0_2tx ...
         repmat(guardas_pos, 4, 1)];
     clusters_antenna1_2tx = [...
         repmat(guardas_neg, 4, 1) ...
         clusters_antenna1_2tx ...
         repmat(dc, 4, 1) ...
         clusters_antenna1_2tx ...
         repmat(guardas_pos, 4, 1)];
     config.symbol_structure = zeros(Ntx, 4, Nfft);
     config.symbol_structure(1, :, :) = clusters_antenna0_2tx;
     config.symbol_structure(2, :, :) = clusters_antenna1_2tx;
end

% config.randomizer = seqgen.pn('GenPoly', [1 zeros(1,8) 1 0 1],...
%     'InitialStates', [1 1 1 1 1 0 0 0 0 0 0]);

config.randomizer = commsrc.pn('GenPoly', [1 zeros(1,13) 1 1],...
    'InitialStates', [0 1 1 0 1 1 1 0 0 0 1 0 1 0 1]);
