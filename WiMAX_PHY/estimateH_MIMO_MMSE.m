function [D] = estimateH_MIMO_MMSE(H_est, BEM, N0)
% Construye los coeficientes BEM segun el algoritmo MMSE.
% Teniendo en cuenta que:
%  gLMMSE = U1*S1*V1*U2*S2*V2*g
% Los coeficientes vienen dados por:
%  cLMMSE = S1*V1*U2*S2*V2*g
% y asumiendo que U1 hace la labor de la la BEM.

Nrx = size(H_est, 1);
Ntx = size(H_est, 2);
Nsymb = size(H_est, 3);
Nused = size(H_est, 4);
Q = size(BEM.Basis, 1);
H_ici = zeros(Ntx, Nrx, Nused, Q);
for ii = 1:Nrx
    for jj = 1:Ntx
        H_siso = squeeze(H_est(ii,jj, :, :));
        H_ici(ii, jj, :, :) = (BEM.Matrix1/...
            (BEM.Eigenvalues+eye(Nrx*Ntx*Nsymb)*10^(-N0/10))*BEM.Matrix2*...
            H_siso).';
    end
end

D = H_ici;

