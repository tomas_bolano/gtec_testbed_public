function [U1 S1 V1 U2 S2 V2] = block_autocorr(b, N, Ng, Nsymb)

Nt = N+Ng;

index_auto = 1:2*N-1;
index_cross = zeros(1, N*Nsymb);
for ii=1:Nsymb
    index_cross((ii-1)*N+1:ii*N) = (ii-1)*Nt+(1:N);
end
weights_auto = [1:N N-1:-1:1];
C_auto = zeros(Nsymb, Nsymb);
C_cross = zeros(Nsymb*Nt, Nsymb);
for ii = 0:Nsymb*Nt-1
    in = index_cross+Nsymb*Nt-ii+Ng;
    C_cross(ii+1,:) = mean(reshape(b(in), N, Nsymb));
end
for ii = 0:Nsymb-1
    for kk = 0:Nsymb-1
        in = index_auto+(Nsymb-ii+kk-1)*Nt+Ng+1;
        C_auto(ii+1, kk+1) = sum(b(in).*weights_auto)/(N^2);
    end
end
[U2 S2 V2] = svd(C_auto, 'econ');
[U1 S1 V1] = svd(C_cross, 'econ');
% bem = C_cross*U1;
% sampled_bem = V1';
% eigenvalues = S1;
