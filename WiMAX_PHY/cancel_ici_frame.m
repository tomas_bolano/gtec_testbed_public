function simb_freq_rx_noici = cancel_ici_frame(simb_freq_rx, simb_freq, pilotos_interp, B,...
    Nused, Nfft, G)

config = wimax_pusc_config(Nfft);

%% Cancelacion de ICI
simb_freq_rx_noici = simb_freq_rx;
subcarrier_used = squeeze(config.symbol_structure(1, 1, :) ~= 0);
simb_freq_used = squeeze(simb_freq(1, :, subcarrier_used)).';
simb_freq_rx_used = squeeze(simb_freq_rx(1, :, subcarrier_used)).';
simb_corrected = zeros(size(simb_freq_rx_used));
simb_corrected(:, 1) = simb_freq_rx_used(:, 1);
simb_corrected(:, 24) = simb_freq_rx_used(:, 24);


m4 = modem.qamdemod(modem.qammod('M', 4,...
    'SymbolOrder', 'gray',...
    'InputType', 'bit'), 'DecisionType', 'hard',...
    'OutputType', 'bit');
m16 = modem.qamdemod(modem.qammod('M', 16,...
    'SymbolOrder', 'gray',...
    'InputType', 'bit'), 'DecisionType', 'hard',...
    'OutputType', 'bit');
m64 = modem.qamdemod(modem.qammod('M', 64,...
    'SymbolOrder', 'gray',...
    'InputType', 'bit'), 'DecisionType', 'hard',...
    'OutputType', 'bit');

[F1, F2] = ofdm_matrix(Nfft, Nused, G);
% load('ici_estimator_data.mat', 'totalG');

kk = 2;
H_est = squeeze(pilotos_interp(1, 1, 1:6, subcarrier_used)).';
H_total = estimateH_LS(H_est, B, Nfft*(1+G), F1, F2, Nfft, 1:4);
m = m4;
for ii = 2:4
    H = squeeze(H_total(ii, :, :));
    [simb_corrected(:, kk), simb_freq_rx_noici(1, kk, config.ROM_portadoras_impar ~= 0)] = ...
        cancelICI_SIC(simb_freq_rx_used(:, kk), ...
        simb_freq_used(:, kk),...
        H, m);
    kk = kk+1;
end
for ii = 2:24-5
    if kk == 9
        m = m16;
    elseif kk == 17
        m = m64;
    end
    H_est = squeeze(pilotos_interp(1, 1, ii:ii+5, subcarrier_used)).';
    H_total = estimateH_LS(H_est, B, Nfft*(1+G), F1, F2, Nfft, 4);
    H = squeeze(H_total(4, :, :));
    [simb_corrected(:, kk), simb_freq_rx_noici(1, kk, config.ROM_portadoras_impar ~= 0)] = ...
        cancelICI_SIC(simb_freq_rx_used(:, kk), ...
        simb_freq_used(:, kk),...
        H, m);
    kk = kk+1;
end
H_total = estimateH_LS(H_est, B, Nfft*(1+G), F1, F2, Nfft, 5);
H = squeeze(H_total(5, :, :));
[simb_corrected(:, kk), simb_freq_rx_noici(1, kk, config.ROM_portadoras_impar ~= 0)] = ...
        cancelICI_SIC(simb_freq_rx_used(:, kk), ...
        simb_freq_used(:, kk),...
        H, m);
end