function simb = read_frame(filename,num_samples,offset_samples)

if nargin < 2
    num_samples = inf;
end

if nargin < 3
    offset_samples = 0;
end

fid = fopen(filename,'r','ieee-le');
if fid == -1
    disp('Error: the file doesn''t exist');
    return;
end

if offset_samples > 0
    status = fseek(fid, offset_samples*4, 'bof');
    if status == -1
        disp(ferror(fid))
    else
        disp(sprintf('Seeked %d bytes',offset_samples*4))
    end
end

data = fread(fid,num_samples*2,'int16');
disp(sprintf('%d IQ symbols were read',length(data)/2))
fclose(fid);

simb = double(data(1:2:end)) + 1j*double(data(2:2:end));

simb = simb.' * 2^-13;

