% function [bursts_rec, CSI] = PHY_rx(config, bursts_sent, simb_conf,est_algorithm)
% Decodes an OFDMA frame generated with the PHY_tx function.
% Input parameters:
%   perfil: same parameters as used to call PHY_tx.
%   burst_sent: same parameter as used to call PHY_tx. In this case, if the
%       'bits' field is missing, burst decoding and BER measure will not be
%       computed.
%   simb_conf: Ntx x Nfft*Nsymb matrix with the received symbols.
%   est_algorithm: Structure with the information about the channel
%       estimator and ICI cancellator to be used. It must contain the
%       following fields:
%          * Algorithm: It can take the following values:
%             'Genie' -> Assumes all subcarriers are known. This method
%             relies on the SignalTx field of this structure.
%             'LinealInterp' -> Uses LS at pilot positions, and applies a
%             linear interpolation at data subcarriers.
%             'LeastSquares' -> Uses a mismatched LMMSE estimator, which
%             only assumes to be known the power of the channel
%             coefficients, and not the full covariance matrix.
%             'LeastSquaresIter' -> Uses the 'LeastSquares' estimator with
%             ICI cancellation, and the decided data symbols are used as
%             feedback with the 'Genie' estimator to refine the estimation.
%          * IciCancellation: 1 or 0, if ICI cancellation is activated or
%          not, respectively. If set to 1, the field BEM must be added to
%          this structure.
%          * SignalTx: Transmitted signal, used by the Genie estimator.
%          * BEM: Matrix with the BEM to use during ICI estimation. Used
%          when IciCancellation field is set to 1.
%
% The function returns an array of structures with the same fields as
% burst_sent, but with the field bits set to the received bits. Moreover,
% the following fields are added to each burst:
%   symbols: received complex symbols of the burst.
%   EVM: Error Vector Magnitude of the received symbols (dB) of the burst.
%   Only if a 'symbols' field is present in the received parameter
%   burst_sent.
%   BER: bit error ratio of the burst. Only if a 'bits' field is present in
%   the received burst_sent structure.
%
% Optionally, it can return information about synchronization and channel
% state. The following fields are available:
%   TimeOffset: Number of samples from the beginning of the received signal
%     in the parameter 'simb_conf' where the frame starts.
%   FrequencyOffset: Estimated carrier frequency offset from the preamble
%     of the frame.
%   ChannelFR: A Nsymb x Nused matrix with the estimated channel frequency
%     response of the channel for the first antenna pair.
%   ChannelIR: A Nsymb x Ng matrix with the estimated impuse response of
%     the channel for the first antenna pair.
%   MeanChannelEV: Mean channel eigenvalues of the frame when MIMO
%     transmissions are processed.
function [bursts_rec, CSI, error_sinc] = PHY_rx(config, bursts_sent, simb_conf,est_algorithm)

nargoutchk(1, 3);

time_offset = [];
bursts_rec = [];
CSI = {};

Nfft = config.Nfft;
Ntx = config.Ntx;
Nrx = config.Nrx;
G = config.GuardInterval;
num_simb_trama = config.NumberOfSymbols;

desv_sincr = 0;


tam_pref = G*Nfft;

%% Filtrado Rx

% eliminamos el offset DC
% simb_conf = simb_conf - mean(simb_conf);
% simb_conf = simb_conf - filter(2^(-8),[1 -(1-2^(-8))],simb_conf);
% simb_diez = sig_in;

sig_in = simb_conf;
simb_diez = sig_in;

%% Sincronizaci�n
% Tener 1 de las 2 lineas de abajo (2 metodos distintos de sincr).
time_offset = synchronization(simb_diez,Nfft,G);
%simb_diez = simb_diez .*exp(1j*2*pi*[0:(length(simb_diez)-1)]*(freq_offset));


time_offset=time_offset+desv_sincr;   % Para modificar el pto. de sincronizaci�n
if (time_offset<1) || (time_offset+(num_simb_trama+1)*Nfft*(1+G)-1)>length(simb_diez)
    error_sinc = 1;
    snr_rx = -1;
    evm_rx = -1;
	return;
else
    error_sinc = 0; 
end

simb_sinc = simb_diez(:, time_offset:(time_offset+(num_simb_trama+1)*Nfft*(1+G)-1));
[simb_sinc, freq_offset_pr] = freq_synchronization(simb_sinc,Nfft,G);
simb_sinc2 = zeros(Nrx, num_simb_trama+1, Nfft*(1+G));
for ii = 1:Nrx
    simb_sinc2(ii, :, :) = reshape(simb_sinc(ii, :),Nfft*(1+G),num_simb_trama+1).';
end
simb_sinc = simb_sinc2;
simb_sinc = simb_sinc(:, :, (tam_pref+1):end);

simb_freq_rx = fft(simb_sinc,[],3)/sqrt(Nfft);
simb_freq_rx = fftshift(simb_freq_rx,3);

%% Eliminar preambulo
simb_freq_rx = simb_freq_rx(:, 2:end,:);

%% Inversi�n del canal
%
% Llamada a la función de extracción de portadoras de datos para obtener la
% EVM por portadora.
%
% [datos_rx,~,evm_subcarrier] = pusc_rx(simb_freq_rx, Nfft);
% figure,
% plot(evm_subcarrier);

% datos_rx = pusc_rx_nodeintr(simb_freq_rx, Nfft, est_algorithm);

if nargout >= 2
    CSI = {};
    [datos_rx, h_freq] = pusc_rx(config, simb_freq_rx, est_algorithm);
    CSI.ChannelFR = squeeze(h_freq(1,1,:,:)).';
    ht = ifft(fftshift(CSI.ChannelFR));
    CSI.ChannelIR = ht(1:tam_pref, :);
    CSI.TimeOffset = time_offset;
    CSI.FrequencyOffset = freq_offset_pr;
    if Ntx == 2 && Nrx == 2
        H1 = squeeze(h_freq(1, 1, :, :));
        H2 = squeeze(h_freq(2, 1, :, :));
        H3 = squeeze(h_freq(1, 2, :, :));
        H4 = squeeze(h_freq(2, 2, :, :));
        CSI.Rrx = zeros(Nrx, Ntx);
        CSI.Rtx = zeros(Nrx, Ntx);
        %asdf = squeeze(h_freq(:, :, :, 300));
        u = zeros(Ntx, size(H1,1)*size(H1,2));
        idx = 1;
        for tt = 1:size(H1, 1)
            for ff = 1:size(H1, 2)
                H = [H1(tt, ff) H2(tt, ff);...
                     H3(tt, ff) H4(tt, ff)];
                CSI.Rrx = CSI.Rrx + H*H';
                CSI.Rtx = CSI.Rtx + H'*H;
                u(:, idx) = svd(H);
                idx = idx+1;
            end
        end
        u = mean(u.');
        CSI.MeanChannelEV = u;
        CSI.Rrx = CSI.Rrx./(config.Ntx*(size(H1,1)*size(H1,2)));
        CSI.Rtx = CSI.Rtx./(config.Ntx*(size(H1,1)*size(H1,2)));
    elseif Ntx == 2 && Nrx == 1
        H1 = squeeze(h_freq(1, 1, :, :));
        H2 = squeeze(h_freq(2, 1, :, :));
        u = zeros(Ntx, size(H1, 1)*size(H1, 2));
        idx = 1;
        for tt = 1:2:size(H1, 1)
            for ff = 1:size(H1, 2)
                H = [H1(tt, ff) H2(tt, ff);...
                     conj(H2(tt+1, ff)) -1*conj(H1(tt+1, ff))];
                u(:, idx) = svd(H);
                idx = idx + 1;
            end
        end
        u = mean(u.');
        CSI.MeanChannelEV = u;
    end
else
    datos_rx = pusc_rx(config, simb_freq_rx, est_algorithm);
end

bursts_rec = bursts_sent;

for nb = 1:length(bursts_sent)
    burst = bursts_sent(nb);
    
    num_simb_rafaga = burst.n_symbols;

    datos_rx_burst = datos_rx(:, (burst.symbol_offset-1)+(1:num_simb_rafaga), ...
        (burst.subchannel_offset-1)*24+(1:burst.n_subchannels*24));
    
    symb_rx = slot_demapping(datos_rx_burst, num_simb_rafaga, Ntx, Nrx);
    
    % bursts_rec(nb).EVM = evm(symb_rx,burst.M);
    bursts_rec(nb).symbols = symb_rx;
    if isfield(burst, 'symbols')
        bursts_rec(nb).EVM = evm_exact(symb_rx, bursts_sent(nb).symbols);
    end
    if isfield(burst, 'bits')
        bursts_rec(nb).bits = cc_tailbiting_dec(burst.coding_config, symb_rx);
        bursts_rec(nb).BER = sum(abs(bursts_rec(nb).bits-burst.bits))/length(burst.bits);
    end
end

end
