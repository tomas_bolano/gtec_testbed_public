% clear;
% close all;
% 
% ici_parameters;
% % x = -1:0.01:1;
% fd = fds(v == 325);

function P_ici = power_ici(fd, N, fs, PSDname, FreqMinMaxJakes)
% Calcula la potencia de la ICI en transmisiónes OFDM para un fading que tiene
% una función de autocorrelación PSDname.
%  * fd: máximo ensanchamiento Doppler en Hz.
%  * N: Número de portadoras OFDM.
%  * fs: frecuencia de muestreo (ancho de banda total, incluyendo guardas).
%  * PSDname: nombre de la función de autocorrelación del fading
%             jakesA: Jakes asimétrico
%             jakesR: Jakes restringido
%             jakes: Jakes normal
%             flat: Espectro plano
%  * FreqMinMaxJakes: en el caso de jakesA y jakesR, frecuencias máximas y 
%             mínimas.
% asdf = autocorrFromPSD('jakesA', fds(v == 50), x*(N/fs), [0.4 1]);
% asdf = real(besselj(0, 2*pi*fds(v == 325)*(N/fs)*x));


if strcmp(PSDname,'jakesA')
    integ = quad(@(x)(1-abs(x)).*autocorrFromPSD('jakesA', fd, x*(N/fs), FreqMinMaxJakes), -1, 1);
elseif strcmp(PSDname, 'jakesR')
    integ = quad(@(x)(1-abs(x)).*autocorrFromPSD('jakesR', fd, x*(N/fs), FreqMinMaxJakes), -1, 1);
elseif strcmp(PSDname,'jakes')
    integ = quad(@(x)(1-abs(x)).*real(besselj(0, 2*pi*fd*(N/fs)*x)), -1, 1);
elseif strcmp(PSDname, 'flat')
    integ = quad(@(x)(1-abs(x)).*autocorrFromPSD('flat', fd, x*(N/fs)), -1, 1);
end
P_ici = real(1-integ);
