clear;
close all;



bits = randi(2, 128, 1)*2-3;

bits_f = fft(bits);
bits_t = ifft(circshift([bits_f; zeros(1024-128, 1)], 128));
