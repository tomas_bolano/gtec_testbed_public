
function simb_freq = pusc_tx(config, datos_orig)

Ntx = config.Ntx;
Nrx = config.Nrx;
Nfft = config.Nfft;

% Ntx = size(datos_orig, 1);

% config = wimax_pusc_config(Nfft, Ntx);

%% Entrelazado de logicas a fisicas
for ii = 1:Ntx
    datos_orig(ii, :, :) = intrlv(squeeze(datos_orig(ii, :, :)).',...
        config.ROM_entrelazado_portadoras).';
end

% datos_orig = squeeze(datos_orig(1, :, :));

%% Construccion de los simbolos
num_simb_trama = size(datos_orig, 2);

symbol_patterns = size(config.symbol_structure, 2);
simb_freq = zeros(Ntx, num_simb_trama, Nfft);
pattern_idx = 1;
if Ntx == 1 && Nrx == 1
    pilotos_orig = 4/3;
else
    pilotos_orig = 1.8836;
end

for ii = 1:Ntx
    for jj = 1:num_simb_trama
        data_carriers = config.symbol_structure(ii, pattern_idx,:) == 2;
        pilot_carriers = config.symbol_structure(ii, pattern_idx,:) == 1;
        simb_freq(ii, jj, data_carriers) = datos_orig(ii, jj,:);
        simb_freq(ii, jj, pilot_carriers) = pilotos_orig;
        pattern_idx = pattern_idx+1;
        if pattern_idx > symbol_patterns
            pattern_idx = 1;
        end
    end
end

reset(config.randomizer);
set(config.randomizer, 'NumBitsOut', Nfft);
R = (ones(num_simb_trama, 1)*(1-2*generate(config.randomizer)'));
for ii = 1:Ntx
    simb_freq(ii, :, :) = squeeze(simb_freq(ii, :, :)) .* R;
end

end

