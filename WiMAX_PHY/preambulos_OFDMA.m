% Pre�mbulo 1K FFT: 86*2 portadoras de guarda
% Index  0, IDcell 0, Segment 0 : 0xA6F294537B285E1844677D133E4D53CCB1F182DE00489E53E6B6E77065C7EE7D0ADBEAF
% Index 32, IDcell 0, Segment 1 : 0x52849D8F020EA6583032917F36E8B62DFD18AD4D77A7D2D8EC2D4F20CC0C75B7D4DF708
% Index 64, IDcell 0, Segment 2 : 0xD27B00C70A8AA2C036ADD4E99D047A376B363FEDC287B8FD1A7794818C5873ECD0D3D56

% Pre�mbulo 512 FFT: 42(izq) + 41(der) portadoras de guarda
% Index  0, IDcell 0, Segment 0 : 0x66C9CB4D1C8F31D60F5795886EE02FFF6BE4
% Index 32, IDcell 0, Segment 1 : 0xE0187D89220D11B5F60DAC078A5E2EED6EF0
% Index 64, IDcell 0, Segment 2 : 0xC6325F42597BD48A8914944C7DB973D83E64


% preamb_hex = 'A6F294537B285E1844677D133E4D53CCB1F182DE00489E53E6B6E77065C7EE7D0ADBEAF';
% guard_i=86;
% guard_d=86;
% segment=0;
% Nfft=1024;

function preamb_out = preambulos_OFDMA(Nfft,idCell,segment)

if Nfft==512
    preamb_hex = '66C9CB4D1C8F31D60F5795886EE02FFF6BE4';
    pr_guard_i=42;
    pr_guard_d=41;
elseif Nfft==1024
    preamb_hex = 'A6F294537B285E1844677D133E4D53CCB1F182DE00489E53E6B6E77065C7EE7D0ADBEAF';
    pr_guard_i=86;
    pr_guard_d=86;
end

%segment = 0;

clear bin;
for n = 1:length(preamb_hex);
    bin((1+4*(n-1)):4*n)=dec2bin(hex2dec(preamb_hex(n)),4);
end
bits_sobrantes=(length(preamb_hex)*4*3+pr_guard_i+pr_guard_d-Nfft)/3;
bin=bin(1:end-bits_sobrantes);
preamb_bpsk = (bin=='1');
% preamb_bpsk = 4*sqrt(2)*(1/2 - preamb_bpsk);
preamb_bpsk = 2*(1/2 - preamb_bpsk);
preamb = upsample(preamb_bpsk,3);
switch (segment)
    case 0
        preamb = [zeros(1,pr_guard_i) preamb zeros(1,pr_guard_d)];
    case 1
        preamb = [zeros(1,pr_guard_i) 0 preamb(1:end-1) zeros(1,pr_guard_d)];
    case 2
        preamb = [zeros(1,pr_guard_i) 0 0 preamb(1:end-2) zeros(1,pr_guard_d)];
end
%preamb = fftshift(preamb);
preamb(Nfft/2 + 1) = 0;   %DC

escalado_preamb=2*sqrt(2);
% preamb_tiempo=ifft(fftshift(preamb)*escalado_preamb)*(Nfft-pr_guard_i-pr_guard_d)/sqrt(Nfft);
preamb_out=preamb*escalado_preamb;

% umbral_cuant_sincr = 0.05;
% 
% quantized_real = double(real(preamb_tiempo)>umbral_cuant_sincr) - double(real(preamb_tiempo)<-umbral_cuant_sincr);
% quantized_imag = double(imag(preamb_tiempo)>umbral_cuant_sincr) - double(imag(preamb_tiempo)<-umbral_cuant_sincr);
% 
% preamb_tiempo_q = quantized_real+1j*quantized_imag;
% preamb_tiempo_q = preamb_tiempo_q(end-63:end);
