% [time_offset, freq_offset] = synchronization(symb_time,Nfft,G);
% Synchronization for WiMAX OFDMA preambles. The CSB property is used for
% time offset estimation. The frequency offset is obtained by using the
% autocorrelation of the cyclic prefix.
% 
% time_offset --> Indicates the first sample of the preamble (including
%                 cyclic prefix).
% freq_offset --> Indicates the frequency offset. To correct the signal:
%       symb_time = symb_time .*exp(1j*2*pi*[0:(length(symb_time)-1)]*(freq_offset));
%

function [time_offset, freq_offset] = synchronization(symb_time, Nfft, G)

% Signals from all antennas are added up. Useful to obtain some gain when
% severan rx antennas are available.
% symb_time = sum(symb_time, 1);

% Rough estimate of the position of the frames. A scan using the property
% of triple repetition is used along all the received signal.
preamble_fragment_size = round(Nfft/3);
preamble_fragment_window = ceil(preamble_fragment_size/2);
rough_estimation = zeros(size(symb_time));
for n=1:Nfft/4:length(symb_time) - Nfft
	fragment1 = symb_time(:, n:n+preamble_fragment_window).';
	fragment2 = symb_time(:, n+preamble_fragment_size:n+preamble_fragment_size+preamble_fragment_window).';
    fragment3 = symb_time(:, n+2*preamble_fragment_size:n+2*preamble_fragment_size+preamble_fragment_window).';
	estimation1 = sum(fragment1.*conj(fragment2))./sum(fragment1.*conj(fragment1));
	estimation2 = sum(fragment2.*conj(fragment3))./sum(fragment2.*conj(fragment2));
	estimation3 = sum(fragment1.*conj(fragment3))./sum(fragment3.*conj(fragment3));
	rough_estimation(:, n) = estimation1.*estimation2.*estimation3;
end

[rough_sorted, candidate_idx] = sort(abs(rough_estimation), 2, 'descend');

Ng = Nfft*G;
% Array fo the correlation metric of one half of an OFDM symbol with the
% mirrored versión of the next half. It exploits the properties of real
% signals after FFT, which are symmetric.
corr = zeros(1,length(symb_time));
% Previous correlation normalized with received power.
corr_norm = zeros(1,length(symb_time));

% Buffer to compute recursively the autocorrelatión of the cyclic prefix
% with the end of an OFDM symbol.
buf_auto = zeros(1,Ng);
% Buffer to compute recursively the power at the beginning and at the end
% of an OFDM symbol. Useful to normalize when under highly time-selective
% scenarios, where the power during an OFDM symbol can change dramatically.
buf_pot = zeros(2,Ng);

% Only three largest candidates are tested.
% Ncandidates = 3;
Ncandidates = 2;
candidates_values = rough_sorted(:, 1:Ncandidates);
[~, best_channel] = max(var(candidates_values.'));
candidates = candidate_idx(best_channel, 1:Ncandidates);
symb_time = symb_time(best_channel, :);
search_index = zeros(size(symb_time));
search_index(candidates) = 1;
search_index = conv(search_index, ones(1, 1.5*Nfft+1));
% Remove tails of convolution (otherwise, indices at the beginning would be
% unreachable).
search_index = search_index(0.75*Nfft:end-0.75*Nfft);
search_index = find(search_index > 0.5);

potencia = 0;
% Autocorrelation at each time instant.
autocorr = zeros(1,length(symb_time));
% Autocorrelation at each time instant normalized with power.
autocorr_norm = zeros(1,length(symb_time));
clipped_symb_time = symb_time;%(symb_time>0)*2-1;
pow_limit = 10*var(symb_time);

for n=search_index(search_index < length(symb_time)-Nfft)
    % Symmetrical property of the preamble.
    corr(n) = sum(clipped_symb_time(n+1:n+Nfft/2-1) .* clipped_symb_time(n+Nfft-1:-1:n+Nfft/2+1));
    pot = sum(abs(clipped_symb_time(n+Nfft-1:-1:n+Nfft/2+1)).^2);

    if pot > 0.01
        corr_norm(n) = corr(n) / pot;
    else
        corr_norm(n) = 0;
    end
    
    % Autocorreltaion of the cyclic prefix.
    v = symb_time(n)*symb_time(n+Nfft)';
    if (n>1)
        autocorr(n)=v+autocorr(n-1)-buf_auto(1);
    else
        autocorr(n)=v+0-buf_auto(1);
    end
    buf_auto = [buf_auto(2:end) v];
    p_end = symb_time(n+Nfft)*symb_time(n+Nfft)';
    p_ini = symb_time(n)*symb_time(n)';
    potencia = (p_end+p_ini)/2+potencia-(buf_pot(1,1)+buf_pot(2,1))/2;
    buf_pot = [buf_pot(:, 2:end) [p_ini; p_end]];
    if potencia > pow_limit
        autocorr_norm(n) = autocorr(n)*autocorr(n)'/potencia^2;
    else
        autocorr_norm(n) = 0;
    end 
    
    if abs(corr_norm(n))*autocorr_norm(n) > 0.4
        
        break;
    end
end


timing_metric = abs(corr_norm).*autocorr_norm;

%%
% Se buscan picos de sincronizacion que no estén demasiado cerca del final
% del array, ya que puede ser detectada una trama incompleta.
%   [~,time_offset] = max(timing_metric(1:end-Nfft*(G+1)*25));
%
% En caso de no tener problema en acercarse demasiado al final, se
% considera toda la métrica.
time_offset = find(timing_metric > 0.2);
if isempty(time_offset)
    [~,time_offset] = max(timing_metric);
else
    time_offset = time_offset(1);
end

% time_offset = prefix_length+1; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

freq_offset = angle(autocorr(time_offset))/(2*pi*Nfft);

time_offset = max([time_offset-Ng 1]);

end
%disp('pto_sincr');
%disp(time_offset);

%disp('freq offset');
%disp(freq_offset);




