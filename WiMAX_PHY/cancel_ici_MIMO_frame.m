function [simb_freq_rx_noici, decided_rx] = cancel_ici_MIMO_frame(config, simb_freq_rx, simb_freq, pilotos_interp, BEM,...
    Nused)

% config = wimax_pusc_config(Nfft);
Nfft = config.Nfft;
B = BEM.Basis;
Ntx = size(pilotos_interp, 1);
Nrx = size(pilotos_interp, 2);
Nsymb = size(pilotos_interp, 3);
Q = size(B, 1);
G = config.GuardInterval;
Nt = Nfft*(1+G);
Ngroup = size(B, 2) / Nt;

%% Cancelacion de ICI
simb_freq_rx_noici = simb_freq_rx;
decided_rx = zeros(size(simb_freq_rx));
subcarrier_used = squeeze(config.symbol_structure(1, 1, :) ~= 0);
simb_freq_used = zeros(Nrx, Nsymb, sum(subcarrier_used));
% simb_freq_rx_used = zeros(Nrx, Nsymb, sum(subcarrier_used));

simb_freq_used(:, :, :) = squeeze(simb_freq(:, :, subcarrier_used));
% simb_freq_rx_used(:, :, :) = squeeze(simb_freq_rx(:, :, subcarrier_used));
simb_freq_rx_used = simb_freq_rx;

% simb_corrected = zeros(size(simb_freq_rx_used));
% simb_corrected(:, 1) = simb_freq_rx_used(:, 1);
% simb_corrected(:, 24) = simb_freq_rx_used(:, 24);

m4 = modem.qamdemod(modem.qammod('M', 4,...
    'SymbolOrder', 'gray',...
    'InputType', 'bit'), 'DecisionType', 'hard',...
    'OutputType', 'bit');
m16 = modem.qamdemod(modem.qammod('M', 16,...
    'SymbolOrder', 'gray',...
    'InputType', 'bit'), 'DecisionType', 'hard',...
    'OutputType', 'bit');
m64 = modem.qamdemod(modem.qammod('M', 64,...
    'SymbolOrder', 'gray',...
    'InputType', 'bit'), 'DecisionType', 'hard',...
    'OutputType', 'bit');
scale4 = 1/sqrt(2);
scale16 = 1/sqrt(10);
scale64 = 1/sqrt(42);

[F1, F2] = ofdm_matrix(Nfft, Nused, G);
% load('ici_estimator_data.mat', 'totalG');

%% Estimacion de los coeficientes
symbol_groups = Nsymb-Ngroup+1;
H_total = zeros(Ntx, Nrx, symbol_groups, Nused, Q);
for ii = 1:symbol_groups
    H_est = pilotos_interp(:, :, ii:ii+5, subcarrier_used);
    if strcmp(BEM.Method, 'LeastSquares') == 1
        H_total(:, :, ii, :, :) = estimateH_MIMO_LS(H_est, B, Nfft*(1+G));
    elseif strcmp(BEM.Method, 'LinearMMSE') == 1
        H_total(:, :, ii, :, :) = estimateH_MIMO_MMSE(H_est, BEM, 40);
    end
end

%% Cancelacion de ICI
m_demod = m4;
scale = scale4;
H_group = zeros(Ntx, Nrx, Nused, Q);
H_group(:, :, :, :) = squeeze(H_total(:, :, 1, :, :));
for ii = 1:3
    Bf = bem_freq(B, ii, F1, F2);
    [simb_freq_rx_noici(:, ii, :), decided_rx(:, ii, subcarrier_used)] = ...
            cancelICI_MIMO_SIC(squeeze(simb_freq_rx_used(:, ii, :)), ...
            squeeze(simb_freq_used(:, ii, :)), ...
            H_group, Bf, m_demod, scale);
end

Bf = bem_freq(B, 4, F1, F2);
for ii = 4:22
    if ii == 9
        m_demod = m16;
        scale = scale16;
    elseif ii == 17
        m_demod = m64;
        scale = scale64;
    end
    
    H_group(:, :, :, :) = squeeze(H_total(:, :, ii-3, :, :));
    [simb_freq_rx_noici(:, ii, :), decided_rx(:, ii, subcarrier_used)] = ...
        cancelICI_MIMO_SIC(squeeze(simb_freq_rx_used(:, ii, :)), ...
        squeeze(simb_freq_used(:, ii, :)), ...
        H_group, Bf, m_demod, scale);
end

for ii = 23:24
    Bf = bem_freq(B, ii-18, F1, F2);
    [simb_freq_rx_noici(:, ii, :), decided_rx(:, ii, subcarrier_used)] = ...
            cancelICI_MIMO_SIC(squeeze(simb_freq_rx_used(:, ii, :)), ...
            squeeze(simb_freq_used(:, ii, :)), ...
            H_group, Bf, m_demod, scale);
end


end
