% function simb_conf = PHY_tx(perfil,G,num_simb_trama,bursts,int_fact)
% Genera una trama OFDMA basada en el Downlink de Mobile WiMAX.
% Par�metros de entrada:
%   perfil: Indica el perfil a utilizar. Hay 5 valores posibles:
%       0 -> 3.5 MHz (4 MHz) 512 portadoras
%       1 -> 5 MHz (5.6 MHz) 512 portadoras
%       2 -> 7 MHz (8 MHz) 1024 portadoras
%       3 -> 8.75 MHz (10 MHz) 1024 portadoras
%       4 -> 10 MHz (11.2 MHz) 1024 portadoras
%   G: Indica el tama�o del prefijo c�clico.
%       Valores posibles: 1/4, 1/8, 1/16 o 1/32.
%   num_simb_trama: Indica el número de símbolos que contendrá el downlink.
%       Independientemente del número de ráfagas, se rellenará la trama con
%       símbolos que solo contienen pilotos hasta cumplir con el número de
%       símbolos especificado en este parámetro.
%   bursts: Indica la configuraci�n de las r�fagas a incluir en la trama. 
%       Debe ser un array de estructuras con los siguientes campos:
%           M : Indica el nivel de la modulaci�n QAM de la r�faga.
%               Puede ser 4, 16 o 64.
%           code_rate: Indica la tasa del c�digo convolucional a utilizar.
%               0 -> 1/2
%               1 -> 2/3 (solo v�lida para 64-QAM)
%               2 -> 3/4
%           bits: Vector fila con los bits a transmitir en la r�faga.
%   int_fact: Factor de interpolación de la señal banda base. Por
%   defecto es 1.
%
% La funci�n devuelve en simb_conf un vector fila con los s�mbolos
% complejos en banda base a enviar. La interpolaci�n ser� mayor o menor
% dependiendo del perfil utilizado.
% Las r�fagas se mapean utilizando pares de s�mbolos OFDM completos, si no
% hay bits suficientes para rellenar un s�mbolo, se rellenar� con unos.
% El valor �ptimo para rellenar 2 s�mbolos OFDM totalmente y evitar este
% "padding" es:
%       num_unc_bits = 24*num_clusters*log2(M)*code_rate_value
%   donde:
%       num_clusters = 30 (con 512 portadoras) o 60 (1024 portadoras).
%       M: nivel de modulaci�n.
%       code_rate_value: tasa de codificaci�n a usar (1/2, 2/3 o 3/4).
%
function [simb_conf, bursts_tx, simb_freq] = PHY_tx(config, bursts, int_fact)


Nfft = config.Nfft;
Ntx = config.Ntx;
Nrx = config.Nrx;
num_simb_trama = config.NumberOfSymbols;
G = config.GuardInterval;

if Ntx == 2 && Nrx == 2
    STC_rate = 2;
else
    STC_rate = 1;
end

if nargin < 5
    int_fact = 1;
end

if Nfft==512
    num_clusters=30;
elseif Nfft==1024
    num_clusters=60;
end

segment = 0;
idCell = 0;
bursts_tx = bursts;

%% Generar s�mbolos OFDM
%datos_orig = randsrc(num_simb_trama,num_clusters*12) + 1j*randsrc(num_simb_trama,num_clusters*12);
%datos_orig = datos_orig / sqrt(2);
% simb_freq = pusc_tx(zeros(Ntx, num_simb_trama, num_clusters*12), Nfft);
datos_orig_total = zeros(Ntx, num_simb_trama, num_clusters*12);
for ii=1:length(bursts)
    burst = bursts(ii);
    
    num_simb_rafaga = burst.n_symbols;
    num_subchannels = burst.n_subchannels;
    
    %bits_orig = randint(1, 48*300);
    % C�lculo del padding de bits (ones)
    coding_config = wimax_encoding_config(burst.M, burst.code_rate);
    bits_x_simb_ofdm = 24*num_clusters*log2(burst.M)*coding_config.code_rate_value*STC_rate;
    padding_bits = bits_x_simb_ofdm*ceil(length(burst.bits)/bits_x_simb_ofdm) - length(burst.bits);
    bits_orig = [burst.bits ones(1,padding_bits)];
    
    % Padding de s�mbolos (zeros)
    symb_orig = cc_tailbiting_enc(coding_config, bits_orig);
    bursts_tx(ii).symbols = symb_orig.';
    bursts_tx(ii).coding_config = coding_config;
%     num_simb_rafaga = ceil(length(symb_orig) / (num_clusters*12*2*2*STC_rate));
%     num_subchannels = ceil(length(symb_orig)/num_simb_rafaga);
%     symb_orig = [symb_orig zeros(1, num_simb_rafaga*num_subchannels*STC_rate-length(symb_orig))];
    
    datos_orig = slot_mapping(symb_orig, num_simb_rafaga, Ntx, Nrx);
    datos_orig_total(:, burst.symbol_offset:burst.symbol_offset + num_simb_rafaga - 1,...
        (burst.subchannel_offset-1)*24+1:(burst.subchannel_offset + num_subchannels - 1)*24) = datos_orig;
%     datos_orig = cat(3, datos_orig, zeros(Ntx, num_simb_rafaga, num_clusters*12-size(datos_orig, 3)));
end

% config = wimax_pusc_config(Nfft, Ntx);
simb_freq = pusc_tx(config, datos_orig_total);

%% Preambulo
preambulo_freq = zeros(Ntx, 1, Nfft);
preambulo_freq(1, :, :) = preambulos_OFDMA(Nfft,idCell,segment);
simb_freq_pr = [preambulo_freq simb_freq];

%% IFFT + CP
simb_tiempo = ifft(fftshift(simb_freq_pr,3),[],3)*Nfft/sqrt(Nfft);
simb_tiempo = cat(3, simb_tiempo(:, :,(Nfft*(1-G)+1):Nfft), simb_tiempo);  % Prefijo c�clico
%simb_tiempo(2:end,1) = (simb_tiempo(2:end,1)+simb_tiempo(1:(end-1),end))*0.5;    % Enventanado haciendo la media
%simb_tiempo(2:end,1) = simb_tiempo(1:(end-1),Nfft*G+1);    % Enventanado/postfijo c�clico

simb_tiempo_antenna = zeros(Ntx, Nfft*(1+G)*(num_simb_trama+1));
for ii = 1:Ntx
    simb_tiempo_antenna(ii, :) = ...
        reshape(squeeze(simb_tiempo(ii, :, :)).',...
        1,Nfft*(1+G)*(num_simb_trama+1));
end

% simb_tiempo_antenna = simb_tiempo_antenna*sqrt(Ntx);

%% Filtrado Tx
if int_fact == 1
    simb_conf = simb_tiempo_antenna;
else
    simb_conf = resample(simb_tiempo_antenna,int_fact,1,100);
end

%% Se crea el array de INT16
% idata = real(simb_conf);
% qdata = imag(simb_conf);
% 
% % First, the data is converted to Fix_16_13
% if max([abs(idata) abs(qdata)]) >= 4
%     disp('Warning!! The written data will be clipped')
% end
% 
% idata = round(idata * 2^13);
% qdata = round(qdata * 2^13);
% 
% idata = int16(idata);
% qdata = int16(qdata);
% 
% data = [idata;qdata];
% data = data(:);

end
