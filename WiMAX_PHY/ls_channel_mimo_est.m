function h_freq = ls_channel_mimo_est(config, simb_freq_rx)


%%
% LS con la matriz de FFT
% config = wimax_pusc_config(Nfft, Ntx);
Nfft = config.Nfft;
Ntx = config.Ntx;
L = 32;
n_symb = size(simb_freq_rx, 2);
Nrx = size(simb_freq_rx, 1);
Nused = sum(config.symbol_structure(1, 1, :) == 2);


%% Construcción de los símbolos transmitidos
% simb_freq = pusc_tx(zeros(Ntx, n_symb, Nused), Nfft);
simb_freq = pusc_tx_config(config, zeros(Ntx, n_symb, Nused));

%% Matrices del estimador
a = 0:Nfft-1;
F = 1/sqrt(Nfft)*exp(-1j*2*pi*(a'*a)/Nfft);
Fl = F(:, 1:L);

%% Estimador LS
h_freq = zeros(Ntx, Nrx, n_symb, Nfft);

% if ici == 1
%     p = power_ici(1500, 1024, 8e6, 'jakes');
% else
%     p=0;
% end
symbol_patterns = size(config.symbol_structure, 2);
pattern_idx = 1;
for ii = 1:Ntx
    for jj = 1:Nrx
        for kk = 1:n_symb
            pilot_carriers = config.symbol_structure(ii, pattern_idx,:) == 1;
%             data_carriers = config.symbol_structure(ii, pattern_idx,:) == 2;
            guard_carriers = config.symbol_structure(ii, pattern_idx,:) == 0;
            d = simb_freq_rx(jj, kk, pilot_carriers);
            g = simb_freq_rx(jj, kk, guard_carriers);
            snr_est = (var(d)*L-var(g))/var(g);
            
            Fp = F(fftshift(pilot_carriers), 1:L);
            s_rx = fftshift(simb_freq_rx(jj, kk, :));
            s_tx = fftshift(simb_freq(ii, kk, :));
            H_freq = squeeze( s_rx(fftshift(pilot_carriers)) ./ ...
                s_tx(fftshift(pilot_carriers)) );
            h_freq(ii, jj, kk, :) = fftshift(Fl*((Fp'*Fp + eye(L)*(1/snr_est))\Fp')*H_freq);

            pattern_idx = pattern_idx+1;
            if pattern_idx > symbol_patterns
                pattern_idx = 1;
            end
        end
    end
end
% 10*log10(snr_est/L)
end
