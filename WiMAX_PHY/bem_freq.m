function Bf = bem_freq(B, n, F1, F2)

Nt = size(F2, 1);
Nf = size(F2, 2);
N = size(F1, 1);
Q = size(B, 1);

% Bf = zeros(Q, Nf, Nf);
Bf = zeros(Q, N, Nf);
bn = B(:, (n-1)*Nt+1:n*Nt);
for ii = 1:Q
    b = bn(ii, :);
%     Bf(ii, :, :) = F1*diag(b)*F2;
    Bf(ii, :, :) = F1*bsxfun(@times, F2, b.');
end

end
