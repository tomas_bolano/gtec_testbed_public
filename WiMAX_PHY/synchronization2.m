% [time_offset, freq_offset] = synchronization2(symb_time,Nfft,G);
% Synchronization for WiMAX OFDMA preambles. The frequency offset is obtained by using the
% autocorrelation of the cyclic prefix.
% 
% time_offset --> Indicates the first sample of the preamble (including
%                 cyclic prefix).
% freq_offset --> Indicates the frequency offset. To correct the signal:
%       symb_time = symb_time .*exp(1j*2*pi*[0:(length(symb_time)-1)]*(freq_offset));
%
function [time_offset, freq_offset] = synchronization2(symb_time, Nfft, G)

prefix_length = Nfft*G;

corr_norm = zeros(1,length(symb_time));

buf_auto = zeros(1,prefix_length);
buf_pot = zeros(1,prefix_length);
potencia = 0;
autocorr = zeros(1,length(symb_time));
autocorr_norm = zeros(1,length(symb_time));
clipped_symb_time = symb_time;%(symb_time>0)*2-1;
pow_limit = var(symb_time)*10;
% snr_pref = zeros(1,length(symb_time));
% snr_csb = zeros(1,length(symb_time));
% snr_tdd = zeros(1,length(symb_time));
% noise = zeros(1,length(symb_time));
% p1 = zeros(1,length(symb_time));
% p2 = zeros(1,length(symb_time));
% noise_tdd = zeros(1,length(symb_time));

for n=1:length(symb_time)-Nfft
    % CSB
%     block1 = symb_time(n+1:n+round((Nfft-2)/6));
%     block2 = symb_time(n+round((Nfft-2)/6)+1:n+round(2*(Nfft-2)/6));
%     block3 = symb_time(n+round(2*(Nfft-2)/6)+1:n+round(3*(Nfft-2)/6));
%     block4 = symb_time(Nfft/2+n+1:Nfft/2+n+round((Nfft-2)/6));
%     block5 = symb_time(Nfft/2+n+round((Nfft-2)/6)+1:Nfft/2+n+round(2*(Nfft-2)/6));
%     block6 = symb_time(Nfft/2+n+round(2*(Nfft-2)/6)+1:Nfft/2+n+round(3*(Nfft-2)/6));
%     
%     corr = sum(block1 .* (fliplr(block2(1:end-1))));% + sum(block1 .* conj(block5));
%     pot = sum(abs(block2(1:end-1)).^2);% + sum(abs(block5).^2);
%     corr = corr*conj(corr);
%     pot = pot^2;
    corr = sum(clipped_symb_time(n+1:n+Nfft/2-1) .* clipped_symb_time(n+Nfft-1:-1:n+Nfft/2+1));
    pot = sum(abs(clipped_symb_time(n+Nfft-1:-1:n+Nfft/2+1)).^2);
    corr = corr + sum(clipped_symb_time(n+1+1:n+Nfft/2-1+1) .* clipped_symb_time(n+Nfft-2+1:-1:n+Nfft/2+1));
    %pot = pot + sum(abs(clipped_symb_time(n+Nfft-2:-1:n+Nfft/2)).^2);
    if pot > 0.01
        corr_norm(n) = corr / pot;
    else
        corr_norm(n) = 0;
    end
    
    % cyclic prefix
    v = symb_time(n)*symb_time(n+Nfft)';
    if (n>1)
        autocorr(n)=v+autocorr(n-1)-buf_auto(1);
    else
        autocorr(n)=v+0-buf_auto(1);
    end
    buf_auto = [buf_auto(2:end) v];
    p = symb_time(n+Nfft)*symb_time(n+Nfft)';
    potencia = p+potencia-buf_pot(1);
    buf_pot = [buf_pot(2:end) p];
    if potencia > pow_limit
        autocorr_norm(n) = autocorr(n)*autocorr(n)'/potencia^2;
    else
        autocorr_norm(n) = 0;
    end 
    
%   if abs(corr_norm(n))*autocorr_norm(n) > 0.6
%       break;
%   end
    

%     noise_csb = sum(abs(clipped_symb_time(n+1:n+Nfft/2-1) - conj(clipped_symb_time(n+Nfft-1:-1:n+Nfft/2+1))).^2);
%     power_csb = sum(abs(clipped_symb_time(n+1:n+Nfft/2-1)).^2);
%     snr_csb(n) = power_csb/noise_csb;
% 
%     if (n<length(symb_time)-Nfft-prefix_length+1)
%         noise = sum(abs(symb_time(n:n+prefix_length-1) - symb_time(n+Nfft:n+Nfft+prefix_length-1)).^2);
%         p1 = sum(abs(symb_time(n:n+prefix_length-1)).^2);
%         %p2(n) = sum(abs(symb_time(n+Nfft:n+Nfft+prefix_length-1)).^2);
%         snr_pref(n) = p1/noise;
%     end

%     if (n>prefix_length+1)&&(n<length(symb_time)-Nfft-prefix_length+1)
%         noise_tdd(n) = sum(abs(symb_time(n-prefix_length:n-1)).^2);
%         snr_tdd(n) = (p1(n)-noise_tdd(n))/noise_tdd(n);
%         
%         if (snr_tdd(n)<0)
%             snr_tdd(n) = 0;
%         end
%     end

end

figure;
subplot(211);
plot(abs(corr_norm));
hold on;
plot(autocorr_norm,'r');
subplot(212);
plot(abs(corr_norm).*autocorr_norm);

% figure;
% % plot((p1-noise_tdd)./noise_tdd);
% hold on;
% % plot((p2-noise_tdd)./noise_tdd,'r');
% plot(snr_pref);
% %plot((p2)./noise,'k');
% plot(snr_csb,'r');


[asdf,time_offset] = max(abs(corr_norm.*autocorr_norm));

freq_offset = angle(autocorr(time_offset))/(2*pi*Nfft);

time_offset = time_offset-prefix_length;

%disp('pto_sincr');
%disp(time_offset);

%disp('freq offset');
%disp(freq_offset);




