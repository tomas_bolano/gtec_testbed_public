function evm_dB = evm_exact(simb_rec, simb_tx)
% evm_dB = evm(simb_rec, M)
% Devuelve el error vector magnitude en dB
% simb_rec -> simbolos qam recibidos con la constelación normalizada a
%             potencia 1.
% M -> niveles de la modulacion QAM (4, 16 o 64)
s_real = real(simb_rec);
s_imag = imag(simb_rec);
s_real(s_real>3) = 3;
s_real(s_real<-3) = -3;
s_imag(s_imag>3) = 3;
s_imag(s_imag<-3) = -3;
simb_rec = s_real + 1j*s_imag;
ruido = simb_rec - simb_tx;
valor_cuadratico_ruido = ruido'*ruido/length(ruido);
valor_cuadratico_orig = simb_tx'*simb_tx/length(ruido);
evm_dB = 10 * log10(valor_cuadratico_ruido/valor_cuadratico_orig);
