function y = cancelICI_LS(x, s, H, Ntx, B)
% Cancela la ICI mediante una aproximación del metodo LS. Para evitar hacer
% una inversión de la matriz H del canal de forma completa, se hacen
% inversas de submatrices solapadas a lo largo de la diagonal principal.

%% Algoritmo de cancelacion LS
% y = zeros(size(x));
% Nused = 108;
Nused = size(H,1)/Ntx;
y = x-H*s;
% y = x;
% y(s == 1) = 0;
q = B*2;
% Ntx = size(H, 2)/Nused;
% Nrx = size(H, 1)/Nused;
indexH = zeros((q+1)*Ntx, 1);
indexY = q/2+Nused*(0:Ntx-1);
indexYhead = zeros((q/2+1)*Ntx, 1);
indexYtail = zeros((q/2+1)*Ntx, 1);
indexY_minihead = zeros((q/2+1)*Ntx, 1);
indexY_minitail = zeros((q/2+1)*Ntx, 1);
for ii = 1:Ntx
    indexH((q+1)*(ii-1)+1:(q+1)*ii) = (0:q)+Nused*(ii-1);
    indexYhead((q/2+1)*(ii-1)+1:(q/2+1)*ii) = (1:q/2+1)+Nused*(ii-1);
    indexYtail((q/2+1)*(ii-1)+1:(q/2+1)*ii) = (Nused-q/2:Nused)+Nused*(ii-1);
    indexY_minihead((q/2+1)*(ii-1)+1:(q/2+1)*ii) = (1:q/2+1)+(q+1)*(ii-1);
    indexY_minitail((q/2+1)*(ii-1)+1:(q/2+1)*ii) = (q/2+1:q+1)+(q+1)*(ii-1);
end

% indexH = [0:q Nused:Nused+q];
% indexY = [q/2; Nused+q/2];
% Htrunc = H(s==0, s==0);
% y_trunc = y(s==0);
% Por último se cancela la ICI generada por los datos entre si
for ii = 2:(Nused-q-1)
    if s(ii+q/2) == 0
%     Hmini = Htrunc(ii:ii+q, ii:ii+q);
%     y_mini = inv(Hmini)*y_trunc(ii:ii+q);
%     y_trunc(ii+q/2) = y_mini(q/2+1);
        Hmini = H(ii+indexH, ii+indexH);
        y_mini = inv(Hmini)*x(ii+indexH);
%         y(ii+indexY) = [y_mini(q/2+1) y_mini(q+q/2+2)];
        y(ii+indexY) = y_mini(q/2+1+((q+1)*(0:Ntx-1)));
    end
end

Hmini = H(1+indexH, 1+indexH);
y_mini = inv(Hmini)*x(1+indexH);
y(indexYhead) = y_mini(indexY_minihead);
% y(1:q/2+1) = y_mini(1:q/2+1);
% y(Nused+1:Nused+q/2+1) = y_mini(q+2:q+q/2+2);

Hmini = H(Nused-q+indexH, Nused-q+indexH);
y_mini = inv(Hmini)*x(Nused-q+indexH);
y(indexYtail) = y_mini(indexY_minitail);
% y(Nused-q/2:Nused) = y_mini(q/2+1:q+1);
% y(end-q/2:end) = y_mini(end-q/2:end);
% y = y_trunc;


% y = inv(H)*x;
