function [simb_correg,pilotos_interp] = genie_channel_ici_inv(config, simb_freq_rx, simb_freq, BEM)

% load('simb_freq.mat', 'simb_freq');
% config = wimax_pusc_config(Nfft, Ntx);
Nused = sum(squeeze(config.symbol_structure(1, 1, :)) ~= 0);


pilotos_interp = genie_channel_mimo_est(config, simb_freq_rx, simb_freq);

simb_freq_rx2 = cancel_ici_MIMO_frame(config, simb_freq_rx, simb_freq, pilotos_interp, BEM,...
    Nused);

pilotos_interp = genie_channel_mimo_est(config, simb_freq_rx2, simb_freq);

simb_correg = ls_channel_mimo_inv(config, simb_freq_rx2, pilotos_interp);

end
