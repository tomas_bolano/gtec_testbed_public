function [index] = subcarrier(k, s, N_subchannels, p_s)
% k -> posici�n de la subportadora dentro del subcanal [0:23]
% s -> n�mero de subcanal [0:N_subchannels-1]
% N_subchannels -> n�mero de subcanales del grupo
% p_s -> PermutationBase

%N_subchannels = 6;
N_subcarriers = 24;
DL_PermBase = 0;
%p_s = [3 2 0 4 5 1];
p_s = [p_s(s+1:end) p_s(1:s)];

n_k= mod(k+13*s, N_subcarriers);

index = N_subchannels*n_k + mod(p_s(mod(n_k, N_subchannels)+1) + DL_PermBase, N_subchannels);
