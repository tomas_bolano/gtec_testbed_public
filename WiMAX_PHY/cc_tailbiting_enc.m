
function symb = cc_tailbiting_enc(config, total_bits, randomize, repetition)
% This function codifies an array of bits with a convolutional encoder that
% follows the configuration indicated in the IEEE 802.16 standard. The
% tasks of this function are:
%   * Partition of the incoming bits in blocks using the concatenation
%   rules for blocks indicated in the standard.
%   * Randomization, encoding, and interleaving of each individual block.
%   * Mapping to a QAM constellation.

block_size = length(total_bits);
%total_bits = randint(1, block_size);

% config = wimax_encoding_config(M, code_rate);
M = config.M;

total_num_slots_block = block_size/(log2(M)*config.code_rate_value*48);

index = 1;
symb = [];

while total_num_slots_block > 0
    if total_num_slots_block <= config.max_num_slots
        num_slots_block = total_num_slots_block;
    elseif total_num_slots_block < 2*config.max_num_slots
        num_slots_block = ceil(total_num_slots_block/2);
    else
        num_slots_block = config.max_num_slots;
    end
    total_num_slots_block = total_num_slots_block-num_slots_block;
    bits = total_bits(index:index+num_slots_block*48*log2(M)*config.code_rate_value-1);
    index = index+num_slots_block*48*log2(M)*config.code_rate_value;
    
    if nargin>=3 && randomize == 1
        reset(config.randomizer);
        set(config.randomizer, 'NumBitsOut', length(bits));
        bits = double(xor(bits, generate(config.randomizer)'));
    end

    coded_bits = convenc([bits(end-config.constraint_length+2:end) bits], config.trellis, config.punct_pattern);
    coded_bits = coded_bits((config.constraint_length-1)/config.code_rate_value+1:end);

    %% ROM Interleaving
    k=0:length(coded_bits)-1;
    s = log2(M)/2;
    d = 16;
    %Primera permutacion
    m = ((length(coded_bits)/d) * mod(k,d) + floor(k/d));
    %Segunda permutacion
    indicesInterleaving = (s*floor(m/s) + mod((m + length(coded_bits) - floor(d*m/length(coded_bits))), s))+1;
    
    interleaved_bits = intrlv(coded_bits, indicesInterleaving);
    carriers = modulate(config.modulator, reshape(interleaved_bits, log2(M), length(interleaved_bits)/log2(M)));
    symb = [symb carriers];
end

symb = config.scale_factor*symb;
if M == 4 && nargin == 4
    symb = reshape(symb, 48, length(symb)/48);
    symb = repmat(symb, repetition, 1);
end

symb = symb(:).';
