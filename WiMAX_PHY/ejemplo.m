clear;

% Ejemplo de uso de la capa f�sica
% Configuraci�n general
perfil = 2;
Ntx = 2;
Nrx = 2;

config = wimax_pusc_config(perfil, Ntx, Nrx);
config.Seed = 1000;
bursts = burst_example(config);

% Generaci�n de la se�al a transmitir
[simb_tx, bursts_tx, simb_freq] = PHY_tx(config,bursts);
%save('simb_freq.mat', 'simb_freq');

%% Canal
% Aqu� se transmitir�a la se�al. Antes de transmitirla posiblemente sea
% necesario cuantizar los n�meros complejos a 16 bits. Al final de PHY_tx
% hay c�digo comentado para convertir los datos a enteros de 16 bits.
% simb_rx = simb_tx;   % en este ejemplo simplemente se copian los datos
simb_tx = [zeros(Ntx, 10) simb_tx zeros(Ntx, 400)];
ts = 1/8e6;
fd = 10;
for ii = 1:Ntx
    for jj = 1:Nrx
        chan(ii, jj) = ricianchan(ts, fd, 4); % stdchan(ts, fd, 'itur3GPAx');
    end
end

simb_rx = zeros(Nrx, length(simb_tx));
for ii = 1:Ntx
    for jj = 1:Nrx
        h_coeff = randn + 1j*randn;
%         simb_rx(jj, :) = simb_rx(jj, :) + h_coeff * awgn(simb_tx(ii, :), 30);
        simb_rx(jj, :) = simb_rx(jj, :) + filter(chan(ii, jj), awgn(simb_tx(ii, :), 40));
    end
end
% simb_rx = sum(simb_rx, 1);
% simb_tx = [simb_tx(1,:)*(randn+1j*randn);
%     simb_tx(2,:)*(randn + 1j*randn)];

% simb_rx = awgn(sum(simb_tx, 1), 300);


%% Receptor
% Se procesa la se�al recibida
load('bases_4MHz_ajakes.mat', 'Bdps');
load('bases_1024_8MHz_MMSE.mat', 'U1_j', 'S1_j', 'V1_j', 'U2_j', 'S2_j', 'V2_j');
Q = 5;
BEMmmse = load_block_BEM(squeeze(U1_j(12, :, :)), ...
    squeeze(S1_j(12, :, :)), ...
    squeeze(V1_j(12, :, :)), ...
    squeeze(U2_j(12, :, :)), ...
    squeeze(S2_j(12, :, :)), ...
    squeeze(V2_j(12, :, :)), Q, 1);
% Bpoly = [(1:Nfft*(G+1)*6).^0;...
%          (1:Nfft*(G+1)*6).^1;...
%          (1:Nfft*(G+1)*6).^2];
% Bd = squeeze(Bdps(12, 1:Q, :));

% El ultimo parametro de PHY_rx es una estructura con información para el
% estimador de canal.
% load('simb_freq.mat', 'simb_freq');
channel_estimation = {};
% Puede ser 'Genie', 'LeastSquares', 'LinealInterp', o 'LeastSquaresIter'
channel_estimation.Algorithm = 'LeastSquaresIter';
% Solo necesario si algoritmo es Genie
channel_estimation.SignalTx = simb_freq;
% Cambiar a 1 para activar la cancelacion de ICI
channel_estimation.IciCancellation = 0;
<<<<<<< HEAD
% channel_estimation.BEM = Bd;
% channel_estimation.SignalTx = simb_freq;
=======
BEM = {};
BEM.Method = 'LeastSquares';
BEM.Basis = Bd;
channel_estimation.BEM = BEM;
% channel_estimation.BEM = BEMmmse;
>>>>>>> analysis

[bursts_rx, CSI] = PHY_rx(config, bursts_tx, simb_rx, channel_estimation);
% ratio = size(hf, 1)/config.Nfft;
% hf = downsample(hf, 8);
% axis_t = (0:frame_OFDM_symbols-1)*ts*config.Nfft*(1+G)*1e3;
% axis_f = linspace(-1/ts/2, 1/ts/2, size(hf, 1))*1e-6*ratio;
% axis_tau = (0:(size(ht,1)-1))*ts*1e6;
% 
% [mesh_t, mesh_tau] = meshgrid(axis_t, axis_tau);
% figure;
% mesh(mesh_tau, mesh_t, 10*log10(abs(ht)));
% xlabel('tau [\mus]');
% ylabel('t [ms]');
% zlabel('Power [dB]');
% 
% [mesh_t, mesh_f] = meshgrid(axis_t, axis_f);
% figure;
% mesh(mesh_f, mesh_t, 10*log10(abs(hf)));
% xlabel('f [MHz]');
% ylabel('t [ms]');
% zlabel('Power [dB]');


% Se muestran las distintas r�fagas
fprintf('\n%s\n=====\n', channel_estimation.Algorithm)
% Se muestran las distintas r�fagas
for n = 1:length(bursts)
    fprintf('bursts_rx(%d): %f (%f)\n',n, bursts_rx(n).EVM, bursts_rx(n).BER);
end

channel_estimation.IciCancellation = 1;
bursts_rx2 = PHY_rx(config, bursts_tx, simb_rx, channel_estimation);

fprintf('\n%s+ICI\n=========\n', channel_estimation.Algorithm);
% Se muestran las distintas r�fagas
for n = 1:length(bursts)
    fprintf('bursts_rx(%d): %f (%f)\n',n, bursts_rx2(n).EVM, bursts_rx2(n).BER);
end

