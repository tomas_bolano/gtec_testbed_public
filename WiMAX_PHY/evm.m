function evm_dB = evm(simb_rec, M)
% evm_dB = evm(simb_rec, M)
% Devuelve el error vector magnitude en dB
% simb_rec -> simbolos qam recibidos con la constelación normalizada a
%             potencia 1.
% M -> niveles de la modulacion QAM (4, 16 o 64)

switch M
    case 64
        esc = sqrt(42);
    case 16
        esc = sqrt(10);
    case 4
        esc = sqrt(2);
end

simb_decididos = qamdemod(esc*simb_rec,M);
ruido = (simb_rec - qammod(simb_decididos,M)/esc);
valor_cuadratico_ruido = ruido'*ruido/length(ruido);
valor_cuadratico_orig = (qammod(simb_decididos,M)/esc)'*(qammod(simb_decididos,M)/esc)/length(qammod(simb_decididos,M));
evm_dB = 10 * log10(valor_cuadratico_ruido/valor_cuadratico_orig);



